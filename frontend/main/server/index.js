process.env.GRNE_ENV = process.env.GRNE_ENV || 'tst'

const express = require ('express')
const logger = require ('./logger')
const port = Number (process.env.PORT || '3000')
const setup = require ('./middlewares/frontendMiddleware')
const resolve = require ('path').resolve
const app = express ()

setup (app)

const host = process.env.HOST
const hostArg = host || null
const prettyHost = host || 'localhost'

app.listen (port, hostArg, (err) => {
  if (err)
    return logger.error (err.message)

  logger.appStarted (port, prettyHost)
})
