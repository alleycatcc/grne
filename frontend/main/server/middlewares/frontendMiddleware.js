module.exports = (app) => {
  const webpackConfig = require ('../../internals/lib/webpack/webpack.devserver.babel')
  const addDevMiddlewares = require ('./addDevMiddlewares')
  addDevMiddlewares (app, webpackConfig)

  return app
}
