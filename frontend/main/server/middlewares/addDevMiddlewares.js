// --- not passed through babel: i.e. const is ok, but not import.

const path = require ('path')
const webpack = require ('webpack')
const webpackDevMiddleware = require ('webpack-dev-middleware')
const webpackHotMiddleware = require ('webpack-hot-middleware')

const { createProxyMiddleware, } = require ('http-proxy-middleware')

const createWebpackMiddleware = (compiler, publicPath) =>
  webpackDevMiddleware (compiler, {
    publicPath,
    stats: 'errors-only',
  })

module.exports = function addDevMiddlewares (app, webpackConfig) {
  const compiler = webpack (webpackConfig)
  const middleware = createWebpackMiddleware (compiler, webpackConfig.output.publicPath)

  app.use (middleware)
  app.use (webpackHotMiddleware (compiler))

  const { outputPath, } = compiler

  app.use ('/api', createProxyMiddleware ({
    // auth: 'grne:put password here',
    target: 'http://grne-do-tst.alleycat.cc:2301',
    // target: 'http://localhost:8080',
    changeOrigin: true,
  }))

  /* This is the syntax for path rewrites should they be necessary -- not necessary for us because we
   * pass it with prefix and all to nginx.
   *   pathRewrite: { '^/api': '' }
   */

  // --- webpackDevMiddleware uses memory-fs to store build artifacts.
  const fs = middleware.context.outputFileSystem

  // --- recommended by migration guide, but causes an error.
  // compiler.close ()

  // --- this is necessary for browsing straight to a url which contains a path.
  app.get ('*', (req, res) => {
    fs.readFile (path.join (compiler.outputPath, 'index.html'),
      (err, file) => {
        if (err) res.sendStatus (404)
        else res.send (file.toString ())
      }
    )
  })
}
