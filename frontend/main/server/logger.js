const chalk = require ('chalk')

module.exports = logger = {

  error: (err) => {
    console.error (chalk.red (err))
  },

  appStarted: (port, host) => {
    console.log (`Server started! ${chalk.green ('✓')}`)

    console.log (`
Localhost: ${chalk.magenta (`http://${host}:${port}`)}
      LAN: ${chalk.magenta (`http://127.0.0.1:${port}`)}\n`)
  },
}
