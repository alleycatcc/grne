We divide the store into slices we call 'slices'.

The 'app' domain (reducer + selectors) is for values which affect the whole
app.

Think debug mode, global properties.

The 'domain' domain is for domain data (e.g. fetched through an API).

The 'ui' domain is for keeping track of UI state across components.
