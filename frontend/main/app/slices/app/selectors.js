import {
  pipe, compose, composeRight,
  map, dot, dot1, whenOk, ifOk,
  gt, ok, ne, eq,
  prop,
  divideBy, minus,
  whenFalsy, lets,
  defaultToV,
  head,
  take,
  tap,
  ifPredicate,
  reduce, sprintfN,
  appendToM, mergeToM, id,
} from 'stick-js/es'

import { createSelector, defaultMemoize as memoize, } from 'reselect'

import { fold, toJust, } from 'alleycat-js/es/bilby'
import { logWith, ierror, reduceX, } from 'alleycat-js/es/general'

const selectSlice = (store, props) => store | prop ('app')

export const makeSelectError = _ => createSelector (
  selectSlice,
  prop ('error'),
)

export const makeSelectHistoryCur = _ => createSelector (
  selectSlice,
  prop ('historyCur'),
)

export const makeSelectHistoryList = _ => createSelector (
  selectSlice,
  prop ('historyList'),
)

export const makeSelectHistoryWasBack = _ => createSelector (
  selectSlice,
  prop ('historyWasBack'),
)
