import {
  pipe, compose, composeRight,
  assoc, eq, id,
  condS, guard, otherwise, guardV,
  prop, dot1, tap, drop,
  lets,
  prepend,
  path, ifTrue,
  take, merge, not, split,
  deconstruct, deconstruct2,
} from 'stick-js/es'

import { logWith, composeManyRight, pop, } from 'alleycat-js/es/general'
import { ifPredicateResult, } from 'alleycat-js/es/predicate'

import { reducer, browseLinkForRouter, } from '../../common'

import {
  HISTORY_BROWSED,
  HISTORY_ADD_LEMMA,
  HISTORY_POP_LEMMA,
} from '../../containers/App/constants'

const match = 'match' | dot1
const historyMatchBrowse = /\/browse\/(\d+)$/ | match
const ifHistoryMatchBrowse = historyMatchBrowse | ifPredicateResult

const initialState = {
  // --- `true` means the reducer is totally corrupted and the app should halt.
  error: false,

  historyCur: void 8,
  historyWasBack: void 8,
  historyList: [],
}

const updateHistory = (lemmaId, naamFull) =>
  prepend ({
    lemmaId,
    naamFull,
    link: lemmaId | browseLinkForRouter,
  })
  >> take (7)

const removeQueryParams = split ('?') >> prop (0)

const reducerTable = {
  [HISTORY_BROWSED]: ({ data: { url, wasBack, }}) => url
    | removeQueryParams
    | ifHistoryMatchBrowse (
      (_, m) => merge ({
        historyCur: m [1] | Number,
        historyWasBack: wasBack,
      }),
      () => id,
  ),
  [HISTORY_ADD_LEMMA]: ({ data: { lemmaId, naamFull, }}) => deconstruct2 (
    ({ historyList, }) => lets (
      () => historyList | path ([0, 'lemmaId']) | eq (lemmaId),
      (skip) => skip | ifTrue (
        () => id,
        () => historyList
          | updateHistory (lemmaId, naamFull)
          | assoc ('historyList')
      ),
    ),
  ),
  [HISTORY_POP_LEMMA]: () => deconstruct2 (
    ({ historyList, }) => historyList
      | drop (1)
      | assoc ('historyList'),
  ),
}

export default reducer ('app', initialState, reducerTable)
