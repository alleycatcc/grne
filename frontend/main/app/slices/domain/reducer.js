import {
  pipe, compose, composeRight,
  assoc,
  id, always, tap, noop, ifTrue,
  lets,
  prop,
  arg1,
  reduce,
  map,
  take, head,
  mergeTo,
  concat,
  condS, eq, guard, otherwise,
  precat, die, defaultTo, sprintf1,
  dot2,
  timesV,
  merge,
  whenPredicate,
  gt,
  divideBy,
  minus,
  deconstruct2,
  plus, update,
  concatTo, precatTo,
} from 'stick-js/es'

import { select, } from 'redux-saga/effects'

import { foldJust, Just, Nothing, cata, fold, toJust, } from 'alleycat-js/es/bilby'
import { logAndroidPerf, logWith, defaultToV, composeManyRight, slice, roundUp, toString, } from 'alleycat-js/es/general'

import { reducer, } from '../../common'

import {
  ADVANCED_SEARCH_CLEAR_RESULTS,
  ADVANCED_SEARCH_FETCH_RESULTS,
  ADVANCED_SEARCH_LOAD_MORE_RESULTS,
  ADVANCED_SEARCH_FETCH_RESULTS_SUCCESS,
  ADVANCED_SEARCH_FETCH_RESULTS_ERROR,
  ADVANCED_SEARCH_SET_NUM_RESULTS,
  ADVANCED_SEARCH_SET_QUERY,
  ADVANCED_SEARCH_SET_QBUILDER,
  ADVANCED_SEARCH_SET_PAGE_NUM,
  ADVANCED_SEARCH_FETCH_PREVIEWS_ERROR,
  ADVANCED_SEARCH_FETCH_PREVIEWS_SUCCESS,
  ADVANCED_SEARCH_SELECT_RESULT,
  ENTRIES_FETCH_SUCCESS,
  ENTRIES_FETCH_ERROR,
  ENTRIES_CLEAR,
  ENTRIES_FETCH,
  LEMMATA_FETCH,
  LEMMATA_FETCH_SUCCESS,
  LEMMATA_FETCH_ERROR,
  LEMMATA_CLEAR,
  LEMMATA_CROP,
  LEMMATA_CROP_CLEAR,
  HISTORY_ADD_LEMMA,
  ADVANCED_SEARCH_INCREASE_PAGE_NUM,
  ADVANCED_SEARCH_RESET_PAGE_NUM,
  ADVANCED_SEARCH_SET_FIELD,
  ADVANCED_SEARCH_FETCH_PREVIEWS,
} from '../../containers/App/constants'

import {
  AdvSearchResults, AdvSearchResultsInit, AdvSearchResultsLoading, AdvSearchResultsLoadingMore, AdvSearchResultsError, AdvSearchResultsResults,
  advSearchToLoadingMore, advSearchLoadingXToResults,
  AdvSearchAuxFieldFulltext, AdvSearchAuxFieldVertM, AdvSearchAuxFieldHoofdW,
  Request, RequestInit, RequestLoading, RequestError, RequestResults,
  requestLoadingDefault,
  fetchUpdater, requestResultsCombine2,
  FetchReplace, FetchAppend, FetchPrepend,
  CropNone, CropFake, CropReal,
} from '../../types'

const whenGreaterThanOne = 1 | gt | whenPredicate
const initialState = {
  // --- `error=true` means the reducer is totally corrupted and the app should halt.
  error: false,

  lemmata: RequestInit,
  lemmataCrop: CropNone,
  // lemmataCropSave: null,
  // entriesExploreMode: false,

  entries: RequestInit,
  // --- used to keep track of the last action which updated the entries.
  entriesFetchType: void 8,

  maxLemmaId: Nothing,
  // entriesFetchingAppend: false,
  // entriesFetchingPrepend: false,
  // --- when prepending entries in entriesOnly mode, selectedEntryIdx needs to be increased by this
  // amount so it stays correct.
  entriesIdxShift: 0,
  advancedSearchResults: AdvSearchResultsInit,
  advancedSearchNumResultsSetting: 50,
  advancedSearchQuery: void 8,
  advancedSearchField: AdvSearchAuxFieldFulltext,

  // --- note, this is a mutable object, which at the moment is slightly more convenient in this
  // case, but against the rules of redux.
  // --- in particular, it's cleared when 'reset' is clicked.
  // --- @todo make immutable
  advancedSearchQBuilder: void 8,

  // --- note: 0-based.
  advancedSearchPageNum: -1,
  advancedSearchNumHits: Nothing,
  advancedSearchSelectedResult: null,
}

// @todo use transformer
const advSearchProcessResults = map (
  ({
    'link-id': linkId,
    'hoofdW-clean': hoofdWClean,
    'search-highlight': searchHighlight,
  }) => ({
    linkId,
    hoofdWClean,
    searchHighlight,
    html: null,
  })
)

const advSearchUpdateResultsWithHtml = previewResults => results => results | map (
  ({ linkId, hoofdWClean, searchHighlight, html=null, }) => ({
    linkId,
    hoofdWClean,
    searchHighlight,
    html: html | defaultTo (
      () => previewResults [linkId + 1]
        | defaultTo (
          () => die ((linkId + 1) | sprintf1 ('Unable to find entry %s in previewResults'))
        )
        | prop ('html')
        | defaultToV (null),
    )
  })
)

/*
const calculateMaxPage = (numHitsMb, numPerPage) => numHitsMb | map (
    defaultToV (0) >> divideBy (numPerPage) >> roundUp >> minus (1),
  )
*/

const reducerTable = {
  [ENTRIES_FETCH]: ({ data: { type, }}) => composeManyRight (
    update (
      'entries', fetchUpdater (type),
    ),
    merge ({
      entriesFetchType: type,
    }),
  ),

  // kill type? but it's necessary for shift.
  [ENTRIES_FETCH_SUCCESS]: ({ data: { results: { results }, type, }}) => composeManyRight (
    update (
      'entries', requestResultsCombine2 (results),
    ),
    deconstruct2 (
      ({ entriesIdxShift, }) => lets (
        () => type | cata ({
          FetchReplace: () => 0,
          FetchPrepend: () => results.length,
          FetchAppend: () => 0,
        }),
        (delta) => update (
          'entriesIdxShift', plus (delta),
        ),
      ),
    ),
  ),

  [ENTRIES_FETCH_ERROR]: (err) => merge ({
    entries: err | RequestError,
    entriesFetchType: void 8,
  }),

  [ENTRIES_CLEAR]: () => merge ({
    entries: RequestInit,
    entriesFetchType: void 8,
    entriesIdxShift: 0,
  }),

  [LEMMATA_FETCH]: ({ data: { type, }}) => update (
    'lemmata', fetchUpdater (type),
  ),

  // --- results: { results, metadata, }
  // @todo kill type
  [LEMMATA_FETCH_SUCCESS]: ({ data: { results: { results, metadata, }}}) => composeManyRight (
    update ('lemmata', requestResultsCombine2 (results)),
    assoc ('maxLemmaId', metadata.maxLemmaId),
  ),

  [LEMMATA_FETCH_ERROR]: (err) => merge ({
    lemmata: err | RequestError,
  }),

  [LEMMATA_CLEAR]: () =>
    RequestInit | assoc ('lemmata'),

  [LEMMATA_CROP]: ({ data: crop, }) => merge ({
    lemmataCrop: crop,
  }),

  [LEMMATA_CROP_CLEAR]: () => merge ({
    lemmataCrop: CropNone,
  }),

  // [ENTRIES_EXPLORE_MODE_CHANGE]: ({ data: onOff, }) =>
    // onOff | assoc ('entriesExploreMode'),

  [ADVANCED_SEARCH_CLEAR_RESULTS]: _ => merge ({
    advancedSearchResults: AdvSearchResultsInit,
    advancedSearchPageNum: 0,
  }),

  [ADVANCED_SEARCH_FETCH_RESULTS]: _ =>
    AdvSearchResultsLoading | assoc ('advancedSearchResults'),

  [ADVANCED_SEARCH_LOAD_MORE_RESULTS]: _ => state => state | assoc (
    'advancedSearchResults',
    state.advancedSearchResults | advSearchToLoadingMore,
  ),

  [ADVANCED_SEARCH_FETCH_RESULTS_SUCCESS]: ({ data: { data: { metadata, results, }}}) => state => {
    const { advancedSearchResults, } = state

    // --- for testing: set to Nothing for normal case.
    const limit = Nothing
    const test = limit | fold (
      (x) => [x, take (x)],
      [metadata.num_hits, id],
    )
    metadata.num_hits = test [0]
    const newResults = advSearchLoadingXToResults (
      metadata,
      results | test [1] | advSearchProcessResults,
    ) (advancedSearchResults)

    return state | merge ({
      advancedSearchResults: newResults,
      // @todo transform
      advancedSearchNumHits: metadata.num_hits | Just,
    })
  },

  [ADVANCED_SEARCH_FETCH_RESULTS_ERROR]: ({ data: err, }) =>
    AdvSearchResultsError (err) | assoc ('advancedSearchResults'),

  [ADVANCED_SEARCH_SET_NUM_RESULTS]: ({ data: num, }) =>
    num | assoc ('advancedSearchNumResultsSetting'),

  [ADVANCED_SEARCH_SET_QUERY]: ({ data: query, }) => merge ({
    advancedSearchQuery: query,
  }),

  [ADVANCED_SEARCH_SET_QBUILDER]: ({ data: qBuilder, }) => qBuilder
    | assoc ('advancedSearchQBuilder'),

  [ADVANCED_SEARCH_SET_PAGE_NUM]: ({ data: pageNum, }) =>
    pageNum | assoc ('advancedSearchPageNum'),

  [ADVANCED_SEARCH_INCREASE_PAGE_NUM]: () => deconstruct2 (
    ({ advancedSearchPageNum, }) => merge ({
      advancedSearchPageNum: advancedSearchPageNum + 1,
    }),
  ),

  [ADVANCED_SEARCH_RESET_PAGE_NUM]: () => merge ({
    advancedSearchPageNum: -1,
  }),

  [ADVANCED_SEARCH_SET_FIELD]: ({ data: advancedSearchField, }) => merge ({
    advancedSearchField,
  }),

  [ADVANCED_SEARCH_FETCH_PREVIEWS]: () => merge ({
    advancedSearchFetchingPreviews: true,
  }),

  [ADVANCED_SEARCH_FETCH_PREVIEWS_ERROR]: ({ data: err, }) => merge ({
    advancedSearchResults: AdvSearchResultsError (err),
  }),

  [ADVANCED_SEARCH_FETCH_PREVIEWS_SUCCESS]: ({ data: previewResults, }) =>
    composeManyRight (
      deconstruct2 (({ advancedSearchResults, }) => advancedSearchResults
        | map (([metadata, results]) => [
          metadata,
          results | advSearchUpdateResultsWithHtml (previewResults),
        ])
        | assoc ('advancedSearchResults'),
      ),
      merge ({
        advancedSearchFetchingPreviews: false,
      }),
    ),

  [ADVANCED_SEARCH_SELECT_RESULT]: ({ data: { history: _, lemmaId, highlighting, }}) =>
    [lemmaId, highlighting] | assoc ('advancedSearchSelectedResult'),
}

export default reducer ('domain', initialState, reducerTable)
