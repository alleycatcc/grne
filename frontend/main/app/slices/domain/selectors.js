// ------ sending a 'slice' parameter to a makeSelector function is a way to get it to accept a more
// specific chunk of the reducer: useful for calling the selector from the reducer.

import {
  pipe, compose, composeRight,
  map, dot, dot1, whenOk, ifOk,
  gt, ok, ne, eq,
  prop,
  divideBy, minus,
  whenFalsy, lets,
  defaultToV,
  head,
  take,
  tap,
  ifPredicate,
  reduce, sprintfN,
  appendToM, mergeToM, id, T, F,
  bindProp,
  timesF, always,
  die,
  last, not,
} from 'stick-js/es'

import { createSelector, defaultMemoize as memoize, } from 'reselect'

import memoize1 from 'memoize-one'

import { fakeLemma, } from '../../fake-lemma-html'
import {
  RequestInit, RequestLoading, RequestError, RequestResults,
  requestLoading, requestLoadingTag,
  fetchIsAppend, fetchIsPrepend, fetchIsReplace,
} from '../../types'

import { fold, toJust, cata, } from 'alleycat-js/es/bilby'
import { logWith, ierror, reduceX, min, max, roundUp, slice, } from 'alleycat-js/es/general'
import { ifEmptyList, } from 'alleycat-js/es/predicate'

export const toSet = xs => new Set (xs)
export const setAddM = x => s => (s.add (x), s)
export const setAddToM = s => x => (s.add (x), s)

export const setHas = bindProp ('has')
export const ifSetHas = setHas >> ifPredicate

const selectSlice = (store, props) => store | prop ('domain')

export const makeSelectError = _ => createSelector (
  selectSlice,
  prop ('error'),
)

// --- { metadata, results, }
export const makeSelectAdvancedSearchResults = _ => createSelector (
  selectSlice,
  prop ('advancedSearchResults'),
)

export const makeSelectAdvancedSearchResultsResults = _ => createSelector (
  makeSelectAdvancedSearchResults (),
  prop ('results'),
)

export const makeSelectAdvancedSearchGetResultForIdx = _ => createSelector (
  makeSelectAdvancedSearchResultsResults (),
  (results) => (idx) => results [idx],
)

export const makeSelectAdvancedSearchNumResultsSetting = _ => createSelector (
  selectSlice,
  prop ('advancedSearchNumResultsSetting'),
)

export const makeSelectAdvancedSearchQuery = _ => createSelector (
  selectSlice,
  prop ('advancedSearchQuery'),
)

export const makeSelectAdvancedSearchQBuilder = _ => createSelector (
  selectSlice,
  prop ('advancedSearchQBuilder'),
)

export const makeSelectAdvancedSearchPageNum = _ => createSelector (
  selectSlice,
  prop ('advancedSearchPageNum'),
)

export const makeSelectAdvancedSearchNumHits = _ => createSelector (
  selectSlice,
  prop ('advancedSearchNumHits'),
)

export const makeSelectAdvancedSearchField = _ => createSelector (
  selectSlice,
  prop ('advancedSearchField'),
)

export const makeSelectAdvancedSearchSelectedResult = _ => createSelector (
  selectSlice,
  prop ('advancedSearchSelectedResult'),
)

// --- 0-based.
export const makeSelectAdvancedSearchMaxPage = _ => createSelector (
  [
    makeSelectAdvancedSearchNumHits (),
    makeSelectAdvancedSearchNumResultsSetting (),
  ],
  (numHitsMb, numPerPage) => numHitsMb | map (
    defaultToV (0) >> divideBy (numPerPage) >> roundUp >> minus (1),
  ),
)

export const makeSelectAdvancedSearchFetchingPreviews = _ => createSelector (
  selectSlice,
  prop ('advancedSearchFetchingPreviews'),
)

const addHomonymNr = (lemmaId, homonymNr=null, lemma) =>
  homonymNr | ifOk (
    (nr) => [nr, lemma] | sprintfN ('%s. %s'),
    () => lemma,
  )

// --- @todo This gets repeated unnecessarily when new chunks are retrieved.

// --- lemmaId is 0-based.
const lemmataReducer = ((lemmata) => lets (
  // --- init acc
  () => ({
    entries: [], lemmaList: [], lemmaIdToIdx: {},
    lemmaIdMin: void 8, lemmaIdMax: void 8,
  }),
  // --- reducer
  () => (
      // --- deconstruct acc
      {
        entries, lemmaList, lemmaIdToIdx, lemmaIdMin, lemmaIdMax,
      },
      // --- deconstruct current element
      { id: lemmaId, html, htmlSchool, t1Gecontroleerd, lemma, homonymNr },
      // --- element idx is available because we are using `reduceX`
      idx,
    ) => ({
      entries: [lemmaId, homonymNr, lemma] | appendToM (entries),
      lemmaList: [lemmaId, homonymNr, html, htmlSchool, t1Gecontroleerd] | appendToM (lemmaList),
      lemmaIdToIdx: { [lemmaId]: idx, } | mergeToM (lemmaIdToIdx),
      lemmaIdMin: lemmaIdMin | ifOk (lemmaId | min, () => lemmaId),
      lemmaIdMax: lemmaIdMax | ifOk (lemmaId | max, () => lemmaId),
    }),
  (init, reducer) => lemmata | reduceX (reducer) (init),
))

const makeSelectLemmata = _ => createSelector (
  selectSlice,
  prop ('lemmata'),
)

// --- not reduced as in schoolreductie but as in transformed
const makeSelectLemmataReduced = lets (
  () => fold (id, []) >> lemmataReducer,
  (select) => select | memoize1,
  (_, select) => _ => createSelector (
    makeSelectLemmata (),
    select,
  ),
)

export const makeSelectLemmataReplacePending = _ => createSelector (
  selectSlice,
  prop ('lemmata') >> requestLoading,
)

const makeSelectEntriesLoadingMore = _ => createSelector (
  makeSelectEntriesFromReducer (),
  requestLoadingTag >> fold (id, null),
)

export const makeSelectEntriesAppending = _ => createSelector (
  makeSelectEntriesLoadingMore (),
  ifOk (fetchIsAppend, F),
)

export const makeSelectEntriesPrepending = _ => createSelector (
  makeSelectEntriesLoadingMore (),
  ifOk (fetchIsPrepend, F),
)

// --- this is different from EntriesAppending and EntriesPrepending because it maintains its value
// even after the entries have been set.
export const makeSelectEntriesFetchType = _ => createSelector (
  selectSlice,
  prop ('entriesFetchType'),
)

export const makeSelectLemmataCrop = _ => createSelector (
  selectSlice,
  prop ('lemmataCrop'),
)

// --- returns (selectedLemmaIdx) => adjustedIdx
export const makeSelectAdjustSelectedIdxForCrop = _ => createSelector (
  makeSelectLemmataCrop (),
  (crop) => crop | cata ({
    CropNone: id | always,
    CropFake: (selectedIdx, numBefore, numAfter) =>
      numBefore | always,
    CropReal: (beginIdx, endIdx) => minus (beginIdx),
  }),
)

const prepareFake = html => [null, null, html, null]

export const makeSelectLemmataList = _ => createSelector (
  [
	makeSelectLemmataReduced (),
    // --- @todo not using this.
	makeSelectLemmataCrop (),
  ],
  (lemmata, crop) => lets (
    () => lemmata | prop ('lemmaList'),
    (list) => crop | cata ({
      CropNone: () => list,
      CropFake: (selectedIdx, numBefore, numAfter) => [
        ... fakeLemma (prepareFake, 2),
        list [selectedIdx] | tap (logWith ('ll')),
        ... fakeLemma (prepareFake, 2),
      ],
      CropReal: (beginIdx, endIdx) => list
        | slice (beginIdx, endIdx + 1),
    }),
  ),
)

export const makeSelectHaveLemmata = _ => createSelector (
  makeSelectLemmataList (),
  prop ('length') >> ne (0),
)

export const makeSelectLemmaIdMin = _ => createSelector (
  makeSelectLemmataReduced (),
  prop ('lemmaIdMin'),
)

export const makeSelectLemmaIdMax = _ => createSelector (
  makeSelectLemmataReduced (),
  prop ('lemmaIdMax'),
)

export const makeSelectLemmaIdToIdx = _ => createSelector (
  makeSelectLemmataReduced (),
  prop ('lemmaIdToIdx'),
)

const lemmaIdToEntry = (idxTable, entryTable) => memoize (
  (lemmaId, failOk=false) => {
    const idx = idxTable [lemmaId]
    if (!ok (idx)) return failOk ?
      null :
      ierror ('no idx for lemma id', lemmaId)
    const entry = entryTable [idx]
    if (!ok (entry)) return failOk ?
      null :
      ierror ('no entry for lemma id', lemmaId)
    return entry
  }
)

const makeSelectEntriesDerivedFromLemmata = lets (
  () => prop ('entries') >> map (([lemmaId, homonymNr, lemma]) =>
    [lemmaId, addHomonymNr (lemmaId, homonymNr, lemma)]
  ),
  (select) => select | memoize1,
  (_, selectm) => _ => createSelector (
    makeSelectLemmataReduced (),
    selectm,
  ),
)

export const makeSelectEntriesFromReducer = _ => createSelector (
  selectSlice,
  prop ('entries'),
)

const entriesReducer = xs => xs | reduce (
  (acc, { id: lemmaId, lemma, homonymNr, }) => [lemmaId, addHomonymNr (lemmaId, homonymNr, lemma)] | appendToM (acc),
  [],
)

// --- if we are exploring and loading more, it returns derived @todo
const _makeSelectEntries = (entriesFromReducer, entriesDerived) =>
  entriesFromReducer | fold (entriesReducer, entriesDerived)

// --- note that 'derived from lemmata' is not currently used, though it could be useful in the
// future.

export const makeSelectEntries = _ => createSelector (
  makeSelectEntriesFromReducer (),
  makeSelectEntriesDerivedFromLemmata (),
  _makeSelectEntries | memoize1,
)

/*
export const makeSelectEntriesHaveNonDerived = _ => createSelector (
  makeSelectEntriesFromReducer (),
  fold (T, false),
)
*/

export const makeSelectEntryIdMin = _ => createSelector (
  makeSelectEntries (),
  (entries) => entries
  | ifOk (
    (entries) => entries | ifEmptyList (
      () => die ('makeSelectEntryIdMin: empty list'),
      // --- lemmaId of first
      head >> head,
    ),
    () => null,
  ),
)

export const makeSelectEntryIdMax = _ => createSelector (
  makeSelectEntries (),
  (entries) => entries
  | ifOk (
    (entries) => entries | ifEmptyList (
      () => die ('makeSelectEntryIdMax: empty list'),
      // --- lemmaId of last
      last >> head,
    ),
    () => null,
  ),
)

export const makeSelectEntriesIdxShift = _ => createSelector (
  selectSlice,
  prop ('entriesIdxShift'),
)

export const makeSelectLemmaIdToEntry = lets (
  () => lemmaIdToEntry,
  (select) => select | memoize1,
  (_, selectm) => _ => createSelector (
    makeSelectLemmaIdToIdx (),
    makeSelectEntries (),
    // should be changed to makeSelectEntriesDerivedFromLemmata (),
    selectm,
  ),
)

export const makeSelectLemmaIdToEntryLemma = _ => createSelector (
  makeSelectLemmaIdToEntry (),
  // --- the tables might not have been loaded yet, hence whenOk.
  f => f >> whenOk (prop (1)),
)

export const makeSelectMaxLemmaId = _ => createSelector (
  selectSlice,
  prop ('maxLemmaId'),
)

export const makeSelectEntriesExploreMode = _ => createSelector (
  // selectSlice,
  // prop ('entriesExploreMode'),
  makeSelectEntriesFetchType (),
  (type) => type | whenOk (fetchIsReplace >> not),
)
