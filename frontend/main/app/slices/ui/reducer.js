import {
  pipe, compose, composeRight,
  ok, whenOk, dot,
  drop, assoc,
  tap, eq,
  condS, guard, otherwise, guardV,
  id,
  always,
  prop, path,
  merge,
} from 'stick-js/es'

import {
  SET_SIZE,
  FORCE_UPDATE_WINDOW_LIST,
  SIMPLE_QUERY_SET,
  ENTRY_SET_SELECTED_OFFSET_TOP,
} from '../../containers/App/constants'

import { logWith, } from 'alleycat-js/es/general'
import { reducer, } from '../../common'

const initialState = {
  fontSize: 'medium',
  renderLemmaListKey: 0,
  // --- the last query which was selected and caused a location change.
  // --- currently no way to clear it as that doesn't seem necessary.
  // --- { type = 'lemma' / 'pars', query = query }
  latestSimpleQuery: void 8,
  // --- we store this so we can maintain the scroll position of the entry list (mobile & desktop)
  // when new entries are loaded with the up arrow, by finding the new position of the selected
  // entry after the new chunk is fetched and adjusting scrollTop with the difference.
  // --- we could also have tracked for example the first entry, instead of the selected one, but
  // the selected one is better because we also need to deal with multiple clicks on the up arrow.
  entrySelectedOffsetTop: 45,
}

const reducerTable = {
  [SET_SIZE]: ({ data: size }) => merge ({
    fontSize: size,
  }),
  [FORCE_UPDATE_WINDOW_LIST]: () => state => state |  merge ({
    renderLemmaListKey: state.renderLemmaListKey + 1,
  }),
  [SIMPLE_QUERY_SET]: ({ data: { type, query, }}) => (
    { type, query, } | assoc ('latestSimpleQuery')
  ),
  [ENTRY_SET_SELECTED_OFFSET_TOP]: ({ data: entrySelectedOffsetTop, }) => merge ({
    entrySelectedOffsetTop,
  }),
}

export default reducer ('ui', initialState, reducerTable)
