import { createSelector, } from 'reselect'

import {
  pipe, compose, composeRight, add, whenOk,
  tap,
  prop,
  ifOk, always,
} from 'stick-js/es'

import { logWith, } from 'alleycat-js/es/general'
import { ifEquals, } from 'alleycat-js/es/predicate'

const selectSlice = (store, props) => store | prop ('ui')

export const makeSelectFontSize = _ => createSelector (
  selectSlice,
  prop ('fontSize'),
)

export const makeSelectRenderLemmaListKey = _ => createSelector (
  selectSlice,
  prop ('renderLemmaListKey'),
)

const makeSelectLatestSimpleQuery = _ => createSelector (
  selectSlice,
  prop ('latestSimpleQuery'),
)

export const makeSelectLatestSimpleLemmaQuery = _ => createSelector (
  makeSelectLatestSimpleQuery (),
  ({ type, query, } = {}) => type | ifEquals ('lemma') (
    () => query,
    () => null,
  )
)

export const makeSelectLatestSimpleParsQuery = _ => createSelector (
  makeSelectLatestSimpleQuery (),
  ({ type, query, } = {}) => type | ifEquals ('pars') (
    () => query,
    () => null,
  )
)

export const makeSelectEntrySelectedOffsetTop = _ => createSelector (
  selectSlice,
  prop ('entrySelectedOffsetTop'),
)
