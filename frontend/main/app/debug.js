import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, recurry,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, blush, always, T, F,
  prop, path,
  lets, letS,
  die, eq, ne,
  factory, factoryProps,
  bindProp,
} from 'stick-js'

import configure from 'alleycat-js/es/configure'
import { defaultToV, } from 'alleycat-js/es/general'

import { debugIt, } from './common'
import config from './config'

const spec = null
// const spec = 'browse'

const specs = {
  browse: [
    // 'lemma',
    'lemma-list-leaving-abridge',
    'lemma-list-render',
    'lemma-list-scroll',
    // 'lemma-list-browse-mode',
  ],
}

const toSet = xs => new Set (xs)

const tags = specs [spec] | defaultToV ([]) | toSet

const hasTag = tags | bindProp ('has')
const whenTag = hasTag | whenPredicate

export const debug = (tag, ...xs) => tag | whenTag (
  () => debugIt ('<' + tag + '>', '→', ...xs),
)
