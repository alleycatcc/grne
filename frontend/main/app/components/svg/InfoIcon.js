import {
  pipe, compose, composeRight,
  noop, prop,
} from 'stick-js/es'

import React, { PureComponent, } from 'react'
import styled from 'styled-components'

import { whenTrueV, } from 'alleycat-js/es/predicate'

const InfoIconS = styled.svg``

const InfoIcon = ({
  height=20,
  width=20,
  stroke='black',
  strokeWidth='1.0px',
  strokeWidthInner='3.0px',
  onClick,
}) => <InfoIconS
   width={width}
   height={height}
   viewBox="0 0 12.4 12.4"
   onClick={onClick}
   stroke={stroke}
   strokeWidthInner={strokeWidthInner}
 >
  <g
     id="layer1"
     transform="translate(-51.05,-94.1)">
    <g
       transform="matrix(0.26458333,0,0,0.26458333,-0.07249589,-0.1315462)"
       id="flowRoot815">
      <g
         transform="scale(0.855) translate(36, 62.5)">
        <path
           d="m 214.46944,367.80469 c 0,-0.58667 0.2,-1.08 0.6,-1.48 0.4,-0.4 0.88,-0.6 1.44,-0.6 0.56,0 1.02667,0.2 1.4,0.6 0.4,0.4 0.6,0.89333 0.6,1.48 0,0.56 -0.2,1.05333 -0.6,1.48 -0.37333,0.4 -0.84,0.6 -1.4,0.6 -0.56,0 -1.04,-0.2 -1.44,-0.6 -0.4,-0.42667 -0.6,-0.92 -0.6,-1.48 z"
           id="path1139"
           strokeWidth={strokeWidthInner}
           fill='none'
         />
        <path
           d="m 218.2014,388.92469 c 0.16,0.53333 0.54667,1 1.16,1.4 l 0.48,0.4 c 0.18667,0.13333 0.37334,0.29333 0.56,0.48 0,0.58667 -0.44,1.05333 -1.32,1.4 -0.64,0.18667 -1.29333,0.28 -1.96,0.28 -0.96,0.0533 -1.56,-0.92 -1.8,-2.92 -0.0533,-0.48 -0.0933,-1.06667 -0.12,-1.76 -0.0267,-0.72 0.13331,-1.5629 -0.04,-2.48 v -2 -0.36 c 0,-0.34667 0.0133,-0.89333 0.04,-1.64 0,-0.74667 -0.0133,-1.4 -0.04,-1.96 -0.0267,-0.56 -0.0667,-1.05333 -0.12,-1.48 -0.10666,-0.26667 -0.21333,-0.58667 -0.32,-0.96 -0.10666,-0.4 -0.22666,-0.85333 -0.36,-1.36 0,-0.26667 0.2,-0.48 0.6,-0.64 0.0533,0 0.36,-0.0667 0.92,-0.2 0.37334,-0.08 0.65334,-0.12 0.84,-0.12 0.29334,0 0.54667,0.10667 0.76,0.32 0.08,0.13333 0.14667,0.25333 0.2,0.36 0.08,0.10667 0.14667,0.22667 0.2,0.36 0.08,0.10667 0.14667,0.77333 0.2,2 0.0267,0.34667 0.1478,0.77226 0.04,1.24 v 1.64 6.44 c 0,0.77333 0.0267,1.29333 0.08,1.56 z"
           strokeWidth={strokeWidthInner}
           fill='none'
           id="path1141"
         />
      </g>
    </g>
    <circle
      stroke={stroke}
      strokeWidth={strokeWidth}
      fill='none'
      id="path1146"
      cx="57.234642"
      cy="100.30636"
      r="5.6696429" />
  </g>
</InfoIconS>

export default InfoIcon
