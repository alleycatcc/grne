import {
  pipe, compose, composeRight,
  lets, letS, sprintf1, sprintfN,
  tap, id,
  whenOk, eq, ifPredicate, whenPredicate,
  deconstruct, deconstructN,
} from 'stick-js/es'

import React, { PureComponent, } from 'react'
import styled from 'styled-components'

import { logWith, reverseM, } from 'alleycat-js/es/general'
import { ifEquals, } from 'alleycat-js/es/predicate'
import { createRef, } from 'alleycat-js/es/react-legacy'

const BurgerMenuS = styled.svg`
  margin: auto;
`

const durationStage1 = '0.2s'
const durationStage1MiddleBar = '0.1s'
const beginStage2 = '0.4s'

const animateRotate = (start, stop) => dir => lets (
  () => dir === 0 ? id : reverseM,
  (f) => [start, stop] | f,
  (_, [x, y]) => [
    x | sprintf1 ('%s 6 6'),
    y | sprintf1 ('%s 6 6'),
  ],
  (_, __, [from, to]) => <animateTransform
    attributeName="transform"
    type="rotate" from={from} to={to}
    begin={beginStage2} dur={durationStage1} repeatCount="1" fill="freeze"
  />,
)

const animateTranslate = (start, stop, dur) => dir => lets (
  () => dir === 0 ? id : reverseM,
  (f) => [start, stop] | f,
  (_, [[a, b], [c, d]]) => [
    [a, b] | sprintfN ('%s %s'),
    [c, d] | sprintfN ('%s %s'),
  ],
  (_, __, [from, to]) => <animateTransform
    attributeName="transform"
    type="translate" from={from} to={to}
    begin="0s" dur={dur} repeatCount="1" fill="freeze"
  />
)

const animateScale = (start, stop, dur) => dir => lets (
  () => dir === 0 ? id : reverseM,
  (f) => [start, stop] | f,
  (_, [[a, b], [c, d]]) => [
    [a, b] | sprintfN ('%s %s'),
    [c, d] | sprintfN ('%s %s'),
  ],
  (_, __, [from, to]) => <animateTransform
    attributeName="transform"
    type="scale" from={from} to={to}
    begin="0s" dur={dur} repeatCount="1" fill="freeze"
  />,
)

const a0 = animateRotate (0, 45)
const a1 = animateTranslate ([0, 0], [0, 5], durationStage1)
const a2 = animateTranslate ([0, 0], [6, 0], durationStage1MiddleBar)
const a3 = animateScale ([1, 1], [0, 1], durationStage1MiddleBar)
const a4 = animateRotate (0, -45)
const a5 = animateTranslate ([0, 0], [0, -5], durationStage1)

const isForwards = 0 | eq
const ifForwards = isForwards | ifPredicate
const whenForwards = isForwards | whenPredicate

const BurgerMenuInner = ({
  width, height, onClick, theRef,
  stroke, strokeWidth, fill,
  direction,
}) => <BurgerMenuS
   ref={theRef}
   width={width}
   height={height}
   viewBox="0 0 12 12"
   onClick={onClick}
 >
  <g
    stroke={stroke}
    strokeWidth={strokeWidth}
    strokeLinecap='round'
    strokeLinejoin='bevel'
    fill={fill}
  >
    {/*
    Undoing the rotation on the reverse transformation is not necessary, because the rotation value
    is getting overwritten somehow and getting set to the initial value.
    */}

    <g>
      {direction | whenForwards (a0)}
      <g>
        <path d="M 0,1 H 12" strokeWidth={strokeWidth} stroke={stroke}>
          {direction | whenOk (a1)}
        </path>
      </g>
    </g>
    <g>
      {direction | whenOk (a2)}
      <g>
        <path d="M 0,6 H 12" strokeWidth={strokeWidth} stroke={stroke}>
          {direction | whenOk (a3)}
        </path>
      </g>
    </g>
    <g>
      {direction | whenForwards (a4)}
      <g>
        <path d="M 0,11 H 12" strokeWidth={strokeWidth} stroke={stroke}>
          {direction | whenOk (a5)}
        </path>
      </g>
    </g>
  </g>
</BurgerMenuS>

class BurgerMenu extends PureComponent {
  theRefs = {
    svg: createRef ()
  }

  componentDidUpdate = () => this | deconstruct (
    ({ theRefs, }) => theRefs | deconstruct (
      ({ svg, }) => svg.current.setCurrentTime (0),
    )
  )

  render = () => this | deconstruct (
    ({ theRefs, props, }) => [theRefs, props] | deconstructN (
      (
        { svg },
        {
          ratio=47.5/38.2,
          height=50,
          // width=height*ratio,
          width=50,
          stroke='white',
          fill='white',
          strokeWidth='1.0px',
          onClick,
          toState=null,
        }
      ) => lets (
        () => toState === true ? 0 : toState === false ? 1 : null,
        (direction) => ({
          theRef: svg,
          width, height, onClick, stroke, strokeWidth, fill, direction,
        }),
        (_, passProps) => <BurgerMenuInner {...passProps}/>,
      ),
    ),
  )
}

export default BurgerMenu
