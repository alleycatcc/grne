import {
  pipe, compose, composeRight,
  lets, letS, sprintf1, sprintfN,
  tap, id,
  whenOk, eq, ifPredicate, whenPredicate,
  deconstruct, deconstructN,
} from 'stick-js/es'

import React, { PureComponent, } from 'react'
import styled from 'styled-components'

import { logWith, reverseM, } from 'alleycat-js/es/general'
import { ifEquals, } from 'alleycat-js/es/predicate'
import { createRef, } from 'alleycat-js/es/react-legacy'

const NotEqualIconS = styled.svg`
  margin: auto;
`

const NotEqualIcon = ({
  width, height,
  theRef=null,
  stroke='black', strokeWidth='0.5',
  fill, direction,
}) => <NotEqualIconS
   ref={theRef}
   width={width}
   height={height}
   viewBox="0 0 16 16"
 >
  <g>
    <path
      stroke={stroke}
      strokeWidth={strokeWidth}
      transform='translate(16, 0) scale(-1,1)'
      d="M 2.7285156 2.0214844 L 2.0214844 2.7285156 L 6.2382812 6.9453125 C 6.0562048 6.9703046
        5.8803547 7.0049071 5.6816406 7.0058594 L 5.6640625 7.0058594 C 4.8710625 7.0058594 4.1418906
        6.7237344 2.7128906 6.0527344 L 2.2871094 6.9589844 C 3.8281094 7.6819844 4.6850625 8.0058594
        5.6640625 8.0058594 L 5.6855469 8.0058594 C 6.2126496 8.0036947 6.6719886 7.9217455 7.1015625
        7.8085938 L 8.5820312 9.2890625 C 8.3234926 9.3669758 8.0697511 9.4487657 7.8261719 9.5351562 C
        7.1411719 9.7781563 6.4936406 10.009672 5.6816406 10.013672 C 4.8496406 10.006672 4.1508906
        9.7355469 2.7128906 9.0605469 L 2.2871094 9.9648438 C 3.8701094 10.707844 4.6665 10.987719
        5.6875 11.011719 C 6.6685 11.007719 7.4261563 10.737562 8.1601562 10.476562 C 8.5709092
        10.330888 8.979915 10.20342 9.4082031 10.115234 L 13.271484 13.978516 L 13.978516 13.271484 L
        2.7285156 2.0214844 z M 10.316406 6 C 9.5954062 5.991 8.9811563 6.143125 8.4101562 6.328125 L
        9.2304688 7.1484375 C 9.5724688 7.0694375 9.9285 7.014 10.3125 7 C 11.1025 7.004 11.830578
        7.3202188 13.267578 8.0742188 L 13.732422 7.1875 C 12.172422 6.3705 11.310406 6.005 10.316406 6
        z M 11.195312 9.1132812 L 13.050781 10.96875 C 13.123781 11.00675 13.190578 11.040078 13.267578
        11.080078 L 13.732422 10.193359 C 12.639422 9.6203594 11.889313 9.2742812 11.195312 9.1132812 z"
    />
  </g>
</NotEqualIconS>

export default NotEqualIcon
