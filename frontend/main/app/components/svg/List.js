import {
  pipe, compose, composeRight,
  noop, prop, lets,
} from 'stick-js/es'

import React, { PureComponent, } from 'react'
import styled from 'styled-components'

import { whenTrueV, } from 'alleycat-js/es/predicate'

const ListIconS = styled.svg`
  ${prop ('disabled') >> whenTrueV ('opacity: 0.3;')}
`

const ListIcon = ({
  ratio=1,
  height=30,
  width=height*ratio,
  strokeWidth=1,
  disabled=false,
  selected=false,
  withFrame=false,
  onClick,
}) => lets (
  () => selected ? ['white', '#44A'] : ['black', 'none'],
  ([stroke, fill]) => <ListIconS
    // shapeRendering='crispEdges'
    width={width}
    height={height}
    stroke={stroke}
    strokeWidth={2 + 'px'}
    disabled={disabled}
    viewBox="-1 -1 32 32"
    onClick={disabled ? noop : onClick}
   >
    {(withFrame || selected) &&
      <rect
        width="30"
        height="30"
        fill={fill}
        stroke={stroke}
        strokeWidth={strokeWidth + 'px'}
        x="0"
        y="0"
        rx="6"
        ry="6"
      />
    }
     <path
       strokeLinecap='round'
       d="M 5,5 H 23"
    strokeWidth={2 + 'px'}
     />
     <path
       strokeLinecap='round'
       d="M 5,10 H 18"
    strokeWidth={2 + 'px'}
     />
     <path
       strokeLinecap='round'
       d="M 5,15 H 23"
    strokeWidth={2 + 'px'}
     />
     <path
       strokeLinecap='round'
       d="M 5,20 H 23"
    strokeWidth={2 + 'px'}
     />
     <path
       strokeLinecap='round'
       d="M 5,25 H 19"
    strokeWidth={2 + 'px'}
     />
   </ListIconS>,
)

export default ListIcon
