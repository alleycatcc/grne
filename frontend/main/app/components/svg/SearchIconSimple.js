import {
  pipe, compose, composeRight,
  noop, prop,
  concat,
  multiply, lets,
} from 'stick-js/es'

import React, { PureComponent, } from 'react'
import styled from 'styled-components'

import { whenTrueV, } from 'alleycat-js/es/predicate'

const SearchIconSimpleS = styled.svg`
  ${prop ('disabled') >> whenTrueV ('opacity: 0.3;')}
`

const SearchIconSimple = ({
  ratio=1,
  height=50,
  width=height*ratio,
  stroke='black',
  strokeWidth=3,
  disabled=false,
  selected=false,
  withFrame=false,
  onClick,
}) => lets (
  () => selected ? ['white', '#44A'] : [stroke, 'none'],
  ([strokeInner, fill]) => <SearchIconSimpleS
     width={width}
     height={height}
     stroke={stroke}
     strokeWidth={strokeWidth | String | concat ('px')}
     disabled={disabled}
     viewBox='-16 -16 86 86'
     onClick={disabled ? noop : onClick}
   >

    {(withFrame || selected) &&
     <rect
       width='86'
       height='86'
       fill={fill}
       stroke={strokeInner}
       strokeWidth={strokeWidth | multiply (1) | String | concat ('px')}
       x='-16'
       y='-16'
       rx='15'
       ry='15'
     />
    }

    <path
      fill={fill}
      stroke={strokeInner}
      d="M51.704,51.273L36.845,35.82c3.79-3.801,6.138-9.041,6.138-14.82c0-11.58-9.42-21-21-21s-21,9.42-21,21s9.42,21,21,21
      c5.083,0,9.748-1.817,13.384-4.832l14.895,15.491c0.196,0.205,0.458,0.307,0.721,0.307c0.25,0,0.499-0.093,0.693-0.279
      C52.074,52.304,52.086,51.671,51.704,51.273z
      M21.983,40c-10.477,0-19-8.523-19-19s8.523-19,19-19s19,8.523,19,19
      S32.459,40,21.983,40z"
    />

  </SearchIconSimpleS>,
)

export default SearchIconSimple
