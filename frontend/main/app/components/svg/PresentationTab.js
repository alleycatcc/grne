import {
  pipe, compose, composeRight,
  noop, prop,
  sprintf1, lets,
} from 'stick-js/es'

import React, { PureComponent, } from 'react'
import styled from 'styled-components'

import { whenTrueV, } from 'alleycat-js/es/predicate'

const PresentationTabS = styled.svg`
`

// const transform = 'scale(1.0, %s) translate(-7.3230805,-84.0)'
const transform = 'scale(1.0, %s) translate(-7.1905805,-83.598582)'

  /*
const path = `
  m 7.3230805,96.708499
  c 0,-7.9375 3.2236145,-12.977417 9.0826405,-12.977417
  h 54.554337
  c 5.60018,0 9.123439,5.039917 9.123439,12.977417"
`
*/

const connectPath = `
  M 80.083497,100.67725
  H 7.3230805
`

  /*
const getPath = closePath => lets (
  () => closePath ? 'z' : '',
  (z) => path + z,
)
*/

const closePath = false

const PresentationTab = ({
  ratio=72.86/15.49,
  height=50,
  width=Math.floor(height*ratio),
  fill='white',
  stroke='black',
  strokeUnselected='#AAA',
  strokeWidth=0.8,
  selected=false,
  onClick,
  verticalScale=1.0,
  // closePath=false,
}) => <PresentationTabS
   width={width}
   height={height}
   selected={selected}
   stroke={selected ? stroke : strokeUnselected}
   strokeWidth={strokeWidth}
   fill={fill}
   viewBox="0 0 73.025413 17.21117"
   onClick={selected ? onClick : noop}
 >
  <g
    stroke={selected ? stroke : strokeUnselected}
    strokeWidth={strokeWidth}
    fill={fill}
    transform={verticalScale | sprintf1 (transform)}
  >
    <path d="m 7.3230805,100.67725 c 0,-13.229167 3.2236145,-16.417001 9.0826405,-16.417001 h 54.554337 c 5.60018,0 9.123439,3.187834 9.123439,16.417001"/>

    <g transform='translate(0, -2.5)'>
      {selected || <path
        stroke='black'
        strokeWidth={1}
        d={connectPath}
      />}
    </g>
  </g>
</PresentationTabS>

export default PresentationTab
