/*

*/

import {
  pipe, compose, composeRight,
  lets, letS, ifTrue, bindProp, concat, ifFalse,
  whenOk, prop, path, plus, id,
  list, tail, tap, ifOk, mergeM, concatTo,
  dot2, side2, map, ok, appendTo,
  join, noop, ifNotOk, condS, eq, guard, otherwise,
  cond, split, always, whenNotOk,
  side1,
  assoc,
  not, T, F,
  allAgainst, guardV, lt, gt, subtractFrom, whenNo,
  ifPredicate,
  whenPredicate,
  sprintf1,
  deconstructN,
} from 'stick-js/es'

import React, { PureComponent, Component, Fragment, memo, } from 'react'

import styled from 'styled-components'

import { cata, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { defaultToV, iwarn, mapX, logWith, ierror, notBelow, setTimeoutOn, toString, pluck, pluckN, repluck, repluckN, repluckAll, pluckAll, } from 'alleycat-js/es/general'
import { ifTrueV, ifFalseV, whenTrueV, allV, } from 'alleycat-js/es/predicate'
import { createRef, shouldComponentUpdate, shouldUpdateShallow, compMeth, setState, whyUpdateMeth, whyUpdate, } from 'alleycat-js/es/react-legacy'
import { whyYouRerender, } from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import { Lemma, } from '../../components/Lemma'
import ParsResultsTable from '../../containers/ParsResultsTable'

import { mediaPhone, mediaTablet, mediaDesktop, debugDev, debugDevWith, disableSingle, } from '../../common'
import config from '../../config'

import {
  browseModeLemmataIsAbridge,
  browseModeLemmataIsSingle,

  ScrollListenerIdle,
  ScrollListenerBlocked,
  ScrollListenerActive,
  scrollListenerIsBlocked,
} from '../../types'

const configTop = config | configure.init

const debugRender = 'debug.rerender' | configTop.get

const doDebugScrollListener = false
const debugScrollListener = s => tap (
  doDebugScrollListener ? debugDevWith (s) : noop,
)

// --- we use scrollListener to listen for 'long scrolls', i.e. the user scrolls for a certain
// amount of time, before triggering feed-style events (see comments in Browse).
// --- this is being updated on every single scroll event, so it's definitely a good place to look
// if there are performance issues.
const scrollListener = {
  // --- how long you have to keep scrolling until the window opens.
  blockDuration: 300,
  // --- how long the window stays open after the last scroll event.
  activeWindowDuration: 500,
  listenerState: ScrollListenerIdle,
  _scheduleToIdle () {
    return this.activeWindowDuration | setTimeoutOn (
      () => this.listenerState = ScrollListenerIdle | debugScrollListener ('-> idle'),
    )
  },
  _scheduleToActive () {
    return this.blockDuration | setTimeoutOn (
      () => this.listenerState = ScrollListenerActive (
        this._scheduleToIdle (),
      ) | debugScrollListener ('-> active'),
    )
  },
  update () {
    this.listenerState = this.listenerState | cata ({
      ScrollListenerIdle: () => ScrollListenerBlocked (this._scheduleToActive ())
        | debugScrollListener ('-> blocked'),
      ScrollListenerBlocked: ScrollListenerBlocked >> debugScrollListener ('= blocked'),
      ScrollListenerActive: (tid) => {
        tid | clearTimeout
        return ScrollListenerActive (this._scheduleToIdle ()) | debugScrollListener ('= active')
      }
    })
  },
  isBlocked () {
    return this.listenerState | scrollListenerIsBlocked
  }
}

const LemmaWrapperS = styled.div`
  position: relative;
`

// @todo
const LINE_HEIGHT = 33

const ifNotAbridge = (browseModeLemmataIsAbridge >> not) | ifPredicate
const whenAbridge = browseModeLemmataIsAbridge | whenPredicate
const getAbridgeInfo = whenAbridge (prop ('singleMode'))

const getStyleForBrowseMode = getAbridgeInfo >> ifNotOk (
  {} | always,
  ({ height, offset, }) => ({
    height: height + 4 * LINE_HEIGHT + 'px',
    overflowY: 'hidden',
    offsetTop: offset - 2 * LINE_HEIGHT,
  }),
)

class LemmaListInner extends Component {
  // --- try to keep LemmaListInner stateless -- makes it easier to reason about.
  // --- update: that was mostly true when we were using a virtual list.
  state = {
  }

  theRefs = {
    wrapper: createRef (),
  }

  innerOffsetTop = void 8

  shouldUpdate = shouldUpdateShallow ([], ['scrollTop', 'onClick', 'onLemmaClicked'])
    // >> tap (debugDevWith ('updating because:'))
    >> Boolean

  // scrollListener = scrollListener

  // --- only propagates to LemmaList.onLemmaClicked if we're not going to explore mode.
  onLemmaClicked = (lemmaId, lemmaIdx) => () => this | compMeth ((
    { onLemmaClicked, isMobile, selectedLemmaIdx },
  ) => onLemmaClicked (lemmaId, lemmaIdx))

  rowRenderer = (isSingle) => (lemma, idx) => this | compMeth ((
    {
      history, isMobile,
      lemmata, selectedLemmaIdx, reductieModeSchool,
      annotationClicked, annotationInfo,
      citGClicked, fontSize,
      browseMode,
    },
    _,
    {
      onLemmaClicked,
    },
  ) => {
    const [lemmaId, homonymNr, html, htmlSchool, t1Gecontroleerd] = lemma
    const isSelected = idx === selectedLemmaIdx
    // debugDev ('LemmaListInner -> rowRenderer, isSelected', isSelected, 'idx', idx, 'selectedLemmaIdx', selectedLemmaIdx)
    const disableAnnotationClick = allV (
      isMobile,
      (!isSingle && !disableSingle ()) || !isSelected,
    )

    const row = <Lemma
      history={history}
      isMobile={isMobile}
      isSelectable={true}
      isSelected={isSelected}
      browseMode={browseMode}
      homonymNr={homonymNr}
      onClick={onLemmaClicked (lemmaId, idx)}
      html={html}
      htmlSchool={htmlSchool}
      t1Gecontroleerd={t1Gecontroleerd}
      annotationClicked={annotationClicked (lemmaId, idx)}
      reductieModeSchool={reductieModeSchool}
      annotationInfo={annotationInfo [idx]}
      postProcess={true}
      citGClicked={citGClicked}
      fontSize={fontSize}
      disableAnnotationClick={disableAnnotationClick}
    />

    return <LemmaWrapperS key={idx}>
      {row}
    </LemmaWrapperS>
  })

  shouldComponentUpdate = this | shouldComponentUpdate (this.shouldUpdate)

  componentDidUpdate = (prevProps, prevState) => this | compMeth ((
    { browseMode, lemmata, onInnerOffsetTop },
    {},
    {
      updateBrowseMode,
      innerOffsetTop,
      theRefs: { wrapper: { current: ref }}
    },
  ) => {
    this | whyUpdate ('LemmaListInner', debugRender) (prevProps, prevState)
    onInnerOffsetTop (innerOffsetTop)
  })

  onInnerOffsetTop = (val) => this.innerOffsetTop = val

  render = () => this | compMeth ((
    {
      theClassName, reductieModeSchool, reductieModeBeknopt,
      onLemmaClicked, annotationClicked, lemmata,
      windowListInnerScroll,
      browseMode,
    },
    _,
    {
      rowRenderer,
      onInnerOffsetTop,
      theRefs: { wrapper: ref },
    },
  ) => lets (
    () => browseMode | getStyleForBrowseMode,
    () => browseMode | browseModeLemmataIsSingle,
    ({ height, overflowY, offsetTop, }, isSingle) => {
      onInnerOffsetTop (offsetTop)
      return <WrapperS
        className={theClassName}
        ref={ref}
        height={height}
        overflowY={overflowY}
        offsetTop={offsetTop}
      >
        <div className='x__inner-wrapper'>
          {lemmata | mapX (rowRenderer (isSingle))}
        </div>
      </WrapperS>
    }),
  )
}

// --- the cropping effect for abridge mode can be done using scrollTop or offsetTop.
// --- we choose offsetTop because that can be done using only css, while scrollTop needs js.

const WrapperS = styled.div`
  ${prop ('height') >> whenOk (sprintf1 ('height: %s;'))}
  ${prop ('overflowY') >> whenOk (sprintf1 ('overflow-y: %s;'))}
  .x__inner-wrapper {
    ${prop ('offsetTop') >> whenOk (sprintf1 ('top: -%spx;'))}
    position: relative;
  }
`

export default LemmaListInner
