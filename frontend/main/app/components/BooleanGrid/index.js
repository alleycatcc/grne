import {
  pipe, compose, composeRight,
  dot, whenOk, map,
  tap, letS, prop, defaultTo,
  always,
  take, tail,
  condS, guard, otherwise, guardV,
  cond,
  eq,
  lets,
  reduce, id, ifYes, join,
  ne,
  repeatV, concat,
  concatM, not, ifPredicate,
  sprintfN,
} from 'stick-js/es'

import React from 'react'
import styled from 'styled-components'

import configure from 'alleycat-js/es/configure'
import { mapX, logWith, toString, partition, } from 'alleycat-js/es/general'
import { ierrorError, errorError, } from 'alleycat-js/es/react-s-alert'
import { withDisplayName, } from 'alleycat-js/es/react'

import NotEqualIcon from '../../components/svg/NotEqualIcon'

import config from '../../config'
import { makeTree, And, Or, toJS, cache, } from '../../tree-function'

const isEmptyArray = xs => xs.length === 0
const ifNotEmptyArray = (isEmptyArray >> not) | ifPredicate
const ifEmptyArray = isEmptyArray | ifPredicate

const max = a => b => Math.max (a, b)

const colorAnd = '#687275'
const colorOr = '#84590b'
const colorNot = '#a51e1e'
const colorNot2 = '#850e0e'

const BooleanGridS = styled.div`
  position: relative;
  height: 100%;
  width: 100%;
`

const LineS = styled.div`
`

const LineSegmentVerticalS = styled.div`
  background-color: ${prop ('background-color')};
  position: absolute;
  width: 3px;
  top: ${prop ('top')}%;
  height: ${prop ('height')}%;
  left: ${prop ('left')}%;
`

const NotS = styled.div`
  .not-wrapper {
    display: inline-block;
    position: absolute;
  }
`

const Not = ({ maxY, y, showNotEqualLine=false, showNotEqualIcon, }) => lets (
  _ => 8,
  _ => y / maxY * 100,
  (adjPx, topPerc) => [topPerc, adjPx] | sprintfN ('calc(%s%% - %spx)'),
  (_, _1, top) => <NotS>
    {showNotEqualLine &&
      <LineHorizontal
        maxY={maxY}
        p1={[y, 2.0]}
        p2={[y, 1.0]}
        color={colorNot}
      />
    }
    {showNotEqualIcon && lets (
      () => showNotEqualLine ? '62%' : '50%',
      (left) =>
        <div style={{ top, left, }} className='not-wrapper'>
          <NotEqualIcon width={20} height={20} stroke={colorNot2}/>
        </div>
    )}
  </NotS>,
)

const Line = ({ maxY, line: [p1, p2] }) => lets (
  _ => lets (
    _ => p1 [0],
    _ => p2 [0],
    (p1y, p2y) => p1y === p2y ? LineHorizontal : LineVertical,
  ),

  (Elem) => <Elem
    maxY={maxY}
    p1={p1}
    p2={p2}
  />
)

const LineSegmentHorizontalS = styled.div`
  background-color: ${prop ('background-color')};
  position: absolute;
  height: 3px;
  top: ${prop ('top')}%;
  left: ${prop ('left')}%;
  width: ${prop ('width')}%;
`

const LineSegmentHorizontal = (props) => <LineSegmentHorizontalS {...props} />
const LineSegmentVertical = (props) => <LineSegmentVerticalS {...props}/>

const LineHorizontal = ({ maxY, p1, p2, color: colorArg, }) => {
  const [p1y, p1x] = p1
  const [p2y, p2x] = p2
  const xLeft = Math.max (p1x, p2x)
  const xRight = Math.min (p1x, p2x)
  const left = 100 - xLeft / 2 * 100
  const width = (xLeft - xRight) * 50
  const top = p1y / maxY * 100
  const color = colorArg || (xLeft | condS ([
    2 | eq | guardV (colorAnd),
    otherwise | guardV (colorOr),
  ]))
  return <LineS>
    <LineSegmentHorizontal
      background-color={color}
      top={top}
      left={left}
      width={width}
    />
  </LineS>
}

const LineVertical = ({ maxY, p1, p2, }) => {
  const [p1y, p1x] = p1
  const [p2y, p2x] = p2
  const yTop = Math.min (p1y, p2y)
  const yBottom = Math.max (p1y, p2y)
  const top = yTop / maxY * 100
  const bottom = yBottom / maxY * 100
  const left = 100 - 50 * p1x
  const color = p1x | condS ([
    2 | eq | guardV (colorAnd),
    otherwise | guardV (colorOr),
  ])
  return <LineS>
    <LineSegmentVertical
      background-color={color}
      top={top}
      height={bottom - top}
      left={left}
    />
  </LineS>
}

const getMaxY = (() => {
  const reducer = (acc, [p1, p2]) => p1 [0] | max (p2 [0]) | max (acc)
  return reduce (reducer) (0)
}) ()

const skew = ({ hor=1, maxX=2, }) => map (map (
  ([y, x]) => [
    y,
    x | condS ([
      -1 | eq | guard (id),
      0 | eq | guard (id),
      otherwise | guard (x => maxX - ((maxX - x) / hor)),
    ])
  ],
))

const toBool = ifYes (_ => And, _ => Or)
const make = (xs) => {
  if (xs.length === 0) return []
  // --- @todo actually send nots through the calculator.
  const [ys, zs] = partition (2 | ne, xs)
  const cached = cache (ys | join (','))
  const tree = cached
    ? tree
    : ys | map (toBool) | makeTree | toJS
  const tailY = 2 * ys.length
  const tailStuk = zs | mapX ((_, idx) => lets (
    _ => tailY + (idx + 1) * 2,
    y => [[y, -1], [y, -1]],
  ))
  return tree | concatM (tailStuk)
}

export default withDisplayName ('BooleanGrid') (
  ({ spec, horizontalSkew=1, showNotEqualLine=true, showNotEqualIcon=true, }) =>
    spec | make | letS ([
      (specMade) => specMade | getMaxY,
      (specMade) => specMade | skew ({ hor: horizontalSkew, }),
      // eslint-disable-next-line react/display-name
      (_, maxY, skewedSpec) => <BooleanGridS>
          {skewedSpec | mapX ((line, idx) => cond (
          (_ => line[0][1] === -1) | guard (_ => <Not
            key={idx}
            maxY={maxY}
            y={line[0][0]}
            showNotEqualLine={showNotEqualLine}
            showNotEqualIcon={showNotEqualIcon}
          />),
          otherwise | guard (_ => <Line
            key={idx}
            maxY={maxY}
            line={line}
          />),
        ))}
      </BooleanGridS>
    ]),
)
