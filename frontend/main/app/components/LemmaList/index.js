/*
 @todo Much of this doesn't apply at all any more.

 Notes

 Changing the height of LemmaListInner is not possible -- react-virtualized needs it to be constant.

 LemmaList handles scrolling, e.g, scrolling to a lemma when it's selected. To do this properly, it needs to know browseMode. So, browseMode is figured out here and passed as a prop to LemmaListInner.

 LemmaListInner, once it knows the heights, triggers an update on LemmaList. So, DidUpdate is also fired on mount.

 updateScrollSelect is called on DidReallyUpdate.

 When coming in for the first time, mobile is always fixed or abridge.

 updateScrollSelect should be 'intelligent' when the location is changed using the left pane or the kruimelpad, or when the reductie/font buttons are used.

 updateScrollSelect should be 'simple' when we change the location ourselves by clicking in the right pane. 'simple' means just scroll into view. In most cases it won't do anything, but if the lemma is partially off-screen it will reveal it.

 Why not always intelligent? because in many cases the lemma should stay where it is, and only adjust slightly if part of it needs to be revealed, while moving it to the middle is not what we want.

 Mount then update. During the render of mount, the heights aren't there, so browse mode can't be determined; during the second render, they are. When fetching new lemmata, we are unmounted and mounted, so this works. When moving around within the lemmata we already have, the view is the same, so it still works.

*/

import {
  pipe, compose, composeRight,
  lets, letS, ifTrue, bindProp, concat, ifFalse,
  whenOk, prop, path, plus, id,
  list, tail, tap, ifOk, mergeM, concatTo,
  dot2, side2, map, ok, appendTo,
  join, noop, ifNotOk, condS, eq, guard, otherwise,
  cond, split, always, whenNotOk,
  side1,
  assoc,
  not, T, F,
  allAgainst, guardV, lt, gt, subtractFrom,
  ifPredicate, notOk, rangeTo,
  sprintfN,
  deconstruct,
  ifBind, whenBind, bindTryPropTo, bindTryProp,
  dot1,
} from 'stick-js/es'

import React, { PureComponent, Component, Fragment, memo, } from 'react'

import { Link as RouterLink, } from 'react-router-dom'
import styled from 'styled-components'

import { Nothing, Just, cata, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { querySelector, scrollIntoView, } from 'alleycat-js/es/dom'
import { defaultToV, iwarn, mapX, logWith, ierror, notBelow, setTimeoutOn, toString, pluck, pluckN, repluck, repluckN, repluckAll, pluckAll, } from 'alleycat-js/es/general'
import { ifTrueV, ifFalseV, whenTrueV, isEmptyList, isNotEmptyObject, allV, } from 'alleycat-js/es/predicate'
import { createRef, shouldUpdateShallow, compMeth, setState, whyUpdate, } from 'alleycat-js/es/react-legacy'
import { mediaQuery, } from 'alleycat-js/es/styled'

import LemmaListInner from '../../components/LemmaListInner'
import ParsResultsTable from '../../containers/ParsResultsTable'

import {
  browseToId, debugDev, debugDevWith, mediaPhone, mediaTablet, mediaDesktop,
  shouldDisableMomentumScroll,
  disableSingle,
} from '../../common'
import config from '../../config'

import {
  BrowseModeLemmataNormal,
  BrowseModeLemmataExplore,
  BrowseModeLemmataSingle,

  BrowseModeLemmataSingleModeInit,
  BrowseModeLemmataSingleModeFixed,
  BrowseModeLemmataSingleModeAbridge,

  browseModeLemmataIsExploring,
  browseModeLemmataIsSingle,
  browseModeLemmataIsAbridge,
  browseModeLemmataIsNormal,
  browseModeLemmataIsFixed,
  browseModeLemmataToggleExploringWithCondition,

  ScrollListenerIdle,
  ScrollListenerBlocked,
  ScrollListenerActive,
  scrollListenerIsBlocked,
} from '../../types'

import { debug, } from '../../debug'

const configTop = config | configure.init

const debugRender = 'debug.rerender' | configTop.get

const mapGet = 'get' | dot1

// --- polyfill for browsers which don't support Element.scrollTo.
// --- options must contain only the property 'top'.
const scrollTo = (options) => (el) => el | bindTryProp ('scrollTo') | ifOk (
  f => f (options),
  () => el.scrollTop = options.top,
)

const ParsWrapperS = styled.div`
  ${mediaQuery (
    // mediaPhone ('font-size: 20px'),
    // mediaTablet ('font-size: inherit'),
  )}
  ${prop ('show') >> ifTrueV (
    'z-index: 1',
    'z-index: 0',
  )};
  height: 100%;
  top: 0;
  width: 100%;
  position: absolute;
  top: 0px;
  margin: auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  > * {
    flex: 0 0 auto;
    margin: auto;
  }
`

const scrollLemmaIntoView = (intelligent, wrapperRef, lemmaHeight, idx) => {
  const [align, getRef] = cond (
    [() => intelligent | not, () => ['nearest', getLemmaRef]],
    [T, () => lets (
      () => [wrapperRef.clientHeight, lemmaHeight],
      ([ph, ch]) => ch > ph ?
        ['start', getHoofdwRef] :
        ['center', getLemmaRef],
    )],
  )
  scrollIntoViewWithAlignment (wrapperRef, idx, align, getRef)
}

// --- @todo ugly way to use selector
const getLemmaChildren = (wrapperRef) =>
  wrapperRef.querySelector ('.x__contents .x__inner-wrapper').children

const getLemmaRef = (wrapperRef, idx) => getLemmaChildren (wrapperRef) [idx]

const getHoofdwRef = getLemmaRef >> querySelector ('.hoofdW')

const scrollIntoViewWithAlignment = (wrapperRef, idx, alignment, getRef=getLemmaRef) => lets (
  () => getRef (wrapperRef, idx),
  (elem) => elem | whenOk (scrollIntoView ({
    block: alignment,
  }))
)

const scrollAdjust = (wrapperRef, idx) => scrollIntoViewWithAlignment (wrapperRef, idx, 'start')

; `
selectedLemmaIdx: the index of the child corresponding the lemma id from the route -- first, we need to make sure it's visible.
curEntryIdx: the entry (left) which is scrolled at the top and which we might need to scroll to match.
`

class LemmaList extends Component {
  static getDerivedStateFromProps (props, state) {
    const gotNewProp = props.selectedLemmaIdx !== state.deriveFromPropsKey
    debugDev ('LemmaList: getDerivedStateFromProps: props.selectedLemmaIdx', props.selectedLemmaIdx, 'gotNewProp', gotNewProp)
    return gotNewProp | ifTrue (
      () => ({
        selectedLemmaIdx: props.selectedLemmaIdx,
        deriveFromPropsKey: props.selectedLemmaIdx,
      }),
      () => null,
    )
  }

  state = {
    // --- we only need to show one popup at once, but more can be shown if desired.
    annotationInfo: {},
    // --- @todo this might not be necessary now that we are using the history API.
    // --- when a lemma in the list is selected we improve performance by handling it with
    // setState; then we browse to a new link with a delay and the same lemmaId will trickle
    // down again as a prop.
    selectedLemmaIdx: void 8,
    // --- we keep props and state in sync using selectedLemmaIdx as a key.
    deriveFromPropsKey: -1,
  }

  browseModeSingleInfo = [false, false]

  innerScrollTop = void 8
  innerOffsetTop = void 8

  // ------ these are first indexed by 'lemmata', then by 'lemmaIdx'.
  // --- the last lemmaIdx which was abridged before leaving abridged mode.
  leftAbridge = new Map
  // --- a table of which lemmata (by idx) require abridged mode.
  // --- they're added as they're encountered.
  abridged = new Map

  // --- reset the scrolledIntoView flag if we are exploring.
  resetScrollSelect = () => this | compMeth (
    ({ browseMode, }) => {
      debug ('lemma-list-scroll', 'resetScrollSelect')
      if (browseMode | browseModeLemmataIsExploring)
        this.scrolledIntoView = null
    },
  )

  onChangeBrowseMode = (browseMode) => this | compMeth (
    ({ onChangeBrowseMode, }) => {
      browseMode | onChangeBrowseMode
      // if (browseMode | browseModeLemmataIsExploring) this.scrolledIntoView = null
    }
  )

  shouldUpdate = shouldUpdateShallow (['curEntryIdx'], [])

  theRefs = {
    wrapper: createRef (),
  }

  // --- [curIdx, renderKey]
  // --- used to remember when 'we' changed the location (by clicking on a word, as opposed to the
  // Left pane, kruimelpad etc.)
  // --- @todo do we still need renderKey?
  locationChanged = [void 8, void 8]

  scrolledIntoView = void 8

  onLemmaListClicked = () => this | compMeth ((
    { onLemmaListClicked }, _, { closeAbbrPopup },
  ) => {
    closeAbbrPopup ()
    onLemmaListClicked ()
  })

  // setUitgelicht = idx => this | setState ({ idxUitgelicht: idx, })
  // unsetUitgelicht = () => this.setUitgelicht (-1)
  closeAbbrPopup = () => this | setState ({ annotationInfo: {}, })

  /* When the inner list renders it will send us its offsetTop.
   * When it's in abridge mode the offsetTop is the distance from the top of the list to the top of
   * the cropped view.
   * Otherwise, it's null.
   * If we receive null after having received a value, it means we are leaving abridge mode, and we
   * should set the scrollTop of
   * xxx
   * to the previou value of offsetTop to preserve the position and avoid jumps.
   */
  onInnerOffsetTop = (val) => this | compMeth ((
    {}, _,
    {
      // innerOffsetTop: prev,
      theRefs: { wrapper: { current: ref }},
    }
  ) => {
    if (val | ok) {
      const top = val
      debug ('lemma-list-leaving-abridge', 'setting innerOffsetTop to', top)
      this.innerOffsetTop = top
    }
  })

  // --- note that there is a prop 'onLemmaListClicked' which we receive from BrowseComponent, and our
  // own onLemmaClicked, which gets used in annotationClicked and also gets set as the onClick of
  // Lemma.
  // --- @todo not sure if the null check on lemmaIdx is still necessary.

  // --- @todo bug mobile: if you're in browse mode, and you use the search fields at the top to
  // navigate to a new lemma, the mode doesn't change to single. We need something like the logic
  // which happens during onLemmaClicked (which calls the toggle function) to also happen in that
  // case.

  onLemmaClicked = (renderKey, lemmaId, lemmaIdx=null, bubbling=false) => this | compMeth ((
    {
      history, browseMode, onLeavingAbridge, lemmata,
    },
    {
      selectedLemmaIdx: selectedLemmaIdxFromState,
    },
    {
      onChangeBrowseMode, abridged,
      annotationWasClicked,
      theRefs: { wrapper: { current: ref }},
    }) => {
      debug ('lemma-list-render', 'onLemmaClicked', 'annotationWasClicked', annotationWasClicked)
      const isSameLemma = lemmaIdx === selectedLemmaIdxFromState

      // --- mobile:
      //   if single & not the same lemma, toggle to explore.
      //   if explore, move to single
      //   keep everything else the same
      const newBrowseMode = lets (
        () => disableSingle () ? BrowseModeLemmataExplore : BrowseModeLemmataSingle (BrowseModeLemmataSingleModeInit),
        (newState) => browseMode | browseModeLemmataToggleExploringWithCondition (
          newState, isSameLemma,
        ),
      )

      // --- we need this because just testing browse mode is not good enough (it might be Init).
      const isAbridge = abridged | mapGet (lemmata) | ifOk (
        (o) => o [selectedLemmaIdxFromState],
        () => false,
      )
      debug ('lemma-list-browse-mode', 'onLemmaClicked', 'isAbridge', isAbridge)

      // --- there's only one way to leave abridged mode: mouse click, except on an annotation.
      if (!bubbling && isAbridge && !isSameLemma) {
        debug ('lemma-list-leaving-abridge', 'onLemmaClicked', 'leaving abridge', 'setting innerScrollTop to', ref.scrollTop)
        onLeavingAbridge (true)
        this.innerScrollTop = ref.scrollTop
      } else {
        debug ('lemma-list-leaving-abridge', 'onLemmaClicked', 'not leaving abridge')
        onLeavingAbridge (false)
      }

      newBrowseMode | onChangeBrowseMode
      const isExplore = newBrowseMode | browseModeLemmataIsExploring
      const allowed = disableSingle () || !isExplore

      if ((lemmaIdx | ok) && allowed) {
        this.locationChanged = [lemmaIdx, renderKey]
        this | setState ({
          selectedLemmaIdx: lemmaIdx,
        })
        0 | setTimeoutOn (() => browseToId (history, lemmaId))
      }
    }
  )

  annotationClicked = (lemmaId, lemmaIdx) => (type, expansion, inlineElement, lemmaSElement, event) => {
    const { state, } = this
    const { annotationInfo, } = state

    // --- we disable propagation of the annotation event, but we do want to call onLemmaClicked
    // manually, so that we can do a slight scroll if the annotation is partially off-screen.
    // @todo using a render key of 0.
    this.onLemmaClicked (0, lemmaId, lemmaIdx, true)
    this.annotationWasClicked = true

    // @todo
    const ADJUST_Y = 8
    const LEMMA_PADDING_LEFT = 30

    // --- note: don't use event.offsetX, which is experimental / browser-dependent.
    // click on the wrapped part (left) then offsetLeft is too high and the popup will show up too
    // far right.
    // --- event.offsetY isn't useful though.
    // --- note: don't use the `x` property of the bounding rect (problems on Edge) -- use `left`
    // instead.
    const lemmaWidth = lemmaSElement.offsetWidth
    const lemmaLeft = lemmaSElement.getBoundingClientRect ().left
    const xPx = event.clientX - lemmaLeft - LEMMA_PADDING_LEFT
    const x = xPx / lemmaWidth * 100
    const y = (inlineElement.offsetTop - ADJUST_Y) / lemmaSElement.offsetHeight * 100

    this | setState ({
      annotationInfo: {
        [lemmaIdx]: {
          type, expansion, x, y, lemmaWidth,
        },
      },
    })
  }

  updateScrollSympathetic (curCur, nxtCur) {
    const { props, theRefs, } = this
    const { lemmata, } = props

    if (nxtCur === curCur) return

    const { wrapper: { current: wrapperRef, }} = theRefs
    scrollAdjust (wrapperRef, nxtCur)
  }

  // --- returns an empty object if the child row doesn't exist yet.
  getRowInfo = (idx) => this | compMeth (
    (props, state, { theRefs: { wrapper: { current: wrapperRef, }}}) => lets (
      () => getLemmaRef (wrapperRef, idx),
      (ref) => ref | ifOk (
        () => ({
          height: ref.offsetHeight,
          offset: ref.offsetTop,
        }),
        () => ({} | tap (() => iwarn ('getRowInfo: returning empty object'))),
      ),
    )
  )

  updateScrollSelect (intelligent) {
    const { props, state, theRefs, getRowInfo, scrolledIntoView, browseModeSingleInfo, leftAbridge, } = this
    const { lemmata, isMobile, browseMode, leavingAbridge, } = props
    const { selectedLemmaIdx: curIdx, } = state
    const { wrapper: { current: wrapperRef, }} = theRefs

    const [isFixed, isAbridge] = browseModeSingleInfo
    const isExplore = browseMode | browseModeLemmataIsExploring

    if (allV (
      !isAbridge,
      leftAbridge | mapGet (lemmata) | eq (curIdx),
    )) {
      debug ('lemma-list-scroll', 'updateScrollSelect', 'this was the last lemma before we left abridge mode, not updating scroll')
      return
    }

    if (isMobile && (!isFixed && !isAbridge && !isExplore)) {
      debug ('lemma-list-scroll', 'updateScrollSelect', 'init + mobile -> returning')
      wrapperRef | scrollTo ({ top: 0, })
      return
    }
    const sameLemma = scrolledIntoView === curIdx
    debug ('lemma-list-scroll', 'updateScrollSelect', 'sameLemma', sameLemma, 'scrolledIntoView', scrolledIntoView, 'curIdx', curIdx)
    if (isAbridge && !sameLemma) {
      debug ('lemma-list-scroll', 'updateScrollSelect', 'abridged + not same lemma, scrolling to 0')
      wrapperRef | scrollTo ({ top: 0, })
      this.scrolledIntoView = curIdx
      return
    }

    debug ('lemma-list-leaving-abridge', 'leavingAbridge', leavingAbridge)
    if (leavingAbridge) {
      debug ('lemma-list-leaving-abridge', 'leaving abridge, not updating scroll')
      return
    }

    if (curIdx | notOk) {
      debug ('lemma-list-scroll', 'updateScrollSelect, skipping, curIdx is nil')
      return
    }

    // --- getRowInfo can return an empty object.
    const { height: lemmaHeight, } = curIdx | getRowInfo
    scrollLemmaIntoView (intelligent, wrapperRef, lemmaHeight, curIdx)
    this.scrolledIntoView = curIdx
  }

  componentDidMountOrUpdate = () => this | compMeth ((
    { lemmata, renderLemmaListKey, browseMode, leavingAbridge, onLeavingAbridge },
    { selectedLemmaIdx: curIdx },
    {
      locationChanged, resetScrollSelect, onChangeBrowseMode,
      innerOffsetTop,
      innerScrollTop,
      leftAbridge,
      theRefs: { wrapper: { current: wrapperRef }},
    },
  ) => {
    // --- this also happens on the first mount of mobile, so we can call onChangeBrowseMode here,
    // to make sure that the first render on mobile disables the buttons.
    //
    // --- keep in mind that we only know the browseMode after the render, because we have to do
    // measurements.

    resetScrollSelect ()

    const isExplore = browseMode | whenOk (browseModeLemmataIsExploring)
    const isSingle = browseMode | whenOk (browseModeLemmataIsSingle)

    debug (
      'lemma-list-browse-mode',
      [isExplore, isSingle] | sprintfN (
        'LemmaList -> mountOrUpdate, isExplore %s, isSingle %s',
      ),
    )

    // When coming in for the first time it's always fixed or abridge, so do scroll.
    // When clicking away from a selected lemma, we don't want the scroll.
    // @todo simplify, !isExplore && !isFixed && !isAbridge = isNormal dus.

    debug ('lemma-list-browse-mode', 'render', 'browseMode', browseMode.toString ())
    debug ('lemma-list-leaving-abridge', 'mountOrUpdate', 'leavingAbridge', leavingAbridge)

    const [x, y] = locationChanged
    const isSame = x === curIdx && y === renderLemmaListKey
    if (!lemmata.length) {
      debug ('lemma-list-render', 'render', 'lemmata.length is 0, returning')
      return
    }
    const doIntelligent = isSame | not
    // --- don't do scroll select if we're here because of an annotation click.
    // --- the annotation will still scroll itself into view if it's partially concealed.
    const annotationWasClicked = this.annotationWasClicked
    if (annotationWasClicked) {
      debug ('lemma-list-render', 'render', 'annotation was clicked, returning')
      this.annotationWasClicked = false
      return
    }
    if (leavingAbridge) {
      debug ('lemma-list-leaving-abridge', 'setting scrollTop to sum of', innerOffsetTop, innerScrollTop)
      wrapperRef.scrollTop = innerOffsetTop + innerScrollTop
      onLeavingAbridge (false)
      leftAbridge.clear ()
      leftAbridge.set (lemmata, curIdx)
      return
    }
    debug ('lemma-list-scroll', 'doing updateScrollSelect, doIntelligent =', doIntelligent)
    this.updateScrollSelect (doIntelligent)
  })

  componentDidMount = () => this | compMeth ((
    {}, { selectedLemmaIdx },
  ) => {
    // --- we need to force one update so that the refs are non-null and we can measure heights
    // during componentDidMountOrUpdate.
    this.forceUpdate ()
    this.componentDidMountOrUpdate ()
  })

  componentDidUpdate = (prevProps, prevState) => this | compMeth ((
    { lemmata, showParsTable, },
    { annotationInfo, selectedLemmaIdx: curIdx, },
    { closeAbbrPopup, } = this
  ) => {
    this | whyUpdate ('LemmaList', debugRender) (prevProps, prevState)
    if (showParsTable && isNotEmptyObject (annotationInfo))
      closeAbbrPopup ()
    this.componentDidMountOrUpdate ()
  })

  shouldComponentUpdate (nextProps, nextState) {
    const { props, state, shouldUpdate, } = this
    const { curEntryIdx: curCur, } = props
    const { curEntryIdx: nxtCur, } = nextProps

    if (curCur !== nxtCur) this.updateScrollSympathetic (
      curCur, nxtCur,
    )

    return shouldUpdate ([nextProps, props], [nextState, state])
  }

  // --- decide fixed or abridged.
  getBrowseSingleModeForLemma = (idx) => this | deconstruct (
    ({
      getRowInfo,
      theRefs: { wrapper: { current: ref, }}
    }) => lets (
      // --- getRowInfo can return empty @todo
      () => idx | getRowInfo,
      ({ height, }) => ref.offsetHeight * 0.8 > height,
      ({ height, offset }, lemmaFits) => lemmaFits
        ? BrowseModeLemmataSingleModeFixed
        : BrowseModeLemmataSingleModeAbridge (height, offset),
    ),
  )

  render = () => this | compMeth ((
    {
      history, isMobile,
      lemmata, reductieModeSchool, reductieModeBeknopt,
      citGClicked, onScroll, fontSize, showParsTable,
      renderLemmaListKey,
      browseMode,
    },
    {
      selectedLemmaIdx,
      annotationInfo,
    },
    {
      theRefs: { wrapper: wrapperRef },
      onLemmaClicked,
      annotationClicked, onLemmaListClicked, setWindowList,
      onChangeBrowseMode,
      onHaveWindowListHeight,
      onInnerOffsetTop,
      getBrowseSingleModeForLemma,
      abridged,
    },
  ) => {
    const presentationClass = reductieModeBeknopt
      | ifTrueV ('beknopt', 'not-beknopt')
      | concatTo ('presentation-')
    const clss = ['x__contents', presentationClass] | join (' ')

    debug ('lemma-list-browse-mode', 'browseMode', browseMode.toString ())

    // --- @todo sometimes selectedLemmaIdx is null due to a weird timing issue.
    if (selectedLemmaIdx | notOk) return null

    // --- note, measurements based on the *last* render.

    const newBrowseMode = cond (
      [() => wrapperRef.current | notOk, browseMode | always],
      [() => browseMode | browseModeLemmataIsSingle | not, browseMode | always],
      [() => disableSingle (), () => BrowseModeLemmataExplore],
      [T, () => selectedLemmaIdx | getBrowseSingleModeForLemma | BrowseModeLemmataSingle],
    )

    debug ('lemma-list-browse-mode', 'newBrowseMode', newBrowseMode.toString ())

    const isFixed = newBrowseMode | browseModeLemmataIsFixed
    const isAbridge = newBrowseMode | browseModeLemmataIsAbridge
    const disableMomentumScroll = shouldDisableMomentumScroll ()

    // --- @todo this is not pure (doesn't belong in render).
    lets (
      () => abridged.has (lemmata)
        ? abridged.get (lemmata)
        : {},
      (o) => o | mergeM ({
        [selectedLemmaIdx]: isAbridge,
      }),
      (o) => abridged.set (lemmata, o),
    )

    this.browseModeSingleInfo = [isFixed, isAbridge]

    return <Fragment>
      <ParsWrapperS show={showParsTable}>
        <ParsResultsTable
          originatorKey='lemma'
          ambient='lemmaList'
        />
      </ParsWrapperS>
      <LemmaListInnerWrapperS
        ref={wrapperRef}
        onClick={onLemmaListClicked}
        fixed={isFixed}
        disableMomentumScroll={disableMomentumScroll}
      >
        <LemmaListInner
          key={renderLemmaListKey}
          history={history}
          isMobile={isMobile}
          browseMode={newBrowseMode}
          lemmata={lemmata}
          theClassName={clss}
          selectedLemmaIdx={selectedLemmaIdx}
          reductieModeSchool={reductieModeSchool}
          reductieModeBeknopt={reductieModeBeknopt}
          onLemmaClicked={(id, idx) => onLemmaClicked (renderLemmaListKey, id, idx)}
          // onChangeBrowseMode={onChangeBrowseMode}
          annotationClicked={annotationClicked}
          annotationInfo={annotationInfo}
          citGClicked={citGClicked}
          onScroll={onScroll}
          onInnerOffsetTop={onInnerOffsetTop}
          fontSize={fontSize}
        />
        <div className='x__abbr-popup'/>
      </LemmaListInnerWrapperS>
    </Fragment>
  })
}

const LemmaListInnerWrapperS = styled.div`
  ${mediaQuery (
    mediaPhone (`
      margin-left: 0px;
      margin-right: 0px;
    `),
    mediaTablet (`
      margin-left: 10px;
      margin-right: 0px;
    `),
  )}

  // --- see comment App/index.js
  transform: translate3d(0, 0, 0);

  ${({ fixed, disableMomentumScroll=false, }) => fixed ?
    `
      overflow-y: hidden;
    `
    :
    `
      overflow-y: scroll;
      ${disableMomentumScroll || '-webkit-overflow-scrolling: touch;'}
    `
  }
  position: relative;
  height: 100%;
  .x__contents {
    position: relative;
  }
  .presentation-not-beknopt {
    .punc-bekn {
      display: none;
    }
  }
  .presentation-beknopt {
    .punc-stan, .cit {
      display: none;
    }
  }
`

const doDebugScrollListener = false
const debugScrollListener = s => tap (
  doDebugScrollListener ? debugDevWith (s) : noop,
)

export default LemmaList
