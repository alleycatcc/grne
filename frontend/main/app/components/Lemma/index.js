import {
  pipe, compose, composeRight,
  lets, condS, otherwise, concat,
  eq, guard, join,
  tap, againstBoth, ifPredicate, ok, not,
  bindProp, path, each, dot1, dot2,
  whenOk, noop, mergeM,
  whenPredicate, prop, sprintf1, precat, ifNotOk,
  split, cond,
  ifTrue,
  defaultTo,
  flip,
  T, F,
  ifFalse,
  deconstruct, deconstructN,
  arg0,
  multiply,
  guardV,
} from 'stick-js/es'

import React, { PureComponent, Component, } from 'react'
import styled from 'styled-components'

import memoize from 'memoize-immutable'
import { withResizeDetector, } from 'react-resize-detector'

import { cata, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { eachSelector, onClickNative, onClickNativeStopPropagation, addClass, clss, scrollIntoView, } from 'alleycat-js/es/dom'
import { ierror, logWith, setTimeoutOn, } from 'alleycat-js/es/general'
import { ifTrueV, whenFalseV, isEmptyString, isNotEmptyString, ifOkV, } from 'alleycat-js/es/predicate'
import { shouldUpdateShallow, compMeth, createRef, } from 'alleycat-js/es/react-legacy'
import { whyYouRerender, } from 'alleycat-js/es/react'
import { media, mediaQuery, } from 'alleycat-js/es/styled'

import { browseToId, mediaPhone, mediaTablet, mediaDesktop, disableSingle, } from '../../common'
import config from '../../config'
import { debug, } from '../../debug'
import {
  BrowseModeLemmataNormal,
  BrowseModeLemmataExplore,
  browseModeLemmataIsExploring,
} from '../../types'

const configTop = config | configure.init
const debugRender = 'debug.rerender' | configTop.get

// --- because we're retrieving html from the server and using 'dangerouslySetInnerHTML' we're using
// string mixins instead of simply using styled components.

const HoofdWSMixin = (props) => `
  .hoofdW {
    font-weight: bold;
    color: #139;
  }
`

const HoofdWS = styled.div`
  ${HoofdWSMixin}
  display: inline-block;
`

export const HoofdW = HoofdWS

const LemmaS = styled.div`
  ${HoofdWSMixin}

  display: inline-block;

  .hoofdW {
    ${prop ('selectedOnlyExplore') >> ifTrueV (
      'border-bottom: 2px solid #139',
      '',
    )};
  }
  font-family: Source Sans Pro;
  // --- temporary fix for virtual list problem.
  &.x--mobile {
    // font-family: sans-serif;
  }
  cursor: ${prop ('cursorPointer') >> ifTrueV (
    'pointer', 'text',
  )};

  width: 100%;
  display: inline-block;
  text-indent: -20px;
  // --- see also LEMMA_PADDING_LEFT in LemmaList.
  padding-left: 30px;
  padding-right: 15px;
  font-size: 14px;
  line-height: 21px;
  opacity: 1.0;

  // --- matches the template in the xslt transform.
  // --- these are all <div> elements (so they can contain children) but they should lay out like
  // <span>.
  .vorm, .xlBet, .bet, .escBet, .cit, .citIsgel, .isgel, .isgelCit, .citG, .citNV, .vertM, .gebrW,
  .morfI, .etym, .toel, .hulpI, .werkV, .hoofdW, .verw, .aut, .werk, .plaats, .kruisVerw, .tmp, .i,
  .r, .s, .b, .link {
    display: inline;
  }

  .vertM, .werkV {
    font-weight: bold;
  }
  .i, .r {
    font-style: normal;
  }
  .s, .aut {
    font-variant: small-caps;
  }
  .i, .werk, .gebrW, .morfI {
    font-style: italic;
  }
  .toel {
    color: black;
  }
  .toel, .hulpI {
    font-weight: normal;
    font-style: italic;
  }
  .link {
    font-style: normal;
    cursor: pointer;
    ${prop ('linksDisabled') >> ifTrueV (
      'color: black', 'color: #139',
    )}
  };

  .annotation-link, .citg-word {
    cursor: pointer;
  }

  .snippet {
    // --- @todo ugly
    opacity: 1.0 !important;

    .abbr, .citg-word, .verw {
      cursor: inherit;
      border-bottom: 0px;
    }
    .link {
      color: inherit;
      cursor: inherit;
    }
  }

  div.lem, div.verwLem, div.escLem, div.xlLem, div.snippet {
    padding-top: 0px;
    padding-bottom: 0px;
  }

  &.x--small {
    font-size: 14px;
    line-height: 21px;
  }

  &.x--medium {
    font-size: 16px;
    line-height: 24px;
  }

  &.x--big {
    font-size: 18px;
    line-height: 27px;
  }

  &.x--mobile {
    font-size: 16px;
    line-height: 30px;
    border-top: 0px solid black;
    border-bottom: 0px solid black;
  }

  &.x--selected {
    .annotation-link, .citg-word {
      :hover {
        border-bottom: 2px black dotted;
      }
    }
    &.x--mobile {
      .annotation-link, .citg-word {
        :hover {
          border-bottom-width: 0px;
        }
      }
      &.x--mobile.x--not-exploring {
        padding-top: 0px;
        padding-bottom: 0px;
        border-top: 1px solid black;
        border-bottom: 1px solid black;
      }
      &.x--mobile.x--exploring {
        padding-top: 1px;
        padding-bottom: 1px;
        border-top-width: 0px;
        border-bottom-width: 0px;
      }
    }
  }

  &.x--not-selected {
    &.x--mobile.x--not-exploring {
      padding-top: 1px;
      padding-bottom: 1px;
      // filter: blur(3px);
      cursor: pointer;
      &, .hoofdW, .link {
        color: #888 !important;
      }
      opacity: 0.7;
    }
    &.x--mobile.x--exploring {
      padding-top: 1px;
      padding-bottom: 1px;
      opacity: 1.0;
    }
    &.x--not-mobile {
      opacity: 0.5;
    }
    &.x--mobile {
      .annotation-link, .citg-word {
        :hover {
          border-bottom-width: 0px;
        }
      }
    }
    :hover {
      opacity: 1.0;
    }
  }

  .search-highlight {
    background: yellow;
  }
  .snippet {
    display: inline-block;
  }

  // --- niv123 and xlNiv123 have subtle differences but it's not clear why.
  // --- here we try to make them behave the same (space after list marker, padding/margins etc.).

  ol {
    margin-top: 0px;
    margin-bottom: 0px;
  }
  ol.niv123 {
    text-indent: 0px;
    padding-left: 10px;
    list-style-type: decimal;
  }
  ol.xlNiv123 {
    padding-left: 0px;
    list-style-type: decimal;
  }
  ol.nivABC {
    text-indent: 0px;
    padding-left: 13px;
    list-style-type: lower-latin;
  }
  ol.xlNivABC {
    padding-left: 14px;
    list-style-type: lower-alpha;
  }
  ol.xlNiv-3 {
    padding-left: 11px;
    list-style-type: lower-roman;
  }
  ol.xlNiv-4 {
    padding-left: 13px;
    list-style-type: decimal;
  }
  ol.xlNiv-5 {
    padding-left: 13px;
    list-style-type: lower-alpha;
  }
  ol.xlNiv123, ol.xlNivABC, ol.xlNiv-3, ol.xlNiv-4, ol.xlNiv-5 {
    text-indent: 0px;
  }
  ${mediaQuery (
    mediaPhone (`
      ol.niv123 {
        margin-left: 0px;
      }
      ol.xlNiv123 {
        margin-left: 11px;
      }
    `),
    mediaTablet (`
      ol.niv123 {
        margin-left: 15px;
      }
      ol.xlNiv123 {
        margin-left: 26px;
      }
    `),
  )}

`

const LemmaWrapperS = styled.div`
  ${prop ('t1Gecontroleerd') >> whenFalseV (`
    .hoofdW {
      color: #AA1212;
      &:before {
        color: #AA1212;
        font-size: 20px;
        content: '⦁ ';
      }
    }
    `,
  )}
`

export class Lemma extends Component {
  ref = this.props.theRef || createRef ()
  shouldUpdate = shouldUpdateShallow (['annotationClicked', 'onClick'])

  componentDidUpdate (prevProps, prevState) {
    if (debugRender) whyYouRerender ('Lemma', [prevProps, this.props], [prevState, this.state])
  }

  onAnnotationClicked = (f) => onClickNative ((event, elem) => this | compMeth ((
    { disableAnnotationClick, }
  ) => disableAnnotationClick | ifTrue (
      noop,
      () => {
        debug ('lemma', 'onAnnotationClicked ()')
        f (event, elem)
        event.stopPropagation ()
      },
    )
  ))

  // --- this is all really slow -- we should move to server-side rendering.
  postProcess () {
    const { props, ref, onAnnotationClicked, } = this
    const { homonymNr, annotationClicked, citGClicked, history, } = props
    const isValid = againstBoth (ok, isNotEmptyString)
    const whenValid = isValid | whenPredicate
    const ifValid = isValid | ifPredicate

    const lemmaS = ref.current

    const browseToLemmaId = history | browseToId

    if (homonymNr | ok) lemmaS | eachSelector ('.hoofdW') ((elem) => lets (
      () => homonymNr | sprintf1 ('%s. '),
      (hr) => elem.innerHTML = elem.innerHTML | precat (hr),
    ))

    lemmaS | eachSelector ('.abbr') ((elem) => lets (
      () => elem.getAttribute ('data-abbr'),
      (expansion) => expansion | whenValid (() => elem
        | addClass ('annotation-link')
        | onAnnotationClicked ((event, elem) => annotationClicked | whenOk (
          (callback) => (callback ('abbr', expansion, elem, lemmaS, event))
        )),
      ),
    ))

    lemmaS | eachSelector ('.verw') ((elem) => lets (
      () => elem.getAttribute ('data-abbr-verw'),
      (expansion) => expansion | whenValid (() => elem
        | addClass ('annotation-link')
        | onAnnotationClicked ((event, elem) => annotationClicked | whenOk (
          (callback) => callback ('verw', expansion, elem, lemmaS, event)
        ),)
      ),
    ))
    lemmaS | eachSelector ('.link') (
      onAnnotationClicked ((event, elem) => lets (
        () => elem.getAttribute ('data-targetid'),
        (lemmaId) => browseToLemmaId (lemmaId),
      )),
    )
    lemmaS | eachSelector ('.citg-word') (
      onAnnotationClicked ((event, elem) => citGClicked | whenOk (
     (callback) => elem.innerHTML | callback,
      )),
    )
  }

  /*
  we use 'stop propagation' to stop the onclick being processed by both parent and child
  and causing the popup to immediately close again.
  */

  componentDidMount () {
    const { props, ref, } = this
    const { postProcess=true, } = props
    if (postProcess) this.postProcess ()
  }

  shouldComponentUpdate (nextProps, nextState) {
    const { props, state, shouldUpdate, } = this
    return shouldUpdate ([nextProps, props], [nextState, state])
  }

  render () {
    const { props, ref, } = this
    const {
      isMobile=false,
      disableAnnotationClick,
      reductieModeSchool, isSelected,
      browseMode=BrowseModeLemmataNormal,
      homonymNr, html,
      htmlSchool=null, t1Gecontroleerd: t1GecontroleerdProp,
      onClick, className, annotationInfo=null,
      cursorPointer=false,
      // --- false for advanced search results, analyse.
      isSelectable=false, fontSize,
    } = props
    const theHtml = reductieModeSchool ? htmlSchool : html

    const { floor, random, } = Math

    const t1Gecontroleerd = reductieModeSchool | ifTrue (
      // () => random () | multiply (2) | floor | Boolean,
      () => t1GecontroleerdProp,
      () => null,
    )

    const isExploring = browseMode | browseModeLemmataIsExploring

    const cls = clss (
      className,
      isMobile ? 'x--mobile' : 'x--not-mobile',
      isSelectable && (isSelected ? 'x--selected' : 'x--not-selected'),
      isExploring ? 'x--exploring' : 'x--not-exploring',
      'x--' + fontSize,
    )

    return <LemmaWrapperS t1Gecontroleerd={t1Gecontroleerd}>
      <LemmaS
        selectedOnlyExplore={isSelected && disableSingle ()}
        onClick={onClick}
        cursorPointer={cursorPointer}
        className={cls}
        homonymNr={homonymNr}
        dangerouslySetInnerHTML={{ __html: theHtml, }}
        ref={ref}
        linksDisabled={disableAnnotationClick}
      />
      <Annotation info={annotationInfo} />
    </LemmaWrapperS>
  }
}

const T1GecontroleerdS = styled.div`
  ${prop ('show') >> ifTrueV (
    `display: block;`,
    `display: none;`,
  )}
  width: 15px;
  height: 15px;
  border-radius: 10000px;
  background: #991212;
  display: inline-block;
`

const T1Gecontroleerd = ({ gecontroleerd, }) => <T1GecontroleerdS
  show={gecontroleerd | not}
/>

const AbbrPopupS = styled.div`
  text-align: center;
`

const AbbrPopup = ({ expansion, }) => <AbbrPopupS>
  {expansion}
</AbbrPopupS>

const VerwPopupS = styled.div`
  text-align: center;
  .x__werk {
    font-style: italic;
  }
`

const expandVerw = memoize ((expansion) => lets (
  // -- e.g. aut___werk___plaats, aut______plaats, etc.
  () => expansion | split ('___'),
  ([aut, werk, plaats]) => cond (
    (() => !werk && !plaats) | guard (() => [false, false]),
    (() => !werk && plaats)  | guard (() => [false, true]),
    (() => werk && !plaats)  | guard (() => [true, false]),
    (() => werk && plaats)   | guard (() => [true, true]),
  ),
  ([aut, werk, plaats], [comma, dot]) => <VerwPopupS>
    {aut}{comma && ','} <span className='x__werk'>{werk}</span> {plaats}{dot && '.'}
  </VerwPopupS>,
))

const VerwPopup = arg0 >> prop ('expansion') >> expandVerw

const popupTable = {
  abbr: AbbrPopup,
  verw: VerwPopup,
}

const annotationStyle = ([key, val]) => lets (
  () => val | String | concat ('%'),
  (valp) => [key, valp] | join (': '),
)

const Annotation = ({ info, }) => info | ifNotOk (
  () => null,
  ({ type, expansion, x, y, lemmaWidth, }) => lets (
    () => popupTable [type] || ierror ('Annotation: ' + type),
    () => x < 50 ? ['left', x] : ['right', 100 - x],
    () => ['bottom', 100 - y],
    (Component, leftRight, topBottom) =>
      <AnnotationPopup
        leftRight={leftRight | annotationStyle}
        topBottom={topBottom | annotationStyle}
        lemmaWidth={lemmaWidth}
      >
        <Component expansion={expansion}/>
      </AnnotationPopup>,
  ),
)

class AnnotationPopup extends PureComponent {
  state = {
    center: false,
  }
  ref = createRef ()
  update = () => this | compMeth ((
    ({ lemmaWidth }, _, { ref: { current: domRef, }}) => {
      // --- timeout established through trial & error -- seems to fix the case where it doesn't
      // scroll unless the lemma is already selected.
      100 | setTimeoutOn (() =>
        domRef | scrollIntoView ({
          behavior: 'smooth',
          block: 'nearest',
        }),
      )
      if (domRef.offsetWidth > lemmaWidth / 2) this.setState ({
        center: true,
      })
    }
  ))
  componentDidUpdate = () => this.update ()
  componentDidMount = () => this.update ()
  render = () => this | compMeth (
    (props, { center }, { ref, }) => props | deconstruct (
      ({ leftRight, topBottom, }) => <AnnotationPopupS
        {...props}
        center={center}
        ref={ref}
      />,
    ),
  )
}

const AnnotationPopupS = styled.div`
  ${({ center, leftRight, }) => center | ifTrue (
    () => 'left: 50%; transform: translateX(-50%)',
    () => leftRight,
  )};
  ${prop ('topBottom')};
  ${mediaQuery (
    mediaPhone (`
      white-space: normal;
      min-width: 70%;
    `),
    mediaTablet (`
      white-space: nowrap;
      min-width: auto;
    `),
  )}
  position: absolute;
  border: 1px solid black;
  background: white;
  white-space: nowrap;
  padding: 14px 24px 14px 20px;
  z-index: 4;
  cursor: pointer;
  box-shadow: 0px 0px 6px #999;
`
