import {
  pipe, compose, composeRight,
  dot, whenOk, map, ifOk,
  tap, prop, defaultTo,
  always,
  take, tail,
  condS, guard, otherwise, guardV, ok,
  eq,
  lets, letS,
  reduce, concatTo, xReplace, id,
  filter, lt, gt,
  join,
  path,
  divideBy,
  rangeTo,
  noop,
  ne, whenPredicate,
  ifNotOk,
  concat,
  minus, plus,
  deconstruct2,
  arg0, ifTrue,
} from 'stick-js/es'

import React, { Fragment, } from 'react'
import styled from 'styled-components'

import { foldJust, cata, toJust, fold, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { toString, length, mapX, logWith, notBelow, notAbove, getQueryParams, } from 'alleycat-js/es/general'
import { ifTrueV, } from 'alleycat-js/es/predicate'
import { deconstructProps, withDisplayName, } from 'alleycat-js/es/react'
import { errorError, } from 'alleycat-js/es/react-s-alert'
import { mediaQuery, } from 'alleycat-js/es/styled'

import { spinner, } from '../../alleycat-components'
import { H1, H1M, H2, Section, ButtonsRowM, ButtonM, } from '../../components/shared'
// import { Link, } from 'react-router-dom'
import BooleanGrid from '../../components/BooleanGrid'
import { Lemma, } from '../../components/Lemma'

import { mediaPhone, mediaTablet, mediaDesktop, } from '../../common'
import config from '../../config'
import {
  advSearchGetState,
  AdvSearchAuxFieldFulltext, AdvSearchAuxFieldVertM, AdvSearchAuxFieldHoofdW,
} from '../../types'

const configTop = config | configure.init

const fst = arg0

const whenGreaterThanZero = 0 | gt | whenPredicate

const SpinnerComet = 'comet' | spinner

const resultColor = 'colors.theme2' | configTop.get

const lemma = condS ([
  1 | eq | guardV ('lemma'),
  otherwise | guardV ('lemmata'),
])

const ResultsInnerS = styled.div`
  .x__top-line {
    position: absolute;
    background-color: #999;
    height: 3px;
    top: 0;
    left: 0;
    width: ${ prop ('active') >> ifTrueV ('100%', '0%')};
    transition: width .1s;
  }
  .x__error {
    color: #D11;
    margin-left: 45px;
  }
`

const Results = ({
  isMobile, numResultsSetting, setNumResults, setPage, curPage, maxPageMb, active, loadingMore,
  numHits, results, error, onSelectResult,
  curField, setField,
}) => <div>
  <ResultsInnerS active={active}>
    <div className='x__top-line'/>
    <div className='x__error'>
      {error | condS ([
        true | eq | guard (() => 'Error.'),
        ok        | guard (fst >> concatTo ('Error: ')),
        otherwise | guard (() => ''),
      ])}
    </div>
    {results | whenOk (showResultsResults (
      isMobile,
      numHits, numResultsSetting,
      loadingMore, curPage, maxPageMb,
      setNumResults, setPage, onSelectResult,
      curField, setField,
    ))}
  </ResultsInnerS>
</div>

const ResultsRowS = styled.div`
  min-height: 40px;
  .x__left {
    .x__hoofdW, .x__hit-num {
      vertical-align: top;
      display: inline-block;
      font-weight: bold;
      font-size: 1.17em;
      color: ${resultColor | always};
      cursor: pointer;
    }
    .x__hoofdW a {
      color: ${resultColor | always};
      text-decoration: none;
    }
    .x__hit-num {
      padding-right: 3px;
      white-space: nowrap;
      color: #666;
      text-align: left;
    }
  }
`

const cleanHoofdW = xReplace (/[,;].+/) ('')

const LemmaWrapperS = styled.div`
  margin-top: 10px;
  margin-bottom: 20px;
  margin-left: 45px;
  min-height: 18px;
  .hoofdW {
  }
  ${mediaQuery (
    mediaPhone ('width: 77%;'),
    mediaTablet ('width: 620px;'),
  )}
`

const PreviewSpinnerS = styled.div`
  position: relative;
  top: 4px;
  // --- for if we want the spinner near the words
  width: 20px;
  // --- for if we want the spinner near the center
  // width: 100%;
`

const SnippetS = styled.div`
`

const Snippet = ({ html, onClick, }) => <SnippetS>
  <Lemma
    html={html}
    onClick={onClick}
    cursorPointer={true}
    postProcess={false}
  />
</SnippetS>

const row = (rowIdx, hoofdW, html, onSelect, _) =>
  <ResultsRowS>
    <div className='x__left'>
      <div className='x__hit-num'>
        {rowIdx + 1}.
      </div>
      <div className='x__hoofdW' onClick={
        () => rowIdx | onSelect
      }>
          {hoofdW | cleanHoofdW}
      </div>
      <LemmaWrapperS>
        {html | ifNotOk (
          () => <PreviewSpinnerS>
            <SpinnerComet size={Math.floor (18 / 1.6)} spinning={true}/>
          </PreviewSpinnerS>,
          () => <Snippet html={html} onClick={() => rowIdx | onSelect}/>,
        )}
      </LemmaWrapperS>
    </div>
  </ResultsRowS>

const ResultsRow = ({ idx, contents, onSelect, }) => contents | letS ([
  contents => contents | prop ('hoofdWClean') | defaultTo (
    _ => 'Error' | tap (_ => 'Missing hoofdW' | errorError)
  ),
  contents => contents | prop ('linkId') | defaultTo (
    _ => 'Error' | tap (_ => 'Missing link-id' | errorError)
  ),
  contents => contents | prop ('html') | defaultTo (_ => null),
  (_, hoofdW, linkId, html) => row (idx, hoofdW, html, onSelect, linkId),
])

const ResultsWrapperS = styled.div`
  .x__title {
    padding-top: 2%;
    padding-bottom: 3%;
  }
  .x__heading, .x__controls {
    display: inline-block;
  }
  .x__controls {
    margin-top: 5px;
    vertical-align: top;
    cursor: text;
  }
  .x__heading1 {
    margin-bottom: 7px;
  }
`

// @todo
const nums = [50, 100, 200]
const getNums = numHits => nums | filter (numHits | lt)

const numResultsChoices = (cur, onChange) => mapX ((choice, idx) =>
  <Selector
    key={idx}
    onClick={_ => choice | onChange}
    text={choice}
    isCur={choice === cur}
  />
)

const ControlsS = styled.table`
  ${mediaQuery (
    mediaPhone ('display: none'),
    mediaTablet ('display: block'),
  )}
  > tbody {
    > tr {
      > td:nth-child(1) {
        font-weight: bold;
        padding-right: 10px;
      }
    }
  }
  display: ${prop ('show') >> ifTrueV ('inline-block', 'none')};
  padding: 3px;
  margin-top: 9px;
  margin-bottom: 2px;
  font-size: 14px;
`

const minWidthControls = '220px'

const NumResultsChooserS = styled.tr`
  display: ${prop ('visible') >> ifTrueV ('table row', 'none')};
`

const NumResultsChooser = ({ cur, onChange, choices, }) =>
  <NumResultsChooserS
    visible={choices | length | gt (1)}
  >
    <td>
      Aantal resultaten per pagina
    </td>
    <td>
      {choices | numResultsChoices (cur, onChange)}
    </td>
  </NumResultsChooserS>

const SelectorS = styled.div`
  font-size: ${'fontSize' | prop};
  cursor: pointer;
  display: inline-block;
  padding-left: 2px;
  padding-right: 2px;
  margin-left: 7px;
  margin-right: 7px;
  &.x--cur {
    border-bottom: 1px solid black;
    cursor: text;
  }
`

const Selector = ({ text, isCur, onClick, fontSize='16px', }) => lets (
  _ => isCur ? ['x--cur'] : [],
  cn => <SelectorS
    fontSize={fontSize}
    className={cn | join (' ')}
    onClick={onClick}
  >
    {text}
  </SelectorS>,
)

const Paginator = ({ curPage, minPage, maxPage, maxPages, setPage, }) =>
  <tr>
    <td>
      Pagina
    </td>
    <td>
      {lets (
        () => minPage | plus (maxPages) | notAbove (maxPage),
        (upper) => minPage | rangeTo (upper + 1) | map ((n) => lets (
          () => n === curPage,
          () => _ => n | setPage,
          (isCur, onClick) => <Selector
            key={n}
            text={String (n + 1)}
            isCur={isCur}
            onClick={isCur ? noop : onClick}
          />,
        ),
      ))}
    </td>
  </tr>

const ResultsRowsWrapperS = styled (Section)`
`

const H1Component = deconstructProps (
  ({ isMobile, children, }) => props => lets (
    () => isMobile ? [H1M, { withLeftMargin: true, }] : [H1, {}],
    ([H, passProps]) =>
      <H {...passProps} {...props}>{children}</H>,
  ),
)

const InfiniteScrollSpinnerS = styled.div`
  position: absolute;
  left: 50%;
  height: 50px;
  transform: translateX(-50%);
  color: ${resultColor};
`

const ButtonsRowMWrapperS = styled.div`
  width: 85%;
  margin: auto;
  font-size: 14px;
`

const FieldControlsMS = styled.div`
  // white-space: nowrap;
  vertical-align: middle;
  margin-top: -3px;
  padding-top: 26px;
  form {
    display: flex;
    height: 50px;
    width: 95%;
    margin: auto;
    > * {
      flex: auto;
      margin: auto;
    }
  }
  .x__radio {
    width: 20px;
    height: 20px;
    display: inline-block;
    text-align: center;
    vertical-align: middle;
    input {
      vertical-align: middle;
    }
  }
  .x__text {
    display: inline-block;
    vertical-align: middle;
  }
`

// --- the radio buttons were an option to show Leiden.
// --- completely removing them now from the DOM (this element just eats its children), because the
// presence of both the radio buttons and the buttons was causing a crash on iOS safari.

const SearchFieldsRadioFormS = styled.div`
`

const ResultsWrapper = ({
  isMobile,
  numHits, numResultsSetting,
  loadingMore,
  curPage, maxPageMb, setPage, results,
  curField,
  setNumResults,
  setField,
  onSelectResult,
}) => <ResultsWrapperS>
  {isMobile &&
    <FieldControlsMS>
      {lets (
        () => [
          AdvSearchAuxFieldFulltext | setField,
          AdvSearchAuxFieldHoofdW | setField,
          AdvSearchAuxFieldVertM | setField,
        ],
        ([sf, sh, sv]) => curField | cata ({
          AdvSearchAuxFieldFulltext: () => [
            [true, false, false],
            [sh, sv],
          ],
          AdvSearchAuxFieldHoofdW: () => [
            [false, true, false],
            [sf, sv],
          ],
          AdvSearchAuxFieldVertM: () => [
            [false, false, true],
            [sh, sf],
          ],
        }),
        ([sf, sh, sv], [[f, h, v], [leftButtonClick, rightButtonClick]]) =>
          <Fragment>
            <SearchFieldsRadioFormS>
              <form>
                <div className='x__radio'>
                  <input type='radio' name='dummy'
                    onChange={sf}
                    defaultChecked={f}/>
                </div>
                <div className='x__text'>
                  fulltext
                </div>
                <div className='x__radio'>
                  <input type='radio' name='dummy'
                    onChange={sh}
                    defaultChecked={h}
                  />
                </div>
                <div className='x__text'>
                  hoofdwoord
                </div>
                <div className='x__radio'>
                  <input type='radio' name='dummy'
                    onChange={sv}
                    defaultChecked={v}
                  />
                </div>
                <div className='x__text'>
                  vertaalmogelijkheid
                </div>
              </form>
            </SearchFieldsRadioFormS>
          </Fragment>,
      )}
    </FieldControlsMS>
  }
  <div className='x__title'>
    <div className='x__heading'>
      <div className='x__heading1'>
        <H1Component
          isMobile={isMobile}
          withBottomMargin={false}
        >Zoekresultaten</H1Component>
      </div>
      <H2
        style={{ display: 'inline-block', }}
        withTopMargin={false}
      >{numHits} {numHits | lemma} gevonden</H2>
    </div>
    <div className='x__controls'>
        <ControlsS>
          <tbody>
            {lets (
              () => [
                AdvSearchAuxFieldFulltext | setField,
                AdvSearchAuxFieldHoofdW | setField,
                AdvSearchAuxFieldVertM | setField,
              ],
              () => curField | cata ({
                AdvSearchAuxFieldFulltext: () => [true, false, false],
                AdvSearchAuxFieldHoofdW: () => [false, true, false],
                AdvSearchAuxFieldVertM: () => [false, false, true],
              }),
              ([sf, sh, sv], [f, h, v]) =>
                <tr>
                  <td>
                    Zoek op
                  </td>
                  <td>
                    <Selector
                      text='fulltext'
                      isCur={f}
                      onClick={sf}
                      fontSize='14px'
                    />
                    <Selector
                      text='hoofdwoord'
                      isCur={h}
                      onClick={sh}
                      fontSize='14px'
                    />
                    <Selector
                      text='vertaalmogelijkheid'
                      isCur={v}
                      onClick={sv}
                      fontSize='14px'
                    />
                  </td>
                </tr>,
            )}
      {/* maxPage is 0-based; -1 on no results */}
      {maxPageMb | foldJust (whenGreaterThanZero (maxPage =>
        <Fragment>
            <NumResultsChooser
              onChange={setNumResults}
              cur={numResultsSetting}
              choices={numHits | getNums}
            />
            <Paginator
              curPage={curPage}
              minPage={curPage | minus (5) | notBelow (0)}
              maxPage={maxPage}
              maxPages={10}
              setPage={setPage}
            />
        </Fragment>
      ))}
          </tbody>
        </ControlsS>
    </div>
  </div>
  <ResultsRowsWrapperS>
    {results | mapX ((contents, idx) => lets (
      () => isMobile ? 0 : curPage * numResultsSetting,
      (pageCorrection) => <ResultsRow
        key={idx}
        idx={idx + pageCorrection}
        contents={contents}
        onSelect={onSelectResult}
      />
    ))}
  </ResultsRowsWrapperS>
  {loadingMore && <InfiniteScrollSpinnerS>
    <SpinnerComet size={16} spinning={true}/>
  </InfiniteScrollSpinnerS>}
</ResultsWrapperS>

const showResultsResults = (
  isMobile,
  numHitsMb, numResultsSetting,
  loadingMore, curPage, maxPageMb,
  setNumResults, setPage, onSelectResult,
  curField, setField,
) =>
  (results) => numHitsMb | fold (
    (numHits) => <ResultsWrapper
      isMobile={isMobile}
      numHits={numHits}
      numResultsSetting={numResultsSetting}
      curPage={curPage}
      curField={curField}
      maxPageMb={maxPageMb}
      results={results}
      loadingMore={loadingMore}
      setField={setField}
      setNumResults={setNumResults}
      setPage={setPage}
      onSelectResult={onSelectResult}
    />,
    'error',
  )

const booleanGroupsSpec = []

const WrapperS = styled.div`
  position: relative;
  height: 100%;
`

export default withDisplayName ('AdvancedSearchResults') (
  ({
    isMobile, numResults: numResultsMbArg, setNumResults, setPage, curPage, maxPageMb, results, numResultsSetting, onSelectResult,
    curField, setField,
  }) => <WrapperS>
    {lets (
      () => results | advSearchGetState (numResultsMbArg),
      ([active, loadingMore, numResultsMb, results, err]) => <Results
        isMobile={isMobile}
        curPage={curPage}
        curField={curField}
        maxPageMb={maxPageMb}
        numResultsSetting={numResultsSetting}
        active={active}
        loadingMore={loadingMore}
        numHits={numResultsMb}
        results={results}
        error={err}
        setPage={setPage}
        setNumResults={setNumResults}
        setField={setField}
        onSelectResult={onSelectResult}
      />,
    )}
    <BooleanGrid
      spec={booleanGroupsSpec}
    />
  </WrapperS>,
)
