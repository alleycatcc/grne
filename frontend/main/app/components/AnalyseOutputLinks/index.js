import {
  pipe, compose, composeRight,
  tap, lets, ifOk, map, prop,
} from 'stick-js/es'

import React, { Fragment, } from 'react'

import { FixedSizeList as List, } from 'react-window'
import AutoSizer from 'react-virtualized-auto-sizer'
import styled from 'styled-components'

import { fold, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { partition, } from 'alleycat-js/es/general'
import { withDisplayName, } from 'alleycat-js/es/react'

import { ExternalLink, } from '../../components/shared'
import config from '../../config'

const configTop = config | configure.init

const configFont = configTop.get ('font.analyse')
const { family: fontFamily, } = lets (
  _ => configFont | configure.init,
  conf => conf.gets ('family'),
)

const rowRenderer = (results) => withDisplayName ('Row') (
  ({ index, style, }) => <div
    style={style}
  >
    {lets (
      () => results [index],
      (props) => linksOutput (props),
    )}
  </div>
)

const linksOutput = ({ naam, homonym_nr, unresolved, link_id, }) => lets (
  () => homonym_nr | ifOk (
    (hr) => naam + '-' + String (homonym_nr),
    () => naam,
  ),
  () => '/browse/' + String (link_id),
  (fullnaam, url) => <div>
    Couldn&apos;t resolve {unresolved} in lemma {
      <ExternalLink url={url}>{fullnaam}</ExternalLink>
    }
  </div>,
)

const WrapperS = styled.pre`
  font-size: 11px;
  height: 400px;
  width: 100%;
  .x__list {
    height: 300px;
    margin-bottom: 30px;
    border: 1px solid #330000;
    padding: 5px;
  }
  .x__title {
    font-family: ${_ => fontFamily};
    font-size: 22px;
    text-decoration: underline;
  }
`

// --- partition the results into two lists, one with is_kruisverw `true` and the other with
// `false`.

const partitionResults = partition (prop ('is_kruisverw'))

const WindowList = ({ results, }) =>
  <AutoSizer>
    {({ height, width, }) =>
      <List
        height={height}
        width={width}
        itemCount={results.length}
        itemSize={35}
      >
        {results | rowRenderer}
      </List>
    }
  </AutoSizer>

export default withDisplayName ('AnalyseOutputLinks') (
  ({ results, }) => <WrapperS>
    {results | fold (
      _ => '',
      map (partitionResults)
      >> fold (([resultsKruisVerw, resultsOther]) =>
        <Fragment>
          <div className='x__title'>
            Links in kruisverwijzing elements
          </div>
          <div className='x__list'>
            <WindowList results={resultsKruisVerw}/>
          </div>
          <div className='x__title'>
            Links in other elements
          </div>
          <div className='x__list'>
            <WindowList results={resultsOther}/>
          </div>
        </Fragment>,
        null,
      ),
    )}
  </WrapperS>,
)
