import {
  pipe, compose, composeRight,
  tap, prop,
  lets, noop,
  concat, multiply,
  sprintf1,
} from 'stick-js/es'

import React, { Fragment, memo, } from 'react'

import styled from 'styled-components'

import configure from 'alleycat-js/es/configure'
import { logWith, } from 'alleycat-js/es/general'
import { clss, } from 'alleycat-js/es/dom'
import { ifTrueV, } from 'alleycat-js/es/predicate'
import { RouterLinkDark, LinkTheme2, } from '../../components/shared'

import config from '../../config'

const configTop = config | configure.init

const [position, width] = lets (
  () => 'panelWidth' | configTop.get | String,
  (pos) => pos | sprintf1 ('calc(100%% - %spx)'),
  (pos) => pos | sprintf1 ('%spx'),
  (_, p, w) => [p, w],
)

const MenuMS = styled.div`
  position: absolute;
  // --- 12px through trial & error.
  height: calc(100% - 50px - 12px);
  top: 50px;
  width: ${width};
  padding: 15px;
  padding-left: 25px;
  background: #f7f3fd;
  box-shadow: 1px 1px 10px #999;
  color: black;
  z-index: 100;
  ${prop ('isOpen') >> ifTrueV (
    `
      left: ${position};
      transition: left 300ms;
    `,
    `
      left: 100%;
      transition: left 100ms;
    `,
  )}
  .x__contents {
  }
`

const ItemS = styled.div`
  color: black;
  font-size: 15px;
  height: 48px;
  margin-left: -10px;
  position: relative;
  .x__wrapper {
    white-space: nowrap;
    display: inline-block;
    padding: 10px;
  }
  .x--is-link {
    text-decoration: underline;
  }
`

const Item = ({ makeLink, link, onClick, children, }) => lets (
  () => makeLink ?
    ['x--is-link', RouterLinkDark, { onClick, to: link, }] :
    [null, Fragment, {}],
  ([cls]) => clss ('x__wrapper', cls),
  ([_, Elem, props], clsn) => <ItemS>
    <div className={clsn}>
      <Elem {...props}>{children}</Elem>
    </div>
  </ItemS>
)

const item = (curpath, onClick) => ({ key, link, text, }) => lets (
  () => curpath !== link,
  (makeLink) => <Item onClick={onClick} key={key} link={link} makeLink={makeLink}>
    {text}
  </Item>,
)

const MenuM = ({ curLocation, onClick, isOpen, onSelect, }) => lets (
  () => item (curLocation.pathname, onSelect),
  (makeItem) => [
    makeItem ({ key: 0, link: '/advanced-search', text: 'Geavanceerd zoeken'}),
    makeItem ({ key: 1, link: '/colofon', text: 'Colofon'}),
    makeItem ({ key: 2, link: '/feedback', text: 'Problemen & feedback'}),
    // makeItem ({ key: 3, link: '/hulp', text: 'Hulp'}),
  ],
  (_, items) => <MenuMS onClick={onClick} isOpen={isOpen}>
    <div className='x__contents'>
      {items}
    </div>
  </MenuMS>
)

export default MenuM | memo
