import {
  pipe, compose, composeRight,
  prop, whenTrue, sprintf1,
  ifOk, always,
  tap, id, lets, mergeTo,
  whenOk, concatTo,
  sprintfN,
  arg0,
} from 'stick-js/es'

import React, { PureComponent, } from 'react'
import styled from 'styled-components'
import { Link, } from 'react-router-dom'

import configure from 'alleycat-js/es/configure'
import { logWith, mapX, } from 'alleycat-js/es/general'
import { ifTrueV, whenTrueV, } from 'alleycat-js/es/predicate'
import { ElemP, deconstructProps, withDisplayName} from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import { mediaPhone, mediaTablet, mediaDesktop, isMobileWidth, browseLinkForRouter, } from '../../common'

import { spinner, } from '../../alleycat-components'

import config from '../../config'

const configTop = config | configure.init

const SpinnerComet = 'comet' | spinner

const themeColor2 = 'colors.theme2' | configTop.get

// --- @todo styling is repeated here and SearchField.
const InputBaseMixin = `
  border: 1px solid #7c7c7c;
  height: 100%;
  width: 100%;
  font-size: 13px;
  :-ms-input-placeholder {
    color: #7c7c7c !important;
  }
  :focus {
    border: 2px solid #759fe6;
  }
`

const InputS = styled.input`
  ${_ => InputBaseMixin}
  height: ${prop ('height')};
  width: ${prop ('width')};
  padding: ${prop ('padding')};
  :focus {
    padding: calc(${prop ('padding')} - 1px);
  }
`

export const InputAuto = deconstructProps (({
  theRef,
  height='35px',
  width='200px',
  padding='9px',
}) => withDisplayName ('InputAuto') (
  props => <InputS {...props} ref={theRef} height={height} width={width} padding={padding}/>,
))

export const Input = (props) => <InputAuto {...props}
  autoComplete='off'
  autoCorrect='off'
  autoCapitalize='off'
  spellCheck='false'
/>

const TextAreaS = styled.textarea`
  ${_ => InputBaseMixin}
  height: ${prop ('height')};
  width: ${prop ('width')};
`

export const TextArea = deconstructProps (({
  theRef,
  height='70px',
  width='200px',
}) => withDisplayName ('TextArea') (
  props => <TextAreaS {...props} ref={theRef} height={height} width={width} />,
))

; `
Section is for the part under a heading.

Normally the left margin of Section, H1, and H2 is standardised at 45px, but in a few places we need
to override this.
`

const SectionS = styled.div`
  margin-left: ${prop ('withLeftMargin') >> ifTrueV ('45px', '0px')};
  cursor: text;
`

export const Section = deconstructProps (
  ({ withLeftMargin=true, }) => withDisplayName ('Section') (
    props => <SectionS withLeftMargin={withLeftMargin} {...props} />,
  )
)

export const SectionNLM = (props) => <Section withLeftMargin={false} {...props}/>

export const H2NLM = (props) => <H2 withLeftMargin={false} {...props}/>
export const H1NLM = (props) => <H1 withLeftMargin={false} {...props}/>

// --- this part is horrible.

export const HS = styled.div`
  cursor: text;
  font-weight: normal;
  ${({ withTopMargin, withLeftMargin, withBottomMargin, }) => [
    withTopMargin ? '16' : '0',
    withLeftMargin ? '45' : '0',
    withBottomMargin ? '10' : '0',
  ] | sprintfN (`
    margin-top: %spx;
    margin-left: %spx;
    margin-bottom: %spx;
  `)}
  margin-right: 45px;
  ${mediaQuery (
    mediaPhone ('color: black'),
    mediaTablet ('color: #999'),
  )}
`

export const HMS = styled (HS)`
  color: black;
`

export const H1S = styled (HS)`
  font-size: 20px;
`

export const H2S = styled (HS)`
  font-size: 14px;
  font-weight: ${prop ('withBoldFont') >> ifTrueV ('bold', 'normal')};
`

export const H1MS = styled (HMS)`
  font-size: 20px;
  margin-top: 8px;
  margin-bottom: 8px;
`

export const H2MS = styled (HMS)`
  margin-left: ${prop ('withLeftMargin') >> ifTrueV ('45px', '0px')};
  font-size: 14px;
  margin-top: 8px;
  margin-bottom: 8px;
`

export const H1M = (props) => <H1MS
  withLeftMargin={false}
  {...props}
/>

export const H2M = (props) => <H2MS
  withLeftMargin={false}
  {...props}
/>


export const H2 = deconstructProps (({
  withTopMargin=true, withLeftMargin=true,
  withBottomMargin=true, withBoldFont=false
}) => withDisplayName ('H2') (
  props => <H2S
    withLeftMargin={withLeftMargin}
    withTopMargin={withTopMargin}
    withBottomMargin={withBottomMargin}
    withBoldFont={withBoldFont}
    {...props}
  />,
))

export const H1 = deconstructProps (({
  withTopMargin=true, withLeftMargin=true,
  withBottomMargin=true,
}) => withDisplayName ('H1') (
  props => <H1S
    withLeftMargin={withLeftMargin}
    withTopMargin={withTopMargin}
    withBottomMargin={withBottomMargin}
    {...props}
  />,
))

// --- fakeDisabled means the onClick handler is called, but the button doesn't move when clicked.
// --- it's useful for e.g. letting the click event bubble to an outer comopnent.
const ButtonBaseS = styled.button`
  border: 1px solid black;
  border-radius: 2px;
  display: inline-block;
  ${prop ('fakeDisabled') >> ifTrueV (
    ``,
    `
    &:not(:disabled) {
      cursor: pointer;
      &:active {
        transition: all .03s;
        transform: translateY(1px) translateX(1px);
        opacity: 0.8;
      }
    }
    `,
  )}
  &:focus {
    outline: none;
  }
`

export const ButtonS = styled (ButtonBaseS)`
  background: #e5e5e5;
  padding: 4px;
  position: relative;
  opacity: 0.5;
  &:not(:disabled) {
    opacity: 1.0;
    &:active {
      border-style: solid;
    }
  }
`

export const Button = (props) => <ButtonS type='submit' {...props}>
  <div>
  {props.children}
</div>
  {/* <div className='x__overlay'/> */}
</ButtonS>

export const Button2 = styled (ButtonBaseS)`
  border-radius: 5px;
  padding: 7px;
`

const ExternalLinkS = styled.span`
`

const HoverText = styled.a`
  ${prop ('noColor') >> ifTrueV (
    'color: inherit', 'color: #56a',
  )};
  transition: color 200ms;
  text-decoration: none;
  :hover {
    color: #333;
    border-bottom: 1px solid #ddd;
  }
`

export const ExternalLink = ({ url, noColor=false, children, }) => <ExternalLinkS>
  <HoverText
    noColor={noColor}
    href={url}
    target='_blank'
  >
    {children}
  </HoverText>
</ExternalLinkS>

const RouterLinkS = styled (Link)`
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
`

const RouterLinkDarkS = styled (RouterLinkS)`
  color: black;
`

export const BlueLink = styled (Link)`
  color: darkblue;
`

export const RouterLink = RouterLinkS
export const RouterLinkDark = RouterLinkDarkS

export const LegacyCss = (props) => <div className='legacy' {...props}/>

const TextDivS = styled.div`
  cursor: text;
`

export const TextDiv = (props) => <TextDivS {...props}/>

const SpinnerS = styled.div`
  display: flex;
  height: 100%;
  width: 100%;
  justify-content: center;
  align-items: center;
  color: #370707;
  .x__kid-t, .x__kid-d, .x__kid-m {
    position: relative;
  }
  ${mediaQuery (
    mediaPhone (`
      .x__kid-t, .x__kid-d { display: none; }
      .x__kid-m { display: block; top: -20%; }
    `),
    mediaTablet (`
      .x__kid-m, .x__kid-d { display: none; }
      /* --- why was the left necessary?
      .x__kid-t { display: block; top: -18%; left: -10%; }
      */
      .x__kid-t { display: block; top: -18%; }
    `),
    mediaDesktop (`
      .x__kid-m, .x__kid-t { display: none; }
      /* --- why was the left necessary?
      .x__kid-d { display: block; top: -10%; left: -10%; }
      */
      .x__kid-d { display: block; top: -10%; }
    `),
  )}
`

export const BrowseSpinner = ({ shrink=1, }) => lets (
  () => shrink * 1.3,
  (shrinker) => <SpinnerS>
    <div className='x__kid-m'>
      <SpinnerComet size={20 / shrinker} spinning={true}/>
    </div>
    <div className='x__kid-t'>
      <SpinnerComet size={30 / shrinker} spinning={true}/>
    </div>
    <div className='x__kid-d'>
      <SpinnerComet size={30 / shrinker} spinning={true}/>
    </div>
  </SpinnerS>,
)

const linkBaseMixin = `
  text-decoration: underline;
  cursor: pointer;
`

const linkTheme2Mixin = themeColor2 | sprintf1 (`
  color: %s;
  ${linkBaseMixin}
`)

const linkMixin = sprintf1 (`
  color: darkblue;
  ${linkBaseMixin}
`)

export const LinkLikeTheme2 = styled.span`
  ${linkTheme2Mixin}
`

export const LinkTheme2 = styled (Link)`
  ${linkTheme2Mixin}
`

export const LinkLike = styled.span`
  ${linkMixin}
`

// --- trivial styled component to allow passing ref.
export const Div = styled.div`
`

const ButtonsRowMS = styled.div`
  ${prop ('disabled') >> whenTrueV (
    'opacity: 0.5',
  )};
  .x__inner {
    display: flex;
    justify-content: space-between;
    align-items: center;
    ${prop ('doWrap') >> ifTrueV (
      'flex-wrap: wrap',
      'flex-wrap: nowrap',
    )};
    width: 100%;
    margin-left: auto;
    margin-right: auto;
    // --- create vertical gutter when wrapping.
    margin-top: -10px;
    > * {
      margin-top: 10px;
    }
    .x__button-wrapper {
      margin-left: auto;
      margin-right: auto;
    }
  }
`

// --- @todo withButtonsWrapper is ugly.
// --- @todo no reason to deconstruct twice.
export const ButtonsRowM = deconstructProps (
  ({ children, }) => deconstructProps (
    ({ withButtonsWrapper=true, doWrap=true, }) =>
       withDisplayName ('ButtonsRowM') (
         (props) => <ButtonsRowMS {...props} doWrap={doWrap}>
           <div className='x__inner'>
             {withButtonsWrapper ?
               (children | mapX (
                 (child, idx) => <div key={idx} className='x__button-wrapper'>
                   {child}
                 </div>
               )) : children
             }
           </div>
          </ButtonsRowMS>,
       ),
  ),
)

const ButtonMS = styled (Button2)`
  ${prop ('selected') >> ifTrueV (
    `
      background: #44A;
      color: white;
    `,
    `
      background: #FFF;
      color: black;
    `,
  )}
  ${prop ('greyed') >> ifTrueV (
    'opacity: 0.5;', '',
  )}
  width: ${prop ('width')};
  flex: 1 0 auto;
`

export const ButtonM = ({ text, selected, disabled, greyed, onClick, width='100px', }) =>
  <ButtonMS
    selected={selected}
    disabled={disabled}
    fakeDisabled={true}
    greyed={greyed}
    onClick={onClick}
    width={width}
  >
    {text}
  </ButtonMS>
