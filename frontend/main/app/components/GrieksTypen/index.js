import {
  pipe, compose, composeRight,
} from 'stick-js/es'

import React from 'react'
import styled from 'styled-components'

import { H2, Section, H2NLM, SectionNLM, } from '../shared'

import configure from 'alleycat-js/es/configure'
import { withDisplayName, } from 'alleycat-js/es/react'
import config from '../../config'

export default withDisplayName ('GrieksTypen') (
  ({ toetsenbordLocation = 'rechtsboven', }) => <div>
    <H2NLM>Grieks typen</H2NLM>
    <SectionNLM>
      Grieks typen kan met het Latijnse alfabet - voor de transliteratie raadpleeg je het toetsenbordje { toetsenbordLocation }. Je kunt ook Unicode gebruiken om Grieks te typen.
    </SectionNLM>
  </div>,
)
