import {
  pipe, compose, composeRight,
  map,
  sprintfN,
  tap,
  timesV, join, prop,
  not,
  side1,
} from 'stick-js/es'

import React, { PureComponent, } from 'react'

import styled from 'styled-components'

import configure from 'alleycat-js/es/configure'
import { logWith, nAtATime, zipWith3, mapX, } from 'alleycat-js/es/general'
import { ifTrueV, ifEquals, whenTrueV, } from 'alleycat-js/es/predicate'
import { compMeth, setState, whyUpdateMeth, } from 'alleycat-js/es/react-legacy'
import { mediaQuery, } from 'alleycat-js/es/styled'

import KeyboardExplanation from '../../components/KeyboardExplanation'
import { RouterLink, } from '../../components/shared'

import LogoSmall from '../../components/svg/LogoSmall'
import BurgerMenu from '../../components/svg/BurgerMenu'

import { mediaPhone, mediaTablet, mediaDesktop, } from '../../common'
import config from '../../config'

const configTop = config | configure.init

const keyboardImg = 'images.keyboard' | configTop.get
const debugRender = 'debug.rerender' | configTop.get

const headerImg = configTop.get (['images', 'header'])
const menuPageOverImg = configTop.get (['images', 'menuPageOver'])
const headerImgSmall = configTop.get (['images', 'headerSmall'])

const HeaderS = styled.div`
  position: relative;
  z-index: 11;
  ${mediaQuery (
    mediaPhone ('height: 50px;'),
    mediaTablet ('height: 129px;'),
  )}
  .x__main-m {
    ${mediaQuery (
      mediaPhone ('display: inherit;'),
      mediaTablet ('display: none;'),
    )}
  }
  .x__main-d {
    position: relative;
    ${mediaQuery (
      mediaPhone ('display: none;'),
      mediaTablet (`
        display: inherit;
        .x__header-img {
          margin-left: -133px;
        }
      `),
      mediaDesktop (`
        width: 900px;
        .x__header-img {
          margin-left: 0px;
        }
      `),
    )}
  }
  .x__header-img {
    cursor: pointer;
  }
`

const ToolbarS = styled.div`
  ${mediaQuery (
    // mediaPhone ('height: 100px;'),
    mediaTablet ('width: 100%;'),
    mediaDesktop ('width: 100%;'),
  )}
  position: relative;
  margin-top: -25px;
  overflow-y: hidden;
  padding-left: 35px;
  height: 25px;
  .x__kb {
    display: inline-block;
    height: 100%;
    position: absolute;
    right: 30px;
    cursor: pointer;
    transition: color 200ms ease-in;
  }
.x__button-size-small {
    font-size: 10px;
    padding: 7px;
    width: 20px;
    display: inline-block;
    height: 100%;
    position: absolute;
    right: 115px;
    cursor: pointer;
    transition: color 200ms ease-in;
    :hover {
      color: #FFF;
      background: url(${menuPageOverImg});
    }
  }
.x__button-size-medium {
    font-size: 13px;
    padding: 5px;
    padding-top: 4px;
    width: 20px;
    display: inline-block;
    height: 100%;
    position: absolute;
    right: 98px;
    cursor: pointer;
    transition: color 200ms ease-in;
    :hover {
      color: #FFF;
      background: url(${menuPageOverImg});
    }
  }
.x__button-size-big {
    width: 20px;
    font-size: 18px;
    padding: 5px;
    padding-top: 0px;
    margin-top: -1px;
    display: inline-block;
    height: 100%;
    position: absolute;
    right: 82px;
    cursor: pointer;
    transition: color 200ms ease-in;
    :hover {
      color: #FFF;
      background: url(${menuPageOverImg});
    }
  }
`

const MenuItemLinkS = styled (RouterLink)`
  display: inline-block;
  height: 25px;
  padding: 5px 10px;
  padding-top: 4px;
  transition: color 200ms ease-in;
  font-weight: bold;
  font-size: 11px;
  :hover {
    color: #FFF;
    background: url(${menuPageOverImg});
  }
`

const menuItemList = [
  ['Home', '/'],
  ['Geavanceerd zoeken', '/advanced-search'],
  ['Hulp', '/hulp'],
  ['Problemen & feedback', '/feedback'],
  ['Colofon', '/colofon']
]

const MenuItem = ({ menuItem: [name, url] }) =>
  <MenuItemLinkS to={url}>
    {name}
  </MenuItemLinkS>

const SizeButtonS = styled.div`
  ${props => props.selected ? 'color: #FFF' : 'color: #bbb'};
`

const SizeButton = ({ size, onClick, selected }) => {
  const className = ['x__button-size-', size].join('')
  const onSelect = () => onClick(size)
  return <SizeButtonS
    selected={selected}
    onClick={onSelect}
    className={className}
  >
  A
</SizeButtonS>
}

const SizeButtons = ({ onClick, fontSize }) =>
  ['small', 'medium', 'big'].map((size,idx) =>
    <SizeButton
      selected={fontSize === size}
      size={size}
      onClick={onClick}
      key={idx}
    />
  )

const Toolbar = ({ onKeyboardClick, sizeSelected, showFontsizer, fontSize, }) => <ToolbarS>
  {menuItemList.map((menuItem, idx) => <MenuItem
    menuItem={menuItem}
    key={idx}
  />
  )}
  {showFontsizer && <SizeButtons onClick={sizeSelected} fontSize={fontSize}/>}
  <div className='x__kb' onClick={onKeyboardClick}>
    <img src={keyboardImg}/>
  </div>
</ToolbarS>

const ToolBarMS = styled.div`
  height: 50px;
  background: #006;
  .x__item {
    flex: 0 0 20px;
    :first-child {
      margin-left: 7px;
    }
    :last-child {
    }
  }
  .x__wrapper {
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 89%;
    height: 100%;
    margin: auto;
  }
`

const KeyboardDropdownS = styled.div`
  display: ${prop ('show') >> ifTrueV ('block', 'none')};
  position: absolute;
  top: 100%;
  right: 3px;
  z-index: 10;
  box-shadow: 1px 1px 13px #999;
  height: 480px;
  width: 335px;
`

export const KeyboardDropdown = ({ show, close, }) => <KeyboardDropdownS show={show}>
  <KeyboardExplanation showClose={true} close={close}/>
</KeyboardDropdownS>

const LogoSmallWrapperS = styled.div`
  // margin-left: 20px;
  // svg { float: left; }
`

const BurgerMenuWrapperS = styled.div`
  cursor: pointer;
  width: 55px;
  height: 100%;
  display: flex;
  svg {
    position: relative;
    left: 8px;
    margin: auto;
  }
`

const ToolBarM = ({ onHomeClick, onMenuIconClick, menuMOpen=null, pullDownPosition, }) => <ToolBarMS>
  <div className='x__wrapper'>
    <div className='x__item'>
      <RouterLink to='/' onClick={onHomeClick}>
        <LogoSmallWrapperS>
          <LogoSmall height={28}/>
        </LogoSmallWrapperS>
      </RouterLink>
    </div>
    <div className='x__item'>
      <BurgerMenuWrapperS onClick={onMenuIconClick} isOpen={menuMOpen}>
        {/* undefined, true, false */}
        <BurgerMenu height={25} toState={menuMOpen}/>
      </BurgerMenuWrapperS>
    </div>
  </div>
</ToolBarMS>

export default class Header extends PureComponent {
  state = {
    showKeyboardDropdown: false,
  }

  closeKeyboardDropdown = () => this | setState ({
    showKeyboardDropdown: false,
  })

  onKeyboardClick = () => this | compMeth (
    (_, { showKeyboardDropdown, }) => this | setState ({
      showKeyboardDropdown: showKeyboardDropdown | not,
    }),
  )

  componentDidUpdate = this | whyUpdateMeth ('Header', debugRender)

  render () {
    const { props, state, onKeyboardClick, closeKeyboardDropdown, } = this
    const { showFontsizer, menuMOpen, onMenuMIconClick, onHomeClickM, sizeSelected, fontSize, } = props
    const { showKeyboardDropdown, pullDownPosition, } = state

    return <HeaderS>
      <div className='x__main-m'>
        <ToolBarM
          onHomeClick={onHomeClickM}
          onMenuIconClick={onMenuMIconClick}
          menuMOpen={menuMOpen}
        />
      </div>
      <div className='x__main-d'>
        <RouterLink to='/'>
          <img
            className='x__header-img'
            src={headerImg}
          />
        </RouterLink>
        <Toolbar
          onKeyboardClick={onKeyboardClick}
          sizeSelected={sizeSelected}
          showFontsizer={showFontsizer}
          fontSize={fontSize}
        />
        <KeyboardDropdown
          show={showKeyboardDropdown}
          close={closeKeyboardDropdown}
        />
      </div>
    </HeaderS>
  }
}
