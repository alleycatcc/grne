import {
  pipe, compose, composeRight,
  not, lets,
  mergeM,
  tap, whenTrue,
  ok, notOk,
  recurry,
} from 'stick-js/es'

import React, { PureComponent, } from 'react'

import ReactAutosuggest from 'react-autosuggest';
import styled from 'styled-components';

import { then, thenTap, recover, resolveP, } from 'alleycat-js/es/async'
import configure from 'alleycat-js/es/configure'
import { PromiseCancellation, setTimeoutOn, logWith, mapX, encodeFormData, } from 'alleycat-js/es/general'
import { ifTrueV, isEmptyList, ifEmptyList, } from 'alleycat-js/es/predicate'
import { createRef, compMeth, setState, toggleStateValue, } from 'alleycat-js/es/react-legacy'
import { mediaQuery, } from 'alleycat-js/es/styled'

import { spinner, } from '../../alleycat-components'

import { mediaPhone, mediaTablet, mediaDesktop, } from '../../common'
import config from '../../config'

const setStateIf = recurry (3) (
  x => o => self => x | whenTrue (
    () => self | setState (o),
  )
)

const SpinnerTextblocks = 'textblocks' | spinner

const SpinnerS = styled (SpinnerTextblocks)`
  display: inline-block;
  padding-left: 4%;
  padding-top: 1px;
  position: relative;
  top: 5px;
`

const SearchFieldS = styled.div`
  display: inline-block;
  width: 100%;
  white-space: nowrap;
`

const delay = ms => new Promise ((res, _) => ms | setTimeoutOn (res))

const ListItemS = styled.div`
  position: auto;
  padding-left: 10px;
  font-size: 13px;
  font-family: Source Sans Pro;
  height: 22px;
`

export const AutosuggestS = styled.div`
  display: inline-block;
  ${mediaQuery (
    mediaPhone (`
     width: 98%;
     max-width: 270px;
      `),
    mediaTablet (`
      max-width: none;
      width: 82%;
      `),
  )};
  .react-autosuggest__container {
	position: relative; }
  .react-autosuggest__input {
    ${mediaQuery (
      mediaPhone (`
       font-size: 15px;
      `),
      mediaTablet (`
        font-size: 13px;
      `),
      mediaDesktop ('font-size: 13px'),
    )};
    border: 1px solid #7c7c7c;
    position: relative;
    width: 100%;
    height: 33px;
    color: black;
    z-index: 0;
    border-radius: 3px;
    outline: none;
    padding: 9px;
    cursor: text;
    :-ms-input-placeholder {
      color: #7c7c7c !important;
    }
  }
  .react-autosuggest__input--focused {
    border: 2px solid #759fe6;
    padding: 8px;
  }
  .react-autosuggest__input--not-focused {
  }
  .react-autosuggest__suggestions-container {
    display: none;
  }
  .react-autosuggest__suggestions-container--open {
	display: block;
    border: 1px solid #7c7c7c59;
    border-radius: 2px;
    box-shadow: 0 1px 333 rgba(0, 0, 0, 0.1);
    background: rgba(255, 255, 255, 0.9);
    padding: none;
    font-size: 80%;
    overflow: none;
    background-color: white;
    // --- based on 'enneakaieikosikaieptakosioplasiakis'
    // and 'παππαπαππαπαῖ (παππαπαππαπαῖ)'.
    width: 100%;
    transition: opacity .3s;
    z-index: 2;
    position: absolute;
    ${mediaQuery (
      mediaTablet('top: 33px;'),
    ) }
  }
  .react-autosuggest__suggestions-list {
	margin: 0;
	padding: 0;
	list-style-type: none; }
  .react-autosuggest__suggestion {
    cursor: pointer;
    padding: 3px; }
  .react-autosuggest__suggestion--highlighted {
    background-color: #333;
    color: white;
    }
`

export const Autosuggest = (props) => <AutosuggestS>
  <ReactAutosuggest {...props}/>
</AutosuggestS>

export default class SearchField extends React.Component {
  static getDerivedStateFromProps (props, state) {
    const { initialValue, } = props
    const { valueCopied, } = state
    if (notOk (initialValue) || valueCopied) return null
    return {
      value: initialValue,
      valueCopied: true,
    }
  }

  theRefs = {
    wrapper: createRef (),
  }
  latestPromise = void 8
  mounted = false

  state = {
    value: '',
    valueCopied: false,
    suggestions: [],
    pending: false,
  }

  retrieveData (searchtext) {
    const setPending = _ => this | setState ({ pending: true, })
    const job = setTimeout (setPending, 200)
    const type = this.props.type
    const data = {
      // @todo config
      limit: 15,
      mode: type === 'lemma' ? 'lemma' : 'pars',
      query: searchtext,
    }
    const options = {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: data | encodeFormData,
    }
    const url = '/api/autocomplete'

    const fetchPromise = fetch (url, options)

    this.latestPromise = fetchPromise

    return fetchPromise
      // | then ((res) => delay (3000) | then (() => res))
      | then ((response) => {
        const isLatest = fetchPromise === this.latestPromise
        if (!isLatest) throw new PromiseCancellation ()
        return response.json ()
      })
      | thenTap (_ => clearTimeout (job))
      // --- we use a timeout to catch potentially straggling calls.
      // --- (we should really use a saga here).
      // --- it can happen that pending gets set to true after the last promise has resolved, possibly
      // because of a batched setState.
      | thenTap (_ => 300 | setTimeoutOn (
        () => this | setStateIf (this.mounted) ({ pending: false, }),
      ))
  }

  onChange = (event, { newValue, }) => {
    this.setState({
      value: newValue
    });
  }

  onSuggestionsFetchRequested = ({ value }) => {
    const type = this.props.type
    this.retrieveData (value)
      | then (response => {
        const mapper = type === 'lemma' ?
          ((([id, form, _], idx) => ({ idx: idx, id: id, label: form }))) :
          ((word, idx) => ({ idx: idx, id: idx, label: word }))

        const suggestions = response | mapX (mapper)
        if (!this.mounted) return
        this.setState ({ suggestions })
      })
      | recover ((err) => {
        // --- ok - we cancelled a promise.
        if (err instanceof PromiseCancellation) {}
        // --- really an error.
        else console.error (err)
      })
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  onSuggestionSelected = (_, { suggestion, suggestionValue, }) => {
    const { history, type, onSelectResult, } = this.props
    const { theRefs: { wrapper: { current: ref, }}} = this
    const lemmaId = type === 'lemma' ? suggestion.id : null

    this.setState({
      value: suggestionValue
    })

    // --- force the (Android) soft keyboard to hide (blur doesn't work).
    // --- necessary for browseview.
    lets (
      () => ref.querySelector ('input'),
      (input) => input.setAttribute ('readonly', 'readonly'),
      (input) => 0 | setTimeoutOn (
        () => input.removeAttribute ('readonly')
      ),
    )

    // --- the middle argument is the text to be restored when populating a field with the last
    // search value.
    onSelectResult (lemmaId, suggestionValue, suggestionValue)
  }

  getSuggestionValue = suggestion => suggestion.label;

  componentDidMount = () => this | mergeM ({
    // --- we use this to avoid setState on an unmounted component, allowing for a small memory
    // leak.
    // --- a better way would be to use a saga.
    mounted: true,
  })

  componentWillUnmount = () => this | mergeM ({
    mounted: false,
  })

  renderSuggestion = (suggestion,) => {
    return <ListItemS>
      {suggestion.label}
    </ListItemS>
  }

  render() {
    const { autoFocus, placeholder, onFocus, onBlur, onSelectResult } = this.props;
    const { value, suggestions, pending, } = this.state;
    const { theRefs: { wrapper: ref, }} = this

    const inputProps = {
      placeholder,
      value,
      onFocus,
      onBlur,
      onChange: this.onChange,
      autoComplete: 'off',
      autoCorrect: 'off',
      autoCapitalize: 'off',
      spellCheck: 'false',
      autoFocus,
    };

    return (
      <SearchFieldS
        ref={ref}
      >
        <Autosuggest
          suggestions={suggestions}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          getSuggestionValue={this.getSuggestionValue}
          renderSuggestion={this.renderSuggestion}
          onSuggestionSelected={this.onSuggestionSelected}
          inputProps={inputProps}
          highlightFirstSuggestion={true}
        />
        <SpinnerS spinning={pending} fontSize='13px'/>
      </SearchFieldS>
    );
  }
}
