import {
  pipe, compose, composeRight,
  map, flip, tap, bindProp, path,
  take, head, dot2, side2,
  noop, bindPropTo, defaultTo,
  ifTrue, ifFalse, whenOk, ok,
  side1, condS, guardV, otherwise, eq, lets, letS,
  multiply, concat, prop, sprintf1, always,
  ifPredicate, sprintfN, minus, plus,
  id, ampersandN, appendToM, not, ifOk, merge,
  die, guard, cond, T, F,
  deconstruct2,
  deconstruct,
} from 'stick-js/es'

import React, { PureComponent, memo, } from 'react'

import styled from 'styled-components'

import { Just, Nothing, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { clss, } from 'alleycat-js/es/dom'
import { iwarn, mapX, reduceX, logWith, reverse, ierror, notBelow, notAbove, defaultToV, repluckAll, setTimeoutOn, toString, } from 'alleycat-js/es/general'
import { ifTrueV, ifEquals, isEmptyList, isNotEmptyList, whenTrueV, } from 'alleycat-js/es/predicate'
import { withResizeListener, createRef, compMeth, setState, incrementStateValueMeth, whyUpdateMeth, whyUpdate, setStateValue, setStateValueMeth, } from 'alleycat-js/es/react-legacy'
import { mediaQuery, } from 'alleycat-js/es/styled'

import EntryList from '../../components/EntryList/'
import LemmaList from '../../components/LemmaList/'
import ListIcon from '../../components/svg/List'
import PresentationTabSVG from '../../components/svg/PresentationTab'
import SearchIconSimple from '../../components/svg/SearchIconSimple'
import SearchField from '../../components/SearchField'
import { BlueLink, BrowseSpinner, Button2, ButtonsRowM, ButtonM, } from '../../components/shared'

import Home from '../../containers/Home'
import KruimelPad from '../../containers/KruimelPad'
import Left from '../../containers/Left/'
import ParsResultsTable from '../../containers/ParsResultsTable'

import { mediaPhone, mediaTablet, mediaDesktop, isMobileWidth, browseLinkForRouter, browseToId, debugDev, disableSingle, browseTo, onKruimelItemClickStandard, } from '../../common'
import config from '../../config'
import { envIsPrd, } from '../../env'
import {
  BrowseModeLemmataExplore,
  BrowseModeLemmataSingle,
  BrowseModeLemmataSingleModeInit,
  BrowseModeLemmataNormal,
  browseModeLemmataIsSingle,
  fetchIsPrepend,
} from '../../types'

const configTop = config | configure.init

const debugRender = 'debug.rerender' | configTop.get

const showIconFrameSearch = false
const showIconFrameList = false

const [entriesPosition, entriesWidth] = lets (
  () => 'panelWidth' | configTop.get | String,
  (pos) => pos | sprintf1 ('calc(100%% - %spx)'),
  (pos) => pos | sprintf1 ('%spx'),
  (_, p, w) => [p, w],
)

const TopS = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
`

const IconWrapperMS = styled.div`
  ${prop ('selected') >> ifTrueV ('', '')};
  left: ${prop ('leftShift') >> ifOk (
    String >> concat ('px'),
    () => '0px',
  )};
  width: 41px;
  height: 41px;
  position: relative;
  cursor: pointer;
  > * {
    margin: auto;
    width: 100%;
    top: 50%;
    left: 0;
    position: absolute;
    transform: translateY(-50%);
  }
`

// --- top (when showSearchFields is false) must match height of HomeWrapperS.
// --- don't use a percentage, because when the keyboard is visible the height will decrease and the
// value will change.

const TopMS = styled (TopS)`
  ${prop ('showSearchFields') >> ifTrueV (
    `
      top: 0px;
      transition: top 100ms;
    `,
    `
      top: -300px;
      transition: top 100ms;
    `,
  )}
  position: relative;
  .x__buttons {
    position: absolute;
    left: -100px;
	button {
	  padding: 10px;
	  display: inline-block;
	  border: 1px solid #999999;
	}
  }
`

const MainS = styled.div`
  height: 100%;
  display: flex;
  align-items: stretch;
  align-content: stretch;
`

const MainMS = styled.div`
  height: 100%;
`

// --- disable transitions: they are choppy due to lemma re-renders (unlike MenuM).
const EntriesMS = styled.div`
  ${prop ('show') >> ifTrueV (
    `
      left: ${entriesPosition};
      // transition: left .3s;
    `,
    `
      left: 100%;
      // transition: left .1s;
    `,
  )}
  overflow-y: scroll;
  -webkit-overflow-scrolling: touch;
  background: white;
  z-index: 9;
  box-shadow: 1px 1px 13px #999;
  border-top: 1px solid grey;
  border-bottom: 1px solid grey;
  position: absolute;
  height: 100%;
  width: ${entriesWidth};
  top: 0;
  > * {
    padding-left: 25px;
    padding-right: 25px;
  }
`

const RightWrapperSBase = styled.div`
  overflow-y: hidden;
  border-bottom: 1px solid black;
  position: relative;
  z-index: 9;
  border-top-width: 0px;
  margin-left: 0px;
  margin-right: 45px;
`

const RightWrapperS = styled (RightWrapperSBase)`
  flex: 1 0 1px;
`

const RightWrapperMS = styled (RightWrapperSBase)`
  height: 100%;
  margin-left: 10px;
  margin-right: 10px;
  border-top: 1px solid black;
`

const BrowseS = styled.div`
  flex: 1 0 1px;
  max-height: calc(90% - 90px);
`

const BrowseMS = styled.div`
  flex: 1 0 calc(100% - 55px);
  position: relative;
  overflow-x: hidden;
  overflow-y: hidden;
`

const SearchFieldWrapperS = styled.div`
  ${mediaQuery (
    mediaPhone ('display: none'),
    mediaTablet (`
      display: block;
      .x__lemma {
        width: 200px;
      }
      .x__pars {
        width: 200px;
        margin-left: 0px;
      }
    `),
    mediaDesktop (`
      .x__lemma {
        width: 207px;
      }
      .x__pars {
        width: 207px;
        margin-left: 0px;
      }
    `),
  )}
  > * {
    vertical-align: top;
  }
  position: absolute;
  z-index: 10;
  .x__field {
    display: inline-block;
  }
`

const ControlsS = styled.div`
  position: relative;
  margin-left: 45px;
  margin-right: 45px;
  ${mediaQuery (
    mediaPhone (`
      flex: 0 0 55px;
    `),
    mediaTablet (`
      flex: 0 0 90px;
    `),
    mediaDesktop(`
      flex: 0 0 90px;
      margin-bottom: -10px;
    `),
  )}
`

const Controls = ({ history, isMobile, parsResultSelectedDispatch, }) =>
  <ControlsS>
    <KruimelPad
      history={history}
      isMobile={isMobile}
      alwaysMakeLinks={false}
      onItemClick={onKruimelItemClickStandard (history)}
    />
    <SearchFieldWrapperS>
      <div className='x__field x__lemma'>
        <SearchField
          type='lemma'
          placeholder='zoekterm woordenboek'
          history={history}
          // @todo duplicated in Home
          onSelectResult={(lemmaId, ..._) => browseToId (history, lemmaId)}
        />
      </div>
      <div className='x__field x__pars'>
        <SearchField
          type='pars'
          placeholder='determineer een vorm'
          history={history}
          // @todo duplicated in Home
          onSelectResult={(_, __, suggestion) => suggestion | parsResultSelectedDispatch}
        />
        <ParsResultsTable
          originatorKey='searchField'
          ambient='browse'/>
      </div>
    </SearchFieldWrapperS>
  </ControlsS>

const PresentationTab = ({ text, pos, hide=false, selected, onClick, }) =>
  <PresentationTabS
    show={!hide}
    pos={pos}
    selected={selected}
    onClick={hide ? noop : onClick}
  >
    <div className='x__text'>
      {text}
    </div>
    <PresentationTabSVG
      verticalScale={1.0}
      height={25}
      selected={selected}
      strokeUnselected={tabDisabledColor}
    />
  </PresentationTabS>

const PresentationTabRowS = styled.div`
  flex: 0 0 27px;
  position: relative;
  .x__wrapper {
    width: 380px;
    position: absolute;
    right: 0px;
  }
`

// --- This is a div which comes above the lines which stick out of the tab svg (covering it up
// and giving it a sharp edge), and below the text of the lemmas.
// --- RightS has a z-index of 6 and giving this one a z-index of -1 works but I'm not sure why.

const TabBlot = styled.div`
  position: absolute;
  z-index: -1;
  width: 370px;
  height: 100px;
  background: white;
  top: 1px;
  right: 10px;
`

const rightBorderLeftWidth = envIsPrd
  ? 'calc(100% - 231px)'
  : 'calc(100% - 330px)'

const RightBorderLeft = styled.div`
  position: absolute;
  width: ${rightBorderLeftWidth};
  height: 1px;
  top: 0px;
  left: 0px;
  background: black;
`

const RightBorderRight = styled.div`
  position: absolute;
  width: 27px;
  height: 1px;
  top: 0px;
  right: 0px;
  background: black;
`

const tabDisabledColor = '#999'
const presentationTabSSelected = () => `
  z-index: 8;
  color: brown;
`
const presentationTabSUnselected = ({ pos, show, }) => `
  z-index: ${pos + 5};
  ${show ? 'cursor: pointer;' : ''}
`
const PresentationTabS = styled.div`
  ${deconstruct2 (({ selected, }) => props => selected | ifTrue (
    () => presentationTabSSelected (props),
    () => presentationTabSUnselected (props),
  ))}
  display: inline-block;
  opacity: ${prop ('show') >> ifTrueV ('1', '0')};
  position: relative;
  top: 7px;
  left: ${prop ('pos') >> multiply (-22) >> sprintf1 ('%dpx')};
  width: 120px;
  .x__text {
    position: absolute;
    width: 100%;
    height: 100%;
    top: 2px;
    text-align: center;
    color: ${prop ('selected') >> ifTrueV ('black', tabDisabledColor)};
  }
`

// --- For disabling click events, onClickCapture generally works, but we are using native dom events on the abbreviations (cuz
// of dangerouslySetInnerHTML) and those still fire.
// --- So we use an 'overlay' div to block mouse events when we're in entries explore mode.

const LemmaListWrapperS = styled.div`
  ${prop ('disabled') >> ifTrueV (
    `
      opacity: 0.3;
      transition: opacity 200ms;
    `,
    `
      opacity: 1.0;
    `,
  )};
  position: relative;
  height: 100%;
`

const LemmaListOverlayS = styled.div`
  ${prop ('show') >> ifTrueV (
    'display: block', 'display: none',
  )};
  position: absolute;
  top: 2px;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 200;
  opacity: 0;
  cursor: not-allowed;
`

class Right extends PureComponent {
  componentDidUpdate = this | whyUpdateMeth ('Right', debugRender)
  render = () => this | compMeth (
    ({
      lemmataReplacePending,
      history,
      isMobile,
      curEntryIdx,
      lemmata,
      selectedLemmaIdx,
      renderLemmaListKey,
      reductieModeSchool,
      reductieModeBeknopt,
      citGClicked,
      browseMode,
      onLemmaListClicked,
      onLemmataScroll,
      entriesExploreMode,
      onChangeBrowseMode,
      leavingAbridge,
      onLeavingAbridge,
      fontSize,
      showParsTable,
    }) => (lemmataReplacePending || !ok (selectedLemmaIdx)) | ifTrue (
      () => <BrowseSpinner/>,
      () => <LemmaListWrapperS
        disabled={!isMobile && entriesExploreMode}
      >
        <LemmaList
          history={history}
          isMobile={isMobile}
          curEntryIdx={curEntryIdx}
          entriesExploreMode={entriesExploreMode}
          lemmata={lemmata}
          selectedLemmaIdx={selectedLemmaIdx}
          renderLemmaListKey={renderLemmaListKey}
          reductieModeSchool={reductieModeSchool}
          reductieModeBeknopt={reductieModeBeknopt}
          citGClicked={citGClicked}
          browseMode={browseMode}
          onLemmaListClicked={onLemmaListClicked}
          onScroll={onLemmataScroll}
          onChangeBrowseMode={onChangeBrowseMode}
          onLeavingAbridge={onLeavingAbridge}
          leavingAbridge={leavingAbridge}
          fontSize={fontSize}
          showParsTable={showParsTable}
        />
        <LemmaListOverlayS show={!isMobile && entriesExploreMode}/>
      </LemmaListWrapperS>,
    )
  )
}

export class BrowseComponent extends PureComponent {
  componentDidUpdate = this | whyUpdateMeth ('BrowseComponent', debugRender)
  // --- BrowseComponent.
  render = () => this | compMeth (
    ({
      history,
      parsResultSelectedDispatch,
      doSchool,
      reductieMode,
      selectReductieMode,
      onScrollToEntry,
      selectedLemmaIdx,
      onSelectEntry,
      lemmataReplacePending,
      isMobile,
      curEntryIdx,
      lemmata,
      entriesExploreMode,
      entrySelectedOffsetTop,
      entrySetSelectedOffsetTopDispatch,
      renderLemmaListKey,
      citgWordClickedDispatch,
      fontSize,
      showParsTable,
      onClick,
      onLemmaListClicked,
    }) => <TopS onClick={onClick}>
      <Controls
        history={history}
        isMobile={isMobile}
        parsResultSelectedDispatch={parsResultSelectedDispatch}
      />
      <PresentationTabRowS>
        <div className='x__wrapper'>
          <PresentationTab text='school' hide={!doSchool} pos={0} selected={reductieMode === 'school'}
            onClick={selectReductieMode.school}
          />
          <PresentationTab text='standaard' pos={1} selected={reductieMode === 'standaard'}
            onClick={selectReductieMode.standaard}
          />
          <PresentationTab text='beknopt' pos={2} selected={reductieMode === 'beknopt'}
            onClick={selectReductieMode.beknopt}
          />
        </div>
      </PresentationTabRowS>
      <BrowseS>
        <MainS>
          <Left
            onScrollToEntry={onScrollToEntry}
            selectedLemmaIdx={selectedLemmaIdx}
            onSelectEntry={onSelectEntry}
            fontSize={fontSize}
            entrySelectedOffsetTop={entrySelectedOffsetTop}
            entrySetSelectedOffsetTopDispatch={entrySetSelectedOffsetTopDispatch}
          />
          <RightWrapperS>
            <Right
              lemmataReplacePending={lemmataReplacePending}
              history={history}
              isMobile={isMobile}
              curEntryIdx={curEntryIdx}
              lemmata={lemmata}
              selectedLemmaIdx={selectedLemmaIdx}
              renderLemmaListKey={renderLemmaListKey}
              reductieModeSchool={reductieMode === 'school'}
              reductieModeBeknopt={reductieMode === 'beknopt'}
              citGClicked={citgWordClickedDispatch}
              browseMode={BrowseModeLemmataNormal}
              onLemmaListClicked={onLemmaListClicked}
              onLemmataScroll={noop}
              onChangeBrowseMode={noop}
              onLeavingAbridge={noop}
              fontSize={fontSize}
              showParsTable={showParsTable}
              entriesExploreMode={entriesExploreMode}
            />
            <TabBlot/>
            <RightBorderLeft className='m--hide'/>
            <RightBorderRight className='m--hide'/>
          </RightWrapperS>
        </MainS>
      </BrowseS>
    </TopS>,
  )
}

const PresentationTabsRowMS = styled.div`
  flex: 0 0 50px;
  position: relative;
  top: 4px;
  width: 90%;
  margin: auto;
`

const VerticalCenterS = styled.div`
  position: relative;
  top: 50%;
  transform: translateY(-50%);
`

const VerticalCenter = ({ children, }) => <VerticalCenterS>
  <div>{children}</div>
</VerticalCenterS>

const PullDownS = styled.div`
  position: absolute;
  left: 50%;
  transform: translateX(-50%);
  width: 50%;
  height: 51px;
  background: beige;
  z-index: 12;
  transition: top 400ms;
  ${prop ('position') >> ifEquals ('up') (
    () => `
      top: -51px;
    `,
    () => `
      top: -30px;
    `,
  )}
  border: 1px solid #999999;
  font-weight: bold;
  color: blue;
  font-size: 20px;
  background: white;
  text-align: center;
  .x__tab {
    position: absolute;
    width: 30px;
    height: 30px;
    top: 100%;
    left: 50%;
    transform: translateX(-50%) translateY(-50%);
    background: white;
    border: 1px solid grey;
  }
  .x__plus-sign {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translateX(-50%) translateY(-50%);
    border: 1px solid grey;
    border-radius: 10000px;
    width: 20px;
    height: 20px;
    div {
      width: 100%;
      height: 100%;
      margin-top: -5px;
      color: grey;
    }
  }
`

const PullDown = ({ position, }) => <PullDownS position={position}>
  <div className='x__arrow'>
    ↓
  </div>
  <div className='x__tab'>
    <div className='x__plus-sign'><div>+</div></div>
  </div>
</PullDownS>

const ButtonWrapperM = styled.div`
  margin-left: 4px;
  margin-right: 4px;
`

// --- onClickExtra is called whether or not the buttons are disabled.

const presentationButtonM = (reductieMode, selectReductieMode, disableAll, onClickExtra) => (which, idx) => lets (
  () => reductieMode === which,
  (sel) => lets (
    () => sel || disableAll,
    (fakeDisabled) =>
      <ButtonWrapperM key={idx}>
        <ButtonM
          text={which}
          selected={sel}
          disabled={false}
          fakeDisabled={fakeDisabled}
          greyed={disableAll}
          onClick={() => {
            onClickExtra ()
            if (fakeDisabled) return
            selectReductieMode [which] ()
          }}
        />
      </ButtonWrapperM>,
  ),
)

const HomeWrapperS = styled.div`
  flex: 0 0 300px;
  overflow-y: scroll;
  &.x--pars-active {
    > .x__wrapper {
      height: calc(100% + 100px);
    }
  }
`

export class BrowseComponentM extends PureComponent {
  state = {
    browseMode: cond (
      [() => this.props.isMobile && disableSingle (), () => BrowseModeLemmataExplore],
      [() => this.props.isMobile, () => BrowseModeLemmataSingle (BrowseModeLemmataSingleModeInit)],
      [T, () => BrowseModeLemmataNormal],
    ),
    entryListRenderKey: 0,
    leavingAbridge: void 8,
    parsActive: false,
  }

  theRefs = {
    homeWrapper: createRef (),
    wrapper: createRef (),
  }

  onLeavingAbridge = this | setStateValueMeth ('leavingAbridge')

  onChangeBrowseMode = browseMode => this | compMeth ((
  ) => {
    this | setState ({
      browseMode,
    })
  })

  // --- we could set parsActive to false again on blur, but only if we first check if the pars
  // table is being shown.
  onParsFocus = () => this | compMeth (({}, {}, {
    theRefs: { homeWrapper: { current: ref, }}
  }) => this | setStateValue ('parsActive', true))

  onParsBlur = () => this | compMeth ((
    { showParsTable },
    {},
    {
      theRefs: { homeWrapper: { current: ref, }},
    },
  ) => {
    if (!showParsTable) this | setStateValue ('parsActive', false)
  })

  adjustEntryScroll = (amount) => this | compMeth ((
    { entriesFetchType, },
    {},
    { theRefs: { wrapper: { current: ref }}},
  ) => {
    if (entriesFetchType | fetchIsPrepend) ref.scrollTop = amount
  })

  onParsInActive = () => this | compMeth (({}, {}, {
    theRefs: { homeWrapper: { current: ref, }}
  }) => this | setStateValue ('parsActive', true))

  onClickShowEntries = () => this | compMeth ((
    { onClickShowEntries },
    { entryListRenderKey },
  ) => {
    this | setState ({
      entryListRenderKey: entryListRenderKey + 1,
    })
    onClickShowEntries ()
  })

  onClickShowSearchFields = () => this | compMeth (
    ({ onClickShowSearchFields, }) => onClickShowSearchFields (),
  )

  // onClickButtonsRow = () => this | setStateValue ('browseMode', BrowseModeLemmataExplore)
  onClickButtonsRow = () => this.onChangeBrowseMode (BrowseModeLemmataExplore)

  // --- mobile.
  onKruimelItemClick = (link) => () => this | compMeth ((
    { history, onClickShowSearchFields },
    {},
    { onChangeBrowseMode, }
  ) => {
      /* We don't know yet if it's fixed or abridge -- let the re-render figure it out. This also
       * works when choosing the 'selected' lemma again (it's not really selected when in explore
       * mode, but the selectedLemmaIdx thinks it is).
       *
       * This timeout is important: without it, clicking an item on the kruimelpad could lead to
       * a random word because the top animation is still happening. (This happens on older phones
       * like iPhone5). In the case of abridged mode you get totally stuck on the wrong word.
       *
       * If the kruimelpad is behaving strange after a click try changing this number.
       */
      browseTo (history, link)
      // --- hide the search fields immediately.
      // --- it's a bit ugly, but it's necessary for the case where the last selected lemma is
      // chosen again.
      onClickShowSearchFields ()
      700 | setTimeoutOn (() => {
        if (!disableSingle ())
          onChangeBrowseMode (BrowseModeLemmataSingle (BrowseModeLemmataSingleModeInit))
      })
    }
  )

  componentDidUpdate = () => this | compMeth ((
    {},
    { parsActive },
    { theRefs: { homeWrapper: { current: ref, }}}
  ) => {
    this | whyUpdate ('BrowseComponentM', debugRender)
    if (parsActive) ref.scrollTop = 1e4
  })

  // --- the showSearchFields flag is necessary because when you come to the browseview for the first time, the
  // LemmaList might not have the right height yet, and we don't want to accidentally see the search
  // fields.

  // --- BrowseComponentM
  render = () => this | compMeth ((
    {
      history,
      isMobile,
      parsResultSelectedDispatch,
      doSchool,
      reductieMode,
      selectReductieMode,
      onScrollToEntry,
      selectedLemmaIdx,
      onSelectEntry,
      lemmataReplacePending,
      curEntryIdx,
      lemmata,
      renderLemmaListKey,
      citgWordClickedDispatch,
      onLemmataScroll,
      showParsTable,
      entries,
      entriesAppending,
      entriesPrepending,
      entriesFetchType,
      showEntries,
      showSearchFields,
      onClick,
      onLemmaListClicked,
      entriesExploreMode,
      entrySelectedOffsetTop,
      entrySetSelectedOffsetTopDispatch,
      entriesIdxShift,
      entriesFetchChunkPrevClickedDispatch,
      entriesFetchChunkNextClickedDispatch,
    },
    {
      browseMode,
      entryListRenderKey,
      parsActive,
      leavingAbridge,
    },
    {
      onChangeBrowseMode,
      onLeavingAbridge,
      scrollToEntry,
      onClickShowEntries,
      onClickShowSearchFields,
      onClickButtonsRow,
      onKruimelItemClick,
      onParsFocus,
      onParsBlur,
      adjustEntryScroll,
      theRefs: {
        homeWrapper: homeWrapperRef,
        wrapper: wrapperRef,
      },
    },
  ) => lets (
    () => browseMode | whenOk (browseModeLemmataIsSingle),
    () => entries | isEmptyList,
    (isSingleMode, haveNoEntries) => [
      isSingleMode || haveNoEntries, // || showEntries,
      isSingleMode || haveNoEntries || showSearchFields,
      isSingleMode || showSearchFields,
    ],
    (_, haveNoEntries, [disableSearchButton, disableListButton, disablePresentationButtons]) =>
      <TopMS
        showSearchFields={showSearchFields}
        onClick={onClick}
      >
        <HomeWrapperS
          ref={homeWrapperRef}
          className={clss (
            parsActive && 'x--pars-active',
          )}
        >
          <div className='x__wrapper'>
            <Home
              history={history}
              isMobile={isMobile}
              plain={true}
              onParsFocus={onParsFocus}
              onParsBlur={onParsBlur}
              onKruimelItemClick={onKruimelItemClick}
            />
          </div>
        </HomeWrapperS>
        <PresentationTabsRowMS>
          <ButtonsRowM
            withButtonsWrapper={false}
            onClick={onClickButtonsRow}
            doWrap={false}
          >
            <IconWrapperMS
              leftShift={3}
              selected={showSearchFields}
              onClick={disableSearchButton ? noop : onClickShowSearchFields}
            >
              <SearchIconSimple
                withFrame={showIconFrameSearch}
                disabled={disableSearchButton}
                height={28}
                selected={showSearchFields}
              />
            </IconWrapperMS>
            {lets (
              () => presentationButtonM (
                reductieMode,
                selectReductieMode,
                disablePresentationButtons,
                onClickButtonsRow,
              ),
              (theTab) => ['standaard', 'beknopt'] | mapX (theTab),
            )}
            <IconWrapperMS
              selected={showEntries}
              onClick={disableListButton ? noop : onClickShowEntries}
            >
              <ListIcon
                withFrame={showIconFrameList}
                disabled={disableListButton}
                height={28}
                selected={showEntries}
              />
            </IconWrapperMS>
          </ButtonsRowM>
        </PresentationTabsRowMS>
        <BrowseMS>
          <MainMS>
            <RightWrapperMS>
              <Right
                lemmataReplacePending={lemmataReplacePending}
                history={history}
                isMobile={isMobile}
                curEntryIdx={curEntryIdx}
                lemmata={lemmata}
                selectedLemmaIdx={selectedLemmaIdx}
                renderLemmaListKey={renderLemmaListKey}
                reductieModeSchool={reductieMode === 'school'}
                reductieModeBeknopt={reductieMode === 'beknopt'}
                citGClicked={citgWordClickedDispatch}
                browseMode={browseMode}
                // @todo change name of dispatch to list?
                onLemmaListClicked={onLemmaListClicked}
                onLemmataScroll={onLemmataScroll}
                onChangeBrowseMode={onChangeBrowseMode}
                leavingAbridge={leavingAbridge}
                onLeavingAbridge={onLeavingAbridge}
                showParsTable={showParsTable}
                entriesExploreMode={entriesExploreMode}
              />
            </RightWrapperMS>
          </MainMS>
          <EntriesMS show={showEntries} ref={wrapperRef}>
            <EntryList
              standalone={true}
              exploreMode={entriesExploreMode}
              // @todo why was this necessary? And why does it cause a re-mount?
              // key={entryListRenderKey}
              entries={entries}
              entriesAppending={entriesAppending}
              entriesPrepending={entriesPrepending}
              fetchType={entriesFetchType}
              adjustEntryScroll={adjustEntryScroll}
              selectedOffsetTop={entrySelectedOffsetTop}
              setSelectedOffsetTopDispatch={entrySetSelectedOffsetTopDispatch}
              selectedLemmaIdx={selectedLemmaIdx + entriesIdxShift}
              onSelectEntry={onSelectEntry}
              fontSize={'medium'}
              entriesFetchChunkPrevClickedDispatch={entriesFetchChunkPrevClickedDispatch}
              entriesFetchChunkNextClickedDispatch={entriesFetchChunkNextClickedDispatch}
            />
          </EntriesMS>
        </BrowseMS>
      </TopMS>,
    ),
  )
}
