import {
  pipe, compose, composeRight,
  tap,
} from 'stick-js/es'

import React, { PureComponent, } from 'react'

import ImageGallery from 'react-image-gallery'
import "react-image-gallery/styles/css/image-gallery.css"

import styled from 'styled-components'

import { createRef, } from 'alleycat-js/es/react-legacy'
import { mediaQuery, } from 'alleycat-js/es/styled'

import { mediaPhone, mediaTablet, mediaDesktop, } from '../../common'
import configure from 'alleycat-js/es/configure'
import { logWith, } from 'alleycat-js/es/general'

import config from '../../config'

const configTop = config | configure.init

const hulpOneImg = configTop.get (['images', 'hulpOne'])
const hulpTwoImg = configTop.get (['images', 'hulpTwo'])
const hulpThreeImg = configTop.get (['images', 'hulpThree'])
const hulpFourImg = configTop.get (['images', 'hulpFour'])
const hulpFiveImg = configTop.get (['images', 'hulpFive'])
const hulpSixImg = configTop.get (['images', 'hulpSix'])
const hulpSevenImg = configTop.get (['images', 'hulpSeven'])
const hulpEightImg = configTop.get (['images', 'hulpEight'])
const hulpNineImg = configTop.get (['images', 'hulpNine'])
const hulpTenImg = configTop.get (['images', 'hulpTen'])

const HulpWrapperS = styled.div`
  position: relative;
  height: 100%;
  overflow-y: scroll;
  overflow-x: hidden;
  .image-gallery-left-nav {
    ${mediaQuery (
      mediaPhone ('top: 10%;'),
      mediaTablet ('top: 110px;'),
      mediaDesktop ('top: 130px;'),
    )}
  }
  .image-gallery-right-nav {
    ${mediaQuery (
      mediaPhone ('top: 10%;'),
      mediaTablet ('top: 110px;'),
      mediaDesktop ('top: 130px;'),
    )}
  }
  .image-gallery-index{
    background: white;
    color: black;
    position: absolute;
    width: 100%;
    text-align: center;
    ${mediaQuery (
      mediaPhone ('top: 2px; font-size: 16px; '),
      mediaTablet ('font-size: 29px; top: 4px;'),
      mediaDesktop ('font-size: 29px; top: 8px;'),
    )}
  }
  .image-gallery-index-separator{
    display: none;
  }
  .image-gallery-index-total{
    display: none;
  }
  .image-gallery-image {
    // --- override max-height from the library which causes the image to shrink.
    max-height: inherit !important;
  }
`

export default class Hulp extends PureComponent {
  theRefs = {
    gallery: createRef (),
  }

  render () {
    const { theRefs, } = this
    const { gallery: ref, } = theRefs

    const images = [
      {
        original: hulpOneImg,
      },
      {
        original:hulpTwoImg,
      },
      {
        original: hulpThreeImg,
      },
      {
        original:hulpFourImg,
      },
      {
        original:hulpFiveImg,
      },
      {
        original:hulpSixImg,
      },
      {
        original:hulpSevenImg,
      },
      {
        original:hulpEightImg,
      },
      {
        original:hulpNineImg,
      },
      {
        original:hulpTenImg,
      },
    ]
    return <HulpWrapperS>
      <ImageGallery
        ref={ref}
        items={images}
        showPlayButton={false}
        showThumbnails={false}
        showIndex={true}
        onClick={() => {ref.current.slideToIndex(ref.current.getCurrentIndex() + 1)}}
      />
    </HulpWrapperS>
  }
}
