import {
  pipe, compose, composeRight,
  always, lets, ifTrue,
} from 'stick-js/es'

import React from 'react'
import styled from 'styled-components'

import configure from 'alleycat-js/es/configure'
import { withDisplayName, } from 'alleycat-js/es/react'

import { H2, Section, H2NLM, SectionNLM, ExternalLink, } from '../shared'

import { mediaPhone, mediaTablet, } from '../../common'
import { grneEnv, envIsPrd, } from '../../env'
import config from '../../config'

const configTop = config | configure.init

const N = null | always

const UpdateS = styled.div`
  font-weight: bold;
`

const Env = () => lets (
  _ => grneEnv,
  env => envIsPrd | ifTrue (
    N,
    _ => <p>
      [Dit is de <b>{env}</b> omgeving]
    </p>,
  ),
)

export default withDisplayName ('OverHetWoordenboek') (
  (props) => <div>
    <H2NLM>Over het woordenboek</H2NLM>
    <SectionNLM>
      <Env />

      <p>
      Dit is de online editie van het woordenboek Grieks/Nederlands, een lexicon op Oudgriekse teksten
      van Homerus tot in de 2e eeuw n. Chr., onder hoofdredactie van Ineke Sluiter, Lucien van Beek, Ton Kessels en Albert Rijksbaron.
      </p>

      <p>
      Helaas is ons hoofdredactielid van het eerste uur, Albert Rijksbaron, op 2 oktober 2023 op 80-jarige leeftijd overleden.
      </p>

      <UpdateS>
        <div>
          <p>
            Update (mei 2024): Welkom op onze website, die ook geschikt is voor mobiele telefoon! Dit digitale woordenboek
            is vanaf heden op deze website compleet te raadplegen. Een boekversie, uitgegeven door <ExternalLink
            url='https://www.aup.nl/nl/promotion/woordenboek-grieks'>Amsterdam University Press</ExternalLink>,
            is verkrijgbaar bij elke boekhandel. Een bijbehorende Vormleer van het Klassiek Grieks is eveneens beschikbaar vanaf 3 juni a.s.
          </p>
        </div>
      </UpdateS>
    </SectionNLM>
  </div>,
)
