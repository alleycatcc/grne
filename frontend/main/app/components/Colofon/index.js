import {
  pipe, compose, composeRight,
  join, whenOk, flip, reduce, concatM,
  map, lets, noop, zipAll,
  letS,
  id, prop,
} from 'stick-js/es'

import React, { PureComponent, memo, } from 'react'
import styled from 'styled-components'

import {
  mediaQuery,
} from 'alleycat-js/es/styled'

import { mapX, nAtATimePlusChunk, intersperse, } from 'alleycat-js/es/general'
import { ifEmptyList, whenNotEmptyList, ifArray, ifFalseV, } from 'alleycat-js/es/predicate'
import { compMeth, setState, setStateValueMeth, } from 'alleycat-js/es/react-legacy'

import {
  H1, H2, ExternalLink, TextDiv, ButtonsRowM, ButtonM,
} from '../../components/shared'

import {
  mediaPhone, mediaTablet, mediaDesktop,
  shouldDisableMomentumScroll,
} from '../../common'

import configure from 'alleycat-js/es/configure'
import config from '../../config'

const configTop = config | configure.init

const contributors = configTop.get ('colofon.contributors')

const ColofonS = styled.div`
  margin: 0px;
  height: 100%;
  overflow-y: scroll;
  ${prop ('shouldDisableMomentumScroll') >> ifFalseV (
    '-webkit-overflow-scrolling: touch;',
    '',
  )}
  .x_wrapper {
  }
`

const HeaderS = styled.div`
  margin-top: 6px;
  margin-bottom: 12px;
  font-weight: !important bold;
  .x__link {
  }
`

const NamesS = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: flex-start;
  margin-left: 45px;
  margin-top: 0px;
  margin-bottom: 16px;

  // common to all items
  .x__item {
    border: 1px solid blue;
  }

  .x__row {
    margin-right: 30px;
    ${mediaQuery (
      mediaPhone ('flex: 0 1 90vw;'),
      mediaTablet ('flex: 0 1 auto;'),
    )}
  }
`

// @todo we treat url = '' as the case where it shouldn't be a link. Check whether reasonable.
const makeOrganisation = ([url, name]) => url | ifEmptyList (
  () => <span className='x__item'>{name}</span>,
  () => <span className='x__link'>
    <ExternalLink url={url}>{name}</ExternalLink>
  </span>
)

const Header = ({ heading, organisations, isMobile, }) =>
  <HeaderS>
    <H2
      withBottomMargin={false}
      withTopMargin={false}
      withBoldFont={isMobile}
    >
      {heading}
      {organisations &&
        <span> ({
          organisations
            | map (makeOrganisation)
            | intersperse (() => ', ')
            | mapX ((e, idx) =>
              <span key={idx}>{e}</span>
            )
        })
        </span>
      }
    </H2>
  </HeaderS>

const Item = ({ item, }) => lets (
  () => item | ifArray (
    ([url, text]) => <ExternalLink url={url}>{text}</ExternalLink>,
    id,
  ),
  (x) => <div>{x}</div>,
)

const Contributors = ({ list, isMobile, }) => <div>
  {list.map(([heading, organisations, numColumns, items], idx) => {
    const rowsPerColumn = Math.ceil(items.length / numColumns)
    const rows = items | (rowsPerColumn | nAtATimePlusChunk)
    return <TextDiv key={idx}>
      <Header
        heading={heading}
        organisations={organisations}
        isMobile={isMobile}
      />
      {rows | whenNotEmptyList (
        () => <NamesS>
          {rows.map((row, idx2) =>
          <div key={idx2} className='x__row'>
            {row.map((item, idx3) =>
              <Item key={idx3} item={item}/>
            )}
          </div>
          )}
        </NamesS>,
      )}
  </TextDiv>
  })}
</div>

const TitleS = styled.div`
  margin-bottom: 16px;
`

const ContentsS = styled.div`
  p {
    margin-top: 0px;
    margin-bottom: 6px;
  }
  .x__text, .x__text-list, .x__sources, .x__sources-links, .x__footer {
    margin-left: 45px;
    margin-right: 45px;
  }
  .x__text {
    p {
      margin-bottom: 15px;
    }
  }
  .x__text-list {
    p {
      margin-bottom: 5px;
    }
  }
  .x__sources {
    margin-top: 20px;
    margin-bottom: 20px;;
    .x__source {
      font-style: italic;
    }
    p {
      margin-bottom: 5px;
    }
    sup {
      font-size: 7px;
    }
  }
  .x__sources-links {
    margin-top: 11px;
    margin-bottom: 15px;;
    p {
      margin-bottom: 2px;
    }
  }
  .x__footer {
    margin-top: 45px;
    margin-bottom: 45px;
  }
`

const TextS = styled.div`
`

const Credits = () => <div>
  <div className='x__text'>
    <p>Graeci uit Nederland en België zijn behulpzaam geweest bij het vaststellen van het corpus.</p>

    <p>Dit woordenboek kwam mede tot stand dankzij een startsubsidie van het Amsterdams Universiteitsfonds (het Van der Valk/Ruijgh-fonds), alsmede door de Spinozapremie 2010 (NWO SPI 31-337) en de Prijs Akademiehoogleraren (KNAW) 2016, beide toegekend aan Ineke Sluiter.</p>

    <p>De technische ontwikkeling van deze online editie werd financieel ondersteund door een subsidie van CLARIN-NL uit 2012 (projectnummer CLARIN-NL-11-008, met begeleiding van prof.dr. Jan Odijk, prof.dr. Piek Vossen en dr. Jeannine Beeken).</p>

    <p>Tal van Nederlandse onderzoekers hebben zich beziggehouden met begrips- en lexicaal onderzoek, om nog maar te zwijgen van het vruchtbare Nederlandse onderzoek naar taalkundige verschijnselen als partikels en voegwoorden. Voor specifiek advies over bepaalde lemma’s danken wij Myrthe Bartels, Tazuko van Berkel, Michel Buijs, Luuk Huitink, Elizabeth Koier, Jean Lallot, Saskia Peels, Adriaan Rademaker, Folkert van Straten en Gerry Wakker.</p>

    <p>We bedanken Helma Dik voor haar database met determinaties (op perseus.uchicago.edu, gebaseerd op data van het Perseus Project), die ze beschikbaar heeft gesteld voor de functie ‘Determineer een vorm’. Ook heeft zij ons, als drijvende kracht achter Logeion, voor ontelbare fouten behoed.</p>

    <p>De fraaie hulppagina’s zijn ontworpen door Daniël Bartelds.</p>

    <p>Net als alle Griekse woordenboeken is dit woordenboek gemaakt met behulp van andere lexica (in meerdere talen) en de grote en krachtige internationale traditie van de classici met hun grammatica’s, specialistische studies, commentaren en vertalingen, die in toenemende mate ook digitaal beschikbaar zijn.</p>

    <p>Van de digitale hulpmiddelen noemen we hier slechts:</p>
  </div>

  <div className='x__sources-links'>
    <p><ExternalLink
      url='http://www.perseus.tufts.edu/hopper'
    >project Perseus</ExternalLink></p>
    <p><ExternalLink
      url='http://www.tlg.uci.edu'
    >Thesaurus Linguae Graecae</ExternalLink></p>
    <p><ExternalLink
      url='https://logeion.uchicago.edu'
    >Logeion</ExternalLink></p>
    <p><ExternalLink
    url='http://loebclassics.com'
  >Loeb Classical Library online</ExternalLink></p>
  </div>

  <div className='x__text'>
    <p>In Nederland is er een rijke traditie van onderzoek naar de Griekse taalkunde, die haar weerslag vindt in twee belangrijke handboeken waarvan wij dankbaar gebruik gemaakt hebben:</p>
  </div>

  <div className='x__sources'>
    <p>Rijksbaron, A., S.R. Slings, P. Stork, G.C. Wakker, <span className='x__source'>Beknopte syntaxis van het klassiek Grieks</span>, Lunteren: Hermaion, 2000.</p>
    <p>Emde Boas, E. van, A. Rijksbaron, L. Huitink, M. de Bakker,<span className='x__source'> The Cambridge Grammar of Classical Greek</span>, Cambridge: Cambridge University Press, 2019.</p>
  </div>

  <div className='x__text-list'>
    <p>Voor een overzicht van de meest gebruikte commentaren, zie de <ExternalLink
  url='https://www.rug.nl/research/research-let/oikos/education/rema-and-phd-curriculum#reading-list'
>reading list</ExternalLink> van OIKOS.</p>
    <p>Voor een overzicht van Nederlandse vertalingen van Griekse literatuur, zie <ExternalLink
  url='https://griekseliteratuur.org'
  >griekseliteratuur.org</ExternalLink>.</p>
  </div>

  <div className='x__text-list'>
    <p>Van de bestaande lexicografische en grammaticale bronnen waarvan wij gebruikt gemaakt hebben, noemen wij in het bijzonder:</p>
  </div>

  <div className='x__sources'>
    <p>Adrados, F.R., J.R. Somolinos, <span className='x__source'>Diccionario Griego-Español</span> (in ontwikkeling), <ExternalLink
    url='http://dge.cchs.csic.es/xdge'
  >www.dge.cchs.csic.es/xdge</ExternalLink>.</p>
    <p>Autenrieth, G., <span className='x__source'>A Homeric Dictionary for Schools and Colleges</span>, New York: Harper and Brothers, 1891.</p>
    <p>Bailly, A., E. Egger, L. Séchan, P. Chantraine, <span className='x__source'>Dictionnaire grec-français</span>, Paris: Hachette, 2000.</p>
    <p>Bauer, W., K. Aland, B. Aland, <span className='x__source'>Griechisch-deutsches Wörterbuch zu den Schriften des Neuen Testamentes und der frühchristlichen Literatur</span>, Berlin-New York: Walter de Gruyter, 1988.</p>
    <p>Beekes, R.S.P., L. van Beek, <span className='x__source'>Etymological dictionary of Greek</span>, Leiden: Brill, 2010.</p>
    <p>Chadwick, J., <span className='x__source'>Lexicographica Graeca. Contributions to the Lexicography of Ancient Greek</span>, Oxford: Clarendon Press, 1996 (repr. 2003).</p>
    <p>Chantraine, P., <span className='x__source'>Dictionnaire étymologique de la langue grecque. Histoire des mots</span>, Paris: Klincksieck, 1999.</p>
    <p>Cunliffe, R.J., <span className='x__source'>A Lexicon of the Homeric Dialect, expanded version</span>, Norman: University of Oklahoma Press 2012 (oorspr. 1924).</p>
    <p>Diggle, J., B.L. Fraser, P. James, O.B. Simkin, A.A. Thompson, S.J. Westripp, <span className='x__source'>The Cambridge Greek Lexicon, 2 delen</span>, Cambridge : Cambridge University Press, 2021.</p>
    <p>Es, A.H.G.P. van den, C.M. Francken, <span className='x__source'>Grieksch Woordenboek</span>, Groningen: Wolters, 1896.</p>
    <p>Hupperts, Ch., <span className='x__source'>Woordenboek Grieks-Nederlands</span>, Leeuwarden: Eisma Edumedia, 2018.</p>
    <p>Liddell, H.G., R. Scott, H.S. Jones, R. McKenzie, <span className='x__source'>A Greek-English Lexicon</span>, Oxford: Clarendon Press, 1940, with a revised supplement, 1996.</p>
    <p>Mehler, J., <span className='x__source'>Woordenboek op de gedichten van Homeros</span>, ’s-Gravenhage: Nijgh & Van Ditmar, 1962.</p>
    <p>Montanari, F., <span className='x__source'>Vocabolario della lingua greca</span> Torino: Loescher, 2004.*</p>
    <p>Montanari, F., <span className='x__source'>The Brill Dictionary of Ancient Greek</span>, eds. M. Goh en C. Schroeder; Engelse vertaling van Montanari 2004, Leiden: Brill, 2014.</p>
    <p>Montijn, J.F.L., W.J.W. Koster, <span className='x__source'>Grieks-Nederlands woordenboek</span>, Zwolle: Tjeenk Willink, 1965<sup>6</sup>.</p>
    <p>Muller, F.Jzn, J.H. Thiel, W. den Boer, <span className='x__source'>Beknopt Grieks-Nederlands woordenboek</span>, Utrecht: Koenen Woordenboeken, 2001<sup>11</sup>.</p>
    <p>Pape, W., M. Sengebusch, <span className='x__source'>Handwörterbuch der griechischen Sprache, 4 delen</span>, Braunschweig: Vieweg, 1845.</p>
    <p>Slater, W.J., <span className='x__source'>Lexicon to Pindar</span>, Berlin: De Gruyter, 1969.</p>
    <p>Snell, B., H.J. Mette et al., <span className='x__source'>Lexikon des frühgriechischen Epos, 4 delen</span>, Göttingen: Vandenhoeck & Ruprecht, 1955-2010.</p>
    <p>Veitch, W., <span className='x__source'> Greek Verbs, Irregular and Defective. Their Forms, Meaning, and Quantity</span>, Oxford: Clarendon Press, 1871.</p>
  </div>

  <div className='x__text'>
    <p>*Het woordenboek van Montanari (2004) is van grote waarde geweest voor het vaststellen van het basismateriaal.</p>
  </div>


  <div className='x__footer'>
    <p>Dit werk is gelicenseerd onder een <ExternalLink
        url='http://creativecommons.org/licenses/by-nc/3.0/deed.nl'
      >Creative Commons Naamsvermelding-NietCommercieel 3.0 Unported licentie.</ExternalLink></p>
  </div>

</div>

const PresentationButtonsMS = styled.div`
  ${mediaQuery (
    mediaPhone ('display: block'),
    mediaTablet ('display: none'),
  )}
  min-height: 50px;
  width: 60%;
  margin: auto;
`

const PresentationButtonsM = ({ which, onClicks, }) => <PresentationButtonsMS>
  <ButtonsRowM>
    {
      zipAll (
        [['Medewerkers', which === 0], ['Credits', which === 1]],
        onClicks,
      ) | mapX (([[text, selected], onClick], idx) =>
        <ButtonM
          key={idx}
          text={text}
          selected={selected}
          disabled={selected}
          onClick={onClick}
        />
      )
    }
  </ButtonsRowM>
</PresentationButtonsMS>

class Colofon extends PureComponent {
  getInitialState = () => this | compMeth (
    ({ isMobile, }) => isMobile ? 1 : 3,
  )
  state = {
    // --- bitfield to show tabs: 1 = tab 1, 2 = tab 2
    show: this.getInitialState (),
  }
  show = this | setStateValueMeth ('show')
  onClickX = () => this.show (1)
  onClickY = () => this.show (2)
  render = () => this | compMeth (
    ({ isMobile }, { show }, { onClickX, onClickY, }) => <ColofonS
      shouldDisableMomentumScroll={shouldDisableMomentumScroll ()}
    >
      <TitleS>
        <H1 className='x__header'>
          Colofon
        </H1>
      </TitleS>
      <PresentationButtonsM
        which={show & 1 ? 0 : 1}
        onClicks={[
          onClickX, onClickY,
        ]}
      />
      <ContentsS>
        {Boolean (show & 1) && <Contributors list={contributors} isMobile={isMobile}/>}
        {Boolean (show & 2) && <Credits/>}
      </ContentsS>
    </ColofonS>,
  )
}

export default Colofon
