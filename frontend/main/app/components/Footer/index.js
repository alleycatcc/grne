import {
  pipe, compose, composeRight,
  tap,
  addIndex,
  plus,
  lets,
  map,
  deconstruct,
} from 'stick-js/es'

import React from 'react'

import styled from 'styled-components'

import configure from 'alleycat-js/es/configure'
import { logWith, mapX, intersperse, addIndexF, } from 'alleycat-js/es/general'
import { ifTrueV, } from 'alleycat-js/es/predicate'
import { withDisplayName, } from 'alleycat-js/es/react'
import { media, mediaQuery, mlt, mediaRule, } from 'alleycat-js/es/styled'

import { TextDiv, ExternalLink, } from '../../components/shared'

import { mediaPhone, mediaTablet, mediaDesktop, isMobileWidth, browseLinkForRouter, } from '../../common'
import config from '../../config'

const configTop = config | configure.init

const footerInfo = 'footer.info' | configTop.get

const ItemS = styled (TextDiv)`
  ${mediaQuery (
    mediaPhone (`
    `),
    mediaTablet (`
    `),
  )}
  display: inline-block;
  cursor: pointer;
  transition: color 200ms;
  :hover {
    color: black;
  }
`

const Item = ({ text, link, }) => <ItemS>
  <ExternalLink noColor={true} url={link}>{text}</ExternalLink>
</ItemS>

const TopS = styled.div`
  ${mediaQuery (
    mediaPhone (`
      position: relative;
      left: 50%;
      transform: translateX(-50%);
    `),
    mediaTablet (`
      white-space: nowrap;
    `),
  )}
  text-align: center;
  color: #999;
`

const separatorShow = lets (
  () => map (ifTrueV ('inline-block', 'none')),
  (f) => [true, false, false, false, false] | f,
  (f) => [true, true, true, true, false] | f,
  (_, phone, tablet) => [phone, tablet],
)

const SeparatorS = styled (TextDiv)`
  ${({ idx, }) => separatorShow | deconstruct (
    ([phone, tablet]) => mediaQuery (
      mediaPhone ('display: ' + phone [idx]),
      mediaTablet ('display: ' + tablet [idx]),
      (335 | mlt | mediaRule) ('opacity: 0'),
    ),
  )}
  margin-left: 4px;
  margin-right: 4px;
`
const Separator = ({ idx, }) => <SeparatorS idx={idx}>·</SeparatorS>

const ItemWrapperS = styled.div`
  display: inline-block;
  text-align: center;
  ${mediaQuery (
    mediaPhone (`
      :nth-child(3) {
        display: block;
      }
    `),
    mediaTablet (`
      :nth-child(3) {
        display: inline-block;
      }
    `),
    // (335 | mlt | mediaRule) ('display: block'),
  )}
`

export default withDisplayName ('Footer') (
  ({ isMobile, }) => <TopS>
      {/*window.navigator.userAgent*/}
    {footerInfo (isMobile)
    | mapX (([text, link], idx) =>
      <ItemWrapperS key={idx}>
        <Item text={text} link={link}/>
        <Separator idx={idx}/>
      </ItemWrapperS>
    )
    }
  </TopS>
)
