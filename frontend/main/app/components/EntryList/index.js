import {
  pipe, compose, composeRight,
  always,
  lets,
  head,
  bindProp,
  prop,
  concatTo, join,
  mergeM,
  whenOk, path,
  noop, id,
  tap,
  deconstruct,
  last,
  sprintfN, condS, guardV,
  eq,
  applyTo1,
  ifPredicate,
  ifPredicateV,
} from 'stick-js/es'

import React, { PureComponent, } from 'react'
import styled from 'styled-components'
import { Link, } from 'react-router-dom'

import { clss, } from 'alleycat-js/es/dom'
import { HoofdW, } from '../../components/Lemma'

import { logWith, mapX, } from 'alleycat-js/es/general'
import { whenTrueV, ifTrueV, isNotEmptyList, ifEqualsV, } from 'alleycat-js/es/predicate'
import { withResizeListener, whyUpdateMeth, whyUpdate, createRef, compMeth, } from 'alleycat-js/es/react-legacy'

import { mediaQuery, } from 'alleycat-js/es/styled'

import { BrowseSpinner, } from '../../components/shared'

import { mediaPhone, mediaTablet, mediaDesktop, isMobileWidth, browseLinkForRouter, debugDev, } from '../../common'
import configure from 'alleycat-js/es/configure'
import config from '../../config'
import { fetchIsPrepend, } from '../../types'

const configTop = config | configure.init

const openHand = require ('../../images/cursor/open-hand.cur')
const closedHand = require ('../../images/cursor/closed-hand.cur')

const debugRender = 'debug.rerender' | configTop.get

  /*
const FetchSpinnerS = styled.div`
  position: absolute;
  left: calc(50% - 8px);
  ${prop ('pos') >> ifEquals ('top') (
    () => 'top: 40px',
    () => 'bottom: 40px',
  )};
`

const FetchSpinner = ({ pos, }) => <FetchSpinnerS pos={pos}>
  <BrowseSpinner small={true}/>
</FetchSpinnerS>
*/

const BrowseSpinnerWrapperS = styled.div`
  ${prop ('pos') >> ifEqualsV ('top') (
    'margin-top: 9px; margin-bottom: 5px;',
    'margin-top: 4px; margin-bottom: 9px;',
  )}
`

const BrowseSpinnerWrapper = ({ pos, }) => <BrowseSpinnerWrapperS pos={pos}>
  <BrowseSpinner shrink={1.55}/>
</BrowseSpinnerWrapperS>

const TopS = styled.div`
  ${prop ('standalone') >> ifTrueV (`
    cursor: default;
    // overflow-y: scroll;
    // -webkit-overflow-scrolling: touch;
    // height: 100%;
  `,
  `
    cursor: url(${openHand}), move;
    :active {
      cursor: url(${closedHand}), move;
    }
  `)}
  width: 100%;
  ${mediaQuery (
    mediaPhone ('line-height: 2.5em'),
    mediaTablet ('line-height: inherit'),
  )};
  display: inline-block;
`

const WordS = styled.div`
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;

  .x--small {
    font-size: 14px;
  }
  .x--medium {
    font-size: 16px;
  }
  .x--big {
    font-size: 18px;
  }
  .x__word {
    opacity: 0.5;
    cursor: pointer;
    ${prop ('legacy-weight') >> whenTrueV (
      'font-weight: normal !important;',
    )}
    :hover {
      opacity: 1;
    }
    &.x--selected {
      opacity: 1;
    }
  }
`

const Word = ({
  word, disableClass, selected, onSelectEntry,
  lemmaId, lemmaIdx, fontSize,
}) => {
  const cls = clss (
    selected && 'x--selected',
    'x__word',
    'hoofdW',
    'x--' + fontSize,
  )
  const onClick = () => onSelectEntry (lemmaId, lemmaIdx)
  return <WordS legacy-weight={false}>
    <HoofdW className={disableClass}>
      <span className={cls} onClick={onClick}>
        {word}
      </span>
    </HoofdW>
  </WordS>
}

// @todo repeated
const ARROW_HEIGHT = 8
const ARROW_PADDING = 5

const imgArrowUp = configTop.get ('images.arrow.up')
const imgArrowDown = configTop.get ('images.arrow.down')

const ArrowImageS = styled.div`
  height: 20px;
  width: 100%;
  text-align: center;
  cursor: pointer;
  ${({ pos, }) => [ARROW_PADDING, ARROW_PADDING]
    | sprintfN ('padding-top: %spx; padding-bottom: %spx;')
  }
  img {
    vertical-align: top;
    height: 100%;
  }
  :active {
    opacity: 0.5;
    img {
      transform: translateY(3px) translateX(3px);
    }
  }
`

const ArrowImage = ({ which, onClick, }) => lets (
  () => which | condS ([
    'prev' | eq | guardV ([imgArrowUp, 'top']),
    'next' | eq | guardV ([imgArrowDown, 'bottom']),
  ]),
  ([src, pos]) => <ArrowImageS pos={pos} onClick={onClick}>
    <img src={src}/>
  </ArrowImageS>
)

export default class EntryList extends PureComponent {
  // @todo not used
  state = {
    // --- because the normal 'scrollIntoView' method doesn't play nice with react-draggable.
    selectedWordOffsetY: void 8,
  }

  theRefs = {
    selectedWord: createRef (),
    top: createRef (),
  }

  // --- can't use scrollToView, because it will also scroll horizontally and reveal the off-screen
  // element.

  scrollToEntry = (y) => this | deconstruct (
    ({ theRefs: {
      top: { current: topRef },
    }}) => topRef.scrollTop = y - topRef.offsetHeight / 2
  )

  fetchChunkPrev = () => this | compMeth ((
    { entriesFetchChunkPrevClickedDispatch, setSelectedOffsetTopDispatch, },
    {},
    { theRefs: { selectedWord: { current: ref, }}},
  ) => {
    setSelectedOffsetTopDispatch (ref.offsetTop)
    entriesFetchChunkPrevClickedDispatch ()
  })

  componentDidMount = () => {
    // --- necessary so that the drag positions in the parent can be calculated.
    // --- the reason is that our DidMount is called before the parent's DidMount, which breaks the
    // calculation.
    this.forceUpdate ()
  }

  componentDidUpdate (prevProps, prevState) {
    this | whyUpdate ('EntryList', debugRender) (prevProps, prevState)
    const { props, theRefs, scrollToEntry, } = this
    const {
      scrollToEntry: scrollToEntryProp=noop,
      standalone=false,
      fetchType,
      entries, exploreMode,
      selectedOffsetTop,
      adjustEntryScroll,
    } = props
    const {
      selectedWord: { current: selectedWordRef, },
      top: { current, }
    } = theRefs
    if (prevProps.exploreMode !== exploreMode) return
    // --- for some reason this happens twice, once when the spinner is shown and once when the
    // results are in.
    // --- the -20 lets us see the prev spinner and keeps the position the same when the results are
    // in.
    // --- the +20 doesn't actually do anything, because the way scrolling happens at the end is
    // different.
    if (exploreMode && prevProps.entries !== entries) {
      return lets (
        () => selectedWordRef.offsetTop - selectedOffsetTop,
        () => fetchType | ifPredicateV (fetchIsPrepend) (
          -20, 20,
        ),
        () => adjustEntryScroll,
        (o, d, f) => f | whenOk (applyTo1 (o + d)),
      )
    }
    lets (
      () => standalone ? scrollToEntry : scrollToEntryProp,
      (scroll) => selectedWordRef | whenOk (prop ('offsetTop') >> scroll),
    )
  }

  render () {
    const { props, theRefs, fetchChunkPrev, } = this
    // --- standalone = false: embedded inside Draggable.
    const {
      standalone=false, exploreMode=false,
      entries, disableClass, selectedLemmaIdx,
      entriesAppending, entriesPrepending,
      maxLemmaId,
      entriesFetchChunkPrevClickedDispatch,
      entriesFetchChunkNextClickedDispatch,
      onSelectEntry: onSelectEntryProp, fontSize, innerStyle={},
      selectedOffsetTop,
      setSelectedOffsetTopDispatch,
    } = props
    const {
      top: topRef,
      selectedWord: selectedWordRef,
    } = theRefs
    const onSelectEntry = (lemmaId, lemmaIdx) => onSelectEntryProp (lemmaId, lemmaIdx, exploreMode)
    const showArrows = entries | isNotEmptyList
    const firstLemmaId = entries | head | whenOk (0 | prop)
    const lastLemmaId = entries | last | whenOk (0 | prop)
    const showArrowUp = showArrows && firstLemmaId !== 0
    const showArrowDown = showArrows && lastLemmaId !== maxLemmaId

    return <TopS
      ref={topRef}
      style={innerStyle}
      standalone={standalone}
      onClick={event => event.stopPropagation ()}
    >
      {showArrowUp && <ArrowImage which='prev' onClick={fetchChunkPrev}/>}
      {entriesPrepending && <BrowseSpinnerWrapper pos='top'/>}
      <div>
        {entries | mapX (([lemmaId, word], idx) => lets (
          () => selectedLemmaIdx === idx,
          (selected) => <div
            ref={selected ? selectedWordRef : null}
            key={idx}
          >
            <Word
              disableClass={disableClass}
              selected={selected}
              onSelectEntry={onSelectEntry}
              word={word}
              lemmaIdx={idx}
              lemmaId={lemmaId}
              fontSize={fontSize}
            />
          </div>,
        ))}
      </div>
      {entriesAppending && <BrowseSpinnerWrapper pos='bottom'/>}
      {showArrowDown && <ArrowImage which='next' onClick={entriesFetchChunkNextClickedDispatch}/>}
    </TopS>
  }
}
