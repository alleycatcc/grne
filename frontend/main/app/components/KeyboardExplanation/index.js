import {
  pipe, compose, composeRight,
  tap, join, prop, zipAll, map,
  take, drop,
  deconstruct,
} from 'stick-js/es'

import React from 'react'

import memoize from 'memoize-immutable'
import styled from 'styled-components'

import CloseIcon from '../../components/svg/CloseIcon'

import configure from 'alleycat-js/es/configure'
import { logWith, nAtATime, zipWithAll, mapX, } from 'alleycat-js/es/general'
import { withDisplayName, } from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import { mediaPhone, mediaTablet, mediaDesktop, } from '../../common'
import config from '../../config'

const configTop = config | configure.init

const keyboardRows = memoize ((numLetterRows, numAccentCols) => {
  const makePair = ([g, l]) => [g, l] | mapX (
    (x, idx) => <span key={idx}>{x}</span>
  )
  let key = 0
  const makeRow = (...xs) => <tr key={key += 1}>
    {xs | mapX (
      (x, idx) => <td key={idx}>{x}</td>,
    )}
  </tr>

  const gr = [
    'α', 'β', 'γ', 'δ', 'ε', 'ζ', 'η', 'θ', 'ι', 'κ', 'λ', 'μ', 'ν', 'ξ', 'ο', 'π', 'ρ', 'σ', 'τ',
    'υ', 'φ', 'χ', 'ψ', 'ω'
  ]
  const la = [
    'a', 'b', 'g', 'd', 'e', 'z', 'h', 'q', 'i', 'k', 'l', 'm', 'n', 'c', 'o', 'p', 'r', 's', 't',
    'u', 'f', 'x', 'y', 'w'
  ]
  const gra = ['᾿', '῾', '´', '`', '¨', '῀']
  const laa = [')', '(', '/', '\\', '+', '=']

  const f = (x, y) => zipAll (x, y) | map (makePair)
  const g = (a, b, n) => f (a, b) | nAtATime (n)
  const accents = f (gra, laa)

  return [
    // --- note that makeRow only works for perfectly rectangular blocks.
    ...zipWithAll (makeRow, ...g (gr, la, numLetterRows)),
    makeRow (...(accents | take (numAccentCols))),
    makeRow (...(accents | drop (numAccentCols))),
  ]
})

const KeyboardExplanationS = styled.div`
  position: relative;
  z-index: 1;
  background: rgb(246, 246, 246);
  font-size: 17px;
  text-align: center;
  height: 100%;
  width: 100%;
  display: flex;
  table {
    width: 335px;
    width: 100%;
    border-spacing: 0;
    border-collapse: collapse;
  }

  ${mediaQuery (
    mediaPhone (`
      padding: 10px;
      padding-top: 37px;
      table {
        height: 300px;
      }
      `),
    mediaTablet (`
      padding: 10px;
      padding-top: 25px;
      table {
        height: 400px;
      }
    `),
  )}

  tr {
    :nth-child(9), :nth-child(10) {
      span:nth-child(1) {
        font-size: 28px;
      }
    }
  }
  td {
    :nth-child(1), :nth-child(3) {
    }
    text-align: center;
    span {
      padding: 3px;
    }
  }
  .x__main {
    margin: auto;
  }
  .x__close {
    position: absolute;
    top: 3px;
    right: 3px;
    cursor: pointer;
    width: 48px;
    height: 43px;
    display: flex;
    * {
      margin: auto;
    }
  }
`

// --- stateless and `close` is controlled from outside, to make it easier to close by e.g. clicking
// away.

export default withDisplayName ('KeyboardExplanation') (
  ({ numLetterRows=8, numAccentCols=3, showClose, close, }) => <KeyboardExplanationS>
    <div className='x__main'>
      <table>
        <tbody>
          {keyboardRows (numLetterRows, numAccentCols)}
        </tbody>
      </table>
      De iota subscriptum voer je als een iota in
    </div>
    {showClose && <div className='x__close' onClick={close}>
      <CloseIcon
        height={22}
        width={22}
        strokeWidth='0.3px'
      />
    </div>
    }
  </KeyboardExplanationS>,
)
