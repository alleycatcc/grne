import {
  pipe, compose, composeRight,
  lets, sprintf1,
  deconstruct2,
} from 'stick-js/es'

import React from 'react'
import { ReactReduxContext, } from 'react-redux'
import hoistNonReactStatics from 'hoist-non-react-statics'

import getInjectors from './sagaInjectors'

; `
mode:
  const { RESTART_ON_REMOUNT, DAEMON, ONCE_TILL_UNMOUNT, } = constants
  RESTART_ON_REMOUNT(default) -> saga started component mount and cancelled(with \`task.cancel()\`) on unmount.
  DAEMON -> saga starts on component mount and never cancels or restarts.
  ONCE_TILL_UNMOUNT: saga cancels on unmount and never starts again.
`

export default ({ key, saga, mode }) => deconstruct2 (({ displayName, name, }) =>
  WrappedComponent => lets (
    () => class InjectSaga extends React.Component {
      // --- why is this necessary?
      static WrappedComponent = WrappedComponent

      static contextType = ReactReduxContext
      static displayName = (displayName || name || '[unknown]') | sprintf1 ('withSaga (%s)')

      injectors = getInjectors (this.context.store)

      constructor (props, context) {
        super (props, context)
        const { injectors, } = this
        injectors.injectSaga (key, { saga, mode }, props)
      }

      componentWillUnmount () {
        const { injectors, } = this
        const { ejectSaga } = injectors
        key | ejectSaga
      }

      render () {
        const { props, } = this
        return <WrappedComponent {...props} />
      }
    },
    component => hoistNonReactStatics (component, WrappedComponent),
  )
)
