import {
  pipe, compose, composeRight,
  lets, sprintf1,
  deconstruct2,
} from 'stick-js/es'

import React from 'react'
import { ReactReduxContext, } from 'react-redux'
import hoistNonReactStatics from 'hoist-non-react-statics'

import getInjectors from './reducerInjectors'

export default ({ key, reducer, }) => deconstruct2 (({ displayName, name, }) =>
  WrappedComponent => lets (
    () => class ReducerInjector extends React.Component {
      // --- why is this necessary?
      static WrappedComponent = WrappedComponent

      static contextType = ReactReduxContext
      static displayName = (displayName || name || '[unknown]') | sprintf1 ('withReducer (%s)')

      constructor (props, context) {
        super (props, context)
        getInjectors (context.store).injectReducer (key, reducer)
      }

      render () {
        const { props, } = this
        return <WrappedComponent {...props} />
      }
    },
    component => hoistNonReactStatics (component, WrappedComponent),
  )
)
