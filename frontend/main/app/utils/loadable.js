import {
  pipe, compose, composeRight,
  lets,
} from 'stick-js/es'

import React, { Suspense, lazy, } from 'react'

export default (importFunc, { fallback = null, } = {}) => lets (
  () => importFunc | lazy,
  (Component) => (props) => (
    <Suspense fallback={fallback}>
      <Component {...props}/>
    </Suspense>
  ),
)
