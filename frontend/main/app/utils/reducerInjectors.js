import {
  pipe, compose, composeRight,
  againstAll, isString, isFunction,
} from 'stick-js/es'

import invariant from 'invariant'

import { allV, isNotEmptyString, } from 'alleycat-js/es/predicate'

import checkStore from './checkStore'
import createReducer from '../reducers'

const isReallyNotEmptyString = againstAll ([
  isString, isNotEmptyString,
])

export const injectReducerFactory = (store, isValid) => {
  return (key, reducer) => {
    if (!isValid) checkStore (store)

    invariant (
      allV (
        key | isReallyNotEmptyString,
        reducer | isFunction,
      ),
      '(app/utils...) injectReducer: Invalid arguments',
    )

    // --- react-boilerplate:
    // check `store.injectedReducers[key] === reducer` for hot reloading when a key is the same but
    // a reducer is different
    if (Reflect.has (store.injectedReducers, key) && store.injectedReducers[key] === reducer) return

    store.injectedReducers [key] = reducer
    store.replaceReducer (createReducer (store.injectedReducers))
  }
}

export default (store) => {
  checkStore (store)

  return {
    injectReducer: injectReducerFactory (store, true),
  }
}
