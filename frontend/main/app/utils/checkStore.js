import {
  pipe, compose, composeRight,
  isObject, isFunction,
} from 'stick-js'

import invariant from 'invariant'

import { conformsTo, } from '../common'

const shape = {
  dispatch: isFunction,
  subscribe: isFunction,
  getState: isFunction,
  replaceReducer: isFunction,
  runSaga: isFunction,
  injectedReducers: isObject,
  injectedSagas: isObject,
}

export default (store) => invariant (
  store | conformsTo (shape),
  '(app/utils...) injectors: Expected a valid redux store',
)
