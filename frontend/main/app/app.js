import {
  pipe, compose, composeRight,
  lets,
} from 'stick-js/es'

// --- for generators (regenerator runtime) and built-ins like 'Reflect'.
// --- in the @babel/preset-env section of the babel config, set 'useBuiltIns' to 'entry' and
// 'corejs' to the right version.
// --- core-js will figure out what needs to get polyfilled based on the 'targets' or 'browserslist'
// options.
import 'core-js/stable'
import 'regenerator-runtime/runtime'

// --- misc. polyfills, e.g. IE Node methods.
import './polyfills'

import './manifest.json'

import './images/icons/favicon.ico'
import './images/icons/icons-192.png'
import './images/icons/icons-512.png'

import React, { Fragment, } from 'react'
import { render, unmountComponentAtNode, } from 'react-dom'
import { Provider, } from 'react-redux'
import { Router, } from 'react-router-dom'

; `
Might need to use hash history (or possibly memory history?) if files will be served using the file:// protocol (e.g. a hybrid app) or from static files (i.e. a build/ directory served by e.g. nginx).
`

import { createBrowserHistory, } from 'history'
const createHistory = createBrowserHistory

import 'sanitize.css'
import 'sanitize.css/forms.css'
import 'sanitize.css/typography.css'

import { withBackDetect, } from 'alleycat-js/es/history'

; `
--- css-loader.
--- importing css here makes it available to all components.
import './xxx.css'
`

import App from './containers/App'

import configureStore from './configureStore'
import { GlobalStyle, } from './global-styles'

const initialState = {}

/*
 * History with backbutton detection (experimental)
 * Causing problems on Firefox (sometimes doesn't detect history change) and not tested on mobile.
 * const [initHistory, listenHistory, history] = createHistory ()
 *  | withBackDetect ()
 *  initHistory ()
 * <App listenHistory={listenHistory}/>
 */

const store = configureStore (initialState)
const mount_node = document.getElementById ('app')

const history = createHistory ()

const renderApp = () => render (
  <Fragment>
    <Provider store={store}>
      <Router history={history}>
        <App history={history}/>
      </Router>
    </Provider>
    <GlobalStyle/>
  </Fragment>,
  mount_node,
)

if (module.hot) module.hot.accept (
  // ['./i18n', 'containers/App'],
  ['containers/App'],
  () => {
    unmountComponentAtNode (mount_node)
    renderApp ()
  },
)

renderApp ()
