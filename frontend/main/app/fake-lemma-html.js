import {
  pipe, compose, composeRight,
  lets, timesF, map,
} from 'stick-js/es'

const getFakeLemmaHtml = lets (
  () => fakeLemmaHtml (),
  (hs) => () => hs [Math.floor (Math.random () * hs.length)],
)

export const fakeLemma = (f, n) => getFakeLemmaHtml
  | timesF (n)
  | map (f)

function fakeLemmaHtml () {
  return [
    `
      <div class="lem">
        <div class="vorm">
          <div class="hoofdW">&#x3B2;&#x1F71;&#x3C4;&#x3C1;&#x3B1;&#x3C7;&#x3BF;&#x3C2; -&#x3BF;&#x3C5;, &#x1F41;, <div class="i"><span class="abbr" data-abbr="Ionisch">Ion.</span>
    ook </div>&#x3B2;&#x3C1;&#x1F79;&#x3C4;&#x3B1;&#x3C7;&#x3BF;&#x3C2;, </div>
        </div>
        <div class="bet">
          <div class="vertM">kikker.</div>
        </div>
      </div>
    `,
    `
      <div class="verwLem">
        <div class="hoofdW">&#x3C0;&#x3B1;&#x3C1;&#x3C0;&#x1F73;&#x3C4;&#x3B1;&#x3C4;&#x3B1;&#x3B9; </div>
        <div class="morfI"><span class="abbr" data-abbr="po&#xEB;tisch, dichterlijk">po&#xEB;t.</span> <span class="abbr" data-abbr="indicativus">indic.</span> <span class="abbr" data-abbr="praesens">praes.</span> <span class="abbr" data-abbr="medium, media(a)l(e)">med.</span> 3 <span class="abbr" data-abbr="singularis (enkelvoud)">sing.</span> </div>
        <div class="kruisVerw"><div class="i">van</div> &#x3C0;&#x3B1;&#x3C1;&#x3B1;&#x3C0;&#x1F73;&#x3C4;&#x3BF;&#x3BC;&#x3B1;&#x3B9;.</div>
      </div>
    `,
    `
      <div class="lem">
        <div class="vorm">
          <div class="hoofdW">&#x3C0;&#x3B1;&#x3C1;&#x3C0;&#x1F79;&#x3B4;&#x3B9;&#x3BF;&#x3C2; -&#x3BF;&#x3BD; </div>
          <div class="etym">[<div class="link" target="">&#x3C0;&#x3B1;&#x3C1;&#x1F71;</div>, <div class="link" target="">&#x3C0;&#x3BF;&#x1F7B;&#x3C2;</div>] </div>
        </div>
        <div class="bet">
          <div class="vertM">op handen zijnd.</div>
        </div>
      </div>
    `,

    `
  <div class="lem">
    <div class="vorm">
      <div class="hoofdW">&#x3C0;&#x3C1;&#x3BF;-&#x3BC;&#x3B9;&#x3C3;&#x3B8;&#x1F79;&#x3BF;&#x3BC;&#x3B1;&#x3B9; </div>
    </div>
    <div class="bet">
      <div class="vertM">vooraf huren: </div>
      <div class="gebrW"><span class="abbr" data-abbr="participium (deelwoord)">ptc.</span> <span class="abbr" data-abbr="perfectum">perf.</span> <span class="abbr" data-abbr="passivum">pass<span class="punc-stan">.</span><span class="punc-bekn">.</span> </span></div>
      <div class="cit">
        <div class="citG"><span class="citg-word">&#x3C4;&#x1F78;</span> <span class="citg-word">&#x3C0;&#x3C1;&#x3BF;&#x3BC;&#x3B5;&#x3BC;&#x3B9;&#x3C3;&#x3B8;&#x3C9;&#x3BC;&#x1F73;&#x3BD;&#x3BF;&#x3BD;</span> <span class="citg-word">&#x3BF;&#x1F34;&#x3BA;&#x3B7;&#x3BC;&#x3B1;</span> </div>
        <div class="citNV">het vooraf gehuurde huis </div>
        <div class="verw" data-abbr-verw="Plutarchus___Marcellus___5.2">
          <div class="aut">Plut. </div>
          <div class="werk">Marc. </div>
          <div class="plaats">5.2.</div>
        </div>
      </div>
    </div>
  </div>
    `,
    `
    <div class="lem">
        <div class="vorm">
          <div class="hoofdW">&#x3C0;&#x3C1;&#x3BF;&#x3BC;&#x3BD;&#x3B7;&#x3C3;&#x3C4;&#x3B9;&#x3BA;&#x1F75;, &#x1F21; </div>
          <div class="etym">[<div class="link" target="">&#x3C0;&#x3C1;&#x3BF;&#x3BC;&#x3BD;&#x1F71;&#x3BF;&#x3BC;&#x3B1;&#x3B9;</div>] </div>
        </div>
        <div class="bet">
          <div class="gebrW"><span class="abbr" data-abbr="scilicet (dat wil zeggen, namelijk, vul aan)">sc.</span> <div class="r"><div class="link" target="">&#x3C4;&#x1F73;&#x3C7;&#x3BD;&#x3B7;</div></div> </div>
          <div class="vertM">kunst van de huwelijksbemiddeling.</div>
        </div>
      </div>
    `,
    `
    <div class="lem">
        <div class="vorm">
          <div class="hoofdW">&#x3C0;&#x3C1;&#x3BF;&#x3BC;&#x3BD;&#x3B7;&#x3C3;&#x3C4;&#x1FD6;&#x3BD;&#x3BF;&#x3B9; -&#x3B1;&#x3B9; </div>
          <div class="morfI"><span class="abbr" data-abbr="pluralis, meervoud">plur.</span> </div>
        </div>
        <div class="bet">
          <div class="vertM">de een na de ander.</div>
        </div>
      </div>
    `,
    `
    <div class="lem">
        <div class="vorm">
          <div class="hoofdW">&#x3C0;&#x3C1;&#x3BF;&#x3BC;&#x3BD;&#x1F75;&#x3C3;&#x3C4;&#x3C1;&#x3B9;&#x3B1; -&#x3B1;&#x3C2;, &#x1F21; </div>
          <div class="etym">[<div class="link" target="">&#x3C0;&#x3C1;&#x3BF;&#x3BC;&#x3BD;&#x1F75;&#x3C3;&#x3C4;&#x3C9;&#x3C1;</div>] </div>
        </div>
        <div class="bet">
          <div class="vertM">koppelaarster.</div>
        </div>
      </div>
    `,
  ]
}
