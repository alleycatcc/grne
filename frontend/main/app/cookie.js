// --- cookie implementation, ported from mootools 1.4

import {
  pipe, compose, composeRight,
  mergeM, factory, factoryProps,
  tap, ifTrue, concatTo, ifOk,
  always, join,
  ifPredicate,
  ok, map,
  whenOk,
  lets,
  dot1, dot2,
  bindProp,
  id,
} from 'stick-js/es'

import { Just, Nothing, } from 'alleycat-js/es/bilby'
import { logWith, } from 'alleycat-js/es/general'

const liftAry = pred => ifPredicate (pred) (
  x => [x],
  _ => [],
)

const liftMaybe = pred => ifPredicate (pred) (
  Just,
  Nothing | always,
)

const liftAryOk = ok | liftAry
const liftAryYes = Boolean | liftAry
const liftMaybeOk = ok | liftMaybe


;`
Usage: [
  ... (domain | whenOkAry (concatTo ('domain=')))
  ... (path | whenOkAry (concatTo ('path=')))
]

Results in
  ['domain=<domain>', 'path=<path>']
or
  ['domain=<domain>']
or
  ['path=<path>']
or
  []

`
const whenOkAry = f => liftAryOk >> map (whenOk (f))

const props = {
  document,
  path: '/',
  encode: true,
  key: void 8,
  domain: void 8,
  duration: void 8,
  secure: void 8,
}

const msFromNow = ms => lets (
  ()              => new Date,
  (date)          => date.getTime () + ms,
  (date, newtime) => date.setTime (newtime),
  (date)          => date.toGMTString ()
)

const hoursToMs = hours => hours * 60 * 60 * 1000
const daysToHours = days => days * 24
const daysToMs = daysToHours >> hoursToMs

const hoursFromNow = hoursToMs >> msFromNow
const daysFromNow = daysToMs >> msFromNow

const replace = dot2 ('replace')
const escapeRegExp = replace (/([-.*+?^${}()|[\]/\\])/g, '\\$1')

const write = dot1 ('write')
const clone = Object | bindProp ('create')

const proto = {
  init () {
    return this
  },

  write (value) {
    const { key, encode, domain, path, duration, secure, document, } = this

    const cookieVal = [
      ... ([value]  | map (encode ? encodeURIComponent : id)),
      ... (domain   | whenOkAry (concatTo ('domain='))),
      ... (path     | whenOkAry (concatTo ('path='))),
      ... (duration | whenOkAry (hoursFromNow >> concatTo ('expires='))),
      ... (secure   | whenOkAry ('secure' | always)),
    ] | join ('; ') | tap (logWith ('cookie'))

    document.cookie = key + '=' + cookieVal

    return this
  },

  read () {
    const { document, key, } = this
    const value = lets (
      () => '(?:^|;)\\s*',
      () => escapeRegExp (key),
      () => '=([^;]*)',
      (a, b, c) => document.cookie.match (a + b + c),
    )
    return value ? decodeURIComponent (value [1]) : null
  },

  dispose () {
    return this
      | clone
      | mergeM ({ duration: -24, })
      | write ('')
  },
}

export default proto | factory | factoryProps (props)
