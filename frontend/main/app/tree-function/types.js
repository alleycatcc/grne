import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, roll,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, blush, always, T, F,
  prop, path, has, hasIn,
  bindPropTo, bindProp, bindTo, bind,
  assoc, assocPath, assocM, assocPathM,
  concatTo, concat, appendTo, append,
  concatToM, concatM, appendToM, appendM,
  merge, mergeTo, mergeM, mergeToM,
  mergeIn, mergeInTo, mergeInM, mergeInToM,
  updateM, update, updatePathM, updatePath,
  lets, letS, compactOk, compact,
  die, raise, decorateException, exception,
  lt, gt, eq, ne, lte, gte,
  factory, factoryProps,
} from 'stick-js/es'

import flatMap from 'ramda/es/chain'

const flatten = flatMap (id)

import daggy from 'daggy'

export const toString = dot ('toString')
export const toJS = dot ('toJS')

export const Point = daggy.tagged ('Point', ['row', 'column'])
export const Line = daggy.tagged ('Line', ['p1', 'p2'])
export const Tree = daggy.tagged ('Tree', ['lines'])
export const Height = daggy.tagged ('Height', ['height'])

export const Block = daggy.taggedSum ('Block', {
  BlockAnd: ['blockTree'],
  BlockOr: ['blockTree'],
  ConnectBlock: ['blockTree'],
})

export const blockTree = prop ('blockTree')

export const treeLines = prop ('lines')

export const combineTrees = (tree1, tree2) => lets (
  _ => tree1 | treeLines,
  _ => tree2 | treeLines,
  (l1, l2) => [...l1, ...l2] | Tree,
)

export const makeTreeFromBlocks =
  map (blockTree) >> map (treeLines) >> flatten >> Tree

export const { BlockAnd, BlockOr, ConnectBlock, } = Block

const Bool = daggy.taggedSum ('Bool', {
  And: [],
  Or: [],
})

export const { And, Or, } = Bool

const BoolLine = daggy.taggedSum ('BoolLine', {
  AndLine: ['line'],
  OrLine: ['line'],
})

export const { AndLine, OrLine, } = BoolLine

const jsArray2 = (x, y) => jsArray ([x, y])
const jsArray = (xs) => lets (
  _ => xs | map (toString),
  (ys) => ys | join (','),
  (_, str) => str | sprintf1 ('[%s]'),
)

Point.prototype.toString = function () {
  const { row, column, } = this
  return jsArray2 (row, column)
}

Line.prototype.toString = function () {
  const { p1, p2, } = this
  return jsArray2 (p1 | toString, p2 | toString)
}

Tree.prototype.toString = function () {
  return this
    | prop ('lines')
    | map (toString)
    | jsArray
}

Point.prototype.toJS = function () {
  const { row, column, } = this
  return [row, column]
}

Line.prototype.toJS = function () {
  const { p1, p2, } = this
  return [p1 | toJS, p2 | toJS]
}

Tree.prototype.toJS = function () {
  return this
    | prop ('lines')
    | map (toJS)
}
