import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, roll,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, blush, always, T, F,
  prop, path, has, hasIn,
  bindPropTo, bindProp, bindTo, bind,
  assoc, assocPath, assocM, assocPathM,
  concatTo, concat, appendTo, append,
  concatToM, concatM, appendToM, appendM,
  merge, mergeTo, mergeM, mergeToM,
  mergeIn, mergeInTo, mergeInM, mergeInToM,
  updateM, update, updatePathM, updatePath,
  lets, letS, compactOk, compact,
  die, raise, decorateException, exception,
  lt, gt, eq, ne, lte, gte,
  factory, factoryProps,
  rangeToBy,
} from 'stick-js/es'

const cata = dot1 ('cata')

import {
  toString,
  Point, Line, Tree, Height,
  BlockAnd, BlockOr, ConnectBlock,
  And, Or, AndLine, OrLine,
} from './types'

const combine = (xs, ys) => [...xs, ...ys]

const reducer = (accHeight, v_v) => (acc, i) => lets (
  _ => accHeight + i,
  (j) => [
    Line (Point (j, v_v ()), Point (j - 2, v_v ())),
    Line (Point (j, v_v ()), Point (j, 0))
  ],
  (j, xs) => combine (acc, xs),
)

const reduceAndOr = (accHeight, v_v) => reduce (reducer (accHeight, v_v))

const getV_V = cata ({
  And: _ => 1,
  Or: _ => 2,
})

const getBoolLine = prop ('line')

const isAndLine = cata ({
  AndLine: T,
  OrLine: F,
})

const block = condS ([
  isAndLine | guard (_ => Tree >> BlockAnd),
  otherwise | guard (_ => Tree >> BlockOr),
])

// @todo use start instead of xs
export const andOrFunction = (start, xs, accHeight) => {
  return lets (
  // assume not empty
  () => _ => getV_V (xs | head),
  () => rangeToBy (2) (2 * (xs.length + 1)) (2),
  (_, ys) => [getBoolLine (start)],
  (v_v, ys, start_) => ys | reduceAndOr (accHeight, v_v) (start_) | block (start),
)
}
