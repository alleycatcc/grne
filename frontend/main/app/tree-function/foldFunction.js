import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, roll,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, blush, always, T, F,
  prop, path, has, hasIn,
  bindPropTo, bindProp, bindTo, bind,
  assoc, assocPath, assocM, assocPathM,
  concatTo, concat, appendTo, append,
  concatToM, concatM, appendToM, appendM,
  merge, mergeTo, mergeM, mergeToM,
  mergeIn, mergeInTo, mergeInM, mergeInToM,
  updateM, update, updatePathM, updatePath,
  lets, letS, compactOk, compact,
  die, raise, decorateException, exception,
  lt, gt, eq, ne, lte, gte,
  factory, factoryProps,
} from 'stick-js/es'

import {
  toString,
  Point, Line, Tree, Height,
  BlockAnd, BlockOr, ConnectBlock,
  And, Or, AndLine, OrLine,
  blockTree, makeTreeFromBlocks,
  combineTrees,
} from './types'

import { andOrFunction, } from './andOrFunction'

const { log, } = console

// import util from 'util'
// const inspect = x => util.inspect (x, { depth: null, colors: process.stdout.isTTY, })
// const xlogWith = header => (...args) => log (inspect ([header, ...args]))

const logWith = header => noop

const length = prop ('length')
const cata = dot1 ('cata')

// const getNewHeight = (x, h, l, n, m) => x | cata ({
  // And: _ => Height (h + 2*l + 2),
  // Or: _ => cond (
    // (_ => n === 0) | guard (_ => Height (h + 2*l + 2)),
    // (_ => m === 0) | guard (_ => Height (h + 2*l    )),
    // otherwise      | guard (_ => Height (h + 2*l - 2)),
  // ),
// })
//

const getNewHeight = (x, h, l, n, m) => x | cata ({
  And: _ => h + 2*l + 2,
  Or: _ => cond (
    (_ => n === 0) | guard (_ => h + 2*l + 2),
    (_ => m === 0) | guard (_ => h + 2*l    ),
    otherwise      | guard (_ => h + 2*l - 2),
  ),
})

const getFoldVars = (ys, n, m, h) => {
  const [x, ...xs] = ys
  const [w, ...ws] = xs
  const l = ys | length
  const newHeight = getNewHeight (x, h, l, n, m)
  return { x, xs, w, ws, l, newHeight, }
}

const startLineOr  = h => OrLine  (Line (Point (h, 2), Point (h, 0)))
const startLineAnd = h => AndLine (Line (Point (h, 1), Point (h, 0)))

const connectDown = (l, n, h) => lets (
  _ => n + h + 2*l - 2,
  (s) => [
    Line (Point (h, 2), Point (s, 2)),
    Line (Point (s, 2), Point (s, 1)),
  ],
  (_, xs) => ConnectBlock (Tree (xs)),
)

const connectUp = (l, m, h) => lets (
  _ => h - 2 - m,
  (r) => [
    Line (Point (r, 1), Point (r, 2)),
    Line (Point (r, 2), Point (h, 2)),
  ],
  (_, xs) => ConnectBlock (Tree (xs)),
)

const f = (ys, h, m, n) => {
  return cata ({
  And: _ => blockTree (andOrFunction (startLineAnd (h), ys, h)),
  Or: _ => cond (
    (_ => m === 0 && n === 0) | guard (
      _ => blockTree (andOrFunction (startLineOr (h), ys, h))
    ),
    (_ => m === 0) | guard (
      _ => makeTreeFromBlocks ([
        andOrFunction (startLineOr (h), ys | tail, h),
        connectDown (ys | length, n + 2, h) | tap (logWith ('f, m = 0, connectDown'))
      ])
    ),
    (_ => n === 0) | guard (
      _ => makeTreeFromBlocks ([
        connectUp (ys | length, m, h) | tap (logWith (`f, n = 0, connectUp, ys=${ys}`)),
        andOrFunction (startLineOr (h), ys | tail, h),
      ]) | tap (logWith ('f, n = 0, makeTreeFromBlocks'))
    ),
    (_ => ys | length | lt (2)) | guard (
      _ => makeTreeFromBlocks ([
        connectUp (ys | length, m, h),
        connectDown (ys | length, n, h),
      ])
    ),
    otherwise | guard (
      _ => makeTreeFromBlocks ([
        connectUp (ys | length, m, h),
        andOrFunction (startLineOr (h), ys | tail | tail, h),
        connectDown (ys | length, n, h),
      ]),
    ),
  )
})}

// f ([And, And], 0, 0, 0) (And) | tap (logWith ('f [And, And]'))

export const foldFunction = ([accHeight, accTree], [m, ys, n]) =>
  lets (
    () => getFoldVars (ys, n, m, accHeight) | tap (logWith ('getFoldVars')),
    ({ x, }) => f (ys, accHeight, m, n) (x) | tap (logWith ('foldFunction newTree')),
    ({ newHeight }, newTree) => [newHeight, combineTrees (accTree, newTree)],
  )
