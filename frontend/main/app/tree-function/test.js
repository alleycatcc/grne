#!/usr/bin/env node


import fishLib, {
  log, info, warn, error, green, yellow, magenta, brightRed, cyan, brightBlue,
  sprintf, forceColors, getopt, shellQuote,
} from 'fish-lib'

import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, roll,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, blush, always, T, F,
  prop, path, has, hasIn,
  bindPropTo, bindProp, bindTo, bind,
  assoc, assocPath, assocM, assocPathM,
  concatTo, concat, appendTo, append,
  concatToM, concatM, appendToM, appendM,
  merge, mergeTo, mergeM, mergeToM,
  mergeIn, mergeInTo, mergeInM, mergeInToM,
  updateM, update, updatePathM, updatePath,
  lets, letS, compactOk, compact,
  die, raise, decorateException, exception,
  lt, gt, eq, ne, lte, gte,
  factory, factoryProps,
  rangeToBy,
  repeatSide, take,
} from 'stick-js/es'

const reverse = dot ('reverse')

import {
  toString,
  Point, Line, Tree, Height,
  BlockAnd, BlockOr, ConnectBlock,
  And, Or, AndLine, OrLine,
} from './types'

import { makeTree, } from './index'

import util from 'util'
const inspect = x => util.inspect (x, { depth: null, colors: process.stdout.isTTY, })

const p1 = Point (0, 0)
const p2 = Point (0, 1)

const p3 = Point (0, 2)

const andline = Line (p1, p2)
const orline = Line (p1, p3)

makeTree ([And, And]) | toString | log
makeTree ([And, Or]) | toString | log

const tests = [

    [[Or],Tree ([
    Line (Point (0,2),Point (0,0)),
    Line (Point (2,2),Point (0,2)),
    Line (Point (2,2),Point (2,0))])],

    [[And],Tree ([
    Line (Point (0,1),Point (0,0)),
    Line (Point (2,1),Point (0,1)),
    Line (Point (2,1),Point (2,0))])],

    [[Or,Or],Tree ([
    Line (Point (0,2),Point (0,0)),
    Line (Point (2,2),Point (0,2)),
    Line (Point (2,2),Point (2,0)),
    Line (Point (4,2),Point (2,2)),
    Line (Point (4,2),Point (4,0))])],

    [[And,And],Tree ([
    Line (Point (0,1),Point (0,0)),
    Line (Point (2,1),Point (0,1)),
    Line (Point (2,1),Point (2,0)),
    Line (Point (4,1),Point (2,1)),
    Line (Point (4,1),Point (4,0))])],

    [[And, Or],Tree ([
    Line (Point (0,1),Point (0,0)),
    Line (Point (2,1),Point (0,1)),
    Line (Point (2,1),Point (2,0)),
    Line (Point (1,1),Point (1,2)),
    Line (Point (1,2),Point (4,2)),
    Line (Point (4,2),Point (4,0))])],

    [[Or,And],Tree ([
    Line (Point (0,2),Point (0,0)),
    Line (Point (0,2),Point (3,2)),
    Line (Point (3,2),Point (3,1)),
    Line (Point (2,1),Point (2,0)),
    Line (Point (4,1),Point (2,1)),
    Line (Point (4,1),Point (4,0))])],

    [[And,Or,And],Tree ([
    Line (Point (0,1),Point (0,0)),
    Line (Point (2,1),Point (0,1)),
    Line (Point (2,1),Point (2,0)),
    Line (Point (1,1),Point (1,2)),
    Line (Point (1,2),Point (4,2)),
    Line (Point (4,2),Point (5,2)),
    Line (Point (5,2),Point (5,1)),
    Line (Point (4,1),Point (4,0)),
    Line (Point (6,1),Point (4,1)),
    Line (Point (6,1),Point (6,0))])],

    [[Or,And,Or],Tree ([
    Line (Point (0,2),Point (0,0)),
    Line (Point (0,2),Point (3,2)),
    Line (Point (3,2),Point (3,1)),
    Line (Point (2,1),Point (2,0)),
    Line (Point (4,1),Point (2,1)),
    Line (Point (4,1),Point (4,0)),
    Line (Point (3,1),Point (3,2)),
    Line (Point (3,2),Point (6,2)),
    Line (Point (6,2),Point (6,0))])],

    [[And,Or,Or,And],Tree ([
    Line (Point (0,1),Point (0,0)),
    Line (Point (2,1),Point (0,1)),
    Line (Point (2,1),Point (2,0)),
    Line (Point (1,1),Point (1,2)),
    Line (Point (1,2),Point (4,2)),
    Line (Point (4,2),Point (4,0)),
    Line (Point (4,2),Point (7,2)),
    Line (Point (7,2),Point (7,1)),
    Line (Point (6,1),Point (6,0)),
    Line (Point (8,1),Point (6,1)),
    Line (Point (8,1),Point (8,0))])]
]

const runTests = (ts, verbose) => {
  const mapperVerbose = (t) => {
    const [input, expected] = t
    const expectString = expected | toString
    const output = makeTree (input) | toString
    const ok = expectString === output
    if (ok) return info ('✔ ok')
    ; [output, expectString] | sprintfN ('✘ not ok,\n got     =%s\n expected=%s') | info
  }
  const mapperQuiet = (t) => {
    const [input, expected] = t
    makeTree (input) | toString
  }
  const mapper = verbose ? mapperVerbose : mapperQuiet
  ts | map (mapper)
}

const t1 = new Date
const n = 10000
const theTests = tests | reverse | take (1)
const numTests = theTests.length
const go = _ => runTests (theTests)
n | repeatSide (go)
const dt = new Date - t1
console.log ('dt', dt)
; [n * numTests, dt, dt / n / numTests] | sprintfN ('%s tests took %.2f (%.2f per test)') | info
