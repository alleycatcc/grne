import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, roll,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, blush, always, T, F,
  prop, path, has, hasIn,
  bindPropTo, bindProp, bindTo, bind,
  assoc, assocPath, assocM, assocPathM,
  concatTo, concat, appendTo, append,
  concatToM, concatM, appendToM, appendM,
  merge, mergeTo, mergeM, mergeToM,
  mergeIn, mergeInTo, mergeInM, mergeInToM,
  updateM, update, updatePathM, updatePath,
  lets, letS, compactOk, compact,
  die, raise, decorateException, exception,
  lt, gt, eq, ne, lte, gte,
  factory, factoryProps,
  rangeToBy,
  recurry,
} from 'stick-js/es'

import groupWith from 'ramda/es/groupWith'

import { zipWith3, } from 'alleycat-js/es/general'

import {
  toString, toJS,
  Point, Line, Tree, Height,
  BlockAnd, BlockOr, ConnectBlock,
  And, Or, AndLine, OrLine,
} from './types'

export { And, Or, toJS, }

import { foldFunction, } from './foldFunction'

const { log, } = console
const length = prop ('length')

const allButLast = (xs) => {
  const l = xs.length
  if (l === 0) return []
  return xs.slice(0, xs.length - 1)
}

const includeLengthPrevFol = (a) => lets (
  () => (x, y, z) => [x | length, y, z | length],
  () => [[], ...allButLast (a)],
  () => [...(a | tail), []],
  (f, b, c) => zipWith3 (f, b, a, c),
)

const groupBool = groupWith (eq)

export const makeTree = letS ([
  (bs) => [0, Tree ([])],
  (bs, acc) => includeLengthPrevFol (
    groupBool (bs)
  ),
  (bs, acc, xs) => xs | reduce (foldFunction, acc) | prop (1),
])

const _cache = {
  '0,0,0,1,1,1,0,1,1': [[[0,2],[0,0]],[[2,2],[0,2]],[[2,2],[2,0]],[[4,2],[2,2]],[[4,2],[4,0]],[[0,2],[9,2]],[[9,2],[9,1]],[[6,1],[6,0]],[[8,1],[6,1]],[[8,1],[8,0]],[[10,1],[8,1]],[[10,1],[10,0]],[[12,1],[10,1]],[[12,1],[12,0]],[[9,1],[9,2]],[[9,2],[14,2]],[[14,2],[16,2]],[[16,2],[16,1]],[[14,1],[14,0]],[[16,1],[14,1]],[[16,1],[16,0]],[[18,1],[16,1]],[[18,1],[18,0]]],
}

export const cache = x => _cache [x]
