import {
  pipe, compose, composeRight,
  noop, id, always,
  T, F, tap,
  concat, map,
  defaultTo,
  die, concatTo, precatTo,
  recurry,
} from 'stick-js/es'

import daggy from 'daggy'

import { Just, Nothing, cata, fold, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { logWith, iwarn, ierror, toString, } from 'alleycat-js/es/general'
import { ifTrueV, } from 'alleycat-js/es/predicate'

import config from './config'

const configTop = config | configure.init
const configFields = 'advancedSearch.fields' | configTop.get | configure.init
const {
  fulltext: fulltextField,
  hoofdW: hoofdWField,
  vertM: vertMField,
} = configFields.gets ('fulltext', 'hoofdW', 'vertM')

// --- @todo we could use the generic Request structure instead.
export const AdvSearchResults = daggy.taggedSum ('AdvSearchResults', {
  AdvSearchResultsInit: [],
  AdvSearchResultsLoading: [],
  AdvSearchResultsLoadingMore: ['metadata', 'results'],
  AdvSearchResultsError: ['reason'],
  AdvSearchResultsResults: ['metadata', 'results'],
})

const {
  AdvSearchResultsInit, AdvSearchResultsLoading, AdvSearchResultsLoadingMore,
  AdvSearchResultsError, AdvSearchResultsResults,
} = AdvSearchResults

AdvSearchResults.prototype.map = function (f) {
  return this | cata ({
    AdvSearchResultsInit: () => AdvSearchResultsInit,
    AdvSearchResultsLoading: () => AdvSearchResultsLoading,
    AdvSearchResultsLoadingMore: AdvSearchResultsLoadingMore,
    AdvSearchResultsError: AdvSearchResultsError,
    AdvSearchResultsResults: (metadata, results) => AdvSearchResultsResults (
      ...f ([metadata, results]),
    )
  })
}

// --- loading or loading more
export const advSearchIsLoadingAny = cata ({
  AdvSearchResultsInit: F,
  AdvSearchResultsLoading: T,
  AdvSearchResultsLoadingMore: T,
  AdvSearchResultsError: F,
  AdvSearchResultsResults: F,
})

export const advSearchIsLoadingMore = cata ({
  AdvSearchResultsInit: F,
  AdvSearchResultsLoading: F,
  AdvSearchResultsLoadingMore: T,
  AdvSearchResultsError: F,
  AdvSearchResultsResults: F,
})

// --- go from Results -> LoadingMore
export const advSearchToLoadingMore = cata ({
  AdvSearchResultsInit: _ => ierror ('Unexpected (advSearchToLoadingMore, AdvSearchResultsInit)'),
  AdvSearchResultsLoading: _ => ierror ('Unexpected (advSearchToLoadingMore, AdvSearchResultsLoading)'),
  AdvSearchResultsLoadingMore: _ => ierror ('Unexpected (advSearchToLoadingMore, AdvSearchResultsLoadingMore)'),
  AdvSearchResultsError: _ => ierror ('Unexpected (advSearchToLoadingMore, AdvSearchResultsError)'),
  AdvSearchResultsResults: (metadata, results) => AdvSearchResultsLoadingMore (metadata, results),
})

// --- go from Loading or LoadingMore -> Results
export const advSearchLoadingXToResults = (metadata, results) => cata ({
  AdvSearchResultsInit: _ => ierror ('Unexpected'),
  AdvSearchResultsLoading: _ => AdvSearchResultsResults (metadata, results),
  AdvSearchResultsLoadingMore: (metadataPrev, resultsPrev) =>
    AdvSearchResultsResults (metadataPrev, resultsPrev | concat (results)),
  AdvSearchResultsError: _ => ierror ('Unexpected'),
  AdvSearchResultsResults: _ => ierror ('Unexpected'),
})

// --- true on Loading, but not LoadingMore.
export const advSearchLoadingFirst = cata ({
  AdvSearchResultsInit: F,
  AdvSearchResultsLoading: T,
  AdvSearchResultsLoadingMore: F,
  AdvSearchResultsResults: F,
  AdvSearchResultsError: F,
})

export const advSearchGetState = (numResultsMbArg) => cata ({
  AdvSearchResultsInit: () => [false, false, null, null, null],
  AdvSearchResultsLoading: () => [false, false, null, null, null],
  AdvSearchResultsLoadingMore: (_, results) => [true, true, numResultsMbArg, results, null],
  AdvSearchResultsResults: (_, results) => [true, false, numResultsMbArg, results, null],
  AdvSearchResultsError: (err) => [false, false, null, null, err | defaultTo (T)],
})

export { AdvSearchResultsInit, AdvSearchResultsLoading, AdvSearchResultsLoadingMore, AdvSearchResultsError, AdvSearchResultsResults, }

// --- The 'aux' field is the field which at least one of the terms should match, i.e., the # clause in the P-expr.

export const AdvSearchAuxField = daggy.taggedSum ('AdvSearchAuxField', {
  AdvSearchAuxFieldFulltext: [],
  AdvSearchAuxFieldVertM: [],
  AdvSearchAuxFieldHoofdW: [],
})

const { AdvSearchAuxFieldFulltext, AdvSearchAuxFieldVertM, AdvSearchAuxFieldHoofdW, } = AdvSearchAuxField
export { AdvSearchAuxFieldFulltext, AdvSearchAuxFieldVertM, AdvSearchAuxFieldHoofdW, }

export const searchFieldAuxIsFullText = cata ({
  AdvSearchAuxFieldFulltext: T,
  AdvSearchAuxFieldVertM: F,
  AdvSearchAuxFieldHoofdW: F,
})

export const searchFieldAuxDataToText = cata ({
  AdvSearchAuxFieldFulltext: () => fulltextField,
  AdvSearchAuxFieldVertM: () => vertMField,
  AdvSearchAuxFieldHoofdW: () => hoofdWField,
})

/*
 * For some piece of data which is retrieved via an API call.
 * You can store what you like in RequestResults.
 * In GrNe the API calls usually return metadata and results. Normally we deal with the metadata in
 * some way in the reducer and store the results in RequestResults.
 * In the case of advanced search the metadata is also stored in the Results structure. If we were
 * to use this structure instead of a separate one, we could store [metadata, results] as an array.
 */

export const Request = daggy.taggedSum ('Request', {
  RequestInit: [],
  // --- combiner is a function, e.g. `concat`, telling us how to combine new results with existing
  // ones -- useful for e.g. an infinite scroll loading more results.
  // --- combinerMb is an array [currentResults, combiner, tag], where tag is a string or a datatype
  // which can be used to find out what this combiner will do.
  // --- @todo this array should probably be its own datatype.
  RequestLoading: ['combinerMb'],
  RequestError: ['reason'],
  RequestResults: ['results'],
})

const { RequestInit, RequestLoading, RequestError, RequestResults, } = Request

export { RequestInit, RequestLoading, RequestError, RequestResults, }

export const requestHasResults = cata ({
  RequestInit: F,
  RequestLoading: F,
  RequestError: F,
  RequestResults: T,
})

export const requestHasResultsOrPending = cata ({
  RequestInit: F,
  RequestLoading: T,
  RequestError: F,
  RequestResults: T,
})

export const requestState = cata ({
  RequestInit: () => [true, false, false, false],
  RequestLoading: () => [false, true, false, false],
  RequestResults: () => [false, false, true, false],
  RequestError: () => [false, false, false, true],
})

export const requestLoading = cata ({
  RequestInit: F,
  RequestLoading: T,
  RequestError: F,
  RequestResults: F,
})

export const requestLoadingDefault = () => Nothing | RequestLoading

Request.prototype.fold = function (f, y) {
  const z = y | always
  return this | cata ({
    RequestInit: z,
    RequestLoading: (combinerMb) => combinerMb | fold (
      ([results, ..._]) => results | f,
      y,
    ),
    RequestError: z,
    RequestResults: f,
  })
}

Request.prototype.map = function (f) {
  return this | cata ({
    RequestInit: () => RequestInit,
    RequestLoading: RequestLoading,
    RequestError: RequestError,
    RequestResults: f >> RequestResults,
  })
}

/* Indicates how the fetched results should interact with any results we already have. For example,
 * when fetching 'more 'in an infinite scrolling list, they should append.
 *
 * Currently this is sent as part of the action payload. Another way to do this in the future would
 * be to store it in the RequestLoading structure, and then, when the request is successful, using
 * a state transition to go from RequestLoading -> RequestResults, paying attention to the fetch
 * type, instead of (as we do now) going straight to RequestResults and reading the action payload
 * to get the type.
 */

export const Fetch = daggy.taggedSum ('Fetch', {
  FetchReplace: [],
  FetchAppend: [],
  FetchPrepend: [],
})

export const fetchIsAppend = cata ({
  FetchReplace: F,
  FetchAppend: T,
  FetchPrepend: F,
})

export const fetchIsPrepend = cata ({
  FetchReplace: F,
  FetchAppend: F,
  FetchPrepend: T,
})

export const fetchIsReplace = cata ({
  FetchReplace: T,
  FetchAppend: F,
  FetchPrepend: F,
})

const { FetchReplace, FetchAppend, FetchPrepend, } = Fetch
export { FetchReplace, FetchAppend, FetchPrepend, }

const Tab = daggy.taggedSum ('Tab', {
  TabHome: [],
  TabAnalyse: [],
  TabAnalyseXmlToHtmlPrint: [],
  TabColofon: [],
  TabFeedback: [],
  TabAdvancedSearch: [],
  TabBrowse: [],
  TabHulp: [],
})

const {
  TabHome, TabAnalyse, TabAnalyseXmlToHtmlPrint, TabColofon, TabFeedback, TabAdvancedSearch, TabBrowse, TabHulp,
} = Tab

export {
  TabHome, TabAnalyse, TabAnalyseXmlToHtmlPrint, TabColofon, TabFeedback, TabAdvancedSearch, TabBrowse, TabHulp,
}

const BrowseModeLemmata = daggy.taggedSum ('BrowseModeLemmata', {
  // --- 'normal' is the desktop version.
  BrowseModeLemmataNormal: [],
  // --- mobile only: you can browse through the lemmata but can't click expansions or links.
  BrowseModeLemmataExplore: [],
  // --- mobile: a single lemma is shown and you can click expansions and links.
  // ---  further divided into 'fixed' and 'abridge' variants, but that can only be decided after render.
  // ---  the argument is a BrowseModeLemmataSingleMode value:
  //   ...Init -> hasn't been determined yet (before render).
  //   ...Fixed -> no scrolling (lemma fits on screen)
  //   ...Abridge -> only scrolling through one lemma (lemma is taller than screen)
  BrowseModeLemmataSingle: ['singleMode'],
})

const {
  BrowseModeLemmataNormal, BrowseModeLemmataExplore, BrowseModeLemmataSingle,
} = BrowseModeLemmata

export {
  BrowseModeLemmataNormal, BrowseModeLemmataExplore, BrowseModeLemmataSingle,
}

const BrowseModeLemmataSingleMode = daggy.taggedSum ('BrowseModeLemmataSingleMode', {
  BrowseModeLemmataSingleModeInit: [],
  BrowseModeLemmataSingleModeFixed: [],
  BrowseModeLemmataSingleModeAbridge: ['height', 'offset'],
})

const { BrowseModeLemmataSingleModeInit, BrowseModeLemmataSingleModeFixed, BrowseModeLemmataSingleModeAbridge, } = BrowseModeLemmataSingleMode
export { BrowseModeLemmataSingleModeInit, BrowseModeLemmataSingleModeFixed, BrowseModeLemmataSingleModeAbridge, }

export const browseModeLemmataIsNormal = cata ({
  BrowseModeLemmataExplore: F,
  BrowseModeLemmataNormal: T,
  BrowseModeLemmataSingle: F,
})

export const browseModeLemmataIsExploring = cata ({
  BrowseModeLemmataExplore: T,
  BrowseModeLemmataNormal: F,
  BrowseModeLemmataSingle: F,
})

export const browseModeLemmataIsSingle = cata ({
  BrowseModeLemmataExplore: F,
  BrowseModeLemmataNormal: F,
  BrowseModeLemmataSingle: T,
})

export const browseModeLemmataIsAbridge = cata ({
  BrowseModeLemmataExplore: F,
  BrowseModeLemmataNormal: F,
  BrowseModeLemmataSingle: cata ({
    BrowseModeLemmataSingleModeInit: F,
    BrowseModeLemmataSingleModeFixed: F,
    BrowseModeLemmataSingleModeAbridge: T,
  }),
})

export const browseModeLemmataIsFixed = cata ({
  BrowseModeLemmataExplore: F,
  BrowseModeLemmataNormal: F,
  BrowseModeLemmataSingle: cata ({
    BrowseModeLemmataSingleModeInit: F,
    BrowseModeLemmataSingleModeFixed: T,
    BrowseModeLemmataSingleModeAbridge: F,
  }),
})

// --- toggle explore to the new state
// --- toggle single to explore if NOT cond
// --- keep single at single if cond
// --- keep normal at normal

export const browseModeLemmataToggleExploringWithCondition = (newState, cond) => cata ({
  BrowseModeLemmataNormal: BrowseModeLemmataNormal | always,
  BrowseModeLemmataExplore: newState | always,
  BrowseModeLemmataSingle: (...args) => cond | ifTrueV (
    BrowseModeLemmataSingle (...args), BrowseModeLemmataExplore,
  ),
})

const ScrollListener = daggy.taggedSum ('ScrollListener', {
  ScrollListenerIdle: [],
  ScrollListenerBlocked: ['timeoutId'],
  ScrollListenerActive: ['timeoutId'],
})

const { ScrollListenerIdle, ScrollListenerBlocked, ScrollListenerActive, } = ScrollListener
export { ScrollListenerIdle, ScrollListenerBlocked, ScrollListenerActive, }

export const scrollListenerIsBlocked = cata ({
  ScrollListenerIdle: F,
  ScrollListenerBlocked: T,
  ScrollListenerActive: F,
})

const Crop = daggy.taggedSum ('Crop', {
  CropNone: [],
  CropFake: ['showIdx', 'numBefore', 'numAfter'],
  CropReal: ['beginIdx', 'endIdx'],
})

const { CropNone, CropFake, CropReal, } = Crop
export { CropNone, CropFake, CropReal, }

/*
 * Transforms Results to Loading and throws an error on other states.
 * Uses the function `f` (e.g. `concatTo` for an infinite scroll) to combine the eventually fetched
 * results with the existing ones.
 */

export const requestResultsCombine = recurry (3) (
  f => tag => cata ({
	RequestInit: () => die ('requestResultsCombine: invalid (RequestInit)'),
	RequestLoading: () => die ('requestResultsCombine: invalid (RequestLoading)'),
	RequestError: () => die ('requestResultsCombine: invalid (RequestError)'),
	RequestResults: (results) => RequestLoading (Just ([results, f, tag])),
  }),
)

/* If loading, returns Just String or Just null.
 * If not loading, returns Nothing.
 */

export const requestLoadingTag = cata ({
  RequestInit: Nothing | always,
  RequestLoading: (combinerMb) => combinerMb
    | fold (([results, f, tag]) => tag, null)
    | Just,
  RequestError: Nothing | always,
  RequestResults: Nothing | always,
})

/*
 * Used during the 'fetch' stage (loading results) and results in a `RequestLoading` state with the
 * right combiner function.
 */

export const fetchUpdater = fetchType => results => fetchType | cata ({
  FetchReplace: () => requestLoadingDefault (),
  FetchAppend: () => results | requestResultsCombine (concatTo, FetchAppend),
  FetchPrepend: () => results | requestResultsCombine (precatTo, FetchPrepend),
})

/*
 * @todo better name.
 *
 * Transforms Loading to Results and throws an error on other states.
 * If the Loading state contains a combiner function, use it to combine the results with the
 * existing ones; otherwise, replace the results.
 */

export const requestResultsCombine2 = results => cata ({
  RequestInit: () => die ('requestResultsCombine2: invalid (RequestInit)'),
  RequestError: () => die ('requestResultsCombine2: invalid (RequestError)'),
  RequestResults: () => die ('requestResultsCombine2: invalid (RequestResults)'),
  RequestLoading: (combinerMb) => combinerMb
    | fold (([cur, f, ..._]) => f (cur, results), results)
    | RequestResults,
})

