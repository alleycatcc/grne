import {
  pipe, compose, composeRight,
  againstBoth, ne, not, eq,
  concatTo, lt, bindProp, join, sprintf1,
  noop, bindTryProp, defaultTo,
  lets, invoke,
  T, F,
  prop,
  recurry,
  mergeM, tap, whenOk,
  cond, gt, guard, otherwise, divideBy, concat,
  condS,
  arg0, side2, remapTuples, reduce,
} from 'stick-js/es'

import emailValidator from 'email-validator'

import configure from 'alleycat-js/es/configure'
import { logWith, getQueryParams, } from 'alleycat-js/es/general'
import { all, allV, } from 'alleycat-js/es/predicate'
import { reducerTell, } from 'alleycat-js/es/redux'

import {
  mlt, mgt, mediaRule,
} from 'alleycat-js/es/styled'

import config from './config'
import { envIsDev, } from './env'

const configTop = config | configure.init

export const log = console | bindProp ('log')
export const debug = console | bindTryProp ('debug') | defaultTo (() => log)

export const debugIt = (...args) => debug ('[debug] *', ...args)
export const debugDev = envIsDev ? debugIt : noop
export const debugDevWith = header => (...args) => debugDev (... [header, ...args])

; `
These are based on the Bootstrap 4 breakpoints.

Note that these are from the given width *and up*; in other words, mediaTablet is tablets and all
desktops, etc.

For GrNe we generally need mediaPhone, mediaTablet, and mediaDesktop.
`

const mediaPhoneWidth      = 0
const mediaPhoneBigWidth   = 576
const mediaTabletWidth     = 768
const mediaDesktopWidth    = 992
const mediaDesktopBigWidth = 1200

export const mediaPhone      = mediaPhoneWidth      | mgt | mediaRule
export const mediaPhoneBig   = mediaPhoneBigWidth   | mgt | mediaRule
export const mediaTablet     = mediaTabletWidth     | mgt | mediaRule
export const mediaDesktop    = mediaDesktopWidth    | mgt | mediaRule
export const mediaDesktopBig = mediaDesktopBigWidth | mgt | mediaRule

export const isMobileWidth = mediaTabletWidth | lt

export const scrollIntoViewSmooth = (opts={}) => (elem) => elem.scrollIntoView ({
  behavior: 'smooth',
  block: 'center',
  ...opts,
})

export const scrollIntoViewAuto = (opts={}) => (elem) => elem.scrollIntoView ({
  behavior: 'auto',
  block: 'center',
  ...opts,
})

export const browseLink = String >> concatTo ('/browse/')
export const browseLinkForRouter = String >> concatTo ('/browse/')

export const browseToId = recurry (2) (
  (history) => (id) => browseTo (history) (id | browseLink)
)

export const browseTo = recurry (2) (
  (history) => (to) => history.push (to),
)

window | mergeM ({
  browseToId, browseTo,
})

export const isValidEmail = emailValidator | bindProp ('validate')

// --- note, user agent tests are never totally reliable.
// --- see mdn docs for a pretty good break-down.

export const isSafari = () => lets (
  () => window.navigator.userAgent,
  (ua) => allV (
    ua.indexOf ('Safari') !== -1,
    ua.indexOf ('Chrome') === -1,
    ua.indexOf ('Chromium') === -1,
  ),
)

/* Momentum scroll seems to be causing problems (flickering, bouncing weird & possibly causing other
 * problems).
 * Seen on iPhone 5 (iOS11 / 605) and iPhone SE.
 * For now, disabling on all (mobile) Safaris.
 */
export const shouldDisableMomentumScroll = invoke (() => {
  const cutoff = Infinity

  const q = getQueryParams ()
  if (q.momentum === '1') return F

  return () => lets (
    () => window.navigator.userAgent,
    (ua) => all (
      () => isSafari (),
      () => ua.match (/AppleWebKit\/(\d+)/),
      (_, m) => m | prop (1) | Number | lt (cutoff),
    ),
  )
})

/*
 * Safari on iPhone is giving a lot of problems related to single/abridge mode.
 * This is a stop-gap measure to just disable single/abridge on mobile and (effectively) always use
 * BrowseModeLemmataExplore.
 */

export const disableSingle = getQueryParams >> prop ('disableSingle') >> eq ('1')

export const onKruimelItemClickStandard = (history) => (link) =>
  () => browseTo (history, link)

export const conformsTo = (shape) => (o) => {
  for (const k in shape) {
    const p = shape [k]
    if (!p (o [k])) return false
  }
  return true
}

export const checkUploadFilesSize = ({
  file,
  maxFileSize: max = 1024 * 1024,
  prettyBytesDecimalPlaces: places = 0,
  alertFunc = noop,
}) => file.size | condS ([
  max | gt | guard (_ => max
    | prettyBytes (places)
    | sprintf1 ('File too large! (max = %s)')
    | tap (alertFunc)
    | F
  ),
  otherwise | guard (T),
])

export const prettyBytes = invoke (() => {
  const row = recurry (4) (
    fmt => pred => n => suffix =>
      Math.pow (1024, n + 1) | pred | guard (
        arg0 >> divideBy (Math.pow (1024, n)) >> sprintf1 (fmt) >> concat (' ' + suffix)
      )
  )

  return numDecimals => lets (
    _ => numDecimals | sprintf1 ('%%.%sf'),
    (fmt) => row (fmt),
    (_, rowFmt) => condS ([
      rowFmt (lt, 0, 'b'),
      rowFmt (lt, 1, 'k'),
      rowFmt (lt, 2, 'M'),
      rowFmt (_ => T, 3, 'G'),
    ]),
  )
})

export const makeFormData = invoke (() => {
  const app = side2 ('append')
  return (data) => data
    | remapTuples (app)
    | reduce (pipe, new FormData ())
})

export const reducer = 'debug.reducers' | configTop.get | reducerTell
