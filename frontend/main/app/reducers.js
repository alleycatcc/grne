import {
  pipe, compose, composeRight,
  assoc,
  id,
  condS, guard, otherwise, guardV,
  eq, ifOk,
} from 'stick-js/es'

import { combineReducers, } from 'redux'

const idReducer = state => state

export default function createReducer (injectedReducers) {
  return injectedReducers | ifOk (
    (rs) => combineReducers ({ ...rs, }),
    () => idReducer,
  )
}
