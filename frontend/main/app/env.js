import {
  pipe, compose, composeRight,
  againstBoth, ne, eq, not,
} from 'stick-js/es'

export const grneEnv = process.env.GRNE_ENV || 'blah'
export const envIsNotPrdLike = grneEnv | againstBoth (ne ('acc'), ne ('prd'))
export const envIsPrdLike = envIsNotPrdLike | not
export const envIsPrd = grneEnv | eq ('prd')
export const envIsNotPrd = envIsPrd | not
export const envIsAcc = grneEnv | eq ('acc')
export const envIsTst = grneEnv | eq ('tst')
export const envIsDev = grneEnv | eq ('dev')
export const envIsNotTst = envIsTst | not
export const envIsTstLike = envIsTst || envIsDev
export const envIsNotTstLike = envIsTstLike | not
