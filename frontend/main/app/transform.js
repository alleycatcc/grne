import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, recurry, roll,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, blush, always, T, F,
  prop, path, has, hasIn,
  bindPropTo, bindProp, bindTo, bind,
  assoc, assocPath, assocM, assocPathM,
  concatTo, concat, appendTo, append,
  concatToM, concatM, appendToM, appendM,
  merge, mergeTo, mergeM, mergeToM,
  mergeIn, mergeInTo, mergeInM, mergeInToM,
  updateM, update, updatePathM, updatePath,
  lets, letS, compactOk, compact,
  die, raise, decorateException, exception,
  lt, gt, eq, ne, lte, gte,
  factory, factoryProps,
  reduceObj, not,
} from 'stick-js/es'

/* @todo make more generic
 * See App/saga for an example of how to use this.
 */

import { Left, Right, fold, flatMap, liftA2, liftA2N, } from 'alleycat-js/es/bilby'
import { logWith, } from 'alleycat-js/es/general'

const isUndefined = void 8 | eq
const isDefined = isUndefined >> not
const ifDefined = isDefined | ifPredicate

// --- reduce a table whose values are objects matching the spec.
export const objObjReducer = spec => data => data
  | reduceObj (spec | reducerObjectObject) ([] | Right)

export const objReducer = spec => data => data
  | (spec | reducerObject)

const specReducer = (result) => (accEither, [setKey, getKey, errMsg]) => accEither | flatMap (
  (acc) => result | getKey | ifDefined (
    val => acc | mergeM ({ [setKey]: val, }) | Right,
    () => errMsg | concatTo ('Bad value: ') | Left,
  ),
)

// --- reduce an object whose values are objects to an array of objects.
const reducerObjectObject = (spec) => (accEither, [_, value]) => {
  const inner = spec | reduce (value | specReducer) ({} | Right)
  return accEither | flatMap (
    (acc) => inner | fold (
      Left,
      o => acc | appendM (o) | Right,
    )
  )
}

// --- reduce an object to an object.
const reducerObject = spec => data =>
  spec | reduce (data | specReducer) ({} | Right)

