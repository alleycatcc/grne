import {
  pipe, compose, composeRight,
  join, take, id, lets,
} from 'stick-js/es'

import { fontFace, cssFont, } from 'alleycat-js/es/font'

import { envIsDev, envIsNotPrd, } from './env'

const config = {
  description: '',
  debug: {
    reducers: envIsDev,
    rerender: envIsDev,
  },
  font: {
    main: {
      css: getMainFontCss (),
      family: 'Source Sans Pro',
    },
    analyse: {
      css: cssFont ('https://fonts.googleapis.com/css?family=EB+Garamond'),
      family: 'EB Garamond',
    },
  },
  analyse: {
    maxExportZipUploadSize: 10 * 1024 * 1024,
  },
  // --- MenuM and EntriesM, in px
  panelWidth: 200,
  images: {
    arrow: {
      up: require ('./images/arrow-up.png'),
      down: require ('./images/arrow-down.png'),
    },
    header: require ('./images/header.jpg'),
    headerSmall: require ('./images/header-small.jpg'),
    keyboard: require ('./images/keyboard.jpg'),
    menuPageOver: require ('./images/menu_page_over.jpg'),
    hulpOne: require ('./images/hulp-2019/hulppagina_2019-01.png'),
    hulpTwo: require ('./images/hulp-2019/hulppagina_2019-02.png'),
    hulpThree: require ('./images/hulp-2019/hulppagina_2019-03.png'),
    hulpFour: require ('./images/hulp-2019/hulppagina_2019-04.png'),
    hulpFive: require ('./images/hulp-2019/hulppagina_2019-05.png'),
    hulpSix: require ('./images/hulp-2019/hulppagina_2019-06.png'),
    hulpSeven: require ('./images/hulp-2019/hulppagina_2019-07.png'),
    hulpEight: require ('./images/hulp-2019/hulppagina_2019-08.png'),
    hulpNine: require ('./images/hulp-2019/hulppagina_2019-09.png'),
    hulpTen: require ('./images/hulp-2019/hulppagina_2019-10.png'),
  },
  colors: {
    // --- deep red hoofdW adv. search results.
    theme2: '#7b2d13',
  },
  showSchoolVersion: envIsNotPrd,
  advancedSearch: {
    // --- must match backend.
    fields: {
      fulltext: '_text_',
      hoofdW: 'hoofdW-combined',
      vertM: 'vertM-combined',
    },
  },
  placeholder: {
    normalLem: 'Bijv. egw, e)gw/, εγω, ἐγώ',
    normalPars: 'Bijv. edahn, e)da/hn, εδαην, ἐδάην',
    advanced: 'Bijv. kudos, van vader, peri*',
  },
  footer: {
    _info: [
      ['Amsterdam University Press', 'http://www.aup.nl/'],
      ['Universiteit Leiden', 'http://woordenboekgrieks.leidenuniv.nl/'],
      ['AlleyCat Amsterdam', 'https://alleycat.cc'],
      ['TAT Zetwerk', 'http://www.tatzetwerk.nl/'],
    ],
    info: isMobile => lets (
      () => isMobile ? take (3) : id,
      (f) => config.footer._info | f,
    ),
  },
  colofon: {
    contributors: [
      ['Hoofdredactie', null, 3, [
        'Ineke Sluiter (Universiteit Leiden)',
        'Lucien van Beek (Universiteit Leiden)',
        'Ton Kessels (Radboud Universiteit Nijmegen)',
        'Albert Rijksbaron (Universiteit van Amsterdam, 1943–2023)'
      ]],
      ['Projectcoördinatie', null, 2, [
        'Dr. Lucien van Beek (2015–)',
        'drs. Michiel Cock (tot jan. 2015)'
      ]],
      ['Lemmabouwers (eerste conceptversies)', null, 3, [
        'Dr. Daniël Bartelds',
        'Stephen van Beek, MA',
        'Dr. Michel Buijs',
        'Drs. Michiel Cock',
        'Bob Corthals, MPhil',
        'Drs. David van Eijndhoven',
        'Nina Kroese, MA',
        'Dr. Arjan Nijk',
        'Dr. Stefan Norbruis',
        'Dr. Remco Regtuit',
        'Dr. Janric van Rookhuijzen',
        'Dr. Peter Stork',
      ]],
      ['Eindredactie', null, 2, [
        'Dr. Daniël Bartelds (v.a. mei 2014)',
        'Dr. Lucien van Beek (v.a. feb. 2015)',
        'Stephen van Beek, MA (2017-maart 2023)',
        'Drs. Michiel Cock (2012–2014)',
        'Prof.dr. Ton Kessels',
        'Dr. Stefan Norbruis (v.a. maart 2023)',
        'Dr. Janric van Rookhuijzen (v.a. april 2022)',
        'Prof.dr. Ineke Sluiter',
        'Jan Vonk (tot aug. 2015)',
      ]],
      ["Technische ontwikkeling",
        [
          ['','Digitaal Productiecentrum UvA'],
          ['https://alleycat.cc', 'AlleyCat'],
        ],
        2,
        [
          'Ir. Caspar Treijtel (DPC)',
          'Drs. Joris Osterhaus, MA (DPC)',
          'Hans Scholte (DPC)',
          'Drs. Jan de Jong (DPC)',
          'Allen Haim (AlleyCat)',
          'Arie Blom (AlleyCat)',
          'Anton Luccioni (AlleyCat)',
        ],
      ],
      [ "Ontwerp",
        // null,
        [
          ["http://www.tatzetwerk.nl", 'TAT Zetwerk'],
          ["https://gijsmathijs.nl/", "Gijs Matthijs Ontwerpers"],
        ],
        2,
        [
          'Ivo Geradts (binnenwerk boek)',
          'Gijs Klundert (Gijs Mathijs Ontwerpers) (omslag boek)',
        ]
      ],
      ['Uitgever', [["http://www.aup.nl", "Amsterdam University Press"]], 2, [
        'Irene van Rossum',
        'Rixt Runia (1963–2020)',
        'Saskia de Vries (tot 2012)',
        'Jan Peter Wissink',
      ]],
      ["Verzameling basismateriaal (studenten en vrijwilligers)", null, 3, [
        'Saskia Aerts (UL)',
        'Drs. Guus Bal (UvA)',
        'Eefje Bekkenutte (RU)',
        'Baukje van den Berg (RUG)',
        'Sanne van den Berg (VUA)',
        'Ruth Bootsma (RU)',
        'Nick Breur, MA (docent)',
        'Chelsea O\'Brien (UvA)',
        'Lineke Brink (UL)',
        'Margriet ten Broecke (VUA)',
        'Jurjen Broers (RUG)',
        'Richard Calis (UvA)',
        'Drs. Alwies Cock (docent)',
        'Bob Corthals (UL)',
        'Marianne van Dijk (RU)',
        'Erik-Jan Dros (UL)',
        'Polle Dujardin (UL)',
        'Pim Faessen (RU)',
        'Figen Geerts (UvA)',
        'Drs. Fred Graumans',
        'Drs. Lizzy Groot',
        'Thomas den Haan (UvA)',
        'Suzanne van Helvoirt (RU)',
        'Karin van den Heuvel (RU)',
        'Liesbeth Hofstede (UL)',
        'Maarten den Hollander (UL)',
        'Bert ter Horst (RU)',
        'Justin Jacobse',
        'Leanne Jansen (UL)',
        'Maria Klomp',
        'Shane Kolenbrander (UL)',
        'Jo-ann de Kort (RU)',
        'Elly Koutamanis',
        'Inge Kraus (UL)',
        'Nina Kroese (UL)',
        'Mike Kruijer (UvA)',
        'Anton Manders (RU)',
        'Tomas Meijs (RU)',
        'Rève Mosman (VUA)',
        'Hanna Mosterdijk (UL)',
        'Renée Müskens (RU)',
        'Daan Mulder (UL)',
        'Stefan Norbruis (VUA/UL)',
        'Eline Nuninga (RUG)',
        'Drs. Karin Oomen (docent)',
        'Steven Ooms (UL)',
        'Vincent Oort (UL)',
        'Edgar Pasma (UvA)',
        'Philip van Reeuwijk (UvA)',
        'Eveline van Rijn (UL)',
        'Inge Rodenburg (UL)',
        'Janric van Rookhuijzen (RU)',
        'Eveline Rutten (RU)',
        'Drs. Michiel Simons (docent)',
        'Myrthe Spitzen (RU)',
        'Miba Stierman',
        'Dr. Peter Stork (UL)',
        'Denise Telder (UL)',
        'John Tholen (RU)',
        'Sanne Toornstra (RUG)',
        'Paul van Uum (RU)',
        'Esmée Veldhuis (RU)',
        'Bob van Velthoven (UL)',
        'Sietse Venema (RU)',
        'Floris Verhaart (UL)',
        'Gary Vos (UL)',
        'Bas Wagenaar (VUA)',
        'Silvia de Wild (RUG)',
        'Anne Winnubst (UL)',
        'Antti Yang (UL)',
        'Matthijs Zoeter (RU)',
        'Sanne van der Zwet (UL)',
      ]],
      ['Student-assistentie', null, 3, [
        'Robin Bakker (UL)',
        'Daniël Bartelds (UL)',
        'Myrthe Bartels (UL)',
        'Bob Corthals (UL)',
        'Kyo Gerrits (UvA)',
        'Henric Jansen (UL)',
        'Nina Kroese (UL)',
        'Olivia Monster (UL)',
        'Luc Segers (UL)',
      ]]
    ],
  },
}

export default config

function getMainFontCss () {
  return [
    fontFace (
      'Source Sans Pro',
      [
        [
          require ('fonts/source-sans-pro/EOT/SourceSansPro-Regular.eot'),
          'embedded-opentype',
        ],
        [
          require ('fonts/source-sans-pro/WOFF/OTF/SourceSansPro-Regular.otf.woff'),
          'woff',
        ],
        [
          require ('fonts/source-sans-pro/OTF/SourceSansPro-Regular.otf'),
          'opentype',
        ],
        [
          require ('fonts/source-sans-pro/TTF/SourceSansPro-Regular.ttf'),
          'truetype',
        ],
      ],
      {
        weight: 'normal',
        style: 'normal',
        stretch: 'normal',
      },
    ),
    fontFace (
      'Source Sans Pro',
      [
        [
          require ('fonts/source-sans-pro/EOT/SourceSansPro-It.eot'),
          'embedded-opentype',
        ],
        [
          require ('fonts/source-sans-pro/WOFF/OTF/SourceSansPro-It.otf.woff'),
          'woff',
        ],
        [
          require ('fonts/source-sans-pro/OTF/SourceSansPro-It.otf'),
          'opentype',
        ],
        [
          require ('fonts/source-sans-pro/TTF/SourceSansPro-It.ttf'),
          'truetype',
        ],
      ],
      {
        weight: 'normal',
        style: 'italic',
        stretch: 'normal',
      },
    ),
    fontFace (
      'Source Sans Pro',
      [
        [
          require ('fonts/source-sans-pro/EOT/SourceSansPro-Semibold.eot'),
          'embedded-opentype',
        ],
        [
          require ('fonts/source-sans-pro/WOFF/OTF/SourceSansPro-Semibold.otf.woff'),
          'woff',
        ],
        [
          require ('fonts/source-sans-pro/OTF/SourceSansPro-Semibold.otf'),
          'opentype',
        ],
        [
          require ('fonts/source-sans-pro/TTF/SourceSansPro-Semibold.ttf'),
          'truetype',
        ],
      ],
      {
        weight: 'bold',
        style: 'normal',
        stretch: 'normal',
      },
    ),
    fontFace (
      'Source Sans Pro',
      [
        [
          require ('fonts/source-sans-pro/EOT/SourceSansPro-SemiboldIt.eot'),
          'embedded-opentype',
        ],
        [
          require ('fonts/source-sans-pro/WOFF/OTF/SourceSansPro-SemiboldIt.otf.woff'),
          'woff',
        ],
        [
          require ('fonts/source-sans-pro/OTF/SourceSansPro-SemiboldIt.otf'),
          'opentype',
        ],
        [
          require ('fonts/source-sans-pro/TTF/SourceSansPro-SemiboldIt.ttf'),
          'truetype',
        ],
      ],
      {
        weight: 'bold',
        style: 'italic',
        stretch: 'normal',
      },
    ),
  ] | join ('\n\n')
}
