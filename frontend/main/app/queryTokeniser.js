; `
We are not using this module any more, though the code could be useful again some time.

The exported "parse" function can be used to tokenise and encode an input field value where the tokens may or may not be surrounded by double quotes, e.g.

  "van vader" plat hdt
`

const DOTESTS = false

import {
  pipe, compose, composeRight,
  not,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, blush, always, T, F,
  prop, path, has, hasIn,
  bindPropTo, bindProp, bindTo, bind,
  assoc, assocPath, assocM, assocPathM,
  concatTo, concat, appendTo, append,
  concatToM, concatM, appendToM, appendM,
  merge, mergeTo, mergeM, mergeToM,
  mergeIn, mergeInTo, mergeInM, mergeInToM,
  updateM, update, updatePathM, updatePath,
  take, drop,
  lets, letS, compactOk, compact,
  die, raise, decorateException, exception,
  lt, gt, eq, ne, lte, gte,
  factory, factoryProps,
  deconstruct,
} from 'stick-js/es'

import daggy from 'daggy'

import {
  length, toString, logWith, trim, nAtATime,
} from 'alleycat-js/es/general'

import {
  ifEmptyString,
} from 'alleycat-js/es/predicate'

import { cata, fold, } from 'alleycat-js/es/bilby'

const twoAtATime = 2 | nAtATime

const Str = daggy.taggedSum ('Str', {
  StrEmpty: [],
  StrNonempty: ['string'],
})

const { StrEmpty, StrNonempty, } = Str

Str.prototype.map = function (f) {
  return this | cata ({
    StrEmpty: _ => StrEmpty,
    StrNonempty: f >> StrNonempty,
  })
}

Str.prototype.fold = function (f, g) {
  return this | cata ({
    StrEmpty: _ => '' | f,
    StrNonempty: g,
  })
}

Str.prototype.toString = function () {
  return this | fold (id, id)
}

Str.prototype.encode = function () {
  return this | map (JSON.stringify)
}

Str.prototype.combine = function (t, joiner) {
  return this | fold (
    _ => t,
    s => t | fold (
      _ => StrNonempty (s),
      tt => StrNonempty (s + joiner + tt),
    ),
  )
}

const combine = dot2 ('combine')

const encode = dot ('encode')
const combineOn = (() => {
  const reducer = joiner => (acc, x) => acc | combine (x, joiner)
  return joiner => deconstruct (
    ([y, ...ys]) => ys | reduce (joiner | reducer, y)
  )
}) ()

const makeString = trim >> ifEmptyString (
  _ => StrEmpty,
  StrNonempty,
)

; `
Usage:
  [1, 2, 3, 4, 5, ...] | makeChunks2 // => [1, [2, 3], [4, 5], ...]
`
const makeChunks2 = ([fst, ...rest]) => [fst, ... (rest | twoAtATime)]

export const parse = (() => {
  const reducer2 = (acc, qe) => [acc, qe] | combineOn (' $ and $ ')
  const reducer = (acc, [dq, nq]) => lets (
    _ => [dq, ...(nq | split (/\s+/))],
    map (makeString >> encode) >> reduce (reducer2, acc),
  )
  const prepareFst = (nq) => lets (
    _ => nq | split (/\s+/),
    map (makeString >> encode),
    (_, [x, ...xs]) => xs | reduce (reducer2, x),
  )
  const _parse = fst => reduce (reducer) (fst | prepareFst)

  return letS ([
    split (/("[^"]+")/) >> makeChunks2,
    (_, [fst, ...ys]) => ys | _parse (fst) | toString,
  ])
}) ()

const tests = [
  `"van vader" plat hdt`,
  `"van vader"`,
  `"van vader" "van moeder"`,
  `"van vader" plat "van moeder"`,
  `plat hdt`,
  `plat`,
  `plat het  `,
  ``,
]

if (DOTESTS) {
  // const selectTest = drop (4) >> take (1)
  const selectTest = id
  const runTest = parse >> console.log
  tests | selectTest | map (runTest)
}
