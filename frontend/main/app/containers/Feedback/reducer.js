import {
  pipe, compose, composeRight,
  assoc,
  id, always, tap, noop, ifTrue,
  lets,
  prop,
  arg1,
  reduce,
  map,
  take, head,
  mergeTo,
  concat,
  condS, eq, guard, otherwise,
  precat, die,
  merge,
} from 'stick-js/es'

import { logAndroidPerf, logWith, defaultToV, } from 'alleycat-js/es/general'
import { Just, Nothing, cata, fold, } from 'alleycat-js/es/bilby'
import { reducer, } from '../../common'

import {
  EMAIL_SEND,
  EMAIL_SEND_SUCCESS,
  EMAIL_SEND_ERROR,
} from '../../containers/App/constants'

import {
  CLEAR_REQUEST,
} from './constants'

import {
  RequestInit, RequestLoading, RequestError, RequestResults,
  requestLoadingDefault,
} from '../../types'

const initialState = ({
  request: RequestInit,
})

const reducerTable = {
  [EMAIL_SEND]: () =>
    assoc ('request') (requestLoadingDefault ()),

  [EMAIL_SEND_SUCCESS]: () =>
    assoc ('request') (null | RequestResults),

  [EMAIL_SEND_ERROR]: ({ err, }) =>
    assoc ('request') (err | RequestError),

  [CLEAR_REQUEST]: () =>
    assoc ('request') (RequestInit),
}

export default reducer ('Feedback', initialState, reducerTable)
