import {
  pipe, compose, composeRight,
  merge, tap, concatTo,
  map, ifOk,
  ifTrue,
} from 'stick-js/es'

import { all, call, put, select, takeLatest, take, } from 'redux-saga/effects'

import { Left, Right, fold, flatMap, } from 'alleycat-js/es/bilby'
import { requestJSON, defaultOpts, resultFoldMap, resultFold, } from 'alleycat-js/es/fetch'
import { logWith, iwarn, ierror, tellIf, } from 'alleycat-js/es/general'
import { errorError, error as alertError, } from 'alleycat-js/es/react-s-alert'

import {
  EMAIL_SEND,
  EMAIL_SEND_CLICKED,
} from '../../containers/App/constants'

import {
  emailSend,
  emailSendSuccess,
  emailSendError,
} from '../App/actions'

function *generalAlert () {
  true | alertError
}

function *sagaEmailSend ({ data: { name, email, bericht, }}) {
  const url = '/api/feedback/mail'
  const opts = defaultOpts | merge ({
    method: 'POST',
    body: JSON.stringify ({
      data: {
        name,
        email,
        message: bericht,
      },
    }),
  })

  const results = yield call (requestJSON, url, opts)
  yield results
    | resultFoldMap (({ error, ok, }) => ok | ifTrue (
      () => Right (),
      () => error | Left,
    ))
    | resultFold (
      () => emailSendSuccess () | put,
      (umsg) => umsg | emailSendError | put,
      (imsgMb) => null
        // --- in both cases (request ok but email failed/request failed), show the oops bubble.
        | tap (errorError)
        | tap (
          () => imsgMb | map (concatTo ('Error sending email: ') >> iwarn),
        )
        | emailSendError
        | put,
    )
}

function *sagaEmailSendClicked ({ data: { name, email, bericht, }}) {
  yield emailSend (name, email, bericht) | put
}

export default function* defaultSaga () {
  yield all ([
    takeLatest (EMAIL_SEND_CLICKED, sagaEmailSendClicked),
    takeLatest (EMAIL_SEND, sagaEmailSend),
  ])
}
