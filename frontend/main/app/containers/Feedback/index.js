import {
  pipe, compose, composeRight,
  noop, tap,
  not,
  values, find,
  prop,
  side1,
  allAgainst, againstBoth,
  match, dot,
  lets, assocM,
  side2,
  mergeM, ifTrue, path,
  timesV, condS, eq, guard, whenPredicate,
  side, whenOk, propOf, map,
  deconstruct,
} from 'stick-js/es'

import React, { PureComponent, } from 'react'

import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import styled from 'styled-components'

import { emailSendClicked, } from '../App/actions'
import { clearRequest, } from './actions'

import { cata, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { keyPressListen, inputValue, } from 'alleycat-js/es/dom'
import { logWith, trim, } from 'alleycat-js/es/general'
import { isEmptyString, ifTrueV, isNotEmptyString, } from 'alleycat-js/es/predicate'
import { createRef, setState, } from 'alleycat-js/es/react-legacy'
import { deconstructProps, } from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import { spinner, } from '../../alleycat-components'

import {
  H1NLM, H2NLM, SectionNLM, TextDiv,
  Input as BasicInput, TextArea as BasicTextArea,
  Button, Div,
} from '../../components/shared'

import { mediaPhone, mediaTablet, mediaDesktop, isMobileWidth, isValidEmail, } from '../../common'
import config from '../../config'
import {
  RequestInit, RequestLoading, RequestError, RequestResults,
  requestState,
} from '../../types'
import injectSaga from '../../utils/injectSaga'
import injectReducer from '../../utils/injectReducer'

import { makeSelectRequest, } from './selectors'
import saga from './saga'
import reducer from './reducer'

const focus = side ('focus')
const isOnlyWhitespace = trim >> isEmptyString
const isNotOnlyWhitespace = isOnlyWhitespace >> not

const SpinnerTextblocks = 'textblocks' | spinner

const TopS = styled.div`
  height: 100%;
  margin-left: 40px;
  margin-right: 25px;
  overflow-y: auto;
  ul {
    padding-left: 16px;
  }
  .x__form-block {
    margin-top: 5px;
    margin-bottom: 5px;
    > div {
      padding-top: 2px;
      padding-bottom: 2px;
    }
  }
`

const WrapperS = styled.div`
  ${mediaQuery (
    mediaPhone (`
      height: calc(100% + 400px);
      .x__input-bericht {
        max-height: 150px;
      }
    `),
    mediaTablet (`
      height: 90%;
      .x__input-bericht {
        max-height: inherit;
      }
    `),
  )}
`

const Input = (props) => <BasicInput
  height='23px' width='250px' padding='3px'
  {...props}
/>

const TextArea = deconstructProps (({ isMobile, }) =>
  (props) => lets (
    () => isMobile ? '250px' : '80%',
    (width) => <BasicTextArea
      height='300px'
      width={width}
      {...props}
    />,
  ),
)

const FeedbackForm = styled.div`
  line-height: 1.5em;
`

const validate = side2 ('validate')

const StatusS = styled.div`
  font-size: 15px;
  display: ${prop ('show') >> ifTrueV ('inline-block', 'none')};
  .x__ok, .x__not-ok {
    div {
      display: inline-block;
      margin-right: 5px;
    }
  }
  .x__ok {
    div {
      color: green;
    }
  }
  .x__not-ok {
    div {
      color: #b32222;
    }
  }
`

const Status = ({ show, ok, }) => <StatusS show={show}>
  {ok | ifTrue (
    () => <div className='x__ok'><div>✔</div>Je mail is verstuurd.</div>,
    () => <div className='x__not-ok'><div>✘</div>Het spijt ons, er is iets misgegaan. Probeer later opnieuw.</div>,
  )}
</StatusS>

const TextDivLocal = styled(TextDiv)`
  margin-right: 5px;
`

const SpinnerWrapperS = styled.div`
  display: inline-block;
  margin-left: 10px;
  padding-top: 0px !important;
  padding-bottom: 0px !important;
  top: 7px;
  position: relative;
`

const SpinnerWrapper = ({ spinning, }) => <SpinnerWrapperS>
  <SpinnerTextblocks spinning={spinning} fontSize='16px'/>
</SpinnerWrapperS>

const isInputValueNotEmpty = inputValue >> isNotOnlyWhitespace
const whenInputValueNotEmpty = isInputValueNotEmpty | whenPredicate

const focusTable = {
  name: 'email',
  email: 'bericht',
}

class Feedback extends PureComponent {
  state = {
    canSubmit: false,
    inputs: {
      name: '',
      email: '',
      bericht: '',
    }
  }

  theRefs = {
    top: createRef (),
    wrapper: createRef (),
    name: createRef (),
    email: createRef (),
    bericht: createRef (),
    berichtHeading: createRef (),
  }

  canSubmit = () => this.inputsValid | values | allAgainst (Boolean)
  onSubmit = () => {
    const { props, state, } = this
    const { emailSendClickedDispatch, } = props
    const { inputs, } = state
    const { name, email, bericht, } = inputs
    emailSendClickedDispatch (name, email, bericht)
  }

  inputsValid = {
    name: false,
    email: false,
    bericht: false,
  }

  validators = {
    name: isNotOnlyWhitespace,
    email: againstBoth (isNotOnlyWhitespace, isValidEmail),
    bericht: isNotOnlyWhitespace,
  }

  validate = (which, val) => lets (
    () => this.validators [which] (val),
    (isValid) => this
      | mergeM ({
        inputsValid: this.inputsValid | assocM (which) (isValid),
      })
      | setState ({
        canSubmit: this.canSubmit (),
      })
  )

  focusInput = which => {
    this | deconstruct (
      path (['theRefs', which, 'current']) >> focus,
    )
  }

  updateFocus = propOf (focusTable) >> whenOk (this.focusInput)

  onInputChange = which => event => lets (
    () => event | inputValue,
    (value) => this
      | validate (which, value)
      | setState ({
        inputs: this.state.inputs | assocM (which) (value)
      })
  )

  onInputChangeAny = () => {
    this.props.clearRequestDispatch ()
  }
  _onInputChangeName = 'name' | this.onInputChange
  _onInputChangeEmail = 'email' | this.onInputChange
  _onInputChangeBericht = 'bericht' | this.onInputChange
  onInputChangeName = tap (this.onInputChangeAny) >> this._onInputChangeName
  onInputChangeEmail = tap (this.onInputChangeAny) >> this._onInputChangeEmail
  onInputChangeBericht = tap (this.onInputChangeAny) >> this._onInputChangeBericht

  onKeyPress = which => event => event | whenInputValueNotEmpty (
    keyPressListen (
      () => which | this.updateFocus,
      'Enter',
    )
  )
  onKeyPressName = 'name' | this.onKeyPress
  onKeyPressEmail = 'email' | this.onKeyPress
  onKeyPressBericht = 'bericht' | this.onKeyPress

  // @repeated from Home
  onFocusBericht = () => this | deconstruct (
    ({ theRefs, }) => theRefs | deconstruct (
      ({ top, berichtHeading, }) => [top, berichtHeading]
      | map (prop ('current')) | deconstruct (
        ([topc, headingc]) => [topc, headingc]
        | map (prop ('offsetTop')) | deconstruct (
          ([topoff, headingoff]) => lets (
            () => headingoff - topoff,
            (top) => () => topc.scrollTo ({
              top,
              behavior: 'smooth',
            }),
            // --- setTimeout seems to help against Android refusing to scroll sometimes while the
            // soft keyboard is shown.
            (_, f) => setTimeout (f, 100),
          )
        )
      )
    )
  )

  componentWillUnmount = () => {
    const { props, } = this
    const { clearRequestDispatch, } = props
    clearRequestDispatch ()
  }

  render = () => {
    const {
      props, state, theRefs,
      onInputChangeName, onInputChangeEmail, onInputChangeBericht,
      onKeyPressName, onKeyPressEmail, onKeyPressBericht,
      onFocusBericht,
      onSubmit,
    } = this
    const { request, isMobile, } = props
    const { canSubmit, } = state
    const {
      name: nameRef, email: emailRef, bericht: berichtRef,
      top: topRef, wrapper: wrapperRef,
      berichtHeading: berichtHeadingRef,
    } = theRefs

    const onKeyPress = isMobile ?
      [onKeyPressName, onKeyPressEmail, noop] :
      (noop | timesV (3))

    const onFocus = isMobile ?
      [noop, noop, onFocusBericht] :
      (noop | timesV (3))

    const [_, pending, ok, error] = request | requestState

    return <TopS ref={topRef}>
      <WrapperS ref={wrapperRef}>
        <H1NLM>Problemen &amp; feedback</H1NLM>
        <SectionNLM>
          <TextDivLocal>
             De redactie stelt het op prijs om feedback en correcties te ontvangen. Daarvoor kun je onderstaand formulier gebruiken. Hartelijk dank!
          </TextDivLocal>
          <TextDivLocal>
            <div className='x__form-block'>
              <div>Naam</div>
              <div>
                <Input
                  theRef={nameRef}
                  onKeyPress={onKeyPress [0]}
                  onChange={onInputChangeName}
                  onFocus={onFocus [0]}
                />
              </div>
            </div>
            <div className='x__form-block'>
              <div>Emailadres</div>
              <div>
                <Input
                  type='email'
                  theRef={emailRef}
                  onKeyPress={onKeyPress [1]}
                  onChange={onInputChangeEmail}
                  onFocus={onFocus [1]}
                />
              </div>
            </div>
            <div className='x__form-block'>
              <Div ref={berichtHeadingRef}>Bericht</Div>
              <div>
                <TextArea
                  className='x__input-bericht'
                  theRef={berichtRef}
                  onKeyPress={onKeyPress [2]}
                  onChange={onInputChangeBericht}
                  onFocus={onFocus [2]}
                  isMobile={isMobile}
                />
              </div>
            </div>
            <div className='x__form-block'>
              <Button disabled={canSubmit | not} onClick={onSubmit}>
                Sturen
              </Button>
              <SpinnerWrapper spinning={pending} />
              <Status ok={ok} show={ok || error} />
            </div>
          </TextDivLocal>
        </SectionNLM>
      </WrapperS>
    </TopS>
  }
}

const mapStateToProps = createStructuredSelector ({
  request: makeSelectRequest (),
})

const mapDispatchToProps = (dispatch) => ({
  clearRequestDispatch: clearRequest >> dispatch,
  emailSendClickedDispatch: emailSendClicked >> dispatch,
})

export default Feedback
  | connect       (mapStateToProps, mapDispatchToProps)
  | injectSaga    ({ saga, key: 'feedback', })
  | injectReducer ({ reducer, key: 'feedback', })
