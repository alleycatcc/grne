import {
  pipe, compose, composeRight,
  prop,
} from 'stick-js/es'

import { createSelector, defaultMemoize as memoize, } from 'reselect'

const selectSlice = (store, props) => store | prop ('feedback')

export const makeSelectRequest = _ => createSelector (
  selectSlice,
  prop ('request'),
)
