import {
  CLEAR_REQUEST,
} from './constants'

export const clearRequest = (state) => ({
  type: CLEAR_REQUEST,
})
