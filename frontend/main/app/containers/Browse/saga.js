import {
  pipe, compose, composeRight,
  merge,
  tap, concatTo,
  prop,
  map,
  ok,
  id, minus,
  mergeM,
  reduceObj,
  reduce,
  ifOk,
  appendM,
  eq, ifPredicate, not, whenOk,
  sprintfN,
} from 'stick-js/es'

import { all, call, put, select, takeLatest, take, } from 'redux-saga/effects'

import { debugDev, } from '../../common'
import { envIsTst, } from '../../env'

import {
  FetchReplace, FetchAppend, FetchPrepend,
} from '../../types'

import {
  ENTRIES_FETCH_CHUNK_PREV_CLICKED,
  ENTRIES_FETCH_CHUNK_NEXT_CLICKED,
  PARS_RESULT_SELECTED,
  CITG_WORD_CLICKED,
  LEMMA_CLICKED,
  ENTRY_LIST_ELEMENT_CLICKED,
} from '../../containers/App/constants'

import {
  lemmataClear,
  lemmataFetchChunkPrev,
  lemmataFetchChunkNext,
  parsgetFetch,
  parsgetResultsClear,
  entriesClear,
  // entriesExploreModeChange,
  entriesFetchChunkPrev,
  entriesFetchChunkNext,
} from '../App/actions'

import {
  makeSelectLemmaIdMin,
  makeSelectLemmaIdMax,
} from '../../slices/domain/selectors'

import { Left, Right, fold, flatMap, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { toString, iwarn, ierror, tellIf, logWith, } from 'alleycat-js/es/general'
import { errorError, error as alertError, } from 'alleycat-js/es/react-s-alert'

import config from '../../config'

const configTop = config | configure.init

const delay = ms => new Promise ((resolve, _) => setTimeout (resolve, ms))

function *generalAlert () {
  true | alertError
}

function *sagaEntriesFetchChunkPrevClicked () {
  yield entriesFetchChunkPrev () | put
}

function *sagaEntriesFetchChunkNextClicked () {
  yield entriesFetchChunkNext () | put
}

function *sagaParsResultSelected ({ data: word, }) {
  yield parsgetFetch ('searchField', word) | put
}

function *sagaCitgWordClicked ({ data: word, }) {
  yield parsgetFetch ('lemma', word) | put
}

function *sagaLemmaClicked () {
  yield parsgetResultsClear ('lemma') | put
}

// --- we use refresh as a quick way to get out of explore mode and force a refresh of everything by
// clearing the entries and the lemmata.
function *sagaEntryListElementClicked ({ data: { refresh, }}) {
  if (refresh) {
    yield entriesClear () | put
    yield lemmataClear () | put
  }
  yield parsgetResultsClear ('lemma') | put
}

export default function *defaultSaga () {
  yield all ([
    takeLatest (ENTRIES_FETCH_CHUNK_PREV_CLICKED, sagaEntriesFetchChunkPrevClicked),
    takeLatest (ENTRIES_FETCH_CHUNK_NEXT_CLICKED, sagaEntriesFetchChunkNextClicked),
    takeLatest (PARS_RESULT_SELECTED, sagaParsResultSelected),
    takeLatest (CITG_WORD_CLICKED, sagaCitgWordClicked),
    takeLatest (LEMMA_CLICKED, sagaLemmaClicked),
    takeLatest (ENTRY_LIST_ELEMENT_CLICKED, sagaEntryListElementClicked)
  ])
}
