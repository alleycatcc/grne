/*

@todo Some of this doesn't apply at all any more.

The Browse component contains a number of tricky interactions.

The lists on which Left and Right are based are expected to always correspond exactly. 'lemmaIdx' always refers to the list index. 'lemmaId' refers to all the lemmata in the db (so it runs from 0 to ~ 23,000). And the id in the database is usually referred to as 'mainId' and starts at 1, but the front end mostly doesn't need to use it.

lemmaId is passed to Browse as a prop, based on the location route. It is also stored in the state, which is the source of truth. This is to avoid clicks being sluggish due to having to first update the route and then wait from the lemmaId prop to reach Browse. We keep props and state in sync using getDerivedStateFromProps and a key for syncing, deriveFromPropsKey, which is a copy of lemmaId. (We can't just always copy the prop to the state because getDerivedStateFromProps is called a lot, and this would cause state updates to get lost). When there's a new lemmaId (prop), and it doesn't match deriveFromPropsKey, copy it to state and also to deriveFromPropsKey. During the getDerivedStateFromProps, we also (try) to do a lookup of the idx based on the id using a selector. This can fail, if the selector isn't ready yet. selectedLemmaIdx is also stored in the state.

selectedLemmaIdx is stored in the state of Browse, and updated by clicking on the lemma in Left. It is passed to Left and Right as prop.

curEntryIdx is stored in the state of Browse, and refers to the idx of the top-most entry in view, used for keeping the scrolls in sync.

renderLemmaListKey is stored in the ui reducer.
It's an integer which we increment each time the reductieMode is changed. It's passed as a prop to Right and then to LemmaList. componentDidUpdate of LemmaList manually checks it (prevProps v props) and forces a the windowed list to recalculate heights & refresh.

EntryList: if the list is scrolled, with the wheel or by dragging, onScrollToEntry is used to send a message to Right so it can update its scroll. When a lemma is clicked: clear pars table, set state in Browse to the new idx (the source of truth), and set a timeout of 500 to actually browse to its location. By setting state first we keep the UI fast, and the router and the props catch up a moment later.

LemmaList: on didMount, calls forceUpdate (react) and, if there are any lemmata, this.updateScrollSelect with usingOffsets set to true, with a timeout of 0 (this seems to ensure the heights in the windowed list have been measured).

LemmaList: on every didUpdate, if there are lemmata, calls this.updateScrollSelect with usingOffsets set to false.

LemmaList -> updateScrollSelect: makes sure that the selected lemma is always visible. But aborts early if 'locationChangedByUs' is true. This is a flag which is set if a lemma in the LemmaList was clicked. We don't want any scrolling to happen if that's the case. Takes a 'usingOffsets' parameter. If false, we use the scrollToRow API function of react-virtualized. This ensures that it's visible in some way. It's nicer to set it to true (when the offsets are available), to scroll more precisely. In this case, measure the height of the lemma we're going to. If it's taller than the parent, scroll to it (so it's at the top of the parent). If not, center it.

LemmaList -> shouldComponentUpdate: LemmaList is a Component (not PureComponent) with manual shouldComponentUpdate. If curEntryIdx was changed (meaning EntryList was scrolled), do updateScrollSympathetic. If curEntryIdx was the only prop or state that changed, don't update.

LemmaListInner: didMount and didUpdate both call this.update (). (didMount does it with a timeout of 0, which seems to ensure that the heights in the windowed list have been measured).

LemmaListInner: didUpdate: calls force update window list, and forces recalculation of heights if we have received new lemmata.

LemmaListInner: has onLemmaClicked method and onLemmaListClicked prop. The prop version calls lemmaClickedDispatch, which clears pars results. The method version gets set as the onClick of Lemma, and is also called by annotationClicked.

LemmaListInner: maintains browseMode in the state and passes it to LemmaList via a listener. browseMode is an ADT with states normal (desktop), fixed (mobile: show one lemma, allow annotations and disable scrolling), exploring (mobile: allow scrolling, disable annotations), and abridged (mobile: show one lemma which is too large for the screen, allow just enough scrolling, allow annotations).

LemmaListInner -> update: it browseMode was not exploring or fixed, calculate lemma height and screen height, then set browseMode to fixed or explore, depending on whether lemma is too long.
@todo

LemmaListInner: desktop version: if you click on an annotation of a non-selected lemma, it should select the lemma and show the annotation. This is done by using stopPropagation on the event handler, so that it doesn't bubble up and trigger an onClick which closes popups, or else you could never open a popup.


*/

import {
  pipe, compose, composeRight,
  map, flip, tap, bindProp, path,
  take, head, dot2, side2,
  noop, bindPropTo, defaultTo,
  ifTrue, ifFalse, whenOk, ok,
  side1, condS, guardV, otherwise, eq, lets, letS,
  multiply, concat, prop, sprintf1, always,
  ifPredicate, sprintfN, minus, plus,
  id, ampersandN, appendToM, not, ifOk, merge,
  lt, gt, mergeM,
} from 'stick-js/es'

import React, { Fragment, PureComponent, } from 'react'

import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import styled from 'styled-components'
import Draggable from 'react-draggable'

import { Just, Nothing, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import {
  iwarn, mapX, reduceX, logWith, reverse, ierror, notBelow, notAbove, defaultToV,
  getQueryParams,
} from 'alleycat-js/es/general'
import { ifTrueV, ifEquals, isNotEmptyList, } from 'alleycat-js/es/predicate'
import {
  withResizeListener, createRef, setState, whyUpdateMeth,
  compMeth, setStateValueMeth, toggleStateValue, toggleStateValueMeth,
} from 'alleycat-js/es/react-legacy'
import { mediaQuery, } from 'alleycat-js/es/styled'

import {
  parsResultSelected,
  citgWordClicked,
  lemmaClicked,
  entryListElementClicked,
  forceUpdateWindowList,
  lemmataCrop,
  lemmataCropClear,
  parsgetResultsClear,
  entriesFetchChunkNextClicked,
  entriesFetchChunkPrevClicked,
  entrySetSelectedOffsetTop,
} from '../App/actions'

import {
  makeSelectLemmataList,
  makeSelectLemmaIdToIdx,
  makeSelectLemmataReplacePending,
  makeSelectEntries,
  makeSelectEntriesFetchType,
  makeSelectEntriesExploreMode,
  makeSelectEntriesIdxShift,
  makeSelectAdjustSelectedIdxForCrop,
  makeSelectEntriesAppending,
  makeSelectEntriesPrepending,
} from '../../slices/domain/selectors'

import {
  makeSelectFontSize,
  makeSelectRenderLemmaListKey,
  makeSelectEntrySelectedOffsetTop,
} from '../../slices/ui/selectors'

import {
  makeSelectLemmaHasResults as makeSelectParsHasResults,
} from '../ParsResultsTable/selectors'

import { BrowseComponent, BrowseComponentM, } from '../../components/BrowseComponent'

import {
  mediaPhone, mediaTablet, mediaDesktop, isMobileWidth, browseLinkForRouter, browseToId, debugDev,
} from '../../common'
import config from '../../config'
import { envIsNotTstLike, } from '../../env'
import { CropNone, CropFake, CropReal, } from '../../types'
import injectSaga from '../../utils/injectSaga'
import injectReducer from '../../utils/injectReducer'

import reducer from './reducer'
import saga from './saga'

const configTop = config | configure.init

const doSchool = 'showSchoolVersion' | configTop.get
const debugRender = 'debug.rerender' | configTop.get

class Browse extends PureComponent {
  static getDerivedStateFromProps (props, state) {
    const { lemmaIdToIdx, lemmaId: lemmaIdFromProps, adjustSelectedIdxForCrop, } = props
    const { selectedLemmaIdx, } = state

    // --- selectedLemmaIdx is tricky because the lookup table is not ready the first time that
    // needsSync is true, and we can't just always set it because then we lose click events.
    // --- when we first come in, it's undefined, so we reason based on that.
    const selectedForce = selectedLemmaIdx | ifOk (
      () => ({}),
      () => ({
        selectedLemmaIdx: lemmaIdToIdx [lemmaIdFromProps] | defaultToV (null),
      }),
    )

    // --- we've moving to a new lemma thanks to props.
    const needsSync = lemmaIdFromProps !== state.deriveFromPropsKey

    return needsSync | ifTrue (
      () => ({
        lemmaId: lemmaIdFromProps,
        deriveFromPropsKey: lemmaIdFromProps,
        selectedLemmaIdx: lemmaIdToIdx [lemmaIdFromProps] | defaultToV (null),
        // --- always hide the search fields if we're going to a new lemma.
        showSearchFields: false,
      }),
      () => ({}),
    )
    | merge (selectedForce)
  }

  state = {
    // --- this is the idx of the top-most entry in the scrollable view, used for keeping the
    // right pane sympathetically in sync.
    curEntryIdx: void 8,
    // --- 'standaard', 'beknopt', 'school'
    reductieMode: envIsNotTstLike ? 'standaard' : lets (
      () => getQueryParams (),
      (params) => params.reductie || 'standaard',
    ),
    selectedLemmaIdx: void 8,
    lemmaId: void 8,
    // --- we keep props and state in sync using lemmaId as a key.
    deriveFromPropsKey: -1,

    lastScrollDirection: void 8,

    // --- only for mobile.
    // --- these could also have been handled by BrowseComponent, but ok.
    showEntries: false,
    showSearchFields: false,
  }

  onClick = noop

  onClickShowEntries = this | toggleStateValueMeth ('showEntries')

  onClickShowSearchFields = () => this | compMeth (
    ({ parsgetResultsClearDispatch, }) => {
      this | toggleStateValue ('showSearchFields')
      parsgetResultsClearDispatch('searchField')
  })

  onLemmaListClicked = () => this | compMeth (
    ({ lemmaClickedDispatch, }) => {
      this | setState ({
        showEntries: false,
        showSearchFields: false,
      })
      lemmaClickedDispatch ()
    })

  // --- we use 'feed style' to refer to the effect of revealing something when scrolling up and
  // hiding it when scrolling down.
  // --- this flag is used to block scroll events which didn't occur during a long scrolling action.
  lemmataScrollFeedStyleShouldBlock = false

  onSelectEntry = (lemmaId, lemmaIdx, immediate=false) => this | compMeth (
    ({ history, entryListElementClickedDispatch, adjustSelectedIdxForCrop, }) => {
      entryListElementClickedDispatch (null, immediate)
      // @todo leaving abridged false
      this | toggleStateValue ('showEntries')
      if (immediate) {
        lemmaId | browseToId (history)
        return
      }
      this | setState ({ selectedLemmaIdx: lemmaIdx, })
      setTimeout (() => lemmaId | browseToId (history), 500)
    },
  )

  selectReductieMode = lets (
    () => (reductieMode) => {
      this | setState ({
        reductieMode,
      })
      this.props.forceUpdateWindowListDispatch()
    },
    (set) => ({
      school: () => 'school' | set,
      standaard: () => 'standaard' | set,
      beknopt: () => 'beknopt' | set,
    })
  )

  onScrollToEntry = (lemmaIdx) => {
    const { props, state, } = this
    const { curEntryIdx, } = state
    if (curEntryIdx === lemmaIdx) return
    this.setState ({
      curEntryIdx: lemmaIdx,
    })
  }

  cropLemmata = (lemmaIdx) => this | compMeth (
    ({ lemmataCropDispatch, forceUpdateWindowListDispatch }) => {
      lemmataCropDispatch (CropFake (lemmaIdx, 2, 2))
      forceUpdateWindowListDispatch ()
    },
  )

  uncropLemmata = () => this | compMeth (
    ({ lemmataCropClearDispatch, forceUpdateWindowListDispatch }, { selectedLemmaIdxSaveCrop }) => {
      lemmataCropClearDispatch ()
      forceUpdateWindowListDispatch ()
    },
  )

  // --- seems onScroll of windowList is not reliable: sometimes we're at the top and it reports a
  // scrollTop of 35 or even 50 for example.
  onLemmataScroll = (delta, scrollTop, shouldBlock) => {
    const { state, } = this
    const { lastScrollDirection, } = state
    this | setState ({
      lemmataScrollFeedStyleShouldBlock: shouldBlock,
      // --- note that the min possible value is 1
      lemmataScrollIsTop: scrollTop < 5,
      lastScrollDirection: delta | condS ([
        -30 | lt | guardV ('up'),
        +10 | gt | guardV ('down'),
        otherwise | guardV (lastScrollDirection)
      ])
    })
  }

  componentDidUpdate = this | whyUpdateMeth ('Browse', debugRender)

  componentDidMount = () => this | compMeth ((
    { parsgetResultsClearDispatch, }) => {
    parsgetResultsClearDispatch('searchField')
  })

  render = () => this | compMeth ((
    {
      history, isMobile,
      lemmata, entries,
      lemmataReplacePending,
      parsResultSelectedDispatch,
      citgWordClickedDispatch,
      entryListElementClickedDispatch,
      fontSize,
      renderLemmaListKey,
      lemmataCropDispatch,
      lemmataCropClearDispatch,
      adjustSelectedIdxForCrop,
      parsHasResults,
      entriesExploreMode,
      entrySelectedOffsetTop,
      entrySetSelectedOffsetTopDispatch,
      entriesAppending, entriesPrepending,
      entriesFetchType,
      entriesIdxShift,
      entriesFetchChunkPrevClickedDispatch,
      entriesFetchChunkNextClickedDispatch,
    },
    {
      curEntryIdx, reductieModeBeknopt, reductieMode, selectedLemmaIdx,
      lastScrollDirection, lemmataScrollIsTop,
      lemmataScrollFeedStyleShouldBlock,
      showEntries, showSearchFields,
    },
    {
      onScrollToEntry, selectReductieMode, onSelectEntry,
      onLemmataScroll, cropLemmata, uncropLemmata,
      onClickShowEntries, onClickShowSearchFields,
      onClick,
      onLemmaListClicked,
    }
  ) => lets (
    () => ({
      history,
      parsResultSelectedDispatch,
      onLemmaListClicked,
      entryListElementClickedDispatch,
      entriesFetchChunkPrevClickedDispatch,
      entriesFetchChunkNextClickedDispatch,
      doSchool,
      reductieMode,
      selectReductieMode,
      onScrollToEntry,
      onSelectEntry,
      lemmataReplacePending,
      isMobile,
      curEntryIdx,
      lemmata,
      selectedLemmaIdx: selectedLemmaIdx | adjustSelectedIdxForCrop,
      renderLemmaListKey,
      citgWordClickedDispatch,
      onLemmataScroll,
      lastScrollDirection,
      lemmataScrollIsTop,
      lemmataScrollFeedStyleShouldBlock,
      fontSize,
      cropLemmata: isMobile ? cropLemmata : noop,
      uncropLemmata: isMobile ? uncropLemmata : noop,
      // --- avoid one re-render by forcing false.
      showParsTable: parsHasResults || false,
      entries,
      entriesAppending,
      entriesPrepending,
      entriesFetchType,
      entriesIdxShift,
      entriesExploreMode,
      entrySelectedOffsetTop,
      entrySetSelectedOffsetTopDispatch,
      showEntries,
      showSearchFields,
      onClickShowEntries,
      onClickShowSearchFields,
      onClick,
    }),
    () => isMobile ? BrowseComponentM : BrowseComponent,
    (browseProps, Component) => <Component
      {...browseProps}
    />,
  ))
}

const mapStateToProps = createStructuredSelector ({
  lemmata: makeSelectLemmataList (),
  lemmaIdToIdx: makeSelectLemmaIdToIdx (),
  lemmataReplacePending: makeSelectLemmataReplacePending (),
  adjustSelectedIdxForCrop: makeSelectAdjustSelectedIdxForCrop (),
  fontSize: makeSelectFontSize (),
  renderLemmaListKey: makeSelectRenderLemmaListKey (),
  parsHasResults: makeSelectParsHasResults (),
  entries: makeSelectEntries (),
  entriesAppending: makeSelectEntriesAppending (),
  entriesPrepending: makeSelectEntriesPrepending (),
  entriesFetchType: makeSelectEntriesFetchType (),
  entriesExploreMode: makeSelectEntriesExploreMode (),
  entrySelectedOffsetTop: makeSelectEntrySelectedOffsetTop (),
  entriesIdxShift: makeSelectEntriesIdxShift (),
})

const mapDispatchToProps = (dispatch) => ({
  parsResultSelectedDispatch: parsResultSelected >> dispatch,
  citgWordClickedDispatch: citgWordClicked >> dispatch,
  lemmaClickedDispatch: lemmaClicked >> dispatch,
  entryListElementClickedDispatch: entryListElementClicked >> dispatch,
  forceUpdateWindowListDispatch: forceUpdateWindowList >> dispatch,
  lemmataCropDispatch: lemmataCrop >> dispatch,
  lemmataCropClearDispatch: lemmataCropClear >> dispatch,
  parsgetResultsClearDispatch: parsgetResultsClear >> dispatch,
  entriesFetchChunkNextClickedDispatch: entriesFetchChunkNextClicked >> dispatch,
  entriesFetchChunkPrevClickedDispatch: entriesFetchChunkPrevClicked >> dispatch,
  entrySetSelectedOffsetTopDispatch: entrySetSelectedOffsetTop >> dispatch,
})

export default Browse
  | connect       (mapStateToProps, mapDispatchToProps)
  | injectSaga    ({ saga, key: 'browse' })
