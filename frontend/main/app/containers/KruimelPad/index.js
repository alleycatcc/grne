import {
  pipe, compose, composeRight,
  appendToM, mergeM,
  lets, flip, not,
  whenOk, prop,
  tap, deconstruct,
  head, noop,
  id,
} from 'stick-js/es'

import React, { Fragment, PureComponent, } from 'react'

import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'
import styled from 'styled-components'
import {
  makeSelectHistoryList,
} from '../../slices/app/selectors'

import { BlueLink, LinkLikeTheme2, LinkLike, } from '../../components/shared'

import configure from 'alleycat-js/es/configure'
import { reduceX, reverse, logWith, setTimeoutOn, } from 'alleycat-js/es/general'
import { mediaQuery, } from 'alleycat-js/es/styled'
import { createRef, whyUpdate, compMeth, } from 'alleycat-js/es/react-legacy'
import { withDisplayName, } from 'alleycat-js/es/react'

import { mediaPhone, mediaTablet, mediaDesktop, isMobileWidth, browseLinkForRouter, browseTo, } from '../../common'
import config from '../../config'
import injectSaga from '../../utils/injectSaga'
import injectReducer from '../../utils/injectReducer'

import reducer from './reducer'
import saga from './saga'

const configTop = config | configure.init

const debugRender = 'debug.rerender' | configTop.get

const flipReduceX = reduceX | flip

const TopS = styled.div`
  width: 100%;
  ${mediaQuery (
    mediaPhone (`
      overflow-x: auto;
    `),
    mediaTablet (`
      overflow-x: auto;
    `),
  )}
  white-space: nowrap;
  height: 53px;
  position: relative;
  font-size: 12px;
  .x__wrapper {
    ${mediaQuery (
      mediaPhone (`
        margin-top: 18px;
        margin-right: 10px;
      `),
      mediaTablet (`
        margin-top: 18px;
      `),
    )}
    height: 18px;
  }
  .x__item-wrapper {
    display: inline-block;
    cursor: text;
  }
  .x__arrow {
    display: inline-block;
    font-size: 8px;
    margin-left: 8px;
    margin-right: 8px;
  }
  .x__location {
    display: inline-block;
  }
`

const LinkElement = (isMobile) => isMobile ? LinkLikeTheme2 : LinkLike

const makeKruimelItem = (history, isMobile, alwaysMakeLinks, onItemClick) =>
  (historyLength, first, xs, link, naamFull, lastNaamFull, idx) => lets (
    () => first | not,
    () => alwaysMakeLinks || (idx !== historyLength - 1 && naamFull !== lastNaamFull),
    (_, wantLink) => wantLink ?
      LinkElement (isMobile) :
      Fragment,
    (makeArrow, _, Element) => <div className='x__item-wrapper' key={idx}>
      {makeArrow && <div className='x__arrow'>➔</div>}
      <div className='x__location' onClick={onItemClick (link)}>
        <Element>{naamFull}</Element>
      </div>
    </div> | appendToM (xs),
  )

class KruimelPad extends PureComponent {
  ref = createRef ()

  componentDidUpdate = (prevProps, prevState) => this | deconstruct (
    ({ ref, }) => {
      this | whyUpdate ('KruimelPad', debugRender) (prevProps, prevState)
      ref.current.scrollLeft = 1e5
    },
  )

  render = () => this | compMeth ((
    {
      history,
      isMobile,
      historyList,
      alwaysMakeLinks=false,
      onItemClick=noop,
    },
    {},
    {
      ref,
    },
  ) => lets (
    () => makeKruimelItem (history, isMobile, alwaysMakeLinks, onItemClick),
    (makeItem) => <TopS ref={ref}>
      <div className='x__wrapper'>
        {lets (
          () => historyList.length,
          (length) => historyList | head | whenOk (prop ('naamFull')),
          (length, lastNaamFull) => historyList | reverse | flipReduceX ([true, []]) (
            ([first, xs], { link, naamFull }, idx) => lets (
              () => makeItem (length, first, xs, link, naamFull, lastNaamFull, idx),
              (ys) => [false, ys],
            )
          ),
        )}
      </div>
    </TopS>,
  ))
}

const mapStateToProps = createStructuredSelector ({
  historyList: makeSelectHistoryList (),
})

const mapDispatchToProps = (dispatch) => ({
})

export default KruimelPad
  | connect       (mapStateToProps, mapDispatchToProps)
  | injectSaga    ({ saga, key: 'kruimelPad' })
