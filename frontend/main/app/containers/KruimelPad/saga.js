import {
  pipe, compose, composeRight,
} from 'stick-js/es'

import { all, call, put, select, takeLatest, take, } from 'redux-saga/effects'

import {
  iwarn, ierror, tellIf,
} from 'alleycat-js/es/general'

; `
import {
} from '../../containers/App/constants'
import {
} from '../App/actions'
import {
} from '../../slices/domain/selectors'
`

export default function* defaultSaga () {
  yield all ([
  ])
}
