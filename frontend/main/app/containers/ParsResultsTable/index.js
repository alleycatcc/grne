import {
  pipe, compose, composeRight,
  defaultTo,
  lets, die,
  zipAll, prop,
  concat, tap,
  deconstruct,
  noop,
} from 'stick-js/es'

import React, { Fragment, PureComponent, } from 'react'

import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import styled from 'styled-components'

import { parsLinkClicked, parsCloseClicked, } from '../App/actions'
import reducer from './reducer'
import saga from './saga'

import { cata, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { clss, } from 'alleycat-js/es/dom'
import { ierror, mapX, logWith, } from 'alleycat-js/es/general'
import { ifTrueV, ifEquals, isEmptyList, } from 'alleycat-js/es/predicate'
import { compMeth, } from 'alleycat-js/es/react-legacy'
import { whyYouRerender, } from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import { spinner, } from '../../alleycat-components'
import CloseIcon from '../../components/svg/CloseIcon'
import InfoIcon from '../../components/svg/InfoIcon'
import { BlueLink, BrowseSpinner, } from '../../components/shared'

import { browseLinkForRouter, mediaPhone, mediaTablet, mediaDesktop, isMobileWidth, } from '../../common'
import config from '../../config'
import injectSaga from '../../utils/injectSaga'
import injectReducer from '../../utils/injectReducer'

import {
  makeSelectLemmaQuery, makeSelectLemmaResults,
  makeSelectSearchFieldQuery, makeSelectSearchFieldResults,
} from './selectors'

const configTop = config | configure.init

const debugRender = 'debug.rerender' | configTop.get

const IconS = styled.div`
  display: ${prop ('show') >> ifTrueV (
    'flex', 'none',
  )};
  > * {
    margin-left: auto;
    margin-right: auto;
  }
  // background: yellow;
  cursor: pointer;
  position: absolute;
  right: 9px;
  top: 8px;
  z-index: 1;
  ${mediaQuery (
    mediaPhone ('width: 40px; height: 40px;'),
    mediaTablet ('width: initial; height: initial;'),
    mediaDesktop (': '),
  )}
`

const CloseIconS = styled (IconS) `
`

const IconInfoS = styled (IconS)`
  ${({ adjustHorizontally, }) => mediaQuery (
    mediaPhone (`
      right: ${adjustHorizontally ? '50px' : '50px'};
    `),
    mediaTablet (`
      right: ${adjustHorizontally ? '28px' : '34px'};
    `),
    mediaDesktop (`
    `),
  )}
  top: 7.8px;
`
const CloseIconWrapper = ({ show, onClick=noop, passProps, }) =>
  <CloseIconS show={show} onClick={onClick}>
    <CloseIcon {...passProps} />
  </CloseIconS>

const InfoIconS = ({ show, adjustHorizontally, onClick, passProps, }) =>
  <IconInfoS
    show={show}
    adjustHorizontally={adjustHorizontally}
    onClick={onClick}
  >
    <InfoIcon {...passProps} />
  </IconInfoS>

const TopS = styled.div`
  ${mediaQuery (
    mediaPhone (`
     max-height: 80%;
     overflow-y: scroll;
     box-shadow: 1px 1px 3px 0px #656565;
    `),
    mediaTablet (`
     max-height: initial;
     overflow-y: visible;
     box-shadow: none;
    `)
  )}
  ${prop ('extraShadow') >> ifTrueV ('', 'box-shadow: none !important;')}
`

const SpinnerWrapperS = styled.div`
  margin-top: 3px;
`

const SpinnerWrapper = ({ spinning }) => <SpinnerWrapperS>
  <BrowseSpinner shrink={1.15} spinning={spinning}/>
</SpinnerWrapperS>

const originatorTable = {
  lemma: [makeSelectLemmaQuery, makeSelectLemmaResults],
  searchField: [makeSelectSearchFieldQuery, makeSelectSearchFieldResults],
}

const TableWrapperS = styled.div`
  border: solid #aaa 1px;
  background-color: #f6f6f6;
  &.x--no-result {
    width: 150px;
  }
  ${mediaQuery (
    mediaPhone (`
      width: 240px;
      padding: 15px;
    `),
    mediaTablet (`
      width: 270px;
      top: 7px;
      padding: 5px;
      &.x--adjust-height {
        top: 37px;
      }
    `)
  )}
  min-height: 60px;
  position: relative;
  box-shadow: 1px 1px 3px 0px #656565;
`

const TableS = styled.table`
  ${prop ('noresult') >> ifTrueV (
    'margin-top: 18px; margin-left: 22px;',
    'margin-top: 0px; margin-left: 0px;'
  )}
  width: 100%;
  top: -13px;
`

const Header = ({ query }) => <HeaderS>
  <RowElementLeft>{"vorm: "}
    <HeaderWordS>
      {query}
    </HeaderWordS>
  </RowElementLeft>
</HeaderS>

const HeaderWordS = styled.div`
  border-bottom: 1px dotted black;
  display: inline-block;
  padding-top: 4px;
`

const HeaderS = styled.tr`
`

const Row = ({ item: [woord, vorm, link], onParsLinkClicked }) => <RowS>
  <RowElementLeft>
    {vorm}
  </RowElementLeft>
  <RowElementRight>
    <Woord
      onParsLinkClicked={onParsLinkClicked}
      value={[woord,link]}
    />
  </RowElementRight>
</RowS>

const RowS = styled.tr`
`

const RowElementLeft = styled.td`
  padding: 2px 5px 6px 8px;
  width: 60%;
  // white-space: nowrap;
`

const RowElementRight = styled.td`
  padding: 2px 5px 6px 8px;
  width: 40%;
  // white-space: nowrap;
`

const Woord = ({ value: [woord, link], onParsLinkClicked }) => (link) ?
  <BlueLink
    onClick={onParsLinkClicked}
    to={link | browseLinkForRouter}
  >
    {woord}
    </BlueLink> :
  <DisabledLinkS>
    {woord}
  </DisabledLinkS>


const DisabledLinkS = styled.div`
  color: red;
  text-decoration: underline;
  cursor: text;
`

const Table = ({ results, query, onParsLinkClicked }) => {
  const noresult = results[0] === undefined
  if (noresult) {
    return <TableS noresult={noresult}>
      <tbody>
        <tr>
          <td>Geen resultaten
          </td>
        </tr>
      </tbody>
    </TableS>
  } else {
  const Rows = results.map((item, idx) => <Row item={item} onParsLinkClicked={onParsLinkClicked} key={idx}/>)
  return  <TableS>
  <tbody>
    <Header query={query}/>
    {Rows}
  </tbody>
</TableS>}}

const InfoS = styled.div`
  display: ${prop ('show') >> ifTrueV ('block', 'none')};
  margin: 7px !important;
  padding-top: 9px;
  border-top: 1px solid grey;
`

class ParsResultsTable extends PureComponent {
  constructor (props) {
    super (props);
    this.state = {
      showInfo: false,
    }
  }

  onParsLinkClicked = () => {
    this | deconstruct (
    ({ props, }) => props | deconstruct (
      ({ originatorKey, parsLinkClickedDispatch, }) =>
        originatorKey | parsLinkClickedDispatch,
    ),
  )}

  onParsCloseClicked = () => this | compMeth (
    ({ originatorKey, parsCloseClickedDispatch, }) =>
      originatorKey | parsCloseClickedDispatch,
  )

  onInfoClicked = () => this.setState({
    showInfo: !this.state.showInfo,
  })

  componentDidUpdate = (prevProps, prevState) => this | compMeth (
    (props, state) => debugRender && whyYouRerender (
      'ParsResultsTable', [prevProps, props], [prevState, state],
    ),
  )

  render () {
    const { state, props, onParsLinkClicked, onParsCloseClicked, onInfoClicked, } = this
    const { showInfo } = state
    const { query, results, originatorKey, ambient } = props
    const adjustHeight= showInfo && ambient === 'lemmaList'
    const showCloseIcon = (ambient === 'browse' || ambient === 'lemmaList')
    const extraShadow = ambient === 'lemmaList'
    const ParsTable = ({ results }) => results | cata ({
      RequestInit: _ => null,
      RequestLoading: _ => <TableWrapperS>
        <SpinnerWrapper spinning={true}/>
      </TableWrapperS>,
      RequestResults: (requestResults) => lets (
        () => requestResults | isEmptyList,
        (noresult) => clss (
           noresult && 'x--no-result',
           adjustHeight && 'x--adjust-height',
        ),
        (noresult, className) => <TableWrapperS
          className={className}
          onClick={(event)=>{event.stopPropagation()}}
        >
          <InfoIconS
            adjustHorizontally={ambient==='home'}
            onClick={onInfoClicked}
            show={!noresult}
            passProps={{
              strokeWidth: '0.3px',
              strokeWidthInner: '1.0px',
              height: 17.3,
              width: 17.3,
            }}
          />
          <CloseIconWrapper
            show={showCloseIcon}
            onClick={onParsCloseClicked}
            passProps={{
              strokeWidth: '0.3px',
              height: 18,
              width: 18,
            }}
          />
          <Table
            results={requestResults}
            query={query}
            onParsLinkClicked={onParsLinkClicked}
          />
          <InfoS show={showInfo && !noresult}>
            Let op: de resultaten zijn automatisch gegenereerd en kunnen fouten bevatten.
          </InfoS>
        </TableWrapperS>,
      ),
      RequestError: _ => null,
    })
    return <TopS
      extraShadow={extraShadow}
    >
      <ParsTable results={results}/>
    </TopS>
  }
}

const mapStateToProps = (state, ownProps) => ownProps | deconstruct (
  ({ originatorKey, }) => lets (
    () => originatorTable [originatorKey] | defaultTo (
      () => die ('bad key: ' + originatorKey),
    ),
    ([selectQuery, selectResults]) => createStructuredSelector ({
      query: selectQuery (),
      results: selectResults (),
    }),
    (_, sSel) => sSel (state, ownProps),
  ),
)

const mapDispatchToProps = (dispatch) => ({
  parsLinkClickedDispatch: parsLinkClicked >> dispatch,
  parsCloseClickedDispatch: parsCloseClicked >> dispatch
})

export default ParsResultsTable
  | connect       (mapStateToProps, mapDispatchToProps)
  | injectSaga    ({ saga, key: 'parsResultsTable', })
  // --- local reducer (optional)
  | injectReducer ({ reducer, key: 'parsResultsTable', })
