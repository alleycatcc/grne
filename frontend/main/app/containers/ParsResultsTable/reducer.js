import {
  pipe, compose, composeRight,
  assoc,
  id, always, tap, noop, ifTrue,
  lets,
  prop,
  arg1,
  reduce,
  map,
  take, head,
  mergeTo,
  concat,
  condS, eq, guard, otherwise,
  precat, die,
  merge,
} from 'stick-js/es'

import { logWith, defaultToV, mapX, } from 'alleycat-js/es/general'
import { Just, Nothing, cata, fold, } from 'alleycat-js/es/bilby'

import { reducer, } from '../../common'
import {
  RequestInit, RequestLoading, RequestError, RequestResults,
  requestLoadingDefault,
} from '../../types'

import {
  PARSGET_FETCH,
  PARSGET_FETCH_SUCCESS,
  PARSGET_FETCH_ERROR,
  PARSGET_RESULTS_CLEAR,
} from '../../containers/App/constants'

const initialState = ({
  // --- pars request originated from (a) pars field.
  searchField: {
    results: RequestInit,
    query: undefined,
  },
  // --- pars request originated from a lemma in the right pane.
  lemma: {
    results: RequestInit,
    query: undefined,
  },
})

const reducerTable = {
  // --- pattern:
  // [CONSTANT]: (action) => (state) => newState,

  [PARSGET_FETCH]: ({ data: { originator, query, }}) => state =>
    state | merge ({
      [originator]: {
        ...state [originator],
        results: requestLoadingDefault (),
      },
    }),

  [PARSGET_FETCH_SUCCESS]: ({ data: { originator, results, query, }}) => state =>
    state | merge ({
      [originator]: {
        ...state [originator],
        results: results | RequestResults,
        query,
      },
    }),

  [PARSGET_FETCH_ERROR]: ({ data: { originator, }}) => state =>
    state | merge ({
      [originator]: {
        ...state [originator],
        results: RequestError,
      },
    }),

  [PARSGET_RESULTS_CLEAR]: ({ data: originator, }) => state =>
    state | merge ({
      [originator]: {
        ...state [originator],
        results: RequestInit,
        query: null,
      },
    })
}

export default reducer ('ParsResultsTable', initialState, reducerTable)
