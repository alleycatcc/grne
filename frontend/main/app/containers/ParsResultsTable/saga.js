import {
  pipe, compose, composeRight,
  merge, tap, concatTo, map,
  ifOk, ifTrue, join,
} from 'stick-js/es'

import { all, call, put, select, takeLatest, take, } from 'redux-saga/effects'

import { Left, Right, fold, flatMap, } from 'alleycat-js/es/bilby'
import { requestJSON, resultFold, defaultOpts, } from 'alleycat-js/es/fetch'
import { logWith, encodeFormData, iwarn, } from 'alleycat-js/es/general'
import { errorError, error as alertError, } from 'alleycat-js/es/react-s-alert'

; `
import {
} from '../../slices/domain/selectors'
`

import {
  parsgetFetchSuccess, parsgetFetchError, parsgetResultsClear,
} from '../App/actions'

import {
  PARS_RESULT_SELECTED,
  PARSGET_FETCH,
  PARSGET_FETCH_SUCCESS,
  PARSGET_FETCH_ERROR,
  PARS_LINK_CLICKED,
  PARS_CLOSE_CLICKED,
} from '../../containers/App/constants'

const delay = ms => new Promise ((res, _) => setTimeout (
  res, ms,
))

function *generalAlert () {
  true | alertError
}

function *sagaParsLinkClicked ({ data: originator, }) {
  yield parsgetResultsClear (originator) | put
}

function *sagaParsCloseClicked ({ data: originator, }) {
  yield parsgetResultsClear (originator) | put
}

function *sagaParsgetFetch ({ data: { originator, query, }}) {
  const url = '/api/autocomplete'
  const params = { mode: 'parsget', query } | encodeFormData
  const urlWithParams = [url, params] | join ('?')
  const opts = defaultOpts | merge ({
    method: 'GET',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    },
  })

  const results = yield call (requestJSON, urlWithParams, opts)
  yield results
    | resultFold (
      (results) => parsgetFetchSuccess (originator, query, results) | put,
      (umsg) => parsgetFetchError (originator, umsg) | put,
      (imsgMb) => {
        imsgMb | map (tap (errorError) >> tap (iwarn))
        parsgetFetchError (originator, null) | put
      },
    )
}

export default function *defaultSaga () {
  yield all ([
    takeLatest (PARSGET_FETCH, sagaParsgetFetch),
    takeLatest (PARS_LINK_CLICKED, sagaParsLinkClicked),
    takeLatest (PARS_CLOSE_CLICKED, sagaParsCloseClicked),
  ])
}
