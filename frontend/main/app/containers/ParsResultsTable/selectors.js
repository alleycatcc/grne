import {
  pipe, compose, composeRight,
  prop, whenOk,
} from 'stick-js/es'

import { createSelector, defaultMemoize as memoize, } from 'reselect'

import {
  requestHasResultsOrPending,
} from '../../types'

// --- This slice doesn't exist yet until ParsResultsTable has been mounted, but the selectors might
// be called before that (e.g. by Browse).
// --- Guard against that by returning an empty object and using `whenOk` below.
const selectSlice = (store, props) => store.parsResultsTable || {}

const makeSelectSearchField = _ => createSelector (
  selectSlice,
  prop ('searchField'),
)

// --- 'lemma' refers to the parstable in the browseview; 'searchField' refers to the one by the
// search field.

const makeSelectLemma = _ => createSelector (
  selectSlice,
  prop ('lemma'),
)

export const makeSelectSearchFieldQuery = _ => createSelector (
  makeSelectSearchField (),
  prop ('query'),
)

export const makeSelectSearchFieldResults = _ => createSelector (
  makeSelectSearchField (),
  prop ('results'),
)

export const makeSelectLemmaQuery = _ => createSelector (
  makeSelectLemma (),
  prop ('query'),
)

export const makeSelectLemmaResults = _ => createSelector (
  makeSelectLemma (),
  whenOk (prop ('results')),
)

export const makeSelectLemmaHasResults = _ => createSelector (
  makeSelectLemmaResults (),
  whenOk (requestHasResultsOrPending),
)
