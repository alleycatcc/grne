import {
  pipe, compose, composeRight,
  split, prop, map, join, assoc,
  whenOk, sprintf1,
  eq, not, ifPredicate, whenPredicate,
  ifOk,
  ifTrue,
  prependTo,
  sprintfN,
  bindProp,
  whenTrue,
  concat, divideBy,
  ifFalse,
  always,
  mergeM,
  id,
  tap,
  die, condS, guard,
  flip3,
  reduce,
  lets, noop,
} from 'stick-js/es'

import React, { PureComponent, Fragment, } from 'react'

import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import styled from 'styled-components'

import {
  sizeSelected,
  forceUpdateWindowList,
} from '../App/actions'

import {
  makeSelectFontSize,
} from '../../slices/ui/selectors'

import { Nothing, Just, fold, cata, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { stopPropagation, } from 'alleycat-js/es/dom'
import { mapX, iwarn, ierror, logWith, } from 'alleycat-js/es/general'
import { setState, compMeth, } from 'alleycat-js/es/react-legacy'
import { whyYouRerender, } from 'alleycat-js/es/react'
import { ierrorError, errorError, } from 'alleycat-js/es/react-s-alert'
import { media, mediaQuery, } from 'alleycat-js/es/styled'

import HomeComponent from '../Home'
import AdvancedSearch from '../AdvancedSearch'
import Analyse from '../../containers/Analyse/Loadable'
import Colofon from '../../components/Colofon/'
import Feedback from '../../containers/Feedback/'
import Header from '../../components/Header/'
import MenuM from '../../components/MenuM/'
import Browse from '../../containers/Browse/'
import Hulp from '../../components/Hulp/'

import { mediaPhone, mediaTablet, mediaDesktop, isMobileWidth, } from '../../common'
import config from '../../config'

import {
  TabHome, TabAnalyse, TabAnalyseXmlToHtmlPrint, TabColofon,
  TabFeedback, TabAdvancedSearch, TabBrowse,
} from '../../types'
import injectReducer from '../../utils/injectReducer'
import injectSaga from '../../utils/injectSaga'

import saga from './saga'

const configTop = config | configure.init

const debugRender = 'debug.rerender' | configTop.get
const reduceFlip = reduce | flip3

const Home = (props) => <HomeComponent
  plain={false}
  {...props}
/>

const TopS = styled.div`
  height: 100%;
  width: 100%;
  background: #F6F6F6;
  .x--soft-keyboard-visible & {
    overflow-y: scroll;
  }
  overflow-y: auto;
`

const WrapperS = styled.div`
  margin: auto;
  margin-top: 0px;
  margin-bottom: 0px;
  background-repeat: repeat;
  background: white;
  overflow-x: hidden;
  height: 100%;
  ${mediaQuery (
    mediaPhone (`
      width: 100%;
    `),
    mediaTablet (`
      width: 768px;
    `),
    mediaDesktop (`
      width: 900px;
      margin-left: auto;
      margin-right: auto;
    `),
  )}
`

const HeaderWrapperS = styled.div`
`

// @todo do we need this for plain too?
const HoofdWrapperS = styled.div`
  border-left: 1px solid #ddd;
  border-right: 1px solid #ddd;
  overflow-y: hidden;
  // @todo
  ${media (
    mediaPhone (`
      height: calc(100% - 50px - 10px);
    `),
    mediaTablet (`
      height: calc(100% - 122px - 10px);
    `),
  )}
`

const MenuMWrapperS = styled.div`
`

export class Main extends PureComponent {
  state = {
    selectedIdx: -1,
    menuMOpen: false,
  }

  onClickTop = () => this.closeMenuM ()

  _onMenuIconClickM = () => this | compMeth (
    (_, { menuMOpen, }) => this | setState ({
      menuMOpen: menuMOpen | not,
    })
  )

  onMenuIconClickM = stopPropagation >> this._onMenuIconClickM

  closeMenuM = () => this | setState ({
    menuMOpen: false,
  })

  onHomeClickM = () => this.closeMenuM ()
  onSelectMenuItem = () => this.closeMenuM ()

  getRouteComponent () {
    const { props, } = this
    const { tab, } = props
    return tab | cata ({
      TabHome: () => [Home, [], false, false],
      TabAnalyse: () => [Analyse, [], true, false],
      TabAnalyseXmlToHtmlPrint: () => [Analyse, [['batchId', String]], true, false],
      TabColofon: () => [Colofon, [], false, false],
      TabFeedback: () => [Feedback, [], false, false],
      TabAdvancedSearch: () => [AdvancedSearch, [], false, false],
      TabBrowse: () => [Browse, [['lemmaId', Number]], false, true],
      TabHulp: () => [Hulp, [], false, false],
    })
  }

  componentDidUpdate (prevProps, prevState) {
    const { props, state, } = this
    if (debugRender) whyYouRerender ('Main', [prevProps, props], [prevState, state])
  }

  render () {
    const { props, state, onClickTop, onMenuIconClickM, onHomeClickM, onSelectMenuItem, } = this
    const { isMobile, routeParams={}, history, sizeSelectedDispatch, forceUpdateWindowListDispatch, fontSize, } = props
    const { menuMOpen, } = state
    const { location, } = history
    const [Hoofd, routeParamVals, plain, showFontsizer] = this.getRouteComponent ()
    const hoofdProps = routeParamVals
      | reduceFlip ({}) ((acc, [prop, transform]) => acc |
        mergeM ({ [prop]: routeParams [prop] | transform, })
      )
      | mergeM ({ history, isMobile, })

    return <TopS onClick={onClickTop}>
      {plain | ifTrue (
        () => <Hoofd {...hoofdProps}/>,
        () => <WrapperS>
          <HeaderWrapperS>
            <Header
              showFontsizer={showFontsizer}
              onMenuMIconClick={onMenuIconClickM}
              menuMOpen={menuMOpen}
              onHomeClickM={onHomeClickM}
              sizeSelected={sizeSelectedDispatch}
              forceUpdateWindow={forceUpdateWindowListDispatch}
              fontSize={fontSize}
            />
          </HeaderWrapperS>
          <HoofdWrapperS>
            <Hoofd {...hoofdProps}/>
          </HoofdWrapperS>
          {isMobile && <MenuMWrapperS>
            <MenuM
              onClick={stopPropagation}
              curLocation={location}
              isOpen={menuMOpen}
              onSelect={onSelectMenuItem}
            />
          </MenuMWrapperS>}
        </WrapperS>,
      )}
    </TopS>
  }
}

const mapDispatchToProps = (dispatch) => ({
  sizeSelectedDispatch: sizeSelected >> dispatch,
  forceUpdateWindowListDispatch: forceUpdateWindowList >> dispatch,
})

const mapStateToProps = createStructuredSelector ({
  fontSize: makeSelectFontSize (),
})

export default Main
  | connect       (mapStateToProps, mapDispatchToProps)
  | injectSaga    ({ saga, key: 'Main', })
