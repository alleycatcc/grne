const config = {
  verbose: true,
}

import {
  SIZE_SELECTED,
} from '../../containers/App/constants'

import {
  setSize,
  forceUpdateWindowList,
} from '../App/actions'

import {
  pipe, compose, composeRight,
} from 'stick-js/es'

import { all, call, put, select, takeLatest, take, } from 'redux-saga/effects'

function *sagaSetSize ({ data: size, }) {
  yield setSize (size) | put
  yield forceUpdateWindowList () | put
}

export default function* defaultSaga () {
  yield all ([
    takeLatest (SIZE_SELECTED, sagaSetSize),
  ])
}
