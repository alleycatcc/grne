import {
  pipe, compose, composeRight,
  map, flip, tap,
  bindProp, mergeM, path, take, head, dot2, side2, noop,
  bindPropTo, defaultTo, ifTrue, ifFalse, whenOk, ok, side1,
  condS, guardV, otherwise, eq, lets, letS, multiply, concat, prop, sprintf1,
  always, ifPredicate, sprintfN, minus, plus, id, ampersandN,
  appendToM, not,
  arg3, die,
  cond, nil, last,
  dot, subtractFrom,
  deconstruct,
} from 'stick-js/es'

import React, { Fragment, PureComponent, } from 'react'

import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import styled from 'styled-components'
import Draggable from 'react-draggable'

import { Just, Nothing, cata, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { clss, addEventListener, preventDefault, } from 'alleycat-js/es/dom'
import { warn, iwarn, mapX, reduceX, logWith, reverse, ierror, notBelow, notAbove, toString, } from 'alleycat-js/es/general'
import { ifTrueV, ifEquals, isNotEmptyList, whenAllOk, } from 'alleycat-js/es/predicate'
import { withResizeListener, createRef, compMeth, setState, } from 'alleycat-js/es/react-legacy'
import { mediaQuery, } from 'alleycat-js/es/styled'

import {
  entriesFetchChunkPrevClicked,
  entriesFetchChunkNextClicked,
} from '../App/actions'

import {
  makeSelectEntries,
  makeSelectMaxLemmaId,
  makeSelectEntriesExploreMode,
  makeSelectEntriesFetchType,
  makeSelectEntriesIdxShift,
  makeSelectEntriesAppending,
  makeSelectEntriesPrepending,
} from '../../slices/domain/selectors'

import EntryList from '../../components/EntryList/'
import { BlueLink, BrowseSpinner, } from '../../components/shared'

import { mediaPhone, mediaTablet, mediaDesktop, isMobileWidth, browseLinkForRouter, } from '../../common'
import config from '../../config'
import { fetchIsPrepend, fetchIsAppend, } from '../../types'
import injectSaga from '../../utils/injectSaga'
import injectReducer from '../../utils/injectReducer'

import saga from './saga'

const LeftS = styled.div`
  position: relative;
  flex: 0 0 175px;
  overflow-y: hidden;
  margin-left: 45px;
  border-top: 1px solid black;
  border-bottom: 1px solid black;
  .x--standalone {
    height: 100%;
    overflow-y: scroll;
    -webkit-overflow-scrolling: touch;
  }
  ${mediaQuery (
    mediaPhone ('display: none;'),
    mediaTablet ('display: block;'),
  )}
`

// --- prevent e.g. selection of text; doesn't prevent clicking links.
const dragStart = preventDefault

// @todo repeated
const ARROW_HEIGHT = 8
const ARROW_PADDING = 5
const LINE_SPACING = 3

// @todo, has to match css in <Lemma/>
const LINE_HEIGHT = {
  small: 21,
  medium: 24,
  big: 27,
}

const arrowHeight = () => ARROW_HEIGHT + LINE_SPACING + ARROW_PADDING * 2

const deltaMultiplier = {
  // DOM_DELTA_PIXEL
  0: 1,
  // DOM_DELTA_LINE
  1: 50,
  // DOM_DELTA_PAGE not supported; use some default.
}

const EntryListWrapperS = styled.div`
  border: 1px solid #999999;
`

        // [() => pending, () => <FetchSpinner pos='bottom'/>],
        // todo chunk, pos ChunkSpinner which='prev/next'

const addWheelListener = drag => addEventListener (
  'wheel',
  ({ deltaY, deltaMode, }) => lets (
    () => deltaMultiplier [deltaMode] | defaultTo (
      () => 20 | tap (
        () => warn ('Unsupported delta mode ' + deltaMode)
      ),
    ),
    (multiplier) => multiplier * deltaY,
    (_, amount) => drag (-amount, true),
  ),
)

class Left extends PureComponent {
  state = {
    draggableBounds: void 8,
    // --- will be set the first time EntryList updates.
    dragTop: void 8,
  }

  minY = void 8
  entriesListRef = createRef ()
  containerRef = createRef ()
  wrapperRef = createRef ()

  getMinY = () => this | compMeth ((
    {}, {}, {
      entriesListRef: { current: wordsListNode },
      containerRef: { current: containerNode },
    },
  ) => [containerNode, wordsListNode] | whenAllOk (
    map ('clientHeight' | prop) >> deconstruct (
      ([ch, wh]) => ch - wh,
    )
  ))

  scrollToEntry = (y) => this | compMeth ((
    {},
    { draggableBounds: { top, bottom } },
    { containerRef: { current: { clientHeight: containerHeight }}}
  ) => lets (
    () => y - containerHeight / 2,
    (newY) => newY | notBelow (bottom) | notAbove (-top),
    (_0, dragTop) => this | setState ({ dragTop, }),
  ))

  // --- we ignore `lastY` and use instead the value we store in state, so we can also handle
  // the wheel manually.

  handleDrag = (_, { lastY: lastYFromAPI, deltaY, }) => {
    const { state, } = this
    const { dragTop, } = state
    const lastY = -dragTop
    if (ok (dragTop) && lastY !== lastYFromAPI) iwarn ("Something's wrong (check dragTop/lastY)")
    return this.drag (deltaY, false)
  }

  // --- mouse wheel events need to manually check bounds; bounding of drag events is handled by
  // Draggable.
  // --- `scrollToEntry` has to have been called at least once, or else dragTop will be
  // undefined.

  drag = (deltaY, checkBounds=false) => this | compMeth ((
    { fontSize, onScrollToEntry, entriesExploreMode, },
    { dragTop, },
    { props, state, minY, },
  ) => {
    const lastY = -dragTop
    const bounded = checkBounds ? (notAbove (0) >> notBelow (minY)) : id
    const y = bounded (lastY + deltaY)
    const lineHeight = LINE_HEIGHT [fontSize] | defaultTo (
      () => die ('missing font size for ' + fontSize),
    )
    const [top, finalIdx] = lets (
      () => y + arrowHeight (),
      (yAdj) => Math.round (-yAdj / lineHeight),
      (_, lemmaIdx) => lemmaIdx | notBelow (0),
      (_0, _1, finalIdx) => [y, finalIdx],
    )
    this | setState ({ dragTop: -top, })
    if (entriesExploreMode) return
    // --- could be moved to componentDidUpdate, but ok.
    finalIdx | onScrollToEntry
  })

  // --- @todo this is repeated in BrowseComponentM.
  adjustEntryScroll = (amount) => this | compMeth ((
    { entriesFetchType, },
    {},
    { wrapperRef: { current: ref }},
  ) => {
    if (entriesFetchType | fetchIsPrepend)
      ref.scrollTop = amount
  })

  update = () => this | deconstruct (
    ({ getMinY, minY: curMinY }) => {
      const minY = getMinY ()
      if (!ok (minY) || minY === curMinY) return
      this
        | mergeM ({ minY, })
        | setState ({
          draggableBounds: {
            bottom: 0,
            top: minY,
          },
        })
    }
  )

  componentDidMount = () => this | compMeth ((
    {},
    {},
    { drag, containerRef: { current: ref, }}
  ) => {
      ref | addWheelListener (drag)
      return this.update ()
    }
  )

  componentDidUpdate = (prevProps, prevState) => this | compMeth (
    ({ entries, entriesExploreMode, entriesFetchType },
    {},
    {
      update,
      wrapperRef: { current: ref },
    },
  ) => {
    // --- @todo why was this necessary?
    if (prevProps.entries !== entries) this.forceUpdate ()
    update ()
    if (
      entriesExploreMode &&
      !prevProps.entriesExploreMode &&
      (entriesFetchType | ok)
    )
      // --- when appending, we switch to explore mode, and then the saga gives us a chance to
      // update once before it fetches.
      // --- scroll to the end before it fetches to the fetch is smooth.
      lets (
        () => entriesFetchType | cata ({
          FetchAppend: () => 1e10,
          FetchPrepend: () => 0,
          FetchReplace: () => 0,
        }),
        (scrollTop) => ref.scrollTop = scrollTop,
      )
  })

  render = () => this | compMeth ((
    {
      entries,
      selectedLemmaIdx, entriesIdxShift,
      maxLemmaId,
      entriesAppending, entriesPrepending,
      entrySelectedOffsetTop, entrySetSelectedOffsetTopDispatch,
      entriesExploreMode,
      entriesFetchType,
      entriesFetchChunkPrevClickedDispatch, entriesFetchChunkNextClickedDispatch,
      onSelectEntry, fontSize,
    },
    { draggableBounds, dragTop, },
    { entriesListRef, containerRef, wrapperRef, handleDrag, scrollToEntry, adjustEntryScroll, },
  ) => {
    if (entries | nil) return null

    const showArrows = entries | isNotEmptyList
    const firstLemmaId = entries | head | whenOk (0 | prop)
    const lastLemmaId = entries | last | whenOk (0 | prop)
    const showArrowUp = showArrows && firstLemmaId !== 0
    const showArrowDown = showArrows && lastLemmaId !== maxLemmaId

    const standalone = entriesExploreMode

    const clssWrapper = clss (
      standalone && 'x--standalone',
    )

    return <LeftS ref={containerRef}>
      <Draggable
        disabled={standalone}
        axis='y'
        onMouseDown={dragStart}
        onDrag={handleDrag}
        bounds={draggableBounds}
        position={{ x: 0, y: standalone ? 0 : -dragTop, }}
        cancel='.disable-drag'
      >
        <div ref={wrapperRef} className={clssWrapper}>
          <div ref={entriesListRef}>
            <EntryList
              entries={entries}
              entriesAppending={entriesAppending}
              entriesPrepending={entriesPrepending}
              disableClass='disable-drag'
              selectedLemmaIdx={selectedLemmaIdx + entriesIdxShift}
              maxLemmaId={maxLemmaId}
              scrollToEntry={scrollToEntry}
              exploreMode={entriesExploreMode}
              selectedOffsetTop={entrySelectedOffsetTop}
              setSelectedOffsetTopDispatch={entrySetSelectedOffsetTopDispatch}
              adjustEntryScroll={adjustEntryScroll}
              standalone={standalone}
              onSelectEntry={onSelectEntry}
              fetchType={entriesFetchType}
              fontSize={fontSize}
              entriesFetchChunkPrevClickedDispatch={entriesFetchChunkPrevClickedDispatch}
              entriesFetchChunkNextClickedDispatch={entriesFetchChunkNextClickedDispatch}
            />
          </div>
        </div>
      </Draggable>
    </LeftS>
  })
}

const mapStateToProps = createStructuredSelector ({
  entries: makeSelectEntries (),
  entriesAppending: makeSelectEntriesAppending (),
  entriesPrepending: makeSelectEntriesPrepending (),
  maxLemmaId: makeSelectMaxLemmaId (),
  entriesFetchType: makeSelectEntriesFetchType (),
  entriesExploreMode: makeSelectEntriesExploreMode (),
  entriesIdxShift: makeSelectEntriesIdxShift (),
})

const mapDispatchToProps = (dispatch) => ({
  entriesFetchChunkPrevClickedDispatch: entriesFetchChunkPrevClicked >> dispatch,
  entriesFetchChunkNextClickedDispatch: entriesFetchChunkNextClicked >> dispatch,
})

export default Left
  | withResizeListener
  | connect       (mapStateToProps, mapDispatchToProps)
  | injectSaga    ({ saga, key: 'left' })
