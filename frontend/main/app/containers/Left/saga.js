import {
  pipe, compose, composeRight,
  merge,
  tap, concatTo,
  prop,
  map,
  ok,
  id, minus,
  mergeM,
  reduceObj,
  reduce,
  ifOk,
  appendM,
  eq, ifPredicate, not, whenOk,
  sprintfN,
} from 'stick-js/es'

import { all, call, put, select, takeLatest, take, } from 'redux-saga/effects'

import { Left, Right, fold, flatMap, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { iwarn, ierror, tellIf, logWith, toString, } from 'alleycat-js/es/general'
import { errorError, error as alertError, } from 'alleycat-js/es/react-s-alert'

import {
  makeSelectLemmaIdMin,
  makeSelectLemmaIdMax,
} from '../../slices/domain/selectors'

import { debugDev, } from '../../common'
import config from '../../config'
import { envIsTst, } from '../../env'
import { FetchReplace, FetchAppend, FetchPrepend, } from '../../types'

function *generalAlert () {
  true | alertError
}

export default function *defaultSaga () {
  yield all ([
  ])
}
