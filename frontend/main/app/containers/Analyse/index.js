import {
  pipe, compose, composeRight,
  join, tap, not, sprintfN, ifOk, whenOk, assocM, noop,
  ifNotOk, list, defaultTo, lets, letS, find, prop,
  reduce, mergeM, map, addIndex, each, whenTrue,
  cond, condS, guard, otherwise, guardV, isString,
  eq, values, die, concatTo, ne, dot, whenPredicate,
  path, bindProp, side, side1, ok, notOk, ifPredicate, always,
  merge, id, sprintf1, take, split, head, dot1, ifTrue,
  tail, mapValues, allAgainst, againstAll, whenYes, T, F,
  recurry,
  deconstruct, toPairs, fromPairs,
  side2, invoke,
  ifYes,
  compact,
  assocPathM,
  ifPredicateV,
} from 'stick-js/es'

import React, { Fragment, PureComponent, useEffect, } from 'react'

import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import daggy from 'daggy'
import styled from 'styled-components'

import { then, thenTap, recover, resolveP, allP, } from 'alleycat-js/es/async'
import { Just, Nothing, Left, Right, cata, fold, foldJust, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { inputValue, hideAllChildrenOf, keyPressListenPreventDefault, clss, } from 'alleycat-js/es/dom'
import { requestJSON, resultFold, defaultOpts as defaultFetchOpts, } from 'alleycat-js/es/fetch'
import { mapX, tellIf, iwarn, ierror, logWith, length, defaultToV,
  trim, headMaybe, toString, reverse, toHex, curlyQuote,
  setTimeoutOn,
} from 'alleycat-js/es/general'
import {
  whenNotPredicate, ifTrueV, ifFalseV, ifOkV, ifNotOkV,
  whenNotTrue, isEmptyList, ifArray, notTrue, ifNotTrue,
  isEmptyString,
  ifEmptyList, ifEquals,
  isNotEmptyList,
  okAndNotFalse,
} from 'alleycat-js/es/predicate'
import { createRef, setStateValueMeth, setState, compMeth, } from 'alleycat-js/es/react-legacy'
import { withDisplayName, } from 'alleycat-js/es/react'
import { ierrorError, ierrorErrorDie, error as alertError, } from 'alleycat-js/es/react-s-alert'
import { mlt, mgt, mediaComp, media, } from 'alleycat-js/es/styled'

import { spinner, } from '../../alleycat-components'

import AnalyseOutputLinks from '../../components/AnalyseOutputLinks'
import { Lemma, } from '../../components/Lemma'
import { Div, ExternalLink, Button, } from '../../components/shared'
import InfoIcon from '../../components/svg/InfoIcon'
import CloseIcon from '../../components/svg/CloseIcon'

import { checkUploadFilesSize, makeFormData, } from '../../common'
import config from '../../config'

const configTop = config | configure.init

const configFont = 'font.analyse' | configTop.get
const configMaxZipFileUploadSize = 'analyse.maxExportZipUploadSize' | configTop.get
const { css: fontCss, family: fontFamily, } = lets (
  _ => configFont | configure.init,
  conf => conf.gets ('css', 'family'),
)

// --- not efficient obviously
const takeLastN = n => reverse >> take (n) >> reverse

const whenEmptyString = isEmptyString | whenPredicate

const isDefined = void 8 | ne
const ifYesV = Boolean | ifPredicateV

const SpinnerTextblocks = 'textblocks' | spinner
const SpinnerComet = 'comet' | spinner

const RequestType = daggy.taggedSum ('RequestType', {
  RequestJSON: ['url'],
  RequestForm: ['url'],
})

const { RequestJSON, RequestForm, } = RequestType

// :: RequestType -> [url, method, content-type header mixin]

const requestTypeParams = cata ({
  RequestJSON: (url) => [
    url, 'POST', { 'Content-Type': 'application/json' },
    (data) => ({ data, }) | JSON.stringify,
  ],
  RequestForm: (url) => [
    // --- leave content-type blank; browser will induce 'multipart form' based on the body being
    // FormData.
    url, 'POST', {},
    makeFormData,
  ],
})

const TopS = styled.div`
  ${_ => fontCss}
  font-family: ${_ => fontFamily};

  height: 100%;
  // width: 100%;
  margin: auto;
  border: 1px solid black;
  overflow-y: scroll;
  padding-top: 1%;

  input, textarea {
    font-family: monospace;
    padding-left: 2px;
  }

  .x--soft-keyboard-visible & {
    height: 10000px;
  }
`

const pipelineFunction = recurry (4) (
  (urlSpec) => (inputKeys) => (inputData) => (done) => {
    const getPair = condS ([
      isString | guard (k => ({
        [k]: inputData [k],
      })),
      otherwise | guard (([k1, f]) => ({
        [k1]: f (inputData),
      })),
    ])
    const reducer = reduce ((acc, k) => acc | merge (getPair (k)))
    const data = inputKeys | reducer ({})

    const [url, method, contentType, makeBody] = urlSpec | requestTypeParams
    const body = data | makeBody

    const options = {
      method,
      body,
      headers: {
        ...contentType,
      },
      credentials: 'include',
    }

    // --- error can be a string or a list.
    const isError = againstAll ([ok, isNotEmptyList])
    const ifError = isError | ifPredicate
    // --- note that this is a different data structure than the one we fold over below with
    // `resultFold`.
    const processData = ({ error, results, }) => error | ifError (
      ResultError,
      _ => ResultResult (results),
    )

    // --- can't fail.
    requestJSON (url, options)
      | then (resultFold (
        // --- ok (2xx)
        prop ('data') >> processData >> done,
        // --- user error (e.g. 4xx with a `umsg` field)
        (umsg) => umsg | ResultError | done,
        // --- non-user error (e.g. 4xx without a `umsg` field, 5xx, network failure etc.)
        (imsgMb) => {
          true | alertError
          imsgMb | foldJust (iwarn)
          'Error.' | ResultError | done
        }
      ))
      | recover (() => ierrorErrorDie ('unexpected'))
  }
)

const pipelineFunctionXmlToHtml = pipelineFunction (
  RequestForm ('/api/analyse/xml-to-html'),
  [
    'lemma-xml',
    ['lemma-xml-batch', prop ('lemma-xml-batch')],
    'xslt',
    // --- @todo make generic function for boolean
    ['make_highlight_snippet', prop ('make_highlight_snippet') >> eq ('true')],
    ['reductie_modus', prop ('reductie_modus') >> condS ([
      'geen'    | eq | guardV ('none'),
      otherwise | guard (id),
    ])],
    'terms',
  ],
)

const pipelineFunctionAutocomplete = pipelineFunction (
  RequestJSON ('/api/analyse/autocomplete'),
  ['query', 'database', 'strictness', 'length', 'limit'],
)

const pipelineFunctionAnalyseChars = pipelineFunction (
  RequestJSON ('/api/analyse/analyse-chars'),
  ['query'],
)

const pipelineFunctionAnalyseAndConvertQuery = pipelineFunction (
  RequestJSON ('/api/analyse/analyse-and-convert-query'),
  ['query'],
)

const pipelineFunctionSolrQueryProcess = pipelineFunction (
  RequestJSON ('/api/analyse/solr-query-process'),
  ['query', 'type'],
)

const pipelineFunctionSolrIndexPreProcess = pipelineFunction (
  RequestJSON ('/api/analyse/solr-index-pre-process'),
  ['xml'],
)

const pipelineFunctionSolrIndexAdd = pipelineFunction (
  RequestJSON ('/api/analyse/solr-index-add'),
  ['xml', 'target'],
)

const pipelineFunctionSolrQueryExecute = pipelineFunction (
  RequestJSON ('/api/analyse/solr-query-execute'),
  ['query', 'target',
    // --- @todo make generic function for boolean
    ['raw', prop ('raw') >> eq ('true')],
    // --- @todo make generic function for number
    ['num-rows', prop ('num-rows') >> Number],
    ['start-row', prop ('start-row') >> Number],
  ],
)

const pipelineFunctionLinks = pipelineFunction (
  RequestJSON ('/api/analyse/find-broken-links'),
  [
    // --- @todo make generic function for boolean
    ['only-existing-letters', prop ('only-existing-letters') >> eq ('true')],
  ],
)

const AnalyseOutputS = styled.div`
  ${ media (
    768 | mlt | mediaComp ('flex: 0 0 90vw;'),
    768 | mgt | mediaComp ('flex: 0 0 620px;'),
  )}

  max-width: 620px;

  .x__title {
    text-decoration: underline;
    margin-bottom: 10px;
  }
  .x__code {
    border: 1px solid black;
    padding: 5px;
    font-family: monospace;
    font-size: 15px;
    background: #ddd;
    display: inline-block;
    cursor: text;
  }
`

const AnalyseInputS = styled.div`
  ${ media (
    768 | mlt | mediaComp ('flex: 0 0 90vw;'),
    768 | mgt | mediaComp ('flex: 0 0 500px;'),
  )}
  margin-right: 30px;
  margin-bottom: 50px;
  vertical-align: top;

  .x__submit {
    border: 1px solid black;
    padding: 5px;
    padding-right: 10px;
    border-radius: 30px;
    font-size: 14px;
    display: inline-block;
    cursor: pointer;
    margin-bottom: 25px;
    cursor: pointer;
    top: -2px;
    position: relative;
    &:active {
      border-style: solid;
      transition: all .03s;
      transform: translateY(1px) translateX(1px);
      opacity: 0.8;
    }
    &:focus {
      outline: none;
    }
    .x__span1 {
      display: inline-block;
    }
    .x__span2 {
      display: inline-block;
      vertical-align: middle;
    }
  }
  button:active .x__span2 {
  }

  .x__input {
    display: inline-block;
    margin-right: 1%;
    width: 100%;
    .x__title {
      display: inline-block;
      text-decoration: underline;
      font-size: 17px;
      min-height: 30px;
      vertical-align: middle;
    }
  }

  .x--hide {
    display: none;
  }
`

const TextFieldWrapper = styled.div`
  display: block;
  font-size: 12px;
  border: 1px solid black;
  width: 100%;
  height: ${prop ('big') >> ifTrueV ('300px', '25px')};
`

const InputS = styled.input`
  width: 100%;
  height: 100%;
`

const TextAreaS = styled.textarea`
  width: 100%;
  height: 100%;
`

const TextInput = ({ submit, inputProps, }) => <TextFieldWrapper>
  <InputS
    spellCheck='false'
    autoComplete='off'
    autoCorrect='off'
    autoCapitalize='off'
    onKeyPress={'Enter' | keyPressListenPreventDefault (submit)}
    {...inputProps}
  />
</TextFieldWrapper>

const TextArea = ({ inputProps, }) => <TextFieldWrapper big={true}>
  <TextAreaS
    spellCheck='false'
    autoComplete='off'
    autoCorrect='off'
    autoCapitalize='off'
    {...inputProps}
  />
</TextFieldWrapper>

const FileUploadButtonS = styled.input`
  font-size: 15px;
  font-family: Source Sans Pro;
`

const FileUploadButton = ({ inputProps, }) => <FileUploadButtonS
  type='file'
  {...inputProps}
/>

const RadioGroupS = styled.div`
  font-size: 15px;
  .x__input {
    margin-left: 5px;
    display: inline-block;
  }
  .x__label {
    margin-left: 5px;
    display: inline-block;
  }
`

const RadioGroup = ({ spec, inputProps: { onChange, defaultValue }, InnerElement=RadioGroupS, }) => <InnerElement>
  <form onChange={onChange}>
    {spec | mapX ((item, idx) => item | deconstruct (
      ({ value, label, isDefault, }) => lets (
        _ => (value === defaultValue) || isDefault,
        (checked) =>
          <div className='x__input' key={idx}>
            <input type='radio' defaultChecked={checked} value={value} name='dummy' />
            <div className='x__label'>{label}</div>
          </div>
        ),
      ),
    )}
  </form>
</InnerElement>

const inputGetValueMbNormal = inputValue >> trim >> Just
const inputGetValueMbFile = ({ target: { files=[] }}) => {
  if (files | isEmptyList) return Nothing
  const file = files | head
  if (!checkUploadFilesSize ({
    file,
    alertFunc: alertError,
    maxFileSize: configMaxZipFileUploadSize,
  })) return Nothing
  return file | Just
}

// :: Input -> [input component, inputGetValueMb, extraProps] where
//   inputGetValueMb :: event -> Maybe value

// --- it's a maybe because getting the value of a form field can fail, for example in the case of a
// file which is too big or had its file dialog closed.

const getAnalyseInput = cata ({
  InputDummy: () => [Div, Nothing | always, {}],
  InputRadioGroup: (_, spec) => [RadioGroup, inputGetValueMbNormal, { spec }],
  InputTextLine: () => [TextInput, inputGetValueMbNormal, {}],
  InputTextArea: () => [TextArea, inputGetValueMbNormal, {}],
  InputFileUpload: () => [FileUploadButton, inputGetValueMbFile, {}],
})

const isInputDummy = cata ({
  InputDummy: T,
  InputRadioGroup: F,
  InputFileUpload: F,
  InputTextLine: F,
  InputTextArea: F,
})

const InputTitleS = styled.div`
`

const InfoS = styled.div`
  border: 1px solid #999999;
  padding: 5px;
  margin-bottom: 10px;
  .x__left, .x__right {
    height: 100%;
    display: inline-block;
  }
  .x__left {
    width: calc(100% - 32px);
  }
  .x__right {
    vertical-align: top;
    width: 32px;
  }
`

const Info = ({ children, hide, }) => <InfoS>
  <div className='x__left'>
    {children}
  </div>
  <div className='x__right'>
    <CloseIconS
      onClick={hide}
    />
  </div>
</InfoS>

const InputTitle = ({ title, info: InfoInner, showInfo, hideInfo, infoVisible, }) => <InputTitleS>
  <div className='x__title'>
    {title}
  </div>
  {InfoInner | whenOk (
    _ => infoVisible | whenNotTrue (
      _ => <InfoIconS
        onClick={showInfo}
      />,
    )
  )}
  {(InfoInner && infoVisible) | whenYes (
    _ => <Info hide={hideInfo}>
      <InfoInner />
    </Info>
  )}
</InputTitleS>

class AnalyseInputControl extends PureComponent {
  constructor (props) {
    super (props)
    const { info, } = props
    const setState = this | bindProp ('setState')
    this | mergeM ({
      showInfo: _ => setState ({ infoVisible: true, }),
      hideInfo: _ => setState ({ infoVisible: false, }),
      state: {
        infoVisible: false,
      },
    })
  }

  render () {
    const { props, state, showInfo, hideInfo, } = this
    const { infoVisible, } = state
    const {
      Input,

      // inputGetValueMb :: event -> Maybe value
      inputGetValueMb,

      idx, className, title, info, update, defaultValue, extraProps,
      onSubmit, onChange,
    } = props

    const onChangeProp = inputGetValueMb >> fold (
      update >> onChange,
      noop,
    )

    return <div key={idx} className={className}>
      <InputTitle
        title={title}
        info={info}
        showInfo={showInfo}
        hideInfo={hideInfo}
        infoVisible={infoVisible}
      />
      <Input
        // --- this is so that you can trigger submit by pressing enter in a text field or by
        // changing a submit-on-change field.
        submit={onSubmit}
        inputProps={{
          defaultValue,
          onChange: onChangeProp,
        }}
        {...extraProps}
      />
    </div>
  }
}

// --- we have one of these per Analyse, corresponding to the left/input side.
// --- it consists of a list of <AnalyseInputControl>, each of which contains a title and an input
// field.

const AnalyseInput = ({ spec, showSubmitButton, onSubmit, }) => <AnalyseInputS>
  {showSubmitButton &&
      <button className='x__submit' onClick={onSubmit}><div
        className='x__span1'
      >→</div> <div
        className='x__span2'
      >submit</div></button>
  }

  {spec | mapX ((props, idx) => props | letS ([
    ({ input, }) => input | getAnalyseInput,
    ({ title, submitOnChange, }) => submitOnChange | ifTrueV (
      InputChangeUpdateSubmit, InputChangeUpdate,
    ),
    ({ visible, }) => visible | ifTrueV ('', 'x--hide'),
    // eslint-disable-next-line react/display-name
    (
      { onChange, title, defaultValue, submitOnChange, info },
      [Input, inputGetValueMb, extraProps],
      update,
      visibleClass
    ) => <AnalyseInputControl
      key={idx}
      idx={idx}
      Input={Input}
      className={['x__input', visibleClass] | join (' ')}
      title={title}
      info={info}
      update={update}
      onChange={onChange}
      onSubmit={onSubmit}
      defaultValue={defaultValue}
      extraProps={extraProps}
      inputGetValueMb={inputGetValueMb}
    />,
    ]),
  )}
</AnalyseInputS>

const Reductie = daggy.taggedSum ('Reductie', {
  ReductieGeen: [],
  ReductieSchoolStandaard: [],
  ReductieSchoolAuto: [],
  ReductieSchoolT1T2: [],
  ReductieSchoolT2: [],
  ReductieBeknopt: [],
})

const {
  ReductieGeen,
  ReductieSchoolStandaard,
  ReductieSchoolAuto, ReductieSchoolT1T2, ReductieSchoolT2,
  ReductieBeknopt,
} = Reductie

const makeReductie = condS ([
  'none' | eq | guard (_ => ReductieGeen),
  'school-standaard' | eq | guard (_ => ReductieSchoolStandaard),
  'school-auto' | eq | guard (_ => ReductieSchoolAuto),
  'school-t1-t2' | eq | guard (_ => ReductieSchoolT1T2),
  'school-t2' | eq | guard (_ => ReductieSchoolT2),
  'beknopt' | eq | guard (_ => ReductieBeknopt),
  otherwise | guard (String >> concatTo ('bad reductie type: ') >> ierrorError),
])

const hideCitsAndPuncStan = el => ['.punc-stan', '.cit'] | hideAllChildrenOf (el)
const hidePuncBekn = el => ['.punc-bekn'] | hideAllChildrenOf (el)

const lemmaUpdate = (reductie) => (ref) => reductie | cata ({
  ReductieGeen: _ => ref | hidePuncBekn,
  ReductieBeknopt: _ => ref | hideCitsAndPuncStan,
  ReductieSchoolStandaard: noop,
  ReductieSchoolAuto: noop,
  ReductieSchoolT1T2: noop,
  ReductieSchoolT2: noop,
})

const XmlSchoolS = styled.div`
  width: 100%;
  ${prop('isEmpty') >> whenOk (_ => 'overflow-x: scroll;')}
  font-size: 10px;
  margin-top: 25px;
`

const HtmlXmlTitle = styled.div`
  text-decoration: underline;
  font-size: 20px;
  width: 100%;
  text-align: center;
  margin-bottom: 30px;
`

const XmlSchool = ({ xml, haveResults, }) => haveResults | ifNotTrue (
  _ => null,
  _ => lets (
    () => xml | ok | not,
    (isEmpty) =>
      <XmlSchoolS haveResults={haveResults}>
        <div>
          <HtmlXmlTitle>
            Schoolversie Eind-XML
          </HtmlXmlTitle>
          <pre>
            {isEmpty ? '[leeg]' : xml}
          </pre>
        </div>
      </XmlSchoolS>
  ),
)

class LemmaWrapper extends PureComponent {
  constructor (props) {
    super (props)
    const { reductie, } = props
    this | mergeM ({
      ref: createRef (),
    })
  }

  update () {
    const { props, ref, } = this
    const { reductie, } = props
    const onUpdate = reductie | lemmaUpdate
    ref.current | whenOk (onUpdate)
  }

  componentDidUpdate () {
    return this.update()
  }

  componentDidMount () {
    return this.update()
  }

  render () {
    const { props, ref, } = this
    const { title='Lemma HTML', error, htmlNormal, htmlSchool, reductie, isBatch, filepath, t1Gecontroleerd, } = props

    const emptyMessage = filepath | sprintf1 ('Geen resultaat voor bestand: %s')

    // --- in batch mode we don't show the filename titles (so caller should just send nil) and we
    // reduce the space between the entries.

    return lets (
      _ => reductie | cata ({
        // --- [html, isSchool]
        ReductieGeen: _ => [htmlNormal, false],
        ReductieBeknopt: _ => [htmlNormal, false],
        ReductieSchoolStandaard: _ => [htmlSchool, true],
        ReductieSchoolAuto: _ => [htmlSchool, true],
        ReductieSchoolT1T2: _ => [htmlSchool, true],
        ReductieSchoolT2: _ => [htmlSchool, true],
      }),
      ([html, isSchool]) => <LemmaS bottomSpace={isBatch | not}>
        <Fragment>
          {isBatch || <HtmlXmlTitle>
            {title}
          </HtmlXmlTitle>}
          {error | ifOk (
            () => "Error: " + error,
            () => <Lemma
              theRef={ref}
              html={htmlNormal}
              htmlSchool={htmlSchool || emptyMessage}
              reductieModeSchool={isSchool}
              t1Gecontroleerd={t1Gecontroleerd}
              postProcess={false}
            />,
          )}
        </Fragment>
      </LemmaS>,
    )
  }
}

const LemmaS = styled.div`
  ${prop ('bottomSpace') >> ifYesV (`
    margin-bottom: 30px;
  `, `
    margin-bottom: 5px;
  `)}
  width: 100%;
`

const StatusInnerS = styled.div`
  flex: 0 0 100%;
  ${prop ('error') >> condS ([
    notOk          | guardV ('color: black;'),
    false     | eq | guardV ('color: green;'),
    otherwise      | guardV ('color: #b32222;'),
  ])}
  margin-left: 30px;
  .x__error-msg {
    display: inline-block;
    overflow-x: auto;
    white-space: nowrap;
    width: 80%;
    vertical-align: middle;
    margin-left: 5%;
    padding: 10px;
    height: 3.5em;
    border-style: solid;
    border-color: black;
    border-width: 0px;
  }
`

const StatusInner = ({ error, spinning, }) => lets (
  _ => error | okAndNotFalse,
  (hasErr) => hasErr | ifTrueV (['x--error'], []),
  (_, errCls) => ['x__error-msg', ...errCls] | join (' '),
  (_, _2, cls) => <StatusInnerS error={error}>
    <div>
      {error | condS ([
        notOk      | guardV (''),
        false | eq | guardV ('✔'),
        otherwise  | guardV ('✘'),
      ])}
      <SpinnerTextblocks spinning={spinning}/>
      <div className={cls}>
        {error}
      </div>
    </div>
  </StatusInnerS>
)

const Status = ({ status, error, }) => status | condS ([
  'ok'      | eq | guard (_ => <StatusInner error={false}/>),
  'pending' | eq | guard (_ => <StatusInner spinning={true}/>),
  'error'   | eq | guard (_ => <StatusInner error={error}/>),
  otherwise |      guard (_ => <StatusInner />),
])

const XmlToHtmlBatchSummaryS = styled.div`
  margin-bottom: 20px;
  .x__arrow {
    display: none;
    font-size: 13px;
    padding-right: 6px;
  }
`

const LinkDisabledS = styled.a`
  cursor: initial;
  opacity: 0.5;
`

const LinkDisabled = ({ children, }) => <LinkDisabledS
  onClick={'preventDefault' | side}
>
  {children}
</LinkDisabledS>

// rel="noopener noreferrer"
// <div><span className='x__arrow'>→</span><LinkDisabled href={/* linkDocxGrabzit */} target="_blank">download .docx bundle (“grabzit”) (disabled)</LinkDisabled></div>

const XmlToHtmlBatchSummary = ({ linkPrintable, /*linkDocxGrabzit, */linkDocxLibreOffice, linkPdf, }) => <XmlToHtmlBatchSummaryS>
  <div className='x__title'>Batch Mode Results</div>
  <div><span className='x__arrow'>→</span><a href={linkPrintable} target="_blank">show printable version</a></div>
  <div><span className='x__arrow'>→</span><a href={linkDocxLibreOffice} target="_blank">download .docx file</a></div>
  {
    /*
    <div><span className='x__arrow'>→</span><a href={linkPdf} target="_blank" rel="noopener noreferrer">download .pdf bundle (experimental)</a></div>
     */
  }
</XmlToHtmlBatchSummaryS>

const AnalyseOutputXmlToHtml = invoke (() => {
  const OutS = styled.div`
    cursor: text;
  `

  // results :: Either String (Maybe Results)

  return ({
    // --- batch mode
    resultsCached,

    // --- normal mode
    results=resultsCached | Just | Right,
    extraProps,
    inputData,
  }) => {

    return lets (
      () => results | fold (
        _ => ({}),
        maybe => maybe | fold (
          ({ is_batch, batch_id, reductie_modus, results, }) => ({
            results,
            isBatch: is_batch,
            batchId: batch_id,
            reductie: reductie_modus | makeReductie,
          }),
          [],
        ),
      ),
      // --- xml_school=null means we got a response with empty xml.
      ({ isBatch, batchId, reductie, results }) => <OutS>
          {(isBatch && !resultsCached) | whenTrue (() =>
            lets (
              () => '/analyse/xml-to-html-print/' + String (batchId),
              // () => '/api/analyse/xml-to-html-docx-grabzit/' + String (batchId),
              () => '/api/analyse/xml-to-html-docx-libreoffice/' + String (batchId),
              () => '/api/analyse/xml-to-html-pdf/' + String (batchId),
              (linkPrintable, /* linkDocxGrabzit, */ linkDocxLibreOffice, linkPdf) => <XmlToHtmlBatchSummary
                linkPrintable={linkPrintable}
                // linkDocxGrabzit={linkDocxGrabzit}
                linkDocxLibreOffice={linkDocxLibreOffice}
                linkPdf={linkPdf}
              />,
            ),
          )}
          {
            results | whenOk ((res) => {
              // --- non-local effect: changes <html> and <body> to make the page print properly.
              // --- tested on FF and Chromium.
              // --- note: the name of the parent component ('Analyse') is hard-coded.
              if (isBatch) useEffect (() => {
                const html = document.querySelector ('html')
                const { body, } = document

                html.style.position = 'static'
                body.style.position = 'static'

                html.style.height = 'initial'
                body.style.height = 'initial'

                html.style.overflowY = 'scroll'
                document.querySelectorAll ('div[class*=Analyse]').forEach ((x) => x.style.overflowY = 'auto')
              }, [])

              return res | mapX (
                ({
                  filepath, this_error: thisError,
                  html_normal: htmlNormal, html_school: htmlSchool, xml_school: xmlSchool,
                  t1_gecontroleerd: t1Gecontroleerd,
                }, idx) =>
                cond (
                  [() => isBatch && resultsCached, () =>
                    <Fragment key={idx}>
                      <LemmaWrapper
                        error={thisError}
                        isBatch={true}
                        htmlNormal={htmlNormal}
                        htmlSchool={htmlSchool}
                        t1Gecontroleerd={t1Gecontroleerd}
                        filepath={filepath}
                        reductie={reductie}
                      />
                    </Fragment>,
                  ],
                  [() => isBatch && !resultsCached, () => null],
                  [T, () =>
                    <Fragment key={idx}>
                      <LemmaWrapper
                        isBatch={false}
                        htmlNormal={htmlNormal}
                        htmlSchool={htmlSchool}
                        t1Gecontroleerd={t1Gecontroleerd}
                        reductie={reductie}
                      />
                      <XmlSchool xml={xmlSchool} haveResults={xmlSchool | isDefined}/>
                    </Fragment>,
                  ],
                )
              )
            })
          }
      </OutS>,
    )
  }
})

const AnalyseOutputAutocomplete = (() => {
  const abridgeFilename = split ('/') >> takeLastN (3) >> join ('/')

  const format = map (
    row => row | toPairs | mapX (
      ([key, value], idx) => lets (
        _ => key | condS ([
          'filename' | eq | guard (_ => value | abridgeFilename),
          otherwise       | guardV (value),
        ]),
        (valueStr) => <ResultDisplayKeyValue
          key={idx}
          theKey={key}
          value={valueStr}
        />
      )
    )
  )

  return ({ results, }) => lets (
    _ => results | fold (_ => Nothing, id),
    (out) => <div>
      {out | fold (
        ifEmptyList (
          _ => 'no results',
          format >> mapX ((str, idx) =>
            <ResultDisplayWrapper key={idx}>
              {str}
            </ResultDisplayWrapper>
          ),
        ),
        '',
      )}
    </div>
  )
}) ()

const AnalyseOutputSolrQueryProcess = (() => {
  const output = (out) => <Fragment>
      <div className='x__title'>
        Generated Solr Query
      </div>
      <div className='x__code'>
        {out}
      </div>
    </Fragment>

  return ({ results, }) => lets (
    _ => results | fold (_ => Nothing, id),
    (out) => out | fold (output, ''),
  )
}) ()

const AnalyseOutputAnalyseChars = (() => {
  const format = (vals) =>  <ResultDisplayWrapper>
      {vals ['analysis'] | mapX ((analysisItem, idx1) =>
        <ResultDisplayKeyValueWrapper key={idx1}>
          {[
            ['input'],
            ['name', void 8, curlyQuote],
            ['ord', 'code point (decimal)'],
            ['ord', 'code point (hexadecimal)', toHex],
          ]| mapX (([theKey, alias=theKey, convert=id], idx2) =>
            <ResultDisplayKeyValue
              key={idx2}
              theKey={theKey}
              alias={alias}
              value={analysisItem [theKey] | ifOk (convert) (_ => 'none')}
            />
           )}
        </ResultDisplayKeyValueWrapper>
      )}
      <ResultDisplayKeyValue
        theKey={'num_non_canonical'}
        alias={'# non-canonical chars'}
        value={vals ['num_non_canonical'] || '0'}
      />
    </ResultDisplayWrapper>

      return ({ results, }) =>  lets (
    _ => results | fold (_ => Nothing, id),
    (out) => out | fold (format, ''),
  )
}) ()


const AnalyseOutputAnalyseAndConvertQuery = (() => {
  const format = (vals) =>
    <ResultDisplayWrapper>
      {[
        ['converted'],
        ['mode'],
        ['pure_spiritus_left', 'pure spiritus left'],
        ['pure_spiritus_right', 'pure spiritus right'],
        ['pure_spiritus_combined', 'pure spiritus combined'],
        ['uninorm_nfc', 'normalisation NFC'],
        ['uninorm_nfd', 'normalisation NFD'],
      ] | mapX (([theKey, alias=theKey], idx) =>
        <ResultDisplayKeyValue
          key={idx}
          theKey={theKey}
          alias={alias}
          value={vals [theKey] || 'none'}
        />
      )}
    </ResultDisplayWrapper>

  return ({ results, }) => lets (
    _ => results | fold (_ => Nothing, id),
    (out) => out | fold (format, ''),
  )
}) ()

const AnalyseOutputSolrIndexPreProcess = (() => {
  const format = ({ output, stage1, }) => <ResultDisplayWrapper>
    <div className='x__title'>
      Stage 1 (first pass)
    </div>
    <ResultDisplay>
      <pre>
        {stage1}
      </pre>
    </ResultDisplay>

    <div className='x__title'>
      Stage 2 (sent to solr)
    </div>
    <ResultDisplay>
      <pre>
        {output}
      </pre>
    </ResultDisplay>
  </ResultDisplayWrapper>

  return ({ results, }) => lets (
    _ => results | fold (_ => Nothing, id),
    (out) => out | fold (format, ''),
  )
}) ()

const AnalyseOutputSolrIndexAdd = (() => {
  return ({ results, }) => lets (
    _ => results | fold (_ => Nothing, id),
    (out) => out | fold (_ => 'ok', ''),
  )
}) ()

const ResultDisplayWrapperS = styled.div`
  padding-bottom: 15px;
  .x__title {
    text-decoration: underline;
  }
`

const ResultDisplayWrapper = ({ children, }) => <ResultDisplayWrapperS>
  {children}
</ResultDisplayWrapperS>

const ResultDisplayS = styled.div`
  font-size: 12px;
  line-height: 1.4em;
  font-family: monospace;
  margin-bottom: 5px;
  pre {
    white-space: pre-wrap;
  }
  .x__listitem {
    margin-left: 10px;
  }
`

const ResultDisplay = ({ children, }) => <ResultDisplayS>
  {children}
</ResultDisplayS>

const ResultDisplayKeyValue = (() => {
  const doArray = mapX ((x, idx) =>
    <div key={idx} className='x__listitem'>{x}</div>
  )
  return ({ theKey, alias=theKey, value, }) => lets (
    _ => value | ifArray (doArray, id),
    (valString) => <ResultDisplayS>
      {alias}: {valString}
    </ResultDisplayS>
  )
}) ()

const ResultDisplayKeyValueWrapperS = styled.div`
  margin-bottom: 15px;
`

const ResultDisplayKeyValueWrapper = ({ children, }) => <ResultDisplayKeyValueWrapperS>
  {children}
</ResultDisplayKeyValueWrapperS>


const AnalyseOutputSolrQueryExecute = (() => {
  const format = map (
    row => row | toPairs | mapX (
      ([key, value], idx) => <ResultDisplayKeyValue
        key={idx}
        theKey={key}
        value={value}
      />
    )
  )

  return ({ results, }) => lets (
    _ => results | fold (_ => Nothing, id),
    (out) => out | fold (
      ({ results, metadata, }) => results | ifEmptyList (
        _ => 'no results',
        letS ([
          () => metadata.num_hits,
          // eslint-disable-next-line react/display-name
          (results, len) => <div>
            <div className='x__title'>
              Total results: {len}
            </div>
            <div className='x__code'>
            {results | format | mapX ((str, idx) =>
              <ResultDisplayWrapper key={idx}>
                {str}
              </ResultDisplayWrapper>
            )}
            </div>
          </div>,
        ]),
      ),
      '',
    ),
  )
}) ()

const TitleS = styled.div`
  margin: auto;
  margin-bottom: 5%;
  text-align: center;
  font-size: 28px;
  .x__title {
    display: inline-block;
    text-decoration: underline;
  }
`

const Title = _ =>
  <TitleS>
    <div className='x__title'>
      GrNe Analyse
    </div>
    <div>
      Dit is de {process.env.GRNE_ENV} omgeving.
    </div>
  </TitleS>

const AnalyseMainS = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: start;
  // --- cross-axis.
  align-content: flex-start;

  background: white;

  a {
    color: #139;
    text-decoration: none;
    border-bottom: 1px solid #139;
    font-size: 16px;
  }

  padding-left: 20px;
  padding-top: 20px;
  padding-bottom: 20px;

  font-size: 20px;
  border-color: ${prop ('primaryColor')};
  border-top-style: solid;

  position: relative;
  min-height: 100vh;

  vertical-align: top;

  > .x__title {
    flex: 0 0 100%;
    font-size: 22px;
    text-decoration: underline;
    color: ${prop ('primaryColor')};
  }

  .x__swatch {
    width: 8px;
    height: calc(100%);
    top: 0px;
    left: 0px;
    position: absolute;
  }
`

const AnalyseMain = ({ children, primaryColor, title, }) => <AnalyseMainS
  primaryColor={primaryColor}
>
    <div className='x__swatch' style={{
      backgroundColor: primaryColor,
    }}/>

    <div className='x__title'>
      {title}
    </div>
    {children}
</AnalyseMainS>

const InputChange = daggy.taggedSum ('InputChange', {
  InputChangeUpdateSubmit: ['value'],
  InputChangeUpdate: ['value'],
  InputChangeSubmit: [],
})

const { InputChangeUpdateSubmit, InputChangeUpdate, InputChangeSubmit, } = InputChange

const Result = daggy.taggedSum ('Result', {
  ResultNone: [],
  ResultPending: [],
  ResultError: ['error'],
  ResultResult: ['result'],
})

const { ResultNone, ResultPending, ResultError, ResultResult, } = Result

const resultCata = cata ({
  ResultNone: _ => [false, false, Nothing, 'none'],
  ResultPending: _ => [false, true, Nothing, 'pending'],
  ResultError: error => [error, false, Nothing, 'error'],
  ResultResult: res => [false, false, res | Just, 'ok'],
})

const Input = daggy.taggedSum ('Input', {
  InputDummy: [],
  InputTextArea: ['submitOnChange'],
  InputTextLine: ['submitOnChange'],
  InputRadioGroup: ['submitOnChange', 'spec'],
  InputFileUpload: [],
})

const { InputDummy, InputTextArea, InputTextLine, InputRadioGroup, InputFileUpload, } = Input

const InputTextAreaNoSoc = InputTextArea (false)
const InputTextLineSoc = InputTextLine (true)
const InputTextLineNoSoc = InputTextLine (false)
const InputRadioGroupSoc = (...args) => InputRadioGroup (true, ...args)
const InputRadioGroupNoSoc = (...args) => InputRadioGroup (false, ...args)

const inputSubmitOnChange = cata ({
  InputDummy: () => false,
  InputTextArea: (soc) => soc,
  InputTextLine: (soc) => soc,
  InputRadioGroup: (soc, _) => soc,
  InputFileUpload: () => false,
})

// --- note that we will use form data to send these (because one of them is a file) -- this means
// avoid values like `null` which will results in the string 'null'.

const AnalyseXmlToHtml = ({ xslt, }) => <Analyse
  title='Lemma XML to HTML'
  primaryColor='#666644'
  inputSpec={[
    { key: 'lemma-xml', title: 'Lemma XML', defaultValue: '', input: InputTextAreaNoSoc },
    { key: 'lemma-xml-batch', title: 'Lemma XML (Batch mode)', input: InputFileUpload, info: InfoXmlBatch },
    { key: 'xslt', title: 'XSLT', defaultValue: xslt, input: InputTextAreaNoSoc, visible: 'false' },
    { key: 'terms', defaultValue: '', optional: true, title: 'Search Highlighting Terms', input: InputTextLineNoSoc, info: InfoHighlightingTerms },
    { key: 'make_highlight_snippet', defaultValue: 'true', title: 'Make Highlighting Snippet', info: InfoMakeHighlightingSnippet, input: InputRadioGroupNoSoc ([
      { label: 'yes', value: 'true' },
      { label: 'no', value: 'false' },
    ])},
    { key: 'reductie_modus', title: 'Reductiemodus', defaultValue: 'geen', info: InfoReductieModus, input: InputRadioGroupNoSoc ([
      { label: 'geen', value: 'geen' },
      { label: 'schoolreductie standaard', value: 'school-standaard' },
      { label: 'schoolreductie test (auto)', value: 'school-auto' },
      { label: 'schoolreductie test (T1+T2)', value: 'school-t1-t2' },
      { label: 'schoolreductie test (T2)', value: 'school-t2' },
      { label: 'beknopt', value: 'beknopt' },
    ])},
  ]}
  pipelineFunction={pipelineFunctionXmlToHtml}
  outputElement={AnalyseOutputXmlToHtml}
/>

const AnalyseAutocomplete = () => <Analyse
  title='Autocomplete (raw)'
  primaryColor='#444488'
  inputSpec={[
    {
      key: 'strictness', defaultValue: 'loose', title: 'Strictness',
      input: InputRadioGroupSoc ([
        { label: 'strict', value: 'strict' },
        { label: 'loose', value: 'loose' },
      ]),
    },
    {
      key: 'database', defaultValue: 'lemma', title: 'Database',
      input: InputRadioGroupSoc ([
        { label: 'lemma', value: 'lemma' },
        { label: 'pars', value: 'pars' },
      ]),
    },
    { key: 'length', defaultValue: '5', title: 'Length', input: InputTextLineSoc },
    { key: 'limit', defaultValue: '20', title: 'Limit', input: InputTextLineSoc },
    { key: 'query', defaultValue: '', title: 'Query', input: InputTextLineSoc },
  ]}
  pipelineFunction={pipelineFunctionAutocomplete}
  outputElement={AnalyseOutputAutocomplete}
/>

const AnalyseSolrQueryProcess = () => <Analyse
  title='Solr Advanced Query Process'
  primaryColor='#660000'
  inputSpec={[
    {
      key: 'query', defaultValue: '', info: InfoSolrQueryProcessQuery,
      title: 'Query', input: InputTextLineNoSoc,
    },
    {
      key: 'type', defaultValue: 'main',
      title: 'Type', input: InputRadioGroupNoSoc ([
        { label: 'main', value: 'main' },
        { label: 'citaat', value: 'cit' },
        { label: 'either', value: 'any' },
      ]),
    },
  ]}
  pipelineFunction={pipelineFunctionSolrQueryProcess}
  outputElement={AnalyseOutputSolrQueryProcess}
/>

const AnalyseAnalyseChars = () => <Analyse
  title='Analyse Chars'
  primaryColor='#004949'
  inputSpec={[
    { key: 'query', defaultValue: '', title: 'Chars', input: InputTextLineSoc },
  ]}
  pipelineFunction={pipelineFunctionAnalyseChars}
  outputElement={AnalyseOutputAnalyseChars}
/>


const AnalyseAnalyseAndConvertQuery = () => <Analyse
  title='Analyse and Convert Query'
  primaryColor='#442211'
  inputSpec={[
    { key: 'query', defaultValue: '', title: 'Query', input: InputTextLineSoc },
  ]}
  pipelineFunction={pipelineFunctionAnalyseAndConvertQuery}
  outputElement={AnalyseOutputAnalyseAndConvertQuery}
/>

const AnalyseSolrIndexPreProcess = () => <Analyse
  title='Solr Index Pre-Process'
  // info={InfoReductieModus}
  primaryColor='#000044'
  inputSpec={[
    {
      key: 'xml', defaultValue: '', input: InputTextAreaNoSoc,
      title: 'XML',
    },
  ]}
  pipelineFunction={pipelineFunctionSolrIndexPreProcess}
  outputElement={AnalyseOutputSolrIndexPreProcess}
/>

const analyseInputSpecTargetRadioGroup = ({
  defaultValue='acc', submitOnChange=false,
} = {}) => lets (
  _ => submitOnChange ? InputRadioGroupSoc : InputRadioGroupNoSoc,
  Input => ({
    defaultValue,
    key: 'target', title: 'Target',
    input: Input ([
      { label: 'tst', value: 'tst' },
      { label: 'acc', value: 'acc' },
    ]),
  }),
)

const AnalyseSolrIndexAdd = () => <Analyse
  title='Solr Index Add'
  primaryColor='#006611'
  inputSpec={[
    { key: 'xml', defaultValue: '', title: 'XML', input: InputTextAreaNoSoc },
  ]}
  pipelineFunction={pipelineFunctionSolrIndexAdd}
  outputElement={AnalyseOutputSolrIndexAdd}
/>

const AnalyseSolrQueryExecute = () => <Analyse
  title='Solr Query Execute'
  primaryColor='#442244'
  inputSpec={[
    { key: 'query', defaultValue: '', title: 'Query', input: InputTextLineNoSoc },
    { key: 'raw', defaultValue: 'false', info: InfoSolrQueryExecuteRaw,
      title: 'Raw', input: InputRadioGroupNoSoc ([
        { label: 'false', value: 'false' },
        { label: 'true', value: 'true' },
      ])
    },
    { key: 'num-rows', defaultValue: '10', title: 'Number of Rows', input: InputTextLineNoSoc },
    { key: 'start-row', defaultValue: '0', title: 'Start at Row', input: InputTextLineNoSoc },
  ]}
  pipelineFunction={pipelineFunctionSolrQueryExecute}
  outputElement={AnalyseOutputSolrQueryExecute}
/>

const AnalyseLinks = ({ existingLetters=[], }) => <Analyse
  title='Find Broken Links'
  primaryColor='#325512'
  inputSpec={[
    { key: 'dummy', input: InputDummy, },
    {
      key: 'only-existing-letters',
      defaultValue: 'true',
      title: 'Only show existing letters',
      info: existingLetters | InfoAnalyseLinks,
      input: InputRadioGroupNoSoc ([
        { label: 'yes', value: 'true' },
        { label: 'no', value: 'false' },
      ]),
    },
  ]}
  pipelineFunction={pipelineFunctionLinks}
  outputElement={AnalyseOutputLinks}
/>

const InputDataEntry = daggy.tagged ('InputDataEntry', ['key', 'value', 'optional'])
const inputDataToTable = mapValues (prop ('value'))

const inputDataHasAnyMissingValue = values >> find (
  againstAll ([
    prop ('optional') >> not,
    prop ('value') >> trim >> eq (''),
  ])
)

const inputSpecToInput = map (
  ({ key, defaultValue='', optional=false, input, }) => lets (
    () => optional || (input | isInputDummy),
    (isOptional) => [key, InputDataEntry (key, defaultValue, isOptional)]
  ),
) >> fromPairs

// --- each section is an <Analyse>.

class Analyse extends PureComponent {
  state = {
    result: ResultNone,
  }

  inputData = this.props.inputSpec | inputSpecToInput
  pipelineFunction = this.props.pipelineFunction
  outputElement = this.props.outputElement

  onCallReturn = this | setStateValueMeth ('result')

  // --- alters `inputData`.
  onInput = (inputKey, inputVal) => this | deconstruct (
    ({ inputData, submit, }) => {
      const update = val => inputData [inputKey].value = val
      inputVal | cata ({
        InputChangeSubmit: submit,
        InputChangeUpdate: update,
        InputChangeUpdateSubmit: update >> submit,
      })
    }
  )

  submit = () => this | deconstruct (
    ({ inputData, inputSpec, pipelineFunction, onCallReturn, }) => {
      /*
      if (inputData | inputDataHasAnyMissingValue)
        return this | setState ({
          result: ResultNone,
        })
        */

      this | setState ({
        result: ResultPending,
      })

      pipelineFunction (
        inputData | inputDataToTable,
        (...args) => onCallReturn (...args),
      )
    },
  )

  render = () => this | compMeth ((
    { outputElement: OutputElement, info: AnalyseInfo, primaryColor='white', inputSpec, title, },
    { result, },
    { inputData, },
  ) => {
    const changed = which => (...args) => this.onInput (which, ...args)
    const [error, callPending, results, status] = result | resultCata

    const spec = inputSpec | map (({ big, title, key, input, info, visible=true, }) => ({
      title,
      input,
      visible,
      info,
      submitOnChange: input | inputSubmitOnChange,
      defaultValue: inputData [key].value || '',
      onChange: key | changed,
    }))

    const showSubmitButton = spec | find (prop ('submitOnChange') >> ne (true)) | ok

    const onSubmit = _ => lets (
      _ => spec | headMaybe | fold ('onChange' | prop, noop),
      onChange0 => InputChangeSubmit | onChange0,
    )

    // --- error can be a list or a string (though we're not currently formatting the list in any
    // way)
    const haveError = error && isNotEmptyList (error)

    const extraOutputProps = {}

    return <AnalyseMain
      primaryColor={primaryColor}
      title={title}
    >
      <Status status={status} error={error}/>
      <AnalyseInput
        spec={spec}
        showSubmitButton={showSubmitButton}
        onSubmit={onSubmit}
      />
      <AnalyseOutputS>
        <OutputElement
          results={haveError ? error | Left : results | Right}
          inputData={inputData | inputDataToTable}
          extraProps={extraOutputProps}
        />
      </AnalyseOutputS>
    </AnalyseMain>
  })
}

// --- used for data which is fetched when the component is mounted.
const ApiData = daggy.taggedSum ('ApiData', {
  ApiDataResults: ['lemmaToHtmlXslt', 'existingLetters', 'batchData'],
  ApiDataNone: [],
})

const { ApiDataResults, ApiDataNone, } = ApiData

// --- test flex
const TestS = styled.div`
  border: 1px solid #999999;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  .x__left, .x__right {
  }
  .x__left {
    background: #ddd;
    // --- grow, shrink, basis
    flex: 0 1 auto;
    width: 500px;
  }
  .x__right {
    background: #ccc;
    flex: 0 1 auto;
    width: 620px;
  }
`

const Test = _ => <TestS>
  <div className='x__left'>
    left
  </div>
  <div className='x__right'>
    right
  </div>
</TestS>

const SpinnerWrapperS = styled.div`
  position: relative;
  top: 10%;
  left: 20%;
`

export class Main extends PureComponent {
  state = {
    apiData: ApiDataNone,
  }

  componentDidMount = () => this.fetch ()

  fetch = () => this | compMeth (
    ({ batchId, }) => lets (
      () => ({
        ...defaultFetchOpts,
        method: 'GET',
      }),
      (options) => batchId | ifOk (
        () => () => requestJSON ('/api/analyse/xml-to-html-cached/' + batchId, options),
        () => () => null,
      ),
      (options, fetchXmlHtmlBatch) => allP (compact ([
        requestJSON ('/api/analyse/xml-to-html/xslt', options),
        requestJSON ('/api/analyse/determine-existing-letters', options),
        fetchXmlHtmlBatch (),
      ]))
      | then (
        map (resultFold (
          // --- ok
          path (['data', 'results']),
          // --- user error
          (umsg) => umsg | die,
          // --- imsg maybe
          (imsgMb) => {
            imsgMb | foldJust (iwarn)
            die ()
          },
        )) >> deconstruct (
          ([xslt, letters, batchData=null]) => this | setState ({
            apiData: ApiDataResults (xslt, letters, batchData),
          }),
        ),
      )
      | recover ((err) => alertError (err.message || true))
    ),
  )

  render () {
    const { state, props, } = this
    const { apiData, } = state
    const { batchId, } = props

    const isBatch = batchId | ok

    return apiData | cata ({
      ApiDataNone: _ => <SpinnerWrapperS>
        <SpinnerComet size={30} spinning={true}/>
      </SpinnerWrapperS>,
      ApiDataResults: (lemmaToHtmlXslt, existingLetters, batchData) => <TopS>
        {isBatch | ifYes (
          () => <AnalyseOutputXmlToHtml
            resultsCached={batchData}
          />,
          () => <Fragment>
            <Title/>
            <AnalyseXmlToHtml
              xslt={lemmaToHtmlXslt}
            />
            <AnalyseAnalyseChars/>
            <AnalyseAnalyseAndConvertQuery/>
            <AnalyseAutocomplete/>
            <AnalyseSolrIndexPreProcess/>
            <AnalyseSolrIndexAdd/>
            <AnalyseSolrQueryProcess/>
            <AnalyseSolrQueryExecute/>
            <AnalyseLinks existingLetters={existingLetters}/>
          </Fragment>,
        )}
      </TopS>,
    })
  }
}

Main.propTypes = {
}

const mapStateToProps = createStructuredSelector ({
})

const mapDispatchToProps = (dispatch) => ({
})

export default Main
  | connect       (mapStateToProps, mapDispatchToProps)

const IconS = styled.div`
  display: inline-block;
  cursor: pointer;
  margin-left: 5px;
  position: relative;
  top: -2px;
`

const InfoIconS = (props) => <IconS>
  <InfoIcon {...props} />
</IconS>

const CloseIconS = (props) => <IconS>
  <CloseIcon {...props} />
</IconS>

const InfoInfoS = styled.div`
  font-size: 17px;
  .x__row {
  }
  .x__heading {
    text-decoration: underline;
  }
  .x__desc, .x__desc_extended {
    padding-left: 10px;
  }
  .x__desc, .x__heading {
    display: inline-block;
  }
`

const InfoReductieModus = _ => <InfoInfo
  spec={[
    ['geen', 'geen reductie.', []],
    ['schoolreductie standaard', 'schoolreductie zoals op de site.', [
      'status-t1 = “gecontroleerd” of “uitgevoerd”: voer T2 uit.',
      'anders: resultaat = leeg.',
    ]],
    ['schoolreductie test (auto)', '', [
      'status-t1 = “gecontroleerd” of “uitgevoerd”: voer T2 uit.',
      'anders: voer T1+T2 uit.',
    ]],
    ['schoolreductie test (T1+T2)', '', ['negeer status-t1; voer T1+T2 uit.']],
    ['schoolreductie test (T2)', '', ['negeer status-t1; voer T2 uit.']],
    ['beknopt', '', ['negeer status-t1; haal alle citaten weg, pas punctuatie aan.']],
  ]}
/>

const InfoXmlBatch = _ => <InfoInfo
  spec={[
    ['', '', [
      '• upload a .zip file containing up to around 200 .xml files (any folder structure is fine).',
      '• also use the “Search Highlighting Terms”, “Make Highlighting Snippet” and “Reductiemodus” fields if you wish.',
      '• if using reduction, you should probably reduce the number of files in the .zip file.',
      '• click “submit”.',
      '• to make a .pdf: choose “show printable version” and use the print dialog of the browser (e.g. Ctrl-P, then “print to file” or “print as PDF”).',
      '• generating .docx files can take a few minutes.',
      // '• if you see “Pagina niet gevonden” please try Ctrl-Shift-R / Command-Shift-R.',
      // '• the 3rd party “grabzit” service (grabz.it) generates better results than libreoffice, but requires a paid subscription.',
    ]],
  ]}
/>

const InfoMakeHighlightingSnippet = _ => <InfoInfo
  spec={[
    ['yes', 'abridge snippets (like on the advanced search results page)', []],
    ['no', 'show the whole lemma (like in the browseview)', []],
    ['', '', ['(either way, this only has an effect if “Search Highlighting Terms” is not empty)']],
  ]}
/>

const InfoHighlightingTerms = _ => <InfoInfo
  spec={[
    ['for most search terms', '', [
      '• use case-sensitive strings, without punctuation, e.g.: Aristot',
      '• don\'t use double quotes, even for phrases.',
      '• don\'t use wildcards.',
      '• separate terms with a vertical bar symbol (“|”).',
    ]],
    ['for verwijzingen', '', [
      '• use case-sensitive strings, with punctuation and spaces, e.g.:',
      '  Plat. Euthyph. 3b',
      '• “plaats” is optional: e.g.: Aristoph. Pax',
    ]],
    ['example', 'Aristot | van voedsel | Plat. Euthyph. 3b | Aristoph. Pax', []],
  ]}
/>

const InfoSolrQueryProcessQuery = () => <InfoInfo
  spec={[
    ['notes', '', [
      '• each term must be surrounded by double quotes.',
      '• to search for a phrase, use two sets of double quotes, preceding the inner ones with a backslash: \\" (see "van voedsel" in the examples)',
      '• the app implicitly surrounds all entered text with double quotes, e.g. "\\"honing\\"".',
      '• you generally want to set “type” to “main” (this is what the app uses).',
      '• you can use the optional % clause to set the field which is searched on — the app always sets this to “_text_” (i.e. fulltext).',
      '• you can use the optional # clause to set the “at-least-one field” — this is how the “vertM”/“hoofdW” buttons in the app work.',
      '• ∀ is a synonym for % and ∃ is a synonym for #.',
    ]],
    ['examples', '', [
      '"honing"',
      '"honing" and "voedsel"',
      '"honing" and "geu/w"',
      '"honing" and "thuc" or "γευω"',
      '"honing" or "voed???"',
      '"honing" or "voedsel" not "tro*"',
      '"honing" and "\\"van voedsel\\""',
      '"honing" and "voedsel" # "vertM"',
      '"honing" and "voedsel" % "_text_" # "vertM"',
      '"honing" and "voedsel" ∀ "_text_" ∃ "vertM"',
    ]],
  ]}
/>

const InfoSolrQueryExecuteRaw = () => <InfoInfo
  spec={[
    ['true', '', [
        'get the raw results as returned by solr. Set this to true if you added test lemmata manually using “Solr Index Add” above.',
      ],
    ],
    ['false', '', [
        'get the search highlighting terms and the (cleaned-up) hoofdwoord; results must not include any “cit” documents.',
      ],
    ],
  ]}
/>

const InfoAnalyseLinks = (existingLetters) => withDisplayName ('InfoAnalyseLinks') (() => <InfoInfo
  spec={[
    [
      'yes',
      'skip links which begin with one of:',
      existingLetters,
    ],
  ]}
/>)

const InfoInfo = ({ spec, }) => <InfoInfoS>
  {spec | mapX (([l='', r, ext], idx) => <div key={idx} className='x__row'>
    {lets (
      () => clss (
        l | whenEmptyString (() => 'u--display-none'),
      ),
      (cls) => <div className={cls}>
        <div className='x__heading'>
          {l}:
        </div>
        <div className='x__desc'>
          {r}
        </div>
      </div>,
    )}
    <div className='x__desc_extended'>
      {ext | mapX ((x, idx) => <div key={idx}>{x}</div>)}
    </div>
  </div>
  )}
</InfoInfoS>
