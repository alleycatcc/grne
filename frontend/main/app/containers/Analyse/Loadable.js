import {
  pipe, compose, composeRight,
  lets, ifTrue,
} from 'stick-js/es'

import React from 'react'

import { LoadableLoading, } from '../../alleycat-components/index'

import { envIsPrd, } from '../../env'
import loadable from '../../utils/loadable'

/* Conditionally load the Analyse component based on environment. Note that it still does get
 * packaged into the bundle by webpack.
 */

export default lets (
  () => envIsPrd | ifTrue (
    _ => import ('../NotFoundPage'),
    _ => import ('./index'),
  ),
  () => <LoadableLoading/>,
  (promise, fallback) => loadable (
    () => promise, { fallback },
  ),
)
