import {
  pipe, compose, composeRight,
  divideBy, ok,
  merge,
  map, mergeM, appendM, reduce, add, lets,
  concatTo, tap, minus,
} from 'stick-js/es'

import { all, call, put, select, takeLatest, take, } from 'redux-saga/effects'

import {
  iwarn, ierror, tellIf, defaultToV, roundUp,
} from 'alleycat-js/es/general'

import { toJust, } from 'alleycat-js/es/bilby'
import { requestJSON, defaultOpts, resultFold, } from 'alleycat-js/es/fetch'
import { errorError, error as alertError, } from 'alleycat-js/es/react-s-alert'

import {
  ADVANCED_SEARCH_SUBMIT_CLICKED,
  ADVANCED_SEARCH_SET_NUM_RESULTS_CLICKED,
  ADVANCED_SEARCH_SET_PAGE_NUM_CLICKED,
  ADVANCED_SEARCH_SET_FIELD_CLICKED,
  ADVANCED_SEARCH_FETCH_PREVIEWS,
  ADVANCED_SEARCH_FETCH_RESULTS_SUCCESS,
  ADVANCED_SEARCH_RESULT_CLICKED,
  ADVANCED_SEARCH_BOTTOM_REACHED,
} from '../../containers/App/constants'

import {
  advancedSearchFetchResults,
  advancedSearchLoadMoreResults,
  advancedSearchNumResultsSet,
  advancedSearchSetQuery,
  advancedSearchSetQBuilder,
  advancedSearchSetPageNum,
  advancedSearchResetPageNum,
  advancedSearchFetchPreviews,
  advancedSearchFetchPreviewsError,
  advancedSearchFetchPreviewsSuccess,
  advancedSearchSelectResult,
  advancedSearchSetField,
  advancedSearchIncreasePageNum,
} from '../App/actions'

import {
  makeSelectAdvancedSearchPageNum,
  makeSelectAdvancedSearchMaxPage,
  makeSelectAdvancedSearchResults,
  makeSelectAdvancedSearchSelectedResult,
  makeSelectAdvancedSearchGetResultForIdx,
  makeSelectAdvancedSearchNumHits,
  makeSelectAdvancedSearchNumResultsSetting,
} from '../../slices/domain/selectors'

const linkToMainId = 1 | add

function *generalAlert () {
  true | alertError
}

function *sagaAdvancedSearchSubmitClicked ({ data: { singlePageMode, qBuilder, query, }}) {
  if (!query || !qBuilder) return
  yield qBuilder | advancedSearchSetQBuilder | put
  yield (singlePageMode ? -1 : 0) | advancedSearchSetPageNum | put
  yield query | advancedSearchSetQuery | put
  yield advancedSearchFetchResults (singlePageMode) | put
}

function *sagaAdvancedSearchNumResultsChangeClicked ({ data: num, }) {
  yield num | advancedSearchNumResultsSet | put
  const curPage = yield makeSelectAdvancedSearchPageNum () | select
  const maxPageMb = yield makeSelectAdvancedSearchMaxPage () | select
  const maxPage = maxPageMb | toJust
  if (!ok (maxPage)) return ierror ('invalid maxPage')
  if (curPage > maxPage)
    yield maxPage | advancedSearchSetPageNum | put
  yield advancedSearchFetchResults (false) | put
}

function *sagaAdvancedSearchSetPageNumClicked ({ data: pageNum, }) {
  yield pageNum | advancedSearchSetPageNum | put
  yield advancedSearchFetchResults (false) | put
}

function *sagaAdvancedSearchSetFieldClicked ({ data: { singlePageMode, field, }}) {
  if (singlePageMode)
    yield advancedSearchResetPageNum () | put
  yield field | advancedSearchSetField | put
  yield advancedSearchFetchResults (singlePageMode) | put
}

function *sagaAdvancedSearchFetchPreviews ({ data: { ids, highlighting, }}) {
  const url = '/api/lemma/by-id'
  const opts = defaultOpts | merge ({
    method: 'POST',
    body: JSON.stringify ({
      data: {
        ids,
        highlighting,
        do_school: false,
        do_post_processing: false,
      },
    })
  })
  const results = yield call (requestJSON, url, opts)
  yield results | resultFold (
    ({ results: html, }) => html | advancedSearchFetchPreviewsSuccess | put,
    (umsg) => umsg | advancedSearchFetchPreviewsError | put,
    (imsgMb) => null
      | tap (errorError)
      | tap (
        () => imsgMb | map (concatTo ('Error fetching previews: ') >> iwarn),
      )
      | advancedSearchFetchPreviewsError
      | put,
  )
}

// @todo move to App/saga?
function *sagaAdvancedSearchFetchResultsSuccess ({ data: { data: { results, }}}) {
  // @todo transform
  const reducer = (
    { ids, highlighting, },
    { 'link-id': linkId, 'search-highlight': terms, },
  ) => lets (
    () => linkId | linkToMainId,
    (mainId) => ({
      ids: ids | appendM (mainId),
      highlighting: highlighting | mergeM ({
        [mainId]: {
          terms,
          abridge: true,
        },
      })
    }),
  )

  const params = results | reduce (reducer) ({ ids: [], highlighting: {}, })
  yield params | advancedSearchFetchPreviews | put
}

function *sagaAdvancedSearchResultClicked ({ data: { resultIdx, history, }}) {
  const results = yield makeSelectAdvancedSearchGetResultForIdx () | select
  const { linkId, searchHighlight, } = resultIdx | results
  yield advancedSearchSelectResult (history, linkId, searchHighlight) | put
}

function *sagaAdvancedSearchBottomReached () {
  const curPage = yield makeSelectAdvancedSearchPageNum () | select
  const maxPageMb = yield makeSelectAdvancedSearchMaxPage () | select
  const maxPage = maxPageMb | toJust
  if (curPage === maxPage) return
  yield advancedSearchLoadMoreResults() | put
}

export default function *defaultSaga () {
  yield all ([
    takeLatest (ADVANCED_SEARCH_SUBMIT_CLICKED, sagaAdvancedSearchSubmitClicked),
    takeLatest (ADVANCED_SEARCH_SET_NUM_RESULTS_CLICKED, sagaAdvancedSearchNumResultsChangeClicked),
    takeLatest (ADVANCED_SEARCH_SET_PAGE_NUM_CLICKED, sagaAdvancedSearchSetPageNumClicked),
    takeLatest (ADVANCED_SEARCH_SET_FIELD_CLICKED, sagaAdvancedSearchSetFieldClicked),
    takeLatest (ADVANCED_SEARCH_FETCH_PREVIEWS, sagaAdvancedSearchFetchPreviews),
    takeLatest (ADVANCED_SEARCH_FETCH_RESULTS_SUCCESS, sagaAdvancedSearchFetchResultsSuccess),
    takeLatest (ADVANCED_SEARCH_RESULT_CLICKED, sagaAdvancedSearchResultClicked),
    takeLatest (ADVANCED_SEARCH_BOTTOM_REACHED, sagaAdvancedSearchBottomReached),
  ])
}
