import {
  pipe, compose, composeRight,
  mergeM, bindProp, path,
  T, F, prop, map, id, concat, concatM, appendM,
  update, updateM, updatePathM,
  tap, cond, always, guardV, otherwise, guard,
  letS, not, side, side1, side2, dot, side3,
  condS, ne, reduce, whenPredicate, ifPredicate,
  append, join, ok, whenTrue, whenFalse, sprintf1,
  assocM, reject, containsV, eq, lets, timesF,
  whenOk, againstBoth, noop, ifOk, dot2,
  find, notOk,
  invoke,
  lt,
  deconstruct,
} from 'stick-js/es'

import React, { Component, PureComponent, } from 'react'

import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import daggy from 'daggy'
import memoize from 'memoize-immutable'
import styled from 'styled-components'

import { Nothing, Just, cata, toJust, foldJust, fold, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { keyPressListen, inputValue, } from 'alleycat-js/es/dom'
import {
  length, logWith as _logWith, mapX, isWhiteSpaceOrEmpty, isEmptyString, trim,
  pop, zipWithAll, zip,
} from 'alleycat-js/es/general'
import { allV, ifTrueV, ifFalseV, whenNotTrueV, ifEmptyList, isNotEmptyString, } from 'alleycat-js/es/predicate'
import { createRef, setState, compMeth, whyUpdate, incrementStateValue, } from 'alleycat-js/es/react-legacy'
import { ElemP, } from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import {
  advancedSearchClearResults,
  advancedSearchSubmitClicked,
  advancedSearchNumResultsChangeClicked,
  advancedSearchSetPageNumClicked,
  advancedSearchResultClicked,
  advancedSearchBottomReached,
  advancedSearchSetFieldClicked,
} from '../App/actions'

import {
  makeSelectAdvancedSearchResults,
  makeSelectAdvancedSearchNumResultsSetting,
  makeSelectAdvancedSearchPageNum,
  makeSelectAdvancedSearchNumHits,
  makeSelectAdvancedSearchMaxPage,
  makeSelectAdvancedSearchQBuilder,
  makeSelectAdvancedSearchFetchingPreviews,
  makeSelectAdvancedSearchField,
} from '../../slices/domain/selectors'

import { spinner, } from '../../alleycat-components'
import AdvancedSearchResults from '../../components/AdvancedSearchResults'
import BooleanGrid from '../../components/BooleanGrid'
import SearchIconSimple from '../../components/svg/SearchIconSimple'
import { Input as BasicInput, H1, H1M, Button, SectionNLM, } from '../../components/shared'

import {
  mediaPhone, mediaTablet, mediaDesktop,
  shouldDisableMomentumScroll,
} from '../../common'

import { envIsPrdLike, } from '../../env'

import config from '../../config'
import { advSearchIsLoadingMore, advSearchLoadingFirst, advSearchIsLoadingAny, } from '../../types'
import injectSaga from '../../utils/injectSaga'
import injectReducer from '../../utils/injectReducer'

import reducer from './reducer'
import saga from './saga'

const configTop = config | configure.init

const debugRender = 'debug.rerender' | configTop.get

const logWith = envIsPrdLike | ifFalseV (_logWith, noop | always)

const replaceFromEndM = n => repl => xs => xs | updateM (
  xs.length - n - 1,
  repl | always,
)
const replaceLastM = 0 | replaceFromEndM
const replaceSecondLastM = 1 | replaceFromEndM

const distanceTo = x => y => Math.abs (x - y)
const scrolledCloseTo = x => y => x | distanceTo (y) | lt (5)

const Spinner = 'textblocks' | spinner

const placeholder = 'placeholder.advanced' | configTop.get

const keyState = ({
  RadioXS: 0,
})

const ScrollWrapperS = styled.div`
  // margin-left: calc(100vw - 100%);
  // margin-left: -10px;
`

const WrapperS = styled.div`
  height: 100%;
  width: 100%;
  margin-left: auto;
  margin-right: auto;
  // --- necessary to avoid left/right bouncy effect on iPhone.
  overflow-x: hidden;
  overflow-y: scroll;
  ${prop ('shouldDisableMomentumScroll') >> ifFalseV (
    '-webkit-overflow-scrolling: touch;',
    '',
  )}
`

const WrapperTallS = styled.div`
`

const QueryWrapperS = styled.div`
  ${mediaQuery (
    mediaPhone ('width: 100%;'),
    mediaTablet ('width: 62%;'),
    mediaDesktop ('width: 57%;'),
  )}
  margin: auto;
`

const SearchS = styled.div`
  white-space: nowrap;
  ${mediaQuery (
    mediaPhone (`
      position: relative;
      margin-bottom: 24px;
      `),
    mediaTablet (`
      position: unset;
      margin-bottom: 0px;
      margin-left: -137px;
      margin-top: 25px;
      `),
    mediaDesktop (`
      position: unset;
      margin-bottom: 0px;
      margin-left: -137px;
      margin-top: 25px;
      `),
  )}
`

const TextInputS = styled.div`
  margin-top: -6px;
  ${mediaQuery (
    mediaPhone (`
      margin-top: 0px;
      width: 100%;
    `),
    mediaTablet (`
      margin-top: -6px;
    `),
    mediaDesktop (`
      margin-top: -6px;
    `),
  )}
  display: inline-block;
  vertical-align: top;
  input {
    width: 100%;
  }
`

class TextInput extends PureComponent {
  render () {
    const { props, } = this
    const { theRef, onChange, onKeyPress, defaultValue, inputProps={}, } = props
    return <TextInputS>
      <BasicInput
        type='text'
        autoComplete='off'
        autoCorrect='off'
        autoCapitalize='off'
        spellCheck='false'
        onChange={onChange}
        onKeyPress={onKeyPress}
        defaultValue={defaultValue}
        theRef={theRef}
        {...inputProps}
      />
    </TextInputS>
  }
}

const ResultsWrapperS = styled.div`
  ${mediaQuery (
    mediaPhone ('margin-top: 9%'),
    mediaTablet ('margin-top: 0'),
  )};
`

const SpinnerS = styled.div`
  display: inline-block;
  position: relative;
  ${mediaQuery (
    mediaPhone ('top: 22px'),
    mediaTablet ('top: 2px'),
  )};
  margin-left: 10px;
  margin-right: 10px;
  opacity: ${prop ('visible') >> ifTrueV ('1', '0')};
`

const SearchRowS = styled.div`
  ${mediaQuery (
    mediaPhone ('height: 72px;'),
    mediaTablet ('height: 50px;'),
  )}
`

const SearchRowsS = styled.div`
  ${mediaQuery (
    mediaPhone (`
      margin-top: -8px;
      width: 58%;
      max-width: 210px;
      `),
    mediaTablet (`
      margin-top: 0px;
      width: 300px;
      `),
  )}
  display: inline-block;
  vertical-align: top;
  z-index: 2;
  position: relative;
  background: white;
`

const SubmitS = styled.div`
  ${mediaQuery (
    mediaPhone (`
      display: block;
      z-index: 2;
      position: absolute;
      left: 305px;
      @media (max-width: 400px) {
         left: 80%;
      }
      top: -21px;
      `),
    mediaTablet (`
      display: inline-block;
      margin-left: 34px;
      vertical-align: top;
      z-index: 0;
      position: unset;
      left: 0px;
      bottom: 0px;
      `),
  )}
  display: inline-block;
  vertical-align: top;
  .x--phone {
    ${mediaQuery (
      mediaPhone (`
        display: inline-block;
        position: relative;
        top: 10px;
        border: 1px solid #999;
        width: 35px;
        height: 35px;
      `),
      mediaTablet (`display: none`),
    )}
  }
  .x--not-phone {
    ${mediaQuery (
      mediaPhone (`display: none;`),
      mediaTablet (`
        display: inline-block;
        position: relative;
        top: -4px;
        margin-left: 94px;
        width: 115px;
      `),
    )}
    width: 100%;
    > div {
      display: flex;
      justify-content: space-evenly;
    }
    button {
    }
  }
`

const BooleanGridWrapperS = styled.div`
  ${mediaQuery (
    mediaPhone (`
      width: 50px;
      margin-left: 4%;
      margin-right: 0px;
    `),
    mediaTablet ('width: 100px;'),
  )}
  display: inline-block;
  height: ${prop ('height')}px;
  top: 8px;
  position: relative;
  z-index: 1;
  margin-right: -5px;
`

const serialiseBool = cata ({
  BooleanBooleanOr:  0 | always,
  BooleanBooleanAnd: 1 | always,
  BooleanBooleanNot: 2 | always,
})

const serialiseBoolString = cata ({
  BooleanBooleanOr:  'or' | always,
  BooleanBooleanAnd: 'and' | always,
  BooleanBooleanNot: 'not' | always,
})

const serialiseBooleansReducer = (acc, bool) => acc | (bool | cata ({
  BooleanRowAdd: _ => id,
  BooleanRowAddRemove: _ => id,
  BooleanRowAndOr: type => concat (type | serialiseBool),
  BooleanRowNot: _ => concat (BooleanBooleanNot | serialiseBool),
}))

// --- make input for grid calculator.
const serialiseBooleans = map (prop ('boolean')) >> reduce (serialiseBooleansReducer) ([])

const BooleanBoolean = daggy.taggedSum ('BooleanBoolean', {
  BooleanBooleanAnd: [],
  BooleanBooleanOr: [],
  BooleanBooleanNot: [],
})

const { BooleanBooleanAnd, BooleanBooleanOr, BooleanBooleanNot, } = BooleanBoolean

const BooleanRow = daggy.taggedSum ('BooleanRow', {
  BooleanRowAdd: [],
  BooleanRowAddRemove: [],
  BooleanRowAndOr: ['bool'],
  BooleanRowNot: ['first'],
})

const booleanRowToBoolean = cata ({
  BooleanRowAndOr: bool => bool | Just,
  BooleanRowNot: always (BooleanBooleanNot) >> Just,
  BooleanRowAdd: Nothing | always,
  BooleanRowAddRemove: Nothing | always,
})

const booleanRowToBooleanPrimitive = booleanRowToBoolean >> fold (
  serialiseBoolString,
  null,
)

const isRowNot = cata ({
  BooleanRowAdd: F,
  BooleanRowAddRemove: F,
  BooleanRowAndOr: F,
  BooleanRowNot: T,
})

export const { BooleanRowAdd, BooleanRowAddRemove, BooleanRowAndOr, BooleanRowNot, } = BooleanRow

const QueryBuilderUpdateEvent = daggy.taggedSum (
  'QueryBuilderUpdateEvent', {
    QueryBuilderUpdateEventAdd: [],
    QueryBuilderUpdateEventRemove: [],
    QueryBuilderUpdateEventBool: ['bool'],
  }
)

const {
  QueryBuilderUpdateEventAdd,
  QueryBuilderUpdateEventRemove,
  QueryBuilderUpdateEventBool,
} = QueryBuilderUpdateEvent

const SearchControlsS = styled.div`
  ${mediaQuery (
    mediaPhone (`
      display: block;
      margin-top: 8px;
      margin-left: 22px;
    `),
    mediaTablet (`
      display: inline-block;
      margin-left: 10px;
      margin-top: 3px;
    `),
    mediaDesktop ('display: inline-block;'),
  )}
`

const AddRemoveButtonS = styled.div`
  position: relative;
  ${mediaQuery (
    mediaPhone (`
      top: 5px;
      margin-left: 10px;
      color: #444;
    `),
    mediaTablet (`
      top: -9px;
      margin-left: 0px;
      color: #999999;
    `),
    mediaDesktop (`
      top: -9px;
      margin-left: 0px;
    `),
  )}
  font-size: 16px;
  height: 35px;
  width: 35px;
  display: inline-block;
  margin-right: 6px;
  cursor: pointer;
  border: 1px solid #999999;
  padding: 4px;
  text-align: center;
  vertical-align: middle;
`

const buttonSpec = {
  add: ['+', '0', '-3px', '20px'],
  remove: ['-', '0', '-8px', '25px'],
  reset: ['∅', '0', '0px', '14px'],
}

const AddRemoveButton = ({ onChange, type, }) => lets (
  () => buttonSpec [type],
  ([txt, left, top, fontSize]) => <AddRemoveButtonS
    onClick={_ => type | onChange}
  >
    <div style={{
      top, left, fontSize,
      display: 'inline-block',
      position: 'relative',
    }}>
      {txt}
    </div>
  </AddRemoveButtonS>,
)

const BooleanRowAndOrComponentS = styled.div`
  .x__control {
    width: 30px;
    display: inline-block;
    ${mediaQuery (
      mediaPhone (`
        margin-left: 15px;
      `),
      mediaTablet (`
        margin-left: 5px;
      `),
    )};
    opacity: 1;
    margin-top: 1px;
    position: relative;
    &--disabled {
      opacity: 0;
    }
  }
  .x__text {
    ${mediaQuery (
      mediaPhone ('padding-left: 5px;'),
      mediaTablet ('padding-left: 2px'),
    )}
    color: black;
    padding-left: 5px;
    position: absolute;
    bottom: 2px;
    left: 15px;
    display: inline-block;
  }
`

const RadioXS = styled.div`
`

const BooleanRowBoolComponent = ({ onChange, bool, enabled, }) => <BooleanRowAndOrComponentS>
  {lets (
    _ => enabled | ifTrueV ('x__control', 'x__control x__control--disabled'),
    _ => bool | cata ({
      BooleanBooleanAnd: _ => [true, false, false],
      BooleanBooleanOr: _ => [false, true, false],
      BooleanBooleanNot: _ => [false, false, true],
    }),
    (className, [isAnd, isOr, isNot]) => <RadioXS key={++keyState.RadioXS}>
      <form>
        <div className={className}>
          <input type='radio' name='dummy' disabled={enabled | not} defaultChecked={isAnd} onChange={_ => BooleanBooleanAnd | onChange}/>
          <div className='x__text'>
            en
          </div>
        </div>
        <div className={className}>
          <input type='radio' name='dummy' disabled={enabled | not} defaultChecked={isOr} onChange={_ => BooleanBooleanOr | onChange}/>
          <div className='x__text'>
            of
          </div>
        </div>
        <div className='x__control'>
          <input type='radio' name='dummy' disabled={false /*enabled | not*/} defaultChecked={isNot} onChange={_ => BooleanBooleanNot | onChange}/>
          <div className='x__text'>
            niet
          </div>
        </div>
      </form>
    </RadioXS>
  )}
</BooleanRowAndOrComponentS>

const SearchControls = ({ onChange, rowNum, rowState, reset, }) => <SearchControlsS>
  {
    /* eslint-disable react/display-name */
    rowState | cata ({
      BooleanRowAdd: _ => <AddRemoveButton type='add'
        onChange={always (QueryBuilderUpdateEventAdd) >> onChange (rowNum)}
      />,
      BooleanRowAddRemove: _ => <div>
        <AddRemoveButton type='add'
          onChange={always (QueryBuilderUpdateEventAdd) >> onChange (rowNum)}
        />
        <AddRemoveButton type='remove'
          onChange={always (QueryBuilderUpdateEventRemove) >> onChange (rowNum)}
        />
        <AddRemoveButton type='reset'
          onChange={reset}
        />
      </div>,
      BooleanRowAndOr: (bool) => <div>
        <BooleanRowBoolComponent
          onChange={QueryBuilderUpdateEventBool >> onChange (rowNum)}
          bool={bool}
          enabled={true}
        />
      </div>,
      BooleanRowNot: (first) => <div>
        <BooleanRowBoolComponent
          onChange={QueryBuilderUpdateEventBool >> onChange (rowNum)}
          bool={BooleanBooleanNot}
          enabled={first}
        />
      </div>,
    })
    /* eslint-enable react/display-name */
  }
</SearchControlsS>

const queryBuilder = {
  init () {
    return this | mergeM ({
      booleans: [this.makeRow (true)],
      texts: [''],
    })
  },

  clear () {
    return this.init ()
  },

  updateBoolean (rowNum, event) {
    return event | cata ({
      QueryBuilderUpdateEventAdd: _ => this.addRow (),
      QueryBuilderUpdateEventRemove: _ => this.removeRow (),
      QueryBuilderUpdateEventBool: cata ({
        BooleanBooleanAnd: _ => this.toAnd (rowNum),
        BooleanBooleanOr: _ => this.toOr (rowNum),
        BooleanBooleanNot: _ => this.toNot (rowNum),
      }),
    })
  },

  updateText (rowNum, value) {
    return this | updateM (
      'texts',
      assocM (rowNum, value | trim),
    )
  },

  hasText () {
    const { texts, } = this
    return texts | find (isNotEmptyString)
  },

  queryOk () {
    const { texts, } = this
    return texts | containsV ('') | not
  },

  generateQuery () {
    const { texts, booleans, } = this
    const connectorToString = booleanRowToBoolean >> toJust >> serialiseBoolString >> sprintf1 (' %s ')
    if (! this.queryOk ()) return Nothing

    const encode = JSON.stringify
    // @todo doesn't reject mess up alignment?
    const [first, ...ts] = texts | reject (isEmptyString)

    /*
    The agreed-upon rule with Leiden is that all fields are implicitly surrounded by double quotes.

    Examples:
       van vader -> $ "\"van vader\""
       vader -> $ "\"van\""
    */

    const doubleQuote = sprintf1 ('"%s"')
    const removeDoubleQuotes = dot2 ('replace') (/"/g, '')
    const prepareField = removeDoubleQuotes >> encode >> encode
    const reducer = (acc, [t, b]) => [acc, t | prepareField] | join (b | connectorToString)
    return texts | ifEmptyList (
      _ => Nothing,
      _ => booleans | ifEmptyList (
        // --- NB, this condition is not necessary -- reducing on an empty array will do just this
        // anyway.
        _ => first | prepareField,
        bs => zip (ts, bs) | reduce (reducer) (first | prepareField),
      ) | Just,
    )
  },

  _toBool (bool, rowNum) {
    return this | updateM (
      'booleans',
      assocM (rowNum, BooleanRowAndOr (bool)),
    )
  },

  toOr (rowNum) {
    this._toBool (BooleanBooleanOr, rowNum)
    return this.updateNots ()
  },

  toAnd (rowNum) {
    this._toBool (BooleanBooleanAnd, rowNum)
    return this.updateNots ()
  },

  toNot (rowNum) {
    this | updateM (
      'booleans',
      assocM (rowNum, BooleanRowNot (true)),
    )
    return this.updateNots ()
  },

  updateNots () {
    const bools = this.booleans
    const len = bools.length
    if (!len) return this
    const lastRow = bools[len - 1]
    let foundNot
    const res = bools | mapX ((row, idx) => {
      if (foundNot) {
        if (row === lastRow) return row
        return BooleanRowNot (false)
      }
      if (row | isRowNot) {
        foundNot = true
        return BooleanRowNot (true)
      }
      return row
    })
    return this | mergeM ({
      booleans: res,
    })
  },

  addRow () {
    return this | updateM (
      'booleans',
        concat ([this.makeRow (false, BooleanRowAddRemove)])
        >> replaceSecondLastM (
           this.makeRow (false, BooleanRowAndOr (BooleanBooleanAnd)),
        )
      )
      | updateM (
        'texts',
        append (''),
      )
      | side ('updateNots')
  },

  removeRow () {
    const only = this.booleans.length === 2
    return this | updateM (
      'booleans',
      pop >> replaceLastM (this.makeRow (only)),
    ) | updateM (
      'texts',
      pop,
    )
  },

  get () {
    const { booleans, texts, } = this
    return zipWithAll (
      (boolean, text) => ({ boolean, text, }),
      booleans, texts,
    )
  },

  set (rows) {
    const reducer = (acc, { text, boolean, }) => ({
      booleans: [...acc.booleans, boolean],
      texts: [...acc.texts, text],
    })
    this | mergeM (
      rows | reduce (reducer, {
        booleans: [],
        texts: [],
      }),
    )
  },

  makeRow (isOnly, force=null) {
    return force || (isOnly ? BooleanRowAdd : BooleanRowAddRemove)
  },

}

const booleanPrimitiveToBoolean = condS ([
  'and'     | eq | guard (_ => BooleanRowAndOr (BooleanBooleanAnd)),
  'or'      | eq | guard (_ => BooleanRowAndOr (BooleanBooleanOr)),
  'not'     | eq | guard (_ => BooleanRowNot (false)),
  otherwise |      guard (_ => BooleanRowAdd),
])

const deserialiseInitialState = (() => {
  const reducer = (acc, [text, bool]) => lets (
    () => bool | booleanPrimitiveToBoolean,
    (boolean) => acc | append ({ boolean, text, })
  )
  return JSON.parse >> reduce (reducer, [])
}) ()

const TitleS = styled.div`
  margin-bottom: 18px;
  margin-left: 9px;
`

const Title = ({ isMobile, children, }) => lets (
  () => isMobile ?
    [H1M, { withLeftMargin: true, }] :
    [H1, {}],
  ([H, passProps]) => <TitleS>
    <H {...passProps}>{children}</H>
  </TitleS>,
)

class SearchRows extends Component {
  // --- when the reset button is clicked, SearchRows is rebuilt with a new key prop and the state
  // is destroyed, so we can test for a dummy value 'renderKey' to reinit the state when it's
  // destroyed and the first time it needs to be initted.

  static getDerivedStateFromProps (props, state) {
    return state.renderKey | ifOk (
      () => null,
      () => ({
        rows: props.qBuilder.get (),
        renderKey: 42,
      }),
    )
  }

  constructor (props) {
    super (props)

    const { onUpdate, qBuilder, reset, } = props
    const update = self => self | lets (
      _ => qBuilder.get (),
      (rows) => updateRefs (rows.length) (),
      (rows) => setState ({ rows }),
    )
    const updateRefs = (num) => _ => {
      this.textInputRefs = createRef | timesF (num)
    }
    const updated = side ('updated')
    const onChange = update >> updated
    const updateText = side2 ('updateText')
    const updateBoolean = side2 ('updateBoolean')
    const focusTextField = side1 ('focusTextField')

    this | mergeM ({
      onQueryChange: rowNum => value => this
        | updateText (rowNum, value)
        | onChange,
      onControlsChange: rowNum => event => this
        | updateBoolean (rowNum, event)
        | focusTextField (rowNum + 1)
        | onChange,
      },
    )
    | tap (updateRefs (this.initialRows.length))
    | updated
  }

  initialRows = this.props.qBuilder.get ()

  state = {
    // rows: this.initialRows,
  }

  textInputRefs = null

  curRowNum = void 8

  focusTextField = (rowNum) => this | deconstruct (
    ({ textInputRefs, }) => textInputRefs
      | prop ('rowNum')
      | whenOk (ref => ref.current.focus ()),
  )

  updated = () => this | deconstruct (
    ({ props, }) => props | deconstruct (
      ({ qBuilder, onUpdate, }) => onUpdate (
        qBuilder.get () | serialiseBooleans,
        qBuilder.queryOk (),
        qBuilder.hasText (),
      )
    )
  )

  updateBoolean = (rowNum, event) => this | deconstruct (
    ({ props, }) => props | deconstruct (
      ({ qBuilder, }) => qBuilder.updateBoolean (rowNum, event),
    ),
  )

  updateText = (rowNum, value) => this | deconstruct (
    ({ props, }) => props | deconstruct (
      ({ qBuilder, }) => qBuilder.updateText (rowNum, value),
    ),
  )

  onSubmit = (event) => {
    const { props, textInputRefs, curRowNum, } = this
    const { submit, } = props
    const inputRef = textInputRefs [curRowNum]
    inputRef.current.blur ()
    submit ()
  }

  onKeyPress = this | deconstruct (
    ({ onSubmit, }) => keyPressListen (onSubmit, 'Enter')
  )

  render () {
    const { props, state, onKeyPress, onQueryChange, } = this
    const { rows, } = state
    const { submit, reset } = props

    return <SearchRowsS>
      {rows | mapX ((row, idx) =>
        <SearchRowS key={idx}>
          <TextInput
            defaultValue={row.text}
            theRef={this.textInputRefs[idx]}
            inputProps={{
              placeholder,
            }}
            onChange={inputValue >> onQueryChange (idx)}
            onKeyPress={(event) => {
              this.curRowNum = idx
              return event | onKeyPress
            }}
          />
          <SearchControls
            rowNum={idx}
            onChange={this.onControlsChange}
            rowState={row.boolean}
            reset={reset}
          />
        </SearchRowS>
      )}
    </SearchRowsS>
  }
}

const OmgevingS = styled.span`
  ${mediaQuery (
    mediaPhone ('display: none'),
    mediaTablet ('display: inline'),
  )}
  span {
    font-weight: bold;
  }
`

const Omgeving = ({ env, }) => <OmgevingS>
  (<span>{env}</span> omgeving)
</OmgevingS>

const LocalTextDiv = styled.div`
  ${mediaQuery (
    mediaPhone ('display: none'),
    mediaTablet ('display: block'),
  )};
  margin-left: -33px;
`
const SearchIconS = styled.div`
  width: 100%;
  height: 100%;
  .x__inner {
    position: relative;
    top: 4px;
    left: 1px;
    width: 20px;
    height: 20px;
    margin: auto;
  }
  ${prop ('disabled') >> whenNotTrueV (`
    :active {
      top: 4px;
      left: 2px;
      filter: blur(0.4px);
    }
  `)}
`

const SearchIcon = ({ onClick, ...props }) => <SearchIconS
  disabled={props.disabled}
  onClick={onClick}
>
  <div className='x__inner'>
    <SearchIconSimple
      height='100%'
      width='100%'
      withFrame={false}
      disabled={props.disabled}
      strokeWidth={2}
      {...props}
    />
  </div>
</SearchIconS>

class AdvancedSearch extends PureComponent {
  state = {
    searchRowsKey: 0,
    booleanSpec: [],
    canSubmit: false,
    canReset: false,
  }

  theRefs = {
    wrapper: createRef (),
  }

  // --- for debouncing the advancedSearchBottomReached action.
  lastBottomReached = null

  qBuilder = this.props.qBuilder || queryBuilder.init ()
  onQueryChange = query => this | mergeM ({ query, })
  onUpdate = (booleanSpec, canSubmit, canReset) => this
    | setState ({ booleanSpec, canSubmit, canReset, })
  query = ''

  componentDidUpdate = (prevProps, prevState) => this | compMeth ((
    { results },
    {},
    { theRefs: { wrapper: { current: ref }}},
  ) => {
    this | whyUpdate ('adv. search', debugRender) (prevProps, prevState)
    if (results | advSearchIsLoadingMore) ref.scrollTo ({
      top: ref.scrollHeight - ref.offsetHeight,
      behavior: 'smooth',
    })
  })

  setSearchField = memoize (
    (field) => this | compMeth (
      ({ isMobile, advancedSearchSetFieldClickedDispatch, }) =>
        () => advancedSearchSetFieldClickedDispatch (isMobile, field),
    ),
  )

  reset = () => this | compMeth ((
    { advancedSearchClearResultsDispatch },
    { searchRowsKey },
    { qBuilder },
  ) => {
    qBuilder.clear ()
    advancedSearchClearResultsDispatch ()
    this | incrementStateValue ('searchRowsKey')
  })

  submit = () => this | compMeth ((
    { isMobile, advancedSearchSubmitClickedDispatch },
    _,
    { qBuilder },
  ) => lets (
    () => isMobile,
    (singlePageMode) => {
      qBuilder.generateQuery ()
        | map (query => advancedSearchSubmitClickedDispatch (singlePageMode, qBuilder, query)),
      this.lastBottomReached = null
    },
  ))

  scrolledMobile = (isLoading, onBottomReached) => (event) => {
    const { props, } = this
    const { isMobile, curPage, advancedSearchFetchingPreviews, } = props
    if (isLoading) console.debug ('scrolledMobile, loading')
    if (!isMobile || isLoading) return
    const { scrollTop: distTop, scrollHeight, offsetHeight, } = event.target
    const heightDiv = scrollHeight - offsetHeight
    // --- just checking for equality is not enough because distTop can have a small decimal part,
    // esp. on mobile.
    const bottomReached = distTop | scrolledCloseTo (heightDiv)
    const fetch = allV (
      bottomReached,
      // --- must disable fetch if the previews of the previous page are still being fetched.
      !advancedSearchFetchingPreviews,
      this.lastBottomReached !== curPage,
    )
    if (!fetch) return
    this.lastBottomReached = curPage
    onBottomReached ()
  }

  render = () => this | compMeth ((
    {
      history,
      isMobile,
      results,
      numResultsSetting,
      curPage,
      numHits,
      maxPageMb,
      advancedSearchField,
      advancedSearchNumResultsChangeClickedDispatch,
      advancedSearchSetPageNumClickedDispatch,
      advancedSearchResultClickedDispatch,
      advancedSearchBottomReachedDispatch,
    },
    {
      booleanSpec, canSubmit, canReset,
      searchRowsKey,
    },
    {
      reset, submit, onQueryChange, onUpdate, qBuilder,
      setSearchField,
      scrolledMobile,
      theRefs: { wrapper: wrapperRef },
    },
  ) => {
    const spinning = results | advSearchLoadingFirst
    const isLoading = results | advSearchIsLoadingAny

    return <WrapperS
      onScroll={scrolledMobile (isLoading, advancedSearchBottomReachedDispatch)}
      ref={wrapperRef}
      shouldDisableMomentumScroll={shouldDisableMomentumScroll ()}
    >
      <WrapperTallS>
        <QueryWrapperS>
          <Title isMobile={isMobile}>
            Geavanceerd zoeken
          </Title>
          <LocalTextDiv>
            <SectionNLM>
              In het invoerveld kun je een zoekterm (Nederlands, Grieks) of een specifieke frase invoeren. Om op een frase te zoeken, hoef je geen aanhalingstekens te gebruiken. Je kunt op elke plek in het woord gebruik maken van de volgende jokertekens:
              <ul>
                <li>
                  ? = precies 1 teken
                </li>
                <li>
                  * = 0 of meer tekens
                </li>
                <li>
                  Om meer zoektermen toe te voegen, klik je op de ‘+’.
                </li>
              </ul>
            </SectionNLM>
          </LocalTextDiv>
          <SearchS>
            <BooleanGridWrapperS height={(isMobile ? 72 : 50) * (booleanSpec | length)}>
              <BooleanGrid
                spec={booleanSpec}
                horizontalSkew={1.3}
                showNotEqualLine={!isMobile}
                showNotEqualIcon={true}
              />
            </BooleanGridWrapperS>
            <SearchRows
              key={searchRowsKey}
              onQueryChange={onQueryChange}
              onUpdate={onUpdate}
              submit={submit}
              qBuilder={qBuilder}
              reset={reset}
            />
            <SubmitS>
              <div className='x--phone'>
                <SearchIcon onClick={submit} disabled={canSubmit | not}/>
              </div>
              <div className='x--not-phone'>
                <div>
                  <Button type='submit' onClick={submit} disabled={canSubmit | not}>
                    Zoeken
                  </Button>
                  <Button type='reset' onClick={reset} disabled={canReset | not}>
                    Wissen
                  </Button>
                </div>
              </div>
              <SpinnerS visible={spinning}>
                <Spinner spinning={spinning} fontSize='13px'/>
              </SpinnerS>
            </SubmitS>
          </SearchS>
        </QueryWrapperS>
        <ResultsWrapperS>
          <AdvancedSearchResults
            isMobile={isMobile}
            results={results}
            numResults={numHits}
            maxPageMb={maxPageMb}
            curPage={curPage}
            curField={advancedSearchField}
            numResultsSetting={numResultsSetting}
            setNumResults={advancedSearchNumResultsChangeClickedDispatch}
            setPage={advancedSearchSetPageNumClickedDispatch}
            setField={setSearchField}
            onSelectResult={history | advancedSearchResultClickedDispatch}
          />
        </ResultsWrapperS>
      </WrapperTallS>
    </WrapperS>
  })
}

const mapStateToProps = createStructuredSelector ({
  results: makeSelectAdvancedSearchResults (),
  numResultsSetting: makeSelectAdvancedSearchNumResultsSetting (),
  curPage: makeSelectAdvancedSearchPageNum (),
  numHits: makeSelectAdvancedSearchNumHits (),
  maxPageMb: makeSelectAdvancedSearchMaxPage (),
  qBuilder: makeSelectAdvancedSearchQBuilder (),
  advancedSearchFetchingPreviews: makeSelectAdvancedSearchFetchingPreviews (),
  advancedSearchField: makeSelectAdvancedSearchField (),
})

const mapDispatchToProps = (dispatch) => ({
  advancedSearchClearResultsDispatch: advancedSearchClearResults >> dispatch,
  advancedSearchSubmitClickedDispatch: advancedSearchSubmitClicked >> dispatch,
  advancedSearchNumResultsChangeClickedDispatch: advancedSearchNumResultsChangeClicked >> dispatch,
  advancedSearchSetPageNumClickedDispatch: advancedSearchSetPageNumClicked >> dispatch,
  advancedSearchResultClickedDispatch: history => advancedSearchResultClicked (history) >> dispatch,
  advancedSearchBottomReachedDispatch: advancedSearchBottomReached >> dispatch,
  advancedSearchSetFieldClickedDispatch: advancedSearchSetFieldClicked >> dispatch,
})

export default AdvancedSearch
  | connect       (mapStateToProps, mapDispatchToProps)
  | injectSaga    ({ saga, key: 'advancedSearch' })
  | injectReducer ({ reducer, key: 'advancedSearch' })
