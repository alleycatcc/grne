const MOCK = true
const MOCKDELAY = 0

const localConfig = {
  verbose: true,
}

import {
  pipe, compose, composeRight,
  add, multiply, join,
  path,
  sprintf1, sprintfN, ok, dot1, timesF, dot, whenOk,
  compact,
  guard, condS,
  exception, raise,
  invoke,
  concatTo,
  lets,
  letS,
  die,
  defaultTo,
  neu1, notOk,
  drop, noop,
  ifPredicate, xMatch, id, reduceObj,
  tap, map,
  applyTo1,
  whenFalse, prop,
  ifOk,
  appendM,
  eq,
  merge, minus, reduce, mergeM,
  recurry,
  not,
  cond, guardV, otherwise, append,
  concat,
  defaultToV,
  split,
  ifAlways, nil,
  plus,
  update,
  remapTuples,
  flip,
  ifNotOk,
} from 'stick-js/es'

import { all, call, put, select, takeLatest, take, } from 'redux-saga/effects'

import { Left, Right, fold, flatMap, liftA2, liftA2N, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { requestJSON, defaultOpts, resultFoldMap, resultFold, } from 'alleycat-js/es/fetch'
import { tellIf, ierror, toString, logWith, resolveP, slice1, getQueryParams, iwarn, } from 'alleycat-js/es/general'
import { ifEmptyString, } from 'alleycat-js/es/predicate'
import { errorError, error as alertError, } from 'alleycat-js/es/react-s-alert'

import {
  HISTORY_BROWSED,
  LEMMATA_FETCH,
  LEMMATA_FETCH_SUCCESS,
  LEMMATA_FETCH_CHUNK_PREV,
  LEMMATA_FETCH_CHUNK_NEXT,
  ENTRIES_FETCH,
  ENTRIES_FETCH_CHUNK_PREV,
  ENTRIES_FETCH_CHUNK_NEXT,
  ADVANCED_SEARCH_FETCH_RESULTS,
  ADVANCED_SEARCH_LOAD_MORE_RESULTS,
  ADVANCED_SEARCH_SELECT_RESULT,
} from '../../containers/App/constants'

import {
  historyPopLemma,
  historyAddLemma,
  lemmataFetch,
  lemmataFetchError,
  lemmataFetchSuccess,
  advancedSearchFetchResults,
  advancedSearchFetchResultsSuccess,
  advancedSearchFetchResultsError,
  advancedSearchIncreasePageNum,
  advancedSearchNumResultsSet,
  parsgetFetch,
  entriesFetch,
  entriesFetchSuccess,
  entriesFetchError,
  entriesClear,
} from '../App/actions'

import {
  makeSelectHistoryCur,
  makeSelectHistoryWasBack,
} from '../../slices/app/selectors'

import {
  makeSelectLemmaIdToEntryLemma,
  makeSelectLemmaIdMin,
  makeSelectLemmaIdMax,
  makeSelectHaveLemmata,
  makeSelectAdvancedSearchQuery,
  makeSelectAdvancedSearchNumResultsSetting,
  makeSelectAdvancedSearchField,
  makeSelectAdvancedSearchPageNum,
  makeSelectAdvancedSearchSelectedResult,
  makeSelectEntries,
  makeSelectEntryIdMin,
  makeSelectEntryIdMax,
} from '../../slices/domain/selectors'

import { debugDev, browseLink, browseToId, } from '../../common'
import config from '../../config'
import { envIsTst, envIsNotTstLike, } from '../../env'
import { objObjReducer, objReducer, } from '../../transform'
import {
  FetchReplace, FetchAppend, FetchPrepend,
  searchFieldAuxDataToText,
  searchFieldAuxIsFullText,
} from '../../types'

const configTop = config | configure.init

const odd = x => x % 2 !== 0

const ifXMatch = xMatch >> ifPredicate

const tell = tellIf (localConfig.verbose)

const delay = (ms) => new Promise ((res, _) => setTimeout (res, ms))
const dq = sprintf1 ('"%s"')

const doSchool = 'showSchoolVersion' | configTop.get

// @todo config
const pageSizeDefault = 51
const pageSize = envIsNotTstLike ? pageSizeDefault : lets (
  () => getQueryParams (),
  (params) => params.pageSize || pageSizeDefault,
)
if (pageSize | odd | not) errorError ('Page size must be odd')

; `
  [getKey function, setKey, error hint, transform function]

  Note that undefined is not allowed as a value, but null is.
`

const _specLemmataResults = [
  ['html', prop ('html'), 'html'],
  ['id', prop ('id') >> Number >> minus (1), 'main_id'],
  ['lemma', prop ('lemma'), 'lemma'],
  ['homonymNr', prop ('homonym_nr'), 'homonym_nr'],
]

const _specLemmataMetadata = [
  ['maxLemmaId', prop ('max_lemma_id'), 'max_lemma_id'],
]

const _specLemmataResultsDoSchool = _specLemmataResults | concat ([
  ['htmlSchool', prop ('html_school'), 'html_school'],
  ['t1Gecontroleerd', prop ('t1_gecontroleerd'), 't1_gecontroleerd'],
])

const specLemmataResults = _specLemmataResults | objObjReducer
const specLemmataResultsDoSchool = _specLemmataResultsDoSchool | objObjReducer
const specLemmataMetadata = _specLemmataMetadata | objReducer
const specEntriesResults = objObjReducer ([
  ['id', prop ('id') >> Number >> minus (1), 'main_id'],
  ['lemma', prop ('lemma'), 'lemma'],
  ['homonymNr', prop ('homonym_nr'), 'homonym_nr'],
])
const specEntriesMetadata = objReducer ([
])

const transformResponseSpec = (results, metadata) => ({
  results,
  metadata,
})

/* `spec` looks like
 *
 *   {
 *     results: some-transformer,
 *     metadata: some-transformer,
 *   }
 */

const transformResponse = (spec) => (response) => {
  // --- first step towards making it more generic (though in this case we know it has exactly two
  // keys, which is why liftA2 works)
  const [resultsTransformed, metadataTransformed] = spec
    | remapTuples ((key, transformer) => response
      | prop (key)
      | defaultTo (() => die ('response missing key ' + key))
      | transformer
    )
  return [resultsTransformed, metadataTransformed] | liftA2N (
    // --- must be curried for liftA2; manual is ok.
    (results) => (metadata) => ({
      results,
      metadata,
    })
  )
}

function *generalAlert () {
  true | alertError
}

function *sagaAdvancedSearchFetchResults ({ data: singlePageMode, }) {
  const curPageNum = yield makeSelectAdvancedSearchPageNum () | select
  const pageNum = singlePageMode ? curPageNum + 1 : curPageNum
  // --- set in AdvancedSearch/saga on submit clicked.
  const query = yield makeSelectAdvancedSearchQuery () | select
  const numRows = yield makeSelectAdvancedSearchNumResultsSetting () | select
  const startRow = pageNum * numRows
  const requestURL = '/api/advanced-search/search'
  const searchFieldData = yield makeSelectAdvancedSearchField () | select
  const theSearchFieldIsFulltext = searchFieldData | searchFieldAuxIsFullText
  const searchFieldString = searchFieldData | searchFieldAuxDataToText
  const queryFull = query
    | ifXMatch (/^ \$/) (
      id,
      concatTo ('$ '),
    )
    | concat (' ∃ ' + dq (searchFieldString))

  const opts = defaultOpts | merge ({
    method: 'POST',
    body: JSON.stringify ({
      data: {
        query: queryFull,
        num_rows: numRows,
        start_row: startRow,
      },
    }),
  })
  const results = yield call (requestJSON, requestURL, opts)
  const onSuccessSinglePageMode = [
    advancedSearchIncreasePageNum () | put,
  ]
  const concatIfSinglePageMode = x => ifAlways (singlePageMode) (x | concat, id)
  const onSuccess = (data) => [
    data | advancedSearchFetchResultsSuccess | put,
  ] | concatIfSinglePageMode (onSuccessSinglePageMode)

  yield results | resultFold (
    (body) => all (body | onSuccess),
    (umsg) => umsg | advancedSearchFetchResultsError | put,
    (imsgMb) => null
      | tap (errorError)
      | tap (() => imsgMb | map (iwarn))
      | advancedSearchFetchResultsError
      | put,
  )
}

function *sagaAdvancedSearchLoadMoreResults () {
  // --- @todo doing this through the action would be better than calling the function directly, but
  // must make sure results doesn't go to Loading but to LoadingMore.
  // yield advancedSearchFetchResults (true) | put
  yield call (sagaAdvancedSearchFetchResults, { data: true, })
}

function *sagaAdvancedSearchSelectResult ({ data: {
  history,
  lemmaId,
  highlighting: _,
}}) {
  browseToId (history) (lemmaId)
}

function *sagaHistoryResolveLatest (failOk) {
  const lemmaId = yield makeSelectHistoryCur () | select
  const wasBack = yield makeSelectHistoryWasBack () | select
  if (wasBack)
    return yield historyPopLemma () | put
  // --- ok, already resolved.
  if (lemmaId | notOk) return
  const lookup = yield makeSelectLemmaIdToEntryLemma () | select
  const lemma = lookup (lemmaId, true)
  if (lemma | notOk) return failOk | whenFalse (
    () => ierror ("sagaHistoryResolveLatest: Can't resolve lemma for " + lemmaId),
  )
  yield historyAddLemma (lemmaId, lemma) | put
}

function *shouldFetchLemmata (lemmaId) {
  const halfPageSize = (pageSize - 1) / 2
  const haveLemmata = yield makeSelectHaveLemmata () | select
  const lemmaIdMin = yield makeSelectLemmaIdMin () | select
  const lemmaIdMax = yield makeSelectLemmaIdMax () | select

  const spec = {
    lemmaId,
    numBefore: halfPageSize,
    numAfter: halfPageSize,
    type: FetchReplace,
  }

  if (!haveLemmata) return spec

  return lets (
    () => lemmaIdMin - lemmaId,
    () => lemmaId - lemmaIdMax,
    (distanceBelow, distanceAbove) => cond (
      (() => distanceBelow >= halfPageSize) | guardV ({
        ...spec,
      }),
      (() => distanceBelow > 0) | guardV ({
        ...spec,
        numAfter: distanceBelow - 1,
        type: FetchPrepend,
      }),
      (() => distanceAbove >= 25) | guardV ({
        ...spec,
      }),
      (() => distanceAbove > 0) | guardV ({
        ...spec,
        numBefore: distanceAbove - 1,
        type: FetchAppend,
      }),
      otherwise | guardV (null),
    ),
  )
}

function *fetchLemmataMaybe (lemmaIdArg) {
  const shouldFetch = yield shouldFetchLemmata (lemmaIdArg)
  if (!shouldFetch) return
  const { type, lemmaId, numBefore, numAfter, } = shouldFetch
  yield lemmataFetch (lemmaId, numBefore, numAfter, type) | put
}

function *sagaHistoryBrowsed (..._) {
  const lemmaId = yield makeSelectHistoryCur () | select
  // --- browsed to non /browse page
  if (lemmaId | nil) return
  yield call (fetchLemmataMaybe, lemmaId)
  yield call (sagaHistoryResolveLatest, true)
}

function *sagaLemmataFetchSuccess (..._) {
  yield call (sagaHistoryResolveLatest, false)
}

function *sagaLemmataFetchSaga ({ data: { lemmaId, numBefore, numAfter, type }}) {
  const args = { data: { lemmaId, numBefore, numAfter, type, }}
  // --- in the future we can avoid one API call by deriving entries from lemmata while we're not
  // yet in entries explore mode, but this is far simpler for now.
  yield call (sagaLemmataFetch, args)
  yield entriesFetch (lemmaId, numBefore, numAfter, type) | put
}

function *sagaLemmataFetch ({ data: { lemmaId, numBefore, numAfter, type }}) {
  yield entriesClear () | put
  const mainId = lemmaId + 1

  // --- [lemmaId, terms]
  const highlightingInfo = yield makeSelectAdvancedSearchSelectedResult () | select
  const highlighting = highlightingInfo | ifNotOk (
    () => null,
    ([_, terms]) => ({
      [mainId]: {
        terms,
        abridge: false,
      },
    })
  )

  const url = '/api/lemma/block-by-id'
  const opts = defaultOpts | merge ({
    method: 'POST',
    body: JSON.stringify ({
      data: {
        id: mainId,
        num_before: numBefore,
        num_after: numAfter,
        do_school: doSchool,
        highlighting,
        do_post_processing: true,
      },
    }),
  })

  const responseTransformer = transformResponseSpec (
    doSchool ? specLemmataResultsDoSchool : specLemmataResults,
    specLemmataMetadata,
  )

  const results = yield call (requestJSON, url, opts)
  yield results
    | map ('results' | prop)
    | resultFoldMap (transformResponse (responseTransformer))
    | resultFold (
      (transformed) => lemmataFetchSuccess (transformed, type) | put,
      (umsg) => umsg | lemmataFetchError | put,
      (imsgMb) => null
        | tap (errorError)
        | tap (
          () => imsgMb | map (concatTo ('Error fetching lemmata: ') >> iwarn),
        )
        | lemmataFetchError
        | put,
    )
}

function *sagaLemmataFetchChunkPrev () {
  const lemmaIdMin = yield makeSelectLemmaIdMin () | select
  if (lemmaIdMin === 0) return
  yield lemmataFetch (lemmaIdMin - 1, 25, 0, FetchPrepend) | put
}

function *sagaLemmataFetchChunkNext () {
  const lemmaIdMax = yield makeSelectLemmaIdMax () | select
  yield lemmataFetch (lemmaIdMax + 1, 0, 25, FetchAppend) | put
}

function *sagaEntriesFetch ({ data: { lemmaId, numBefore, numAfter, type, }}) {
  const mainId = lemmaId + 1
  const url = '/api/lemma/block-by-id'
  const opts = defaultOpts | merge ({
    method: 'POST',
    body: JSON.stringify ({
      data: {
        id: mainId,
        num_before: numBefore,
        num_after: numAfter,
        do_school: false,
        do_post_processing: false,
        only_entries: true,
        highlighting: {},
      },
    }),
  })
  const results = yield call (requestJSON, url, opts)
  const responseTransformer = transformResponseSpec (
    specEntriesResults,
    specEntriesMetadata,
  )

  // @todo why was this necessary?
  // const hasOnlyEntries = yield makeSelectEntriesHaveNonDerived () | select
  // const fetchType = hasOnlyEntries ? type : FetchReplace
  const fetchType = type

  // yield call (delay, 50000)

  yield results
    | map ('results' | prop)
    | resultFoldMap (transformResponse (responseTransformer))
    | resultFold (
      (results) => entriesFetchSuccess (results, fetchType) | put,
      (umsg) => umsg | entriesFetchError | put,
      (imsgMb) => null
        | tap (errorError)
        | tap (
          () => imsgMb | map (concatTo ('Error fetching entries: ') >> iwarn),
        )
        | entriesFetchError
        | put,
    )
}

function *sagaEntriesFetchChunkPrev () {
  const entriesIdMin = yield makeSelectEntryIdMin () | select
  if (entriesIdMin === 0 || nil (entriesIdMin)) return
  yield entriesFetch (entriesIdMin - 1, 100, 0, FetchPrepend) | put
}

function *sagaEntriesFetchChunkNext () {
  const entriesIdMax = yield makeSelectEntryIdMax () | select
  // @todo check against max?
  if (nil (entriesIdMax)) return
  yield entriesFetch (entriesIdMax + 1, 0, 100, FetchAppend) | put
}

export default function *rootSaga () {
  yield all ([
    takeLatest (HISTORY_BROWSED, sagaHistoryBrowsed),
    takeLatest (LEMMATA_FETCH, sagaLemmataFetchSaga),
    takeLatest (LEMMATA_FETCH_SUCCESS, sagaLemmataFetchSuccess),
    takeLatest (LEMMATA_FETCH_CHUNK_PREV, sagaLemmataFetchChunkPrev),
    takeLatest (LEMMATA_FETCH_CHUNK_NEXT, sagaLemmataFetchChunkNext),
    takeLatest (ENTRIES_FETCH, sagaEntriesFetch),
    takeLatest (ENTRIES_FETCH_CHUNK_PREV, sagaEntriesFetchChunkPrev),
    takeLatest (ENTRIES_FETCH_CHUNK_NEXT, sagaEntriesFetchChunkNext),
    takeLatest (ADVANCED_SEARCH_FETCH_RESULTS, sagaAdvancedSearchFetchResults),
    takeLatest (ADVANCED_SEARCH_LOAD_MORE_RESULTS, sagaAdvancedSearchLoadMoreResults),
    takeLatest (ADVANCED_SEARCH_SELECT_RESULT, sagaAdvancedSearchSelectResult),
  ])
}
