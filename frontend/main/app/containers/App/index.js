import {
  pipe, compose, composeRight,
  ifTrue, sprintf1, ifFalse,
  sprintfN,
  whenPredicate,
  die,
  whenTrue,
  mergeM,
  prop, tap,
  lets,
  append,
  dot1,
  noop,
  arg1,
  prependM, take,
  dot,
  head,
  map,
  invoke,
  path, ok, join, flip, reduce, ifOk, appendM,
  id,
  list, compactOk,
  split,
  defaultToV,
  deconstruct,
} from 'stick-js/es'

import React, { PureComponent, } from 'react'
import styled from 'styled-components'
import { Switch, Route, withRouter, } from 'react-router-dom'
import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import FontFaceObserver from 'fontfaceobserver'

import { then, recover, promiseToEither, allP, } from 'alleycat-js/es/async'
import configure from 'alleycat-js/es/configure'
import { clss, } from 'alleycat-js/es/dom'
import { fontFace, cssFont, } from 'alleycat-js/es/font'
import { logWith, info, warn, iwarn, } from 'alleycat-js/es/general'
import { whenPredicateResult, whenTrueV, ifEmptyString, } from 'alleycat-js/es/predicate'
import { withResizeListener, setState, whyUpdate, whyUpdateMeth, compMeth, } from 'alleycat-js/es/react-legacy'
import { mediaQuery, } from 'alleycat-js/es/styled'

import { historyBrowsed, } from './actions'

import domainReducer from '../../slices/domain/reducer'
import uiReducer from '../../slices/ui/reducer'

import appReducer from '../../slices/app/reducer'

import { makeSelectError, } from '../../slices/domain/selectors'

import saga from './saga'

import { Alert, } from '../../alleycat-components'
import Main from '../../containers/Main'
import NotFoundPage from '../../containers/NotFoundPage'

import { mediaPhone, mediaTablet, mediaDesktop, isMobileWidth, shouldDisableMomentumScroll, } from '../../common'
import config from '../../config'

import {
  TabHome, TabAnalyse, TabAnalyseXmlToHtmlPrint, TabColofon,
  TabFeedback, TabAdvancedSearch, TabBrowse, TabHulp,
} from '../../types'

import injectReducer from '../../utils/injectReducer'
import injectSaga from '../../utils/injectSaga'

const configTop = config | configure.init

const debugRender = 'debug.rerender' | configTop.get
const fontMainFamily = 'font.main.family' | configTop.get

const fontStyles = [
  ['normal', 'normal'],
  ['normal', 'italic'],
  ['bold', 'normal'],
  ['bold', 'italic'],
]

const loadFont = 'load' | dot
const slice = dot1 ('slice')

const startFontObserver = fontFamily => fontStyles | map (
    ([weight, style]) => new FontFaceObserver (fontFamily, {
      weight,
      style,
    }),
  )
  | map (loadFont)
  | allP
  // --- doesn't always die on failure @todo
  | recover ((fontDetails) => die (
    fontFamily | sprintf1 ('timed out waiting for font %s'),
    fontDetails | JSON.stringify,
  ))

const AppWrapper = styled.div`
  ${mediaQuery (
    mediaPhone ('font-size: 14px'),
    mediaTablet ('font-size: 12px'),
  )}
  // --- enable 'momentum scroll' on Safari.
  // --- this would be a good place to put this, so it trickles down to all components, but it seems
  // to be causing problems on iPhone5 Safari.
  // -webkit-overflow-scrolling: touch;

  // --- this is mysterious -- it's a hack which *might* help with scrolling/flickering issues on
  // iPhone5 Safari by forcing GPU.
  transform: translate3d(0, 0, 0);

  position: relative;
  overflow-x: hidden;
  overflow-y: hidden;
  margin: 0 auto;
  display: flex;
  height: 100%;
  flex-direction: column;
  z-index: 1;
`
  | withResizeListener

class App extends PureComponent {
  state = {
    isMobileWidth: false,
    fontLoaded: false,
  }

  onResize = ({ width, }) => lets (
    () => width | isMobileWidth,
    (isMobile) => this | setState ({ isMobile, }),
  )

  startHistoryNew () {
    const { props, } = this
    const { listenHistory, historyBrowsedDispatch, } = props
    historyBrowsedDispatch (document.location.href, false)
    const unlisten = listenHistory (
      (location, action, eventInfo) => location | deconstruct (
        ({ pathname, }) => eventInfo | deconstruct (
          ({ type, pathChanged, }) => historyBrowsedDispatch (pathname, type === 'back' && pathChanged),
        ),
      ),
    )
    void unlisten
  }

  startHistoryLegacy () {
    const { props, } = this
    const { history, historyBrowsedDispatch, } = props
    historyBrowsedDispatch (document.location.href, false)
    const unlisten = history.listen (
      (location, action) => location | deconstruct (
        ({ pathname, }) => historyBrowsedDispatch (pathname, false),
      )
    )
  }

  componentDidMount () {
    const { props, state, } = this
    fontMainFamily
      | startFontObserver
      | then (() => this | setState ({ fontLoaded: true, }))
      // --- if the font fails (although local fonts really shouldn't fail), keep going.
      | recover ((err) => {
        err | console.error
        this | setState ({ fontLoaded: true, })
      })

    // this.startHistoryNew ()
    this.startHistoryLegacy ()
  }

  componentDidUpdate = this | whyUpdateMeth ('App', debugRender)

  componentWillUnmount () {
    iwarn ('App unmounting, unexpected.')
  }

  render = () => this | compMeth (
    ({ error, history }, { isMobile, fontLoaded }, { onResize }) => {
      // --- note that after mounting, isMobile is still false for an instant, even on mobile.
      const passProps = { history, isMobile, }
      const cls = clss (
        isMobile | whenTrueV ('x--mobile'),
      )

      return error | ifTrue (_ =>
        <div>
          <div>{'Sorry, but we’ve encountered a fatal error.'}</div>
          <div>Please reload the page and start again.</div>
        </div>
      ) (_ => <AppWrapper className={cls} whenResize={onResize}>
        {fontLoaded | ifFalse (
          () => '',
          () => <Switch>
            <Route exact path="/" render={(props) => <Main
                tab={TabHome}
                {...passProps}
              />}
            />
            <Route exact path="/browse" render={(props) => <Main
                tab={TabBrowse}
                routeParams={{lemmaId: 1}}
                {...passProps}
              />}
            />
            <Route path="/browse/:lemmaId" render={(props) => <Main
              tab={TabBrowse}
                routeParams={props.match.params}
                {...passProps}
              />}
            />
            <Route path="/feedback" render={(props) => <Main
                tab={TabFeedback}
                {...passProps}
              />}
            />
            <Route path="/advanced-search" render={(props) => <Main
                tab={TabAdvancedSearch}
                {...passProps}
              />}
            />
              <Route path="/analyse/xml-to-html-print/:batchId" render={(props) => <Main
                tab={TabAnalyseXmlToHtmlPrint}
                routeParams={props.match.params}
                {...passProps}
              />}
            />
            <Route path="/analyse" render={(props) => <Main
                tab={TabAnalyse}
                {...passProps}
              />}
            />
            <Route path="/colofon" render={(props) => <Main
                tab={TabColofon}
                {...passProps}
              />}
            />
            <Route path="/hulp" render={(props) => <Main
                tab={TabHulp}
                {...passProps}
              />}
            />
            <Route path="" component={NotFoundPage} />
          </Switch>
          )}
          {Alert ()}
        </AppWrapper>
      )
    }
  )
}

const mapDispatchToProps = (dispatch) => ({
  historyBrowsedDispatch: historyBrowsed >> dispatch,
})

const mapStateToProps = createStructuredSelector ({
  error: makeSelectError (),
})

// --- withRouter is necessary to overcome 'blocked updates' (router doesn't respond to hash
// change).

export default App
  | connect       (mapStateToProps, mapDispatchToProps)
  | withRouter
  | injectSaga    ({ saga, key: 'app' })
  | injectReducer ({ reducer: domainReducer, key: 'domain' })
  | injectReducer ({ reducer: uiReducer,     key: 'ui' })
  | injectReducer ({ reducer: appReducer,    key: 'app' })
