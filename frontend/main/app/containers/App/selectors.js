import { createSelector, } from 'reselect'

import {
  pipe, compose, composeRight,
  dot, dot1, whenOk, ifOk,
} from 'stick-js/es'
