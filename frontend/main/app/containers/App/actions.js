/**

Design: consider all actions and constants to be global, and keep them in containers/App.

We have three general stores: api, domain, and ui. `api` keeps track of, for instance, whether any api call is 'loading', in order to show a general spinner for example. `domain` keeps track of the domain data: everything which comes in as a result of api calls. It also has its own `error` property, which if true, means the app should halt (store is totally corrupted). `ui` keeps track of various UI state which for whatever reason shouldn't be tracked as react state: switches, toggles, etc.

Components which are classes are assumed to use react state and/or make use of react lifecycle methods; otherwise they should be stateless.

Containers can in principle be stateless but we always use classes to begin with. They can be whittled down later.

Put the ('fat') saga which sequences actions, performs lots of work, calls other sagas etc. as high
as possible in the tree. We generally use App/saga for this, and the sagas in the component
directories to respond to really specific ('skinny') actions like SOME_BUTTON_CLICKED.

Sagas are generally cancelled when the component unmounts (depending on the flags given to injectSaga). To take advantage of this for the fat sagas, put them not in App/saga but in the highest component which, when unmounted, should cancel them.

For example, let's say the Browse component is pulling in new words and then the user goes to Colofon. Should the fetching be cancelled? Then use Browse/saga. Should it keep fetching in the background (what we do now)? Use App/saga.

Containers can technically have their own constants, actions & reducers, but we tend not to use them
(keep all domain data in the `domain` store). But it could be useful as the app gets more complex.

UI events can either

1) Call an action directly (fetchKeysDispatch prop).

2) Fire a past tense action in that component's saga (e.g. GET_ASSIGNED_KEYS_CLICKED). From there it can pass control to a saga in the main saga (e.g. FETCH_ASSIGNED_KEYS), in which case it's considered a 'fat' action; or, if it stays within its own saga, e.g. updating some local UI, it's a 'skinny' action.

If it's not clear whether an action is fat or skinny, consider it fat and put it in the main saga, then move it out when it's clear it's skinny.

Passing control to the main saga can be done by using put() on an action.

If it's necessary to wait on the main saga and then continue in the local saga, there's two ways:

1) put() an action (FETCH_ASSIGNED_KEYS) and then take() the actions (FETCH_ASSIGNED_KEYS_SUCCESS and FETCH_ASSIGNED_KEYS_ERROR). This feels pretty spaghetti, but is also simple to understand. Advantage: you know if it was success / error.
2) call the saga directly with yield call(). This feels unconventional but saner. But, then you can't call fetchAssignedKeys() from within the local saga, which again means missing a bit of control. And you have to do a new select to find out if it went well.
3) Return a promise from the action creator and use put.sync? Haven't managed this and it all starts getting more complicated.

If it's not necessary to wait, just put the action.

Some store flags:

 domain:
  error: the state has become corrupted. fatal.
    --- 'Oops, the app has shut down unexpectedly.
    Please reload the page to try again'.
    --- the main component should have a selector
    for this and shut everything down.
 api:
  loading: bool
  success: [yes/no/void]
  error: last api request failed. not fatal.
    --- 'A request to the server failed -- please try again'.
 ui:
  error: show the bubble, with 'oops' or a message.

  as part of a fat action:
    FETCH_ASSIGNED_KEYS_ERROR

Past tense actions:

 click on 'switch tab'.
 a ui event which only affects the ui.
 SWITCH_TAB_CLICKED

 click on 'get assigned keys' button.
 a ui event which affects the ui and the domain.
 GET_ASSIGNED_KEYS_CLICKED

 callback returned.

Present tense: we want something to be done. UPLOAD_FILE, SET_THIS, TOGGLE_THAT.

["isn't it overkill to give the event an action? why not just FETCH_ASSIGNED_KEYS as the action?" because, if you later decide that the button should FETCH_ASSIGNED_KEYS and also change the UI (e.g. close a modal), depending on whether or not it succeeded, you will have to switch to a saga. And you can't put the close modal action inside FETCH_ASSIGNED_KEYS, because another button might also want to FETCH_ASSIGNED_KEYS and not close the modal, not semantic anyway etc.]

  buttons saga: GET_ASSIGNED_KEYS_CLICKED: put FETCH_ASSIGNED_KEYS
  api reducer: FETCH_ASSIGNED_KEYS: api loading
  component: api selector / get ('loading'): component spinner
  main saga: FETCH_ASSIGNED_KEYS:
   [skipping FETCH_ASSIGNED_KEYS_BEGIN]
  main saga: do the request.
  main saga: put FETCH_ASSIGNED_KEYS_SUCCESS/FETCH_ASSIGNED_KEYS_ERROR
  api reducer: FETCH_ASSIGNED_KEYS_SUCCESS/FETCH_ASSIGNED_KEYS_ERROR: component loading.
  domain reducer: FETCH_ASSIGNED_KEYS_SUCCESS/FETCH_ASSIGNED_KEYS_ERROR: update state.

  ui events: CLICKED, CHOSEN, CHANGED, OPENED, CLOSED, ...


as for addkeysfromcsvmodal: everything happens in the local saga. if it seems ok, don't over-engineer ...


from ButtonsMain/saga#sagaGetAssignedKeysClicked
 can put fetchAssignedKeys (), but it's async, so what if you want to do some ui after that?
 can do yield sagaFetchAssignedKeys | call, is that weird?
 can do put.sync, how?
avoid actions.ui.blah because typos too easy.

FETCH_ASSIGNED_KEYS_ERROR

If a container has associated components, put them in the folder with the container.

Always use PureComponent, not Component, and if you pass changes through the redux way it should always work. (Component uses a deep compare).

Saga formulas:
  no parens:
    yield keys | fetchAssignedKeysSuccess | put
  yes parens:
  yield fetchAssignedKeys () | put

It's ok to pull things from selectors inside the saga.

*/

`

- action 'getAssignedKeysClicked' fired by button component
- GET_ASSIGNED_KEYS_CLICKED is picked up in the saga of the component
- saga: yield fetchAssignedKeys () | put
- FETCH_ASSIGNED_KEYS is picked up in
 - the domain reducer
   - which prepares the domain store to receive success or error
 - the ui reducer
   - which prepares the ui store to receive success or error, shows a spinner in interested components
 - the App saga.
- saga: takeLatest FETCH_ASSIGNED_KEYS sagaFetchAssignedKeys
- sagaFetchAssignedKeys: yield call (request, requestURL) in a try catch
- on success, put fetchAssignedKeysSuccess
- on exception, put fetchAssignedKeysError

(@todo combine those into a single action which returns an either)


`

import {
  pipe, compose, composeRight,
  tap,
} from 'stick-js/es'

import {
  ADVANCED_SEARCH_SUBMIT_CLICKED,
  ADVANCED_SEARCH_CLEAR_RESULTS,
  ADVANCED_SEARCH_FETCH_RESULTS,
  ADVANCED_SEARCH_FETCH_RESULTS_SUCCESS,
  ADVANCED_SEARCH_FETCH_RESULTS_ERROR,
  ADVANCED_SEARCH_SET_NUM_RESULTS_CLICKED,
  ADVANCED_SEARCH_SET_NUM_RESULTS,
  ADVANCED_SEARCH_SET_QUERY,
  ADVANCED_SEARCH_SET_QBUILDER,
  ADVANCED_SEARCH_SET_PAGE_NUM_CLICKED,
  ADVANCED_SEARCH_SET_PAGE_NUM,
  ADVANCED_SEARCH_FETCH_PREVIEWS_ERROR,
  ADVANCED_SEARCH_FETCH_PREVIEWS_SUCCESS,
  ADVANCED_SEARCH_RESULT_CLICKED,
  ADVANCED_SEARCH_SELECT_RESULT,
  SIMPLE_QUERY_SET,
  BROWSE_ENTERED,
  ENTRIES_FETCH_CHUNK_PREV_CLICKED,
  ENTRIES_FETCH_CHUNK_NEXT_CLICKED,
  ENTRIES_FETCH_CHUNK_PREV,
  ENTRIES_FETCH_CHUNK_NEXT,
  ENTRIES_FETCH,
  ENTRIES_FETCH_SUCCESS,
  ENTRIES_FETCH_ERROR,
  ENTRIES_CLEAR,
  LEMMATA_FETCH,
  LEMMATA_FETCH_SUCCESS,
  LEMMATA_FETCH_ERROR,
  LEMMATA_FETCH_CHUNK_PREV,
  LEMMATA_FETCH_CHUNK_NEXT,
  LEMMATA_CLEAR,
  LEMMATA_CROP,
  LEMMATA_CROP_CLEAR,
  ENTRY_SET_SELECTED_OFFSET_TOP,
  HISTORY_BROWSED,
  HISTORY_ADD_LEMMA,
  HISTORY_POP_LEMMA,
  EMAIL_SEND_CLICKED,
  EMAIL_SEND,
  EMAIL_SEND_SUCCESS,
  EMAIL_SEND_ERROR,
  PARS_RESULT_SELECTED,
  PARSGET_FETCH,
  CITG_WORD_CLICKED,
  PARSGET_FETCH_SUCCESS,
  PARSGET_FETCH_ERROR,
  PARSGET_RESULTS_CLEAR,
  PARS_LINK_CLICKED,
  PARS_CLOSE_CLICKED,
  ENTRY_LIST_ELEMENT_CLICKED,
  LEMMA_CLICKED,
  SIZE_SELECTED,
  SET_SIZE,
  FORCE_UPDATE_WINDOW_LIST,
  ADVANCED_SEARCH_BOTTOM_REACHED,
  ADVANCED_SEARCH_FETCH_PREVIEWS,
  ADVANCED_SEARCH_LOAD_MORE_RESULTS,
  ADVANCED_SEARCH_INCREASE_PAGE_NUM,
  ADVANCED_SEARCH_RESET_PAGE_NUM,
  ADVANCED_SEARCH_SET_FIELD,
  ADVANCED_SEARCH_SET_FIELD_CLICKED,
} from './constants'

import { logWith, } from 'alleycat-js/es/general'

export const advancedSearchSubmitClicked = (singlePageMode, qBuilder, query) => ({
  type: ADVANCED_SEARCH_SUBMIT_CLICKED,
  data: { singlePageMode, qBuilder, query },
})

export const advancedSearchClearResults = () => ({
  type: ADVANCED_SEARCH_CLEAR_RESULTS,
})

export const advancedSearchFetchResults = (singlePageMode) => ({
  type: ADVANCED_SEARCH_FETCH_RESULTS,
  data: singlePageMode,
})

export const advancedSearchFetchResultsSuccess = (results) => ({
  type: ADVANCED_SEARCH_FETCH_RESULTS_SUCCESS,
  data: results,
})

export const advancedSearchFetchResultsError = (err) => ({
  type: ADVANCED_SEARCH_FETCH_RESULTS_ERROR,
  data: err,
})

export const advancedSearchNumResultsChangeClicked = (num) => ({
  type: ADVANCED_SEARCH_SET_NUM_RESULTS_CLICKED,
  data: num,
})

export const advancedSearchNumResultsSet = (num) => ({
  type: ADVANCED_SEARCH_SET_NUM_RESULTS,
  data: num,
})

export const advancedSearchSetQuery = (query) => ({
  type: ADVANCED_SEARCH_SET_QUERY,
  data: query,
})

export const advancedSearchSetQBuilder = (qBuilder) => ({
  type: ADVANCED_SEARCH_SET_QBUILDER,
  data: qBuilder,
})

export const advancedSearchSetPageNumClicked = (pageNum) => ({
  type: ADVANCED_SEARCH_SET_PAGE_NUM_CLICKED,
  data: pageNum,
})

export const advancedSearchSetPageNum = (pageNum) => ({
  type: ADVANCED_SEARCH_SET_PAGE_NUM,
  data: pageNum,
})

export const advancedSearchFetchPreviews = (params) => ({
  type: ADVANCED_SEARCH_FETCH_PREVIEWS,
  data: params,
})

export const advancedSearchFetchPreviewsSuccess = (results) => ({
  type: ADVANCED_SEARCH_FETCH_PREVIEWS_SUCCESS,
  data: results,
})

export const advancedSearchFetchPreviewsError = (err) => ({
  type: ADVANCED_SEARCH_FETCH_PREVIEWS_ERROR,
  data: err,
})

// --- note, curried to take `history` as well.
export const advancedSearchResultClicked = (history) => (resultIdx) => ({
  type: ADVANCED_SEARCH_RESULT_CLICKED,
  data: { history, resultIdx },
})

export const advancedSearchSelectResult = (history, lemmaId, highlighting) => ({
  type: ADVANCED_SEARCH_SELECT_RESULT,
  data: { history, lemmaId, highlighting },
})

export const browseEntered = (lemmaId) => ({
  type: BROWSE_ENTERED,
  data: lemmaId,
})

export const lemmataFetchSuccess = (results, type) => ({
  type: LEMMATA_FETCH_SUCCESS,
  data: { results, type },
})

export const lemmataFetchError = () => ({
  type: LEMMATA_FETCH_ERROR,
})

export const lemmataClear = () => ({
  type: LEMMATA_CLEAR,
})

export const historyBrowsed = (url, wasBack) => ({
  type: HISTORY_BROWSED,
  data: { url, wasBack },
})

export const historyAddLemma = (lemmaId, naamFull) => ({
  type: HISTORY_ADD_LEMMA,
  data: { lemmaId, naamFull },
})

export const historyPopLemma = () => ({
  type: HISTORY_POP_LEMMA,
})

export const lemmataFetch = (lemmaId, numBefore, numAfter, type) => ({
  type: LEMMATA_FETCH,
  data: { lemmaId, numBefore, numAfter, type },
})

export const entriesFetch = (lemmaId, numBefore, numAfter, type) => ({
  type: ENTRIES_FETCH,
  data: { lemmaId, numBefore, numAfter, type },
})

export const entriesFetchSuccess = (results, type) => ({
  type: ENTRIES_FETCH_SUCCESS,
  data: { results, type, },
})

export const entriesFetchError = () => ({
  type: ENTRIES_FETCH_ERROR,
})

export const entriesClear = () => ({
  type: ENTRIES_CLEAR,
})

export const lemmataFetchChunkPrev = () => ({
  type: LEMMATA_FETCH_CHUNK_PREV,
})

export const lemmataFetchChunkNext = () => ({
  type: LEMMATA_FETCH_CHUNK_NEXT,
})

export const entriesFetchChunkPrev = () => ({
  type: ENTRIES_FETCH_CHUNK_PREV,
})

export const entriesFetchChunkNext = () => ({
  type: ENTRIES_FETCH_CHUNK_NEXT,
})

export const entriesFetchChunkPrevClicked = () => ({
  type: ENTRIES_FETCH_CHUNK_PREV_CLICKED,
})

export const entriesFetchChunkNextClicked = () => ({
  type: ENTRIES_FETCH_CHUNK_NEXT_CLICKED,
})

export const lemmataCrop = (crop) => ({
  type: LEMMATA_CROP,
  data: crop,
})

export const lemmataCropClear = () => ({
  type: LEMMATA_CROP_CLEAR,
})

export const entrySetSelectedOffsetTop = (offsetTop) => ({
  type: ENTRY_SET_SELECTED_OFFSET_TOP,
  data: offsetTop,
})

export const emailSendClicked = (name, email, bericht) => ({
  type: EMAIL_SEND_CLICKED,
  data: { name, email, bericht },
})

export const emailSend = (name, email, bericht) => ({
  type: EMAIL_SEND,
  data: { name, email, bericht },
})

export const emailSendSuccess = () => ({
  type: EMAIL_SEND_SUCCESS,
})

export const emailSendError = (err) => ({
  type: EMAIL_SEND_ERROR,
  data: err,
})

export const parsResultSelected = (query) => ({
  type: PARS_RESULT_SELECTED,
  data: query,
})

export const citgWordClicked = (word) => ({
  type: CITG_WORD_CLICKED,
  data: word,
})

export const parsgetFetch = (originator, query) => ({
  type: PARSGET_FETCH,
  data: { originator, query },
})

export const parsgetFetchSuccess = (originator, query, results) => ({
  type: PARSGET_FETCH_SUCCESS,
  data: { originator, results, query },
})

export const parsgetFetchError = (originator, err) => ({
  type: PARSGET_FETCH_ERROR,
  data: { originator, err },
})

export const parsLinkClicked = (originator) => ({
  type: PARS_LINK_CLICKED,
  data: originator,
})

export const parsCloseClicked = (originator) => ({
  type: PARS_CLOSE_CLICKED,
  data: originator,
})

export const parsgetResultsClear = (originator) => ({
  type: PARSGET_RESULTS_CLEAR,
  data: originator,
})

export const entryListElementClicked = (originator, refresh) => ({
  type: ENTRY_LIST_ELEMENT_CLICKED,
  data: { originator, refresh, },
})

export const lemmaClicked = () => ({
  type: LEMMA_CLICKED,
})

export const setSize = (size) => ({
  type: SET_SIZE,
  data: size,
})

export const sizeSelected = (size) => ({
  type: SIZE_SELECTED,
  data: size,
})

export const forceUpdateWindowList = () => ({
  type: FORCE_UPDATE_WINDOW_LIST,
})

export const advancedSearchBottomReached = () => ({
  type: ADVANCED_SEARCH_BOTTOM_REACHED,
})

export const advancedSearchLoadMoreResults = () => ({
  type: ADVANCED_SEARCH_LOAD_MORE_RESULTS,
})

export const advancedSearchIncreasePageNum = () => ({
  type: ADVANCED_SEARCH_INCREASE_PAGE_NUM,
})

export const advancedSearchResetPageNum = () => ({
  type: ADVANCED_SEARCH_RESET_PAGE_NUM,
})

export const advancedSearchSetField = (field) => ({
  type: ADVANCED_SEARCH_SET_FIELD,
  data: field,
})

export const advancedSearchSetFieldClicked = (singlePageMode, field) => ({
  type: ADVANCED_SEARCH_SET_FIELD_CLICKED,
  data: { singlePageMode, field, },
})

export const simpleQuerySet = (type, query) => ({
  type: SIMPLE_QUERY_SET,
  data: { type, query, },
})
