// --- note: in general we keep all actions in the main actions.js.

import {
  DEFAULT_ACTION,
} from './constants';

export function defaultAction () {
  return {
    type: DEFAULT_ACTION,
  }
}
