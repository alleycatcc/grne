import {
  pipe, compose, composeRight,
  lets, noop, tap, always, map, prop, path,
  sprintf1, sprintfN,
  not, letS,
  ifTrue,
  deconstruct,
} from 'stick-js/es'

import React, { Fragment, PureComponent, memo, } from 'react'

import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'
import styled from 'styled-components'

import { parsResultSelected, simpleQuerySet, parsgetResultsClear, } from '../App/actions'

import {
  makeSelectLatestSimpleLemmaQuery,
  makeSelectLatestSimpleParsQuery,
} from '../../slices/ui/selectors'

import Footer from '../../components/Footer'
import GrieksTypen from '../../components/GrieksTypen'
import KeyboardExplanation from '../../components/KeyboardExplanation'
import OverHetWoordenboek from '../../components/OverHetWoordenboek'
import ParsResultsTable from '../../containers/ParsResultsTable'
import SearchField from '../../components/SearchField'
import { H1M, H2, H2M, H2NLM, SectionNLM, LinkLikeTheme2, Div, } from '../../components/shared'
import KruimelPad from '../../containers/KruimelPad'

import configure from 'alleycat-js/es/configure'
import { mapX, logWith, } from 'alleycat-js/es/general'
import { ifTrueV, } from 'alleycat-js/es/predicate'
import { setState, createRef, compMeth, } from 'alleycat-js/es/react-legacy'
import { mediaQuery, } from 'alleycat-js/es/styled'

import { mediaPhone, mediaTablet, mediaDesktop, isSafari, browseToId, browseTo, onKruimelItemClickStandard, } from '../../common'
import config from '../../config'
import injectSaga from '../../utils/injectSaga'
import injectReducer from '../../utils/injectReducer'

import reducer from './reducer'
import saga from './saga'

const configTop = config | configure.init

const placeholderLem = 'placeholder.normalLem' | configTop.get
const placeholderPars = 'placeholder.normalPars' | configTop.get

const ZoekenS = styled.div`
  ${mediaQuery (
    mediaPhone ('display: none;'),
    mediaTablet ('display: inherit;'),
  )}
`

const ZoekenMS = styled.div`
  width: 100%;
  ${mediaQuery (
    mediaPhone ('display: block;'),
    mediaTablet ('display: none;'),
  )}
  .x__flex-wrapper {
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
  }
  .x__kruimel {
    // --- to match the 45px of the following page so kruimelpad looks fixed.
    flex: 0 0 50px;
  }
  .x__about {
    flex: 0 0 120px;
  }
  .x__lemmaveld {
    flex: 0 0 100px;
  }
  .x__parsveld {
    flex: 0 0 100px;
  }
  .x__keyboard {
    flex: 0 0 50px;
    .x__keyboard-contents {
      > * {
        vertical-align: top;
      }
    }
  }
  .x__keyboard-m {
    flex: 1 0 1px;
    height: 90%;
    width: 95%;
    position: relative;
    right: 0px;
    border: 1px solid black;
    box-shadow: 1px 1px 10px #999;
    > * {
      margin: auto;
    }
  }
`

const SearchFieldWrapperS = styled.div`
  ${mediaQuery (
    mediaPhone ('width: 100%'),
    mediaTablet ('width: 100%'),
    mediaDesktop ('width: 95%'),
  )}
  .x__text {
    padding-bottom: 5px;
    width: 91%;
  }
`

const Zoeken = ({ history, isMobile, onSelectLemma, onSelectPars, }) => <ZoekenS>
  <H2NLM>Zoeken</H2NLM>
  <SectionNLM>
    <SearchFieldWrapperS>
      <div className="x__text">
        Zoek een woord in het woordenboek.
      </div>
      <SearchField
        type='lemma'
        history={history}
        placeholder={placeholderLem}
        autoFocus={!isMobile}
        onSelectResult={onSelectLemma}
      />
    </SearchFieldWrapperS>
  </SectionNLM>
  <H2NLM>Determineer een vorm</H2NLM>
  <SectionNLM>
    <SearchFieldWrapperS>
      <div className="x__text">
        Weet je niet zeker van welk hoofdwoord je vorm komt? Typ hier je vorm in.
      </div>
      <SearchField
        type='pars'
        history={history}
        placeholder={placeholderPars}
        onSelectResult={onSelectPars}
      />
      <ParsResultsTable
        originatorKey='searchField'
        ambient='home'
      />
  </SearchFieldWrapperS>
  </SectionNLM>
</ZoekenS>

const SectionMS = styled.div`
  margin-top: 8px;
`

const SectionM = (props) => <SectionMS>
  <SectionNLM {...props}/>
</SectionMS>

const HulpHandleS = styled.div`
  display: inline-block;
  width: 20px;
  margin-left: 0px;
  margin-right: 7px;
  margin-top: 1px;
  .x__contents {
    width: 5px;
    margin: auto;
    transition: transform 300ms;
    ${prop ('isOpen') >> ifTrueV ('0', '-90') >> sprintf1 (`
      transform: rotate(%sdeg);
    `)}
  }
`

const HulpHandle = ({ isOpen, }) => <HulpHandleS isOpen={isOpen}>
  <div className='x__contents'>
    δ
  </div>
</HulpHandleS>

const ZoekenM = ({
  history,
  plain,
  initialValueLemma, initialValuePars,
  onSelectLemma, onSelectPars,
  onFocusLemma, onFocusPars, onBlurLemma, onBlurPars,
  lemmaRef, parsRef, hulpTypenRef,
  parsResultSelectedDispatch, onHulpClicked,
  keyboardMobileVisible, isMobile,
  closeKeyboardDropdown,
  onKruimelItemClick,
}) => <ZoekenMS>
  <div className='x__flex-wrapper'>
    <div className='x__kruimel'>
      <KruimelPad
        history={history}
        isMobile={isMobile}
        alwaysMakeLinks={true}
        onItemClick={onKruimelItemClick}
      />
    </div>
    {plain ||
      <div className='x__about'>
        <p>
          Dit is de online editie van het woordenboek Grieks/Nederlands, een lexicon op Oudgriekse teksten van Homerus tot in de 2e eeuw n. Chr., onder hoofdredactie van Ineke Sluiter, Lucien van Beek, Ton Kessels en Albert Rijksbaron.
        </p>
        <p>
          Helaas is ons hoofdredactielid van het eerste uur, Albert Rijksbaron, op 2 oktober 2023 op 80-jarige leeftijd overleden.
        </p>
      </div>
    }
    <Div className='x__lemmaveld' ref={lemmaRef}>
      <H1M>Zoek een woord</H1M>
      <SectionM>
        <SearchFieldWrapperS>
          <SearchField
            type='lemma'
            initialValue={initialValueLemma}
            history={history}
            placeholder={placeholderLem}
            onFocus={onFocusLemma}
            onBlur={onBlurLemma}
            autoFocus={isMobile | not}
            onSelectResult={onSelectLemma}
          />
        </SearchFieldWrapperS>
      </SectionM>
    </Div>
    <Div className='x__parsveld' ref={parsRef}>
      <H1M>
        Determineer een vorm
      </H1M>
      <SectionM>
        <SearchFieldWrapperS>
          <SearchField
            type='pars'
            initialValue={initialValuePars}
            history={history}
            placeholder={placeholderPars}
            onFocus={onFocusPars}
            onBlur={onBlurPars}
            onSelectResult={onSelectPars}
          />
          <ParsResultsTable originatorKey='searchField'/>
        </SearchFieldWrapperS>
      </SectionM>
    </Div>
    {plain ||
      <Div className='x__keyboard' ref={hulpTypenRef}>
        <SectionM>
          <ClickableM
            onClick={onHulpClicked}
            className='u-cursor-pointer'
          >
            <div className='x__keyboard-contents'>
              <HulpHandle isOpen={keyboardMobileVisible}/>
              <LinkLikeTheme2>
                Hulp bij Grieks typen
              </LinkLikeTheme2>
            </div>
          </ClickableM>
        </SectionM>
      </Div>
    }
    {keyboardMobileVisible &&
      <div className='x__keyboard-m'>
        <KeyboardExplanation
          isMobile={isMobile}
          showClose={true}
          close={closeKeyboardDropdown}
          numLetterRows={6}
          numAccentCols={4}
        />
      </div>
    }
  </div>
</ZoekenMS>

const ClickableMS = styled.div`
  height: 48px;
  display: inline-block;
  > div {
    position: relative;
    top: 50%;
    transform: translateY(-50%);
  }
`
const ClickableM = ({ onClick, children, className, width, }) => <ClickableMS
  onClick={onClick}
  className={className}
  width={width}
>
  <div>{children}</div>
</ClickableMS>

const WrapperS = styled.div`
  ${mediaQuery (
    mediaTablet (`
      flex: 1 0 1px;
    `),
  )}
  display: flex;
  .x__over, .x__grieks {
    flex: 0 1 276px;
  }
  .x__over, .x__zoek, .x__grieks {
    padding-left: 10px;
    padding-right: 10px;
  }
  .x__over {
    ${mediaQuery (
      mediaPhone ('display: none;'),
      mediaTablet ('display: inherit;'),
    )}
  }
  .x__zoek {
    ${mediaQuery (
      mediaPhone (`
        flex: 1 0 1px;
        max-width: 100%;
        padding-left: 0px;
        padding-right: 0px;
      `),
      mediaTablet (`
        flex: 0 1 420px;
        padding-left: 25px;
      `),
    )}
  }
  .x__grieks {
    ${mediaQuery (
      mediaPhone ('display: none;'),
      mediaTablet ('display: inherit;'),
    )}
  }
`

const TopS = styled.div`
  height: 100%;
  padding-left: 9%;
  ${mediaQuery (
    mediaPhone (`
      padding-right: 9%;
      overflow-y: scroll;
    `),
    mediaTablet (`
      margin-right: 0px;
      overflow-y: auto;
      display: flex;
      flex-direction: column;
      justify-content: start;
    `),
  )}
`

const FooterWrapperS = styled.div`
  ${mediaQuery (
    mediaTablet (`
      flex: 0 0 50px;
    `),
  )}
  ${prop ('show') >> ifTrueV (
    'display: flex', 'display: none',
  )};
  margin-top: 40px;
  width: 100%;
`

class Home extends PureComponent {
  state = {
    keyboardMobileVisible: false,
  }

  theRefs = {
    top: createRef (),
    lemmaVeld: createRef (),
    parsVeld: createRef (),
    wrapper: createRef (),
    hulpTypen: createRef (),
  }

  scrollFieldToTop = (which) => this | deconstruct (
    ({ theRefs, }) => theRefs | deconstruct (
      ({ top, }) => [top, theRefs [which]]
      | map (prop ('current')) | deconstruct (
        ([topc, inputc]) => [topc, inputc]
        // --- both offset against AppWrapper.
        | map (prop ('offsetTop')) | deconstruct (
          ([topoff, inputoff]) => lets (
            () => inputoff - topoff,
            (top) => topc.scrollTo ({
              top,
              behavior: 'smooth',
            })
          ),
        ),
      ),
    )
  )

  // --- make WrapperS exactly high enough so that you can scroll to the pars field.
  // --- this should be made into a reusable function.

  enlargeWrapper = (whichRef) => this | deconstruct (
    ({ theRefs, }) => theRefs | deconstruct (
      ({ wrapper, top, }) => [wrapper, theRefs [whichRef], top]
      | map (prop ('current')) | deconstruct (
        ([wrapperc, parsc, topc]) => [wrapperc, parsc, topc]
        | map (prop ('offsetTop')) | deconstruct (
          ([wrapperoff, inputoff, topoff]) => wrapperc.style.height = lets (
            () => topc.offsetHeight,
            () => inputoff - topoff,
            (y, inputtop) => (y + inputtop) | sprintf1 ('%spx'),
          )
        )
      )
    ),
  )

  restoreWrapper = () => this | deconstruct (
    ({ theRefs, }) => theRefs | deconstruct (
      ({ wrapper, }) => wrapper.current.style.height = 'auto',
    ),
  )

  // --- don't do the intelligent placement of the input fields  on Safari, because they will scroll
  // too high.
  // --- on Safari the keyboard sort of pushes the page up before getting overlaid onto it (while on
  // other browsers it just gets overlaid), so it seems that on Safari the field stays visible
  // without us doing anything.

  // @todo chrome on android also claims to be Safari
  // @todo need a better solution which doesn't depend on browser sniffing.
  // @todo isSafari doesn't work: non-Safari browsers also return true.

  onFocusLemmaM = (plain) => () => {
    if (plain) return
    this.enlargeWrapper ('lemmaVeld')
    if (!isSafari ()) this.scrollFieldToTop ('lemmaVeld')
  }

  onFocusParsM = (plain) => () => this | compMeth ((
    { onParsFocus=noop, }
  ) => {
    onParsFocus ()
    if (plain) return
    this.enlargeWrapper ('parsVeld')
    if (!isSafari ()) this.scrollFieldToTop ('parsVeld')
  })

  onBlurLemmaM = (plain) => () => noop // this.restoreWrapper ()

  onBlurParsM = (plain) => () => this | compMeth ((
    { onParsBlur=noop, }
  ) => onParsBlur ())

  componentDidMount = () => this | compMeth ((
    { parsgetResultsClearDispatch, }) => {
    parsgetResultsClearDispatch('searchField')
  })

  onSelectLemmaM = (lemmaId, query, ..._) => this | compMeth ((
    { simpleQuerySetDispatch, },
    _,
    { onSelectLemma, },
  ) => {
    simpleQuerySetDispatch ('lemma', query)
    onSelectLemma (lemmaId)
  })

  onSelectParsM = (_, query, suggestion) => this | compMeth ((
    { simpleQuerySetDispatch, },
    _,
    { onSelectPars, },
  ) => {
    simpleQuerySetDispatch ('pars', query)
    onSelectPars (null, query, suggestion)
  })

  onSelectLemma = (lemmaId, query, ..._) => this | compMeth ((
    { history, },
  ) =>
    browseToId (history) (lemmaId),
  )

  onSelectPars = (_, query, suggestion) => this | compMeth ((
    { parsResultSelectedDispatch, },
  ) => {
    parsResultSelectedDispatch (suggestion)
  })

  showKeyboardMobile = () => true | this.setShowKeyboardMobile
  hideKeyboardMobile = () => false | this.setShowKeyboardMobile

  toggleKeyboardMobile = () => this | compMeth (
    (_, { keyboardMobileVisible, }) =>
      this.setShowKeyboardMobile (keyboardMobileVisible | not),
  )

  scrollToHulpTypen = () => {
    this.enlargeWrapper ('hulpTypen')
    this.scrollFieldToTop ('hulpTypen')
  }

  setShowKeyboardMobile = (val) => this | setState ({
    keyboardMobileVisible: val | tap (ifTrue (
      () => this.scrollToHulpTypen (),
      () => this.restoreWrapper ()
    ))
  })

  render = () => this | compMeth ((
    {
      history, isMobile,
      plain,
      parsResultSelectedDispatch,
      latestSimpleParsQuery,
      latestSimpleLemmaQuery,
      onKruimelItemClick=onKruimelItemClickStandard (history),
    },
    {
      keyboardMobileVisible,
    },
    {
      theRefs, onFocusLemmaM, onFocusParsM, onBlurLemmaM, onBlurParsM,
      onSelectLemmaM, onSelectParsM,
      onSelectLemma, onSelectPars,
      toggleKeyboardMobile, hideKeyboardMobile,
    },
  ) => theRefs | deconstruct (
      ({
        top: topRef, lemmaVeld: lemmaRef,
        parsVeld: parsRef, wrapper: wrapperRef,
        hulpTypen: hulpTypenRef,
      }) => lets (
        () => isMobile ?
        [
          ZoekenM, plain,
          plain ? latestSimpleLemmaQuery : null,
          plain ? latestSimpleParsQuery : null,
          onSelectLemmaM, onSelectParsM,
          toggleKeyboardMobile,
          onFocusLemmaM (plain), onFocusParsM (plain),
          onBlurLemmaM (plain), onBlurParsM (plain),
        ] :
        [
          Zoeken, null,
          null, null,
          onSelectLemma, onSelectPars,
          noop,
          noop, noop, noop, noop,
        ],
        ([
          ZoekComponent, plain,
          initialValueLemma, initialValuePars,
          onSelectLemma, onSelectPars,
          onHulpClicked,
          onFocusLemma, onFocusPars, onBlurLemma, onBlurPars,
        ]) =>
          <TopS ref={topRef}>
            <WrapperS ref={wrapperRef}>
              <div className='x__over'>
                <OverHetWoordenboek/>
              </div>
              <div className='x__zoek'>
                <ZoekComponent
                  history={history}
                  isMobile={isMobile}
                  plain={plain}
                  initialValueLemma={initialValueLemma}
                  initialValuePars={initialValuePars}
                  onSelectLemma={onSelectLemma}
                  onSelectPars={onSelectPars}
                  parsResultSelectedDispatch={parsResultSelectedDispatch}
                  onHulpClicked={onHulpClicked}
                  lemmaRef={lemmaRef}
                  parsRef={parsRef}
                  hulpTypenRef={hulpTypenRef}
                  onFocusLemma={onFocusLemma}
                  onFocusPars={onFocusPars}
                  onBlurLemma={onBlurLemma}
                  onBlurPars={onBlurPars}
                  keyboardMobileVisible={keyboardMobileVisible}
                  closeKeyboardDropdown={hideKeyboardMobile}
                  onKruimelItemClick={onKruimelItemClick}
                />
              </div>
              <div className='x__grieks'>
                <GrieksTypen toetsenbordLocation='rechtsboven'/>
              </div>
            </WrapperS>
            <FooterWrapperS show={plain | not}>
              <Footer isMobile={isMobile}/>
            </FooterWrapperS>
          </TopS>
      )
    )
  )
}

const mapStateToProps = createStructuredSelector ({
  // --- can be null.
  latestSimpleParsQuery: makeSelectLatestSimpleParsQuery (),
  latestSimpleLemmaQuery: makeSelectLatestSimpleLemmaQuery (),
})

const mapDispatchToProps = (dispatch) => ({
  parsResultSelectedDispatch: parsResultSelected >> dispatch,
  simpleQuerySetDispatch: simpleQuerySet >> dispatch,
  parsgetResultsClearDispatch: parsgetResultsClear >> dispatch,
})

export default Home
  | connect       (mapStateToProps, mapDispatchToProps)
  | injectSaga    ({ saga, key: 'home', })
