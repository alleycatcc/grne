// --- note: we nearly always use the general reducers, not the per-component ones.

const initialState = {}

export default (state = initialState, action) => {
  switch (action.type) {
    default:
      return state
  }
}





