import {
  pipe, compose, composeRight,
} from 'stick-js/es'

import { all, call, put, select, takeLatest, take, } from 'redux-saga/effects'

import {
  PARS_RESULT_SELECTED,
} from '../../containers/App/constants'

import {
  parsgetFetch,
} from '../App/actions'

function *sagaParsResultSelected ({ data: word, }) {
  yield parsgetFetch ('searchField', word) | put
}

export default function *homeSaga () {
  yield all ([
    takeLatest (PARS_RESULT_SELECTED, sagaParsResultSelected),
  ])
}
