import {
  pipe, compose, composeRight, sprintf1
} from 'stick-js/es'

import { createGlobalStyle, } from 'styled-components'

import configure from 'alleycat-js/es/configure'

import config from './config'

const configTop = config | configure.init
const fontMainFamily = 'font.main.family' | configTop.get

// --- this has now been placed directly in index.html, since createGlobalStyle doesn't handle it
// well anyway.
// const extUrl = 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,700,700i&display=swap&subset=greek,greek-ext'

// --- remember to use normal syntax for injecting variables (normal strings, not functions).

export const GlobalStyle = createGlobalStyle`
  // --- note: on mobile, height = 100vh includes url bar height and soft buttons, while 100%
  // doesn't.

  html, body {
    height: 100%;
    width: 100%;
    position: fixed;
    overflow: hidden;
  }

  html, body, #app {
    // --- what was this for?
    // overscroll-behavior: none;
  }

  body {
    font-family: ${fontMainFamily};
    color: #333;
  }

  h1, h2, h3, h4, p {
    cursor: text;
  }

  #app {
    height: 100%;
    width: 100%;
  }

  .x--mobile {
    .m--hide {
      display: none;
    }
  }

  .u-cursor-pointer {
    cursor: pointer !important;
  }

  .u--display-none {
    display: none !important;
  }

  .u--hide {
    visibility: hidden !important;
  }
`
