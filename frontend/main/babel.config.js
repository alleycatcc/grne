module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        modules: false,
        // --- remember to import core-js and regenerator runtime at the very beginning of app.js
        useBuiltIns: 'entry',
        // --- yarn add core-js@3
        corejs: 3,
      },
    ],
    '@babel/preset-react',
  ],
  plugins: [
    'styled-components',
    'alleycat-stick-transforms',
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-proposal-function-bind',
    '@babel/plugin-proposal-nullish-coalescing-operator',
    '@babel/plugin-proposal-optional-chaining',
    '@babel/plugin-syntax-dynamic-import',
  ],
  env: {
    production: {
      only: ['app'],
      plugins: [
        '@babel/plugin-transform-react-inline-elements',
      ],
    },
  },
}
