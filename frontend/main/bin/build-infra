#!/usr/bin/env bash

set -eu
set -o pipefail

# --- keeps colors on when running background processes
set -m

bindir=$(realpath "$(dirname "$0")")
rootdir="$bindir"/..
toolsdir="$rootdir"/tools
internalsdir="$rootdir"/internals
nodemodulesdir="$rootdir"/node_modules
nodemodulesbindir="$nodemodulesdir"/.bin
babelcmd="$nodemodulesbindir"/babel

. "$bindir"/functions.bash

USAGE="Usage: $0 [-w]"

opt_w=
while getopts hw-: arg; do
    case $arg in
        h) warn "$USAGE"; exit 0 ;;
        w) opt_w=yes ;;
        -) OPTARG_VALUE="${OPTARG#*=}"
            case $OPTARG in
                help)  warn "$USAGE"; exit 0 ;;
                '')    break ;;
                *)     error "Illegal option --$OPTARG" ;;
                esac ;;
        *) error "$USAGE" ;;
    esac
done
shift $((OPTIND-1))

_ret=
pidtools=
pidinternals=

babel-cmd () {
    local watch=$1; shift
    local opts=()
    if [ "$watch" = yes ]; then
        opts+=(-w)
    fi
    cmd "$babelcmd" "${opts[@]}"  "$@"
}

build-tools () {
    local ret=$1; shift
    local watch=$1; shift
    cwd "$toolsdir" forkit fun babel-cmd "$watch" -d lib src
    local pid=$!
    retvar "$ret" "$pid"
}

build-internals () {
    local ret=$1; shift
    local watch=$1; shift
    stack-push-xport BABEL_ENV cjs
    cwd "$internalsdir" forkit fun babel-cmd "$watch" -d lib src
    local pid=$!
    stack-pop BABEL_ENV
    cpa "$internalsdir"/src/generators/component/*.hbs "$internalsdir"/lib/generators/component
    cpa "$internalsdir"/src/generators/container/*.hbs "$internalsdir"/lib/generators/container
    retvar "$ret" "$pid"
}

go () {
    local watch=$1

    info Building tools
    fun build-tools _ret "$watch"
    pidtools=$_ret

    info Building internals
    fun build-internals _ret "$watch"
    pidinternals=$_ret

    if [ "$watch" = yes ]; then fun watch-wait; fi
}

watch-wait () {
    trap killem EXIT
    info "Ctrl-c to quit"
    while true; do sleep 1; done
}

killem () {
    local i; for i in "$pidtools" "$pidinternals"; do
        if [ ! -z "$i" ]; then cmd kill "$i"; fi
    done
}

fun go "$opt_w"
