devDependencies: babel-related, webpack-related, linters, and anything used
only by tools/internals/dev-server.

dependencies: everything else.

react-app-polyfill is currently only used in webpack.dev.babel for the hot reloader.
