#!/usr/bin/env node

import fs from 'fs'
import {
  pipe, compose, composeRight,
  tap, map, join, dot2, condS, guard, sprintf1, sprintfN,
  prop, eq, neu, deconstruct, deconstruct2, appendM, ifYes, repeatV,
} from 'stick-js/es'

import fishLib from 'fish-lib'
const { log, info, green, brightRed, sysSpawn, bulletSet, } = fishLib

import walkerMod from 'node-source-walk'

import { dirname, } from 'node:path'
import { fileURLToPath, } from 'node:url'
const __dirname = fileURLToPath >> dirname

bulletSet ({ type: 'star', })

// --- dies
const getFilenamesCmd = () => sysSpawn (
  'find',
  [__dirname (import.meta.url) + '/../../app', '-name', '*.js'],
  {
    sync: true,
    outSplit: true,
  },
)

const getFilenames = getFilenamesCmd >> prop ('out')

const getSource = (path) => fs.readFileSync (path).toString ()

const underline = sprintf1 ('\u001b[4m%s\u001b[24m')

const indent = n => s => n | repeatV ('  ') | appendM (s) | join ('')

const onEntry = green >> info
const onImport = underline >> indent (1) >> log
const onDefaultImport = s => ['default' | brightRed, s] | sprintfN ('%s as %s') | indent (2) | log
const onNamedImport = ([local, imported]) => local | eq (imported) | ifYes (
  () => imported | indent (2) | log,
  () => [imported, local] | sprintfN ('%s as %s') | indent (2) | log,
)

const makeWalker = walkerMod | neu
const walk = 'walk' | dot2
const walkSource = (source) => makeWalker
  | walk (
    source,
    deconstruct2 (
      ({ type, }) => node => {
        // return node | inspect | log
        type | condS ([
          'ImportDeclaration' | eq | guard (
            () => node | deconstruct (
              ({ source: { value, }}) => value | onImport,
            ),
          ),
          'ImportDefaultSpecifier' | eq | guard (
            () => node | deconstruct (
              ({ local: { name, }}) => name | onDefaultImport,
            ),
          ),
          'ImportSpecifier' | eq | guard (
            () => node | deconstruct (
              ({ local, imported, }) => [local, imported]
              | map (prop ('name'))
              | onNamedImport,
            ),
          )
        ])
      },
    ),
  )

const walkFile = (entry) => entry
  | getSource
  | walkSource

const go = (entries) => entries | map (
  (entry) => entry | tap (onEntry) | walkFile
)

go (getFilenames ())
