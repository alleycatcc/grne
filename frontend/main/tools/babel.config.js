const config = {
  presets: [
    [
      '@babel/env',
      {
        modules: false,
      },
    ],
  ],
  plugins: [
    // --- reduce code size and avoid namespace pollution (e.g. global
    // polyfills; be sure to add @babel/runtime to runtime deps)
    '@babel/transform-runtime',
    'alleycat-stick-transforms',
  ],
}

module.exports = (api) => {
  api.cache.forever ()
  return config
}
