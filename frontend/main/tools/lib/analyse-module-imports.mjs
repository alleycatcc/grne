#!/usr/bin/env node
import _slicedToArray from "@babel/runtime/helpers/slicedToArray";
import fs from 'fs';
import { pipe, compose, composeRight, tap, map, join, dot2, condS, guard, sprintf1, sprintfN, prop, eq, neu, deconstruct, deconstruct2, appendM, ifYes, repeatV } from 'stick-js/es';
import fishLib from 'fish-lib';
var log = fishLib.log,
    info = fishLib.info,
    green = fishLib.green,
    brightRed = fishLib.brightRed,
    sysSpawn = fishLib.sysSpawn,
    bulletSet = fishLib.bulletSet;
import walkerMod from 'node-source-walk';
import { dirname } from 'node:path';
import { fileURLToPath } from 'node:url';

var __dirname = composeRight(fileURLToPath, dirname);

bulletSet({
  type: 'star'
}); // --- dies

var getFilenamesCmd = function getFilenamesCmd() {
  return sysSpawn('find', [__dirname(import.meta.url) + '/../../app', '-name', '*.js'], {
    sync: true,
    outSplit: true
  });
};

var getFilenames = composeRight(getFilenamesCmd, prop('out'));

var getSource = function getSource(path) {
  return fs.readFileSync(path).toString();
};

var underline = sprintf1("\x1B[4m%s\x1B[24m");

var indent = function indent(n) {
  return function (s) {
    return pipe(pipe(pipe(n, repeatV('  ')), appendM(s)), join(''));
  };
};

var onEntry = composeRight(green, info);
var onImport = composeRight(composeRight(underline, indent(1)), log);

var onDefaultImport = function onDefaultImport(s) {
  return pipe(pipe(pipe([pipe('default', brightRed), s], sprintfN('%s as %s')), indent(2)), log);
};

var onNamedImport = function onNamedImport(_ref) {
  var _ref2 = _slicedToArray(_ref, 2),
      local = _ref2[0],
      imported = _ref2[1];

  return pipe(pipe(local, eq(imported)), ifYes(function () {
    return pipe(pipe(imported, indent(2)), log);
  }, function () {
    return pipe(pipe(pipe([imported, local], sprintfN('%s as %s')), indent(2)), log);
  }));
};

var makeWalker = pipe(walkerMod, neu);
var walk = pipe('walk', dot2);

var walkSource = function walkSource(source) {
  return pipe(makeWalker, walk(source, deconstruct2(function (_ref3) {
    var type = _ref3.type;
    return function (node) {
      // return node | inspect | log
      pipe(type, condS([pipe(pipe('ImportDeclaration', eq), guard(function () {
        return pipe(node, deconstruct(function (_ref4) {
          var value = _ref4.source.value;
          return pipe(value, onImport);
        }));
      })), pipe(pipe('ImportDefaultSpecifier', eq), guard(function () {
        return pipe(node, deconstruct(function (_ref5) {
          var name = _ref5.local.name;
          return pipe(name, onDefaultImport);
        }));
      })), pipe(pipe('ImportSpecifier', eq), guard(function () {
        return pipe(node, deconstruct(function (_ref6) {
          var local = _ref6.local,
              imported = _ref6.imported;
          return pipe(pipe([local, imported], map(prop('name'))), onNamedImport);
        }));
      }))]));
    };
  })));
};

var walkFile = function walkFile(entry) {
  return pipe(pipe(entry, getSource), walkSource);
};

var go = function go(entries) {
  return pipe(entries, map(function (entry) {
    return pipe(pipe(entry, tap(onEntry)), walkFile);
  }));
};

go(getFilenames());