// ------ this is used by the dev server (i.e. yarn/npm run start)


import {
  pipe, compose, composeRight,
} from 'stick-js'

import path from 'path'
import fs from 'fs'

import glob from 'glob'
import webpack from 'webpack'

import CircularDependencyPlugin from 'circular-dependency-plugin'
import HtmlWebpackPlugin from 'html-webpack-plugin'

import base from './webpack.base.babel'

const plugins = [
  new webpack.HotModuleReplacementPlugin (),
  new HtmlWebpackPlugin ({
    inject: true,
    template: 'app/index.html',
  }),
  new CircularDependencyPlugin ({
    // --- a.js: it seems you can name a module e.g. fooa.js to bypass this check if necessary.
    exclude: /a\.js|node_modules/,
    // --- only warn
    failOnError: false,
  }),
]

module.exports = base ({
  plugins,

  mode: 'development',

  entry: [
    // --- @todo not sure this is necessary -- we are also loading polyfills in app.js, and we are
    // not doing this step in the webpack.prod config.
    require.resolve('react-app-polyfill/ie11'),

    // --- probably only necessary for ie11, and even then, react-app-polyfill/ie11 will probably
    // have already loaded it.
    'whatwg-fetch',

    'webpack-hot-middleware/client?reload=true',
    path.join (process.cwd (), 'app/app.js'),
  ],

  // --- react-boilerplate:
  // Don't use hashes in dev mode for better performance
  output: {
    filename: '[name].js',
    chunkFilename: '[name].chunk.js',
  },

  optimization: {
    splitChunks: {
      chunks: 'all',
    },
  },

  devtool: 'eval-source-map',

  performance: {
    hints: false,
  },
})
