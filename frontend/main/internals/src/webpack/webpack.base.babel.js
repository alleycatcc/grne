import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, recurry,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, blush, always, T, F,
  prop, path,
  concat,
  lets, letS,
  die, eq, ne,
  factory, factoryProps,
  mergeTo,
} from 'stick-js'

// --- old, but may require tweaking or reviving in the case of things like the passphrases app
// (i.e. a bundle retrieved via file:/// urls, a webview component, etc.)
// const useRelativePath = process.env.WEBPACK_TYPE === 'build'

import pathMod from 'path'

import webpack from 'webpack'

const basePlugins = [
  new webpack.DefinePlugin ({
    'process.env': {
      GRNE_ENV: JSON.stringify (process.env.GRNE_ENV),
    },
  }),
]

// type: javascript/esm

export default ({
  mode, entry, output, optimization, plugins, devtool, performance={},
  babelOptions={},
}) => ({
  mode,
  entry,
  optimization,
  devtool,
  performance,

  // --- gives us `window` var
  target: 'web',

  output: output | mergeTo ({
    path: pathMod.resolve (process.cwd (), 'build'),
    publicPath: '/',
  }),

  plugins: plugins | concat (basePlugins),

  resolve: {
    // --- @todo consider removing app: we use full relative paths on imports anyway.
    modules: ['node_modules', 'app'],
    extensions: ['.js', '.jsx'],
    mainFields: ['browser', 'jsnext:main', 'main'],
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: babelOptions,
        },
      },

      // --- these node_modules use 'const', so we transpile them.
      // --- added this to support an ancient version of Safari (which we don't support any more).
      // --- not sure how IE ever worked without this.
      {
        test: /memoize-immutable\/.+\.js$/,
        use: {
          loader: 'babel-loader',
          options: {},
        },
      },
      {
        test: /tuplemap\/.+\.js$/,
        use: {
          loader: 'babel-loader',
          options: {},
        },
      },

      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(eot|otf|ttf|woff|woff2|ogg|mp3)$/,
        type: 'asset',
      },
      {
        test: /favicon\.ico$/,
        type: 'asset/resource',
        generator: { filename: '[name][ext]' },
      },
      {
        test: /icons-\d+\.png$/,
        type: 'asset/resource',
        generator: { filename: '[name][ext]' },
      },
      {
        test: /\.(cur|png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
      },
      {
        test: /manifest\.json/,
        type: 'asset/resource',
        exclude: /node_modules/,
        generator: { filename: 'manifest.json' },
      },
    ],
  },
})
