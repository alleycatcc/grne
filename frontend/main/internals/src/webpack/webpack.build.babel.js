// ------ this is used by the build process (i.e. yarn/npm run build ...)


import {
  pipe, compose, composeRight,
  tap, concatTo, lets,
  dot2, xMatchStr, prop,
  ifNo,
} from 'stick-js'

import pathMod from 'path'

import CompressionPlugin from 'compression-webpack-plugin'
import HtmlWebpackPlugin from 'html-webpack-plugin'

import base from './webpack.base.babel'

const replace = 'replace' | dot2

const { GRNE_ENV, } = process.env

const plugins = [
  new HtmlWebpackPlugin ({
    template: 'app/index.html',
    minify: {
      removeComments: true,
      collapseWhitespace: true,
      removeRedundantAttributes: true,
      useShortDoctype: true,
      removeEmptyAttributes: true,
      removeStyleLinkTypeAttributes: true,
      keepClosingSlash: true,
      minifyJS: true,
      minifyCSS: true,
      minifyURLs: true,
    },
    inject: true,
  }),

  // --- necessary? the server is set to gzip responses as well; probably doesn't have much effect
  // to do it twice.
  new CompressionPlugin ({
	algorithm: 'gzip',
	test: /\.js$|\.css$|\.html$/,
	threshold: 10240,
	minRatio: 0.8,
  }),
]

module.exports = base ({
  plugins,

  // --- automatically configures DefinePlugin and sets NODE_ENV to production.
  mode: 'production',

  entry: [
    // --- probably only necessary for ie11.
    'whatwg-fetch',

    pathMod.join (process.cwd (), 'app/app.js'),
  ],

  // --- react-boilerplate
  // use content hash ('chunkhash') instead of compilation hash to improve caching.
  output: {
    filename: '[name].[chunkhash].js',
    chunkFilename: '[name].[chunkhash].chunk.js',
  },

  optimization: {
    minimize: true,
    nodeEnv: 'production',
    sideEffects: true,
    concatenateModules: true,
    runtimeChunk: 'single',
    moduleIds: 'deterministic',
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: 10,
      minSize: 0,
      cacheGroups: {
        defaultVendors: {
          test: RegExp ('/node_modules/'),
          // --- given ramda/a, ramda/b, ramda, make a chunk called
          // npm.ramda.[hash].chunk.js[.gz]
          name: ({ context, }) => lets (
            () => context | xMatchStr (
              '/node_modules/ (.*?) (/|$)',
            ),
            (packageMatch) => packageMatch | prop (1) | replace ('@', ''),
            (_, x) => x | concatTo ('npm.'),
          ),
        },
      },
    },
  },

  devtool: GRNE_ENV === 'tst' && 'eval-source-map',

  performance: {
    assetFilter: assetFilename =>
      !/(\.map$)|(^(main\.|favicon\.))/.test (assetFilename),
  },
})
