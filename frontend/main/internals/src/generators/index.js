import {
  pipe, compose, composeRight,
  side2,
  ifPredicate, passToN, concatTo,
} from 'stick-js'

import fs from 'fs'
import path from 'path'

import componentGenerator from './component'
import containerGenerator from './container'

const setGenerator = side2 ('setGenerator')
const addHelper = side2 ('addHelper')

const relPath = '../../../app/containers'
const containerPath = concatTo ([__dirname, relPath]) >> pathJoinN
const pathJoinN = passToN (path.join)
const exists = fs.existsSync
const ifExists = exists | ifPredicate

const getDirectory = (stub) => stub | containerPath | ifExists (
  _ => 'containers/' + stub,
  _ => 'components/' + stub,
)

module.exports = (plop, cfg) => plop
  | setGenerator ('component', componentGenerator)
  | setGenerator ('container', containerGenerator)
  | addHelper ('directory', getDirectory)
  | addHelper ('curly', (object, open) => (open ? '{' : '}'))
