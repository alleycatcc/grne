import {
  pipe, compose, composeRight,
  guardV, otherwise, eq, condS,
  guard, T,
} from 'stick-js'

import componentExists from '../utils/componentExists'

const relPath = '../../..'
const appPath = relPath + '/app'

module.exports = {
  description: 'Add a component',
  prompts: [{
    type: 'list',
    name: 'type',
    message: 'Select the type of component',
    default: 'Stateless Function',
    choices: () => ['Stateless Function', 'React.PureComponent'],
  }, {
    type: 'input',
    name: 'name',
    message: 'What should it be called?',
    default: 'Button',
    validate: value => value | condS ([
      '' | eq | guardV ('The name is required'),
      componentExists | guardV ('A component or container with this name already exists'),
      otherwise | guard (T),
    ])
  }, {
    type: 'confirm',
    name: 'wantLoadable',
    default: false,
    message: 'Do you want to load the component asynchronously?',
  }],
  actions: (data) => {
    const componentTemplate = data.type | condS ([
      'Stateless Function' | eq | guardV ('./component/stateless.js.hbs'),
      otherwise                 | guardV ('./component/class.js.hbs'),
    ])

    const actions = [{
      type: 'add',
      path: appPath + '/components/{{properCase name}}/index.js',
      templateFile: componentTemplate,
      abortOnFail: true,
    }]

    actions.push ({
      type: 'add',
      path: appPath + '/components/{{properCase name}}/messages.js',
      templateFile: './component/messages.js.hbs',
      abortOnFail: true,
    })

    if (data.wantLoadable) {
      actions.push ({
        type: 'add',
        path: appPath + '/components/{{properCase name}}/Loadable.js',
        templateFile: './component/loadable.js.hbs',
        abortOnFail: true,
      })
    }

    return actions
  },
}
