import {
  pipe, compose, composeRight,
  guardV, otherwise, eq, condS,
  guard, T,
} from 'stick-js'

import componentExists from '../utils/componentExists'

const relPath = '../../..'
const appPath = relPath + '/app'

module.exports = {
  description: 'Add a container',
  prompts: [{
    type: 'input',
    name: 'name',
    message: 'What should it be called?',
    default: 'Button',
    validate: value => value | condS ([
      '' | eq | guardV ('The name is required'),
      componentExists | guardV ('A component or container with this name already exists'),
      otherwise | guard (T),
    ])
  }, {
    type: 'confirm',
    name: 'wantLoadable',
    default: true,
    message: 'Do you want to load resources asynchronously?',
  }],
  actions: (data) => {
    const componentTemplate = data.type | condS ([
      'Stateless Function' | eq | guardV ('./container/stateless.js.hbs'),
      otherwise                 | guardV ('./container/class.js.hbs'),
    ])

    const actions = [{
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/index.js',
      templateFile: componentTemplate,
      abortOnFail: true,
    }]

    actions.push ({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/messages.js',
      templateFile: './container/messages.js.hbs',
      abortOnFail: true,
    })

    actions.push ({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/actions.js',
      templateFile: './container/actions.js.hbs',
      abortOnFail: true,
    })

    actions.push ({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/tests/actions.test.js',
      templateFile: './container/actions.test.js.hbs',
      abortOnFail: true,
    })

    actions.push ({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/constants.js',
      templateFile: './container/constants.js.hbs',
      abortOnFail: true,
    })

    actions.push ({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/selectors.js',
      templateFile: './container/selectors.js.hbs',
      abortOnFail: true,
    })

    actions.push ({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/tests/selectors.test.js',
      templateFile: './container/selectors.test.js.hbs',
      abortOnFail: true,
    })

    actions.push ({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/reducer.js',
      templateFile: './container/reducer.js.hbs',
      abortOnFail: true,
    })

    actions.push ({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/tests/reducer.test.js',
      templateFile: './container/reducer.test.js.hbs',
      abortOnFail: true,
    })

    actions.push ({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/saga.js',
      templateFile: './container/saga.js.hbs',
      abortOnFail: true,
    })

    actions.push ({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/tests/saga.test.js',
      templateFile: './container/saga.test.js.hbs',
      abortOnFail: true,
    })

    if (data.wantLoadable) {
      actions.push ({
        type: 'add',
        path: appPath + '/containers/{{properCase name}}/Loadable.js',
        templateFile: './component/loadable.js.hbs',
        abortOnFail: true,
      })
    }

    return actions
  },
}
