"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _stickJs = require("stick-js");

var _path = _interopRequireDefault(require("path"));

var _compressionWebpackPlugin = _interopRequireDefault(require("compression-webpack-plugin"));

var _htmlWebpackPlugin = _interopRequireDefault(require("html-webpack-plugin"));

var _webpackBase = _interopRequireDefault(require("./webpack.base.babel"));

// ------ this is used by the build process (i.e. yarn/npm run build ...)
var replace = (0, _stickJs.pipe)('replace', _stickJs.dot2);
var GRNE_ENV = process.env.GRNE_ENV;
var plugins = [new _htmlWebpackPlugin["default"]({
  template: 'app/index.html',
  minify: {
    removeComments: true,
    collapseWhitespace: true,
    removeRedundantAttributes: true,
    useShortDoctype: true,
    removeEmptyAttributes: true,
    removeStyleLinkTypeAttributes: true,
    keepClosingSlash: true,
    minifyJS: true,
    minifyCSS: true,
    minifyURLs: true
  },
  inject: true
}), // --- necessary? the server is set to gzip responses as well; probably doesn't have much effect
// to do it twice.
new _compressionWebpackPlugin["default"]({
  algorithm: 'gzip',
  test: /\.js$|\.css$|\.html$/,
  threshold: 10240,
  minRatio: 0.8
})];
module.exports = (0, _webpackBase["default"])({
  plugins: plugins,
  // --- automatically configures DefinePlugin and sets NODE_ENV to production.
  mode: 'production',
  entry: [// --- probably only necessary for ie11.
  'whatwg-fetch', _path["default"].join(process.cwd(), 'app/app.js')],
  // --- react-boilerplate
  // use content hash ('chunkhash') instead of compilation hash to improve caching.
  output: {
    filename: '[name].[chunkhash].js',
    chunkFilename: '[name].[chunkhash].chunk.js'
  },
  optimization: {
    minimize: true,
    nodeEnv: 'production',
    sideEffects: true,
    concatenateModules: true,
    runtimeChunk: 'single',
    moduleIds: 'deterministic',
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: 10,
      minSize: 0,
      cacheGroups: {
        defaultVendors: {
          test: RegExp('/node_modules/'),
          // --- given ramda/a, ramda/b, ramda, make a chunk called
          // npm.ramda.[hash].chunk.js[.gz]
          name: function name(_ref) {
            var context = _ref.context;
            return (0, _stickJs.lets)(function () {
              return (0, _stickJs.pipe)(context, (0, _stickJs.xMatchStr)('/node_modules/ (.*?) (/|$)'));
            }, function (packageMatch) {
              return (0, _stickJs.pipe)((0, _stickJs.pipe)(packageMatch, (0, _stickJs.prop)(1)), replace('@', ''));
            }, function (_, x) {
              return (0, _stickJs.pipe)(x, (0, _stickJs.concatTo)('npm.'));
            });
          }
        }
      }
    }
  },
  devtool: GRNE_ENV === 'tst' && 'eval-source-map',
  performance: {
    assetFilter: function assetFilter(assetFilename) {
      return !/(\.map$)|(^(main\.|favicon\.))/.test(assetFilename);
    }
  }
});