"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _stickJs = require("stick-js");

var _path = _interopRequireDefault(require("path"));

var _webpack = _interopRequireDefault(require("webpack"));

// --- old, but may require tweaking or reviving in the case of things like the passphrases app
// (i.e. a bundle retrieved via file:/// urls, a webview component, etc.)
// const useRelativePath = process.env.WEBPACK_TYPE === 'build'
var basePlugins = [new _webpack["default"].DefinePlugin({
  'process.env': {
    GRNE_ENV: JSON.stringify(process.env.GRNE_ENV)
  }
})]; // type: javascript/esm

var _default = function _default(_ref) {
  var mode = _ref.mode,
      entry = _ref.entry,
      output = _ref.output,
      optimization = _ref.optimization,
      plugins = _ref.plugins,
      devtool = _ref.devtool,
      _ref$performance = _ref.performance,
      performance = _ref$performance === void 0 ? {} : _ref$performance,
      _ref$babelOptions = _ref.babelOptions,
      babelOptions = _ref$babelOptions === void 0 ? {} : _ref$babelOptions;
  return {
    mode: mode,
    entry: entry,
    optimization: optimization,
    devtool: devtool,
    performance: performance,
    // --- gives us `window` var
    target: 'web',
    output: (0, _stickJs.pipe)(output, (0, _stickJs.mergeTo)({
      path: _path["default"].resolve(process.cwd(), 'build'),
      publicPath: '/'
    })),
    plugins: (0, _stickJs.pipe)(plugins, (0, _stickJs.concat)(basePlugins)),
    resolve: {
      // --- @todo consider removing app: we use full relative paths on imports anyway.
      modules: ['node_modules', 'app'],
      extensions: ['.js', '.jsx'],
      mainFields: ['browser', 'jsnext:main', 'main']
    },
    module: {
      rules: [{
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: babelOptions
        }
      }, // --- these node_modules use 'const', so we transpile them.
      // --- added this to support an ancient version of Safari (which we don't support any more).
      // --- not sure how IE ever worked without this.
      {
        test: /memoize-immutable\/.+\.js$/,
        use: {
          loader: 'babel-loader',
          options: {}
        }
      }, {
        test: /tuplemap\/.+\.js$/,
        use: {
          loader: 'babel-loader',
          options: {}
        }
      }, {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }, {
        test: /\.(eot|otf|ttf|woff|woff2|ogg|mp3)$/,
        type: 'asset'
      }, {
        test: /favicon\.ico$/,
        type: 'asset/resource',
        generator: {
          filename: '[name][ext]'
        }
      }, {
        test: /icons-\d+\.png$/,
        type: 'asset/resource',
        generator: {
          filename: '[name][ext]'
        }
      }, {
        test: /\.(cur|png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource'
      }, {
        test: /manifest\.json/,
        type: 'asset/resource',
        exclude: /node_modules/,
        generator: {
          filename: 'manifest.json'
        }
      }]
    }
  };
};

exports["default"] = _default;