"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _stickJs = require("stick-js");

var _componentExists = _interopRequireDefault(require("../utils/componentExists"));

var relPath = '../../..';
var appPath = relPath + '/app';
module.exports = {
  description: 'Add a container',
  prompts: [{
    type: 'input',
    name: 'name',
    message: 'What should it be called?',
    "default": 'Button',
    validate: function validate(value) {
      return (0, _stickJs.pipe)(value, (0, _stickJs.condS)([(0, _stickJs.pipe)((0, _stickJs.pipe)('', _stickJs.eq), (0, _stickJs.guardV)('The name is required')), (0, _stickJs.pipe)(_componentExists["default"], (0, _stickJs.guardV)('A component or container with this name already exists')), (0, _stickJs.pipe)(_stickJs.otherwise, (0, _stickJs.guard)(_stickJs.T))]));
    }
  }, {
    type: 'confirm',
    name: 'wantLoadable',
    "default": true,
    message: 'Do you want to load resources asynchronously?'
  }],
  actions: function actions(data) {
    var componentTemplate = (0, _stickJs.pipe)(data.type, (0, _stickJs.condS)([(0, _stickJs.pipe)((0, _stickJs.pipe)('Stateless Function', _stickJs.eq), (0, _stickJs.guardV)('./container/stateless.js.hbs')), (0, _stickJs.pipe)(_stickJs.otherwise, (0, _stickJs.guardV)('./container/class.js.hbs'))]));
    var actions = [{
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/index.js',
      templateFile: componentTemplate,
      abortOnFail: true
    }];
    actions.push({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/messages.js',
      templateFile: './container/messages.js.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/actions.js',
      templateFile: './container/actions.js.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/tests/actions.test.js',
      templateFile: './container/actions.test.js.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/constants.js',
      templateFile: './container/constants.js.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/selectors.js',
      templateFile: './container/selectors.js.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/tests/selectors.test.js',
      templateFile: './container/selectors.test.js.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/reducer.js',
      templateFile: './container/reducer.js.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/tests/reducer.test.js',
      templateFile: './container/reducer.test.js.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/saga.js',
      templateFile: './container/saga.js.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'add',
      path: appPath + '/containers/{{properCase name}}/tests/saga.test.js',
      templateFile: './container/saga.test.js.hbs',
      abortOnFail: true
    });

    if (data.wantLoadable) {
      actions.push({
        type: 'add',
        path: appPath + '/containers/{{properCase name}}/Loadable.js',
        templateFile: './component/loadable.js.hbs',
        abortOnFail: true
      });
    }

    return actions;
  }
};