"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _stickJs = require("stick-js");

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireDefault(require("path"));

var _component = _interopRequireDefault(require("./component"));

var _container = _interopRequireDefault(require("./container"));

var setGenerator = (0, _stickJs.side2)('setGenerator');
var addHelper = (0, _stickJs.side2)('addHelper');
var relPath = '../../../app/containers';
var containerPath = (0, _stickJs.composeRight)((0, _stickJs.concatTo)([__dirname, relPath]), pathJoinN);
var pathJoinN = (0, _stickJs.passToN)(_path["default"].join);
var exists = _fs["default"].existsSync;
var ifExists = (0, _stickJs.pipe)(exists, _stickJs.ifPredicate);

var getDirectory = function getDirectory(stub) {
  return (0, _stickJs.pipe)((0, _stickJs.pipe)(stub, containerPath), ifExists(function (_) {
    return 'containers/' + stub;
  }, function (_) {
    return 'components/' + stub;
  }));
};

module.exports = function (plop, cfg) {
  return (0, _stickJs.pipe)((0, _stickJs.pipe)((0, _stickJs.pipe)((0, _stickJs.pipe)(plop, setGenerator('component', _component["default"])), setGenerator('container', _container["default"])), addHelper('directory', getDirectory)), addHelper('curly', function (object, open) {
    return open ? '{' : '}';
  }));
};