"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _stickJs = require("stick-js");

var _componentExists = _interopRequireDefault(require("../utils/componentExists"));

var relPath = '../../..';
var appPath = relPath + '/app';
module.exports = {
  description: 'Add a component',
  prompts: [{
    type: 'list',
    name: 'type',
    message: 'Select the type of component',
    "default": 'Stateless Function',
    choices: function choices() {
      return ['Stateless Function', 'React.PureComponent'];
    }
  }, {
    type: 'input',
    name: 'name',
    message: 'What should it be called?',
    "default": 'Button',
    validate: function validate(value) {
      return (0, _stickJs.pipe)(value, (0, _stickJs.condS)([(0, _stickJs.pipe)((0, _stickJs.pipe)('', _stickJs.eq), (0, _stickJs.guardV)('The name is required')), (0, _stickJs.pipe)(_componentExists["default"], (0, _stickJs.guardV)('A component or container with this name already exists')), (0, _stickJs.pipe)(_stickJs.otherwise, (0, _stickJs.guard)(_stickJs.T))]));
    }
  }, {
    type: 'confirm',
    name: 'wantLoadable',
    "default": false,
    message: 'Do you want to load the component asynchronously?'
  }],
  actions: function actions(data) {
    var componentTemplate = (0, _stickJs.pipe)(data.type, (0, _stickJs.condS)([(0, _stickJs.pipe)((0, _stickJs.pipe)('Stateless Function', _stickJs.eq), (0, _stickJs.guardV)('./component/stateless.js.hbs')), (0, _stickJs.pipe)(_stickJs.otherwise, (0, _stickJs.guardV)('./component/class.js.hbs'))]));
    var actions = [{
      type: 'add',
      path: appPath + '/components/{{properCase name}}/index.js',
      templateFile: componentTemplate,
      abortOnFail: true
    }];
    actions.push({
      type: 'add',
      path: appPath + '/components/{{properCase name}}/messages.js',
      templateFile: './component/messages.js.hbs',
      abortOnFail: true
    });

    if (data.wantLoadable) {
      actions.push({
        type: 'add',
        path: appPath + '/components/{{properCase name}}/Loadable.js',
        templateFile: './component/loadable.js.hbs',
        abortOnFail: true
      });
    }

    return actions;
  }
};