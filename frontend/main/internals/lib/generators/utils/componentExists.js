"use strict";

/**
 * componentExists
 *
 * Check whether the given component exist in either the components or containers directory
 */
var fs = require('fs');

var path = require('path');

var pageComponents = fs.readdirSync(path.join(__dirname, '../../../../app/components'));
var pageContainers = fs.readdirSync(path.join(__dirname, '../../../../app/containers'));
var components = pageComponents.concat(pageContainers);

function componentExists(comp) {
  return components.indexOf(comp) >= 0;
}

module.exports = componentExists;