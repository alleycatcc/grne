const config = (modules) => ({
  presets: [
    [
      '@babel/env',
      { modules },
    ],
  ],
  plugins: [
    // --- reduce code size and avoid namespace pollution (e.g. global
    // polyfills; be sure to add @babel/runtime to runtime deps)
    '@babel/transform-runtime',
    'alleycat-stick-transforms',
  ],
})

module.exports = (api) => {
  api.cache.forever ()
  return {
    env: {
      // --- keyed on BABEL_ENV, NODE_ENV or babel --env-name
      es: config (false),
      cjs: config ('commonjs'),
    },
  }
}
