# ---
# last: rewrite + recurse
# break: rewrite + stop
# redirect: 302
# permanent: 301
# note, last & break actually redirect with 30x if the new location is absolute.

# upstream: not using (can be used for e.g. load balancer).
# there is an upstream.keepalive option which is suggested by some tutorials
# but we skip it since we're not using upstream at all.

# this might work as a micro-tweak in the main conf:
#
# map $uri $loggable {
#    ~jpg$  0;
#    default 1;
#}
# access_log /path/to/access.log combined if=$loggable;

# two approaches to caching: 1) set headers "Cache-Control" and/or use the
# "expire" directive; 2) use proxy_cache / uwsgi_cache family of directives.
# the second one tells nginx to explicitly act as a cache; the first one is
# a hint to every link in the chain that it may cache, including the
# browser. the first one is mostly good for either disabling caching or
# explicitly setting the cache time. the browser will cache by default and
# doesn't need to be told to.
#
# it is not possible to combine server caching with try_files, but it's also
# not necessary: the OS does it for us. so just point it the disk and be
# agnostic about whether it's really touching the disk or coming from the OS
# page cache.

uwsgi_cache_path /tmp/nginx-grne-cache/ levels=1:2 keys_zone=mycache:100m max_size=10g inactive=60m use_temp_path=off;
uwsgi_cache_key $request_method$scheme$host$proxy_host$request_uri$request_body;

server {
    listen 8080;
    server_name 127.0.0.1;

    root /tmp/grne-build;
    include uwsgi_params;

    # error_log /tmp/error.log info;
    # rewrite_log on;

    # --- we are caching POST requests since they are mostly read-only, with
    # the exception of /api/feedback and /api/analyse.
    # --- the important thing is to use the request body in the cache key.

    # --- unfortunately quite some repetition here: seems the only way to
    # avoid would be to use  'include' with file snippets.

    location /api/ {
        uwsgi_cache mycache;
        uwsgi_cache_valid 200 10m;
        uwsgi_cache_methods GET HEAD POST;

        # --- used for configuring many different buffers, among them the
        # one used for the cache key.
        # --- if the cache key is bigger than this it won't be cached --
        # this can be a problem for us because we cache the request body as
        # well.
        # --- server logs will show a warning when the key is too big.
        uwsgi_buffer_size 8k;

        # --- bypass the nginx cache if /nocache=1 is given or the
        # Cache-Control header requests no caching.
        uwsgi_cache_bypass $arg_nocache $http_cache_control;

        # --- nice for info: add a custom header with HIT/MISS/BYPASS
        add_header X-Proxy-Cache $upstream_cache_status;

        uwsgi_pass unix:/tmp/grne.wsgi.sock;
        rewrite ^/api/(.*)$      /$1                                break;
    }

    # --- don't cache analyse.
    location /api/analyse/ {
        uwsgi_pass unix:/tmp/grne.wsgi.sock;
        rewrite ^/api/(.*)$      /$1                                break;
    }

    # --- don't cache feedback.
    location /api/feedback/ {
        uwsgi_pass unix:/tmp/grne.wsgi.sock;
        rewrite ^/api/(.*)$      /$1                                break;
    }

    location = /solr {
        rewrite .*              http://$http_host/solr/                 permanent;
    }

    location /solr/ {
        # @todo
        proxy_pass http://10.12.12.42:8983;
    }

    location / {
        try_files $uri /index.html;
    }
}
