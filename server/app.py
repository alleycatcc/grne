#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import proj_paths # noqa

from functools import namedtuple
from sys import exc_info
from traceback import print_tb

from flask import Flask, jsonify, make_response
from flask_cors import CORS

from grne.server.advanced_search import define_advanced_search_routes
from grne.server.analyse import define_analyse_routes
from grne.server.autocomplete import define_autocomplete_routes
from grne.server.feedback import define_feedback_routes
from grne.server.lemma import define_lemma_routes
from grne.server.util import fail
from grne.util.util import get_env_value
from grne.util.util_redis import redis_client_get_app, redis_client_get_persist

from acatpy.speak import yellow
from acatpy.io.speak import info, iwarn, warn
from acatpy.io.io import die
from acatpy.io.speak import set_level as set_speak_level # noqa

from config import get as config_get

Global = namedtuple('Global', (
    # ------ from the environment
    'env',
    'solr_url',
    'smtp_username',
    'smtp_password',
    'smtp_host',
    'smtp_port',
    'feedback_recipients',
    'grabzit_app_key',
    'grabzit_app_secret',

    # ------ other globals

    # --- see comments in config_user_example.py about `redis_client_app`
    # and `redis_client_persist`.
    'redis_client_app',
    'redis_client_persist',
))

config = config_get()

valid_envs = {'tst', 'acc', 'prd'}
default_env = 'tst'
default_solr_url = config.local.get('solr_url', 'https://grne-tst.alleycat.cc')

try:
    env_context = get_env_value('GRNE_ENV', default=default_env, valid=valid_envs)
    solr_url = get_env_value('GRNE_SOLR_URL', default=default_solr_url)
    smtp_username = get_env_value('GRNE_SMTP_USERNAME', required=False)
    smtp_password = get_env_value('GRNE_SMTP_PASSWORD', required=False)
    smtp_host = get_env_value('GRNE_SMTP_HOST', required=False)
    smtp_port = get_env_value('GRNE_SMTP_PORT', required=False)
    # --- GRNE_FEEDBACK_RECIPIENT can contain multiple addresses separated
    # by a comma.
    feedback_recipient = get_env_value('GRNE_FEEDBACK_RECIPIENT', required=False)
    # --- this can disappear if grabzit is for sure not going to be used.
    grabzit_app_key = get_env_value('GRNE_GRABZIT_APP_KEY')
    grabzit_app_secret = get_env_value('GRNE_GRABZIT_APP_SECRET')
except Exception as e:
    die(' '.join(e.args))

redis_client_app = redis_client_get_app(env_context)
redis_client_persist = redis_client_get_persist()
if redis_client_persist is None:
    warn('T2 caching is disabled (no valid redis_client_persist)')
if smtp_username and smtp_password and smtp_host and smtp_port and feedback_recipient:
    can_send_feedback = True
    feedback_recipients = feedback_recipient.split(',')
else:
    can_send_feedback = False
    feedback_recipients = []

info('can send feedback: %s' % yellow(can_send_feedback))

# set_speak_level('DEBUG')

# --- uwsgi expects this to be called 'application'.
application = Flask(
    __name__,
    static_url_path='/static',
    static_folder='../web/htdocs',
)

# --- without this, flask accepts unlimited uploads.
# --- needed so large files result in 413 (Request Entity Too Large)
# --- note that nginx has its own setting (http.client_max_body_size <n>M;)
application.config['MAX_CONTENT_LENGTH'] = config.main['max_file_upload_mb'] * 1024 * 1024

def init(app):
    CORS(app)

@application.errorhandler(404)
def error_not_found(_):
    return make_response(jsonify({'error': 'Not found'}), 404)

@application.errorhandler(413)
def error_too_large(_):
    return make_response(jsonify({'error': 'Entity too large'}), 413)

# --- this will catch exceptions but also HTTP status codes which are not
# specified above (and convert them to 500).

@application.errorhandler(Exception)
def error_handler_general(err):
    # @todo ierror
    the_type, value, traceback = exc_info()
    iwarn("%s\n--\nstacktrace:" % repr(err))
    print_tb(traceback)

    return fail(
        500,
        imsg='Runtime exception (see server logs)',
        abort=False,
    )

app_global = Global(
    env_context,
    solr_url,
    smtp_username, smtp_password, smtp_host, smtp_port, feedback_recipients,
    grabzit_app_key, grabzit_app_secret,
    redis_client_app,
    redis_client_persist,
)

define_advanced_search_routes(application, app_global, '/advanced-search')
# --- this is currently the only one where `redis_client_persist` is needed.
define_analyse_routes(application, app_global, '/analyse')
define_autocomplete_routes(application, app_global, '/autocomplete')
define_feedback_routes(application, app_global, '/feedback', can_send_feedback=can_send_feedback)
define_lemma_routes(application, app_global, '/lemma')
