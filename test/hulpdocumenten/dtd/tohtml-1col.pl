use feature 'say';

$/ = undef;

$a = <>;

say "<html><head>
<style>
    body { 
        font-size: 12px;
        line-height: 1.3em;
    }
</style>
<body>";

$a =~ s/&/&amp;/g;
$a =~ s/</&lt;/g;
$a =~ s/>/&gt;/g;
$a =~ s/\n\n/<p>/g;
$a =~ s/\n/<br>/g;

say $a;

