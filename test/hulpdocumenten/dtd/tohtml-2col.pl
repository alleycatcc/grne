use feature 'say';

#$/ = undef;

@a = <>;
$i = 0;

say "<html><head>
<style>
    body, td { 
        font-size: 12px;
        line-height: 1.3em;
    }
    .col2 {
    border-left: 1px solid black;
    }
</style>
<body>";

#$a =~ s/&/&amp;/g;
#$a =~ s/</&lt;/g;
#$a =~ s/>/&gt;/g;
#$a =~ s/\n\n/<p>/g;
#$a =~ s/\n/<br>/g;

say "<table><tr><td>";

for (@a) {
    if ($col != 2 and $i++ >= @a / 2) {
        say "</td><td class='col2'>";
        $col = 2;
    }
s/&/&amp;/g;
s/</&lt;/g;
s/>/&gt;/g;
s/\n\n/<p>/g;
s/\n/<br>/g;

say;
}

