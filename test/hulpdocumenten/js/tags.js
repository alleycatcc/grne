var Tags = function() {
    var pub = {
        elems: [],

        _methods: [
            'click',
            'toggleTags'
        ]
    };

    var mainDiv = undefined;

    var tagsShown = false;
    var saveHtml = [];
    var DEBOUNCE_MS = 100;
    var highlightDebounce = false;
    var highlighted = undefined;

    // Just remember the html of div main

    function toggleTags() {
        pub.elems.each(function(s) {
            if (! tagsShown) {
                // it's a span
                s.style.display = 'inline';
            }
            else {
                s.style.display = 'none';
            }
        });
        tagsShown = ! tagsShown;
        unHighlight();
    }

    function log(s) {
        if ( typeof console != 'undefined' ) {
            console.log(s);
        }
    }

    function getTag(t) {
        return "<span class='tag'>" + t + "</span>";
    }

    function click(elem) {
        var ep = elem.parentNode;
        if (highlighted != ep) {
            unHighlight();
            highlight(ep);
        }
        else {
            unHighlight();
        }

        /*
        highlightDebounce = true;
        setTimeout(function() { highlightDebounce = false; }, 
                DEBOUNCE_MS
                );
                */
    }

    function highlight(elem) {
        unHighlight();
        elem.addClass('highlightSpan');
        highlighted = elem;
    }

    function unHighlight() {
        if (highlighted) {
            highlighted.removeClass('highlightSpan');
            highlighted = undefined;
        }
    }

    pub._methods.each(function(m) {
        eval(sprintf("pub.%s = %s", m, m));
    });

    return pub;
}();

document.addEvent('domready', function() {
    Tags.elems = $$('.tag');
});


