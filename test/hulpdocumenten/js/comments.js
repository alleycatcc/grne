var Comments = function() {
    var _NS = 'Comments';

    var ANIM_DUR = '200ms';

    var HUE_SHIFT = .05;
    var VALUE_SHIFT = .2;
    //var SAT_SHIFT = .2;
    var HEIGHT = 200;

    // has to match css
    var H_UN = W_UN = 10;

    var H_SEL = W_SEL = 100;

    var g = {
        divs: [],
        dragMsgShown: 1,
        elems: {
            // the little boxes in the text.
            comments: []
        },
        // same indices as elems.comments
        fx: [],
        selectedIdx: -1
    };

    var pub = {
        elems: g.elems,
        _methods: [
            'log'
        ]
    };

    // bind i
    function mover(i) {
        return function() {
            var m = new Fx.Move( g.divs[i], 
                {
                    duration: '50',
                    relativeTo: g.divs[i-1], 
                    // top edge of this to bottom edge of prev
                    position: 'bottomRight', edge: 'topRight',
                    offset: {y:20}
                }
            );
            m.start();
        };
    }

    function makeComment(c, idx, hue, sat, value) {
        var spanPos = c.getPosition();
        var div = document.createElement('div');
        g.divs.push(div);
        div.innerHTML = c.innerHTML + 
            sprintf("<div style='visibility: hidden' class='comment-drag-msg' id='comment_drag_msg%d'>shift-click to drag</div>", idx);
        c.innerHTML = '&nbsp;';
        div.addClass('comment-box');
        var bw = window.getCoordinates().width;
        document.body.appendChild(div);

        // makes the div draggable. return value not relevant.
        new Drag.Move(div, {
            onStart: function(el, ev) {
                if (! ev.shift) {
                    this.stop();
                    div.style.cursor = 'pointer';
                    return;
                }

                if (g.dragMsgShown) {
                    $$('.comment-drag-msg').each(function(d) {
                        d.style.visibility = 'hidden';
                    });
                    g.dragMsgShown = 0;
                };
            },
            onDrag: function() {
            },
            onComplete: function(el, ev) {
                div.style.cursor = 'pointer';
            }
        });

        div.addEvent('mousedown', function(ev) {
            if (ev.shift) {
                div.style.cursor = 'move';
                return;
            }

            if (g.selectedIdx == idx) {
                unselect(g.selectedIdx);
                return;
            }
            else {
                unselect(g.selectedIdx);
            }

            select(idx);
        });
        div.addEvent('mouseover', function() {
            if (! g.dragMsgShown) return;
            $('comment_drag_msg' + idx).style.visibility = 'visible';
        });

        div.addEvent('mouseout', function() {
            if (! g.dragMsgShown) return;
            $('comment_drag_msg' + idx).style.visibility = 'hidden';
        });

        div.style.cursor = 'pointer';

        var dc = div.getCoordinates();
        if (idx == 0 ) div.style.left = (bw - dc.width) + 'px';
        div.style.top = '10px';
        var rgb = hsv2rgb(hue,sat,value);
        div.style.background = rgb;
        c.style.background = rgb;

    }

    function unselect(idx) {
        if (idx == -1) return;
        g.selectedIdx = -1;
        g.elems.comments[idx].removeClass('selected-comment');
        g.fx[idx].start({
            height: H_UN,
            width: W_UN
        });
    }

    function select(idx) {
        g.selectedIdx = idx;
        g.elems.comments[idx].addClass('selected-comment');
        g.fx[idx].start({
            height: H_SEL,
            width: W_SEL
        }).chain(function() {
            this.start({
                height: H_UN,
                width: W_UN
            });
        });
    }

    // adapted from the internet
    function hsv2rgb(h,s,v) {
        h = (h % 1 + 1) % 1; // wrap hue

        var i = Math.floor(h * 6),
            f = h * 6 - i,
            p = v * (1 - s),
            q = v * (1 - s * f),
            t = v * (1 - s * (1 - f));

        var map = function(a) { 
            return ("#" + a.map(function(i) { 
                return (("0" + Math.round(255 * i).toString(16)).slice(-2));
            }).join(''));
        };

        switch (i) {
            case 0: return map([v, t, p]);
            case 1: return map([q, v, p]);
            case 2: return map([p, v, t]);
            case 3: return map([p, q, v]);
            case 4: return map([t, p, v]);
            case 5: return map([v, p, q]);
        }
    }

    function log(s) {
        if ( typeof console != 'undefined' ) {
            console.log(s);
        }
    }


    document.addEvent('domready', function() {
        var idx = 0;
        // hue .17 is yellow
        var h = .17;
        var sat = 1;
        var hue = h;
        var value = 1;
        
        // the little boxes in the text.
        g.elems.comments = $$('.comment');
        g.elems.comments.each(function(c) {
            makeComment(c, idx++, hue, sat, value);
            value -= VALUE_SHIFT;
            if (value < .5) { 
                value = 1;
                //sat -= T.SAT_SHIFT;
                hue += HUE_SHIFT;
            }
            if (hue > 1) hue = h;

            g.fx.push(new Fx.Morph(c, {
                duration: ANIM_DUR
            }));
        });

        for (var i = 1; i < g.divs.length; i++) {
            // align top with bottom of prev
           
            // chain??
            setTimeout(mover(i), i * 200);
        }

        var s = document.createElement('span');
        s.setAttribute('class', 'comment-drag-msg');
        s.innerHTML = 'shift-click to drag';
    });

    pub._methods.each(function(m) {
        if (pub[m]) {
            var n = _NS ? sprintf(" in namespace '%s'", _NS) : '';
            alert(sprintf( "Property '%s' already exists%s, can't make method.", m, n ));
            return;
        }
        eval(sprintf("pub.%s = %s", m, m));
    });

    return pub;
}();
