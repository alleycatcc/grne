<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
  <xsl:output method="xml"  indent="no" omit-xml-declaration="yes" />
  <xsl:template match="/">
    <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE contLem SYSTEM "../../hulpdocumenten/dtd/lemma.dtd">
</xsl:text>
  <xsl:apply-templates select="node()"/>
  </xsl:template>

  <xsl:template match="*|text()|comment()|@*">
      <xsl:copy>
        <xsl:apply-templates select="@* | node()"/>
      </xsl:copy>
  </xsl:template>
  
  <xsl:template match="processing-instruction()">
    <xsl:copy-of select="."/>
    <xsl:text>&#x0A;</xsl:text>
  </xsl:template>
  
  <xsl:template match="//gebrW">
    <xsl:copy>
      <xsl:if test="following-sibling::*[1][self::cit]/@remove_this">
        <xsl:choose>
          <xsl:when test="matches(.,'^[\s]*(meestal|zelden)')">
            <xsl:attribute name="keep_this">
              <xsl:value-of select="'2'"/>
            </xsl:attribute>  
          </xsl:when>
          <xsl:when test="matches(.,'^((later\s+ook\s+)|(ook\s+))\s*(med\.|act\.)\s*$')">
            <xsl:attribute name="keep_this">
              <xsl:value-of select="'3'"/>
            </xsl:attribute>  
          </xsl:when>
          <xsl:when test="matches(.,'^((later\s+(ook\s+)?)|(ook\s+))?\s*met\s+(gen\.|dat\.|acc\.|inf\.|AcI|AcP|ptc)')">
            <xsl:attribute name="keep_this">
              <xsl:value-of select="'4'"/>
            </xsl:attribute>  
          </xsl:when>
          <xsl:when test="matches(.,'^(later\s+)*(ook\s+)*met[\s\)\(]*(gen\.|dat\.|acc\.|inf|AcI|AcP|ptc)[\s\)\(]+(en|of|en/of|\+)[\s\)\(]+(gen\.|dat\.|acc\.|inf|AcI|AcP|ptc)[\s\)\(]*')">
            <xsl:attribute name="keep_this">
              <xsl:value-of select="'5'"/>
            </xsl:attribute>  
          </xsl:when>
          <xsl:when test="matches(.,'^(later\s+)*(ook\s+)*met\s+(ἀνά|ἀπό|ἀμφί|ἀντί|δία|εἰς|ἐκ|ἐν|ἐς|ἐπί|κατά|μετά|παρά|περί|πρός|σύν|ὑπέρ|ὑπό)\s+(en|of|en/of|\+)\s+(gen\.|dat\.|acc\.|inf\.|AcI|AcP|ptc)')">
            <xsl:attribute name="keep_this">
              <xsl:value-of select="'6'"/>
            </xsl:attribute>  
          </xsl:when>          
          <xsl:otherwise>
            <xsl:attribute name="remove_this">
              <xsl:value-of select="'2'"/>
            </xsl:attribute>    
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>
  
</xsl:stylesheet>
