<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
  <xsl:output method="xml"  indent="no" omit-xml-declaration="yes" />
  <xsl:template match="/">
    <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE contLem SYSTEM "../../hulpdocumenten/dtd/lemma.dtd">
</xsl:text>
  <xsl:apply-templates select="node()"/>
  </xsl:template>

  <xsl:template match="*|text()|comment()|@*">
      <xsl:copy>
        <xsl:apply-templates select="@* | node()"/>
      </xsl:copy>
  </xsl:template>
  
  <xsl:template match="processing-instruction()">
    <xsl:copy-of select="."/>
    <xsl:text>&#x0A;</xsl:text>
  </xsl:template>
  
  <xsl:template match="cit[verw/aut[not(matches(.,'^Hdt\.|^Il\.|^Od\.|^Soph\.|^Eur\.|^Plat\.'))]]">
    <xsl:copy>
      <xsl:attribute name="remove_this">
        <xsl:value-of select="'1'"/>
      </xsl:attribute>  
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="cit[verw/aut[matches(.,'^Soph\.')]]">
    <xsl:copy>
      <xsl:if test=".//werk[not(matches(.,'^OT|^Ant\.|^Ai\.|^El\.'))]">
        <xsl:attribute name="remove_this">
          <xsl:value-of select="'1'"/>
        </xsl:attribute>  
       </xsl:if> 
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="cit[verw/aut[matches(.,'^Eur\.')]]">
    <xsl:copy>
      <xsl:if test=".//werk[not(matches(.,'^Hipp\.|^Tr\.|^Med\.|^Ba\.'))]">
        <xsl:attribute name="remove_this">
          <xsl:value-of select="'1'"/>
        </xsl:attribute>  
      </xsl:if> 
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="cit[verw/aut[matches(.,'^Plat\.')]]">
    <xsl:copy>
      <xsl:if test=".//werk[not(matches(.,'^Phaed\.|^Men\.|^Ap\.|^Prot\.|^Resp\.|^Phaedr\.|^Grg\.|^Smp\.'))]">
        <xsl:attribute name="remove_this">
          <xsl:value-of select="'1'"/>
        </xsl:attribute>  
      </xsl:if> 
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>
  
</xsl:stylesheet>
