<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
  <xsl:output method="xml"  indent="no" omit-xml-declaration="yes" />
  <xsl:template match="/">
    <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE contLem SYSTEM "../../hulpdocumenten/dtd/lemma.dtd">
</xsl:text>
  <xsl:apply-templates select="node()"/>
  </xsl:template>

  <xsl:template match="*|text()|comment()|@*">
      <xsl:copy>
        <xsl:apply-templates select="@* | node()"/>
      </xsl:copy>
  </xsl:template>
  
  <xsl:template match="processing-instruction()">
    <xsl:copy-of select="."/>
    <xsl:text>&#x0A;</xsl:text>
  </xsl:template>
  
  <xsl:template match="//gebrW[@remove_this]">
    <xsl:copy>
      <xsl:if test="following-sibling::*[2][self::cit[not(@remove_this)]]">
        <xsl:attribute name="check_this">
          <xsl:value-of select="'1'"/>
        </xsl:attribute>  
      </xsl:if>
      <xsl:if test="matches(normalize-space(.),';.+')">
        <xsl:attribute name="check_this">
          <xsl:value-of select="'3'"/>
        </xsl:attribute>  
      </xsl:if>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="//cit[@remove_this][parent::*[not(vertM)]]">
    <xsl:copy>
      <xsl:attribute name="check_this">
        <xsl:value-of select="'2'"/>
      </xsl:attribute>  
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="//gebrW[not(@remove_this)][matches(normalize-space(.),';.+')]">
    <xsl:copy>
      <xsl:attribute name="check_this">
        <xsl:value-of select="'3'"/>
      </xsl:attribute>  
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="//isgelCit">
    <xsl:copy>
      <xsl:attribute name="check_this">
        <xsl:value-of select="'4'"/>
      </xsl:attribute>  
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>
  
 
</xsl:stylesheet>
