<xsl:stylesheet 
    xmlns:f="http://functions.grne.org"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">


    <xsl:import href="grne.functions.xsl"/>
    <xsl:output method="xml"  indent="no" omit-xml-declaration="yes" />

    <xsl:template match="/">
        <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE contLem SYSTEM "../../hulpdocumenten/dtd/lemma.dtd">
</xsl:text>
        <xsl:apply-templates select="node()"/>
    </xsl:template>

    <xsl:template match="* | text() | comment() | @*">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="* | text() | comment() | @*" mode="special">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()" mode="special"/>
        </xsl:copy>
    </xsl:template>
   
    <xsl:template match="processing-instruction()">
        <xsl:copy-of select="."/>
        <xsl:text>&#x0A;</xsl:text>
    </xsl:template>

    <xsl:template match="text()[ancestor::vertM[not(@remove_this)][descendant::text()[normalize-space(.)!=''][last()]=current()]][matches(current(), ':\s*$')]">
        <xsl:variable name="prnt" select="ancestor::vertM"/>
        <xsl:variable name="sibs" select="f:siblings($prnt/following-sibling::*)"/>
        <xsl:variable name="found"><xsl:value-of select="concat('[', ., ']')"/></xsl:variable>
        <xsl:choose>
            <xsl:when test="matches($sibs, '^Xcit(Xcit|XgebrW)*vertM.*$')">
                <xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 1: //vertM[matches(.,'': $'')]/(...[@remove])*/vertM')"/> </xsl:comment>
                <xsl:value-of select="replace(., ':\s*$', '; ')"/>
            </xsl:when>
            <xsl:when test="matches($sibs, '^Xcit(Xcit|XgebrW)*gebrW.*$')">
                <xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 2: //vertM[matches(.,'': $'')]/(..[@remove])*/gebrW')"/> </xsl:comment>
                <xsl:value-of select="replace(., ':\s*$', '; ')"/>
            </xsl:when>
            <xsl:when test="matches($sibs, '^Xcit(Xcit|XgebrW)*$')">
                <xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 3: //vertM[matches(.,'': $'')]/(...[@remove])*')"/></xsl:comment>
                <xsl:value-of select="f:dotornot(.)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="text()[ancestor::vertM[not(@remove_this)][descendant::text()[normalize-space(.)!=''][last()]=current()]][matches(current(), ',\s*$')]">
        <xsl:variable name="prnt" select="ancestor::vertM"/>
        <xsl:variable name="sibs" select="f:siblings($prnt/following-sibling::*)"/>
        <xsl:variable name="found"><xsl:value-of select="concat('[', ., ']')"/></xsl:variable>
        <xsl:choose>
            <xsl:when test="matches($sibs, '^Xcit(Xcit|XgebrW)*vertM.*$')">
                <xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 1b: //vertM[matches(.,'', $'')]/(...[@remove])*/vertM')"/>)</xsl:comment>
                <xsl:value-of select="replace(., ',\s*$', '; ')"/>
            </xsl:when>
            <xsl:when test="matches($sibs, '^Xcit(Xcit|XgebrW)*gebrW.*$')">
                <xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 2b: //vertM[matches(.,'', $'')]/(..[@remove])*/gebrW')"/></xsl:comment>
                <xsl:value-of select="replace(., ',\s*$', '; ')"/>
            </xsl:when>
            <xsl:when test="matches($sibs, '^Xcit(Xcit|XgebrW)*$')">
                <xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 3b: //vertM[matches(.,'', $'')]/(...[@remove])*')"/></xsl:comment>
                <xsl:value-of select="f:dotornot(.)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="text()[ancestor::vertM[not(@remove_this)][descendant::text()[normalize-space(.)!=''][last()]=current()]][matches(current(), ';\s*$')]">
        <xsl:variable name="prnt" select="ancestor::vertM"/>
        <xsl:variable name="sibs" select="f:siblings($prnt/following-sibling::*)"/>
        <xsl:variable name="found"><xsl:value-of select="concat('[', ., ']')"/></xsl:variable>
        <xsl:choose>
            <xsl:when test="matches($sibs, '^Xcit(Xcit|XgebrW)*$')">
                <xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 3c: //vertM[matches(.,''; $'')]/(...[@remove])*')"/></xsl:comment>
                <xsl:value-of select="f:dotornot(.)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="text()[ancestor::vertM[not(@remove_this)][descendant::text()[normalize-space(.)!=''][last()]=current()]][matches(current(), '[^,.:;\s]\s*$')]">
        <xsl:variable name="prnt" select="ancestor::vertM"/>
        <xsl:variable name="sibs" select="f:siblings($prnt/following-sibling::*)"/>
        <xsl:variable name="found"><xsl:value-of select="concat('[', ., ']')"/></xsl:variable>
        <xsl:choose>
            <xsl:when test="matches($sibs, '^Xcit(Xcit|XgebrW)*$')">
                <xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 3d: //vertM[matches(.,''[^,.:;\s]\s*$'')]/(...[@remove])*')"/> </xsl:comment>
                <xsl:value-of select="f:dotornot(.)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="text()[ancestor::gebrW[not(@remove_this)][descendant::text()[normalize-space(.)!=''][last()]=current()]][matches(current(), ':\s*$')]">
        <xsl:variable name="prnt" select="ancestor::gebrW"/>
        <xsl:variable name="sibs" select="f:siblings($prnt/following-sibling::*)"/>
        <xsl:variable name="found"><xsl:value-of select="concat('[', ., ']')"/></xsl:variable>
        <xsl:choose>
            <xsl:when test="matches($sibs, '^Xcit(Xcit|XgebrW)*gebrW.*$')">
                <xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 4: //gebrW[matches(.,'': $'')]/(...[@remove])*/gebrW')"/> </xsl:comment>
                <xsl:value-of select="replace(., ':\s*$', '; ')"/>
            </xsl:when>
            <xsl:when test="matches($sibs, '^Xcit(Xcit|XgebrW)*vertM.*$')">
                <xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 5: //gebrW[matches(.,'': $'')]/(...[@remove])*/vertM')"/> </xsl:comment>
                <xsl:value-of select="replace(., ':\s*$', '; ')"/>
            </xsl:when>
            <xsl:when test="matches($sibs, '^Xcit(Xcit|XgebrW)*$')">
                <xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 6: //gebrW[matches(.,'': $'')]/(...[@remove])*')"/></xsl:comment>
                <xsl:value-of select="f:dotornot(.)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="text()[ancestor::gebrW[not(@remove_this)][descendant::text()[normalize-space(.)!=''][last()]=current()]][matches(current(), '[^,:;\s]\s*$')]">
        <xsl:variable name="prnt" select="ancestor::gebrW"/>
        <xsl:variable name="sibs" select="f:siblings($prnt/following-sibling::*)"/>
        <xsl:variable name="found"><xsl:value-of select="concat('[', ., ']')"/></xsl:variable>
        <xsl:choose>
            <xsl:when test="matches($sibs, '^Xcit(Xcit|XgebrW)*gebrW$')">
                <xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 4b: //gebrW[matches(.,''[^,:;\s]\s*$'')]/(...[@remove])*/gebrW')"/> </xsl:comment>
                <xsl:value-of select="f:dotornot(.)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="text()[ancestor::cit[not(@remove_this)][descendant::text()[normalize-space(.)!=''][last()]=current()]][matches(., ';\s*$')]">
        <xsl:variable name="prnt" select="ancestor::cit"/>
        <xsl:variable name="sibs" select="f:siblings($prnt/following-sibling::*)"/>
        <xsl:variable name="found"><xsl:value-of select="concat('[', ., ']')"/></xsl:variable>
        <xsl:choose>
            <xsl:when test="matches($sibs, '^Xcit(Xcit|XgebrW)*$')">
                <xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 7: //cit[matches(.,''; $'')]')"/></xsl:comment>
                <xsl:value-of select="f:dotornot(.)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
