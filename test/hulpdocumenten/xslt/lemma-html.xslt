<?xml version="1.0"?>

<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema">

<xsl:param name="frameworkPath"/>

<!-- method important? -->
<xsl:output method="html" encoding="UTF-8"/>

<xsl:variable name="idx123" select="1"/>
<xsl:variable name="idxABC" select="A"/>

<xsl:template match="contLem">

  <xsl:variable name="resourcePath">
    <xsl:choose>
      <xsl:when test="string-length($frameworkPath) != 0">
        <xsl:value-of select="$frameworkPath"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="'../../hulpdocumenten'"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <html>
        <head>
            <!-- Set these paths according to where transformed HTML files
                 are saved by Oxygen -->
            <link rel="stylesheet" href="../../hulpdocumenten/css/lemma-html.css"/>
            <script src="../../hulpdocumenten/js/mootools-core-1.4.5.js"/>
            <script src="../../hulpdocumenten/js/mootools-more-1.4.0.1.js"/>
            <script src="../../hulpdocumenten/js/sprintf-0.6.js"/>
            <script src="../../hulpdocumenten/js/comments.js"/>
            <script src="../../hulpdocumenten/js/tags.js"/>
        </head>
    <body>

        <table id="meta-table">

    <xsl:apply-templates select="metadata"/>

        </table>

        <hr/>

        <p>
        <input type="button" onclick="Tags.toggleTags()" value='Toggle Tags'/>
        </p>

        <p>&#xa;</p>

        <div id="main">

    <xsl:apply-templates select="lem | escLem | verwLem | xlLem"/>

        </div>

    </body>
  </html>
</xsl:template>

<xsl:template match="metadata">
    <xsl:for-each select="*">
        <xsl:variable name="row_class">
            <xsl:choose>
                <xsl:when test="position() mod 2 = 0">
                    row_even
                </xsl:when>
                <xsl:otherwise>
                    row_odd
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <tr>
            <xsl:attribute name="class">
                <xsl:value-of select="$row_class"/>
            </xsl:attribute>

                <!--xsl:if test="position() != 1">
            <br/>
                </xsl:if-->

            <td class="meta-key">
            <xsl:value-of select="local-name()"/> 

            <xsl:for-each select="@*">
                <br/>
                <span class="attribute">
                <xsl:value-of select="local-name()"/>=<xsl:value-of select="."/>
                </span>
            </xsl:for-each>

            </td>

            <td>

        <xsl:apply-templates select=".//comment()"/>

        <!-- list -->
        <xsl:choose>
            <xsl:when test="local-name() = 'indexI'">

                <ul>

                <xsl:for-each select="item">
                    <li>
                        <xsl:value-of select="."/>
                    </li>
                </xsl:for-each>

                </ul>

            </xsl:when>
            <xsl:otherwise>
                <!-- allow for <span> elements -->
                <span>
                <xsl:attribute name="id">
                    <xsl:value-of select="concat('meta-', local-name())"/>
                </xsl:attribute>

                <xsl:copy-of select="node()"/>
                </span>
            </xsl:otherwise>
        </xsl:choose>
            </td>
        </tr>

    </xsl:for-each>
</xsl:template>

<xsl:template match="comment()">
    <span class="comment">
        <xsl:value-of select="."/>
    </span>
</xsl:template>

<xsl:template match="*">

    <xsl:choose>

        <xsl:when test="local-name() = 'niv123'">
    <div class='niv123'>

        <span class="tag" onclick="Tags.click(this)">
            <xsl:value-of select="local-name()"/>
        </span>
            <xsl:number format="1. "/>
            <xsl:apply-templates/>
    </div>
        </xsl:when>

        <xsl:when test="local-name() = 'xlNiv123'">
            <div class='xlNiv123'>
                
                <span class="tag" onclick="Tags.click(this)">
                    <xsl:value-of select="local-name()"/>
                </span>
                <xsl:number format="1. "/>
                <xsl:apply-templates/>
            </div>
        </xsl:when>
        <xsl:when test="local-name() = 'xlNiv-3'">
            <div class='xlNiv-3'>
                
                <span class="tag" onclick="Tags.click(this)">
                    <xsl:value-of select="local-name()"/>
                </span>
                <xsl:number format="i. "/>
                <xsl:apply-templates/>
            </div>
        </xsl:when>
        
        <xsl:when test="local-name() = 'xlNiv-4'">
            <div class='xlNiv-4'>
                
                <span class="tag" onclick="Tags.click(this)">
                    <xsl:value-of select="local-name()"/>
                </span>
                <xsl:number format="1. "/>
                <xsl:apply-templates/>
            </div>
        </xsl:when>
        <xsl:when test="local-name() = 'xlNiv-5'">
            <div class='xlNiv-5'>
                
                <span class="tag" onclick="Tags.click(this)">
                    <xsl:value-of select="local-name()"/>
                </span>
                <xsl:number format="a. "/>
                <xsl:apply-templates/>
            </div>
        </xsl:when>
        
        
        
        <xsl:when test="local-name() = 'nivABC'">
    <div class='nivABC'>
        <span class="tag" onclick="Tags.click(this)">
            <xsl:value-of select="local-name()"/>
        </span>
            <xsl:number format="A. "/>
            <xsl:apply-templates/>

            <!-- end all nivABC with a period -->
            <!--xsl:value-of select="'.'"/-->
    </div>
        </xsl:when>
        <xsl:when test="local-name() = 'xlNivABC'">
            <div class='xlNivABC'>
                <span class="tag" onclick="Tags.click(this)">
                    <xsl:value-of select="local-name()"/>
                </span>
                <xsl:number format="a. "/>
                <xsl:apply-templates/>
                
                <!-- end all nivABC with a period -->
                <!--xsl:value-of select="'.'"/-->
            </div>
        </xsl:when>
        <xsl:when test="local-name() = 'xlNivABC'">
            <div class='xlNivABC'>
                <span class="tag" onclick="Tags.click(this)">
                    <xsl:value-of select="local-name()"/>
                </span>
                <xsl:number format="A. "/>
                <xsl:apply-templates/>
                
                <!-- end all nivABC with a period -->
                <!--xsl:value-of select="'.'"/-->
            </div>
        </xsl:when>        
        <xsl:otherwise>
    <span>

        <xsl:variable name="am_local_last" select=". = (../*)[last()]"/>

        <xsl:variable name="class" as="xs:string">
            <xsl:choose>
                <xsl:when test="$am_local_last">
                    <xsl:value-of select="concat('no-pad-right ', local-name())"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="local-name()"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:attribute name="class">
            <xsl:value-of select="$class"/>
        </xsl:attribute>

    <span class="tag" onclick="Tags.click(this)">
        <xsl:value-of select="local-name()"/>
    </span>

        <xsl:apply-templates/>

    </span>
        </xsl:otherwise>


    </xsl:choose>

</xsl:template>

<xsl:template match="span">
    span
</xsl:template>

<!-- Override the built-in rule which always prints text nodes. -->
  <xsl:template match="text()">
    <!--xsl:value-of select="replace(normalize-space(.), '&#xa;', '&amp;nbsp;')"/-->
    <!-- later: declare entity nbsp in dtd? -->
    <!--xsl:value-of select="normalize-space(.)"/-->
    <xsl:choose>
        <xsl:when test="position() = last()">
            <!--xsl:value-of select="normalize-space(.)"/-->
            <!--xsl:value-of select="replace(., '[S]+$', '')"/-->
            <xsl:value-of select="."/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="."/>
        </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

</xsl:stylesheet>
