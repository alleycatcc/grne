#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from functools import namedtuple
import proj_paths # noqa
import re

from grne.char_add_marking import (
    add_acutus,
    add_grave,
    add_psili,
    add_dasia,
    add_perispomeni,
    add_dialytika,
    add_iota_subscriptum,
    add_macron,
    add_breve,
    combining_non_spiritus,
    strip_dict,
    iota_subscriptum,
    dict_expand_iota_subscriptum,
    set_accented_chars_greek_and_coptic,
    dict_lg,
    dict_gl,
    dict_gl_with_iota,
    set_greek,
    canonical_dict_recurse,
    # canonical_dict_final_combine_3_rxs,
    # canonical_dict_final_combine_2_rxs,
    set_greek_extended,
    xml_entities,
)

from acatpy.io.speak import debug, set_level as set_speak_level # noqa

# set_speak_level('DEBUG')

Spiritus_info = namedtuple('Spiritus_info', ['left', 'right', 'combined', 'has_any'])

# @todo
entities_regexes = None
regex = {
    'greek': None,
}

regex_greek_extended = '|'.join(re.escape(x) for x in set_greek_extended)
regex_accented_greek_and_coptic = '|'.join(re.escape(x) for x in set_accented_chars_greek_and_coptic)
regex_greek_accented = regex_accented_greek_and_coptic + '|' + regex_greek_extended

def cmp(a, b):
    if a < b: return -1
    if a == b: return 0
    else: return 1

# @todo move
def sort_plaats(a, b):
    # plaats was missing
    if a == -1:
        return -1
    if b == -1:
        return 1
    if a.isalpha() or b.isalpha():
        return cmp(a, b)

    def to_list(y):
        # Tokenize on . (and throw it away), on space (and keep it),
        # or on letters (and keep them).
        the_list = re.split('([a-zA-Z])|\\.|(\\s)', y)
        # Take out 'None and '' '
        return [x for x in the_list if x is not None and x is not '']

    a_list, b_list = to_list(a), to_list(b)

    m = min(len(a_list), len(b_list))
    for i in range(0, m):
        a_i = a_list[i]
        b_i = b_list[i]
        if a_i.isdigit() and b_i.isdigit():
            x = cmp(int(a_i), int(b_i))
            if x == 0:
                continue
            else:
                return x
        else:
            x = cmp(a_i, b_i)
            if x == 0:
                continue
            else:
                return x

    return cmp(0, len(a_list) - len(b_list))


set_spiritus_dasia_pure = {
    (7937, 'ἁ'), (7945, 'Ἁ'), (7953, 'ἑ'), (7961, 'Ἑ'), (7969, 'ἡ'), (7977, 'Ἡ'), (7985, 'ἱ'),
    (7993, 'Ἱ'), (8001, 'ὁ'), (8009, 'Ὁ'), (8017, 'ὑ'), (8025, 'Ὑ'), (8033, 'ὡ'), (8041, 'Ὡ'),
    (8065, 'ᾁ'), (8073, 'ᾉ'), (8081, 'ᾑ'), (8089, 'ᾙ'), (8097, 'ᾡ'), (8105, 'ᾩ'), (8165, 'ῥ'),
    (8172, 'Ῥ'), (8190, '῾'),
}


set_spiritus_psili_pure = {
    (7936, 'ἀ'), (7944, 'Ἀ'), (7952, 'ἐ'), (7960, 'Ἐ'), (7968, 'ἠ'), (7976, 'Ἠ'), (7984, 'ἰ'),
    (7992, 'Ἰ'), (8000, 'ὀ'), (8008, 'Ὀ'), (8016, 'ὐ'), (8032, 'ὠ'), (8040, 'Ὠ'), (8064, 'ᾀ'),
    (8072, 'ᾈ'), (8080, 'ᾐ'), (8088, 'ᾘ'), (8096, 'ᾠ'), (8104, 'ᾨ'), (8127, '᾿'), (8164, 'ῤ'),
}

# Chars (and combining chars) containing spiritus and something else.

set_spiritus_dasia_mixed = {
    (7939, 'ἃ'), (7941, 'ἅ'), (7943, 'ἇ'), (7947, 'Ἃ'), (7949, 'Ἅ'), (7951, 'Ἇ'), (7955, 'ἓ'),
    (7957, 'ἕ'), (7963, 'Ἓ'), (7965, 'Ἕ'), (7971, 'ἣ'), (7973, 'ἥ'), (7975, 'ἧ'), (7979, 'Ἣ'),
    (7981, 'Ἥ'), (7983, 'Ἧ'), (7987, 'ἳ'), (7989, 'ἵ'), (7991, 'ἷ'), (7995, 'Ἳ'), (7997, 'Ἵ'),
    (7999, 'Ἷ'), (8003, 'ὃ'), (8005, 'ὅ'), (8011, 'Ὃ'), (8013, 'Ὅ'), (8019, 'ὓ'), (8021, 'ὕ'),
    (8023, 'ὗ'), (8027, 'Ὓ'), (8029, 'Ὕ'), (8031, 'Ὗ'), (8035, 'ὣ'), (8037, 'ὥ'), (8039, 'ὧ'),
    (8043, 'Ὣ'), (8045, 'Ὥ'), (8047, 'Ὧ'), (8067, 'ᾃ'), (8069, 'ᾅ'), (8071, 'ᾇ'), (8075, 'ᾋ'),
    (8077, 'ᾍ'), (8079, 'ᾏ'), (8083, 'ᾓ'), (8085, 'ᾕ'), (8087, 'ᾗ'), (8091, 'ᾛ'), (8093, 'ᾝ'),
    (8095, 'ᾟ'), (8099, 'ᾣ'), (8101, 'ᾥ'), (8103, 'ᾧ'), (8107, 'ᾫ'), (8109, 'ᾭ'), (8111, 'ᾯ'),
    (8157, '῝'), (8158, '῞'), (8159, '῟'),
}

set_spiritus_psili_mixed = {
    (7938, 'ἂ'), (7940, 'ἄ'), (7942, 'ἆ'), (7946, 'Ἂ'), (7948, 'Ἄ'), (7950, 'Ἆ'), (7954, 'ἒ'),
    (7956, 'ἔ'), (7962, 'Ἒ'), (7964, 'Ἔ'), (7970, 'ἢ'), (7972, 'ἤ'), (7974, 'ἦ'), (7978, 'Ἢ'),
    (7980, 'Ἤ'), (7982, 'Ἦ'), (7986, 'ἲ'), (7988, 'ἴ'), (7990, 'ἶ'), (7994, 'Ἲ'), (7996, 'Ἴ'),
    (7998, 'Ἶ'), (8002, 'ὂ'), (8004, 'ὄ'), (8010, 'Ὂ'), (8012, 'Ὄ'), (8018, 'ὒ'), (8020, 'ὔ'),
    (8022, 'ὖ'), (8034, 'ὢ'), (8036, 'ὤ'), (8038, 'ὦ'), (8042, 'Ὢ'), (8044, 'Ὤ'), (8046, 'Ὦ'),
    (8066, 'ᾂ'), (8068, 'ᾄ'), (8070, 'ᾆ'), (8074, 'ᾊ'), (8076, 'ᾌ'), (8078, 'ᾎ'), (8082, 'ᾒ'),
    (8084, 'ᾔ'), (8086, 'ᾖ'), (8090, 'ᾚ'), (8092, 'ᾜ'), (8094, 'ᾞ'), (8098, 'ᾢ'), (8100, 'ᾤ'),
    (8102, 'ᾦ'), (8106, 'ᾪ'), (8108, 'ᾬ'), (8110, 'ᾮ'), (8141, '῍'), (8142, '῎'), (8143, '῏'),
}

def _get_spiritus_info(f, g, the_input):
    dasia = []
    psili = []
    combined = []
    has_any = None
    idx = len(the_input)
    for c in reversed(the_input):
        idx -= 1
        if f(c, idx):
            d, p, c = "1", "0", -7
            has_any = True
        elif g(c, idx):
            d, p, c = "0", "1", 7
            has_any = True
        else:
            d, p, c = "0", "0", 0
        dasia.append(d)
        psili.append(p)
        combined.append(c)

    return Spiritus_info(
        ''.join(dasia),
        ''.join(psili),
        combined,
        bool(has_any)
    )


'''
The input should be canonical.

A note on combining chars: if the input originally contained a single
combining spiritus after a character, then that character is now a monolith
and this will work fine. If it contained a series of combining symbols after
a character, then that character is False for having a pure spiritus. Pull
away all combining characters at the beginning of this function, because
cycling through the string (despite it being a u'' string) won't work
otherwise.
'''

def get_pure_spiritus_info(the_input):
    the_input = re.sub(regex_combining, '', the_input)
    f = lambda x, _: (ord(x), x) in set_spiritus_dasia_pure
    g = lambda x, _: (ord(x), x) in set_spiritus_psili_pure
    return _get_spiritus_info(f, g, the_input)

'''
The input should be canonical.

How to deal with combining chars: cycle through each character, and peek
ahead to the next one. If it's a combining spiritus, throw it away, and set
this location to True. If it's a combining char of any other kind, just
throw it away. (See note above about cycling through u'' strings).
'''

def get_spiritus_info(the_input):
    combining_spiritus_left = 0x0314
    combining_spiritus_right = 0x0313

    def clean_combining(the_input):
        length = len(the_input)
        if length == 0: return the_input, {}
        last = length - 1
        locations = {}

        idx = 0
        idx_out = 0
        new = []
        while idx <= last:
            x = the_input[idx]
            new.append(x)
            check_next = True
            while check_next:
                nextchar = None if idx == last else ord(the_input[idx + 1])
                if nextchar == combining_spiritus_left:
                    locations[idx_out] = -7
                    idx += 1
                elif nextchar == combining_spiritus_right:
                    locations[idx_out] = 7
                    idx += 1
                elif nextchar in combining_non_spiritus:
                    idx += 1
                else:
                    check_next = False
            idx += 1
            idx_out += 1
        return ''.join(new), locations

    the_input, first_pass = clean_combining(the_input)

    f = lambda x, idx: (
        first_pass.get(idx) == -7 or # noqa
        (ord(x), x) in set_spiritus_dasia_pure or # noqa
        (ord(x), x) in set_spiritus_dasia_mixed
    )
    g = lambda x, idx: (
        first_pass.get(idx) == 7 or # noqa
        (ord(x), x) in set_spiritus_psili_pure or # noqa
        (ord(x), x) in set_spiritus_psili_mixed
    )
    return _get_spiritus_info(f, g, the_input)


dict_macron = {
    (8113, 'ᾱ'): (945, 'α'),
    (8121, 'Ᾱ'): (913, 'Α'),
    (8145, 'ῑ'): (953, 'ι'),
    (8153, 'Ῑ'): (921, 'Ι'),
    (8161, 'ῡ'): (965, 'υ'),
    (8169, 'Ῡ'): (933, 'Υ'),
}

dict_breve = {
    (8112, 'ᾰ'): (945, 'α'),
    (8120, 'Ᾰ'): (913, 'Α'),
    (8144, 'ῐ'): (953, 'ι'),
    (8152, 'Ῐ'): (921, 'Ι'),
    (8160, 'ῠ'): (965, 'υ'),
    (8168, 'Ῠ'): (933, 'Υ'),
}

set_macron_combining = {
    #  ̄ combining macron
    0x0304,
    #  ͞ combining double macron, almost certainly not in use.
    0x035e,
}

set_breve_combining = {
    #  ̆ combining breve
    0x0306,
    #  ͝ combining double breve, almost certainly not in use.
    0x035d,
}

set_macron_breve_combining = {
    # the following are from the Combining Diacritical Marks Supplement
    # table and almost certainly not in use.
    #   ᷋ combining breve-macron.
    0x1dcb,
    #   ᷌ combining macron-breve.
    0x1dcc,
    #  ᷄ combining macron-acute.
    0x1dc4,
    #  ᷅ combining grave-marcon.
    0x1dc5,
    #  ᷆ combining macron-grave.
    0x1dc6,
    #  ᷇ combining acute-macron.
    0x1dc7,
}

set_acute_combining = {
    0x0301,
}

set_spiritus_combining = {
    # --- combining comma above
    0x0313,
    # --- combining reversed comma above
    0x0314,
}

# --- might be useful later.
def union_dicts(dicts):
    union = {}
    for d in dicts:
        for k, v in d.items():
            union[k] = v
    return union


set_combining = set().union(set_macron_combining, set_breve_combining, set_macron_breve_combining, set_acute_combining, set_spiritus_combining)
regex_combining = '|'.join(re.escape(chr(x)) for x in set_combining)

def _remove_helper(combining, the_dict, counter):
    def f(x):
        if ord(x) in combining:
            counter['count'] += 1
            return ''
        elif (ord(x), x) in the_dict:
            counter['count'] += 1
            return the_dict[(ord(x), x)][1]
        else:
            return x
    return f

def remove_macron(a):
    counter = {"count": 0}
    f = _remove_helper(set_macron_combining, dict_macron, counter)

    b = [f(x) for x in a]
    b = ''.join(b)
    return b, counter['count']

def remove_breve(a):
    counter = {"count": 0}
    f = _remove_helper(set_breve_combining, dict_breve, counter)

    b = [f(x) for x in a]
    b = ''.join(b)
    return b, counter['count']

def strip_markings(a, iota_intact):
    altered = None

    def replace(m):
        nonlocal altered
        altered = True
        return ''
    b = re.sub(regex_combining, replace, a)
    the_dict = strip_dict_iota_intact if iota_intact else strip_dict

    def f(x):
        r = x
        while True:
            y = ord(r)
            z = the_dict.get(y)
            if z is not None:
                nonlocal altered
                altered = True
                if z == -1:
                    return ''
                r = chr(z)
                continue
            return r
    b = [f(x) for x in b]
    b = ''.join(b)
    return b, bool(altered)


dict_beta = {
    '/': add_acutus,
    '\\': add_grave,
    '(': add_dasia,
    ')': add_psili,
    '=': add_perispomeni,
    '+': add_dialytika,
    '|': add_iota_subscriptum,
    # --- these are just a formality - macron and breve will be pulled off
    # again anyway.
    '&': add_macron,
    "'": add_breve,
}

set_beta = set()
for i in dict_beta:
    set_beta.update(i)

strip_dict_iota_intact = strip_dict.copy()
for k in iota_subscriptum:
    strip_dict_iota_intact.pop(k, None)

def _convert(s, the_dict, dict_beta={}):
    t = []
    errors = []
    altered = None
    for i in range(len(s)):
        if dict_beta:
            if s[i] in dict_beta:
                if i == 0:
                    errors.append('string must not begin with beta char')
                    break
                # get the map corresponding to the beta_char
                f = dict_beta[s[i]]
                g = ord(t[-1])
                x = f.get(g)
                # check whether the previous symbol combines with beta_char
                # and if so, do the map
                if x:
                    t[-1] = chr(x)
                    altered = True
                else:
                    altered = True
                continue
        if s[i] in the_dict:
            t.append(the_dict[s[i]])
            altered = True
        else:
            t.append(s[i])
            continue
    t = ''.join(t)
    return t, bool(altered), errors

def to_latin(x, iota_intact=False, to_lower=True):
    if to_lower:
        s = x.lower()
    else:
        s = x
    if iota_intact:
        s, altered1 = strip_markings(s, True)
        the_dict = dict_gl_with_iota
    else:
        s, altered1 = strip_markings(s, False)
        the_dict = dict_gl
    # --- to_latin can not currently return any errors.
    t, altered2, errors = _convert(s, the_dict)
    return t, (altered1 or altered2), errors

def to_greek_precise(x):
    s, altered, errors = _convert(x, dict_lg, dict_beta=dict_beta)
    if s and s[len(s) - 1] == 'σ':
        t = s[0:-1] + 'ς'
        altered = 1
    else:
        t = s
    return t, altered, errors

def expand_iota_subscriptum(s):
    expand = lambda j: dict_expand_iota_subscriptum.get(j, j)
    return ''.join(expand(_) for _ in s)

set_dash = {0x2d, 0x2010, 0x2011, 0x2012, 0x2013, 0x2014, 0xfe63, 0xff0d}

def normalise_hyphens(s, keep_at_beginning=False, keep_at_end=False):
    t = []
    count_h = 0
    for i in range(len(s)):
        if ord(s[i]) in set_dash:
            count_h += 1
        else:
            t.append(s[i])
    if keep_at_beginning:
        if ord(s[0]) in set_dash:
            t.insert(0, s[0])
            count_h -= 1
    if keep_at_end:
        if ord(s[len(s) - 1]) in set_dash:
            t.append(s[len(s) - 1])
            count_h -= 1
    t = ''.join(t)
    return t, count_h

def normalise_macron_breve(x):
    [s, count_m] = remove_macron(x)
    [t, count_b] = remove_breve(s)
    return t, count_m, count_b

def normalise_names(x, keep_at_beginning=False, keep_at_end=False):
    s, count_m, count_b = normalise_macron_breve(x)
    t, count_h = normalise_hyphens(s, keep_at_beginning=keep_at_beginning, keep_at_end=keep_at_end)
    return t

def sub(pattern, repl, string, **kw):
    replacements = 0

    def replf(m):
        nonlocal replacements
        replacements += 1
        return repl
    return re.sub(pattern, replf, string, **kw), replacements

def canonical(s):
    t = s
    num_replacements = 0
    while True:
        found = False
        '''
        break_all = False
        # --- don't recurse if found here.
        # --- do the 3's first.
        # for k, v in canonical_dict_final_combine_3.items():
        # k = re.escape(k)
        for rx, v in canonical_dict_final_combine_3_rxs.items():
            t, replacements_i = sub(rx, v, t)
            if replacements_i > 0:
                num_replacements += replacements_i
                break_all = True
                break
        # --- don't recurse if found here.
        # --- now the 2's.
        if break_all: break
        # for k, v in canonical_dict_final_combine_2.items():
        # k = re.escape(k)
        for rx, v in canonical_dict_final_combine_2_rxs.items():
            t, replacements_i = sub(rx, v, t)
            if replacements_i > 0:
                num_replacements += replacements_i
                break_all = True
                break
        if break_all: break
        '''
        # --- recurse if found here.
        for k, v in canonical_dict_recurse.items():
            # @todo escape
            t, replacements_i = sub(k, v, t)
            if replacements_i > 0:
                found = True
            num_replacements += replacements_i
        if not found:
            break
    return t, num_replacements

def get_query_to_check_for_markings(the_input, pure_spiritus):
    f = lambda x: x == 0
    sc = (a for a, b in zip(the_input, reversed(pure_spiritus)) if f(b))
    return ''.join(sc)

def analyse_and_convert_query(the_input, force_to_strict=False):
    has_greek = False
    has_beta = False
    has_combining = False
    only_simple_latin = None

    result = None
    pure_spiritus = []
    lookup_mode = None
    errors = []

    for i in the_input:
        if ord(i) in set_greek:
            has_greek = True
        if i in dict_beta:
            has_beta = True
        if has_greek and has_beta:
            break

    has_combining = re.search(regex_combining, the_input)

    # if has_beta:
    if has_beta or has_combining:
        result, _, _errors = to_greek_precise(the_input)
        if _errors:
            errors.extend(_errors)
    else:
        result = the_input
        if not has_greek:
            only_simple_latin = True
    if errors:
        return None, None, None, errors
    # result = result.lower()
    if only_simple_latin:
        lookup_mode = 'loose'
    else:
        result, _ = canonical(result)
    pure_spiritus_info = get_pure_spiritus_info(result)
    if force_to_strict:
        pure_spiritus = None
        has_pure_spiritus = False
    else:
        pure_spiritus = pure_spiritus_info.combined
        has_pure_spiritus = pure_spiritus_info.has_any
    query_to_check_for_markings = (
        get_query_to_check_for_markings(result, pure_spiritus) if has_pure_spiritus else result
    )

    regex_beta = '|'.join(re.escape(x) for x in set_beta)
    # _regex = regex_greek_accented + '|' + regex_beta
    _regex = '|'.join((regex_greek_accented, regex_beta, regex_combining))

    if re.search(_regex, query_to_check_for_markings):
        lookup_mode = 'strict'
        # --- for mixed queries like πῖsos
        result, _, _errors = to_greek_precise(result)
        if _errors:
            errors.extend(_errors)
        # result, _, _ = normalise_macron_breve(result)
    else:
        result, _, _errors = to_latin(result)
        if _errors:
            errors.extend(_errors)
        lookup_mode = 'loose'
    if errors:
        return None, None, None, errors
    return result, lookup_mode, pure_spiritus_info, errors

def regex_greek():
    gr = regex['greek']
    if gr: return gr
    rs = (re.escape(chr(_)) for _ in set_greek)
    rsj = '|'.join(rs)
    regex['greek'] = '(?:%s)' % rsj
    gr = regex['greek']
    return gr

def regex_greek_with_markings():
    return regex_greek_accented

def regex_beta_chars():
    return r'(?:%s)' % '|'.join((
        r'/', r'\\', r'\(', r'\)', '=', r'\+', r'\|', '&', "'",
    ))

# --- unichars '\p{Latin}'
def regex_latin():
    chars = (
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
        'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
        'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ª', 'º',
        'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ',
        'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'Þ', 'ß', 'à', 'á', 'â', 'ã', 'ä',
        'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö',
        'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'þ', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ',
        'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě',
        'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ',
        'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'ĸ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ',
        'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ŋ', 'ŋ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő',
        'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ',
        'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ',
        'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƀ', 'Ɓ', 'Ƃ', 'ƃ', 'Ƅ', 'ƅ', 'Ɔ', 'Ƈ',
        'ƈ', 'Ɖ', 'Ɗ', 'Ƌ', 'ƌ', 'ƍ', 'Ǝ', 'Ə', 'Ɛ', 'Ƒ', 'ƒ', 'Ɠ', 'Ɣ', 'ƕ', 'Ɩ', 'Ɨ', 'Ƙ', 'ƙ',
        'ƚ', 'ƛ', 'Ɯ', 'Ɲ', 'ƞ', 'Ɵ', 'Ơ', 'ơ', 'Ƣ', 'ƣ', 'Ƥ', 'ƥ', 'Ʀ', 'Ƨ', 'ƨ', 'Ʃ', 'ƪ', 'ƫ',
        'Ƭ', 'ƭ', 'Ʈ', 'Ư', 'ư', 'Ʊ', 'Ʋ', 'Ƴ', 'ƴ', 'Ƶ', 'ƶ', 'Ʒ', 'Ƹ', 'ƹ', 'ƺ', 'ƻ', 'Ƽ', 'ƽ',
        'ƾ', 'ƿ', 'ǀ', 'ǁ', 'ǂ', 'ǃ', 'Ǆ', 'ǅ', 'ǆ', 'Ǉ', 'ǈ', 'ǉ', 'Ǌ', 'ǋ', 'ǌ', 'Ǎ', 'ǎ', 'Ǐ',
        'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'ǝ', 'Ǟ', 'ǟ', 'Ǡ', 'ǡ',
        'Ǣ', 'ǣ', 'Ǥ', 'ǥ', 'Ǧ', 'ǧ', 'Ǩ', 'ǩ', 'Ǫ', 'ǫ', 'Ǭ', 'ǭ', 'Ǯ', 'ǯ', 'ǰ', 'Ǳ', 'ǲ', 'ǳ',
        'Ǵ', 'ǵ', 'Ƕ', 'Ƿ', 'Ǹ', 'ǹ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', 'Ȁ', 'ȁ', 'Ȃ', 'ȃ', 'Ȅ', 'ȅ',
        'Ȇ', 'ȇ', 'Ȉ', 'ȉ', 'Ȋ', 'ȋ', 'Ȍ', 'ȍ', 'Ȏ', 'ȏ', 'Ȑ', 'ȑ', 'Ȓ', 'ȓ', 'Ȕ', 'ȕ', 'Ȗ', 'ȗ',
        'Ș', 'ș', 'Ț', 'ț', 'Ȝ', 'ȝ', 'Ȟ', 'ȟ', 'Ƞ', 'ȡ', 'Ȣ', 'ȣ', 'Ȥ', 'ȥ', 'Ȧ', 'ȧ', 'Ȩ', 'ȩ',
        'Ȫ', 'ȫ', 'Ȭ', 'ȭ', 'Ȯ', 'ȯ', 'Ȱ', 'ȱ', 'Ȳ', 'ȳ', 'ȴ', 'ȵ', 'ȶ', 'ȷ', 'ȸ', 'ȹ', 'Ⱥ', 'Ȼ',
        'ȼ', 'Ƚ', 'Ⱦ', 'ȿ', 'ɀ', 'Ɂ', 'ɂ', 'Ƀ', 'Ʉ', 'Ʌ', 'Ɇ', 'ɇ', 'Ɉ', 'ɉ', 'Ɋ', 'ɋ', 'Ɍ', 'ɍ',
        'Ɏ', 'ɏ', 'ɐ', 'ɑ', 'ɒ', 'ɓ', 'ɔ', 'ɕ', 'ɖ', 'ɗ', 'ɘ', 'ə', 'ɚ', 'ɛ', 'ɜ', 'ɝ', 'ɞ', 'ɟ',
        'ɠ', 'ɡ', 'ɢ', 'ɣ', 'ɤ', 'ɥ', 'ɦ', 'ɧ', 'ɨ', 'ɩ', 'ɪ', 'ɫ', 'ɬ', 'ɭ', 'ɮ', 'ɯ', 'ɰ', 'ɱ',
        'ɲ', 'ɳ', 'ɴ', 'ɵ', 'ɶ', 'ɷ', 'ɸ', 'ɹ', 'ɺ', 'ɻ', 'ɼ', 'ɽ', 'ɾ', 'ɿ', 'ʀ', 'ʁ', 'ʂ', 'ʃ',
        'ʄ', 'ʅ', 'ʆ', 'ʇ', 'ʈ', 'ʉ', 'ʊ', 'ʋ', 'ʌ', 'ʍ', 'ʎ', 'ʏ', 'ʐ', 'ʑ', 'ʒ', 'ʓ', 'ʔ', 'ʕ',
        'ʖ', 'ʗ', 'ʘ', 'ʙ', 'ʚ', 'ʛ', 'ʜ', 'ʝ', 'ʞ', 'ʟ', 'ʠ', 'ʡ', 'ʢ', 'ʣ', 'ʤ', 'ʥ', 'ʦ', 'ʧ',
        'ʨ', 'ʩ', 'ʪ', 'ʫ', 'ʬ', 'ʭ', 'ʮ', 'ʯ', 'ʰ', 'ʱ', 'ʲ', 'ʳ', 'ʴ', 'ʵ', 'ʶ', 'ʷ', 'ʸ', 'ˠ',
        'ˡ', 'ˢ', 'ˣ', 'ˤ', '◌ͣ', '◌ͤ', '◌ͥ', '◌ͦ', '◌ͧ', '◌ͨ', '◌ͩ', '◌ͪ', '◌ͫ', '◌ͬ', '◌ͭ', '◌ͮ', '◌ͯ', '◌҅',
        '◌҆', '◌॑', '◌॒', '჻', 'ᴀ', 'ᴁ', 'ᴂ', 'ᴃ', 'ᴄ', 'ᴅ', 'ᴆ', 'ᴇ', 'ᴈ', 'ᴉ', 'ᴊ', 'ᴋ', 'ᴌ', 'ᴍ',
        'ᴎ', 'ᴏ', 'ᴐ', 'ᴑ', 'ᴒ', 'ᴓ', 'ᴔ', 'ᴕ', 'ᴖ', 'ᴗ', 'ᴘ', 'ᴙ', 'ᴚ', 'ᴛ', 'ᴜ', 'ᴝ', 'ᴞ', 'ᴟ',
        'ᴠ', 'ᴡ', 'ᴢ', 'ᴣ', 'ᴤ', 'ᴥ', 'ᴬ', 'ᴭ', 'ᴮ', 'ᴯ', 'ᴰ', 'ᴱ', 'ᴲ', 'ᴳ', 'ᴴ', 'ᴵ', 'ᴶ', 'ᴷ',
        'ᴸ', 'ᴹ', 'ᴺ', 'ᴻ', 'ᴼ', 'ᴽ', 'ᴾ', 'ᴿ', 'ᵀ', 'ᵁ', 'ᵂ', 'ᵃ', 'ᵄ', 'ᵅ', 'ᵆ', 'ᵇ', 'ᵈ', 'ᵉ',
        'ᵊ', 'ᵋ', 'ᵌ', 'ᵍ', 'ᵎ', 'ᵏ', 'ᵐ', 'ᵑ', 'ᵒ', 'ᵓ', 'ᵔ', 'ᵕ', 'ᵖ', 'ᵗ', 'ᵘ', 'ᵙ', 'ᵚ', 'ᵛ',
        'ᵜ', 'ᵢ', 'ᵣ', 'ᵤ', 'ᵥ', 'ᵫ', 'ᵬ', 'ᵭ', 'ᵮ', 'ᵯ', 'ᵰ', 'ᵱ', 'ᵲ', 'ᵳ', 'ᵴ', 'ᵵ', 'ᵶ', 'ᵷ',
        'ᵹ', 'ᵺ', 'ᵻ', 'ᵼ', 'ᵽ', 'ᵾ', 'ᵿ', 'ᶀ', 'ᶁ', 'ᶂ', 'ᶃ', 'ᶄ', 'ᶅ', 'ᶆ', 'ᶇ', 'ᶈ', 'ᶉ', 'ᶊ',
        'ᶋ', 'ᶌ', 'ᶍ', 'ᶎ', 'ᶏ', 'ᶐ', 'ᶑ', 'ᶒ', 'ᶓ', 'ᶔ', 'ᶕ', 'ᶖ', 'ᶗ', 'ᶘ', 'ᶙ', 'ᶚ', 'ᶛ', 'ᶜ',
        'ᶝ', 'ᶞ', 'ᶟ', 'ᶠ', 'ᶡ', 'ᶢ', 'ᶣ', 'ᶤ', 'ᶥ', 'ᶦ', 'ᶧ', 'ᶨ', 'ᶩ', 'ᶪ', 'ᶫ', 'ᶬ', 'ᶭ', 'ᶮ',
        'ᶯ', 'ᶰ', 'ᶱ', 'ᶲ', 'ᶳ', 'ᶴ', 'ᶵ', 'ᶶ', 'ᶷ', 'ᶸ', 'ᶹ', 'ᶺ', 'ᶻ', 'ᶼ', 'ᶽ', 'ᶾ', 'Ḁ', 'ḁ',
        'Ḃ', 'ḃ', 'Ḅ', 'ḅ', 'Ḇ', 'ḇ', 'Ḉ', 'ḉ', 'Ḋ', 'ḋ', 'Ḍ', 'ḍ', 'Ḏ', 'ḏ', 'Ḑ', 'ḑ', 'Ḓ', 'ḓ',
        'Ḕ', 'ḕ', 'Ḗ', 'ḗ', 'Ḙ', 'ḙ', 'Ḛ', 'ḛ', 'Ḝ', 'ḝ', 'Ḟ', 'ḟ', 'Ḡ', 'ḡ', 'Ḣ', 'ḣ', 'Ḥ', 'ḥ',
        'Ḧ', 'ḧ', 'Ḩ', 'ḩ', 'Ḫ', 'ḫ', 'Ḭ', 'ḭ', 'Ḯ', 'ḯ', 'Ḱ', 'ḱ', 'Ḳ', 'ḳ', 'Ḵ', 'ḵ', 'Ḷ', 'ḷ',
        'Ḹ', 'ḹ', 'Ḻ', 'ḻ', 'Ḽ', 'ḽ', 'Ḿ', 'ḿ', 'Ṁ', 'ṁ', 'Ṃ', 'ṃ', 'Ṅ', 'ṅ', 'Ṇ', 'ṇ', 'Ṉ', 'ṉ',
        'Ṋ', 'ṋ', 'Ṍ', 'ṍ', 'Ṏ', 'ṏ', 'Ṑ', 'ṑ', 'Ṓ', 'ṓ', 'Ṕ', 'ṕ', 'Ṗ', 'ṗ', 'Ṙ', 'ṙ', 'Ṛ', 'ṛ',
        'Ṝ', 'ṝ', 'Ṟ', 'ṟ', 'Ṡ', 'ṡ', 'Ṣ', 'ṣ', 'Ṥ', 'ṥ', 'Ṧ', 'ṧ', 'Ṩ', 'ṩ', 'Ṫ', 'ṫ', 'Ṭ', 'ṭ',
        'Ṯ', 'ṯ', 'Ṱ', 'ṱ', 'Ṳ', 'ṳ', 'Ṵ', 'ṵ', 'Ṷ', 'ṷ', 'Ṹ', 'ṹ', 'Ṻ', 'ṻ', 'Ṽ', 'ṽ', 'Ṿ', 'ṿ',
        'Ẁ', 'ẁ', 'Ẃ', 'ẃ', 'Ẅ', 'ẅ', 'Ẇ', 'ẇ', 'Ẉ', 'ẉ', 'Ẋ', 'ẋ', 'Ẍ', 'ẍ', 'Ẏ', 'ẏ', 'Ẑ', 'ẑ',
        'Ẓ', 'ẓ', 'Ẕ', 'ẕ', 'ẖ', 'ẗ', 'ẘ', 'ẙ', 'ẚ', 'ẛ', 'ẜ', 'ẝ', 'ẞ', 'ẟ', 'Ạ', 'ạ', 'Ả', 'ả',
        'Ấ', 'ấ', 'Ầ', 'ầ', 'Ẩ', 'ẩ', 'Ẫ', 'ẫ', 'Ậ', 'ậ', 'Ắ', 'ắ', 'Ằ', 'ằ', 'Ẳ', 'ẳ', 'Ẵ', 'ẵ',
        'Ặ', 'ặ', 'Ẹ', 'ẹ', 'Ẻ', 'ẻ', 'Ẽ', 'ẽ', 'Ế', 'ế', 'Ề', 'ề', 'Ể', 'ể', 'Ễ', 'ễ', 'Ệ', 'ệ',
        'Ỉ', 'ỉ', 'Ị', 'ị', 'Ọ', 'ọ', 'Ỏ', 'ỏ', 'Ố', 'ố', 'Ồ', 'ồ', 'Ổ', 'ổ', 'Ỗ', 'ỗ', 'Ộ', 'ộ',
        'Ớ', 'ớ', 'Ờ', 'ờ', 'Ở', 'ở', 'Ỡ', 'ỡ', 'Ợ', 'ợ', 'Ụ', 'ụ', 'Ủ', 'ủ', 'Ứ', 'ứ', 'Ừ', 'ừ',
        'Ử', 'ử', 'Ữ', 'ữ', 'Ự', 'ự', 'Ỳ', 'ỳ', 'Ỵ', 'ỵ', 'Ỷ', 'ỷ', 'Ỹ', 'ỹ', 'Ỻ', 'ỻ', 'Ỽ', 'ỽ',
        'Ỿ', 'ỿ', 'ⁱ', 'ⁿ', 'ₐ', 'ₑ', 'ₒ', 'ₓ', 'ₔ', 'ₕ', 'ₖ', 'ₗ', 'ₘ', 'ₙ', 'ₚ', 'ₛ', 'ₜ', '◌⃰',
        'K', 'Å', 'Ⅎ', 'ⅎ', 'Ⅰ', 'Ⅱ', 'Ⅲ', 'Ⅳ', 'Ⅴ', 'Ⅵ', 'Ⅶ', 'Ⅷ', 'Ⅸ', 'Ⅹ', 'Ⅺ', 'Ⅻ', 'Ⅼ', 'Ⅽ',
        'Ⅾ', 'Ⅿ', 'ⅰ', 'ⅱ', 'ⅲ', 'ⅳ', 'ⅴ', 'ⅵ', 'ⅶ', 'ⅷ', 'ⅸ', 'ⅹ', 'ⅺ', 'ⅻ', 'ⅼ', 'ⅽ', 'ⅾ', 'ⅿ',
        'ↀ', 'ↁ', 'ↂ', 'Ↄ', 'ↄ', 'ↅ', 'ↆ', 'ↇ', 'ↈ', 'Ⱡ', 'ⱡ', 'Ɫ', 'Ᵽ', 'Ɽ', 'ⱥ', 'ⱦ', 'Ⱨ', 'ⱨ',
        'Ⱪ', 'ⱪ', 'Ⱬ', 'ⱬ', 'Ɑ', 'Ɱ', 'Ɐ', 'Ɒ', 'ⱱ', 'Ⱳ', 'ⱳ', 'ⱴ', 'Ⱶ', 'ⱶ', 'ⱷ', 'ⱸ', 'ⱹ', 'ⱺ',
        'ⱻ', 'ⱼ', 'ⱽ', 'Ȿ', 'Ɀ', 'Ꜣ', 'ꜣ', 'Ꜥ', 'ꜥ', 'Ꜧ', 'ꜧ', 'Ꜩ', 'ꜩ', 'Ꜫ', 'ꜫ', 'Ꜭ', 'ꜭ', 'Ꜯ',
        'ꜯ', 'ꜰ', 'ꜱ', 'Ꜳ', 'ꜳ', 'Ꜵ', 'ꜵ', 'Ꜷ', 'ꜷ', 'Ꜹ', 'ꜹ', 'Ꜻ', 'ꜻ', 'Ꜽ', 'ꜽ', 'Ꜿ', 'ꜿ', 'Ꝁ',
        'ꝁ', 'Ꝃ', 'ꝃ', 'Ꝅ', 'ꝅ', 'Ꝇ', 'ꝇ', 'Ꝉ', 'ꝉ', 'Ꝋ', 'ꝋ', 'Ꝍ', 'ꝍ', 'Ꝏ', 'ꝏ', 'Ꝑ', 'ꝑ', 'Ꝓ',
        'ꝓ', 'Ꝕ', 'ꝕ', 'Ꝗ', 'ꝗ', 'Ꝙ', 'ꝙ', 'Ꝛ', 'ꝛ', 'Ꝝ', 'ꝝ', 'Ꝟ', 'ꝟ', 'Ꝡ', 'ꝡ', 'Ꝣ', 'ꝣ', 'Ꝥ',
        'ꝥ', 'Ꝧ', 'ꝧ', 'Ꝩ', 'ꝩ', 'Ꝫ', 'ꝫ', 'Ꝭ', 'ꝭ', 'Ꝯ', 'ꝯ', 'ꝰ', 'ꝱ', 'ꝲ', 'ꝳ', 'ꝴ', 'ꝵ', 'ꝶ',
        'ꝷ', 'ꝸ', 'Ꝺ', 'ꝺ', 'Ꝼ', 'ꝼ', 'Ᵹ', 'Ꝿ', 'ꝿ', 'Ꞁ', 'ꞁ', 'Ꞃ', 'ꞃ', 'Ꞅ', 'ꞅ', 'Ꞇ', 'ꞇ', 'Ꞌ',
        'ꞌ', 'Ɥ', 'ꞎ', 'ꞏ', 'Ꞑ', 'ꞑ', 'Ꞓ', 'ꞓ', 'ꞔ', 'ꞕ', 'Ꞗ', 'ꞗ', 'Ꞙ', 'ꞙ', 'Ꞛ', 'ꞛ', 'Ꞝ', 'ꞝ',
        'Ꞟ', 'ꞟ', 'Ꞡ', 'ꞡ', 'Ꞣ', 'ꞣ', 'Ꞥ', 'ꞥ', 'Ꞧ', 'ꞧ', 'Ꞩ', 'ꞩ', 'Ɦ', 'Ɜ', 'Ɡ', 'Ɬ', 'Ɪ', 'Ʞ',
        'Ʇ', 'Ʝ', 'Ꭓ', 'Ꞵ', 'ꞵ', 'Ꞷ', 'ꞷ', 'ꟷ', 'ꟸ', 'ꟹ', 'ꟺ', 'ꟻ', 'ꟼ', 'ꟽ', 'ꟾ', 'ꟿ', '꤮', 'ꬰ',
        'ꬱ', 'ꬲ', 'ꬳ', 'ꬴ', 'ꬵ', 'ꬶ', 'ꬷ', 'ꬸ', 'ꬹ', 'ꬺ', 'ꬻ', 'ꬼ', 'ꬽ', 'ꬾ', 'ꬿ', 'ꭀ', 'ꭁ', 'ꭂ',
        'ꭃ', 'ꭄ', 'ꭅ', 'ꭆ', 'ꭇ', 'ꭈ', 'ꭉ', 'ꭊ', 'ꭋ', 'ꭌ', 'ꭍ', 'ꭎ', 'ꭏ', 'ꭐ', 'ꭑ', 'ꭒ', 'ꭓ', 'ꭔ',
        'ꭕ', 'ꭖ', 'ꭗ', 'ꭘ', 'ꭙ', 'ꭚ', 'ꭜ', 'ꭝ', 'ꭞ', 'ꭟ', 'ꭠ', 'ꭡ', 'ꭢ', 'ꭣ', 'ꭤ', 'ﬀ', 'ﬁ', 'ﬂ',
        'ﬃ', 'ﬄ', 'ﬅ', 'ﬆ', 'Ａ', 'Ｂ', 'Ｃ', 'Ｄ', 'Ｅ', 'Ｆ', 'Ｇ', 'Ｈ', 'Ｉ', 'Ｊ', 'Ｋ', 'Ｌ',
        'Ｍ', 'Ｎ', 'Ｏ', 'Ｐ', 'Ｑ', 'Ｒ', 'Ｓ', 'Ｔ', 'Ｕ', 'Ｖ', 'Ｗ', 'Ｘ', 'Ｙ', 'Ｚ', 'ａ',
        'ｂ', 'ｃ', 'ｄ', 'ｅ', 'ｆ', 'ｇ', 'ｈ', 'ｉ', 'ｊ', 'ｋ', 'ｌ', 'ｍ', 'ｎ', 'ｏ', 'ｐ',
        'ｑ', 'ｒ', 'ｓ', 'ｔ', 'ｕ', 'ｖ', 'ｗ', 'ｘ', 'ｙ', 'ｚ'
    )

    return r'(?:%s)' % '|'.join(chars)

def regex_wildcards():
    return r'[\*?]'

def regex_ABC():
    return r'[^>]'

def regex_ABO():
    return r'[^<]'

def regex_dash():
    def x(y):
        pass
    d = ['2d', '2010', '2011', '2012', '2013', '2014', 'fe63', 'ff0d']
    e = (int(_, 16) for _ in d)
    f = (chr(_) for _ in e)
    return r'(?:%s)' % ('|'.join(f))


set_punc = {
    0x0021, 0x0022, 0x0023, 0x0025, 0x0026, 0x0027, 0x0028, 0x0029, 0x002A, 0x002C,
    0x002D, 0x002E, 0x002F, 0x003A, 0x003B, 0x003F, 0x0040, 0x005B, 0x005C, 0x005D,
    0x005F, 0x007B, 0x007D, 0x00A1, 0x00A7, 0x00AB, 0x00B6, 0x00B7, 0x00BB, 0x00BF,
    0x037E, 0x0387, 0x055A, 0x055B, 0x055C, 0x055D, 0x055E, 0x055F, 0x0589, 0x058A,
    0x05BE, 0x05C0, 0x05C3, 0x05C6, 0x05F3, 0x05F4, 0x0609, 0x060A, 0x060C, 0x060D,
    0x061B, 0x061E, 0x061F, 0x066A, 0x066B, 0x066C, 0x066D, 0x06D4, 0x0700, 0x0701,
    0x0702, 0x0703, 0x0704, 0x0705, 0x0706, 0x0707, 0x0708, 0x0709, 0x070A, 0x070B,
    0x070C, 0x070D, 0x07F7, 0x07F8, 0x07F9, 0x0830, 0x0831, 0x0832, 0x0833, 0x0834,
    0x0835, 0x0836, 0x0837, 0x0838, 0x0839, 0x083A, 0x083B, 0x083C, 0x083D, 0x083E,
    0x085E, 0x0964, 0x0965, 0x0970, 0x09FD, 0x0AF0, 0x0DF4, 0x0E4F, 0x0E5A, 0x0E5B,
    0x0F04, 0x0F05, 0x0F06, 0x0F07, 0x0F08, 0x0F09, 0x0F0A, 0x0F0B, 0x0F0C, 0x0F0D,
    0x0F0E, 0x0F0F, 0x0F10, 0x0F11, 0x0F12, 0x0F14, 0x0F3A, 0x0F3B, 0x0F3C, 0x0F3D,
    0x0F85, 0x0FD0, 0x0FD1, 0x0FD2, 0x0FD3, 0x0FD4, 0x0FD9, 0x0FDA, 0x104A, 0x104B,
    0x104C, 0x104D, 0x104E, 0x104F, 0x10FB, 0x1360, 0x1361, 0x1362, 0x1363, 0x1364,
    0x1365, 0x1366, 0x1367, 0x1368, 0x1400, 0x166D, 0x166E, 0x169B, 0x169C, 0x16EB,
    0x16EC, 0x16ED, 0x1735, 0x1736, 0x17D4, 0x17D5, 0x17D6, 0x17D8, 0x17D9, 0x17DA,
    0x1800, 0x1801, 0x1802, 0x1803, 0x1804, 0x1805, 0x1806, 0x1807, 0x1808, 0x1809,
    0x180A, 0x1944, 0x1945, 0x1A1E, 0x1A1F, 0x1AA0, 0x1AA1, 0x1AA2, 0x1AA3, 0x1AA4,
    0x1AA5, 0x1AA6, 0x1AA8, 0x1AA9, 0x1AAA, 0x1AAB, 0x1AAC, 0x1AAD, 0x1B5A, 0x1B5B,
    0x1B5C, 0x1B5D, 0x1B5E, 0x1B5F, 0x1B60, 0x1BFC, 0x1BFD, 0x1BFE, 0x1BFF, 0x1C3B,
    0x1C3C, 0x1C3D, 0x1C3E, 0x1C3F, 0x1C7E, 0x1C7F, 0x1CC0, 0x1CC1, 0x1CC2, 0x1CC3,
    0x1CC4, 0x1CC5, 0x1CC6, 0x1CC7, 0x1CD3, 0x2010, 0x2011, 0x2012, 0x2013, 0x2014,
    0x2015, 0x2016, 0x2017, 0x2018, 0x2019, 0x201A, 0x201B, 0x201C, 0x201D, 0x201E,
    0x201F, 0x2020, 0x2021, 0x2022, 0x2023, 0x2024, 0x2025, 0x2026, 0x2027, 0x2030,
    0x2031, 0x2032, 0x2033, 0x2034, 0x2035, 0x2036, 0x2037, 0x2038, 0x2039, 0x203A,
    0x203B, 0x203C, 0x203D, 0x203E, 0x203F, 0x2040, 0x2041, 0x2042, 0x2043, 0x2045,
    0x2046, 0x2047, 0x2048, 0x2049, 0x204A, 0x204B, 0x204C, 0x204D, 0x204E, 0x204F,
    0x2050, 0x2051, 0x2053, 0x2054, 0x2055, 0x2056, 0x2057, 0x2058, 0x2059, 0x205A,
    0x205B, 0x205C, 0x205D, 0x205E, 0x207D, 0x207E, 0x208D, 0x208E, 0x2308, 0x2309,
    0x230A, 0x230B, 0x329, 0x32A, 0x2768, 0x2769, 0x276A, 0x276B, 0x276C, 0x276D,
    0x276E, 0x276F, 0x2770, 0x2771, 0x2772, 0x2773, 0x2774, 0x2775, 0x27C5, 0x27C6,
    0x27E6, 0x27E7, 0x27E8, 0x27E9, 0x27EA, 0x27EB, 0x27EC, 0x27ED, 0x27EE, 0x27EF,
    0x2983, 0x2984, 0x2985, 0x2986, 0x2987, 0x2988, 0x2989, 0x298A, 0x298B, 0x298C,
    0x298D, 0x298E, 0x298F, 0x2990, 0x2991, 0x2992, 0x2993, 0x2994, 0x2995, 0x2996,
    0x2997, 0x2998, 0x29D8, 0x29D9, 0x29DA, 0x29DB, 0x29FC, 0x29FD, 0x2CF9, 0x2CFA,
    0x2CFB, 0x2CFC, 0x2CFE, 0x2CFF, 0x2D70, 0x2E00, 0x2E01, 0x2E02, 0x2E03, 0x2E04,
    0x2E05, 0x2E06, 0x2E07, 0x2E08, 0x2E09, 0x2E0A, 0x2E0B, 0x2E0C, 0x2E0D, 0x2E0E,
    0x2E0F, 0x2E10, 0x2E11, 0x2E12, 0x2E13, 0x2E14, 0x2E15, 0x2E16, 0x2E17, 0x2E18,
    0x2E19, 0x2E1A, 0x2E1B, 0x2E1C, 0x2E1D, 0x2E1E, 0x2E1F, 0x2E20, 0x2E21, 0x2E22,
    0x2E23, 0x2E24, 0x2E25, 0x2E26, 0x2E27, 0x2E28, 0x2E29, 0x2E2A, 0x2E2B, 0x2E2C,
    0x2E2D, 0x2E2E, 0x2E30, 0x2E31, 0x2E32, 0x2E33, 0x2E34, 0x2E35, 0x2E36, 0x2E37,
    0x2E38, 0x2E39, 0x2E3A, 0x2E3B, 0x2E3C, 0x2E3D, 0x2E3E, 0x2E3F, 0x2E40, 0x2E41,
    0x2E42, 0x2E43, 0x2E44, 0x2E45, 0x2E46, 0x2E47, 0x2E48, 0x2E49, 0x0A0, 0xA4FE,
    0xA4FF, 0xA60D, 0xA60E, 0xA60F, 0xA673, 0xA67E, 0xA6F2, 0xA6F3, 0xA6F4, 0xA6F5,
    0xA6F6, 0xA6F7, 0xA874, 0xA875, 0xA876, 0xA877, 0xA8CE, 0xA8CF, 0xA8F8, 0xA8F9,
    0xA8FA, 0xA8FC, 0xA92E, 0xA92F, 0xA95F, 0xA9C1, 0xA9C2, 0xA9C3, 0xA9C4, 0xA9C5,
    0xA9C6, 0xA9C7, 0xA9C8, 0xA9C9, 0xA9CA, 0xA9CB, 0xA9CC, 0xA9CD, 0xA9DE, 0xA9DF,
    0xAA5C, 0xAA5D, 0xAA5E, 0xAA5F, 0xAADE, 0xAADF, 0xAAF0, 0xAAF1, 0xABEB, 0xFD3E,
    0xFD3F, 0xE10, 0xE11, 0xE12, 0xE13, 0xE14, 0xE15, 0xE16, 0xE17, 0xE18,
    0xE19, 0xE30, 0xE31, 0xE32, 0xE33, 0xE34, 0xE35, 0xE36, 0xE37, 0xE38,
    0xE39, 0xE3A, 0xE3B, 0xE3C, 0xE3D, 0xE3E, 0xE3F, 0xE40, 0xE41, 0xE42,
    0xE43, 0xE44, 0xE47, 0xE48, 0xE49, 0xE4A, 0xE4B, 0xE4C, 0xE4D, 0xE4E,
    0xE4F, 0xE50, 0xE51, 0xE52, 0xE54, 0xE55, 0xE56, 0xE57, 0xE58, 0xE59,
    0xE5A, 0xE5B, 0xE5C, 0xE5D, 0xE5E, 0xE5F, 0xE60, 0xE61, 0xE63, 0xE68,
    0xE6A, 0xE6B, 0xF01, 0xF02, 0xF03, 0xF05, 0xF06, 0xF07, 0xF08, 0xF09,
    0xF0A, 0xF0C, 0xF0D, 0xF0E, 0xF0F, 0xF1A, 0xF1B, 0xF1F, 0xF20, 0xF3B,
    0xF3C, 0xF3D, 0xF3F, 0xF5B, 0xF5D, 0xF5F, 0xF60,
}

set_punc_extra = {
    # --- horizontal ellipsis
    0x2026,
    # --- =
    0x3d,
    # --- (
    0x28,
    # --- )
    0x29,
    # --- :
    0x3a,
}

def regex_punc():
    rr = lambda x: (re.escape(chr(_)) for _ in x)
    rs = '|'.join((*rr(set_punc), *rr(set_punc_extra)))
    return r'(?:%s)' % rs

def _prepare_entities_regexes():
    global entities_regexes
    ec = ((x, y) for x, y in xml_entities.items())
    entities_regexes = [(re.compile(re.escape(x)), y) for x, y in ec]

def expand_entities(x):
    if not entities_regexes:
        _prepare_entities_regexes()
    for rx, repl in entities_regexes:
        x = re.sub(rx, repl, x)
    return x


'''
def has_spiritus(a):

    def f(x):
        if (ord(x), x) in set_spiritus_dasia_pure:
            return -7
        elif (ord(x), x) in set_spiritus_psili_pure:
            return 7
        elif (ord(x), x) in set_spiritus_dasia_mixed:
            return -7
        elif (ord(x), x) in set_spiritus_psili_mixed:
            return 7
        else:
            return 0
    a = [f(x) for x in a]
    if 7 in a or -7 in a:
        return a
    else:
        return []
'''

'''
def has_pure_spiritus(a):

    def f(x):
        if (ord(x), x) in set_spiritus_dasia_pure:
            return -7
        elif (ord(x), x) in set_spiritus_psili_pure:
            return 7
        else:
            return 0
    a = [f(x) for x in a]
    if 7 in a or -7 in a:
        return a
    else:
        return []
'''
