import re

import lxml.etree as etree
from lxml.etree import XMLSyntaxError

from grne.xslt_extensions import (
    ExtensionAddEtymLinks,
    ExtensionAbbrev, ExtensionAbbrevCit,
    ExtensionAdjVerb,
    ExtensionCitGPopup, ExtensionCitHandleBeknopt,
    ExtensionKruisVerw,
    mk_class_extension_resolve_links,
    ExtensionSearchHighlight, ExtensionSearchHighlightEntire,
)
from grne.xslt import format_xslt, make_xslt_extensions
from grne.util.util import get_fulltext_fields

from acatpy.fout import Fout
from acatpy.io.speak import debug, set_level as set_speak_level # noqa
from acatpy.xml import xslt_transform

# set_speak_level('DEBUG')

from config import get as config_get

def get_fulltext_template():
    conf = config_get()
    fields = get_fulltext_fields(conf.schema['schema'])
    template_str = '''*[@class='%s'] | *[ancestor::*[@class='%s']]'''
    fs = (template_str % (field, field) for field in fields)
    return ' | '.join(fs)

fulltext_template = get_fulltext_template()

# --- temporary (see notes in make_site)
def process_remove_etym_links(the_input, parse_input=False):
    return _process(xslt_remove_etym_links(), the_input, parse_input=parse_input, debug_tag='process_remove_etym_links')

def process_add_etym_links(the_input, parse_input=False):
    return _process(xslt_add_etym_links(), the_input, parse_input=parse_input, debug_tag='process_add_etym_links')

def process_resolve_links(the_input, data_naam, data_homonyms, parse_input=False):
    return _process(xslt_resolve_links(), the_input,
                    parse_input=parse_input, extra_data=(data_naam, data_homonyms),
                    debug_tag='process_resolve_links')

def process_kruis_verw(the_input, parse_input=False):
    return _process(xslt_kruis_verw(), the_input, parse_input=parse_input,
                    debug_tag='process_kruis_verw')

def process_adj_verb(the_input, parse_input=False):
    return _process(xslt_adj_verb(), the_input, parse_input=parse_input,
                    debug_tag='process_adj_verb')

def process_abbrev(the_input, parse_input=False):
    return _process(xslt_abbrev(), the_input, parse_input=parse_input, debug_tag='process_abbrev')

def process_remove_comments(the_input, parse_input=False):
    return _process(xslt_remove_comments(), the_input, parse_input=parse_input, debug_tag='process_remove_comments')

def process_cit(the_input, parse_input=False):
    return _process(xslt_cit(), the_input, parse_input=parse_input, debug_tag='process_cit')

def process_abbrev_cit(the_input, parse_input=False, filename='unknown'):
    return _process(xslt_abbrev_cit(), the_input, parse_input=parse_input, debug_tag='process_abbrev_cit', xslt_format={'filename': filename})

def process_beknopt(the_input, parse_input=False, filename='unknown'):
    return _process(xslt_beknopt(), the_input, parse_input=parse_input, debug_tag='process_beknopt', xslt_format={'filename': filename})

'''
i.e. *[@class='gebrW'] | *[ancestor::*[@class='gebrW']]" | ...
for all fulltext fields.
'''

def process_search_highlighting(the_input, terms, parse_input=False, make_snippet=True):
    conf = config_get()

    # --- e.g. 'Plut. Phil. 11.1' -> 'Plut\.\s+Phil\.\s+11\.1'
    def make_entire(term):
        cs = (re.escape(_) for _ in re.split(r'\s+', term))
        return r'\s+'.join(cs)

    def xslt_format_abridge():
        ancestors = conf.main['highlight_snippet_ancestors']
        roots = conf.main['highlight_snippet_roots']
        cls_str = ' or '.join(("@class='%s'" % cls for cls in ancestors))
        rts_str = ' | '.join(("*[@class='%s']" % rt for rt in roots))
        snippet_str = '''
            *[%s][descendant::*[@class='%s']]
        ''' % (
            cls_str,
            conf.main['search_highlight_class'],
        )
        # debug('snippet_str %s' % snippet_str)
        return {
            'snippet_expr': snippet_str,
            'roots_expr': rts_str,
        }

    cur = the_input
    impossible_match = '$.'
    for term, do_entire in terms:
        xslt_format1 = {
            'term': term,
            'fulltext_template': fulltext_template,
            'entire_template': make_entire(term) if do_entire else impossible_match,
        }
        err, cur = _process(
            xslt_search_highlighting(), cur,
            parse_input=parse_input,
            debug_tag='process_search_highlighting',
            xslt_format=xslt_format1,
        )
        if err:
            return err, None
        parse_input = False
    if make_snippet:
        cutoff = conf.main['search_highlight_abridge_cutoff_length']
        as_str = etree.tostring(cur, encoding='unicode')
        the_len = len(as_str)
        if the_len >= cutoff:
            abridge_fun, debug_tag = (
                xslt_search_highlight_snippet_abridge,
                'process_search_highlighting_snippet_abridge',
            )
        else:
            abridge_fun, debug_tag = (
                xslt_search_highlight_snippet_no_abridge,
                'process_search_highlighting_snippet_no_abridge',
            )
        err, cur = _process(
            abridge_fun(),
            cur,
            parse_input=False,
            debug_tag=debug_tag,
            xslt_format=xslt_format_abridge(),
        )
        as_str = etree.tostring(cur, encoding='unicode')
        if err:
            return err, None
    return None, cur

# --- `extra_data` gets passed to the extension class.
def _process(xslt, the_input, parse_input=None, debug_tag='', xslt_format=None, extra_data=None):
    if parse_input:
        try:
            xml_tree = etree.fromstring(the_input)
        except XMLSyntaxError as e:
            fout = Fout('malformed HTML', e, the_input)
            return fout, None
    else:
        xml_tree = the_input
    debug('%s: before transform, xml_tree: %s' % (debug_tag, etree.tostring(xml_tree)))
    err, transformed = _transform_xslt(xml_tree, format_xslt(xslt, xslt_format), extra_data)
    if err:
        fout = Fout('Unable to apply XSLT transform', err)
        return fout, None
    return None, transformed

def _transform_xslt(xml_tree, xslt, extra_data):
    extensions = make_xslt_extensions([
        ('add-etym-links', ExtensionAddEtymLinks),
        ('abbrev', ExtensionAbbrev),
        ('abbrev-cit', ExtensionAbbrevCit),
        ('adj-verb', ExtensionAdjVerb),
        ('citG-popup', ExtensionCitGPopup),
        ('cit-handle-beknopt', ExtensionCitHandleBeknopt),
        ('kruis-verw', ExtensionKruisVerw),
        ('resolve-links', mk_class_extension_resolve_links(extra_data)),
        ('search-highlight', ExtensionSearchHighlight),
        ('search-highlight-entire', ExtensionSearchHighlightEntire),
    ])
    err, transformed = xslt_transform(xml_tree, xslt, extensions=extensions)
    return err, transformed

# --- In many of these xslt stylesheets we match on the required condition
# OR an ancestor matching that condition. See xslt_resolve_links for an
# example. The reason is to make recursion work properly in the transformer.

def xslt_remove_comments():
    return """
    <xsl:stylesheet
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:my="{ns}"
      extension-element-prefixes="my"
      version="1.0"
    >
      <xsl:template match="comment()"/>
      <xsl:template match="node() | @*">
        <xsl:copy>
          <xsl:apply-templates select="node() | @*"/>
        </xsl:copy>
      </xsl:template>
    </xsl:stylesheet>
    """

def xslt_remove_etym_links():
    return r'''
    <xsl:stylesheet
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      version="1.0"
    >
      <xsl:template match="//*[@class='etym']//*[@class='link']">
        <xsl:apply-templates select="node()"/>
      </xsl:template>
      <xsl:template match="node()|@*">
        <xsl:copy>
          <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
      </xsl:template>
    </xsl:stylesheet>
    '''

def xslt_add_etym_links():
    return r'''
    <xsl:stylesheet
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:my="{ns}"
      extension-element-prefixes="my"
      version="1.0"
    >
      <xsl:template match="
        *[@class='etym'] | *[ancestor::*[@class='etym']]
      ">
        <xsl:copy>
          <xsl:copy-of select="@*"/>
          <my:add-etym-links/>
        </xsl:copy>
      </xsl:template>
      <xsl:template match="node()|@*">
        <xsl:copy>
          <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
      </xsl:template>
    </xsl:stylesheet>
    '''

def xslt_resolve_links():
    return r'''
    <xsl:stylesheet
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:my="{ns}"
      extension-element-prefixes="my"
      version="1.0"
    >
      <!-- We don't use xsl:copy here. Drop the outer element, pass the
      matched node as input_node to the extension, and have the extension
      output the replacement node. -->
      <xsl:template match="
        *[contains(./@class, 'link')]
      ">
        <my:resolve-links/>
      </xsl:template>
      <!-- This is for an inner element like <r>, e.g. <div class=link><div
      class=r>xxx</div></div>. It's necessary because the extension recurses
      using self.apply_templates. We do need xsl:copy here or else the inner
      element would get dropped. -->
      <xsl:template match="
        *[ancestor::*[contains(./@class, 'link')]]
      ">
        <xsl:copy>
          <xsl:copy-of select="@*"/>
          <my:resolve-links/>
        </xsl:copy>
      </xsl:template>
      <xsl:template match="node()|@*">
        <xsl:copy>
          <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
      </xsl:template>
    </xsl:stylesheet>
    '''

def xslt_kruis_verw():
    return r'''
    <xsl:stylesheet
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:re="http://exslt.org/regular-expressions"
      xmlns:my="{ns}"
      exclude-result-prefixes="re"
      extension-element-prefixes="my"
      version="1.0"
    >
      <xsl:template match="
        //div[@class='kruisVerw']
        [div[@class='i']
          [re:test(text(), '\s*(zie|van|voor)\s*', '')]
        ]
        [text()]
      ">
        <xsl:copy select=".">
          <xsl:copy-of select="@*"/>
          <my:kruis-verw/>
        </xsl:copy>
      </xsl:template>
      <xsl:template match="node()|@*">
        <xsl:copy>
          <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
      </xsl:template>
    </xsl:stylesheet>
    '''

def xslt_adj_verb():
    return r'''
    <xsl:stylesheet
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:re="http://exslt.org/regular-expressions"
      xmlns:my="{ns}"
      exclude-result-prefixes="re"
      extension-element-prefixes="my"
      version="1.0"
    >
      <xsl:template match="//div
        [@class='r']
        [parent::div[@class='morfI']
        [re:test(text(), 'adj.+?verb.+?van', '')]
      ]">
        <xsl:copy select=".">
          <xsl:copy-of select="@*"/>
          <my:adj-verb/>
        </xsl:copy>
      </xsl:template>
      <xsl:template match="node()|@*">
        <xsl:copy>
          <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
      </xsl:template>
    </xsl:stylesheet>
    '''

def xslt_abbrev():
    return """
    <xsl:stylesheet
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:my="{ns}"
      extension-element-prefixes="my"
      version="1.0"
    >
      <xsl:template match="
        *[@class='hoofdW'] | *[ancestor::*[@class='hoofdW']] |
        *[@class='gebrW'] | *[ancestor::*[@class='gebrW']] |
        *[@class='morfI'] | *[ancestor::*[@class='morfI']] |
        *[@class='toel'] | *[ancestor::*[@class='toel']]
      ">
        <xsl:copy>
          <xsl:copy-of select="@*"/>
          <my:abbrev/>
        </xsl:copy>
      </xsl:template>
      <xsl:template match="node() | @*">
        <xsl:copy>
          <xsl:apply-templates select="node() | @*"/>
        </xsl:copy>
      </xsl:template>
    </xsl:stylesheet>
    """

def xslt_cit():
    return """
    <xsl:stylesheet
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:my="{ns}"
      extension-element-prefixes="my"
      version="1.0"
    >
      <xsl:template match="
        *[@class='citG'] | *[ancestor::*[@class='citG']]
      ">
        <xsl:copy>
          <xsl:copy-of select="@*"/>
          <my:citG-popup/>
        </xsl:copy>
      </xsl:template>
      <xsl:template match="node() | @*">
        <xsl:copy>
          <xsl:apply-templates select="node() | @*"/>
        </xsl:copy>
      </xsl:template>
    </xsl:stylesheet>
    """

def xslt_abbrev_cit():
    return """
    <xsl:stylesheet
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:my="{ns}"
      extension-element-prefixes="my"
      version="1.0"
    >
      <xsl:template match="
        *[@class='verw']
      ">
        <xsl:copy>
          <xsl:copy-of select="@*"/>
          <xsl:attribute name="data-abbr-verw">
            <my:abbrev-cit>
              <xsl:element name="filename">
                <xsl:value-of select="
                  '{filename}'
                "/>
              </xsl:element>
              <xsl:element name="aut">
                <xsl:value-of select="
                  div[@class='aut'][1]
                "/>
              </xsl:element>
              <xsl:element name="werk">
                <xsl:value-of select="
                  div[@class='werk'][1]
                "/>
              </xsl:element>
              <xsl:element name="plaats">
                <xsl:value-of select="
                  div[@class='plaats'][1]
                "/>
              </xsl:element>
            </my:abbrev-cit>
          </xsl:attribute>
          <xsl:apply-templates select="node() | @*"/>
        </xsl:copy>
      </xsl:template>
      <xsl:template match="node() | @*">
        <xsl:copy>
          <xsl:apply-templates select="node() | @*"/>
        </xsl:copy>
      </xsl:template>
    </xsl:stylesheet>
    """

def xslt_beknopt():
    return """
    <xsl:stylesheet
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:my="{ns}"
      extension-element-prefixes="my"
      version="1.0"
    >
      <xsl:template match="node() | @*">
        <xsl:choose>
          <!-- * means element node -->
          <xsl:when test="
            @class!='cit' and
            following-sibling::*[1][@class='cit']
          ">
            <xsl:copy>
                  <!--
                    Given input like: <a/><cit/><cit/><cit/><b/><cit/><cit/><cit/>
                    and the cursor is on <a/>,' return only the last cit in
                    the first group immediately adjacent to <a/>.

                    Explanation:

                    • take the following-sibling::cit axis
                    • narrow it using the predicate: the first adjacent following sibling is not cit
                    • take the first item of that.
                  -->
              <xsl:copy-of select="@*"/>
              <my:cit-handle-beknopt>
                <xsl:element name="last-cit">
                  <xsl:value-of select="
                    string((following-sibling::div[@class='cit'][not(following-sibling::*[1][@class='cit'])])[1])
                  "/>
                </xsl:element>
                <xsl:element name="filename">
                  <xsl:value-of select="
                    '{filename}'
                  "/>
                </xsl:element>
              </my:cit-handle-beknopt>
            </xsl:copy>
          </xsl:when>
          <xsl:otherwise>
            <xsl:copy>
              <xsl:apply-templates select="node() | @*"/>
            </xsl:copy>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:template>
    </xsl:stylesheet>
    """

'''
A few tricky cases are not currently matched by this template, when a
matched string transcends tag boundaries:

e.g.

    <vertM>a <b>big</b> dog</vertM>
    <vertM>an animal <toel>(big dog)</toel></vertM>

We can recover a lot of these cases using 'entire' mode, in which we use the
`string()` (and related) function(s), match it using a regex, and then
highlight the entire field.

'''

def xslt_search_highlighting():
    return """
    <xsl:stylesheet
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:my="{ns}"
      xmlns:re="http://exslt.org/regular-expressions"
      exclude-result-prefixes="re"
      extension-element-prefixes="my"
      version="1.0"
    >
      <xsl:template match="{fulltext_template}">
        <xsl:copy>
          <xsl:copy-of select="@*"/>
          <my:search-highlight>
            <xsl:element name="terms">
              <xsl:value-of select="
                '{term}'
              "/>
            </xsl:element>
          </my:search-highlight>
        </xsl:copy>
      </xsl:template>
      <xsl:template match="*[@class='verw'][re:match(string(), '{entire_template}')]">
        <xsl:copy>
          <xsl:copy-of select="@*"/>
          <my:search-highlight-entire/>
        </xsl:copy>
      </xsl:template>
      <xsl:template match="node()|@*">
        <xsl:copy>
          <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
      </xsl:template>
      <xsl:template match="processing-instruction()|comment()"/>
    </xsl:stylesheet>
    """

def xslt_search_highlight_snippet_abridge():
    return '''
    <xsl:stylesheet
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:re="http://exslt.org/regular-expressions"
      exclude-result-prefixes="re"
      version="1.0"
    >
      <xsl:template match="{snippet_expr}">
        <xsl:copy>
          <xsl:copy-of select="node()|@*"/>
          <xsl:value-of select="' … '"/>
        </xsl:copy>
      </xsl:template>
      <xsl:template match="{roots_expr}">
        <div class="snippet">
          <xsl:apply-templates select="node()|@*"/>
        </div>
      </xsl:template>
      <xsl:template match="node()|@*">
        <xsl:apply-templates select="node()|@*"/>
      </xsl:template>
      <xsl:template match="processing-instruction()|comment()"/>
    </xsl:stylesheet>
    '''

def xslt_search_highlight_snippet_no_abridge():
    return '''
    <xsl:stylesheet
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:re="http://exslt.org/regular-expressions"
      exclude-result-prefixes="re"
      version="1.0"
    >
      <xsl:template match="/*">
        <div class="snippet">
          <xsl:apply-templates select="./*"/>
        </div>
      </xsl:template>
      <xsl:template match="node()|@*">
        <xsl:copy>
          <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
      </xsl:template>
      <xsl:template match="processing-instruction()|comment()"/>
    </xsl:stylesheet>
    '''
