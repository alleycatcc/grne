# -*- coding: utf-8 -*-

import json
import re

from antlr4 import CommonTokenStream, ParseTreeWalker, Token
from antlr4.InputStream import InputStream

from antlr4.error.ErrorListener import ErrorListener as BaseErrorListener

from grne.chars import analyse_and_convert_query
from grne.query_parser.antlr4_parser.QueryLexer import QueryLexer as BaseLexer
from grne.query_parser.antlr4_parser.QueryParser import QueryParser
from grne.query_parser.antlr4_parser.QueryListener import QueryListener as BaseListener

DEBUG_LEXER = False
DEBUG_PARSER = False

class ProgrammerError(Exception): pass
class ListenerError(Exception): pass

def transform_term(s):
    new, mode, pure_spiritus_info, errors = analyse_and_convert_query(s, force_to_strict=True)
    if errors:
        return errors, None, None, None
    return None, new, mode, pure_spiritus_info

def process_p_term_value(s, quoted=True):
    st = s.strip()
    if not quoted:
        st = json.dumps(st)
    return st

'''
We disallow spaces in a-expressions in certain cases, because they lead to
an invalid solr query:
e.g.,
  (field "a b")
would become
  field:a b
'''

# --- @todo compile
allowed_double_quote_patterns = (
    # --- "\"van vader\""
    r'^"\\".+\\""$',
    # --- "a)gh""
    r'^"\S+"$',
)

def process_a_term_value(s):
    st = s.strip()
    is_complex_phrase = None
    found_dq = None
    for rx in allowed_double_quote_patterns:
        if not re.match(rx, st): continue
        err = None
        found_dq = True
        try:
            st = json.loads(st)
            if st == r'""':
                err = 'Empty JSON in string: %s' % st
            break
        except json.JSONDecodeError:
            err = 'Invalid JSON in string: %s' % st
        if err:
            return err, None, None
    if not found_dq and re.match(r'^".*"$', st):
        err = 'Invalid use of double-quotes: %s' % s
        return err, None, None
    if re.match('^".*"$', st):
        is_complex_phrase = True
    errors, new, _, _ = transform_term(st)
    if errors:
        err = 'Error converting query (invalid beta-code?) make_repr=%s errors=%s' % (s, errors)
        return err, None, None
    return None, new, bool(is_complex_phrase)

class Config(object):
    def __init__(self, p_all_field=None, p_one_field=None):
        '''
        'all' field, i.e., given a p-query
            $ "water" $ and $ "plato"
        and a p_all_field of '_text_', then you get
            +_text_:water +_text_:plato
        '''
        self.p_all_field = p_all_field

        '''
        'at least one' field
        '''
        self.p_one_field = p_one_field

class State(object):
    def __init__(self):
        self.is_complex_phrase = None
        # --- keep a flat list of all p-terms which are encountered, which
        # we need for building expressions involving the 'at least one'
        # field.
        self.terms = []

    def update(self, is_complex_phrase=None):
        if is_complex_phrase is not None:
            self.is_complex_phrase = is_complex_phrase

def join_ok(s, xs):
    return s.join(x for x in xs if x is not None)

class Error(object):
    def __init__(self):
        self.errors = []

    def has(self):
        return bool(self.errors)

    def add(self, e):
        self.errors.append(e)

    def fmt(self, tag=None, joiner=None):
        ee = lambda e: join_ok(' ', [
            'Error:',
            '%s:' % tag if tag else None,
            e,
        ])
        es = (ee(x) for x in self.errors)
        return joiner.join(es) if joiner else list(es)

'''
The P-family of classes (e.g. PBool) all inherit from Node, as do the
A-family of classes. `state` and `config` are global to the entire parser
and each node gets a handle to them (`self._state` and `self._config`)

`self._children` contains the children of the node.
'''

class Node(object):
    def __init__(self, state=None, config=None, error_tag=None):
        self._children = []
        self._state = state
        self._config = config
        self._error_tag = error_tag
        self.error = Error()

        if not state: self.error.add('no state!')
        if not config: self.error.add('no config!')

    def make_repr(self):
        if self.error.has():
            raise ListenerError(self.errors())
        rs = (x.make_repr() for x in self._children)
        return ' '.join(rs)

    def errors(self):
        tag = self._error_tag or type(self)
        return self.error.fmt(tag=tag, joiner='/')

    def add_node(self, s):
        self._children.append(s)

    def update_state(self, is_complex_phrase=None):
        self._state.update(is_complex_phrase=is_complex_phrase)

class PBool(Node):
    def __init__(self, op, *args, field=None, **kw):
        super().__init__(*args, **kw)
        self._op = op

    def make_repr(self):
        if self.error.has():
            raise ListenerError(self.errors())
        op = self._op
        rs = (x.make_repr() for x in self._children)
        return '(%s %s)' % (op, ' '.join(rs))

    # @todo is this used?
    def add_term(self, x):
        return self.add_node(x)

class PNot(PBool):
    def __init__(self, *args, **kw):
        super().__init__(None, *args, **kw, error_tag='PNot')

    def make_repr(self):
        dl = len(self._children)
        if dl != 2:
            self.error.add("Expected exactly 2 nodes, got %s" % dl)

        if self.error.has():
            raise ListenerError(self.errors())

        dc = (x.make_repr() for x in self._children)
        return '(and %s (not %s))' % tuple(dc)

class POr(PBool):
    def __init__(self, *args, **kw):
        super().__init__('or', *args, **kw, error_tag='POr')

class PAnd(PBool):
    def __init__(self, *args, **kw):
        super().__init__('and', *args, **kw, error_tag='PAnd')

class PAtom(Node):
    # --- `field` is usually not given, in which case it gets the value of
    # the 'all' field when the tree is built.
    def __init__(self, text, *args, field=None, **kw):
        super().__init__(*args, **kw, error_tag='PAtom')
        self.text = text
        self.field = field

    def make_repr(self):
        if self.error.has():
            raise ListenerError(self.errors())
        config = self._config
        state = self._state
        state.terms.append(self.text)
        field = self.field or config.p_all_field
        return '(%s %s)' % (field, self.text)

class ABool(Node):
    def __init__(self, op, *args, **kw):
        super().__init__(*args, **kw)
        self.op = op

    def make_repr(self, op_override=lambda _, op: op):
        def fmt(x):
            op = op_override(x, self.op)
            if isinstance(x, tuple):
                key, value = x
                return '%s%s:%s' % (op, key, value)
            return '%s%s' % (op, x.make_repr())
        if self.error.has():
            raise ListenerError(self.errors())
        rs = (fmt(x) for x in self._children)
        rss = ' '.join(rs)
        needs_parens = len(self._children) > 1
        return '(%s)' % rss if needs_parens else rss

    def add_term(self, key, value):
        errors, newval, is_complex_phrase = process_a_term_value(value)
        if errors:
            self.error.add(errors)
            return
        if is_complex_phrase:
            self.update_state(is_complex_phrase=True)
        self._children.append((key, newval))

class ANot(ABool):
    def __init__(self, *args, **kw):
        super().__init__('-', *args, **kw)

class AAnd(ABool):
    def __init__(self, *args, **kw):
        super().__init__('+', *args, **kw)

    def make_repr(self):
        def op_override(node, op):
            if isinstance(node, ANot):
                return ''
            return op
        return super().make_repr(op_override)

class AOr(ABool):
    def __init__(self, *args, **kw):
        super().__init__('', *args, **kw)

class QueryLexer(BaseLexer):
    def debug_lexer(self, tag):
        if not DEBUG_LEXER: return
        print('[lex] %s -> %s' % (tag, self.text))

'''
QueryListener has methods '_ascend' and '_descend'.

`self._root` is a Node. The root node is always `AOr`.

`self._descend` and `self._ascend` are called by the listener hooks
(enter/exitX). Descend (corresponding to enter) is where new nodes are
instantiated and the tree is built. Ascend (corresponding to exit) is for
backing up to a previous level. `self._cur` and `self._prev` are used to
navigate up and down in the levels.
'''

class QueryListener(BaseListener):
    def __init__(self, p_all_field=None):
        self._root = None
        self.type = None
        self.config = Config(p_all_field=p_all_field)
        self.state = State()

    def make_repr(self):
        rep = self._root.make_repr()
        state = self.state
        if state.is_complex_phrase:
            return '{!complexphrase inOrder=true}' + rep
        return rep

    def _initQuery(self, cls, query_type):
        self._root = AOr(state=self.state, config=self.config)
        self._prev = []
        self._cur = self._root
        self.type = query_type

    def _descend(self, cls):
        self._prev.append(self._cur)
        self._cur = cls(state=self.state, config=self.config)

    def _ascend(self):
        prev = self._prev.pop()
        prev.add_node(self._cur)
        self._cur = prev

    def enterStartPExprFull(self, ctx):
        self._initQuery(Node, 'p-expr')

    def enterStartAQueryExpr(self, ctx):
        self._initQuery(AOr, 'a-expr')

    def enterSimpleExpr(self, ctx):
        field = ctx.field().getText()
        ts = ctx.searchText()
        if (len(ts) > 1):
            self._descend(AAnd)
        else:
            self._descend(AOr)
        for x in ctx.searchText():
            self._cur.add_term(field, x.getText())

    def exitSimpleExpr(self, ctx):
        prev = self._prev.pop()
        prev.add_node(self._cur)
        self._cur = prev

    def enterNotExpr(self, ctx):
        self._descend(ANot)

    def exitNotExpr(self, ctx):
        self._ascend()

    def enterAndExpr(self, ctx):
        self._descend(AAnd)

    def exitAndExpr(self, ctx):
        self._ascend()

    def enterOrExpr(self, ctx):
        self._descend(AOr)

    def exitOrExpr(self, ctx):
        self._ascend()

    def enterPExprFieldAtLeastField(self, ctx):
        field_name = ctx.pAtomDQ().getText()
        field_name = field_name[1:len(field_name) - 1]
        config = self.config
        config.p_one_field = field_name

    # --- this can be considered as the end of a p-expr parse.
    def exitPExprFull(self, ctx):
        # --- @todo this is an AOr object so it's a bit counter-intuitive that
        # it's populated with P-objects (though it works fine).
        root = self._root
        # --- walk the entire tree once to ensure that `terms` gets
        # populated.
        root.make_repr()
        state = root._state
        config = root._config
        terms = state.terms
        p_one_field = config.p_one_field

        # --- @todo why doesn't this raise a ListenerError?
        AAnd()

        if p_one_field:
            top_node = root._children[0]
            if len(root._children) != 1:
                # @todo error
                raise Exception('len(root._children) != 1')
            or_clause = POr(state=state, config=config)
            for term in terms:
                or_clause.add_node(PAtom(term, field=p_one_field, state=state, config=config))
            and_node = PAnd(state=state, config=config)
            and_node.add_node(top_node)
            and_node.add_node(or_clause)
            # @todo
            root._children = [and_node]

    def enterPExprFull(self, ctx):
        pass

    def exitPExprFieldAtLeastField(self, ctx):
        pass

    def enterPExprFieldAllField(self, ctx):
        field_name = ctx.pAtomDQ().getText()
        field_name = field_name[1:len(field_name) - 1]
        config = self.config
        config.p_all_field = field_name

    def exitPExprFieldAllField(self, ctx):
        pass

    def enterPExprSeries(self, ctx):
        self._descend(PAnd)

    def exitPExprSeries(self, ctx):
        self._ascend()

    '''
    def enterPExprBool(self, ctx):
        op = ctx.pBool().getText()
        if op == 'and':
            opNode = PAnd
        elif op == 'or':
            opNode = POr
        elif op == 'not':
            opNode = PNot
        else:
            raise ProgrammerError('Invalid op: %s' % op)
        self._descend(opNode)
    '''

    def exitPExprBool(self, ctx):
        self._ascend()

    def enterPExprAnd(self, ctx):
        self._descend(PAnd)

    def exitPExprAnd(self, ctx):
        self._ascend()

    def enterPExprOr(self, ctx):
        self._descend(POr)

    def exitPExprOr(self, ctx):
        self._ascend()

    def enterPExprNot(self, ctx):
        self._descend(PNot)

    def exitPExprNot(self, ctx):
        self._ascend()

    def _enterPExprSimple(self, text, quoted=True):
        new = process_p_term_value(text, quoted=quoted)
        self._cur.add_node(PAtom(new, state=self.state, config=self.config))

    def enterPExprSimple(self, ctx):
        text = ctx.pAtomDQ().getText()
        return self._enterPExprSimple(text, quoted=True)

    def enterPExprSimpleBare(self, ctx):
        ts = (_.getText() for _ in ctx.pAtomBare())
        text = ''.join(ts)
        return self._enterPExprSimple(text, quoted=False)

class ErrorListener(BaseErrorListener):
    def __init__(self):
        self.has_errors = False
        self.full_context = False

    def syntaxError(self, *args):
        self.has_errors = True

    def reportAmbiguity(self, *args):
        self.has_errors = True

    def reportAttemptingFullContext(self, *args):
        self.full_context = True

    # --- successful, after full context was attempted.
    # --- we don't clear has_errors here.
    def reportContextSensitivity(*args):
        pass

def get_parser_error_listener():
    listener = ErrorListener()
    return listener

def get_parser(query, p_all_field=None):
    listener = QueryListener(p_all_field=p_all_field)
    walker = ParseTreeWalker()

    lexer = QueryLexer(InputStream(query))
    token_stream = CommonTokenStream(lexer)
    parser = QueryParser(token_stream)

    if not DEBUG_PARSER:
        for x in lexer, parser:
            x.removeErrorListeners()

    error_listener = get_parser_error_listener()
    parser.addErrorListener(error_listener)

    # --- `parse` is a separate function, to possibly allow faster reparsing
    # in the future.

    def parse():
        tree = parser.start()

        if DEBUG_PARSER:
            print(tree.toStringTree(recog=parser))

        has_parse_errors = error_listener.has_errors

        # --- didn't consume entire stream, treat as error
        if lexer.nextToken().type != Token.EOF:
            has_parse_errors = True

        if has_parse_errors:
            return True, None, None, None

        walker.walk(listener, tree)

        try:
            output = listener.make_repr()
        except ListenerError:
            return (
                True,
                None,
                None,
                error_listener.full_context,
            )

        return (
            False,
            output,
            listener.type,
            error_listener.full_context
        )

    return parse

# --- walk a tree, starting at `node`, and return an array containing a flat
# list of all its descendants.

def walk_nodes(node):
    nodes = []

    def inner(node):
        children = node._children
        if not children:
            nodes.append(node.make_repr())
        for d in children:
            inner(d)
    inner(node)
    return nodes
