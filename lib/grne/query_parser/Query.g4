grammar Query;

start:   PM pExprFull              # StartPExprFull
    |    aQueryExpr                # StartAQueryExpr
    ;

aQueryExpr: aCellExpr
    |       aBareExpr
    ;

aCellExpr: LP aBareExpr RP
    ;

// --- not sensitive to precedence ordering.

aBareExpr: 'and' (WS? aCellExpr)+  # AndExpr
    |      'or'  (WS? aCellExpr)+  # OrExpr
    |      'not' (WS? aCellExpr)+  # NotExpr
    |      field (WS? searchText)+ # SimpleExpr
    ;

field: TEXT
    ;

searchText: DQSTRING
    |       TEXT
    ;

pExprFull: pExpr pExprFieldAllField? pExprFieldAtLeastField?
    ;

pExprFieldAllField: PF pAtomDQ
    ;

pExprFieldAtLeastField: PL pAtomDQ
    ;

// pExprField: PF pAtomDQ             # pExprFieldAllField
    // |       PL pAtomDQ             # pExprFieldAtLeastField
    // ;

// --- sensitive to precedence ordering.

pExpr: pExpr WS pExpr              # pExprSeries
    |  pExpr WS 'and' WS pExpr     # pExprAnd
    |  pExpr WS 'or'  WS pExpr     # pExprOr
    |  pExpr WS 'not' WS pExpr     # pExprNot
    |  pAtomDQ                     # pExprSimple

    // --- causes precedence error (parser throws).
    // |  pAtomBare+                  # pExprSimpleBare
    ;

pAtomDQ: DQSTRING
    ;

pAtomBare: TEXT
    |      LP
    |      RP
    ;

LP : '('               { self.debug_lexer('LP')}   ;
RP : ')'               { self.debug_lexer('RP')}   ;
DQ : '"'               { self.debug_lexer('DQ')}   ;
ESCCHAR : '\\' ~[\\]   { self.debug_lexer('ESCCHAR')} ;
ESCBS : '\\\\'         { self.debug_lexer('ESCBS')} ;

// --- @todo is it necessary to exclude $, % etc?
TEXT : ~[ \t\n()$"\\]+ { self.debug_lexer('TEXT')} ;

WS : [ \t\n]+          { self.debug_lexer('WS')}   ;
PM : WS? '$' WS?       { self.debug_lexer('PM')}   ;
PF : WS? [ %∀ ] WS?    { self.debug_lexer('PF')}   ;
PL : WS? [ #∃ ] WS?    { self.debug_lexer('PL')}   ;
DQSTRING : DQ (TEXT | LP | RP | PM | WS | ESCCHAR | ESCBS)+ DQ
                       { self.debug_lexer('DQSTRING')} ;
