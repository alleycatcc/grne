# -*- coding: utf-8 -*-

from lxml import etree

from grne.transform_html import process_search_highlighting

def highlight_search_terms(lemma_html, terms, make_snippet=None):
    err, transformed = process_search_highlighting(lemma_html, terms, parse_input=True, make_snippet=make_snippet)
    as_str = etree.tostring(transformed, encoding='unicode')
    return err, as_str
