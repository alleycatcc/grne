# -*- coding: utf-8 -*-

# --- db.execute, fh.write, fh.read, and various functions like `mkdir`
# throw, as well as _post_process.

from functools import partial
from hashlib import sha256
import re
from sys import stdout

import lxml.html
from lxml import etree

from grne.chars import canonical
from grne.lemma_to_html.transform import lemma_to_html
from grne.school_reductie_xslt import xslt_t1_0, xslt_t1_1, xslt_t1_2, xslt_t1_3, xslt_t2_1
from grne.search_highlight import highlight_search_terms
from grne.transform_html import (
    process_abbrev, process_abbrev_cit,
    process_add_etym_links, process_adj_verb,
    process_beknopt, process_cit,
    process_kruis_verw,
    process_remove_comments, process_remove_etym_links,
    process_resolve_links,
)
from grne.util.util import (
    APIException,
    file_is_empty, exists_and_readable, print_exception,
    get_filename_stub, read_xml_from_file,
    open_db,
    parse_xml, get_xml_status_t1,
)
from grne.util.util_redis import redis_cache

from acatpy.colors import bright_red, yellow, green
from acatpy.io.io import die
from acatpy.io.speak import (
    info, warn, debug,
    set_level as set_speak_level,
    get_level as get_speak_level,
)
from acatpy.speak import info_pref
from acatpy.util import slurp
from acatpy.xml import xslt_transform, xslt_transform_saxon_java

from config import get as config_get

# set_speak_level('DEBUG')

class CacheCounter(object):
    def __init__(self):
        self.num_hits = 0
        self.num_misses = 0

    def hit(self):
        self.num_hits += 1

    def miss(self):
        self.num_misses += 1

'''
For lemma to html transformations, which are called both during the build
and by the server when re-rendering lemmata.
'''

cache = {
    # --- lemma db.
    'db': None,
    'data_naam': {},
    'data_homonyms': {},
}

DEBUG_T2_CACHE = False
# --- @todo
DEBUG_FORCE_T2 = False

class O(): pass # noqa

def init_render_table(db):
    with db:
        db.execute('delete from render')

def write_data(db, data_html):
    # --- autocommit using context.
    with db:
        sql = 'insert into render (main_id, xml, html, html_school, t1_gecontroleerd) values (?, ?, ?, ?, ?)'
        for main_id, (xml, html, html_school, t1_gecontroleerd) in data_html.items():
            db.execute(sql, (main_id, xml, html, html_school, t1_gecontroleerd))

def init():
    config = config_get()
    # --- we use `o` to store data needed for the build.
    o = O()
    lemma_db_path = config.main['web_lemma_db_path']

    if not exists_and_readable(lemma_db_path, pref='lemma db'):
        return None

    o.db = get_db()
    # --- throws
    (o.num_lemmata,) = o.db.execute('select count(*) from main').fetchone()

    debug('lemma_db %s' % lemma_db_path)
    debug('num_lemmata %s' % o.num_lemmata)

    return o

def get_db():
    db = cache.get('db')
    if db:
        return db
    config = config_get()
    lemma_db_path = config.main['web_lemma_db_path']
    db = cache['db'] = open_db(lemma_db_path, throw=True)
    return db

def get_data_naam():
    data = cache.get('data_naam')
    if data:
        return data
    db = get_db()
    sql = 'select id, naam, homonym_nr from main order by id'
    rows = db.execute(sql).fetchall()
    data = cache['data_naam'] = {}
    for row in rows:
        main_id, naam, homonym_nr = row
        homonym_nr = homonym_nr or 1
        if homonym_nr != 1: continue
        data[naam] = main_id - 1
    return data

def get_data_homonyms():
    data = cache.get('data_homonyms')
    if data:
        return data
    db = get_db()
    sql = 'select id, naam, homonym_nr from main order by id'
    rows = db.execute(sql).fetchall()
    data = cache['data_homonyms'] = {}
    for row in rows:
        main_id, naam, homonym_nr = row
        homonym_nr = homonym_nr or 1
        if homonym_nr > 1:
            data[naam] = data.get(naam, {})
            data[naam][homonym_nr] = main_id - 1
    return data

def get_data(db, redis_client_persist=None):
    sql = 'select id, naam, homonym_nr, lemma, filename from main order by id'
    rows = db.execute(sql).fetchall()
    # --- first loop: create lookup table naam -> (0-based) main_id.
    # --- used for creating link targets, which always link to the first homonym.
    data = O()
    data.html = {}
    data.homonyms_legacy = {}
    num = 0
    num_rows = len(rows)
    t2_cache_hit_counter = CacheCounter()
    num_t1_gecontroleerd = 0
    for row in rows:
        main_id, naam, homonym_nr, lemma, filename = row
        homonym_nr = homonym_nr or 1
        # --- dying is ok on these errors because it means something is
        # really broken.
        if not lemma.strip():
            die("Lemma is empty! (id=%s)" % bright_red(main_id))
        if homonym_nr > 1:
            data.homonyms_legacy[naam] = 1
        debug('filename %s' % filename)
        if not exists_and_readable(filename):
            die()
        if file_is_empty(filename):
            die()

        # --- @throws (ok, let it throw, this is a lemma with valid status)
        err, lemma_html, lemma_html_school, _, t1_gecontroleerd = transform_lemma_to_html_from_file(
            filename,
            'normal',
            msg_filename=get_filename_stub(filename),
            redis_client_persist=redis_client_persist,
            t2_cache_hit_counter=t2_cache_hit_counter,
        )
        if err:
            # --- die, because this is a lemma with valid status.
            raise Exception("Error transforming lemma: %s" % err)

        debug('lemma_html %s' % lemma_html)
        debug('lemma_html_school %s' % lemma_html_school)
        lemma_xml = slurp(filename)
        # t1_gecontroleerd = re.search(r'''status-t1=['"]gecontroleerd''', lemma_xml) is not None
        if t1_gecontroleerd:
            num_t1_gecontroleerd += 1
        data.html[main_id] = (lemma_xml, lemma_html, lemma_html_school or None, t1_gecontroleerd)

        num += 1
        stdout.write('\r%s %d %s / %d %s (T2 cache: %d %s / %d %s) (t1 = gecontroleerd: %d)' % (
            info_pref(),
            num, yellow('processed'),
            num_rows, green('total'),
            t2_cache_hit_counter.num_hits, green('hits'),
            t2_cache_hit_counter.num_misses, bright_red('misses'),
            num_t1_gecontroleerd,
        ))
        stdout.flush()
    info('Added %s entries' % yellow(num))
    return data


'''
Possible values for `schoolreductie`: 'none', 'normal', 'dev-auto', 'dev-t1-t2', 'dev-t2'

Return value in all cases is a 2-tuple: normal html, school html.

Normal html consists of the 'standard' and 'beknopt' versions, marking punctuation using HTML markup
so that switching between them can be done in the client.

School html is made based on the `schoolreductie` parameter, and can be `None`.

School html based on mode:
 * 'none': return `None`.
 * normal: apply the 't2' transformation if the status-t1 is 'gecontroleerd', else return `None`.
   update, feedback Steve: also apply it if status-t1 is 'uitgevoerd'
 * dev-auto:
     if status-t1 is '', i.e. missing, apply t1 + t2
     if it's 'gecontroleerd' or 'uitgevoerd', apply t2
     else return `None`
 * dev-t1-t2:
     ignore status-t1, apply t1 + t2
 * dev-t2:
     ignore status-t1, apply t2 (similar to normal, except doesn't check status-t1 and therefore shouldn't
     return `None`)
 * else: error.

Note that the standard/beknopt reduction is an HTML processing step, not an XML one, and is
controlled via a kw arg to `post_process`. So when we have school XML, we turn it off.

`data_naam` and `data_homonyms` are necessary for various post-processing steps (resolving links,
etc.), but can be `None` to skip those parts, in the case of the analyse page for example.

We do the search highlighting *before* the post_process step for both normal
and school modes. This avoids the problem of searching for "met acc" after
abbreviations have been processed, i.e. in the fragment:

met <span abbr ...>acc</span>

But it's not a generic enough solution because we still don't catch

met <b>acc</b>

'''

def transform_lemma_to_html_from_string(
    xml, reductie_modus,
    xslt=None,
    msg_filename=None,
    search_terms=None,
    make_snippet=None,
    do_post_processing=True,
    redis_client_persist=None,
    t2_cache_hit_counter=None,
):
    '''
    In addition to the normal search highlighting, there is a variant
    ('entire mode') which highlights an entire field if a part of it
    matches, e.g. an entire <verw/>.

    Which fields are enabled for this mode is determined via xslt templates,
    and which input triggers it is determined by a heuristic.

    The current heuristic is, if any of the search terms contains a space,
    switch it on.
    '''

    if search_terms:
        get_do_entire = lambda term: re.search(r'\s', term) is not None
        search_terms = [(term, get_do_entire(term)) for term in search_terms]

    def post(html, do_html_reductie=True):
        html, _ = canonical(html)
        return _post_process(html, msg_filename=msg_filename, do_html_reductie=do_html_reductie)

    def lemma_to_school(xml, mode, check_status_t1=None):
        err, xml_school, t1_gecontroleerd = _lemma_to_xml_school(xml, mode,
                                                                 msg_filename=msg_filename,
                                                                 check_status_t1=check_status_t1,
                                                                 redis_client_persist=redis_client_persist,
                                                                 t2_cache_hit_counter=t2_cache_hit_counter)
        if err:
            warn("Lemma to html (school) error: %s" % err)
            return err, None, None, None
        if xml_school is None:
            return None, None, None, None
        err, html_school = lemma_to_html(xml_school, xslt, parse_xml=False)
        xml_school_xml = etree.tostring(xml_school, encoding='unicode')
        if err:
            warn("Lemma to html (school) error: %s" % err)
            return err, None, xml_school_xml, None
        if search_terms:
            err, html_school_highlight = highlight_search_terms(html_school, search_terms)
            if err:
                warn("Lemma to html: school highlight error: %s" % err)
                return err, None, None, None
            html_school = html_school_highlight
        return None, post(html_school, do_html_reductie=False), xml_school_xml, t1_gecontroleerd
    err, html_normal = lemma_to_html(xml, xslt=xslt)
    if html_normal is None or html_normal.strip() == '':
        return 'Result of lemma_to_html was empty', None, None, None, None
    if err:
        warn("Lemma to html (normal) error: %s" % err)
        return err, None, None, None, None
    if search_terms:
        err, html_normal_highlight = highlight_search_terms(html_normal, search_terms, make_snippet=make_snippet)
        if err:
            warn("Lemma to html (normal) highlighting error: %s" % err)
            return err, None, None, None, None
        html_normal = html_normal_highlight
        if html_normal is None or html_normal.strip() == '':
            return 'Result of highlight step was empty', None, None, None, None
    if do_post_processing:
        html_normal = post(html_normal)
    html_school = None
    if reductie_modus == 'none':
        return None, html_normal, None, None, None
    if reductie_modus == 'normal':
        err, html_school, xml_school, t1_gecontroleerd = lemma_to_school(xml, 't2', check_status_t1=True)
    elif reductie_modus == 'dev-auto':
        err, html_school, xml_school, t1_gecontroleerd = lemma_to_school(xml, 'auto', check_status_t1=True)
    elif reductie_modus == 'dev-t1-t2':
        err, html_school, xml_school, t1_gecontroleerd = lemma_to_school(xml, 't1-t2', check_status_t1=False)
    elif reductie_modus == 'dev-t2':
        err, html_school, xml_school, t1_gecontroleerd = lemma_to_school(xml, 't2', check_status_t1=False)
    else:
        raise APIException('Bad reductie modus: %s' % reductie_modus)
    return err, html_normal, html_school, xml_school, t1_gecontroleerd

def transform_lemma_to_html_from_file(xml_file, *args, **kw):
    return transform_lemma_to_html_from_string(read_xml_from_file(xml_file), *args, **kw)

def _lemma_to_xml_school(xml, mode,
                         msg_filename=None,
                         check_status_t1=True,
                         redis_client_persist=None,
                         t2_cache_hit_counter=None):
    config = config_get()
    status_t1_strings_checked = config.main['status_t1_strings_checked']
    status_t1_strings_run = config.main['status_t1_strings_run']
    status_t1_strings_not_run = config.main['status_t1_strings_not_run']
    err, parsed = parse_xml(xml)
    if err:
        return "Can't parse xml", None
    do_t1, do_t2 = None, None
    # if check_status_t1:
    err, status_t1 = get_xml_status_t1(parsed, default='', parse=False)
    if err:
        warn(err)
        return err, None
    t1_gecontroleerd = status_t1 in status_t1_strings_checked
    # --- 't2' means 'normal' or 'dev-t2', the difference being that the
    # latter doesn't check t1 status.
    if mode == 't2':
        if check_status_t1 and status_t1 not in status_t1_strings_run and not DEBUG_FORCE_T2:
            info('Skipping T2 (status_t1 was %s)' % status_t1)
            return None, None, t1_gecontroleerd
        else:
            do_t1, do_t2 = False, True
    elif mode == 'auto':
        if not check_status_t1:
            raise APIException('auto needs check_status_t1')
        if status_t1 in status_t1_strings_run:
            do_t1, do_t2 = False, True
        else:
            do_t1, do_t2 = True, True
    elif mode == 't1-t2':
        # @todo: when do we need check_status_t1=True and mode = 't1-t2'?
        # @todo: can we remove this if block?
        if check_status_t1 and status_t1 not in status_t1_strings_not_run:
            # @todo info
            debug('Skipping T1+T2: status_t1 was %s' % status_t1)
            return None, None, t1_gecontroleerd
        do_t1, do_t2 = True, True
    else:
        raise APIException('Invalid mode: %s' % mode)

    transformed = parsed
    if do_t1:
        try:
            transformed = xslt_transform(transformed, xslt_t1_0(), throw=True)
            transformed = xslt_transform(transformed, xslt_t1_1(), throw=True)
            transformed = xslt_transform(transformed, xslt_t1_2(), throw=True)
            transformed = xslt_transform(transformed, xslt_t1_3(), throw=True)
        except Exception as e:
            err = 'Error with T1 transform (%s): %s' % (msg_filename, e)
            return err, None, t1_gecontroleerd
        debug('after T1: xml=%s' % transformed)
    if do_t2 or DEBUG_FORCE_T2:
        if DEBUG_T2_CACHE:
            save_level = get_speak_level()
            set_speak_level('DEBUG')
        to_string = partial(etree.tostring, encoding='unicode')
        t2_input = to_string(transformed)
        t2_input_bytes = t2_input.encode()
        try:
            transform = lambda: xslt_transform_saxon_java(
                t2_input,
                config.main['schoolreductie_xslt_t2_path'],
                config.user['saxon_jar_path'],
            )
        except Exception as e:
            print_exception()
            err = "Error with T2 transform (file=%s): Couldn't run saxon java: %s, xml follows: %s" % (
                msg_filename, e, t2_input,
            )
            return err, None, t1_gecontroleerd
        hashed_input = sha256(t2_input_bytes).hexdigest()
        try:
            is_cache_hit, transformed = redis_cache(
                redis_client_persist, 't2:t2_0', transform, (),
                key_extra=hashed_input,
                serialise_result=to_string,
                deserialise_hit=etree.fromstring,
                extended_return=True,
            )
        except Exception as e:
            print_exception()
            err = "Error with T2 transform (file=%s t2-cache-key=%s): Couldn't get/set redis cache: %s, xml follows: %s" % (
                msg_filename, 't2:t2_0:' + hashed_input, e, t2_input,
            )
            return err, None, t1_gecontroleerd
        if t2_cache_hit_counter:
            if is_cache_hit:
                t2_cache_hit_counter.hit()
            else:
                t2_cache_hit_counter.miss()
        if DEBUG_T2_CACHE:
            set_speak_level(save_level)
        transformed = xslt_transform(transformed, xslt_t2_1(), throw=True)
    debug('after T2: xml=%s' % transformed)
    return None, transformed, t1_gecontroleerd

'''
@throws

We raise an exception if any step fails.

`do_html_reductie` refers to the standaard/beknopt transformation, which happens during the HTML
phase.

In the case of the school version of the XML, it's not necessary.

This function could use a lot of speed improvements -- it is called during
the build but also when highlighting a full lemma on the fly.

Make sure parse_input is True on the first step and False on subsequent ones.
'''

def _post_process(html, msg_filename=None, do_html_reductie=True):
    data_naam = get_data_naam()
    data_homonyms = get_data_homonyms()

    def throw(msg):
        raise Exception('%s (%s)' % (msg, msg_filename))

    step = html
    debug('starting post_process, before add_etym_links: %s' % step)

    '''
    Temporary.

    The goal is to eventually have no <link> elements in any <etym> elements
    in the XML, and to add them in the process_add_etym_links step.

    The XML as of 2019-08-14 doesn't have any, but newly migrated lemmata
    after this date will probably have them, because they are added by the
    migrator.

    So to allow for mixed input we remove them all first. This step can
    disappear when the input is uniform.
    '''

    err, step = process_remove_etym_links(step, parse_input=True)
    if err:
        throw("Error with process_remove_etym_links: %s" % err)

    err, step = process_add_etym_links(step)
    if err:
        throw("Error with process_add_etym_links: %s" % err)
    debug('after add_etym_links, before process_adj_verb: %s' % step)

    err, step = process_adj_verb(step)
    if err:
        throw("Error with process_adj_verb: %s" % err)
    debug('after adj_verb, before process_kruis_verw: %s' % step)

    err, step = process_kruis_verw(step)
    if err:
        throw("Error with process_kruis_verw: %s" % err)
    debug('after kruis_verw, before process_resolve_links: %s' % step)

    err, step = process_resolve_links(step, data_naam, data_homonyms)
    if err:
        throw("Error with process_resolve_links: %s" % err)
    debug('after process resolve links, before process_remove_comments: %s' % step)

    err, step = process_remove_comments(step)
    if err:
        throw("Error with process_remove_comments: %s" % err)
    debug('after process remove comments, before process_abbrev: %s' % step)

    err, step = process_abbrev(step)
    if err:
        throw("Error with process_abbrev: %s" % err)
    debug('after process abbrev, before process_cit: %s' % step)

    err, step = process_cit(step)
    if err:
        throw("Error with process_cit: %s" % err)
    debug('after process cits, before process_abbrev_cit: %s' % step)

    err, step = process_abbrev_cit(step, filename=msg_filename)
    if err:
        throw("Error with process_abbrev_cit: %s" % err)
    debug('after process_abbrev_cit, before process_beknopt: %s' % step)

    if do_html_reductie:
        err, step = process_beknopt(step, filename=msg_filename)
        if err:
            throw("Error with process_beknopt: %s" % err)

    as_string = lxml.html.tostring(step).decode()

    return '%s\n%s' % (
        # --- manually add xml decl, to match legacy script.
        '<?xml version=\"1.0\"?>',
        as_string,
    )

# --- public.
def go(speak_level=None, redis_client_persist=None):
    if speak_level: set_speak_level(speak_level)
    o = init()
    if o is None: return False

    init_render_table(o.db)

    data = get_data(o.db, redis_client_persist=redis_client_persist)
    write_data(o.db, data.html)
    o.db.close()

    return True
