# -*- coding: utf-8 -*-

"""
Note that in general, db errors, fh.write, fh.read, `cmd` (with die=True), and various
functions like `mkdir` die or throw immediately, because this is a build script and would
otherwise be too complicated.
"""

from functools import cmp_to_key
from os import path
import re

from lxml import etree
import pydash
from pydash import memoize
from pyuca import Collator

# @todo
from grne.chars import sort_plaats, cmp

from grne.chars import regex_greek, regex_dash, expand_iota_subscriptum, regex_punc
from grne.chars import to_latin, get_spiritus_info, canonical
from grne.util.util import dir_exists_and_readable, exists_and_readable, open_db, file_is_empty
from grne.util.util import get_good_xml, get_xml_from_file, comma, get_filename_stub

from acatpy.colors import bright_red, green, yellow, underline
from acatpy.io.io import cmd, die, rm
from acatpy.io.speak import info, debug, warn, say, sayn, iwarn
from acatpy.io.speak import set_level as set_speak_level
from acatpy.util import pathjoin, against_all, alias_kw

from config import get as config_get

rx_punc = regex_punc()
collator = Collator()

# set_speak_level('DEBUG')

class Homonyms(object):
    def __init__(self):
        self.data = set()

    def register(self, naam, homonym_nr):
        naam_l = naam.lower()
        if self.has(naam_l) or homonym_nr == 1:
            return
        self.data.add(naam_l)

    def has(self, naam):
        return naam.lower() in self.data

def head(xs):
    return xs[0]

def head_safe(xs):
    return xs[0] if xs else None

def safeattr(o, a):
    return getattr(o, a) if o is not None else None

@alias_kw(rmarg='rm')
def init(db_path, rmarg=True):
    exists = path.exists(db_path)
    # o = O()
    if exists:
        # --- @todo: this should be `if rmarg`
        if rm:
            info('Removing db %s' % db_path)
            rm(db_path)
            info('Creating db %s.' % green(db_path))
        else:
            info('Updating db %s' % yellow(db_path))
    else:
        info('Creating db %s.' % green(db_path))
    err, db = open_db(db_path)
    if err:
        die("Unable to open db %s: %s" % (bright_red(db_path), err))
    return db

def populate_db_lemma(db):
    info('Creating tables & indexes.')
    init_schema_lemma(db)
    lemmata, homonyms = get_data_lemma()
    info('Sorting lemmata.')
    lemmata = sort_lemmata(lemmata)
    info('Done sorting.')
    info('Inserting lemma data.')
    max_length = insert_lemmata(db, lemmata, homonyms)
    debug('max_length %s' % max_length)
    init_schema_autocomplete(db, max_length, 'forms_id')
    populate_autocomplete(db, max_length, populate_autocomplete_lemma)
    return True

def init_schema_autocomplete(db, max_length, join_table):
    for length in range(1, max_length + 1):
        for mode in 'strict', 'loose':
            table = 'autocomplete_%s_%s' % (mode, length)
            index = 'i_ac_%s_%s' % (mode, length)
            add_table(db, table, 'pref varchar(%s) not null, %s integer not null' % (length, join_table))
            add_index(db, table, index, 'pref')

def init_schema_cit(db):
    add_table(
        db,
        'aut',
        '''id integer not null primary key,
        aut text''',
    )

    add_table(
        db,
        'werk',
        '''id integer not null primary key,
        aut_id integer,
        werk text''',
    )

    add_table(
        db,
        'cit',
        '''id integer not null primary key,
        lemma_id integer,
        aut_id integer,
        werk_id integer,
        plaats text,
        naam text,
        homonym_nr smallint''',
    )

    # --- names of indices are arbitrary.
    add_index(db, 'aut', 'i_aut', 'aut')
    add_index(db, 'werk', 'i_werk', 'aut_id, werk')
    add_index(db, 'cit', 'i_cit', 'aut_id, werk_id')

def populate_db_pars(db):
    config = config_get()
    lemmatiser_path = config.local['lemmatiser_path']
    debug('lemmatiser_path %s' % lemmatiser_path)
    info('Creating tables & indexes.')
    init_schema_pars(db)
    pars_forms = get_data_pars_forms(lemmatiser_path)
    info('Sorting pars forms.')
    pars_forms = sort_pars_forms(pars_forms)
    info('Done sorting.')
    info('Inserting pars data.')
    max_length = insert_pars_forms(db, pars_forms)
    debug('max_length %s' % max_length)
    init_schema_autocomplete(db, max_length, 'main_id')
    populate_autocomplete(db, max_length, populate_autocomplete_pars)
    return True

def populate_db_cit(db):
    config = config_get()
    db_lemma_path = config.main['web_lemma_db_path']
    err, db_lemma = open_db(db_lemma_path, read_only=True)
    if err:
        die("Can't open lemma db %s: %s" % (db_lemma_path, err))
    init_schema_cit(db)
    data = get_data_cit(db_lemma)
    debug('data %s' % data)
    info('Inserting cit data')
    insert_citaten(db, data)
    return True


"""
  The nth strict table is a mapping from the exact n-length prefix to the
  lexid.

  The nth loose table is a mapping from L to the main id. L is made by taking
  the value from the token_stripped column, in other words, without accents,
  and sending it through the to_latin function. This converts α to a, β
  to b, etc., and both 'σ' and 'ς' to 's'. The sigma ambiguity will be
  resolved by logic in the autocompleter CGI module.

  In the lemma db / lemmaveld, searches go first to the ac table (strict or
  loose), which is mapped to the forms_strict or forms_loose table, which is
  mapped to the main table. Hence we throw away $main_id below and store
  $id.

  In the pars db / parsveld, searches go first to the ac table (strict or
  loose), and then directly to the main table, without hopping first through
  forms_loose. The reason for the lack of parallelism here with lemmaveld is
  that the ac dropdown in lemmaveld should show *forms*, i.e., indexI forms,
  while the ac dropdown of the parsveld shows *lemmas*, i.e.. the actual
  word. The forms_loose table is important for if there are wildcard
  searches.

  Update: we *do* have both a forms_loose table and forms_strict, and these
  are looped through to make autocomplete, not the main table. Autocomplete
  queries don't need the forms_xxx tables, except in the case of wildcards.
"""

def populate_autocomplete(db, max_length, populate):
    info("Inserting autocomplete data.")
    with db:
        for pref_length in range(1, max_length + 1):
            for mode in 'strict', 'loose':
                say("  → doing prefix of length %s, mode %s." % (yellow(pref_length), yellow(mode)))
                ac_table = 'autocomplete_%s_%s' % (mode, pref_length)
                db.execute('delete from %s' % ac_table)
                populate(db, ac_table, pref_length, mode)

def populate_autocomplete_lemma(db, ac_table, pref_length, mode):
    table = 'forms_%s' % mode
    # --- don't need main_id: see above.
    sql = 'select id, token from %s where length(token) >= ?' % table
    res = db.execute(sql, (pref_length,)).fetchall()
    if not res:
        debug('%s: nothing in col %s which is longer than %s' % (mode, yellow('token'), yellow(pref_length)))
        return
    for row in res:
        the_id, token = row
        pref = token[0:pref_length]
        # debug('pref %s' % pref)
        sql = 'insert into %s (pref, forms_id) values (?, ?)' % ac_table
        # debug('sql %s' % sql)
        # debug('pref, the_id %s %s' % (pref, the_id))
        db.execute(sql, (pref, the_id))

def populate_autocomplete_pars(db, ac_table, pref_length, mode):
    table = 'forms_%s' % mode
    # --- no id, just main_id: a bit different from the lemma loop.
    sql = 'select main_id, token from %s where length(token) >= ?' % table
    res = db.execute(sql, (pref_length,)).fetchall()
    if not res:
        debug('%s: nothing in col %s which is longer than %s' % (mode, yellow('token'), yellow(pref_length)))
        return
    cnt = -1
    num_rows = len(res)
    # debug('num_rows %s' % num_rows)
    for row in res:
        main_id, token = row
        pref = token[0:pref_length]
        # --- the pars table contains proper names (eigennamen), which start
        # with a capital letter.
        pref = pref.lower()
        sql = 'insert into %s (pref, main_id) values(?, ?)' % ac_table
        db.execute(sql, (pref, main_id))
        cnt += 1
        prog = None
        if cnt == num_rows - 1:
            sayn('\r%s%s' % (green('✔'), ' ' * 20))
        elif cnt and not cnt % 1e4:
            prog = '%d' % (cnt / num_rows * 100)
        if prog:
            sayn('\r%s: %s %%' % (underline('progress'), bright_red(prog)))
    say('')

def init_schema_lemma(db):
    add_table(
        db,
        'main',
        '''id integer not null primary key,
        naam text,
        homonym_nr smallint,
        lemma text,
        filename text''',
    )

    # --- @todo this should move to make_site; as it is now we have to
    # rerun make_index with do_lemma if we want to change the schema of the
    # render table.
    add_table(
        db,
        'render',
        '''id integer not null primary key,
        main_id integer,
        xml text,
        html text,
        html_school text,
        t1_gecontroleerd smallint''',
    )

    """
    A lemma can have several forms due to:
    -parentheses expansion
    -<hoofdW>x en y</hoofdW> (not doing) XX
    -<hoofdW>x -yy -zz</hoofdW> (not doing) XX

    The 'main' table is what determines what the entry for the dictionary
    view will be. So the entry will be for example (ἐ)πέλασ(σ)α

    The 'forms_strict' and 'forms_loose' tables will contain in that case
      ἐπέλασσα
      πέλασσα
      ἐπέλασα
      πέλασα
    and
      epelassa
      pelassa
      epelasa
      pelasa

    respectively.

    These tables are used to generate autocomplete possiblities, and they
    are also used by the autocompleter cgi when wildcards are used.

    Spiritus works by creating a vector of bits which represent the
    locations of the two kinds of spiritus. For example, εἶἑν has
    spiritus_left 100 and spiritus_right 10. Note that they are reversed.
    Then in the autocompleter they are bitwise and-ed with the pattern of
    spirituses that the user enters.

    Sqlite can handle up to 8 bytes in integer (though after 63 bits it
    looks like they start rounded.) Anyway our longest word (so far) is
    about 33 long. So integer is fine for the spiritus columns.
    """

    add_table(
        db,
        'forms_strict',
        '''id integer not null primary key,
        main_id integer,
        token text,
        spiritus_left integer,
        spiritus_right integer''',
    )

    # --- the spiritus columns in forms_loose are not used, but they are
    # there to keep the schema a bit easier to understand.
    add_table(
        db,
        'forms_loose',
        '''id integer not null primary key,
        main_id integer,
        token text,
        spiritus_left integer,
        spiritus_right integer''',
    )

    # --- add indices -- names are just a formality.
    add_index(db, 'main', 'i_main', 'lemma')

    # --- for fullnaam/id lookup after advanced search.
    add_index(db, 'main', 'i_main2', 'naam,homonym_nr')

    add_index(db, 'render', 'i_render', 'id,main_id')

    add_index(db, 'forms_strict', 'i_fs', 'main_id')
    add_index(db, 'forms_loose', 'i_fl', 'main_id')

def init_schema_pars(db):
    add_table(
        db,
        'main',
        '''id integer not null primary key,
        token text,
        lemma text,
        code text''',
    )

    add_table(
        db,
        'forms_loose',
        '''main_id integer,
        token text,
        spiritus_left integer,
        spiritus_right integer''',
    )

    add_table(
        db,
        'forms_strict',
        '''main_id integer,
        token text,
        spiritus_left integer,
        spiritus_right integer''',
    )

    # --- index names are arbitrary.
    add_index(db, 'forms_loose', 'i_fl', 'main_id')
    add_index(db, 'forms_strict', 'i_fs', 'main_id')
    add_index(db, 'main', 'i_main', 'token')

def sort_key_wrapper(sort, progress_tick=1e4):
    ops = 0

    def sort_key(item):
        nonlocal ops
        ops += 1
        if not ops % progress_tick:
            info('%s sort operations processed' % bright_red(comma(ops)))
        return sort(item)
    return sort_key

def sort_lemmata(lemmata):
    def sort(lemma):
        _, naam, homonym_nr, filename, _ = lemma
        naam = naam.lower()
        naam_i = expand_iota_subscriptum(naam)

        # @migrate: remove space at beginning
        naam_i = re.sub(r'^\s+', '', naam_i)
        return collator.sort_key(naam_i), homonym_nr

    return sorted(lemmata, key=sort_key_wrapper(sort))

def sort_pars_forms(forms):
    def sort(form):
        token, *_ = form
        token_i = expand_iota_subscriptum(token)
        # --- note that case information is still used here, as opposed to above, where we ignore
        # case and use homonym number as a second key.
        return collator.sort_key(token_i)
    return sorted(forms, key=sort_key_wrapper(sort, progress_tick=5e4))

def sort_forms_entries(entries):
    def sort(entry):
        strict = entry[1]
        return collator.sort_key(strict)

    return sorted(entries, key=sort_key_wrapper(sort))

def insert_lemmata(db, lemmata, homonyms):
    max_length = 0
    forms_entries = []
    # --- auto commit using context
    with db:
        for row in lemmata:
            lemma, naam, homonym_nr, filename, forms = row
            sql = 'insert into main (naam, homonym_nr, lemma, filename) values(?, ?, ?, ?)'
            homonym_nr_to_insert = homonym_nr if homonyms.has(naam) else None
            cursor = db.execute(sql, (naam, homonym_nr_to_insert, lemma, filename))
            main_id = cursor.lastrowid
            if main_id is None:
                raise Exception("Can't get last row id")
            for form in forms:
                loose, _, errors = to_latin(form)
                if errors:
                    warn("Error linking form")
                    warn(errors)
                    continue
                loose = loose.lower()
                strict = form
                forms_entries.append([main_id, strict, loose])
                max_length = max(len(loose), len(strict), max_length)
                # --- ordinary to_latin makes προθυμίῃσι -> proqumihsi
                # this version makes -> proqumihisi; add that as well to
                # forms_loose.
                iota_expand, _, errors = to_latin(strict, iota_intact=True)
                if iota_expand != loose:
                    forms_entries.append([main_id, strict, iota_expand])
        sql_s = 'insert into forms_strict (main_id, token, spiritus_left, spiritus_right) values (?, ?, ?, ?)'
        sql_l = 'insert into forms_loose (main_id, token, spiritus_left, spiritus_right) values (?, ?, ?, ?)'

        say('')
        info('Sorting forms entries.')
        forms_entries = sort_forms_entries(forms_entries)
        for form in forms_entries:
            main_id, strict, loose = form
            spiritus_left, spiritus_right = get_spiritus(strict)
            db.execute(sql_s, (main_id, strict, spiritus_left, spiritus_right))
            db.execute(sql_l, (main_id, loose, spiritus_left, spiritus_right))
    return max_length

def insert_pars_forms(db, forms):
    max_length = 0
    errors = []
    for form in forms:
        strict, lemma, code = form
        if not lemma:
            errors.append('Lemma is empty for token=%s code=%s' % (strict, code))
            continue
        forms_loose = []
        strict_c, _ = canonical(strict)
        lemma_c, _ = canonical(lemma)
        loose, _, err = to_latin(strict_c)
        if err:
            errors.append('Error with to_latin for token=%s: %s' % (err, strict_c))
            continue
        loose_i, _, err = to_latin(strict_c, iota_intact=True)
        if err:
            errors.append('Error with to_latin + iota_expand for token=%s: %s' % (err, strict_c))
            continue
        forms_loose.append(loose)
        if loose != loose_i:
            forms_loose.append(loose_i)
        max_length = max(len(loose), len(strict_c), max_length)
        sql = 'insert into main (token, lemma, code) values (?, ?, ?)'
        # --- ordinary to_latin makes προθυμίῃσι -> proqumihsi
        # this version makes -> proqumihisi; add that as well to
        # forms_loose.
        # --- this loop is a bit different than the lemma case (no
        # forms_strict to worry about)
        cursor = db.execute(sql, (strict_c, lemma_c, code))
        main_id = cursor.lastrowid
        if main_id is None:
            raise Exception("Can't get last row id")
        spiritus_left, spiritus_right = get_spiritus(strict_c)
        sql_l = 'insert into forms_loose (main_id, token, spiritus_left, spiritus_right) values (?, ?, ?, ?)'
        sql_s = 'insert into forms_strict (main_id, token, spiritus_left, spiritus_right) values (?, ?, ?, ?)'
        for loose in forms_loose:
            db.execute(sql_l, (main_id, loose, spiritus_left, spiritus_right))
        db.execute(sql_s, (main_id, strict_c, spiritus_left, spiritus_right))
    return max_length

def get_spiritus(word):
    spiritus_info = get_spiritus_info(word)
    spiritus_left = int(spiritus_info.left, 2)
    spiritus_right = int(spiritus_info.right, 2)
    return spiritus_left, spiritus_right

def add_index(db, table, index, spec, unique=False):
    i = 'unique index' if unique else 'index'
    sql = 'create %s %s on %s(%s)' % (i, index, table, spec)
    db.execute(sql)

def add_table(db, name, spec):
    sql = 'create table %s (%s)' % (name, spec)
    db.execute(sql)

def _get_words_filenames(letter=None, verbose_cmds=True):
    config = config_get()
    lemma_input_path = config.local['lemma_input_path']
    debug('lemma_input_path %s' % lemma_input_path)
    if not dir_exists_and_readable(lemma_input_path):
        die("Invalid lemma input path: %s" % bright_red(lemma_input_path))
    lemma_filename_filters = config.main['lemma_filename_filters']
    debug('lemma_filename_filters %s' % lemma_filename_filters)
    letter_filter = []
    if letter is not None:
        letter_filter = ['-wholename', '*/%s/*' % letter]
    test_filter = ['!', '-wholename', '*/test/*']
    filters = [*letter_filter, *test_filter]
    ret = cmd(['find', '-L', pathjoin(lemma_input_path, ''), *filters, '-name', '*.xml', '-print0'], capture_stdout=True, die=True, verbose=verbose_cmds)
    all_files = ret.out.split('\0')[0:-1]
    filter_file = lambda filename: against_all(lemma_filename_filters, filename)
    files, errors, empty = [], [], []
    num_ignored = 0
    for filename in all_files:
        fn_stub = get_filename_stub(filename)
        shld_reject = not filter_file(filename)
        if shld_reject:
            num_ignored += 1
            warn('File %s rejected based on filename pattern' % filename)
            continue
        is_empty = file_is_empty(filename)
        if is_empty:
            num_ignored += 1
            warning = 'File was empty: %s' % fn_stub
            warn(warning)
            empty.append(warning)
            continue
        if not exists_and_readable(filename):
            die("File not readable: %s" % bright_red(filename))
        files.append(filename)
    return files, errors, empty, num_ignored, len(all_files) - len(files)

get_words_filenames = memoize(_get_words_filenames)

# --- @todo move this to a more generic module (it's also used by scripts
# for example).
def get_words_files(warn_on_bad_status=True, letter=None, verbose_cmds=True, also_status_ok=None):
    also_status_ok = also_status_ok or []
    config = config_get()
    status_strings_good = config.main['status_strings_good']
    # --- num_unreadable includes num_ignored, not the best name.
    files, errors, empty, num_ignored, num_unreadable = get_words_filenames(letter=letter, verbose_cmds=verbose_cmds)
    errors.extend(empty)

    # @todo return parsed, avoid double parse
    fns_bad_parse, fns_bad_status, _, fns_status_ok = get_good_xml([*status_strings_good, *also_status_ok], files)
    if fns_bad_parse: warn(
        'Ignoring %s unreadable files or files with invalid xml: %s' %
        (bright_red(len(fns_bad_parse)), ', '.join(fns_bad_parse))
    )
    if warn_on_bad_status and fns_bad_status:
        fns = ('    %s' % x for x in fns_bad_status)
        warn(
            'Ignoring %s files with unfinalised xml:\n%s' %
            (yellow(len(fns_bad_status)), '\n'.join(fns))
        )
    errors.extend(['Unreadable or invalid: %s' % x for x in fns_bad_parse])
    return (fns_bad_parse, fns_bad_status, fns_status_ok), errors, (num_ignored, len(files), num_unreadable)

def get_data_lemma(warn_on_bad_status=True, letter=None, verbose_cmds=True,
                   output_report=True):
    (
        (fns_bad_parse, fns_bad_status, fns_status_ok),
        errors,
        (num_ignored, num_files, num_unreadable)
    ) = get_words_files(warn_on_bad_status=warn_on_bad_status,
                        letter=letter,
                        verbose_cmds=verbose_cmds)
    lemmata = []
    homonyms = Homonyms()
    for filename in fns_status_ok:
        fn_stub = get_filename_stub(filename)
        xml = get_xml_from_file(filename)
        if not xml:
            errors.append('File already checked but fails: %s' % filename)
            continue
        hw_e = pydash.get(xml.findall('//hoofdW'), 0)
        if hw_e is None:
            errors.append('No hoofdW (%s)' % filename)
            continue
        hw = ''.join((hw_e.text, *(_.tail for _ in hw_e)))
        naam_full_e = pydash.get(xml.findall('//metadata/naam'), 0)
        if naam_full_e is None:
            errors.append('No naam (%s)' % filename)
            continue
        naam_full = naam_full_e.text
        err, (naam, homonym_nr) = break_apart_naam(naam_full)
        if err:
            errors.append(err)
            continue
        indexI_e = xml.findall('//metadata/indexI/item')
        indexI = []
        for ii in indexI_e:
            txt = (ii.text or '').strip()
            if not txt:
                errors.append('Saw empty indexI item in %s' % fn_stub)
                continue
            indexI.append(txt)
        lemma = extract_lemma_from_hoofdW(hw)
        if not lemma:
            x = "Couldn't extract lemma from hoofdW: %s (%s)" % (hw, fn_stub)
            warn(x)
            errors.append(x)
            continue
        lemma, _ = canonical(lemma)
        naam, _ = canonical(naam)
        indexI = [canonical(_)[0] for _ in indexI]

        debug('naam:%s homonym_nr:%s hw:%s indexI:%s lemma:%s' % (naam, homonym_nr, hw, indexI, lemma))
        homonyms.register(naam, homonym_nr)
        lemmata.append([lemma, naam, homonym_nr, filename, indexI])

    if output_report:
        report(errors, lemmata, 'lemmata', (fns_bad_parse, fns_bad_status), (num_files, num_ignored, num_unreadable))

    return lemmata, homonyms

def get_data_pars_forms(lemmatiser_path):
    if not exists_and_readable(lemmatiser_path):
        die('Bad lemmatiser db %s' % lemmatiser_path)
    err, db = open_db(lemmatiser_path, read_only=True)
    if err:
        die("Can't open lemmatiser db %s: %s" % (lemmatiser_path, err))
    cur = db.execute('select token, lemma, code from Lexicon')
    return cur.fetchall()

# --- @todo named tuple would be nicer.
def get_data_cit(db_lemma):
    (fns_bad_parse, fns_bad_status, fns_status_ok), errors, (num_ignored, num_files, num_unreadable) = get_words_files()
    debug('fns_status_ok[0:20] %s' % fns_status_ok[0:20])
    debug('ignored %s' % num_ignored)
    errors = []
    data = {
        'aut': set(),
        'werk': {},
        'cit': [],
    }
    for filename in fns_status_ok:
        xml = get_xml_from_file(filename)
        if not xml:
            raise Exception('File already checked but fails: %s' % filename)
        naam_full_e = xml.find('//metadata/naam')
        if naam_full_e is None: die('No naam (%s)' % filename)
        naam_full = naam_full_e.text
        naam_full, _ = canonical(naam_full)
        # --- not looking at volgN any more, unlike legacy script.
        err, lemma_id = id4naam(db_lemma, naam_full)
        if err:
            errors.append(err)
            continue
        err, (naam, homonym_nr) = break_apart_naam(naam_full)
        if err:
            errors.append(err)
            continue
        verws = xml.findall('//verw')
        if verws:
            err, data = get_data_cit_inner(data, lemma_id, naam, homonym_nr, verws)
            if err:
                errors.extend(err)
    debug('data %s' % data)
    report(errors, data['cit'], 'citaten', (fns_bad_parse, fns_bad_status), (num_files, num_ignored, num_unreadable))
    return data

def insert_citaten(db, data):
    with db:
        return insert_citaten_inner(db, data)

def insert_citaten_inner(db, data):
    data_aut = data['aut']
    data_werk = data['werk']
    data_cit = data['cit']
    werk_ids = {}
    aut = {}
    sql = 'insert into aut(aut) values(?)'
    for auteur in sorted(data_aut):
        cursor = db.execute(sql, (auteur,))
        aut_id = cursor.lastrowid
        if aut_id is None:
            raise Exception("Can't get last row id")
        aut[auteur] = aut_id
    debug('data_werk.keys() %s' % data_werk.keys())
    werks = sorted(data_werk.keys())
    sql = 'insert into werk(aut_id, werk) values(?, ?)'
    for werk in werks:
        if werk not in data_werk:
            # @todo ierror
            iwarn("Missing werk")
            die()
        for werk_aut in data_werk[werk]:
            # --- use werk=-1 to mean that this author has some citaats
            # which consist only of aut and plaats.
            if werk_aut not in aut:
                # @todo ierror
                iwarn("Missing werk_aut")
                die()
            aut_id = aut[werk_aut]
            cursor = db.execute(sql, (aut_id, werk))
            werk_id = cursor.lastrowid
            if werk_id is None:
                raise Exception("Can't get last row id")
            # if werk is None: werk = -1
            # --- like werk but with actual ids instead of text.
            if not werk_ids.get(aut_id):
                werk_ids[aut_id] = {}
            werk_ids[aut_id][werk] = werk_id
    cit_ids = []
    for cit in data_cit:
        lemma_id, auteur, werk, *rest = cit
        if not aut[auteur]:
            # @todo ierror
            iwarn("Missing aut_id")
            die()
        aut_id = aut[auteur]
        werk_id = pydash.get(werk_ids, [aut_id, werk])
        if not werk_id:
            # @todo ierror
            iwarn("Missing werk_id for werk %s" % werk)
            die()
        cit_ids.append([lemma_id, aut_id, werk_id, *rest])

    def sort_cit(a, b):
        _, auteur_a, werk_a, plaats_a, *_ = a
        _, auteur_b, werk_b, plaats_b, *_ = b
        return cmp(auteur_a, auteur_b) or cmp(werk_a, werk_b) or sort_plaats(plaats_a, plaats_b)
    debug('before: cit_ids %s' % cit_ids)
    cit_ids = sorted(cit_ids, key=cmp_to_key(sort_cit))
    debug('after: cit_ids %s' % cit_ids)
    # @todo only insert homonym_nr if it's part of a group; see lemma case.
    sql = 'insert into cit(lemma_id, aut_id, werk_id, plaats, naam, homonym_nr) values(?, ?, ?, ?, ?, ?)'
    for cit_id in cit_ids:
        # --- werk or plaats (but not both) can be -1, meaning it's missing
        # in this citaat, ok.
        lemma_id, aut_id, werk_id, plaats, naam, homonym_nr = cit_id
        db.execute(sql, (lemma_id, aut_id, werk_id, plaats, naam, homonym_nr))

def report(errors, data, desc, fns, nums):
    (fns_bad_parse, fns_bad_status) = fns
    (num_files, num_ignored, num_unreadable) = nums
    say('')
    not_ok_s = lambda num=None: '' if num == 0 else bright_red('✘')
    ok_s = lambda num=None: '' if num == 0 else green('✔')
    say('---')
    info('%s: %s' % (underline('Total files'), num_files))
    info('  → ignored: %s' % num_ignored)
    info('  → unreadable/empty: %s %s' % (num_unreadable, not_ok_s(num_unreadable)))
    num = len(fns_bad_parse)
    info('  → total unparseable: %s %s' % (num, not_ok_s(num)))
    num = len(fns_bad_status)
    info('  → total files with status not ok: %s %s' % (num, not_ok_s(num)))
    num = len(data)
    info('%s: %s %s' % (underline('Total %s to process' % desc), num, ok_s(num)))
    if errors:
        err_c = ('  %s' % x for x in errors)
        info('%s:\n%s' % (underline('Errors'), '\n'.join(err_c)))
    else:
        info('%s. %s' % (underline('No errors'), ok_s()))
    say('')


"""
  Alters `data`.

  Note that nothing we do in this script affects the citG advanced search.
  Skipping an entry in this function or returning None will only make an
  (unusable) citaat unavailable from the drop-downs.

  If either plaats or werk is missing, (but not both), store a -1.

  Each entry is a <verw> node. The point is to fill data->cit with full
  citaat representation: aut + (werk and/or plaats).
"""

def get_data_cit_inner(data, lemma_id, naam, homonym_nr, verws):
    def getx(pred, getter, desc, verw, tag):
        nodes = verw.findall('%s' % tag)
        l = len(nodes)
        if not pred(l):
            err = 'Expected %s %s node(s) in %s, found %s' % (desc, tag, tostring(verw), l)
            return err, None
        node = getter(nodes)
        if node is None:
            return None, None
        text = (safeattr(node, 'text') or '').strip()
        if not text:
            err = 'No text in child <%s> of node %s' % (tag, tostring(verw))
            return err, None
        return None, text

    def get1(*args):
        return getx(lambda x: x == 1, head, 'exactly one', *args)

    def get0or1(*args):
        return getx(lambda x: x < 2, head_safe, 'zero or one', *args)

    errors = []
    for verw in verws:
        err, aut = get1(verw, 'aut')
        if err: errors.append(err)
        err, werk = get0or1(verw, 'werk')
        if err: errors.append(err)
        err, plaats = get0or1(verw, 'plaats')
        if err: errors.append(err)
        if errors:
            return errors, data
        if not (werk or plaats):
            continue
        aut = clean_aut(aut)
        werk = clean_werk(werk) if werk else '-1'
        plaats = clean_plaats(plaats) if plaats else '-1'
        data['aut'].add(aut)
        # if werk != -1:
        if not data['werk'].get(werk):
            data['werk'][werk] = set()
        data['werk'][werk].add(aut)
        data['cit'].append([lemma_id, aut, werk, plaats, naam, homonym_nr])

    return None, data

# @todo move (server needs it too)
def id4naam(db_lemma, naam_full):
    err, (naam, homonym_nr) = break_apart_naam(naam_full)
    if err:
        return err, None
    if not naam:
        return "Couldn't get naam for %s" % naam_full, None
    if not homonym_nr:
        return "Couldn't get homonym_nr for %s" % naam_full, None

    sql = 'select id from main where naam = ? and (homonym_nr is null or homonym_nr = ?)'
    res = db_lemma.execute(sql, (naam, homonym_nr)).fetchall()
    if not res:
        return "Couldn't get id for %s" % naam_full, None
    first = res[0]
    if not first:
        return "Couldn't get id for %s" % naam_full, None
    return None, first[0]

def strip_tags(x):
    rx = r'< (\w+) \W .*? </\1>'
    return re.sub(rx, '', x, flags=re.X)

def clean_aut(aut):
    ret = strip_tags(aut).strip()
    return re.sub(r'[\.;]+$', '', ret)

def clean_werk(werk):
    ret = strip_tags(werk).strip()
    ret = re.sub(r'[\[\]]', '', ret)
    return re.sub(r'[\.;]+$', '', ret)

def clean_plaats(plaats):
    ret = strip_tags(plaats).strip()
    rx = r'(\s | %s)* $' % rx_punc
    ret = re.sub(rx, '', ret, flags=re.X)
    return ret


"""
In the case of (w)xxx(y)zzz we use wxxxyzzz as the lemma.
Note that parens can also be at the end.
"""

def extract_lemma_from_hoofdW(hoofdW_arg, strip_parens=True, strip_star=True):
    # --- in case of e.g. '2. xxx yyy zzz'
    hoofdW, _ = get_homonym_info(hoofdW_arg)
    rx_inner = r'(?: %s | \( %s \) | %s)' % (regex_dash(), regex_greek(), regex_greek())
    rx = r'^ \s* \*? (%s+)' % rx_inner
    m = re.search(rx, hoofdW, flags=re.X)
    if not m:
        return None
    lemma = m.groups()[0]
    strip = []
    if strip_parens: strip.append(r'[()]')
    if strip_star: strip.append(r'^ \s* \*')
    for rx_strip in strip:
        lemma = re.sub(rx_strip, '', lemma, flags=re.X)
    return lemma


"""
Returns hoofdW (minus possible digits at the front), and homonym nr if applicable.

Given: 2. xxx yyy zzz, return ('xxx yyy zzz', 2)
Given:    xxx yyy zzz, return ('xxx yyy zzz', None)
"""

def get_homonym_info(hw):
    m = re.search(r'^ \s* (\d+) \. \s* (.+)', hw, flags=re.X)
    if m:
        l, r = m.groups()
        return r, int(l)
    return hw, None

def break_apart_naam(naam_full):
    rx = r'^ (.+) - (\d+) $'
    m = re.search(rx, naam_full, flags=re.X)
    if not m:
        x = 'Unable to break apart naam: %s' % naam_full
        warn(x)
        return x, (None, None)
    naam, homonym_nr = m.groups()
    return None, (naam, int(homonym_nr))

def go(db_path, populate_db, speak_level=None):
    if speak_level: set_speak_level(speak_level)
    db = init(db_path)
    ok = populate_db(db)
    db.close()
    return ok

def tostring(node):
    return etree.tostring(node)

# ------ public.
def go_lemma(**kw):
    config = config_get()
    db_path = config.main['web_lemma_db_path']
    return go(db_path, populate_db_lemma, **kw)

def go_pars(**kw):
    config = config_get()
    db_path = config.main['web_pars_db_path']
    return go(db_path, populate_db_pars, **kw)

def go_cit(**kw):
    config = config_get()
    db_path = config.main['web_cit_db_path']
    return go(db_path, populate_db_cit, **kw)
