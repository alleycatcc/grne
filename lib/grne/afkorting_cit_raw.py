# -*- coding: utf-8 -*-

import re

'''
From Resolutie afkortingen v1.2.xlsx

Export as .csv using | as the delimiter, quote every field with double
quotes, and use vim etc. to make the tuple below.

Be sure to update the quirks (below) as wel.
'''

def _expansions_aut_werk_raw():
    return (
        '''"Aeschylus"|"Aeschl."|"Agamemnon"|"Ag."''',
        '''"Aeschylus"|"Aeschl."|"Choephoroe"|"Ch."''',
        '''"Aeschylus"|"Aeschl."|"Eumenides"|"Eum."''',
        '''"Aeschylus"|"Aeschl."|"Persae"|"Pers."''',
        '''"Aeschylus"|"Aeschl."|"Prometheus Vinctus"|"PV"''',
        '''"Aeschylus"|"Aeschl."|"Septem contra Thebas"|"Sept."''',
        '''"Aeschylus"|"Aeschl."|"Supplices"|"Suppl."''',
        '''"Alcaeus"|"Alc."||''',
        '''"Alcmaeon"|"Alcmae."||''',
        '''"Anaxagoras"|"Anaxag."||''',
        '''"Anaxarchus"|"Anaxarch."||''',
        '''"Anaximander"|"Anaximand."||''',
        '''"Anaximenes"|"Anaximen."||''',
        '''"Andocides"|"And."|"De mysteriis"|1''',
        '''"Andocides"|"And."|"De reditu suo"|2''',
        '''"Andocides"|"And."|"De pace"|3''',
        '''"Andocides?"|"[And.]"|"In Alcibiadem "|4''',
        '''"Anthologia Palatina"|"AP"||''',
        '''"Antiphon"|"Antiphon"||''',
        '''"Apollodorus, toegeschreven aan Demosthenes"|"Apollod. [Dem.]"|"In Neaeram"|59''',
        '''"Archelaus"|"Archel."||''',
        '''"Archilochus"|"Archil."||''',
        '''"Archytas"|"Archyt."||''',
        '''"Aristophanes"|"Aristoph."|"Acharnenses (Acharniërs)"|"Ach."''',
        '''"Aristophanes"|"Aristoph."|"Aves (Vogels)"|"Av."''',
        '''"Aristophanes"|"Aristoph."|"Ecclesiazusae"|"Eccl."''',
        '''"Aristophanes"|"Aristoph."|"Equites (Ridders)"|"Eq."''',
        '''"Aristophanes"|"Aristoph."|"Lysistrata"|"Lys."''',
        '''"Aristophanes"|"Aristoph."|"Nubes (Wolken)"|"Nub."''',
        '''"Aristophanes"|"Aristoph."|"Pax (Vrede)"|"Pax"''',
        '''"Aristophanes"|"Aristoph."|"Plutus"|"Pl."''',
        '''"Aristophanes"|"Aristoph."|"Ranae (Kikkers)"|"Ran."''',
        '''"Aristophanes"|"Aristoph."|"Thesmophoriazusae"|"Th."''',
        '''"Aristophanes"|"Aristoph."|"Vespae (Wespen)"|"Ve."''',
        '''"Aristoteles"|"Aristot."|"Ethica Nicomachea"|"EN"''',
        '''"Aristoteles"|"Aristot."|"Poetica"|"Poët."''',
        '''"Aristoteles"|"Aristot."|"Politica"|"Pol."''',
        '''"Aristoteles"|"Aristot."|"Rhetorica"|"Rh."''',
        '''"Bacchylides"|"Bacchyl."||''',
        '''"Critias"|"Critias"||''',
        '''"Damon"|"Damon"||''',
        '''"Democritus"|"Democr."||''',
        '''"Demosthenes"|"Dem."|"Olynthiaca 1"|1''',
        '''"Demosthenes"|"Dem."|"Olynthiaca 2"|2''',
        '''"Demosthenes"|"Dem."|"Olynthiaca 3"|3''',
        '''"Demosthenes"|"Dem."|"Philippica 1"|4''',
        '''"Demosthenes"|"Dem."|"Philippica 2"|6''',
        '''"Demosthenes"|"Dem."|"Philippica 3"|9''',
        '''"Demosthenes"|"Dem."|"Philippica 4"|10''',
        '''"Demosthenes"|"Dem."|"De corona"|18''',
        '''"Demosthenes"|"Dem."|"De falsa legatione"|19''',
        '''"Demosthenes"|"Dem."|"In Midiam"|21''',
        '''"Diogenes van Apollonia"|"Diog. Apoll."||''',
        '''"Empedocles"|"Emp."||''',
        '''"Epicharmus"|"Epich."||''',
        '''"Euripides"|"Eur."|"Alcestis"|"Alc."''',
        '''"Euripides"|"Eur."|"Andromache"|"Andr."''',
        '''"Euripides"|"Eur."|"Bacchae"|"Ba."''',
        '''"Euripides"|"Eur."|"Cyclops"|"Cycl."''',
        '''"Euripides"|"Eur."|"Electra"|"El."''',
        '''"Euripides"|"Eur."|"Hecuba"|"Hec."''',
        '''"Euripides"|"Eur."|"Helena"|"Hel."''',
        '''"Euripides"|"Eur."|"Heraclidae"|"Hcld."''',
        '''"Euripides"|"Eur."|"Hercules (Furens)"|"HF"''',
        '''"Euripides"|"Eur."|"Hippolytus"|"Hipp."''',
        '''"Euripides"|"Eur."|"Ion"|"Ion"''',
        '''"Euripides"|"Eur."|"Iphigenia Aulidensis"|"IA"''',
        '''"Euripides"|"Eur."|"Iphigenia Taurica"|"IT"''',
        '''"Euripides"|"Eur."|"Medea"|"Med."''',
        '''"Euripides"|"Eur."|"Orestes"|"Or."''',
        '''"Euripides"|"Eur."|"Phoenissae"|"Phoen."''',
        '''"Euripides"|"Eur."|"Rhesus"|"Rh."''',
        '''"Euripides"|"Eur."|"Supplices"|"Suppl."''',
        '''"Euripides"|"Eur."|"Troades"|"Tr."''',
        '''"Gorgias"|"Gorg."||''',
        '''"Hecataeus"|"Hecat."||''',
        '''"Heraclitus"|"Heraclit."||''',
        '''"Herodotus"|"Hdt."|"Historiën"|''',
        '''"Hesiodus"|"Hes."|"Opera et Dies (Werken en Dagen)"|"Op."''',
        '''"Hesiodus"|"Hes."|"Theogonie"|"Th."''',
        '''"Hippias"|"Hippias"||''',
        '''"Hippocrates"|"Hp."|"Aphorismi (Aphorisms)"|"Aph."''',
        '''"Hippocrates"|"Hp."|"De aëre aquis locis (Airs, Waters< Places)"|"Aër."''',
        '''"Hippocrates"|"Hp."|"De alimento (Nutriment)"|"Alim."''',
        '''"Hippocrates"|"Hp."|"De arte (The Art)"|"Ars"''',
        '''"Hippocrates"|"Hp."|"De articulis (On Joints)"|"Art."''',
        '''"Hippocrates"|"Hp."|"De vulneribus capitis (On Wounds in the Head)"|"VC"''',
        '''"Hippocrates"|"Hp."|"De decente habitu (Decorum)"|"Dec."''',
        '''"Hippocrates"|"Hp."|"De dentitione (Dentition)"|"Dent."''',
        '''"Hippocrates"|"Hp."|"De victu = De diaeta (Regimen)"|"Vict."''',
        '''"Hippocrates"|"Hp."|"De diaeta in morbis acutis (Regimen in Acute Diseases)"|"Acut."''',
        '''"Hippocrates"|"Hp."|"De diaeta salubri (Regimen in Health)"|"Sal."''',
        '''"Hippocrates"|"Hp."|"De flatibus (Breaths)"|"Flat."''',
        '''"Hippocrates"|"Hp."|"De fracturis (On Fractures)"|"Fract."''',
        '''"Hippocrates"|"Hp."|"De humoribus (Humours)"|"Hum."''',
        '''"Hippocrates"|"Hp."|"De medico (The Physician)"|"Med."''',
        '''"Hippocrates"|"Hp."|"De epidemiis = De morbis popularibus (Epidemics I and III)"|"Epid."''',
        '''"Hippocrates"|"Hp."|"De morbo sacro (The Sacred Disease)"|"Morb. Sacr."''',
        '''"Hippocrates"|"Hp."|"De natura hominis (Nature of Man)"|"Nat. Hom."''',
        '''"Hippocrates"|"Hp."|"De officina medici (In the Surgery)"|"Off."''',
        '''"Hippocrates"|"Hp."|"De vetere medicina = De prisca medicina (Ancient Medicine)"|"VM"''',
        '''"Hippocrates"|"Hp."|"Iusiurandum (Oath)"|"Iusiur."''',
        '''"Hippocrates"|"Hp."|"Lex (Law)"|"Lex"''',
        '''"Hippocrates"|"Hp."|"Praeceptiones (Precepts)"|"Praec."''',
        '''"Hippocrates"|"Hp."|"Prognosticon (Prognostic)"|"Prog."''',
        '''"Hippocrates"|"Hp."|"Mochlicus = Vectiarius (Instruments of Reduction)"|"Mochl."''',
        '''"Hippon"|"Hippon"||''',
        '''"Homerus"|"Il."|"Ilias"|''',
        '''"Homerus"|"Od."|"Odyssee"|''',
        '''"Ion"|"Ion"||''',
        '''"Isocrates"|"Isocr."|"Ad Nicoclem"|2''',
        '''"Isocrates"|"Isocr."|"Nicocles"|3''',
        '''"Isocrates"|"Isocr."|"Panegyricus"|4''',
        '''"Isocrates"|"Isocr."|"Philippus"|5''',
        '''"Isocrates"|"Isocr."|"Evagoras"|9''',
        '''"Isocrates"|"Isocr."|"Helenae encomium"|10''',
        '''"Leucippus"|"Leucipp."||''',
        '''"Lucianus"|"Luc."|"Phalaris I "|1''',
        '''"Lucianus"|"Luc."|"Phalaris II"|2''',
        '''"Lucianus"|"Luc."|"Hippias (Hippias or the Bath)"|3''',
        '''"Lucianus"|"Luc."|"Bacchus (Dionysus)"|4''',
        '''"Lucianus"|"Luc."|"Hercules (Heracles)"|5''',
        '''"Lucianus"|"Luc."|"Electrum (Amber or the Swans)"|6''',
        '''"Lucianus"|"Luc."|"Muscae encomium (The Fly)"|7''',
        '''"Lucianus"|"Luc."|"Nigrinus "|8''',
        '''"Lucianus"|"Luc."|"Demonax"|9''',
        '''"Lucianus"|"Luc."|"De domo (The Hall)"|10''',
        '''"Lucianus"|"Luc."|"Patriae encomium (My Native Land)"|11''',
        '''"Lucianus"|"Luc."|"Verae historiae I (A True Story I)"|13''',
        '''"Lucianus"|"Luc."|"Verae historiae (A True Story II)"|14''',
        '''"Lucianus"|"Luc."|"Calumniae non temere credendum (Slander)"|15''',
        '''"Lucianus"|"Luc."|"Lis consonantium (Iudicium vocalium) (The Consonants at Law)"|16''',
        '''"Lucianus"|"Luc."|"Symposium (The Carousal)"|17''',
        '''"Lucianus"|"Luc."|"Soloecista (The Sham Sophist or the Solecist)"|18''',
        '''"Lucianus"|"Luc."|"Cataplus (The Downward Journey)"|19''',
        '''"Lucianus"|"Luc."|"Iuppiter confutatus (Zeus Catechized)"|20''',
        '''"Lucianus"|"Luc."|"Iuppiter tragoedus (Zeus Rants)"|21''',
        '''"Lucianus"|"Luc."|"Gallus (The Dream, or the Cock)"|22''',
        '''"Lucianus"|"Luc."|"Prometheus "|23''',
        '''"Lucianus"|"Luc."|"Icaromenippus"|24''',
        '''"Lucianus"|"Luc."|"Timon"|25''',
        '''"Lucianus"|"Luc."|"Charon sive contemplantes (Charon or the Inspectors)"|26''',
        '''"Lucianus"|"Luc."|"Vitarum auctio (Philosophies for Sale)"|27''',
        '''"Lucianus"|"Luc."|"Revivescentes sive piscator (The Dead come to Life)"|28''',
        '''"Lucianus"|"Luc."|"Bis accusatus sive tribunalia (The Double Indictment)"|29''',
        '''"Lucianus"|"Luc."|"De sacrificiis (On Sacrifices)"|30''',
        '''"Lucianus"|"Luc."|"Adversus indoctum et libros multos ementem (The Ignorant Bookcollector)"|31''',
        '''"Lucianus"|"Luc."|"Somnium sive vita Luciani (The Dream or Lucian’s Career)"|32''',
        '''"Lucianus"|"Luc."|"De parasito sive artem esse parasiticam (The Parasite)"|33''',
        '''"Lucianus"|"Luc."|"Philopseudes sive incredulus (The Lover of Lies)"|34''',
        '''"Lucianus"|"Luc."|"Dearum iudicium (The Judgment of the Goddesses)"|35''',
        '''"Lucianus"|"Luc."|"De mercede conductis potentium familiaribus (On Salaried Posts in Great Houses)"|36''',
        '''"Lucianus"|"Luc."|"Anacharsis"|37''',
        '''"Lucianus"|"Luc."|"Menippus sive necyomantia (Menippus or the Descent into Hades)"|38''',
        '''"Lucianus"|"Luc."|"Asinus (Lucius or the Ass)"|39''',
        '''"Lucianus"|"Luc."|"De luctu (On Funerals)"|40''',
        '''"Lucianus"|"Luc."|"Rhetorum praeceptor (A Professor of Public Speaking)"|41''',
        '''"Lucianus"|"Luc."|"Alexander (Alexander the False Prophet)"|42''',
        '''"Lucianus"|"Luc."|"Imagines (Essays in Portraiture)"|43''',
        '''"Lucianus"|"Luc."|"De Syria dea (The Goddess of Syria)"|44''',
        '''"Lucianus"|"Luc."|"De saltatione (The Dance)"|45''',
        '''"Lucianus"|"Luc."|"Lexiphanes "|46''',
        '''"Lucianus"|"Luc."|"Eunuchus (The Eunuch)"|47''',
        '''"Lucianus"|"Luc."|"De astrologia (Astrology)"|48''',
        '''"Lucianus"|"Luc."|"Pro imaginibus (Essays in Portraiture Defended)"|50''',
        '''"Lucianus"|"Luc."|"Pseudologista (The Mistaken Critic)"|51''',
        '''"Lucianus"|"Luc."|"Deorum concilium (The Parliament of the Gods)"|52''',
        '''"Lucianus"|"Luc."|"Tyrannicida (The Tyrannicide)"|53''',
        '''"Lucianus"|"Luc."|"Abdicatus (Disowned)"|54''',
        '''"Lucianus"|"Luc."|"De morte Peregrini (The Passing of Peregrinus)"|55''',
        '''"Lucianus"|"Luc."|"Fugitivi (The Runaways)"|56''',
        '''"Lucianus"|"Luc."|"Toxaris vel amicitia (Toxaris or Friendship)"|57''',
        '''"Lucianus"|"Luc."|"Quomodo historia conscribenda sit (How to Write History)"|59''',
        '''"Lucianus"|"Luc."|"Dipsades (The Dipsads)"|60''',
        '''"Lucianus"|"Luc."|"Saturnalia (Saturnalia)"|61''',
        '''"Lucianus"|"Luc."|"Herodotus"|62''',
        '''"Lucianus"|"Luc."|"Zeuxis (Zeuxis or Antiochus)"|63''',
        '''"Lucianus"|"Luc."|"Pro lapsu inter salutandum (A Slip of the Tongue in Greeting)"|64''',
        '''"Lucianus"|"Luc."|"Apologia (Apology)"|65''',
        '''"Lucianus"|"Luc."|"Harmonides (Harmonides)"|66''',
        '''"Lucianus"|"Luc."|"Hesiodus (A Conversation with Hesiod)"|67''',
        '''"Lucianus"|"Luc."|"Scytha (The Scythian or the Consul)"|68''',
        '''"Lucianus"|"Luc."|"Podagra (Gout)"|69''',
        '''"Lucianus"|"Luc."|"Hermotimus"|70''',
        '''"Lucianus"|"Luc."|"Prometheus es in verbis (The One who said “You’re Prometheus in words”)"|71''',
        '''"Lucianus"|"Luc."|"Navigium (The Ship or the Wishes)"|73''',
        '''"Lucianus"|"Luc."|"Dialogi mortuorum (The Dialogues of the Dead)"|77''',
        '''"Lucianus"|"Luc."|"Dialogi marini (Dialogues of Sea-Gods)"|78''',
        '''"Lucianus"|"Luc."|"Dialogi deorum (Dialogues of the Gods)"|79''',
        '''"Lucianus"|"Luc."|"Dialogi meretricii (Dialogues of the Courtesans)"|80''',
        '''"Lucianus"|"Luc."|"Nero"|84''',
        '''"Lucianus"|"Luc."|"Timarion"|86''',
        '''"Lucianus?"|"[Luc.]"|"Macrobii (Octogenarians)"|12''',
        '''"Lucianus?"|"[Luc.]"|"Amores (Affairs of the Heart)"|49''',
        '''"Lucianus?"|"[Luc.]"|"Demosthenis encomium (In Praise of Demosthenes)"|58''',
        '''"Lucianus?"|"[Luc.]"|"Halcyon"|72''',
        '''"Lucianus?"|"[Luc.]"|"Ocypus (Swift-of-Foot)"|74''',
        '''"Lucianus?"|"[Luc.]"|"Cynicus (The Cynic)"|76''',
        '''"Lucianus?"|"[Luc.]"|"Philopatris (The Patriot)"|82''',
        '''"Lucianus?"|"[Luc.]"|"Charidemus "|83''',
        '''"Lucianus?"|"[Luc.]"|"Epigrammata"|85''',
        '''"Lysias"|"Lys."|"De caede Eratosthenis (Over de moord op Eratosthenes)"|1''',
        '''"Lysias"|"Lys."|"Epitaphius"|2''',
        '''"Lysias"|"Lys."|"Contra Simonem"|3''',
        '''"Lysias"|"Lys."|"Over het opzettelijk toebrengen van een verwonding"|4''',
        '''"Lysias"|"Lys."|"Pro Callia"|5''',
        '''"Lysias"|"Lys."|"In Andocidem "|6''',
        '''"Lysias"|"Lys."|"Areopagiticus"|7''',
        '''"Lysias"|"Lys."|"Pro milite"|9''',
        '''"Lysias"|"Lys."|"In Theomnestum 1"|10''',
        '''"Lysias"|"Lys."|"In Eratosthenem"|12''',
        '''"Lysias"|"Lys."|"In Agoratum"|13''',
        '''"Lysias"|"Lys."|"In Alcibiadem 1"|14''',
        '''"Lysias"|"Lys."|"In Alcibiadem 2"|15''',
        '''"Lysias"|"Lys."|"Pro Mantitheo"|16''',
        '''"Lysias"|"Lys."|"Δημοσίων ἀδικημάτων "|17''',
        '''"Lysias"|"Lys."|"Over het bezit van de broer van Nicias"|18''',
        '''"Lysias"|"Lys."|"Over het geld van Aristophanes"|19''',
        '''"Lysias"|"Lys."|"Pro Polystrato"|20''',
        '''"Lysias"|"Lys."|"Verdediging tegen aanklacht van omkoping"|21''',
        '''"Lysias"|"Lys."|"Tegen de graanhandelaren"|22''',
        '''"Lysias"|"Lys."|"In Pancleonem"|23''',
        '''"Lysias"|"Lys."|"Voor de invalide"|24''',
        '''"Lysias"|"Lys."|"Verdediging tegen de aanklacht van omverwerping van de democratie"|25''',
        '''"Lysias"|"Lys."|"Tegen Euander"|26''',
        '''"Lysias"|"Lys."|"In Epicratem"|27''',
        '''"Lysias"|"Lys."|"In Ergoclem"|28''',
        '''"Lysias"|"Lys."|"In Philocratem"|29''',
        '''"Lysias"|"Lys."|"In Nicomachum"|30''',
        '''"Lysias"|"Lys."|"In Philonem"|31''',
        '''"Lysias"|"Lys."|"In Diogitonem"|32''',
        '''"Lysias"|"Lys."|"Olympiacus"|33''',
        '''"Lysias"|"Lys."|"Over (het omverwerpen van) de constitutie"|34''',
        '''"Lysias?"|"[Lys.]"|"Aanklacht wegens smaad jegens clubgenoten"|8''',
        '''"Lysias?"|"[Lys.]"|"In Theomnestum 2"|11''',
        '''"Melissus"|"Meliss."||''',
        '''"Menander"|"Men."|"Aspis"|"Asp."''',
        '''"Menander"|"Men."|"Dyscolus"|"Dysc."''',
        '''"Menander"|"Men."|"Epitrepontes"|"Epitr."''',
        '''"Menander"|"Men."|"Periciromene"|"Peric."''',
        '''"Menander"|"Men."|"Samia"|"Sam."''',
        '''"Metrodorus"|"Metrod."||''',
        '''"Nausiphanes"|"Nausiph."||''',
        '''"Nessas"|"Nessas"||''',
        '''"Nieuwe Testament"|"NT"|"Acta apostolorum (De handelingen van de apostelen)"|"Act. Ap."''',
        '''"Nieuwe Testament"|"NT"|"Apocalypsis Joannis (Openbaring van Johannes)"|"Apoc."''',
        '''"Nieuwe Testament"|"NT"|"Epistula Jacobi (De brief van Jakobus)"|"Iac."''',
        '''"Nieuwe Testament"|"NT"|"Epistula Joannis I (De eerste brief van Johannes)"|"1 Io."''',
        '''"Nieuwe Testament"|"NT"|"Epistula Joannis II (De tweede brief van Johannes)"|"2 Io."''',
        '''"Nieuwe Testament"|"NT"|"Epistula Joannis III (De derde brief van Johannes)"|"3 Io."''',
        '''"Nieuwe Testament"|"NT"|"Epistula Juda (De brief van Judas)"|"Iud."''',
        '''"Nieuwe Testament"|"NT"|"Epistula Pauli ad Colossenses (Paulus' brief aan de Kolossenzen)"|"Col."''',
        '''"Nieuwe Testament"|"NT"|"Epistula Pauli ad Corinthios I (Paulus' eerste brief aan de Korintiërs)"|"1 Cor."''',
        '''"Nieuwe Testament"|"NT"|"Epistula Pauli ad Corinthios II (Paulus' tweede brief aan de Korintiërs)"|"2 Cor."''',
        '''"Nieuwe Testament"|"NT"|"Epistula Pauli ad Ephesios (Paulus' brief aan de Efeziërs)"|"Eph."''',
        '''"Nieuwe Testament"|"NT"|"Epistula Pauli ad Galatas (Paulus' brief aan de Galaten)"|"Gal."''',
        '''"Nieuwe Testament"|"NT"|"Epistula Pauli ad Hebraeos (Paulus' brief aan de Hebreeën)"|"Hebr."''',
        '''"Nieuwe Testament"|"NT"|"Epistula Pauli ad Philemonem (Paulus' brief aan Filemon)"|"Philem."''',
        '''"Nieuwe Testament"|"NT"|"Epistula Pauli ad Philippenses (Paulus' brief aan de Filippenzen)"|"Phil."''',
        '''"Nieuwe Testament"|"NT"|"Epistula Pauli ad Romanos (Paulus' brief aan de Romeinen)"|"Rom."''',
        '''"Nieuwe Testament"|"NT"|"Epistula Pauli ad Thessalonicenses I (Paulus' eerste brief aan de Tessalonicenzen)"|"1 Thess."''',
        '''"Nieuwe Testament"|"NT"|"Epistula Pauli ad Thessalonicenses II (Paulus' tweede brief aan de Tessalonicenzen)"|"2 Thess."''',
        '''"Nieuwe Testament"|"NT"|"Epistula Pauli ad Timotheum I (Paulus' eerste brief aan Timoteüs)"|"1 Tim."''',
        '''"Nieuwe Testament"|"NT"|"Epistula Pauli ad Timotheum II (Paulus' tweede brief aan Timoteüs)"|"2 Tim."''',
        '''"Nieuwe Testament"|"NT"|"Epistula Pauli ad Titum (Paulus' brief aan Titus)"|"Tit."''',
        '''"Nieuwe Testament"|"NT"|"Epistula Petri I (De eerste brief van Petrus)"|"1 Pet."''',
        '''"Nieuwe Testament"|"NT"|"Epistula Petri II (De tweede brief van Petrus)"|"2 Pet."''',
        '''"Nieuwe Testament"|"NT"|"Evangelium secundum Joannem (Het evangelie volgens Johannes)"|"Io."''',
        '''"Nieuwe Testament"|"NT"|"Evangelium secundum Lucam (Het evangelie volgens Lucas)"|"Luc."''',
        '''"Nieuwe Testament"|"NT"|"Evangelium secundum Marcum (Het evangelie volgens Marcus)"|"Marc."''',
        '''"Nieuwe Testament"|"NT"|"Evangelium secundum Matthaeum (Het evangelie volgens Matteüs)"|"Mt."''',
        '''"Parmenides"|"Parm."||''',
        '''"Philolaus"|"Philol."||''',
        '''"Pindarus"|"Pind."|"Olympische Oden"|"O."''',
        '''"Pindarus"|"Pind."|"Pythische Oden"|"P."''',
        '''"Plato"|"Plat."|"Alcibiades I"|"Alc.1"''',
        '''"Plato"|"Plat."|"Alcibiades II"|"Alc.2"''',
        '''"Plato"|"Plat."|"Amatores"|"Amat."''',
        '''"Plato"|"Plat."|"Apologia Socratis"|"Ap."''',
        '''"Plato"|"Plat."|"Charmides"|"Chrm."''',
        '''"Plato"|"Plat."|"Clitophon"|"Clit."''',
        '''"Plato"|"Plat."|"Cratylus"|"Crat."''',
        '''"Plato"|"Plat."|"Crito"|"Crit."''',
        '''"Plato"|"Plat."|"Critias"|"Criti."''',
        '''"Plato"|"Plat."|"Epinomis "|"Epin."''',
        '''"Plato"|"Plat."|"Epistulae"|"Epist."''',
        '''"Plato"|"Plat."|"Eryxias"|"Eryx."''',
        '''"Plato"|"Plat."|"Euthydemus"|"Euthyd."''',
        '''"Plato"|"Plat."|"Euthyphro"|"Euthyph."''',
        '''"Plato"|"Plat."|"Gorgias"|"Grg."''',
        '''"Plato"|"Plat."|"Hipparchus"|"Hipparch."''',
        '''"Plato"|"Plat."|"Hippias maior "|"HpMa"''',
        '''"Plato"|"Plat."|"Hippias minor"|"HpMi"''',
        '''"Plato"|"Plat."|"Ion"|"Ion"''',
        '''"Plato"|"Plat."|"Laches"|"Lach."''',
        '''"Plato"|"Plat."|"Leges"|"Lg."''',
        '''"Plato"|"Plat."|"Lysis"|"Lys."''',
        '''"Plato"|"Plat."|"Menexenus"|"Menex."''',
        '''"Plato"|"Plat."|"Meno"|"Men."''',
        '''"Plato"|"Plat."|"Minos"|"Min."''',
        '''"Plato"|"Plat."|"Parmenides"|"Parm."''',
        '''"Plato"|"Plat."|"Phaedo"|"Phaed."''',
        '''"Plato"|"Plat."|"Phaedrus"|"Phaedr."''',
        '''"Plato"|"Plat."|"Philebus"|"Phlb."''',
        '''"Plato"|"Plat."|"Politicus"|"Plt."''',
        '''"Plato"|"Plat."|"Protagoras"|"Prot."''',
        '''"Plato"|"Plat."|"Respublica"|"Resp."''',
        '''"Plato"|"Plat."|"Sophista"|"Sph."''',
        '''"Plato"|"Plat."|"Symposium"|"Smp."''',
        '''"Plato"|"Plat."|"Theaetetus"|"Tht."''',
        '''"Plato"|"Plat."|"Theages"|"Theag."''',
        '''"Plato"|"Plat."|"Timaeus"|"Tim."''',
        '''"Plato?"|"[Plat.]"|"Axiochus"|"Ax."''',
        '''"Plato?"|"[Plat.]"|"De iusto"|"Iust."''',
        '''"Plato?"|"[Plat.]"|"De virtute"|"Virt."''',
        '''"Plato?"|"[Plat.]"|"Definitiones"|"Def."''',
        '''"Plato?"|"[Plat.]"|"Demodocus"|"Demod."''',
        '''"Plato?"|"[Plat.]"|"Sisyphus"|"Sis."''',
        '''"Plutarchus"|"Plut."|"Aemilius Paullus"|"Aem."''',
        '''"Plutarchus"|"Plut."|"Agesilaus"|"Ages."''',
        '''"Plutarchus"|"Plut."|"Agis et Cleomenes"|"Agis et Cl."''',
        '''"Plutarchus"|"Plut."|"Alcibiades"|"Alc."''',
        '''"Plutarchus"|"Plut."|"Alexander"|"Alex."''',
        '''"Plutarchus"|"Plut."|"Antonius"|"Ant."''',
        '''"Plutarchus"|"Plut."|"Aratus"|"Arat."''',
        '''"Plutarchus"|"Plut."|"Aristides"|"Arist."''',
        '''"Plutarchus"|"Plut."|"Artaxerxes"|"Art."''',
        '''"Plutarchus"|"Plut."|"Brutus"|"Brut."''',
        '''"Plutarchus"|"Plut."|"Caesar"|"Caes."''',
        '''"Plutarchus"|"Plut."|"Camillus"|"Cam."''',
        '''"Plutarchus"|"Plut."|"Cato Maior"|"CMa"''',
        '''"Plutarchus"|"Plut."|"Cato Minor"|"CMi"''',
        '''"Plutarchus"|"Plut."|"Cicero"|"Cic."''',
        '''"Plutarchus"|"Plut."|"Cimon"|"Cim."''',
        '''"Plutarchus"|"Plut."|"Coriolanus, Gaius Marcius"|"Cor."''',
        '''"Plutarchus"|"Plut."|"Crassus"|"Crass."''',
        '''"Plutarchus"|"Plut."|"Demetrius"|"Demetr."''',
        '''"Plutarchus"|"Plut."|"Demosthenes"|"Demosth."''',
        '''"Plutarchus"|"Plut."|"Dion"|"Dion"''',
        '''"Plutarchus"|"Plut."|"Eumenes"|"Eum."''',
        '''"Plutarchus"|"Plut."|"Fabius Maximus"|"Fab."''',
        '''"Plutarchus"|"Plut."|"Flamininus, Titus Quintus"|"Flam."''',
        '''"Plutarchus"|"Plut."|"Galba"|"Galb."''',
        '''"Plutarchus"|"Plut."|"Lucullus"|"Luc."''',
        '''"Plutarchus"|"Plut."|"Lycurgus"|"Lyc."''',
        '''"Plutarchus"|"Plut."|"Lysander"|"Lys."''',
        '''"Plutarchus"|"Plut."|"Marcellus"|"Marc."''',
        '''"Plutarchus"|"Plut."|"Marius"|"Mar."''',
        '''"Plutarchus"|"Plut."|"Nicias"|"Nic."''',
        '''"Plutarchus"|"Plut."|"Numa"|"Num."''',
        '''"Plutarchus"|"Plut."|"Otho"|"Oth."''',
        '''"Plutarchus"|"Plut."|"Pelopidas"|"Pel."''',
        '''"Plutarchus"|"Plut."|"Pericles"|"Per."''',
        '''"Plutarchus"|"Plut."|"Philopoemen"|"Phil."''',
        '''"Plutarchus"|"Plut."|"Phocion"|"Phoc."''',
        '''"Plutarchus"|"Plut."|"Pompeius"|"Pomp."''',
        '''"Plutarchus"|"Plut."|"Publicola"|"Publ."''',
        '''"Plutarchus"|"Plut."|"Pyrrhus"|"Pyrrh."''',
        '''"Plutarchus"|"Plut."|"Romulus"|"Rom."''',
        '''"Plutarchus"|"Plut."|"Sertorius"|"Sert."''',
        '''"Plutarchus"|"Plut."|"Solon"|"Sol."''',
        '''"Plutarchus"|"Plut."|"Sulla"|"Sull."''',
        '''"Plutarchus"|"Plut."|"Themistocles"|"Them."''',
        '''"Plutarchus"|"Plut."|"Theseus"|"Thes."''',
        '''"Plutarchus"|"Plut."|"Tiberius et Gaius Gracchus"|"TG et CG"''',
        '''"Plutarchus"|"Plut."|"Timoleon"|"Tim."''',
        '''"Polyclitus"|"Polycl."||''',
        '''"Prodicus"|"Prodic."||''',
        '''"Protagoras"|"Protag."||''',
        '''"Sappho"|"Sapph."||''',
        '''"Sophocles"|"Soph."|"Ajax"|"Ai."''',
        '''"Sophocles"|"Soph."|"Antigone"|"Ant."''',
        '''"Sophocles"|"Soph."|"Electra"|"El."''',
        '''"Sophocles"|"Soph."|"Oedipus Coloneus"|"OC"''',
        '''"Sophocles"|"Soph."|"Oedipus Tyrannus"|"OT"''',
        '''"Sophocles"|"Soph."|"Philoctetes"|"Ph."''',
        '''"Sophocles"|"Soph."|"Trachiniae"|"Tr."''',
        '''"Thales"|"Thal."||''',
        '''"Theocritus"|"Theocr."|"Idyllia"|"Id."''',
        '''"Theocritus"|"Theocr."|"Syrinx"|"Syr."''',
        '''"Theophrastus"|"Thphr."|"Characteres"|"Char."''',
        '''"Thrasymachus"|"Thrasym."||''',
        '''"Thucydides"|"Thuc."|"Historiën"|''',
        '''"Xenophanes"|"Xenoph."||''',
        '''"Xenophon"|"Xen."|"Anabasis"|"An."''',
        '''"Xenophon"|"Xen."|"Cyropaedia"|"Cyr."''',
        '''"Xenophon"|"Xen."|"Hellenica"|"Hell."''',
        '''"Xenophon"|"Xen."|"Memorabilia"|"Mem."''',
        '''"Zeno"|"Zen."||''',
    )

# --- used in both the preparation of expansions (below) and when expanding.
def quirks1(aut, werk, plaats):
    aut_nb = re.sub(r'[][]\s*', '', aut)
    case1 = {'Il.', 'Od.', 'Hdt.', 'Thuc.'}
    # --- e.g. Il.//16.453 -> Il./Il./16.453
    if aut_nb in case1:
        werk = aut_nb
    return aut, werk, plaats

# --- used only when expanding.
def quirks2(aut, werk, plaats):
    aut_nb = re.sub(r'[][]\s*', '', aut)
    case2 = {'Lys.', 'Luc.', 'Dem.', 'Apollod.', 'And.', 'Isocr.'}
    # --- e.g. Isocr.//2.12 -> Isocr./2/12
    if aut_nb in case2:
        werk, *p = plaats.split('.')
        plaats = '.'.join(p)
    return aut, werk, plaats

def quirks(aut, werk, plaats):
    return quirks2(*quirks1(aut, werk, plaats))

def remove_quotes(x):
    return re.sub(r'^ "(.*)" $', r'\1', x, flags=re.X)

def _prepare_expansions_aut_werk():
    exp_aut, exp_cit = {}, {}
    raw = _expansions_aut_werk_raw()
    for row in raw:
        auteur, aut, werk, wer = [remove_quotes(_) for _ in row.split('|')]
        aut, wer, _ = quirks1(aut, wer, None)
        exp_aut[aut] = auteur
        exp_cit[aut] = exp_cit.get(aut, {})
        exp_cit[aut][wer] = (auteur, werk)
    return exp_aut, exp_cit
