# -*- coding: utf-8 -*-

'''

* add xmlns:re everywhere: these functions are automatically available from
lxml xslt api, while xslt 2.0 functions (like `matches`) are not.
* replace matches() with re:match() or re:test()

@todo * what about replace?

* inline grne.functions.xsl in T2, merge headers
* take out the comment-producing nodes in T2 (don't compile)
'''

'''
Note: lxml does not properly handle (or maybe it's on purpose, but anyway)
the DOCTYPE and the processing instructions at the root level, and they will
be thrown away by using these stylesheets.

One solution is to output a dummy element at the root level and then remove
it at the end. Another is to manually add the first three lines again (they
are the same for every lemma).

A third solution would be to change

  <xsl:template match="processing-instruction()">
    <xsl:copy-of select="."/>
    <xsl:text>&#x0A;</xsl:text>
  </xsl:template>

to

  <xsl:template match="processing-instruction()">
    <xsl:copy/>
    <xsl:text>&#x0A;</xsl:text>
  </xsl:template>

It's not clear why this works (and it only matters for the PIs before the root
element). It's also not clear why the author wants to add a newline after the
PIs, but that's not important.

'''

def xslt_t1_0():
    # --- grne_print_version.xslt
    return '''
<xsl:stylesheet
  version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:re="http://exslt.org/regular-expressions"
>
  <xsl:output method="xml"  indent="no" omit-xml-declaration="yes" />
  <xsl:template match="/">
    <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE contLem SYSTEM "../../hulpdocumenten/dtd/lemma.dtd">
</xsl:text>
  <xsl:apply-templates select="node()"/>
  </xsl:template>

  <xsl:template match="*|text()|comment()|@*">
      <xsl:copy>
        <xsl:apply-templates select="@* | node()"/>
      </xsl:copy>
  </xsl:template>

  <xsl:template match="processing-instruction()">
    <xsl:copy-of select="."/>
    <xsl:text>&#x0A;</xsl:text>
  </xsl:template>

  <xsl:template match="cit[verw/aut[not(re:match(.,'^Hdt\.|^Il\.|^Od\.|^Soph\.|^Eur\.|^Plat\.'))]]">
    <xsl:copy>
      <xsl:attribute name="remove_this">
        <xsl:value-of select="'1'"/>
      </xsl:attribute>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="cit[verw/aut[re:match(.,'^Soph\.')]]">
    <xsl:copy>
      <xsl:if test=".//werk[not(re:match(.,'^OT|^Ant\.|^Ai\.|^El\.'))]">
        <xsl:attribute name="remove_this">
          <xsl:value-of select="'1'"/>
        </xsl:attribute>
       </xsl:if>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="cit[verw/aut[re:match(.,'^Eur\.')]]">
    <xsl:copy>
      <xsl:if test=".//werk[not(re:match(.,'^Hipp\.|^Tr\.|^Med\.|^Ba\.'))]">
        <xsl:attribute name="remove_this">
          <xsl:value-of select="'1'"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="cit[verw/aut[re:match(.,'^Plat\.')]]">
    <xsl:copy>
      <xsl:if test=".//werk[not(re:match(.,'^Phaed\.|^Men\.|^Ap\.|^Prot\.|^Resp\.|^Phaedr\.|^Grg\.|^Smp\.'))]">
        <xsl:attribute name="remove_this">
          <xsl:value-of select="'1'"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
'''

# --- grne_print_version_2.xslt
def xslt_t1_1():
    return '''
<xsl:stylesheet
  version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:re="http://exslt.org/regular-expressions"
>
  <xsl:output method="xml"  indent="no" omit-xml-declaration="yes" />
  <xsl:template match="/">
    <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE contLem SYSTEM "../../hulpdocumenten/dtd/lemma.dtd">
</xsl:text>
  <xsl:apply-templates select="node()"/>
  </xsl:template>

  <xsl:template match="*|text()|comment()|@*">
      <xsl:copy>
        <xsl:apply-templates select="@* | node()"/>
      </xsl:copy>
  </xsl:template>

  <xsl:template match="processing-instruction()">
    <xsl:copy-of select="."/>
    <xsl:text>&#x0A;</xsl:text>
  </xsl:template>

  <xsl:template match="//gebrW">
    <xsl:copy>
      <xsl:if test="following-sibling::*[1][self::cit]/@remove_this">
        <xsl:choose>
          <xsl:when test="re:match(.,'^[\s]*(meestal|zelden)')">
            <xsl:attribute name="keep_this">
              <xsl:value-of select="'2'"/>
            </xsl:attribute>
          </xsl:when>
          <xsl:when test="re:match(.,'^((later\s+ook\s+)|(ook\s+))\s*(med\.|act\.)\s*$')">
            <xsl:attribute name="keep_this">
              <xsl:value-of select="'3'"/>
            </xsl:attribute>
          </xsl:when>
          <xsl:when test="re:match(.,'^((later\s+(ook\s+)?)|(ook\s+))?\s*met\s+(gen\.|dat\.|acc\.|inf\.|AcI|AcP|ptc)')">
            <xsl:attribute name="keep_this">
              <xsl:value-of select="'4'"/>
            </xsl:attribute>
          </xsl:when>
          <xsl:when test="re:match(.,'^(later\s+)*(ook\s+)*met[\s\)\(]*(gen\.|dat\.|acc\.|inf|AcI|AcP|ptc)[\s\)\(]+(en|of|en/of|\+)[\s\)\(]+(gen\.|dat\.|acc\.|inf|AcI|AcP|ptc)[\s\)\(]*')">
            <xsl:attribute name="keep_this">
              <xsl:value-of select="'5'"/>
            </xsl:attribute>
          </xsl:when>
          <xsl:when test="re:match(.,'^(later\s+)*(ook\s+)*met\s+(ἀνά|ἀπό|ἀμφί|ἀντί|δία|εἰς|ἐκ|ἐν|ἐς|ἐπί|κατά|μετά|παρά|περί|πρός|σύν|ὑπέρ|ὑπό)\s+(en|of|en/of|\+)\s+(gen\.|dat\.|acc\.|inf\.|AcI|AcP|ptc)')">
            <xsl:attribute name="keep_this">
              <xsl:value-of select="'6'"/>
            </xsl:attribute>
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="remove_this">
              <xsl:value-of select="'2'"/>
            </xsl:attribute>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
'''

def xslt_t1_2():
    # --- grne_print_version_4.xslt
    return '''
<xsl:stylesheet
  version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:re="http://exslt.org/regular-expressions"
>
  <xsl:output method="xml"  indent="no" omit-xml-declaration="yes" />
  <xsl:template match="/">
    <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE contLem SYSTEM "../../hulpdocumenten/dtd/lemma.dtd">
</xsl:text>
  <xsl:apply-templates select="node()"/>
  </xsl:template>

  <xsl:template match="*|text()|comment()|@*">
      <xsl:copy>
        <xsl:apply-templates select="@* | node()"/>
      </xsl:copy>
  </xsl:template>

  <xsl:template match="processing-instruction()">
    <xsl:copy-of select="."/>
    <xsl:text>&#x0A;</xsl:text>
  </xsl:template>

  <xsl:template match="//gebrW[@remove_this]">
    <xsl:copy>
      <xsl:if test="following-sibling::*[2][self::cit[not(@remove_this)]]">
        <xsl:attribute name="check_this">
          <xsl:value-of select="'1'"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="re:match(normalize-space(.),';.+')">
        <xsl:attribute name="check_this">
          <xsl:value-of select="'3'"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="//cit[@remove_this][parent::*[not(vertM)]]">
    <xsl:copy>
      <xsl:attribute name="check_this">
        <xsl:value-of select="'2'"/>
      </xsl:attribute>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="//gebrW[not(@remove_this)][re:match(normalize-space(.),';.+')]">
    <xsl:copy>
      <xsl:attribute name="check_this">
        <xsl:value-of select="'3'"/>
      </xsl:attribute>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="//isgelCit">
    <xsl:copy>
      <xsl:attribute name="check_this">
        <xsl:value-of select="'4'"/>
      </xsl:attribute>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
'''

def xslt_t1_3():
    # --- grne_print_version_6.xslt
    return '''
<xsl:stylesheet
  version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:re="http://exslt.org/regular-expressions"
>
  <xsl:output method="xml"  indent="no" omit-xml-declaration="yes" />
  <xsl:template match="/">
    <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE contLem SYSTEM "../../hulpdocumenten/dtd/lemma.dtd">
</xsl:text>
  <xsl:apply-templates select="node()"/>
  </xsl:template>

  <xsl:template match="*|text()|comment()|@*">
      <xsl:copy>
        <xsl:apply-templates select="@* | node()"/>
      </xsl:copy>
  </xsl:template>

  <xsl:template match="processing-instruction()">
    <xsl:copy-of select="."/>
    <xsl:text>&#x0A;</xsl:text>
  </xsl:template>

  <xsl:template match="//gebrW">
    <xsl:copy>
      <xsl:if test="(following-sibling::*[1][self::cit] and following-sibling::*[2][self::cit])">
        <xsl:attribute name="check_this_again">
          <xsl:value-of select="'5'"/>
        </xsl:attribute>
      </xsl:if>
       <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
'''

# from hulpdocumenten/xslt/schoolreductie-T1/grne_print_version_t1_update_status.xslt
def xslt_t1_update_status():
    return '''
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
  <xsl:output method="xml"  indent="no" omit-xml-declaration="yes" />
  <xsl:template match="/">
    <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE contLem SYSTEM "../../hulpdocumenten/dtd/lemma.dtd">
    </xsl:text>
    <xsl:apply-templates select="node()"/>
  </xsl:template>

  <xsl:template match="*|text()|comment()|@*">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="processing-instruction()">
    <xsl:copy-of select="."/>
    <xsl:text>&#x0A;</xsl:text>
  </xsl:template>

  <xsl:template match="//metadata">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
      <!-- manually managing the indent level with xsl:text is probably not
      the best way, but it works for now. -->
      <xsl:text>  </xsl:text>
      <status-t1 status-t1="uitgevoerd"/>
      <xsl:text>&#x0A;</xsl:text>
      <xsl:text>  </xsl:text>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
'''

# --- not used: lxml api fails in several ways and we are currently using
# saxon9he-java externally.
def xslt_t2_0():
    # --- grne_interpunction.xslt with grne.functions.xsl inlined and
    # headers merged.

    return '''
<xsl:stylesheet
    version="2.0"
    xmlns:f="http://functions.grne.org"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:re="http://exslt.org/regular-expressions"
>

    <xsl:function name="f:dotornot" as="xs:string">
        <xsl:param name="strng"/>
        <xsl:variable name="s">
            <xsl:choose>
                <!-- when there is a dot already do not add an extra dot -->
                <xsl:when test="re:match($strng, '\.\s*[,:;]?\s*$')">
                    <xsl:value-of select="replace($strng, '\.\s*[,:;]?\s*$', '.')"/>
                </xsl:when>
                <xsl:when test="re:match($strng, '[,;:]\s*$')">
                    <xsl:value-of select="replace($strng, '.\s*$', '.')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="replace($strng, '([^\s])\s*$', '$1.')"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:sequence select="$s"/>
    </xsl:function>

   <xsl:function name="f:siblings" as="xs:string">
        <xsl:param name="nodes"/>
        <xsl:variable name="s">
            <xsl:for-each select="$nodes">
                <xsl:choose>
                    <xsl:when test="((local-name() = 'gebrW') and @remove_this)">
                        <xsl:value-of select="'XgebrW'"/>
                    </xsl:when>
                    <xsl:when test="((local-name() = 'cit') and @remove_this)">
                        <xsl:value-of select="'Xcit'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="local-name()"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </xsl:variable>
        <xsl:sequence select="$s"/>
    </xsl:function>

    <xsl:output method="xml"  indent="yes" omit-xml-declaration="yes" />

    <xsl:template match="/">
        <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE contLem SYSTEM "../../hulpdocumenten/dtd/lemma.dtd">
</xsl:text>
        <xsl:apply-templates select="node()"/>
    </xsl:template>

    <xsl:template match="* | text() | comment() | @*">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="* | text() | comment() | @*" mode="special">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()" mode="special"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="processing-instruction()">
        <xsl:copy-of select="."/>
        <xsl:text>&#x0A;</xsl:text>
    </xsl:template>

    <xsl:template match="text()[ancestor::vertM[not(@remove_this)][descendant::text()[normalize-space(.)!=''][last()]=current()]][re:match(current(), ':\s*$')]">
        <xsl:variable name="prnt" select="ancestor::vertM"/>
        <xsl:variable name="sibs" select="f:siblings($prnt/following-sibling::*)"/>
        <xsl:variable name="found"><xsl:value-of select="concat('[', ., ']')"/></xsl:variable>
        <xsl:choose>
            <xsl:when test="re:match($sibs, '^(Xcit|XgebrW)+vertM.*$')">
                <!--xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 1: //vertM[re:match(.,'': $'')]/(...[@remove])*/vertM')"/> </xsl:comment-->
                <xsl:value-of select="replace(., ':\s*$', '; ')"/>
            </xsl:when>
            <xsl:when test="re:match($sibs, '^(Xcit|XgebrW)+gebrW.*$')">
                <!--xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 2: //vertM[re:match(.,'': $'')]/(..[@remove])*/gebrW')"/> </xsl:comment-->
                <xsl:value-of select="replace(., ':\s*$', '; ')"/>
            </xsl:when>
            <xsl:when test="re:match($sibs, '^(Xcit|XgebrW)+$')">
                <!--xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 3: //vertM[re:match(.,'': $'')]/(...[@remove])*')"/></xsl:comment-->
                <xsl:value-of select="f:dotornot(.)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="text()[ancestor::vertM[not(@remove_this)][descendant::text()[normalize-space(.)!=''][last()]=current()]][re:match(current(), ',\s*$')]">
        <xsl:variable name="prnt" select="ancestor::vertM"/>
        <xsl:variable name="sibs" select="f:siblings($prnt/following-sibling::*)"/>
        <xsl:variable name="found"><xsl:value-of select="concat('[', ., ']')"/></xsl:variable>
        <xsl:choose>
            <xsl:when test="re:match($sibs, '^(Xcit|XgebrW)+vertM.*$')">
                <!--xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 1b: //vertM[re:match(.,'', $'')]/(...[@remove])*/vertM')"/>)</xsl:comment-->
                <xsl:value-of select="replace(., ',\s*$', '; ')"/>
            </xsl:when>
            <xsl:when test="re:match($sibs, '^(Xcit|XgebrW)+gebrW.*$')">
                <!--xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 2b: //vertM[re:match(.,'', $'')]/(..[@remove])*/gebrW')"/></xsl:comment-->
                <xsl:value-of select="replace(., ',\s*$', '; ')"/>
            </xsl:when>
            <xsl:when test="re:match($sibs, '^(Xcit|XgebrW)+$')">
                <!--xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 3b: //vertM[re:match(.,'', $'')]/(...[@remove])*')"/></xsl:comment-->
                <xsl:value-of select="f:dotornot(.)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="text()[ancestor::vertM[not(@remove_this)][descendant::text()[normalize-space(.)!=''][last()]=current()]][re:match(current(), ';\s*$')]">
        <xsl:variable name="prnt" select="ancestor::vertM"/>
        <xsl:variable name="sibs" select="f:siblings($prnt/following-sibling::*)"/>
        <xsl:variable name="found"><xsl:value-of select="concat('[', ., ']')"/></xsl:variable>
        <xsl:choose>
            <xsl:when test="re:match($sibs, '^(Xcit|XgebrW)+$')">
                <!--xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 3c: //vertM[re:match(.,''; $'')]/(...[@remove])*')"/></xsl:comment-->
                <xsl:value-of select="f:dotornot(.)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="text()[ancestor::vertM[not(@remove_this)][descendant::text()[normalize-space(.)!=''][last()]=current()]][re:match(current(), '[^,:;\s]\s*$')]">
        <xsl:variable name="prnt" select="ancestor::vertM"/>
        <xsl:variable name="sibs" select="f:siblings($prnt/following-sibling::*)"/>
        <xsl:variable name="found"><xsl:value-of select="concat('[', ., ']')"/></xsl:variable>
        <xsl:choose>
            <xsl:when test="re:match($sibs, '^(Xcit|XgebrW)+gebrW.*$')">
                <!--xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 1c: //vertM[re:match(.,''[^,:;\s]\s*$'')]/(...[@remove])*/gebrW')"/> </xsl:comment-->
                <xsl:value-of select="replace(., '(.)\s*$', '$1; ')"/>
            </xsl:when>
            <xsl:when test="re:match($sibs, '^(Xcit|XgebrW)+vertM.*$')">
                <!--xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 2c: //vertM[re:match(.,''[^,:;\s]\s*$'')]/(...[@remove])*/vertM')"/> </xsl:comment-->
                <xsl:value-of select="replace(., '(.)\s*$', '$1; ')"/>
            </xsl:when>
            <xsl:when test="re:match($sibs, '^(Xcit|XgebrW)+$')">
                <!--xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 3d: //vertM[re:match(.,''[^,:;\s]\s*$'')]/(...[@remove])*')"/> </xsl:comment-->
                <xsl:value-of select="f:dotornot(.)"/>
            </xsl:when>
             <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="text()[ancestor::gebrW[not(@remove_this)][descendant::text()[normalize-space(.)!=''][last()]=current()]][re:match(current(), ':\s*$')]">
        <xsl:variable name="prnt" select="ancestor::gebrW"/>
        <xsl:variable name="sibs" select="f:siblings($prnt/following-sibling::*)"/>
        <xsl:variable name="found"><xsl:value-of select="concat('[', ., ']')"/></xsl:variable>
        <xsl:choose>
            <xsl:when test="re:match($sibs, '^(Xcit|XgebrW)+gebrW.*$')">
                <!--xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 4: //gebrW[re:match(.,'': $'')]/(...[@remove])*/gebrW')"/> </xsl:comment-->
                <xsl:value-of select="replace(., ':\s*$', '; ')"/>
            </xsl:when>
            <xsl:when test="re:match($sibs, '^(Xcit|XgebrW)+vertM.*$')">
                <!--xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 5: //gebrW[re:match(.,'': $'')]/(...[@remove])*/vertM')"/> </xsl:comment-->
                <xsl:value-of select="replace(., ':\s*$', '; ')"/>
            </xsl:when>
            <xsl:when test="re:match($sibs, '^(Xcit|XgebrW)+$')">
                <!--xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 6: //gebrW[re:match(.,'': $'')]/(...[@remove])*')"/></xsl:comment-->
                <xsl:value-of select="f:dotornot(.)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="text()[ancestor::gebrW[not(@remove_this)][descendant::text()[normalize-space(.)!=''][last()]=current()]][re:match(current(), '[^,:;\s]\s*$')]">
        <xsl:variable name="prnt" select="ancestor::gebrW"/>
        <xsl:variable name="sibs" select="f:siblings($prnt/following-sibling::*)"/>
        <xsl:variable name="found"><xsl:value-of select="concat('[', ., ']')"/></xsl:variable>
        <xsl:choose>
            <xsl:when test="re:match($sibs, '^(Xcit|XgebrW)+gebrW$')">
                <!--xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 4b: //gebrW[re:match(.,''[^,:;\s]\s*$'')]/(...[@remove])*/gebrW')"/> </xsl:comment-->
                <xsl:value-of select="replace(., '(.)\s*$', '$1; ')"/>
            </xsl:when>
            <xsl:when test="re:match($sibs, '^(Xcit|XgebrW)+$')">
                <!--xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 6b: //gebrW[re:match(.,''[^,:;\s]\s*$'')]/(...[@remove])*')"/> </xsl:comment-->
                <xsl:value-of select="f:dotornot(.)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="text()[ancestor::cit[not(@remove_this)][descendant::text()[normalize-space(.)!=''][last()]=current()]][re:match(., ';\s*$')]">
        <xsl:variable name="prnt" select="ancestor::cit"/>
        <xsl:variable name="sibs" select="f:siblings($prnt/following-sibling::*)"/>
        <xsl:variable name="found"><xsl:value-of select="concat('[', ., ']')"/></xsl:variable>
        <xsl:choose>
            <xsl:when test="re:match($sibs, '^(Xcit|XgebrW)+$')">
                <!--xsl:comment><xsl:value-of select="concat('FOUND: ', $found, ', MATCH: Nr. 7: //cit[re:match(.,''; $'')]')"/></xsl:comment-->
                <xsl:value-of select="f:dotornot(.)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
'''

def xslt_t2_1():
    return '''
<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>

  <xsl:template match="//*[@remove_this]"/>
  <xsl:template match="@keep_this" />
  <xsl:template match="@check_this" />
  <xsl:template match="@check_this_again" />

  <xsl:template match="node() | @*">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
'''
