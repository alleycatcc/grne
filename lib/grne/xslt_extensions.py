from copy import deepcopy
from functools import partial
from html import escape as html_escape
import re

import lxml.etree as etree
from lxml.etree import XSLTExtension, XSLTError
from lxml.etree import Element
import pydash

from grne.afkorting import regex_afkortingen
from grne.afkorting import expand_citation, expand_abbrev_normal
from grne.chars import (
    analyse_and_convert_query, normalise_names,
    regex_greek, to_latin,
)
from grne.util.util import ProgrammerException, ident, is_odd, chunks

from grne.util.xml.replace_text import replace_text_in_xml_tree

from acatpy.io.speak import debug, warn, iwarn
from acatpy.xml import get_text

from config import get as config_get

conf = config_get()
# --- some of the nodes provided to the extension functions are so-called
# 'read-only proxies' with limited functionality -- by making a deepcopy of
# them we can recover a lot of functionality (printing, examining, looping).
from_proxy = deepcopy
to_string = partial(etree.tostring, encoding='unicode')
proxy_to_string = lambda x: to_string(from_proxy(x))

rx_afk = regex_afkortingen()
rx_gr = regex_greek()

'''
Like re.split(rx, text), except that the text is transformed first by the
function `transform`, but returned in its original form.

Example: if `transform` changes Greek to Latin, then

    transform_split(r'(a)', 'παλ', transform)

gives ['π', 'α', 'λ']

`transform` must result in a string with exactly the same length as `text`.
'''

def transform_split(rx, text, transform, flags=0):
    debug('transform_split: given text ^%s$, regex %s' % (text, rx))
    transformed = transform(text)
    split = re.split(rx, transformed, flags=flags)
    output = []
    i = 0
    debug('split %s' % split)
    for token in split:
        l = len(token)
        j = i + l
        output.append(text[i:j])
        i += l
    debug('transform_split: returning %s' % output)
    return output

def make_link_node(text, tail):
    el = Element('div')
    el.attrib['class'] = 'link'
    el.text = text
    el.tail = tail
    return el

'''
`make_resolved_link_node` removes all hyphens from the hoofdW and checks against
naam.

This is how the legacy perl code did it and it works for our use cases.

κλιβαν- and κλιβαν will both link to κλιβαν-, but it's a content error if
someone tries to link to κλιβαν, because it doesn't exist.
'''

def make_resolved_link_node(data_naam, data_homonyms):
    def f(text, tail):
        debug('calling make_resolved_link_node/f, text = %s, tail = %s' % (text, tail))
        hoofdW = text
        normalised = normalise_names(hoofdW)

        m = re.match(r'^ (\d+) \. \s* (.+)', normalised, flags=re.X)
        naam, homonym_nr = None, None
        if m:
            homonym_nr, naam = m.groups()
            debug('make_resolved_link_node/f: found homonym: hr=%s naam=%s' % (homonym_nr, naam))
        else:
            homonym_nr, naam = '1', normalised
        if homonym_nr == '1':
            id0 = pydash.get(data_naam, naam)
        else:
            hs = pydash.get(data_homonyms, naam)
            id0 = pydash.get(hs, homonym_nr)
        debug('make_resolved_link_node/f naam=%s hr=%s id0=%s' % (naam, homonym_nr, id0))

        el = Element('span')
        if id0 is None:
            el.attrib['class'] = 'broken-link'
            el.attrib['data-unresolved'] = html_escape(hoofdW)
        else:
            # --- switch, remove
            el.attrib['data-targetid'] = str(id0)
            el.attrib['class'] = 'link'
        el.text = hoofdW
        el.tail = tail
        debug('make_resolved_link_node/f: returning %s' % to_string(el))
        return el
    return f

def make_abbr_node(abbr, tail):
    debug('abbr %s' % abbr)
    el = Element('span')
    el.attrib['class'] = 'abbr'
    expanded = expand_abbrev_normal(abbr)
    if expanded is not None:
        el.attrib['data-abbr'] = expanded
    else: iwarn(
        "Abbrev expansion: no expansion for %s"
        % abbr
    )
    el.text = abbr
    el.tail = tail
    return el

def make_citg_popup_node(citg, tail):
    el = Element('span')
    el.attrib['class'] = conf.main['citG_class']
    el.text = citg
    el.tail = tail
    return el

def make_highlight_node(hit, tail):
    el = Element('span')
    el.attrib['class'] = conf.main['search_highlight_class']
    el.text = hit
    el.tail = tail
    return el

r"""
Given functions `f` and `g`, `text` which looks like
     a [x b [ x c ... ]]

and a regex which matches 'x'

     (e.g.:
       'every, single, day', regex=r', '
      or
       'met acc. of zonder acc.', regex=r'acc\.'
     )

this function yields the sequence

     f(a), g(x, b), g(x, c), ...

     (e.g.:
       f('every'), g(', ', 'single'), g(', ', 'day'))
      or
       f('met '), g('acc.', ' of zonder '), g('acc.', '')
     )

Note that parentheses will be placed around the entire regex, to ensure that nothing is thrown away
and that the length of the split array is odd.

You should ensure that the regex you pass doesn't contain any parentheses, because this will result
in `None` being present in the split array. If you know what you're doing you can use `zap_nones` to
remove these.
"""

def map_chunks(regex, f, g, text, regex_flags=None, zap_nones=False, input_transform=None):
    rx = r'(%s)' % regex
    if input_transform:
        tt = transform_split(rx, text, input_transform, flags=regex_flags)
    else:
        tt = re.split(rx, text, flags=regex_flags)
    # debug2('map_chunks: regex=%s, text=%s, split(tt)=%s' % ('...', text, tt))
    if not is_odd(len(tt)):
        raise ProgrammerException('Length was not odd: tt=%s, text=%s, rx=%s' % (tt, text, rx))
    if zap_nones:
        tt = [_ for _ in tt if _ is not None]
    newtext, *rest = tt
    f_newtext = f(newtext)
    # debug2('map_chunks: newtext=%s, rest=%s, f_newtext=%s' % (newtext, rest, f_newtext))
    # --- @todo why twice f()?
    to_yield = f(f_newtext)
    # debug2('map_chunks: yielding leftmost piece: %s' % to_yield)
    yield to_yield
    ts = chunks(rest, 2)
    # debug2('map_chunks: ts=%s' % list(chunks(rest, 2)))
    for x in (g(left, right) for left, right in ts):
        # debug2('map_chunks: inside rest loop: yielding (as node) %s' % etree.tostring(x, encoding='unicode'))
        yield x


"""
The lxml api works like this for 'mixed content' xml, where there are no
restrictions on where text can occur:

Given

<span>left<a/>middle</b>right</span>

left is the 'text' of the span node, while middle & right are the 'tail' of
<a/> and </b> respectively.

This makes the following a bit more complex than a more traditional api
would have.

Our extensions are executed after a node is matched using xsl:template. The
cursor node becomes `input_node` in the extension code, and an
`output_parent` is also passed which the extension then
updates/clobbers/augments/whatever it wants.

The general pattern is that we want to search for text within the input node
(meaning its own text and the tails of its children) using a regular
expression, which may match several times, run the text through an arbitrary
transformation, and replace it with text and/or nodes.

Also, the input node may contain other nodes. We iterate through these and
run the xslt templates on each of them, which may result in more recursion.

Two crucial assumptions: the list is not empty (i.e., the xslt transform
must return at least one node), and there is no leading text in what is
returned.

As for the string transformations: given a string like

a x1 b x2 c x3 ...

where `xn` are strings which can be matched with a regex `r`, and the
strings before `x1` and the last `x` are optional, return

A <X>...</X> B <X>...</X> C ...

where `A`, `B`, ... are strings and the `Xn` are elements and the transforms
are given by arbitrary functions.

The general example is to turn

<span class="gebrW">
  adj.
</span>

into

<span class="gebrW">
  <span class="abbr" data-abbr="adj.">adj.</span>
</span>

And

<span class="gebrW">
  adj. <toel>...</toel> wisk.
</span>

into

<span class="gebrW">
  <span class="abbr" data-abbr="adj.">adj.</span>
  <toel>...</toel>
  <span class="abbr" data-abbr="wisk.">wisk.</span>
</span>

and so on for arbitrary content.

This pattern is encapsulated in the `_execute_text_transform` method.

Most of the extensions follow this pattern. `abbrev-cit` is an example of
one which doesn't.

'Looking around' the input xml is quite difficult from inside the extension,
so we send 'arguments' to the extension in the form of elements passed as
children to the extension element, which we then access by running
process_children on the children.

(In extensions:
 self_node: extension node
 input_node: xml node, e.g. span)

The functions `_execute_text_transform`, `_text_transform` and
`map_chunks` are correct (albeit slow and not that easy to follow) for many
different kinds of transforms (citG, abbrev, highlighting). If you're
tracking down a bug in a particular transform, you probably want to check
the `transform` function for that step before looking here.
"""

class Extension(XSLTExtension):
    '''
    Populate the output node using a text transform based on the given
    `transform` function, which takes text as its argument and must return an
    array whose head is a new piece of text (string) and whose tail is 0 or
    more nodes:

    [text, node?, node? ...]

    Note that `input_node` is an element. Text nodes don't exist in the lxml
    api, and it results in a runtime error if the extension is called after
    a template matches a text node.
    '''
    def _execute_text_transform(self, context, self_node, input_node, output_parent, transform):
        transformed = transform(input_node.text)
        text, *rest = transformed
        output_parent.text = (output_parent.text or '') + text
        output_parent.extend(rest)

        child_idx = -1
        for child in input_node:
            child_idx += 1
            res = self.apply_templates(context, child, output_parent=None)
            debug('after apply templates on child %s, res = %s' %
                  (proxy_to_string(child), res))
            # --- for sanity's sake, disallow the case in which the xslt
            # transform returns an empty list.
            if not res: raise XSLTError(
                'XSLT transform returned no results, input was %s' %
                proxy_to_string(child)
            )
            *all_but_last, last = res
            last = from_proxy(last)
            transformed = transform(child.tail)
            newtail, *rest = transformed
            last.tail = last.tail or ''
            last.tail += newtail
            output_parent.extend([*all_but_last, last, *rest])

    def _text_transform(self, text, regex, make_node, text_transform=ident, regex_flags=re.X, input_transform=None):
        debug('text_transform: text=%s, regex=%s' % (text, regex))
        # --- @todo is this right?
        if not text: return ['']
        return map_chunks(regex, text_transform, make_node, text, regex_flags=regex_flags, input_transform=input_transform)

class ExtensionAddEtymLinks(Extension):
    def execute(self, context, self_node, input_node, output_parent):
        arguments = self.process_children(context)
        if len(arguments) != 0:
            raise Exception('Expected exactly 0 child(ren) for ExtensionAddEtymLinks')

        def transform(text, **kw):
            rx = r'(?:\d+\.\s+)?-?%s+-?' % rx_gr
            debug('add-etym-links ext transform() text=%s' % text)
            return self._text_transform(text, rx, make_link_node)
        self._execute_text_transform(context, self_node, input_node, output_parent, transform)

class ExtensionKruisVerw(Extension):
    def execute(self, context, self_node, input_node, output_parent):
        arguments = self.process_children(context)
        if len(arguments) != 0:
            raise Exception('Expected exactly 0 child(ren) for ExtensionKruisVerw')

        # --- the input_node is <div class="kruisVerw"/>
        text_to_link = input_node[0].tail
        debug('text_to_link %s' % text_to_link)

        def transform(text, **kw):
            # --- grab the homonym nr. as well.
            rx = r'(?:\d+ \. \s*)? %s+' % rx_gr
            return self._text_transform(text, rx, make_link_node)
        self._execute_text_transform(context, self_node, input_node, output_parent, transform)

class ExtensionAdjVerb(Extension):
    def execute(self, context, self_node, input_node, output_parent):
        arguments = self.process_children(context)
        if len(arguments) != 0:
            raise Exception('Expected exactly 0 child(ren) for ExtensionAdjVerb')

        # --- the input_node is <div class="r"/>
        text_to_link = input_node.text
        debug('text_to_link %s' % text_to_link)

        def transform(text, **kw):
            rx = r'%s+' % rx_gr
            return self._text_transform(text, rx, make_link_node)
        self._execute_text_transform(context, self_node, input_node, output_parent, transform)

# --- `extra_data` can be `None`, so we need to wait until `transform` is
# actually called to deconstruct it.
def mk_class_extension_resolve_links(extra_data):
    class ExtensionResolveLinks(Extension):
        def execute(self, context, self_node, input_node, output_parent):
            debug('''ExtensionResolveLinks: executing with self_node %s
                  input_node %s output_parent %s''' %
                  (proxy_to_string(self_node), proxy_to_string(input_node),
                   proxy_to_string(output_parent)))
            arguments = self.process_children(context)
            if len(arguments) != 0:
                raise Exception('Expected exactly 0 child(ren) for ExtensionResolveLinks')

            def transform(text, **kw):
                data_naam, data_homonyms = extra_data
                rx = r'.+'
                debug('calling self._text_transform')
                return self._text_transform(text, rx, make_resolved_link_node(data_naam, data_homonyms))
            self._execute_text_transform(context, self_node, input_node, output_parent, transform)
    return ExtensionResolveLinks

class ExtensionAbbrev(Extension):
    def execute(self, context, self_node, input_node, output_parent):
        arguments = self.process_children(context)
        if len(arguments) != 0:
            raise Exception('Expected exactly 0 child(ren) for ExtensionAbbrev')

        debug('this_text=%s' % input_node.text)

        def transform(text):
            rx = rx_afk
            debug('afkorting ext transform() regex=%s, text=%s' % ('...', text))
            return self._text_transform(text, rx, make_abbr_node)
        self._execute_text_transform(context, self_node, input_node, output_parent, transform)

class ExtensionAbbrevCit(Extension):
    def execute(self, context, self_node, input_node, output_parent):
        arguments = self.process_children(context)
        if len(arguments) != 4:
            raise Exception('Expected exactly 4 children for ExtensionAbbrevCit')
        filename_node, aut_node, werk_node, plaats_node = arguments
        filename = filename_node.text
        aut, werk, plaats = (
            re.sub(r'[,;]?\s*$', '', (aut_node.text or '')),
            re.sub(r'[,;]?\s*$', '', (werk_node.text or '')),
            re.sub(r'[,;\.]?\s*$', '', (plaats_node.text or '')),
        )
        warnings, expanded = expand_citation(aut, werk, plaats)
        if warnings:
            wc = (('\n  %s' % _) for _ in warnings)
            warn(
                "Abbrev-cit expansion, filename=%s: %s"
                % (filename, ''.join(wc))
            )
        debug('aut=%s, werk=%s, plaats=%s, expanded=%s' % (aut, werk, plaats, expanded))
        if expanded is not None:
            output_parent.text = expanded

class ExtensionCitGPopup(Extension):
    def execute(self, context, self_node, input_node, output_parent):
        def transform(text, **kw):
            rx = r'%s+' % rx_gr
            return self._text_transform(text, rx, make_citg_popup_node)
        self._execute_text_transform(context, self_node, input_node, output_parent, transform)

def get_is_strict(term):
    _, mode, _, err = analyse_and_convert_query(term)
    if err: return err, None
    return None, mode == 'strict'

class ExtensionSearchHighlightEntire(Extension):
    def execute(self, context, self_node, input_node, output_parent):
        node = make_highlight_node(None, None)
        node.insert(0, from_proxy(input_node))
        output_parent.append(node)

class ExtensionSearchHighlight(Extension):
    def execute(self, context, self_node, input_node, output_parent):
        '''
        `to_latin` does not change the length of the input as long as
        we use `iota_intact`.

        Also note that we check the loose version of all fields, which
        in rare cases could lead to a false positive; this will simply
        mean a (matching) term will be highlighted which wasn't actually
        matched in the index, which is not really a problem.
        '''

        def input_to_latin(the_input):
            latin, _, err = to_latin(the_input, to_lower=False)
            if err:
                warn("Couldn't transform %s to latin: %s" % (the_input, err))
                return the_input
            return latin

        # --- given a search highlighting term and a piece of text, build a
        # regex to search for the term and then insert nodes built by
        # `make_highlight_node` where the matched text occurs.

        def transform(term, text, **kw):
            # --- we are surrounding the regex with \b because we assume
            # solr is tokenising on words.
            # --- we need to check for * at the beginning because \b will fail otherwise.
            # --- (hoofdwoord can start with *)
            escaped = re.escape(term)
            if escaped == term:
                rx = '-?' + '-?'.join(escaped)
            else:
                # --- prints a lot of warnings.
                # warn("transform: highlighting: term contained special chars, so not attempting to match hyphens")
                rx = escaped
            if re.search(r'^\*', term):
                rx = r'%s\b' % rx
            else:
                rx = r'\b%s\b' % rx
            err, is_strict = get_is_strict(term)
            if err:
                warn("Couldn't determine strictness for %s" % term)
                is_strict = True
            input_transform = None if is_strict else input_to_latin
            debug('ExtensionSearchHighlight: transform: rx=%s, text=^%s$' % (rx, text))
            return self._text_transform(text, rx, make_highlight_node, regex_flags=0, input_transform=input_transform)

        arguments = self.process_children(context)
        if len(arguments) != 1:
            raise Exception('Expected exactly 1 child for ExtensionSearchHighlight')
        term_node, = arguments
        term = term_node.text
        # --- remember that transform operates on text + tail + tail + ...
        transformer = partial(transform, term)
        self._execute_text_transform(context, self_node, input_node, output_parent, transformer)

# --- almost impossible to navigate using self_node or input_node because
# they are 'read only proxies' missing most functionality.

class ExtensionCitHandleBeknopt(Extension):

    # --- if any errors were encountered, just copy the input node to the
    # output parent.

    # --- @static
    def _with_errors(input_node, output_parent, errors):
        for error in errors:
            warn(error)
        output_parent.text = input_node.text
        for child in input_node:
            output_parent.append(child)

    def execute(self, context, self_node, input_node, output_parent):
        errors = []
        with_errors = partial(ExtensionCitHandleBeknopt._with_errors, input_node, output_parent)
        arguments = self.process_children(context)
        if len(arguments) != 2:
            raise Exception('Expected exactly 2 children for ExtensionCitHandleBeknopt')
        last_cit_node, filename_node = arguments
        input_node_text = get_text(input_node)
        debug('input_node_text "%s"' % input_node_text)
        debug('last_cit_node.tag %s' % last_cit_node.tag)
        if last_cit_node.tag != 'last-cit':
            raise Exception('Expected tag="last-cit"')
        last_cit_text = last_cit_node.text
        if filename_node.tag != 'filename':
            raise Exception('Expected tag="filename"')
        filename = filename_node.text
        rx_punc = r'([,.:;]) \s* $'
        debug('filename %s' % filename)
        debug('last_cit_text "%s"' % last_cit_text)
        m = re.search(rx_punc, last_cit_text, flags=re.X)
        if not m:
            errors.append("Last cit doesn't end in punctuation: filename=%s, last cit text=%s" % (
                filename,
                last_cit_text,
            ))
            return with_errors(errors)

        punc_last_cit = m.groups()[0]

        # xxx
        if input_node_text.strip() == '':
            warn("Last element before cits was empty in file: %s" % filename)
            errors.append('...')
            return with_errors(errors)

        rx1 = r'\.%s' % rx_punc
        m1 = re.search(rx1, input_node_text, flags=re.X)
        debug('rx_punc %s' % rx_punc)
        m2 = re.search(rx_punc, input_node_text, flags=re.X)
        punc_input_node = None
        if m1:
            debug('m1')
            punc_input_node = m1.groups()[0]
            if punc_last_cit == '.':
                debug('m1-1')
                punc_stan = punc_input_node
                punc_bekn = ''
            else:
                debug('m1-2')
                punc_stan = punc_input_node
                punc_bekn = punc_last_cit
        elif m2:
            debug('m2')
            punc_input_node = m2.groups()[0]
            if punc_input_node == '.':
                debug('m2-1')
                if punc_last_cit == '.':
                    debug('m2-1-1')
                    punc_stan = punc_input_node
                    punc_bekn = '.'
                else:
                    debug('m2-1-2')
                    punc_stan = punc_input_node
                    punc_bekn = punc_input_node + punc_last_cit
            else:
                debug('m2-2')
                punc_stan = punc_input_node
                punc_bekn = punc_last_cit
        else:
            debug('m3')
            punc_stan = ''
            punc_bekn = punc_last_cit

        if m1 or m2:
            def replace(m):
                debug('m was %s' % m)
                debug('m.groups() %s' % list(m.groups()))
                middle = []
                if punc_stan: middle.append(
                    '<span class="punc-stan">%s</span>' % punc_stan,
                )
                if punc_bekn: middle.append(
                    '<span class="punc-bekn">%s</span>' % punc_bekn,
                )
                return ''.join([
                    *middle,
                    m.groups()[1],
                ])
            rx_punc_save_spaces = r'([,.:;]) (\s*) $'
            debug('rx_punc_save_spaces %s' % rx_punc_save_spaces)
            debug('replace %s' % replace)
            errors, newtree = replace_text_in_xml_tree(
                from_proxy(input_node), rx_punc_save_spaces, replace,
                re_flags=re.X,
            )
        else:
            def replace(m):
                middle = []
                if punc_stan: middle.append(
                    '<span class="punc-stan">%s</span>' % punc_stan,
                )
                if punc_bekn: middle.append(
                    '<span class="punc-bekn">%s</span>' % punc_bekn,
                )
                return ''.join([
                    m.groups()[0],
                    *middle,
                    m.groups()[1],
                ])
            rx_punc_save_spaces = r'(.) (\s*) $'
            errors, newtree = replace_text_in_xml_tree(
                from_proxy(input_node), rx_punc_save_spaces, replace,
                re_flags=re.X,
            )

        debug('newtree, errors %s' % errors)
        debug('newtree %s' % etree.tostring(newtree))
        debug('punc_last_cit "%s"' % punc_last_cit)
        debug('punc_input_node "%s"' % punc_input_node)
        debug('punc_stan "%s"' % punc_stan)
        debug('punc_bekn "%s"' % punc_bekn)
        debug('output_parent.text: %s' % output_parent.text)
        output_parent.text = newtree.text
        for child in newtree:
            output_parent.append(child)
