# -*- coding: utf-8 -*-

import proj_paths # noqa

import re

from lxml import etree

from grne.chars import canonical
from grne.solr.extensions import extension_wrapper

from acatpy.colors import bright_red, underline
from acatpy.io.speak import warn, info_str
from acatpy.util import rejectc, slurp
from acatpy.xml import xslt_transform

from config import get as config_get

(
    ExtensionMakeLoose,
    ExtensionMakeHoofdWClean,
    ExtensionMakeHoofdWSort,
    ExtensionMakeUUID,
    ExtensionGetCitRoot,
) = extension_wrapper()

def matches_doctype(line):
    return re.match('^.+DOCTYPE.+$', line) is not None

def remove_doctype(xml):
    xml_lines = xml.splitlines()
    num, result = rejectc(matches_doctype, xml_lines)
    if num > 1: warn('Too many DOCTYPE lines removed (%s)' % bright_red(str(num)))
    if num == 0: warn('No DOCTYPE removed')
    return ''.join(result)

def remove_entity_iota_macron_asper(xml):
    return re.sub('\\&iota-macron-asper;', 'ῑ̔', xml)

def pre_transform(xml, remove_doctype_arg=False, remove_entities=False):
    stage1 = remove_doctype(xml) if remove_doctype_arg else xml
    return remove_entity_iota_macron_asper(stage1) if remove_entities else stage1

def transform(xml):
    conf_main = config_get().main
    xslt = slurp(conf_main['stylesheet_pre_index_stage_0'])
    return xslt_transform(xml, xslt, parse_xml=True)

def post_transform(xml):
    conf_main = config_get().main
    py_ext_ns = conf_main['python_extension_ns']

    extensions = [
        (py_ext_ns, 'make-loose', ExtensionMakeLoose),
        (py_ext_ns, 'make-hoofdW-clean', ExtensionMakeHoofdWClean),
        (py_ext_ns, 'make-hoofdW-sort', ExtensionMakeHoofdWSort),
        (py_ext_ns, 'make-uuid', ExtensionMakeUUID),
        (py_ext_ns, 'get-cit-root', ExtensionGetCitRoot),
    ]

    xslt_fmt = slurp(conf_main['stylesheet_pre_index_stage_1'])
    xslt = xslt_fmt.format(ns=py_ext_ns)

    return xslt_transform(xml, xslt, extensions=extensions)

def process(xml, strip_root=True, verbose=True):
    stage1 = None
    print_output_buf = []
    xml_can, _ = canonical(xml)

    def print_input():
        if verbose:
            return '\n'.join((
                info_str(underline('stage 0: input\n')),
                xml,
                '',
            ))
        return xml

    def print_stage1():
        tree = etree.tostring(stage1, pretty_print=True, encoding='unicode')
        if verbose:
            return '\n'.join((
                info_str(underline('stage 1: first pass\n')),
                tree,
                '',
            ))
        return tree

    def print_output():
        out = '\n'.join(print_output_buf)
        if verbose:
            return '\n'.join((
                info_str(underline('stage 2: sent to solr\n')),
                out,
            ))
        return out

    err, stage1 = transform(pre_transform(xml_can, remove_entities=True))
    if err:
        warn("Error with stage 1: %s" % err)
        return err, None, None

    err, out_tree = post_transform(stage1)
    if err:
        warn("Error with stage 2: %s" % err)
        return err, None, None

    err, out_tree = post_transform(stage1)
    if err:
        warn("Error with post transform: %s" % err)
        return err, None, None

    docs = out_tree.findall('/*') if strip_root else [out_tree]

    def tostring(doc):
        print_output_buf.append(
            etree.tostring(doc, pretty_print=True, encoding='unicode')
        )
        return(etree.tostring(doc, encoding='utf-8'))

    def get_type(doc):
        fields = [f for f in doc if f.get('name') == 'type']
        if len(fields) > 1:
            warn('Too many type fields %s' % fields)
            return fields[0]
        if not fields:
            warn('No type field (doc was %s)' % doc)
            return 'error'
        [field_t] = fields
        return field_t.text

    return (
        None,
        [(tostring(_), get_type(_)) for _ in docs],
        (print_input, print_stage1, print_output),
    )
