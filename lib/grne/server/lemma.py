# -*- coding: utf-8 -*-

from sqlite3 import Row

from flask import jsonify
import pydash

from acatpy.io.speak import iwarn, debug

from grne.render import (
    # render_block_with_highlight,
    render_block,
    render_ids,
    render_ids_with_highlight,
    get_id_range,
)
from grne.server.util import request_body_json, make_router, fail
from grne.util.util import is_int, open_db

from config import get as config_get

def check_ints(ns):
    pred = lambda x: not is_int(x)
    if pydash.find(ns, pred) is not None:
        fail(422, imsg='Bad int value(s)')

def process_highlighting_param(highlighting):
    if highlighting is None:
        return {}
    if not isinstance(highlighting, dict):
        fail(422, imsg='Bad dict value(s)')
    ret = {}
    for the_id, spec in highlighting.items():
        terms = spec.get('terms', [])
        abridge = spec.get('abridge', False)
        if not isinstance(terms, list):
            fail(422, imsg='Bad list value(s)')
        try:
            ret[int(the_id)] = {
                'terms': terms,
                'abridge': abridge,
            }
        except ValueError:
            fail(422, imsg='Bad int value(s)')
    return ret

def get_max_lemma_id(db):
    sql = 'select max(id) from main'
    res = db.execute(sql).fetchone()
    return res['max(id)'] - 1

# --- we let the db calls throw to keep the programming model simpler than
# for autocomplete.

def define_lemma_routes(app, app_global, prefix):
    router = make_router(app, prefix, methods=['POST'])

    config = config_get()
    db_path = config.main['web_lemma_db_path']
    db = open_db(db_path, read_only=True, throw=True)
    db.row_factory = Row

    # --- for testing concurrent writes
    # db_test = open_db(db_path, throw=True)

    @router('block-by-id')
    def route_lemma_block_by_id():
        request_body = request_body_json()
        data = request_body['data']
        # @todo consider changing to naam-based query (that's why we have
        # num_before and num_after and not simply a range)
        the_id = data['id']
        num_before = data['num_before']
        num_after = data['num_after']
        do_school = data['do_school']
        only_entries = data.get('only_entries', False)
        do_post_processing = data['do_post_processing']
        # --- [{'id': ..., 'highlighting_terms': ...}, ...]
        highlighting_spec = process_highlighting_param(data['highlighting'])
        if only_entries and highlighting_spec:
            fail(422, imsg='Bad param(s): Highlighting and only_entries must not both be present')
        check_ints((the_id, num_before, num_after))
        err, id_left, id_right = get_id_range(
            db, the_id, num_before, num_after,
            disable_limit=(app_global.env == 'tst' or app_global.env == 'dev'),
        )
        if err:
            fail(422, imsg='Bad param(s): %s' % err)
        debug('id_left %s' % id_left)
        debug('id_right %s' % id_right)
        html_nohi = render_block(
            db, do_school, only_entries,
            id_left, id_right,
        )
        err, html_hi = render_ids_with_highlight(
            db, highlighting_spec, do_school,
            do_post_processing=do_post_processing,
        )
        max_lemma_id = get_max_lemma_id(db)
        html = {**html_nohi, **html_hi}
        if err:
            iwarn(err)
            fail(501, imsg='Unable to render')
        results = {
            'err': None,
            'results': {
                'metadata': {
                    'max_lemma_id': max_lemma_id,
                },
                'results': html,
            },
        }
        return jsonify(results)

    # @todo consider changing to naam-based query.
    @router('by-id')
    def route_lemma_by_id():

        # --- for testing concurrent writes
        # do_write_test(db_path, None)
        # do_write_test(db_path, db_test)

        request_body = request_body_json()
        data = request_body['data']
        ids = data['ids']
        do_school = data['do_school']
        do_post_processing = data['do_post_processing']
        # --- [{'id': ..., 'highlighting_terms': ...}, ...]
        highlighting = process_highlighting_param(data['highlighting'])
        check_ints(ids)
        has_hi = lambda the_id: highlighting.get(the_id) is not None
        ids_hi, ids_nohi = pydash.partition(ids, has_hi)
        html_nohi = render_ids(db, do_school, ids_nohi)
        spec_hi = {}
        for the_id in ids_hi:
            spec = highlighting[the_id]
            spec_hi[the_id] = spec
        err, html_hi = render_ids_with_highlight(
            db, spec_hi, do_school,
            do_post_processing=do_post_processing,
        )
        if err:
            iwarn(err)
            fail(501, imsg='Unable to render')
        html = {**html_nohi, **html_hi}
        results = {
            'err': None,
            'results': html,
        }
        return jsonify(results)

# --- for testing concurrent writes
def do_write_test(db_path, db_test):
    from sqlite3 import OperationalError
    import random
    import time

    def execute_sql(db, sql, bindings):
        try:
            cur = db.execute(sql, bindings)
            return None, cur.fetchall()
        except OperationalError as e:
            return e, None

    db_test = db_test or open_db(db_path, throw=True)

    with db_test:
        (last_id,) = db_test.execute('select max(id) from test').fetchone()
        sql_test = 'insert into test (scratch) values (?)'
        err, res_test = execute_sql(db_test, sql_test, (last_id + 1,))
        sleepy = random.randint(1, 10)
        print('sleeping %s' % sleepy)
        time.sleep(sleepy)
