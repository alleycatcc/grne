# -*- coding: utf-8 -*-

from flask import request, jsonify, make_response, abort

import pydash

from acatpy.colors import underline
from acatpy.io.speak import debug, warn
from acatpy.util import imerge, alias_kw

from config import get as config_get

def request_body_json():
    return _request_body_data(lambda r: r.get_json())

def request_body_form():
    return _request_body_data(lambda r: pydash.get(r, 'form'))

def request_query_params():
    return request.args

def _request_body_data(f):
    data = f(request)
    if data is None:
        fail(
            400,
            imsg='Request is missing a body',
        )
    return data

def reject(p, xs):
    return filter(lambda x: not p(x), xs)

def is_empty(x):
    return x is None or x == ''

def make_router(app, prefix, **kw1):
    def router(stub, **kw2):
        cs = reject(is_empty, [prefix, stub])
        url = '/'.join(cs)
        kw = imerge(kw1, kw2)
        debug('adding flask route → %s' % underline(url))
        return app.route(url, **kw)
    return router

def request_method():
    return request.method

'''
u = a message for the user.
i = a message for the developer, i.e. internal.
our convention is:
  on 5xx errors, don't include a user message.
  on 4xx errors, include a user message only if the user can actually do
  something about it.
'''

@alias_kw(abort_arg='abort')
def fail(code, umsg=None, imsg=None, abort_arg=True):
    res = make_response(
        jsonify(umsg=umsg, imsg=imsg),
        code,
    )
    warn("[ %s ] umsg=%s, imsg=%s" % (code, umsg, imsg))
    if not abort_arg:
        return res
    abort(res)
