# -*- coding: utf-8 -*-

# from functools import namedtuple
from datetime import timedelta
from json import loads as json_decode, dumps as json_encode
from os import getcwd, environ
from os.path import splitext
from sqlite3 import OperationalError
from subprocess import Popen, PIPE
from tempfile import mkstemp

from flask import jsonify, request, send_file

from grne.lemma_to_html.transform import get_xslt as get_lemma_to_html_xslt
from grne.analyse.analyse import analyse_and_convert_query, analyse_chars
from grne.analyse.analyse import autocomplete, xml_to_html
from grne.analyse.analyse import find_broken_links, determine_existing_letters
from grne.analyse.analyse import solr_query, solr_index, solr_index_add
from grne.analyse.analyse import solr_execute_and_process, solr_execute
from grne.analyse.xml_to_html_batch import convert_batch, xml_to_html_batch
from grne.chars import expand_entities
from grne.server.util import request_body_form, request_body_json, make_router, fail
from grne.util.util import ident

from acatpy.io.io import cmd, rm
from acatpy.io.speak import debug
from acatpy.util import make_uuid, pathjoin

from config import get as config_get

class Export(object):
    def convert(self):
        raise Exception('Export: must override `convert`')

class ExportWkPdf(Export):
    def __init__(self):
        self.suffix = 'pdf'

    def convert(self, html, the_basename):
        outfile = pathjoin(getcwd(), '%s.pdf' % the_basename)
        _, tmpfile = mkstemp(suffix='.html')
        with open(tmpfile, 'w') as fh:
            fh.write(html)
        # environ['DISPLAY'] = ':0'
        # --- @todo
        environ['DISPLAY'] = ':1'
        cmd(
            ['wkhtmltopdf', '-O', 'Landscape', tmpfile, outfile],
            throw=True,
        )
        rm(tmpfile, throw=True, verbose=True)

class ExportLibreOffice(Export):
    def __init__(self):
        self.suffix = 'docx'

    def convert(self, html, the_basename):
        html_file = '%s.html' % the_basename
        with open(html_file, 'w') as fh: fh.write(html)
        cmd(['libreoffice', '--headless', '--convert-to', 'odt', html_file], throw=True)
        cmd(['libreoffice', '--headless', '--convert-to', 'docx', '%s.odt' % the_basename], throw=True)

class ExportGrabzit(Export):
    def __init__(self, app_key, app_secret):
        self._app_key = app_key
        self._app_secret = app_secret
        self.suffix = 'docx'

    def _read(self):
        return self._o.stdout.readline()

    def _prepare_input(self, xs):
        return ''.join(x + '\n' for x in xs).encode()

    def convert(self, html, the_basename):
        config = config_get()
        outfile = pathjoin(getcwd(), '%s.docx' % the_basename)
        self._o = Popen(
            config.main['html-export-grabzit-binpath'],
            stdin=PIPE,
            stdout=1,
            stderr=2,
            cwd=config.main['bin_path'],
        )
        self._o.communicate(input=self._prepare_input((
            self._app_key,
            self._app_secret,
            outfile,
            html,
        )))
        self._o = None

def define_analyse_routes(app, app_global, prefix):
    router = make_router(app, prefix, methods=['POST'])
    solr_url = app_global.solr_url
    redis_client_app = app_global.redis_client_app
    # --- for t2 caching
    # @todo batch upload results in all cache misses even after a build.
    # priority is the build, so they can check the school version on the
    # site, and then schoolreductie in batch mode is maybe not so important.
    redis_client_persist = app_global.redis_client_persist
    grabzit_app_key = app_global.grabzit_app_key
    grabzit_app_secret = app_global.grabzit_app_secret

    @router('analyse-chars')
    def route_analyse_analyse_chars():
        request_body = request_body_json()
        data = request_body['data']
        query = data['query']
        analysis, num_non_canonical = analyse_chars(query)

        response_data = {'data': {
            'error': None,
            'results': {
                'analysis': analysis,
                'num_non_canonical': num_non_canonical,
            },
        }}
        return jsonify(response_data)

    @router('analyse-and-convert-query')
    def route_analyse_analyse_and_convert_query():
        request_body = request_body_json()
        data = request_body['data']
        query = data['query']
        orig, converted, mode, pure_spiritus_info, errors, uninorm = analyse_and_convert_query(query, uninorm=True)

        response_data = {'data': {
            'error': errors or None,
            'results': {
                'converted': converted,
                'mode': mode,
                'pure_spiritus_left': pure_spiritus_info.left or [],
                'pure_spiritus_right': pure_spiritus_info.right or [],
                'pure_spiritus_combined': pure_spiritus_info.combined or [],
                'uninorm_nfc': uninorm.nfc,
                'uninorm_nfkc': uninorm.nfkc,
                'uninorm_nfd': uninorm.nfd,
                'uninorm_nfkd': uninorm.nfkd,
            },
        }}
        return jsonify(response_data)

    @router('autocomplete')
    def route_analyse_autocomplete():
        request_body = request_body_json()
        data = request_body['data']
        database = data['database']
        length = data['length']
        query = data['query']
        strictness = data['strictness']
        limit = data['limit']
        errors, results = autocomplete(database, strictness, length, query=query, limit=limit)
        if errors:
            response_data = {'data': {
                'error': errors,
                'results': None,
            }}
        elif not results:
            response_data = {'data': {
                'error': None,
                'results': [],
            }}
        else:
            rows = results['data']
            rows = [dict(_) for _ in rows]
            response_data = {'data': {
                'error': None,
                'results': rows,
            }}
        return jsonify(response_data)

    @router('solr-index-pre-process')
    def route_analyse_solr_index_pre_process():
        request_body = request_body_json()
        data = request_body['data']
        xml = data['xml']
        err, the_input, stage1, output = solr_index(xml)

        response_data = {'data': {
            'error': err,
            'results': {
                'input': the_input(),
                'stage1': stage1(),
                'output': output(),
            },
        }}
        return jsonify(response_data)

    @router('solr-index-add')
    def route_analyse_solr_index_add():
        request_body = request_body_json()
        data = request_body['data']
        xml = data['xml']
        err, _ = solr_index_add(solr_url, xml)
        response_data = {'data': {
            'error': err,
            'results': None,
        }}
        return jsonify(response_data)

    @router('solr-query-process')
    def route_analyse_solr_query_process():
        request_body = request_body_json()
        data = request_body['data']
        query = data['query']
        the_type = data.get('type', 'main')
        if the_type == 'any':
            the_type = None
        err, parsed = solr_query(query, the_type=the_type)
        response_data = {'data': {
            'error': err,
            'results': parsed,
        }}
        return jsonify(response_data)

    # --- other available params are 'fields-main' and 'fields-cit'
    @router('solr-query-execute')
    def route_analyse_solr_query_execute():
        request_body = request_body_json()
        data = request_body['data']
        query = data['query']
        raw = data['raw']
        num_rows = data['num-rows']
        start_row = data['start-row']

        debug('raw %s' % raw)

        if raw:
            errors, metadata, results = solr_execute(
                solr_url, query,
                highlight_field=None,
                start_row=start_row,
                num_rows=num_rows,
            )
            results_process = lambda results: (
                [{'highlighting': str(results.highlighting)}] +
                [dict(_) for _ in results.docs]
            )
        else:
            errors, metadata, results = solr_execute_and_process(solr_url, query, num_rows, start_row)
            results_process = ident

        if errors:
            response_data = {'data': {
                'error': str(errors),
                'results': None,
            }}
        elif not results:
            response_data = {'data': {
                'error': None,
                'results': {
                    'results': [],
                    'metadata': metadata,
                }
            }}
        else:
            response_data = {'data': {
                'error': None,
                'metadata': metadata,
                'results': {
                    'results': results_process(results),
                    'metadata': metadata,
                }
            }}
        return jsonify(response_data)

    # @deprecated
    @router('search-highlighting')
    def route_analyse_search_highlighting():
        request_body = request_body_json()
        data = request_body['data']
        lemma_xml = data['lemma-xml']
        terms = data['terms']
        terms_array = [_.strip() for _ in terms.split('|')]

        lemma_to_html_xslt = get_lemma_to_html_xslt()
        err, html_normal, _, _, _ = xml_to_html(lemma_xml, lemma_to_html_xslt, 'none')

        errors, results = search_highlighting(html_normal, terms_array) # noqa

        if errors:
            response_data = {'data': {
                'error': str(errors),
                'results': None,
            }}
        else:
            response_data = {'data': {
                'error': None,
                'results': results,
            }}
        return jsonify(response_data)

    @router('xml-to-html/xslt', methods=['GET'])
    def route_analyse_xml_to_html_get_xslt():
        response_data = {'data': {'results': get_lemma_to_html_xslt()}}
        return jsonify(response_data)

    reductie_modus_table = {
        'none': 'none',
        'beknopt': 'none',
        'school-standaard': 'normal',
        'school-auto': 'dev-auto',
        'school-t1-t2': 'dev-t1-t2',
        'school-t2': 'dev-t2',
    }

    # for testing event stream
    '''
    import flask.Response
    import time
    def event_stream():
        while True:
            time.sleep(1)
            yield 'event: oranges\n' + 'data: ping!\n\n'

    @router('stream', methods=['GET'])
    def route_stream():
        print('connected to stream')
        return Response(
            event_stream(),
            headers={
                # --- crucial for SSE to work with nginx.
                'X-Accel-Buffering': 'no',
                # --- not crucial, maybe nice.
                'Cache-Control': 'no-cache',
            },
            mimetype='text/event-stream',
        )
    '''

    @router('xml-to-html')
    def route_analyse_xml_to_html():
        request_body = request_body_form()
        zip_file = request.files.get('lemma-xml-batch', None)
        lemma_xml = request_body.get('lemma-xml', None)
        if zip_file is not None:
            if lemma_xml:
                fail(422, umsg="Need either batch upload or single lemma (got both).")
            batch_mode = True
        else:
            if not lemma_xml:
                fail(422, umsg="Need either batch upload or single lemma (got neither).")
            batch_mode = False

        # --- this is a string, because it's form data.
        make_highlight_snippet = request_body['make_highlight_snippet'] == 'true'
        # --- @todo disable extensions?
        xslt = request_body['xslt']
        reductie_modus = request_body['reductie_modus']
        terms = request_body.get('terms')
        terms = (terms or '').strip()
        terms_array = [] if terms == '' else [_.strip() for _ in terms.split('|')]
        the_reductie_modus = reductie_modus_table.get(
            reductie_modus,
            None,
        )
        if the_reductie_modus is None:
            fail(422, imsg='Invalid reductie_modus: %s' % reductie_modus)

        transform = lambda xml: xml_to_html(
            # --- expand_entities is to keep it consistent with build_site
            # and increase the chance of a cache hit.
            # (see `read_xml_from_file`).
            expand_entities(xml.strip()),
            xslt, the_reductie_modus,
            make_highlight_snippet=make_highlight_snippet,
            search_terms=terms_array,
            redis_client_persist=redis_client_persist,
        )

        if batch_mode:
            results = []
            for result in xml_to_html_batch(transform, zip_file):
                this_err, xml_filename, lemma_xml, html_normal, html_school, t1_gecontroleerd = result
                results.append({
                    'filepath': xml_filename,
                    'this_error': this_err,
                    'xml': lemma_xml,
                    'xslt': xslt,
                    'html_normal': html_normal,
                    'html_school': html_school,
                    'xml_school': None,
                    't1_gecontroleerd': t1_gecontroleerd,
                })
            redis_key, batch_id = make_uuid_for_batch(redis_client_app)
            response_results = {
                'is_batch': batch_mode,
                'batch_id': batch_id,
                'reductie_modus': reductie_modus,
                # --- can technically be empty but unlikely / impossible.
                'upload_filename': zip_file.filename or 'words.zip',
                'results': results,
            }
            redis_client_app.set(redis_key, json_encode(response_results))
            redis_client_app.expire(redis_key, timedelta(hours=1))
            # --- save bandwidth -- they're not needed here.
            response_results['results'] = None
            response_data = {
                'data': {
                    'error': None,
                    'results': response_results,
                },
            }
        # --- not batch mode
        else:
            err, html_normal, html_school, xml_school, t1_gecontroleerd = transform(lemma_xml)
            results = [{
                'filepath': None,
                'this_error': None,
                'xml': lemma_xml,
                'xslt': xslt,
                'html_normal': html_normal,
                'html_school': html_school,
                'xml_school': xml_school,
                't1_gecontroleerd': t1_gecontroleerd,
            }]
            response_data = {
                'data': {
                    'error': err,
                    'results': {
                        'is_batch': batch_mode,
                        'reductie_modus': reductie_modus,
                        'results': results,
                    },
                },
            }
        return jsonify(response_data)

    @router('xml-to-html-docx-libreoffice/<batch_id>', methods=['GET'])
    def route_analyse_xml_to_html_docx_libreoffice(batch_id=None):
        return _analyse_xml_to_html_convert('libreoffice', batch_id)

    @router('xml-to-html-docx-grabzit/<batch_id>', methods=['GET'])
    def route_analyse_xml_to_html_docx_grabzit(batch_id=None):
        return _analyse_xml_to_html_convert('grabzit', batch_id)

    @router('xml-to-html-pdf/<batch_id>', methods=['GET'])
    def route_analyse_xml_to_html_pdf(batch_id=None):
        return _analyse_xml_to_html_convert('wkpdf', batch_id)

    def _analyse_xml_to_html_convert(variant_s, batch_id):
        if variant_s == 'libreoffice':
            variant = ExportLibreOffice()
        elif variant_s == 'grabzit':
            variant = ExportGrabzit(grabzit_app_key, grabzit_app_secret)
        elif variant_s == 'wkpdf':
            variant = ExportWkPdf()
        else:
            raise Exception('Invalid variant: %s' % variant_s)
        redis_key = key_for_batch_id(batch_id)
        cached_json = redis_client_app.get(redis_key)
        if cached_json is None: fail(
            404,
            umsg='Expired/invalid results.',
            imsg='_analyse_xml_to_html_convert: Unable to get cached results (key=%s)' % redis_key,
        )
        cached_results = json_decode(cached_json.decode())

        # --- @throws
        # --- file will be deleted by a timer.
        file_to_send = convert_batch(variant, cached_results)

        upload_filename_base, _ = splitext(cached_results['upload_filename'])
        download_name = '%s.%s' % (upload_filename_base, variant.suffix)

        return send_file(
            file_to_send,
            as_attachment=True,
            download_name=download_name,
        )

    @router('xml-to-html-cached/<batch_id>', methods=['GET'])
    def route_analyse_xml_to_html_print(batch_id=None):
        redis_key = key_for_batch_id(batch_id)
        cached_json = redis_client_app.get(redis_key)
        if cached_json is None: fail(
            404,
            umsg='Expired/invalid results.',
            imsg='route_analyse_xml_to_html_print: Unable to get cached results (key=%s)' % redis_key,
        )
        cached = json_decode(cached_json.decode())
        return jsonify({
            'data': {
                'results': cached,
            },
        })

    @router('find-broken-links', methods=['POST'])
    def route_analyse_find_broken_links():
        request_body = request_body_json()
        data = request_body['data']
        only_existing = data['only-existing-letters']
        try:
            err, results = find_broken_links(only_existing)
        except OperationalError as e:
            err, results = e, None
        if err:
            fail(501, imsg='find_broken_links: %s' % err)
        response_data = {'data': {
            'error': None,
            'results': results,
        }}
        return jsonify(response_data)

    @router('determine-existing-letters', methods=['GET'])
    def route_determine_existing_letters():
        # if has_params...
        err, results = determine_existing_letters()
        if err:
            fail(501, imsg='determine_existing_letters: %s' % err)
        response_data = {'data': {
            'error': None,
            'results': results,
        }}
        return jsonify(response_data)

    def key_for_batch_id(batch_id):
        return 'analyse:batch:%s' % batch_id

    def make_uuid_for_batch(redis_client):
        i = 0
        redis_key = None
        while i < 5:
            batch_id = make_uuid()
            redis_key = key_for_batch_id(batch_id)
            if not redis_client.exists(redis_key):
                return redis_key, batch_id
        raise Exception('make_key_for_batch: failed to create unique UUID')
