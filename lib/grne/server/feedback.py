# -*- coding: utf-8 -*-

from flask import jsonify

from flask_mail import Message, Mail
import pydash

from acatpy.io.speak import warn, debug

from grne.server.util import request_body_json, make_router, fail

def define_feedback_routes(app, app_global, prefix, can_send_feedback=None):
    if not can_send_feedback:
        return warn("Feedback routes disabled")
    mail = Mail()
    print('defining routes for prefix %s' % prefix)
    router = make_router(app, prefix, methods=['POST'])
    smtp_username = app_global.smtp_username
    smtp_password = app_global.smtp_password
    smtp_host = app_global.smtp_host
    smtp_port = app_global.smtp_port
    feedback_recipients = app_global.feedback_recipients
    app.config["MAIL_SERVER"] = smtp_host
    app.config["MAIL_PORT"] = smtp_port
    app.config["MAIL_USE_SSL"] = True
    app.config["MAIL_USERNAME"] = smtp_username
    app.config["MAIL_PASSWORD"] = smtp_password
    mail.init_app(app)

    debug('define_feedback_routes: smtp_username=%s' % smtp_username)
    debug('define_feedback_routes: password=%s' % 'password not shown')
    debug('define_feedback_routes: smtp_host=%s' % smtp_host)
    debug('define_feedback_routes: smtp_port=%s' % smtp_port)
    debug('define_feedback_routes: feedback_recipients=%s' % feedback_recipients)

    @router('mail')
    def route_feedback_mail_post():
        request_body = request_body_json()
        message = pydash.get(request_body, 'data.message')
        name = pydash.get(request_body, 'data.name')
        email = pydash.get(request_body, 'data.email')
        if not message or not name or not email:
            fail(422, imsg='Bad param(s)')

        msg = Message(
            'GrNe feedback',
            sender=smtp_username,
            reply_to=email,
            recipients=feedback_recipients,
        )
        msg.body = """
        Er is een feedback bericht van GrNe.
        Verzonden door: %s <%s>

        %s
        """ % (name, email, message)
        try:
            mail.send(msg)
        except Exception as e:
            warn('could not send email: %s' % e)
            results = {
                'error': 'could not send email',
                'ok': False,
            }
            return jsonify(results)

        results = {
            'error': None,
            'ok': True,
        }
        return jsonify(results)
