# -*- coding: utf-8 -*-

from functools import partial
import re

from pydash import uniq

from config import get as config_get

# --- @todo move this
from grne.build.make_index import id4naam
from grne.util.util import open_db

from acatpy.io.speak import error, warn, debug

# --- @todo do we still need this?
known_bad = {
    'εὐθύς',
    'εἴσω',
}

def extract_search_terms(start_token, stop_token, string):
    # searching for 'stuurde de processie'
    # you get 'hij __START__stuurde__STOP__ __START__de__STOP__ __START__processie__STOP__ uit
    # you want to merge 'adjacent' __STOP__ / __START__ combinations, but
    # defining what is adjacent is hard.
    # here we define it as having only spaces between.
    # change here if more unusual phrases aren't matching.
    # say you have 'stuurde? De processie'
    # and they search for "stuurde de processie"
    # maar weet je wat? Er moet een matras gehaald worden
    # only spaces: 'maar weet je wat', 'Er moet', not the end of the world
    # rx_adjacent_filler = r'\s*'
    # \W: 'maar weet je wat? Er moet', better: chance of taking too much?
    rx_adjacent_filler = r'\W*'
    rx_adjacent = r'%s(%s)%s' % (stop_token, rx_adjacent_filler, start_token)
    subf = lambda match: match.groups()[0]
    debug('string before %s' % string)
    string = re.sub(rx_adjacent, subf, string)
    debug('string after %s' % string)

    rx = r'%s(.+?)%s' % (start_token, stop_token)
    debug('rx %s' % rx)
    debug('re.findall(rx, string) %s' % re.findall(rx, string))
    return re.findall(rx, string)

def prepare_solr_result(highlight_field, start_token, stop_token, highlighting, doc):
    config = config_get()
    conf = config.main
    db_path, fields_wanted = conf['web_lemma_db_path'], conf['solr_fields_to_return']
    err, db = open_db(db_path)
    doc_id = doc['id']
    highlight_entry = highlighting.get(doc_id)
    search_terms = []
    if highlight_entry is None:
        warn("Error fetching term info, this shouldn't happen")
    else:
        debug('highlight_entry %s' % highlight_entry)
        terms = highlight_entry.get(highlight_field)
        if terms is None:
            warn("Error fetching term info, this shouldn't happen")
        else:
            extract = partial(extract_search_terms, start_token, stop_token)
            search_terms = [y for x in terms for y in extract(x)]
    # --- @todo
    # if err ...
    naam_full = doc.get('id-naam', None)
    if naam_full is None:
        warn("Error getting naam; perhaps this hit was a citaat?")
        return 'Error getting naam; perhaps the results include a citaat?', None
    if naam_full in known_bad:
        the_id = -1
    else:
        err, the_id = id4naam(db, naam_full)
    if err:
        err_str = "Error fetching id for naam_full %s: %s" % (naam_full, err)
        error(err_str)
        return err_str, None
    the_dict = {k: doc[k] for k in fields_wanted}
    the_dict['link-id'] = the_id - 1
    the_dict['search-highlight'] = uniq(search_terms)
    return None, the_dict
