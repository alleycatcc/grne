# -*- coding: utf-8 -*-

import re

from flask import jsonify

from acatpy.io.speak import iwarn

# @todo move this
from grne.analyse.analyse import solr_query, solr_execute_and_process
from grne.server.util import request_body_json, make_router, fail

from config import get as config_get

def define_advanced_search_routes(app, app_global, prefix):
    router = make_router(app, prefix, methods=['POST'])
    solr_url = app_global.solr_url

    config = config_get()

    @router('search')
    def route_advanced_search_search():
        request_body = request_body_json()
        data = request_body['data']
        query = data['query']
        num_rows = data['num_rows']
        start_row = data['start_row']

        # --- disallow a single star by itself (a star which is not preceded
        # and not followed by a word character), because it will parse
        # correctly but will cause an error on solr.
        # --- if there are more (or more complicated) patterns then we need
        # a better place for these.
        # --- keep in mind that query here is something like e.g.
        #     '$ "\"abc def *\""'

        if re.search(r'(?<!\w)\*(?!\w)', query):
            fail(422, umsg='Het *-teken mag alleen als deel van een woord gebruikt worden.')

        errors, parsed = solr_query(query, the_type='main')
        if errors:
            fail(
                422,
                umsg='Invalid query.',
                imsg='Unable to parse query %s: %s' % (query, str(errors)),
            )

        errors, metadata, the_results = solr_execute_and_process(solr_url, parsed, num_rows, start_row)

        # --- we consider this a server error and abort with 501.
        if errors:
            # @todo ierror
            iwarn('Error with query=%s, parsed=%s: %s' % (query, parsed, str(errors)))
            fail(501, imsg='Unable to execute query=%s, parsed=%s' % (query, parsed))

        response_data = {
            'data': {
                'metadata': metadata,
                'results': the_results,
            }
        }
        return jsonify(response_data)
