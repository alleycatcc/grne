# -*- coding: utf-8 -*-

from functools import partial
import json

from flask import jsonify

from acatpy.io.speak import iwarn

from grne.autocomplete import autocomplete_lemma, autocomplete_pars, autocomplete_parsget
from grne.server.util import request_body_form, make_router, request_method, request_query_params, fail
from grne.util.util_redis import redis_cache

def define_autocomplete_routes(app, app_global, prefix):
    router = make_router(app, prefix, methods=['POST', 'GET'])

    redis_client_app = app_global.redis_client_app

    @router('')
    def route_autocomplete_main_post():
        method = request_method()
        if method == 'GET':
            data = request_query_params()
        elif method == 'POST':
            request_body = request_body_form()
            data = request_body
        else:
            raise Exception(method)

        query = data['query']
        mode = data['mode']
        limit = data.get('limit')

        # --- caching at this level is actually not necessary, but it is a
        # good test: there's a huge speed increase over the non-cached
        # version, but nginx caching kicks in at a higher level than this
        # and it provides an even bigger speed increase.

        cache = partial(redis_cache,
                        serialise_result=json.dumps,
                        # ------ the json module from python 3.7 can decode
                        # bytes; for 3.5 we need to explicitly decode.
                        # --- 3.7
                        # deserialise_hit=json.loads)
                        # --- 3.5
                        deserialise_hit=lambda x: json.loads(x.decode()))

        if mode == 'lemma':
            res = cache(redis_client_app, 'ac:lemma', autocomplete_lemma,
                        (query, limit), key_extra=[query, limit])
        elif mode == 'pars':
            res = cache(redis_client_app, 'ac:pars', autocomplete_pars,
                        (query, limit), key_extra=[query, limit])
        elif mode == 'parsget':
            res = cache(redis_client_app, 'ac:parsget', autocomplete_parsget,
                        (query,), key_extra=[query])
        input_error, search_error = res['error']
        results = res['results']
        if search_error:
            if results:
                iwarn(search_error)
            else:
                # @todo ierror better
                iwarn(search_error)
                # --- for autocomplete there's no useful way to show an
                # error to the user, hence no umsg.
                fail(
                    500,
                    imsg='Error performing search',
                )
        if input_error:
            # --- ignore, return no results
            results = []
        return jsonify(results)
