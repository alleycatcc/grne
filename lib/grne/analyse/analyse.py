# -*- coding: utf-8 -*-

from functools import namedtuple, partial
import html
from os.path import dirname, basename
import re
from sqlite3 import Row, connect
import unicodedata
from urllib.parse import urlparse as url_parse

from lxml import etree
import pydash
from pydash import memoize, uniq

from grne.build.make_index import get_words_filenames
from grne.build.make_site import transform_lemma_to_html_from_string
from grne.chars import canonical, analyse_and_convert_query as chars_analyse_and_convert_query
from grne.chars import strip_markings, regex_greek
from grne.query_parser.parse import get_parser
from grne.server.lib import prepare_solr_result
from grne.solr.admin import with_config as solr_with_config
from grne.solr_pre_index import process as solr_pre_index_process
from grne.util.util import open_db

from acatpy.io.speak import debug, warn
from acatpy.solr_search import init as solr_search_init

from config import get as config_get

Uninorm = namedtuple('Uninorm', ['nfc', 'nfkc', 'nfd', 'nfkd'])

debug_sql = False

def has_table(db, table):
    sql = 'select name from sqlite_master where type="table" and name="%s"' % table
    cur = db.execute(sql)
    return cur.fetchone() is not None

# --- throws sqlite3.OperationalError on bad open
def autocomplete(database, strictness, n, query=None, limit=None):
    config = config_get()

    dispatch = {
        'lemma': (config.main['web_lemma_db_path'], _get_ac_spec_lemma),
        'pars': (config.main['web_pars_db_path'], _get_ac_spec_pars),
    }

    if strictness not in ['loose', 'strict']:
        return 'invalid strictness', None
    if database not in dispatch:
        return 'invalid database', None

    db_path, get_spec = dispatch[database]
    uri = 'file:%s?mode=ro' % db_path
    db = connect(uri, uri=True)
    db.row_factory = Row
    table, sql, bindings = get_spec(strictness, n, query=query, limit=limit)
    if debug_sql:
        print('sql %s, bindings %s' % (sql, bindings))
    has = has_table(db, table)
    if not has:
        return 'invalid prefix length', None
    cur = db.execute(sql, bindings)
    data = cur.fetchall()
    metadata = {
        'maxwidth': get_maxwidth(data),
    }
    results = {
        'metadata': metadata,
        'data': data,
    }
    return None, results

def analyse_chars(s):
    def name(c):
        try:
            return unicodedata.name(c)
        except ValueError:
            return '✘'

    def analyse_char(c):
        return {
            'input': c,
            'ord': ord(c),
            'name': name(c),
        }

    _, num_non_canonical = canonical(s)
    return [analyse_char(_) for _ in s], num_non_canonical

def analyse_and_convert_query(s, uninorm=False):
    converted, mode, pure_spiritus_info, errors = chars_analyse_and_convert_query(s)
    ret = [s, converted, mode, pure_spiritus_info, errors]
    if uninorm:
        return (*ret, unicode_normalise(s))
    return ret

def get_maxwidth(data):
    widths = {}

    # --- does not normalise input.
    def get_width(fld, txt):
        width = pydash.get(widths, fld) or 0
        return max(width, len(str(txt)))

    for row in data:
        for fld, txt in dict(row).items():
            widths[fld] = get_width(fld, txt)
    return widths

def _get_ac_spec_lemma(strictness, n, limit=None, query=None):
    if query:
        where = ['ac.pref = ?']
        bindings = (query,)
        where_s = 'where %s' % (' and '.join(where))
    else:
        bindings = ()
        where_s = ''
    table = 'autocomplete_%s_%s' % (strictness, n)
    cols = (
        'ac.pref', 'f.token',
        'm.naam', 'm.lemma',
        'm.homonym_nr', 'f.spiritus_left', 'f.spiritus_right',
        'm.filename',
    )
    table_sel = '%s as ac' % table
    join = [
        'forms_%s as f' % strictness,
        'main as m',
    ]
    on = [
        'ac.forms_id = f.id',
        'f.main_id = m.id',
    ]
    cols_s = ', '.join(cols)
    join_s = ', '.join(join)
    on_s = ' and '.join(on)
    limit_s = 'limit %s' % limit if limit else ''
    sql = 'select %s from %s join %s on %s %s %s' % (cols_s, table_sel, join_s, on_s, where_s, limit_s)
    print('sql %s' % sql)
    return table, sql, bindings

def _get_ac_spec_pars(strictness, n, limit=None, query=None):
    table = 'autocomplete_%s_%s' % (strictness, n)
    if query:
        where = ['ac.pref = ?']
        bindings = (query,)
        where_s = 'where %s' % (' and '.join(where))
    else:
        # where = []
        bindings = ()
        where_s = ''

    cols = ('ac.pref', 'm.token', 'm.lemma', 'm.code')
    table_sel = '%s as ac' % table
    table_main = 'main as m'
    on = ['ac.main_id = m.id']
    cols_s = ', '.join(cols)
    on_s = ', '.join(on)
    limit_s = 'limit %s' % limit if limit else ''
    sql = 'select %s from %s join %s on %s %s %s' % (cols_s, table_sel, table_main, on_s, where_s, limit_s)
    return table, sql, bindings

def solr_index(xml_in, verbose=False):
    err, _, (print_input, print_stage1, print_output) = solr_pre_index_process(xml_in, verbose=verbose)
    return err, print_input, print_stage1, print_output

def solr_index_add(url, the_input):
    parsed = url_parse(url)
    scheme = parsed.scheme
    host = parsed.hostname
    port = parsed.port or (443 if scheme == 'https' else 80)

    solr = solr_with_config(scheme=scheme, host=host, port=port)
    return solr.add_doc_as_xml(the_input, die=False)

# @todo, move these functions.

# --- use this to pass a p-expr to the parser.
# --- the $ at the beginning is optional.
# --- note that this step includes the character transforms.
# --- p_all_field is the default field which is used if the % clause is
# missing.

def solr_query(query, p_all_field=None, the_type=None):
    config = config_get()
    p_all_field = p_all_field or config.main['solr_fulltext_field']
    if not re.match(r'^\s*\$\s+', query):
        query = '$ ' + query
    has_errors, parsed_a, *_ = get_parser(query, p_all_field=p_all_field)()
    if has_errors:
        return 'error with p-expr', None
    if the_type:
        expr_a = '(and %s (type %s))' % (parsed_a, the_type)
    else:
        expr_a = parsed_a
    debug('expr_a %s' % expr_a)
    debug('parsed_a %s' % parsed_a)
    has_errors_s, text_s, *_ = get_parser(expr_a)()
    if has_errors_s:
        return 'error with a-expr', None
    return None, text_s

# --- the way this is currently programmed, is that e.g. a vertM search on
# 'water' will return only hits with 'water' in the vertM of course, but
# will then highlight 'water' everywhere, not only in vertM.
# --- this is actually kind of nice, but if Leiden decides they don't want
# this, then change the rendering endpoints (e.g. 'by-id'), not here.

def solr_execute(url, query, highlight_field=None, start_row=None, num_rows=None, solr_params=None):
    solr_params = solr_params or {}

    config = config_get()
    start_token, stop_token = config.main['solr_highlight_markers']

    searcher = solr_search_init(url)
    if start_row is not None:
        solr_params['start'] = start_row
    if num_rows is not None:
        solr_params['rows'] = num_rows
    solr_params['sort'] = 'hoofdW-sort asc'
    debug('query %s' % query)
    if highlight_field:
        solr_params = {**solr_params, **{
            'hl': 'on',
            # --- the field(s) which should be marked with __START__ and
            # __STOP__ -- this should match the field that was searched on.
            # --- we use this to know what the exact search hit was.
            # --- it is still possible to do e.g. a fulltext highlight on a
            # vertM search: set hl.fl here to vertM-combined, get the string,
            # and then pass it to the highlighting/rendering functions.
            'hl.fl': highlight_field,
            'hl.simple.pre': start_token,
            'hl.simple.post': stop_token,
            # --- maximum number of snippets per field, i.e., the number of
            # times the search string occurs per field; 20 should be more
            # than safe.
            'hl.snippets': 20,
            # -- solr 9 uses unified as default method; we need this to have
            # the same behaviour as solr 7.
            'hl.method': 'original',
        }}
    err, results = searcher.search(query, **solr_params)
    if err:
        return err, None, None
    metadata = {
        'num_hits': results.hits,
    }
    return None, metadata, results

# --- @todo needs better place.
def solr_execute_and_process(solr_url, query, num_rows, start_row, highlight_field=None):
    config = config_get()

    # --- we are confident that the app expects a fulltext search.
    # --- should this ever change, the parser could be modified to
    # return the field in the % clause, and that could be passed via the kw
    # arg.
    highlight_field = highlight_field or config.main['solr_fulltext_field']

    start_token, stop_token = config.main['solr_highlight_markers']
    prepare_result = partial(prepare_solr_result, highlight_field, start_token, stop_token)

    errors, metadata, results = solr_execute(
        solr_url, query,
        highlight_field=highlight_field,
        num_rows=num_rows,
        start_row=start_row,
    )
    if errors:
        return errors, None, None

    # --- we use solr's 'highlighting' feature -- not to be confused
    # with the highlighting we implement through xslt -- to get the
    # terms and phrases that were actually matched.
    highlighting = results.highlighting

    debug('highlighting %s' % highlighting)

    the_results = []
    if results:
        for doc in results.docs:
            err, prepared = prepare_result(highlighting, doc)
            if err:
                return err, None, None
            the_results.append(prepared)

    return None, metadata, the_results

def xml_to_html(xml_in, xslt, reductie_modus,
                make_highlight_snippet=None,
                search_terms=None, redis_client_persist=None):
    try:
        err, html_normal, html_school, xml_school, t1_gecontroleerd = transform_lemma_to_html_from_string(
            xml_in, reductie_modus,
            xslt=xslt,
            make_snippet=make_highlight_snippet,
            search_terms=search_terms,
            redis_client_persist=redis_client_persist,
        )
    except Exception as e:
        warn('xml_to_html: got exception: %s' % e)
        err = str(e)
    if err:
        return err, None, None, None, None
    return None, html_normal, html_school, xml_school, t1_gecontroleerd

def determine_existing_letters():
    files, errors, _empty, _num_ignored, _num_unreadable = get_words_filenames()
    if errors:
        return errors, None
    get_dir = lambda file: dirname(dirname(file))
    get_base = lambda file: basename(file)
    dirs = [get_dir(_) for _ in files]
    dirs = uniq(dirs)
    return None, sorted([get_base(_) for _ in dirs])

# --- note: will die if the letter is not recognised.
def _letters_for_names_(names):
    config = config_get()
    letter_map = config.main['letter_map']
    return {letter_map[_] for _ in names}

_letters_for_names = memoize(_letters_for_names_)

def letter_ok(letter, existing_names):
    existing_letters = _letters_for_names(existing_names)
    return letter in existing_letters

def _find_broken(existing_letters, broken):
    results = []
    for the_broken in broken:
        unresolved = the_broken.attrib['data-unresolved']
        unresolved = html.unescape(unresolved)
        if existing_letters is not None:
            first_letter, _ = strip_markings(unresolved[0], False)
            first_letter = first_letter.lower()
            if all((
                not letter_ok(first_letter, existing_letters),
                contains_only_greek(first_letter),
                # --- filter ᾿ and · and possibly others.
                re.match(r'^\w', first_letter),
            )):
                continue
        results.append(unresolved)
    return results

def find_broken_links(only_existing):
    config = config_get()
    if only_existing:
        err, existing_letters = determine_existing_letters()
        if err:
            return err, None
    else:
        existing_letters = None
    db_path = config.main['web_lemma_db_path']
    db = open_db(db_path, read_only=True, throw=True)
    sql = 'select m.id, m.naam, m.homonym_nr, r.html from main as m join render as r on m.id = r.main_id'
    results = None
    cur = db.execute(sql)
    rows = cur.fetchall()
    results = []
    for row in rows:
        main_id, naam, homonym_nr, html = row
        parsed = etree.fromstring(html)

        # --- UL wants to know which links occur in a kruisVerw (which often
        # don't exist and therefore represent 'noise' in the output) and
        # which ones don't.

        # --- we use .xpath instead of .findall because .findall doesn't
        # seem to support not-predicates on attributes.

        is_kruisverw = True
        broken = parsed.xpath('//*[@class="kruisVerw"]/*[@class="broken-link"]')
        unresolveds = _find_broken(existing_letters, broken)
        for unresolved in unresolveds:
            results.append({
                'naam': naam,
                'homonym_nr': homonym_nr,
                'unresolved': unresolved,
                'link_id': main_id - 1,
                'is_kruisverw': is_kruisverw,
            })

        is_kruisverw = False
        broken = parsed.xpath('//*[@class!="kruisVerw"]/*[@class="broken-link"]')
        unresolveds = _find_broken(existing_letters, broken)
        for unresolved in unresolveds:
            results.append({
                'naam': naam,
                'homonym_nr': homonym_nr,
                'unresolved': unresolved,
                'link_id': main_id - 1,
                'is_kruisverw': is_kruisverw,
            })

    return None, results

def contains_only_greek(x):
    return re.match(r'^%s+$' % regex_greek(), x)

def contains_non_greek(x):
    return not contains_only_greek(x)

def to_canonical(s):
    return canonical(s)

def is_new_greek(c):
    o = ord(c)
    return o >= 0x0370 and o <= 0x03FF

def unicode_normalise(s):
    norm = [unicodedata.normalize(_, s) for _ in ('NFC', 'NFKC', 'NFD', 'NFKD')]
    return Uninorm(*norm)
