# -*- coding: utf-8 -*-

from os import chdir, getcwd
import re
from subprocess import Popen, PIPE
from tempfile import mkstemp, mkdtemp
from threading import Timer
from zipfile import ZipFile, BadZipFile

from acatpy.io.io import rm, rmd
from acatpy.util import pathjoin

from grne.server.util import fail

from config import get as config_get

def xml_to_html_batch(transform, zip_file):
    results = []
    _, tmpfile = mkstemp()
    # --- @todo find a way to stream the zip data instead of saving a temp
    # file.
    zip_file.save(tmpfile)
    try:
        with ZipFile(tmpfile, 'r') as the_zip:
            for zipinfo in the_zip.filelist:
                if zipinfo_is_dir(zipinfo):
                    continue
                filename = zipinfo.filename
                with the_zip.open(zipinfo) as fh:
                    lemma_xml = fh.read().decode()
                    transform_result = transform(lemma_xml)
                    err, html_normal, html_school, _, t1_gecontroleerd = transform_result
                    results.append((err, filename, lemma_xml, html_normal, html_school, t1_gecontroleerd))
    except BadZipFile as e:
        zipfilename = zip_file.filename
        fail(
            422,
            umsg="Unable to unzip file “%s”" % zipfilename,
            imsg="Unable to unzip '%s': %s" % (zipfilename, str(e)),
        )
    rm(tmpfile, throw=True, verbose=True)
    return results

def zipinfo_is_dir(zipinfo):
    # --- @todo use this when we can upgrade (it's available on
    # zipfile>=3.6)
    # return zipinfo.is_dir()

    # --- after some trial & error and stackoverflow.
    S_IFDIR = 0o40000
    return zipinfo.external_attr >> 16 & S_IFDIR == S_IFDIR

def do_conversion(variant, folder, the_basename, html):
    save_cwd = getcwd()
    chdir(folder)
    variant.convert(html, the_basename)
    chdir(save_cwd)

def _convert_batch(variant, transform_results):
    config = config_get()

    reductie_modus = transform_results['reductie_modus']
    results = transform_results['results']

    o = Popen(
        config.main['html-export-prepare-binpath'],
        stdin=PIPE,
        stdout=PIPE,
        cwd=config.main['bin_path'],
    )

    write = lambda x: o.stdin.write((x + '\n').encode())
    flush = lambda: o.stdin.flush()
    write_marker = lambda: write('=====')
    read = lambda: o.stdout.readline().decode()
    is_marker = lambda x: x == '=====\n'

    scss = get_scss()
    write(scss)
    write_marker()
    flush()

    htmls = []
    for result in results:
        this_error = result['this_error']
        # filepath = result['filepath']
        html_normal = result['html_normal']
        html_school = result['html_school']
        if this_error: continue
        # --- @todo this is unnecessarily complicated, and this code is
        # repeated, also on the front-end -- we really ought to just have
        # one result from the transformation (not both school and normal) and
        # not have to check reductie_modus again.
        if reductie_modus in (
            'school-standaard',
            'school-auto',
            'school-t1-t2',
            'school-t2',
        ):
            html = html_school
        else:
            html = html_normal
        htmls.append(html)

    joiner = '''
        <p style='height: 30px'></p>
    '''
    html_all = joiner.join(htmls)
    # --- remove processing instructions
    # --- @todo these really shouldn't still be around though.
    html_all = re.sub(r'<\?.+?\?>', '', html_all)
    write(html_all)
    write_marker()
    flush()

    html_transformed = []
    while True:
        line = read()
        if is_marker(line):
            break
        else:
            html_transformed.append(line)

    folder = mkdtemp()
    base = 'output'
    do_conversion(variant, folder, base, ''.join(html_transformed))
    return pathjoin(folder, '%s.%s' % (base, variant.suffix)), folder

def convert_batch(variant, transform_results):
    unlink = lambda x: rmd(x, throw=True, verbose=True)
    output_file, tmp_folder = _convert_batch(variant, transform_results)
    # --- an exception in the detached thread is ok, doesn't kill the server.
    Timer(600, unlink, (tmp_folder,)).start()
    return output_file

# --- this is repeated from the Lemma component in the frontend, which is a
# shame.
# --- it's only used for exporting to docx and it's not a huge tragedy if
# there are small differences.

def get_scss():
    return '''
      /* this helps in the conversion to .docx -- seems we need to offset
      the negative margins and indents */
      & { margin-left: 200px; }
      font-family: Source Sans Pro;

      width: 100%;
      display: inline-block;
      text-indent: -20px;
      padding-left: 30px;
      padding-right: 15px;
      font-size: 14px;
      line-height: 21px;
      opacity: 1.0;

      .hoofdW {
        font-weight: bold;
        color: #139;
      }

      .vorm, .xlBet, .bet, .escBet, .cit, .citIsgel, .isgel, .isgelCit, .citG, .citNV, .vertM, .gebrW,
      .morfI, .etym, .toel, .hulpI, .werkV, .hoofdW, .verw, .aut, .werk, .plaats, .kruisVerw, .tmp, .i,
      .r, .s, .b, .link {
        /*
        display: inline-block;
        */
        display: inline;
      }

      .vertM, .werkV {
        font-weight: bold;
      }
      .i, .r {
        font-style: normal;
      }
      .s, .aut {
        font-variant: small-caps;
      }
      .i, .werk, .gebrW, .morfI {
        font-style: italic;
      }
      .toel {
        color: black;
      }
      .toel, .hulpI {
        font-weight: normal;
        font-style: italic;
      }
      .link {
        font-style: normal;
        cursor: pointer;
      };

      .annotation-link, .citg-word {
        cursor: pointer;
      }

      .snippet {
        opacity: 1.0 !important;

        .abbr, .citg-word, .verw {
          cursor: inherit;
          border-bottom: 0px;
        }
        .link {
          color: inherit;
          cursor: inherit;
        }
      }

      div.lem, div.verwLem, div.escLem, div.xlLem, div.snippet {
        padding-top: 0px;
        padding-bottom: 0px;
      }

      &.x--small {
        font-size: 14px;
        line-height: 21px;
      }

      &.x--medium {
        font-size: 16px;
        line-height: 24px;
      }

      &.x--big {
        font-size: 18px;
        line-height: 27px;
      }

      &.x--mobile {
        font-size: 16px;
        line-height: 30px;
        border-top: 0px solid black;
        border-bottom: 0px solid black;
      }

      &.x--selected {
        .annotation-link, .citg-word {
          :hover {
            border-bottom: 2px black dotted;
          }
        }
        &.x--mobile {
          .annotation-link, .citg-word {
            :hover {
              border-bottom-width: 0px;
            }
          }
          &.x--mobile.x--not-exploring {
            padding-top: 0px;
            padding-bottom: 0px;
            border-top: 1px solid black;
            border-bottom: 1px solid black;
          }
          &.x--mobile.x--exploring {
            padding-top: 1px;
            padding-bottom: 1px;
            border-top-width: 0px;
            border-bottom-width: 0px;
          }
        }
      }

      &.x--not-selected {
        &.x--mobile.x--not-exploring {
          padding-top: 1px;
          padding-bottom: 1px;
          cursor: pointer;
          &, .hoofdW, .link {
            color: #888 !important;
          }
          opacity: 0.7;
        }
        &.x--mobile.x--exploring {
          padding-top: 1px;
          padding-bottom: 1px;
          opacity: 1.0;
        }
        &.x--not-mobile {
          opacity: 0.5;
        }
        &.x--mobile {
          .annotation-link, .citg-word {
            :hover {
              border-bottom-width: 0px;
            }
          }
        }
        :hover {
          opacity: 1.0;
        }
      }

      .search-highlight {
        background: yellow;
      }
      .snippet {
        display: inline-block;
      }

      ol {
        margin-top: 0px;
        margin-bottom: 0px;
      }
      ol.niv123 {
        text-indent: 0px;
        padding-left: 10px;
        list-style-type: decimal;
      }
      ol.xlNiv123 {
        padding-left: 0px;
        list-style-type: decimal;
      }
      ol.nivABC {
        text-indent: 0px;
        padding-left: 13px;
        list-style-type: lower-latin;
      }
      ol.xlNivABC {
        padding-left: 14px;
        list-style-type: lower-alpha;
      }
      ol.xlNiv-3 {
        padding-left: 11px;
        list-style-type: lower-roman;
      }
      ol.xlNiv-4 {
        padding-left: 13px;
        list-style-type: decimal;
      }
      ol.xlNiv-5 {
        padding-left: 13px;
        list-style-type: lower-alpha;
      }
      ol.xlNiv123, ol.xlNivABC, ol.xlNiv-3, ol.xlNiv-4, ol.xlNiv-5 {
        text-indent: 0px;
      }
      ol.niv123 {
        margin-left: 15px;
      }
      ol.xlNiv123 {
        margin-left: 26px;
      }
    '''
