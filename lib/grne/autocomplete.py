# -*- coding: utf-8 -*-

import re
from sqlite3 import Row, OperationalError

import pydash

from acatpy.util import subn
from acatpy.io.speak import debug
from acatpy.io.speak import set_level as set_speak_level # noqa

from grne.chars import analyse_and_convert_query, to_latin
from grne.chars import regex_greek, regex_greek_with_markings
from grne.chars import regex_beta_chars, regex_latin, regex_wildcards
from grne.chars import regex_dash, canonical
from grne.chars import normalise_macron_breve
from grne.code_to_determinatie import code_to_determinatie
from grne.util.util import open_db

from config import get as config_get

# set_speak_level('DEBUG')

regex_valid_query = r'^(%s)+$' % '|'.join((
    r'\s',
    regex_greek(), regex_greek_with_markings(),
    regex_beta_chars(), regex_latin(),
    regex_wildcards(),
))

def autocomplete_lemma(query, client_limit):
    return autocomplete(query, client_limit, 'web_lemma_db_path', perform_search_lemma)

def autocomplete_pars(query, client_limit):
    return autocomplete(query, client_limit, 'web_pars_db_path', perform_search_pars)

# @todo search error when too many results
def autocomplete_parsget(query):
    return parsget(query, 'web_lemma_db_path', 'web_pars_db_path')

def autocomplete(query, client_limit, db_path_key, perform_search):
    config = config_get()
    db_path = config.main[db_path_key]
    if not re.search(regex_valid_query, query):
        return {
            'error': ('Invalid input', None),
            'results': None,
        }
    # @todo: allow this?
    if re.search(r'^%s' % regex_wildcards(), query):
        return {
            'error': ('Wildcards disabled at beginning', None),
            'results': None,
        }
    err, db = open_db(db_path, read_only=True)
    if err:
        return {
            'error': (None, '%s: %s' % (err, db_path)),
            'results': None,
        }
    db.row_factory = Row

    query_canonical, lookup_mode, pure_spiritus_info, input_errors = process_input(query)
    if input_errors:
        return {
            'error': (input_errors, None),
            'results': None,
        }

    # --- remove dash: makes search on e.g. 'para-bainw' possible.
    query_canonical = re.sub(r'-', '', query_canonical)

    debug('pure_spiritus_info.combined %s' % pure_spiritus_info.combined)
    debug('pure_spiritus_info.has_any %s' % pure_spiritus_info.has_any)
    has_pure_spiritus = pure_spiritus_info.has_any
    pure_spiritus_left, pure_spiritus_right = pure_spiritus_info.left, pure_spiritus_info.right

    search, search_length, has_wildcard, is_bounded, exact_length = analyse_length_and_wildcards(query_canonical)

    debug('lookup_mode %s' % lookup_mode)
    debug('input_errors %s' % input_errors)
    debug('query_canonical %s' % query_canonical)
    debug('search %s' % search)
    debug('has_wildcard %s' % has_wildcard)
    debug('is_bounded %s' % is_bounded)
    debug('exact_length %s' % exact_length)
    debug('pure_spiritus_left %s' % pure_spiritus_left)
    debug('pure_spiritus_right %s' % pure_spiritus_right)
    debug('search_length %s' % search_length)

    search_error, results = perform_search(
        db=db,
        lookup_mode=lookup_mode,
        exact_length=exact_length,
        is_bounded=is_bounded,
        client_limit=client_limit,
        search=search,
        query=query_canonical,
        pure_spiritus_left=pure_spiritus_left,
        pure_spiritus_right=pure_spiritus_right,
        has_pure_spiritus=has_pure_spiritus,
        has_wildcard=has_wildcard,
        search_length=search_length,
    )

    return {
        'error': (None, search_error),
        'results': results,
    }


"""
This is used in building the pars results table in the popup.

It is quite involved and could make use of caching of intermediate results
if speed ever becomes a problem.

What we need to do is:

• Run the query through the pars db (for example πιμελήν)
• This will give lemma=πιμελή
• Look this up in the lemma db -- BUT the lemma db might contain a version
with macron + breve. In this case it's πῑμελή.
• So, make the result into betacode -- pimelh.
• Run it through the lemma/indexI table.
• If this returns multiple results, cross-reference them back to find the exact lemma.
"""

def parsget(query, db_key_lemma, db_key_pars):
    config = config_get()
    db_path_lemma = config.main[db_key_lemma]
    db_path_pars = config.main[db_key_pars]
    debug('db_path_lemma %s' % db_path_lemma)
    _, _, _, input_errors = process_input(query)
    if input_errors:
        return {
            'error': (input_errors, None),
            'results': None,
        }

    dbs = []
    for db_path in db_path_lemma, db_path_pars:
        err, db = open_db(db_path, read_only=True)
        if err:
            return {
                'error': (None, '%s: %s' % (err, db_path)),
                'results': None,
            }
        db.row_factory = Row
        dbs.append(db)
    db_lemma, db_pars = dbs

    results = []

    sql1 = 'select lemma, code from main where token = ?'
    debug('sql1 %s' % sql1)
    err, res1 = execute_sql(db_pars, sql1, (query,))
    if err:
        return {
            'error': (None, err),
            'results': None,
        }
    for row in res1:
        lemma, code = row
        debug('lemma %s' % lemma)
        debug('code %s' % code)
        loose, _, errors = to_latin(lemma)
        if errors:
            return {
                'error': (errors, None),
                'results': None,
            }
        debug('loose %s' % loose)
        sql2 = ' '.join((
            'select fl.main_id, m.lemma',
            'from forms_loose fl',
            'join main m',
            'on fl.main_id = m.id',
            'where fl.token = ?',
        ))
        debug('sql2 %s' % sql2)
        err, inner_res = execute_sql(db_lemma, sql2, (loose,))
        if err:
            return {
                'error': (None, err),
                'results': None,
            }
        determinatie = code_to_determinatie(code)
        debug('determinatie %s' % determinatie)
        if not inner_res:
            debug("we don't have this form (query:%s, loose:%s)-> red link" % (query, loose))
            results.append([lemma, determinatie, None])
        else:
            get = parsget_inner(lemma, determinatie, inner_res)
            get = unique(get)
            if len(get) > 1:
                return {
                    'error': (
                        None,
                        "Too many results for parsget_inner (query:%s, loose:%s, results:%s)" %
                        (query, loose, get)
                    ),
                    'results': [get[0]],
                }
            results.extend(get)
    return {
        'error': (None, None),
        'results': results,
    }

def parsget_inner(lemma, determinatie, res):
    results = []
    for row in res:
        main_id, final_lemma = row
        lemma_c, _ = canonical(lemma)
        # --- in this example, πῑμελή -> x
        final_lemma_c, *_ = normalise_macron_breve(final_lemma)
        final_lemma_c, *_ = canonical(final_lemma)
        # --- remove streepje (pars db doesn't have them).
        final_lemma_c = re.sub(regex_dash(), '', final_lemma_c)
        debug('final_lemma_c %s' % final_lemma_c)
        if final_lemma_c == lemma:
            id0 = main_id - 1
            results.append([lemma, determinatie, id0])
        else:
            # --- the parser matched an indexI form but not the lemma
            # itself. Make a red link.
            debug("matched indexI form but not lemma (lemma:%s, final_lemma:%s)-> red link" % (lemma, final_lemma))
            results.append([lemma, determinatie, None])
    return results

def execute_sql(db, sql, bindings):
    try:
        cur = db.execute(sql, bindings)
        return None, cur.fetchall()
    except OperationalError as e:
        return e, None

def analyse_length_and_wildcards(query):
    search = None
    has_wildcard = None
    exact_length = None
    is_bounded = None

    query = re.sub(r'^\s+', '', query)
    has_wildcard, has_star, first_wildcard_pos = check_wildcards(query)
    is_bounded, exact_length = check_bounds(query, has_star)
    if has_wildcard:
        search = query[0:first_wildcard_pos]
    else:
        search = query
    if exact_length is not None:
        search = re.sub(r'\s+$', '', search)
    search_length = len(search)
    return search, search_length, has_wildcard, is_bounded, exact_length

def perform_search_lemma(
    db=None,
    lookup_mode=None,
    exact_length=None,
    is_bounded=None,
    client_limit=None,
    search=None,
    query=None,
    pure_spiritus_left=None,
    pure_spiritus_right=None,
    has_pure_spiritus=None,
    has_wildcard=None,
    search_length=None,
):

    # --- using m.lemma isn't quite right, so use fs.token.
    # 'prou ' with a space at the end will return no hits, because its length is 4, while the lemma (πρου-) has a length of 5.

    bindings = []
    where_clause = []

    where_clause.append('ac.pref = ?')
    bindings.append(search)

    if exact_length:
        where_clause.append('length(fs.token) = ?')
        bindings.append(exact_length)
    if has_pure_spiritus:
        spiritus_clauses, _bindings = get_spiritus_clause('lemma', pure_spiritus_left, pure_spiritus_right)
        where_clause.extend(spiritus_clauses)
        bindings.extend(_bindings)
    else:
        spiritus_clauses = []
    wildcard_join_join = ''
    wildcard_join_on = ''
    if has_wildcard:
        wildcard_condition = get_wildcard_sql(query, is_bounded)
        if lookup_mode == 'loose':
            wildcard_join_join = 'inner join forms_loose fl'
            wildcard_join_on = 'and fl.id = fs.id'
            wildcard_clause = 'fl.token like ?'
        else:
            wildcard_clause = 'fs.token like ?'
        where_clause.append(wildcard_clause)
        bindings.append(wildcard_condition)
    limit_clause = 'limit ?'
    bindings.append(client_limit)

    ac_table = 'autocomplete_%s_%s' % (lookup_mode, search_length)
    sql = ' '.join((
        'select m.id, fs.token, m.lemma, m.naam, m.homonym_nr from',
        ac_table, 'as ac',
          'inner join',
        'forms_strict fs',
        wildcard_join_join,
          'inner join',
        'main m',
        'on ac.forms_id = fs.id',
        'and m.id = fs.main_id',
        wildcard_join_on,
          'where',
        ' and '.join(where_clause),
        'order by fs.id',
        limit_clause,
    ))

    debug('sql %s' % sql)
    debug('bindings %s' % bindings)

    try:
        cur = db.execute(sql, bindings)
        data = cur.fetchall()
    except OperationalError as e:
        return e, None
    count_il = {}
    done = {}
    for row in data:
        indexI = row['token']
        lemma = row['lemma']
        path = [indexI, lemma]
        cnt = pydash.get(count_il, path, 0)
        pydash.set_(count_il, path, cnt + 1)

    results = []
    for row in data:
        id0 = row['id'] - 1
        indexI = row['token']
        lemma = row['lemma']
        naam = row['naam']
        # --- Sqlite.Row is safe on missing key.
        homonym_nr = row['homonym_nr'] or 1

        cnt = count_il[indexI]
        if len(cnt) > 1:
            retstr = '%s (%s)' % (indexI, lemma)
        else:
            if not done.get(indexI):
                done[indexI] = True
                retstr = indexI
        naam_hom_str = '%s-%s' % (naam, homonym_nr)
        results.append([id0, retstr, naam_hom_str])

    debug('results %s' % results)
    return None, results

def perform_search_pars(
    db=None,
    lookup_mode=None,
    exact_length=None,
    is_bounded=None,
    client_limit=None,
    search=None,
    query=None,
    pure_spiritus_left=None,
    pure_spiritus_right=None,
    has_pure_spiritus=None,
    has_wildcard=None,
    search_length=None,
):
    bindings = []
    where_clause = []

    # @todo ac.pref
    where_clause.append('pref = ?')
    bindings.append(search)

    if exact_length:
        # --- here we can use m.token, which, unlike m.lemma in the lemma
        # case, is free of dashes and things like that.
        where_clause.append('length(m.token) = ?')
        bindings.append(exact_length)

    # --- see lemma case for comments on limit / spiritus.

    spiritus_join_on = ''
    spiritus_join_join = ''
    if has_pure_spiritus:
        spiritus_clauses, _bindings = get_spiritus_clause('pars', pure_spiritus_left, pure_spiritus_right)
        where_clause.extend(spiritus_clauses)
        bindings.extend(_bindings)
        spiritus_join_on = 'and fss.main_id = m.id'
        spiritus_join_join = 'inner join forms_strict fss'
    wildcard_join_join = ''
    wildcard_join_on = ''
    if has_wildcard:
        wildcard_condition = get_wildcard_sql(query, is_bounded)
        if lookup_mode == 'loose':
            wildcard_join_join = 'inner join forms_loose fl'
            wildcard_join_on = 'and fl.main_id = m.id'
            wildcard_clause = 'fl.token like ?'
        else:
            wildcard_join_join = 'inner join forms_strict fs'
            wildcard_join_on = 'and fs.main_id = m.id'
            wildcard_clause = 'fs.token like ?'
        where_clause.append(wildcard_clause)
        bindings.append(wildcard_condition)
    limit_clause = 'limit ?'
    bindings.append(client_limit)
    '''
    if exact_length:
        ac_table = 'autocomplete_%s_%s' % (lookup_mode, exact_length)
    else:
        ac_table = 'autocomplete_%s_%s' % (lookup_mode, search_length)
    '''
    ac_table = 'autocomplete_%s_%s' % (lookup_mode, search_length)
    sql = ' '.join((
        'select distinct m.token from',
        ac_table, 'as ac',
          'inner join',
        'main m',
          spiritus_join_join,
          wildcard_join_join,
        'on ac.main_id = m.id',
          spiritus_join_on,
          wildcard_join_on,
          'where',
        ' and '.join(where_clause),
        'order by m.id',
        limit_clause,
    ))
    debug('sql %s' % sql)
    debug('bindings %s' % bindings)
    try:
        cur = db.execute(sql, bindings)
        data = cur.fetchall()
    except OperationalError as e:
        return e, None
    results = [row['token'] for row in data]
    debug('results %s' % results)
    return None, results

def process_input(query):
    new, lookup_mode, pure_spiritus_info, errors = analyse_and_convert_query(query)
    if errors: return None, None, None, errors
    return new.lower(), lookup_mode, pure_spiritus_info, errors

def check_wildcards(query):
    m = re.search(r'([*?])', query)
    if not m:
        return False, False, None
    pos = m.start()
    has_star = None
    if not has_star:
        has_star = re.search(r'\*', query)
    return True, bool(has_star), pos


"""
If the user types a space after the query, we consider it 'bounded'.
If it is bounded and does not contain a star, we take it to mean search for
this exact length.
If it's bounded and contains a star, then the length stays on None. An example of why this is useful is 'pa*n' vs. 'pa*n '. The latter returns only words which end in ν.
"""

def check_bounds(query, has_star):
    n, new = subn(r'\s+$', '', query)
    if n == 0:
        return False, None
    exact_length = len(new) if not has_star else None
    return True, exact_length

def get_spiritus_clause(mode, query_psl, query_psr):
    query_psl = query_psl or '0'
    query_psr = query_psr or '0'
    qld = int(query_psl, base=2)
    qrd = int(query_psr, base=2)

    # --- give us hits which match the bitmask.
    # ei)en , ei)e(n , eien , ei=)e(n, all match the hit εἶἑν
    clauses = []
    bindings = []
    if qld > 0:
        if mode == 'lemma':
            clauses.append('fs.spiritus_left & ? == ?')
        if mode == 'pars':
            clauses.append('fss.spiritus_left & ? == ?')
        bindings.extend([qld, qld])
    if qrd > 0:
        if mode == 'lemma':
            clauses.append('fs.spiritus_right & ? == ?')
        if mode == 'pars':
            clauses.append('fss.spiritus_right & ? == ?')
        bindings.extend([qrd, qrd])

    return clauses, bindings


'''
# @todo move to chars.
def do_spiritus(pure_spiritus):
    has_pure_spiritus = None
    pure_spiritus_left = []
    pure_spiritus_right = []
    for x in reversed(pure_spiritus):
        if x == -7:
            has_pure_spiritus = True
            l, r = 1, 0
        elif x == 7:
            has_pure_spiritus = True
            l, r = 0, 1
        else:
            l, r = 0, 0
        pure_spiritus_left.append(str(l))
        pure_spiritus_right.append(str(r))
    return (
        bool(has_pure_spiritus),
        ''.join(pure_spiritus_left),
        ''.join(pure_spiritus_right),
    )
'''

def get_wildcard_sql(query, is_bounded):
    # --- sqlite re: % = 0 or more, _% = 1 or more
    rx = query
    rx = re.sub(r'\*', '_%', rx)
    rx = re.sub(r'\?', '_', rx)
    if not is_bounded:
        rx = rx + '%'
    return rx

'''
# --- a simple way to remove duplicate elements from a list (based on ==) when
# the elements are not hashable (e.g. a list of lists).
# --- if `clone` is `False` the original list gets left in an undefined state.
'''

def unique(xs, clone=True):
    if clone:
        ys = xs.copy()
    else:
        ys = xs
    zs = []
    while ys:
        y = ys.pop(0)
        ys = [_ for _ in ys if _ != y]
        zs.append(y)
    return zs
