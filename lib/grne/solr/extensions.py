from functools import namedtuple
import re

from lxml.etree import XSLTExtension

import pydash

from grne.chars import to_latin, regex_greek

from acatpy.io.speak import warn, say
from acatpy.util import make_uuid

config_to_latin_noisy = False
rx_gr = regex_greek()

# --- encapsulate the extension classes, so that they all share a single
# main_uuids dictionary, and allow dependency injection.

def extension_wrapper():
    # --- mapping from the `id_naam` of a main document to its uuid, so that
    # child (`cit`) documents can link to it.
    main_uuids = {}

    # --- `input_node` is the cursor node.
    # --- `self_node` is the extension node.

    class ExtensionMakeLoose(XSLTExtension):
        def execute(self, context, self_node, input_node, output_parent):
            the_input = input_node.text or ''
            loose = make_loose(the_input)
            output_parent.text = loose

    class ExtensionMakeHoofdWClean(XSLTExtension):
        def execute(self, context, self_node, input_node, output_parent):
            the_input = input_node.text or ''
            clean = make_hoofdW_clean(the_input, allow_star=True)
            output_parent.text = clean

    class ExtensionMakeHoofdWSort(XSLTExtension):
        def execute(self, context, self_node, input_node, output_parent):
            the_input = input_node.text or ''
            clean = make_hoofdW_clean(the_input, allow_star=False)
            output_parent.text = clean.lower()

    class ExtensionMakeUUID(XSLTExtension):
        def execute(self, context, self_node, input_node, output_parent):
            content, *_ = self.process_children(context) or [None]
            if not content:
                return warn("ExtensionMakeUUID: no content")
            uuid = make_uuid()
            document_type, main_naam_id = [x.strip() for x in content.split('|')]
            if document_type == 'main':
                if not main_naam_id:
                    return warn('Unable to find the parent id_naam for this cit')
                main_uuids[main_naam_id] = uuid
            elif document_type != 'cit':
                return warn('Expecting type main or cit, got %s' % document_type)
            output_parent.text = uuid

    class ExtensionGetCitRoot(XSLTExtension):
        def execute(self, context, self_node, input_node, output_parent):
            content, *_ = self.process_children(context) or [None]
            main_naam_id = (content or '').strip()
            if not main_naam_id:
                return warn("ExtensionGetCitRoot: no or empty content")
            parent_uuid = pydash.get(main_uuids, main_naam_id)
            if not parent_uuid:
                return warn(
                    'Unable to find the parent uuid for this cit, main_naam_id was %s' % main_naam_id
                )
            output_parent.text = parent_uuid

    Ext = namedtuple('Ext', ('loose', 'hoofd_clean', 'hoofd_clean_lower', 'uuid', 'cit_root'))

    return Ext(
        ExtensionMakeLoose,
        ExtensionMakeHoofdWClean, ExtensionMakeHoofdWSort,
        ExtensionMakeUUID, ExtensionGetCitRoot,
    )

def make_loose(s):
    latin, _, errors = to_latin(s, to_lower=False)
    if config_to_latin_noisy:
        say('to-latin: %s -> %s' % (s, latin))
    return 'error' if errors else latin

def make_hoofdW_clean(s, allow_star=True):
    rx_kill = r'[^*]' if allow_star else '.'
    rx = r'^ %s*? (%s)' % (rx_kill, rx_gr)
    clean = re.sub(rx, r'\1', s, flags=re.X)
    strip = clean.strip()
    return re.sub(r'[;,.]$', '', strip)
