# -*- coding: utf-8 -*-

from grne.solr_pre_index import process as solr_pre_index_process
from grne.util.util import get_good_xml

from acatpy.colors import bright_red, yellow, underline, green, cyan
from acatpy.io.io import die, cmd, mkdir, cpa
from acatpy.io.speak import info, say, sayn, warn
from acatpy.speak import info_pref
from acatpy.solr import add_field as solr_add_field, add_copy_field as solr_add_copy_field
from acatpy.solr import update_request as solr_update_request
from acatpy.solr import config_no_data_driven as solr_config_no_data_driven
from acatpy.solr import solr_cmd, has_core as solr_has_core, is_running as solr_is_running
from acatpy.util import pathjoin, against_all, imerge

from config import get as config_get

debug_filenames = False
debug_xml_transformations = False
# --- @todo works, but wrong encoding.
debug_no_aggregate = False
# --- set to `None` for off.
debug_limit_num_files = None
debug_skipped_verwijslemmata = True

class Solr(object):
    def __init__(
            self, conf_dir=None, path=None, port=None, solr_data_path=None,
            data_root=None,
            data_filename_filters=None,
            scheme='http',
            host='localhost',
            uripath='/solr',
            schema=None,
            status_strings_good=None,
            core_name=None):
        self._conf_dir = conf_dir
        self._uripath = uripath
        self._scheme = scheme
        self._host = host
        self._port = port
        self._solr_data_path = solr_data_path
        self._data_root = data_root
        self._data_filename_filters = data_filename_filters
        self._core_name = core_name
        self._schema = schema
        self._status_strings_good = status_strings_good
        self._bin = pathjoin(path, 'bin', 'solr')

    def info(self, s):
        info('%s: %s' % (underline('solr-admin'), s))

    def cmd(self, *args, **kw):
        return solr_cmd(self._bin, *args, **kw)

    def url(self):
        return '%s://%s:%s%s' % (
            self._scheme,
            self._host,
            self._port,
            self._uripath,
        )

    # --- only local.
    def start(self, ignore_if_running=True):
        self.info('start server')
        if ignore_if_running and self.is_running():
            self.info('already running, ok')
            return
        dp = self._solr_data_path
        try:
            mkdir(dp)
        except PermissionError:
            die('Unable to create %s -- please check directory permissions.' % bright_red(dp))
        solrxml = pathjoin(self._conf_dir, 'server', 'solr.xml')
        cpa(solrxml, dp)
        self.cmd([
            'start', '-s', dp,
        ])
        say('')
        say('')
        say('')
        say('')

    def is_running(self):
        return solr_is_running(
            self._bin,
            self.url(),
        )

    # --- only local.
    def stop(self, ignore_unless_running=False):
        self.info('stop server')
        if ignore_unless_running and not self.is_running():
            info('solr-admin: not running, ok')
            return

        return self.cmd([
            'stop', '-p', self._port,
        ])

    # --- only local.
    def create(self):
        self.info('create core %s' % yellow(self._core_name))
        cn, cd = self._core_name, self._conf_dir
        return self.cmd([
            'create', '-c', cn,
            '-s', '2',
            '-rf', '2',
            '-d', cd,
        ])

    # --- only local.
    def delete(self, ignore_unless_has=True):
        cn = self._core_name
        self.info('delete core %s' % bright_red(cn))
        if ignore_unless_has and not solr_has_core(self._host, self._port, cn):
            return self.info("core doesn't exist, ok")
        self.cmd([
            'delete', '-c', self._core_name,
        ])

    # --- only local.
    def config(self):
        self.info('config no data-driven')
        solr_config_no_data_driven(self._bin, self._port, self._core_name)

    def add_schema(self):
        self.info('add schema')
        schema = self._schema

        for conf in schema:
            solr_conf, copy_to = conf['solr-conf'], conf['copy-to']
            name = solr_conf['name']
            if not isinstance(copy_to, set):
                die('copy-to field must be a set (use empty set for none)')
            solr_add_field(self._host, self._port, self._core_name, solr_conf)
            for copy_to_name in copy_to:
                solr_add_copy_field(self._host, self._port, self._core_name, name, copy_to_name)

    def add_data(self):
        filters = self._data_filename_filters
        data_root = self._data_root
        filter_file = lambda filename: against_all(filters, filename)
        ret = cmd(['find', data_root, '-name', '*.xml'], capture_stdout=True)
        filenames = ret.out.splitlines()
        filenames = filenames[0:debug_limit_num_files]
        report_filenames(filenames)
        fns_filename_ok = [x for x in filenames if filter_file(x)]
        fns_bad_parse, fns_bad_status, fns_reject_verwLem, fns_status_ok = get_good_xml(
            self._status_strings_good,
            fns_filename_ok,
            custom_cond=[
                (lambda _, parsed: parsed.find('/verwLem') is None, 'is verwijslemma'),
            ],
        )
        if fns_bad_parse: warn(
            'Ignoring %s unreadable files or files with invalid xml: %s' %
            (bright_red(len(fns_bad_parse)), ', '.join(fns_bad_parse))
        )
        if fns_bad_status: warn(
            'Ignoring %s files with unfinalised xml' %
            (yellow(len(fns_bad_status)))
        )
        show_reject_verwLem = '\n'.join(
            (' / '.join(_) for _ in fns_reject_verwLem)
        )
        if fns_reject_verwLem: info(
            'Ignoring %s verwijslemma(ta): %s' %
            (
                cyan(len(fns_reject_verwLem)),
                show_reject_verwLem if debug_skipped_verwijslemmata else '[not shown]',
            )
        )
        # --- alphanumeric string sort
        fns_status_ok.sort()
        if debug_no_aggregate: self.add_data_separate(fns_status_ok)
        else: self.add_data_aggregate(fns_status_ok)

    def do_update_request(self, xml, die=True):
        return solr_update_request(self._host, self._port, self._core_name, xml, die=die)

    def add_data_aggregate(self, filenames):
        xml = add_doc_iterator(filenames)
        return self.do_update_request(xml)

    def add_data_separate(self, filenames):
        reporter = Reporter()
        for xml in xml_filename_loop(filenames, reporter):
            add = ('<add>%s</add>' % xml).encode()
            self.do_update_request(add)
        say('')

    def add_doc_as_xml(self, xml, die=True):
        add = ('<add>%s</add>' % xml).encode()
        return self.do_update_request(add, die=die)

class Reporter(object):
    def __init__(self):
        self.total_cnt = 0
        self.types = {}

    def do_update(self, type):
        cur = self.types.get(type) or 0
        cur += 1
        self.total_cnt += 1
        self.types[type] = cur

    def update(self, type):
        self.do_update(type)
        self.print()

    def print(self):
        cnt = [(yellow(a), b) for a, b in self.types.items()]
        total = [(green('total'), self.total_cnt)]

        make_num_str = lambda type, num: '%s %s' % (num, type)
        make_gen = lambda x: (make_num_str(a, b) for a, b in x)
        make_str = lambda ch, x: ch.join(make_gen(x))
        breakdown_str = make_str(', ', cnt)
        total_str = make_str('', total)
        cnt_str = '%s (%s)' % ((total_str), (breakdown_str))
        sayn('\r%s%s: %s' % (info_pref(), underline('indexed'), cnt_str))

def report_filenames(filenames):
    info('%s: %s' % (underline('# lemma files'), green(len(filenames))))

# --- use a generator to send a stream of chunks through a single request.
# --- here each chunk is a single tag or file, though the chunks can be of
# arbitrary length and solr will parse and index incrementally.

def add_doc_iterator(filenames):
    reporter = Reporter()
    yield b'<add>'
    for xml in xml_filename_loop(filenames, reporter):
        yield xml
    yield b'</add>'
    say('')

def xml_filename_loop(filenames, reporter):
    verbose_filenames = debug_filenames
    verbose_transformations = debug_xml_transformations

    def progress_filename(x):
        if verbose_filenames: say('\n' + x)
        return x

    def progress_xml(xml, type):
        reporter.update(type)
        return xml

    def xml_iter(filename):
        err, xmls, _ = transform(filename)
        if err:
            die("Error with read and transform xml: %s" % err)
        return (progress_xml(xml, type) for xml, type in xmls)

    transform = lambda filename: read_and_transform_xml(
        filename, verbose_transformations=verbose_transformations
    )
    filenames_iter = (progress_filename(x) for x in filenames)
    return (xml for filename in filenames_iter for xml in xml_iter(filename))

def read_and_transform_xml(filename, verbose_transformations=False):
    with open(filename, 'r') as f:
        xml_in = f.read()
        if xml_in.strip() == '':
            say('')
            warn('File %s was empty, skipping' % bright_red(filename))
            return []
        return solr_pre_index_process(xml_in)

def with_config(**overrides):
    conf = config_get()
    conf_main = conf.main
    conf_local = conf.local
    conf_user = conf.user
    conf_schema = conf.schema

    core_name = conf_main['solr_core_name']

    defaults = {
        'conf_dir': conf_main['solr_conf_dir'],
        'path': conf_user['solr_path'],
        'port': str(conf_user['solr_port']),
        'solr_data_path': conf_user['solr_data_path'],
        'data_root': conf_local['lemma_input_path'],
        'data_filename_filters': conf_main['lemma_filename_filters'],
        'core_name': core_name,
        'schema': conf_schema['schema'],
        'status_strings_good': conf_main['status_strings_good'],
    }

    kw = imerge(defaults, overrides)

    return Solr(**kw)
