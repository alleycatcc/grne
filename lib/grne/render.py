# -*- coding: utf-8 -*-

from functools import partial
from sqlite3 import OperationalError

from grne.build.make_site import transform_lemma_to_html_from_string
from acatpy.io.speak import warn, debug, set_level as set_speak_level # noqa

# --- @todo config
max_allowed_ids = 101

TEST_SCHOOL = False

def get_entries(db, d_start, id_end):
    pass

def _sql_block():
    return '''select %s from render r
      join main m
      on r.main_id = m.id
      where r.main_id >= ? and r.main_id <= ?
    '''

def _sql_id():
    return '''select %s from render r
      join main m
      on r.main_id = m.id
      where r.main_id in (%s)
    '''

# --- get rendered normal/school html from the db.
# --- range is inclusive.
def render_block(db, do_school, only_entries, id_start, id_end):
    fields = _get_main_fields(do_school, only_entries)
    sql = _sql_block() % fields
    cur = db.execute(sql, (id_start, id_end))
    ret = {}
    for row in cur:
        row_dict = dict(row)
        main_id = row_dict.pop('main_id')
        row_dict['id'] = main_id
        t1g = row_dict.get('t1_gecontroleerd')
        if t1g is not None: row_dict['t1_gecontroleerd'] = bool(t1g)
        if TEST_SCHOOL and do_school:
            import random
            has_school = random.choice((True, False))
            row_dict['html_school'] = '%s school' % row_dict['lemma'] if has_school else None
        ret[main_id] = row_dict
    return ret

# --- get rendered normal/school html from the db.
def render_ids(db, do_school, ids):
    fields = _get_main_fields(do_school, False)

    placeholders = ', '.join(('?',) * len(ids))
    sql = _sql_id() % (fields, placeholders)

    cur = db.execute(sql, ids)
    ret = {}
    for row in cur:
        row_dict = dict(row)
        main_id = row_dict.pop('main_id')
        row_dict['id'] = main_id
        t1g = row_dict.get('t1_gecontroleerd')
        if t1g is not None: row_dict['t1_gecontroleerd'] = bool(t1g)
        ret[main_id] = row_dict
    return ret

# --- rerun the xml transform pipeline, with highlighting and/or
# schoolreductie.

# the reason we have to run the transformation again is that after doing an
# advanced search and then clicking back through to the browseview, we want
# a rendered lemma with highlighting and possible schoolreductie -- this is
# impossible to cache beforehand.

def render_ids_with_highlight(db, spec, do_school, do_post_processing=None):
    ids = tuple(spec.keys())
    fields = _get_highlight_fields()
    placeholders = ', '.join(('?',) * len(ids))
    sql = _sql_id() % (fields, placeholders)
    cur = db.execute(sql, ids)
    res = {}
    for row in cur:
        row_dict = dict(row)
        main_id = row_dict.pop('main_id')
        row_dict['id'] = main_id
        t1g = row_dict.get('t1_gecontroleerd')
        if t1g is not None: row_dict['t1_gecontroleerd'] = bool(t1g)
        the_spec = spec[main_id]
        terms = the_spec['terms']
        do_abridge = the_spec['abridge']
        res[main_id] = (row_dict, terms, do_abridge)
    return _transform_xml(do_school, res, do_post_processing=do_post_processing)

def _transform_xml(do_school, results, do_post_processing=None):
    ret = {}
    for main_id, spec in results.items():
        row_dict, highlighting_terms, do_abridge = spec
        lemma = row_dict['lemma']
        xml = row_dict.pop('xml')
        try:
            err, html, html_school, _, _ = transform_lemma_to_html_from_string(
                xml,
                # --- reductie_modus=normal: don't use any of the analyse modes
                # --- reductie_modus=none: skip all reductie.
                'normal' if do_school else 'none',
                search_terms=highlighting_terms,
                make_snippet=do_abridge,
                do_post_processing=do_post_processing,
            )
        except Exception as e:
            err = e
        if err:
            err_str = 'Error transforming lemma (%s): %s' % (lemma, err)
            warn(err_str)
            return err_str, None
        row_dict['html'] = html
        if do_school:
            row_dict['html_school'] = html_school
        ret[main_id] = row_dict
    return None, ret

# --- do_school and only_entries can not both be true.
def _get_main_fields(do_school, only_entries):
    if only_entries:
        the_fields = ('main_id', 'm.lemma', 'm.homonym_nr')
    else:
        fields = ('main_id', 'm.naam', 'm.lemma', 'r.html',
                  'r.t1_gecontroleerd', 'm.homonym_nr')
        the_fields = (*fields, 'html_school') if do_school else fields
    return ', '.join(the_fields)

def _get_highlight_fields():
    fields = ('main_id', 'm.naam', 'm.lemma', 'r.xml', 'r.t1_gecontroleerd',
              'm.homonym_nr')
    return ', '.join(fields)

# --- range is inclusive.
# --- the ids get capped between [1, max_id]
# guaranteed that max_id is valid.
# --- capping occurs after range is checked against max_allowed_ids.

def get_id_range(db, the_id, num_before, num_after, disable_limit=False):
    def cap(min_n, max_n, n):
        return min(max(min_n, n), max_n)

    num_requested = num_after + num_before + 1
    if not disable_limit and num_requested > max_allowed_ids:
        err = 'too many requested (max is %d)' % max_allowed_ids
        return err, None, None

    (max_id,) = db.execute('select count(*) from main').fetchone()
    capper = partial(cap, 1, max_id)
    return None, capper(the_id - num_before), capper(the_id + num_after)

def execute_sql(db, sql, bindings, throw=False):
    try:
        res = db.execute(sql, bindings).fetchall()
        return res if throw else (None, res)
    except OperationalError as e:
        if throw:
            raise e
        return e, None
