# -*- coding: utf-8 -*-

"""
Find and replace a piece of text in an xml tree, returning the tree intact
with only the text replaced.

The text must occur in a text node and must begin and end in the same text
node.
"""

import io
import re

from lxml import etree as etree
from lxml.etree import XMLSyntaxError

from acatpy.io.speak import debug
from acatpy.xml import get_text

# --- manually force the order of attributes (which is otherwise arbitrary);
# disappear after @migrate.

def sort_migrate(items):
    def key(a):
        k, *_ = a
        if k == 'data-abbr': return 1
        if k == 'data-targetid': return -1
        if k == 'target': return 1
        return 0
    the_items = list(items)
    the_items.sort(key=key)
    return the_items

class MyParser(object):
    def __init__(self, startpos, matchlen, replace, match):
        self._output = []
        self._startpos = startpos
        self._matchlen = matchlen
        self._replace = replace
        self._match = match
        self._idx = 0
        self._found = False

    def start(self, tag, attrib):
        ts = [
            tag,
            # *['%s="%s"' % (k, v) for k, v in attrib.items()],
            *['%s="%s"' % (k, v) for k, v in sort_migrate(attrib.items())],
        ]
        self._output.append('<%s>' % ' '.join(ts))

    def end(self, tag):
        self._output.append('</%s>' % tag)

    def data(self, data):
        datalen = len(data)
        idx = self._idx
        matchlen = self._matchlen
        startpos = self._startpos
        replace = self._replace
        match = self._match
        if startpos >= idx and startpos < idx + datalen:
            self._found = True
            newdata_s = list(data)
            x = startpos - idx
            y = x + matchlen
            debug('x %s' % x)
            debug('startpos %s' % startpos)
            debug('idx %s' % idx)
            debug('matchlen %s' % matchlen)
            if y > datalen - 1:
                debug('case 1')
                remaining = y - datalen + 1
                z = datalen
                debug('y %s' % y)
                debug('datalen %s' % datalen)
                debug('remaining %s' % remaining)
                debug('z %s' % z)
            else:
                debug('case 2')
                remaining = 0
                z = y + 1
            debug('before, newdata_s %s' % newdata_s)
            # --- second index non-inclusive
            newdata_s[x:z] = replace(match)
            debug('after, newdata_s %s' % newdata_s)
            newdata = ''.join(newdata_s)
            self._remaining = remaining
        elif self._found and self._remaining > 0:
            if datalen >= self._remaining:
                newdata_s = data[self._remaining:]
                self._remaining = 0
            else:
                newdata_s = []
                self._remaining -= datalen
            newdata = ''.join(newdata_s)
        else:
            newdata = data
        self._idx = self._idx + len(data)
        self._output.append(newdata)

    def output(self):
        return ''.join(self._output)

    def close(self):
        if not self._found:
            errors = [
                'Unable to perform replacement; output is %s' % self.output()
            ]
            return errors, None
        return None, self.output()

# --- the replace string can contain xml.
def replace_text_in_xml_tree(xml_tree, search, replace, re_flags=None):
    text = get_text(xml_tree)
    m = re.search(search, text, re_flags)
    if not m:
        errors = [
            "replace_text_in_xml_tree: couldn't find %s in %s" % (search, text),
        ]
        return errors, None
    startpos = m.start()
    matchlen = m.end() - m.start()
    myparser = MyParser(startpos, matchlen, replace, m)
    parser = etree.XMLParser(target=myparser)
    xml = etree.tostring(xml_tree)
    errors, results = etree.parse(io.BytesIO(xml), parser)
    if errors:
        # @todo annotate
        errors.insert(0, 'Text %s was found, but replacement failed' % m.group(0))
        return errors, None
    try:
        """
        results = re.sub(
            r'</span> </span></span>',
            '</span></span> </span>',
            results,
        )
        """

        newtree = etree.fromstring(results)
    except XMLSyntaxError as e:
        error = 'replace_text_in_xml_tree: resulting xml is invalid: %s: %s' % (results, e)
        return error, None
    return None, newtree
