# -*- coding: utf-8 -*-

import io
import os
from os import path
from random import randint
import sqlite3
from sys import exc_info
import traceback

from lxml.etree import XMLSyntaxError
from lxml.etree import parse as lxml_parse_stream, fromstring as lxml_parse_string

from grne.chars import expand_entities

from config import get as config_get

from acatpy.colors import bright_red, yellow, cyan
from acatpy.io.speak import warn, info

config = config_get()

# --- for programmer errors.

class ProgrammerException(Exception):
    pass

# --- similar to programmer errors, e.g. for when a function is called with
# nonsense or unexpected arguments.

class APIException(Exception):
    pass

def inf_list(n):
    i = n
    while True:
        yield i
        i += 1

def file_is_empty(filepath):
    size = os.stat(filepath).st_size
    if size == 0:
        warn("File %s is empty" % bright_red(filepath))
        return True
    return False

def dir_exists_and_writeable(the_dir, **kw):
    return path.isdir(the_dir) and exists_and_writeable(the_dir, extra_flags=os.X_OK, **kw)

def dir_exists_and_readable(the_dir, **kw):
    return path.isdir(the_dir) and exists_and_readable(the_dir, extra_flags=os.X_OK, **kw)

def exists_and_writeable(the_path, extra_flags=0, pref=None):
    ok = os.access(the_path, os.W_OK | extra_flags)
    if not ok:
        pref = pref + ' ' if pref else ''
        warn("%s%s doesn't exist or is not writeable" % (pref, bright_red(the_path)))
    return ok

def exists_and_readable(the_path, extra_flags=0, pref=None):
    ok = os.access(the_path, os.R_OK | extra_flags)
    if not ok:
        pref = pref + ' ' if pref else ''
        warn("%s%s doesn't exist or is not readable" % (pref, bright_red(the_path)))
    return ok

# --- read_only also means, don't make the file if it doesn't exist; note that it's up to you to
# make sure the file exists in this case because the driver doesn't complain.
# --- python3 sqlite3 uses transactions by default.

def open_db(db_path, read_only=False, throw=False):
    if read_only:
        uri = 'file:%s?mode=ro' % db_path
        args = [uri]
        kw = {'uri': True}
    else:
        args = [db_path]
        kw = {}
    db = None
    try:
        '''
        We relax the `check_same_thread` restriction here because we assume
        sqlite3 is in 'serialized' mode by default.

        The assumptions are:
        1) libsqlite3-0 is compiled with -DSQLITE_THREADSAFE=1 and
        --enable-threads. This is true on debian since stretch.
        2) There is no start-time selection of the mode.
        3) Modules/_sqlite/connect.c in libpython3-stdlib uses on a
        per-connection basis either
        `sqlite3_open_v2` without the SQLITE_OPEN_NOMUTEX or
        SQLITE_OPEN_FULLMUTEX flags, or `sqlite3_open`.
        This is true since at least libpython3.5-stdlib (stretch).

        Concurrent writing from different threads is trickier. It seems to
        work if you make the db handle in the thread (i.e. in the route
        handler) instead of making it higher and sharing it. The other
        threads will apparently then wait for this thread's transactions to
        complete. Tested with transaction times of up to 10 seconds.

        See https://www.sqlite.org/threadsafe.html
        '''
        db = sqlite3.connect(*args, check_same_thread=False, **kw)
        return db if throw else (None, db)
    except sqlite3.OperationalError as e:
        if throw:
            raise e
        else:
            return e, None

# --- @todo it would be better to do `expand_entities` at another point,
# because this leads to duplication (in the analyse routines for example).

def read_xml_from_file(fn):
    with open(fn, 'r') as fh:
        return expand_entities(fh.read())


'''
The xml files contain relative paths to a .dtd and an .xsd, but we ignore
those and don't try any validation.

We resolve entities using a global search and replace -- doing it otherwise
doesn't seem to be possible with lxml without altering the xml files.
'''

def get_xml_from_file(fn):
    try:
        contents = read_xml_from_file(fn)
        stream = io.StringIO(contents)
        return lxml_parse_stream(stream)
    except OSError as e:
        warn("Could not open file %s: %s" % (bright_red(fn), e))
    except XMLSyntaxError as e:
        warn("Could not parse xml in file %s: %s" % (bright_red(fn), e))
    except Exception as e:
        warn("Could not open/parse file %s (%s)" % (bright_red(fn), e))

# @todo repeated code
def get_xml_status_from_file(fn):
    parsed = get_xml_from_file(fn)
    if not parsed: return None, None
    status = parsed.find('/metadata/status')
    if status is None:
        return warn("No '/metadata/status' field in %s" % bright_red(fn)), parsed
    return status.get('status'), parsed

def parse_xml(xml, throw=False, quiet=False):
    try:
        parsed = lxml_parse_string(xml.encode())
    except XMLSyntaxError as e:
        err = "Could not parse: err=%s, xml=%s" % (e, xml)
        if not quiet:
            warn(err)
        if throw:
            raise e
        return err, None
    if parsed is not None:
        if throw:
            return parsed
        else:
            return None, parsed

def get_xml_metadata_field(
        field_name, xml,
        attr=None, default=False,
        parse=True, return_parsed=False,
):
    err = None
    if parse:
        err, parsed = parse_xml(xml)
    else:
        parsed = xml
    if err:
        if return_parsed:
            return err, None, None
        else:
            return err, None
    xpath = './/metadata/%s' % field_name
    field_value = parsed.find(xpath)
    if field_value is None:
        if default is not None:
            if return_parsed:
                return None, default, parsed
            else:
                return None, default
        err = "No '%s' field" % xpath
        if return_parsed:
            return err, None, parsed
        else:
            return err, None
    the_value = field_value.get(attr) if attr else field_value
    if return_parsed:
        return None, the_value, parsed
    else:
        return None, the_value

def get_xml_status(*args, **kw):
    return get_xml_metadata_field('status', attr='status', *args, **kw)

def get_xml_status_t1(*args, **kw):
    return get_xml_metadata_field('status-t1', attr='status-t1', *args, **kw)

def get_good_xml(status_strings_good, fns, custom_cond=None):
    custom_cond = custom_cond or [(ident, None)]
    bad_parse, bad_status, reject_custom_cond, ok = [], [], [], []
    for fn in fns:
        # --- parses.
        status, parsed = get_xml_status_from_file(fn)
        if status is None:
            bad_parse.append(fn)
            continue
        if status not in status_strings_good:
            bad_status.append(fn)
            continue
        should_skip = None
        for cond, cond_desc in custom_cond:
            if not(cond(fn, parsed)):
                reject_custom_cond.append((fn, cond_desc))
                should_skip = True
                break
        if should_skip:
            continue
        ok.append(fn)
    return bad_parse, bad_status, reject_custom_cond, ok

def ident(x, *_):
    return x

def is_odd(x):
    return is_int(x) and x % 2 == 1

def is_int(x):
    return int(x) == x

def chunks(xs, n_at_a_time):
    for i in range(0, len(xs), n_at_a_time):
        j = i + n_at_a_time
        yield xs[i:j]

def scramble(xs, clone=True):
    new = []
    ys = xs.copy() if clone else xs
    l = len(ys)
    while ys:
        m = randint(0, l - 1)
        new.append(ys.pop(m))
        l -= 1
    return new

def take(n, xs):
    i = 1
    for x in xs:
        if i > n: break
        i += 1
        yield x

def comma(x):
    def f(acc, xs):
        if len(xs) > 3:
            return f([*acc, xs[0:3]], xs[3:])
        return [*acc, xs]
    s = reverse_string('%d' % x)
    t = f([], s)
    return reverse_string(','.join(t))

def reverse_string(s):
    return ''.join(reversed(list(s)))

def get_filename_stub(filename, stub=3):
    return '/'.join(reversed(list(take(stub, reversed(filename.split('/'))))))

def assemble_solr_url(host, port, stub):
    return '/'.join(('%s:%s' % (host, port), stub))

def get_fulltext_fields(schema):
    fulltext_field = config.main['solr_fulltext_field']
    is_fulltext = lambda conf: fulltext_field in conf['copy-to']
    return (conf['solr-conf']['name'] for conf in schema if is_fulltext(conf))

# --- @throws
def get_env_value(key, default=None, convert=str, valid=None, required=True, quiet=False, sensitive=False):
    def get():
        val = os.environ.get(key)
        # --- it is allowed for the var to be set to an empty string.
        if val is None:
            if default is None and required:
                raise Exception('Missing required environment variable %s' % bright_red(key))
            info('%s not set in environment: using %s' % (key, bright_red(default)))
            return default
        val = convert(val)
        if valid and val not in valid:
            es = (yellow(_) for _ in valid)
            warn(
                '%s (%s) not recognised, valid values are (%s), using %s' %
                (key, bright_red(val), '|'.join(es), bright_red(default))
            )
            return default
        return val
    val = get()
    if not quiet:
        info_env(key, val, sensitive=sensitive)
    return val

def info_env(key, val, sensitive=False):
    if sensitive:
        valp = '***' if val is not None else '<empty>'
    else:
        valp = val
    info('[env: %s] -> %s' % (yellow(key), valp))

def info_cnf(key, val):
    info('[cnf: %s] -> %s' % (cyan(key), val))

def kw_if(f, k, v):
    return {k: v} if f() else {}

def kw_if_not_none(v, k):
    f = lambda: v is not None
    return kw_if(f, k, v)

# --- @throws
def get_env_context():
    valid_envs = {'tst', 'acc', 'prd'}
    default_env = 'tst'
    return get_env_value('GRNE_ENV', default=default_env, valid=valid_envs)

# --- must be called from an exception handler.
def print_exception(kw_pass=None):
    kw_pass = kw_pass or {}
    traceback.print_exception(*exc_info(), **kw_pass)
