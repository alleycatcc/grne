# -*- coding: utf-8 -*-

from functools import partial
import json
from sys import exc_info
from traceback import print_tb

from redis import Redis

from grne.util.util import info_cnf, kw_if_not_none, get_env_value, ident

from config import get as config_get

from acatpy.io.speak import warn, debug
from acatpy.util import alias_kw

config = config_get()

@alias_kw(warnarg='warn')
def redis_client_get_persist(quiet=False, warnarg=True):
    env = partial(get_env_value, quiet=quiet)
    host = env('GRNE_REDIS_PERSIST_HOST', required=False)
    if host is None:
        return 'No GRNE_REDIS_PERSIST_HOST', None
    db = env('GRNE_REDIS_PERSIST_DB', required=False, convert=int)
    port = env('GRNE_REDIS_PERSIST_PORT', required=False, convert=int)
    socket_connect_timeout = env('GRNE_REDIS_PERSIST_SOCKET_CONNECT_TIMEOUT', required=False, convert=int)
    password = env('GRNE_REDIS_PERSIST_PASSWORD', required=False, sensitive=True)
    client = Redis(
        host=host,
        **kw_if_not_none(port, 'port'),
        **kw_if_not_none(db, 'db'),
        **kw_if_not_none(socket_connect_timeout, 'socket_connect_timeout'),
        **kw_if_not_none(password, 'password'),
        socket_timeout=5,
    )

    try:
        client.ping()
    except Exception as e:
        if warnarg:
            warn("Can't connect to 'persist' redis server: %s" % repr(e))
        return e, None
    return None, client

# --- @throws (if client can't connect)
def redis_client_get_app(env_context, quiet=False):
    app = config.user['redis_app'][env_context]
    host = app['host']
    port = app['port']
    db = app['db']
    socket_connect_timeout = app['socket_connect_timeout']
    if not quiet:
        info_cnf('redis_host_app', host)
        info_cnf('redis_port_app', port)
        info_cnf('redis_db_app', db)
        info_cnf('redis_socket_connect_timeout_app',
                 socket_connect_timeout)
    client = Redis(
        host=host,
        port=port,
        db=db,
        socket_connect_timeout=socket_connect_timeout,
    )
    try:
        client.ping()
    except Exception as e:
        warn("Fatal: can't connect to 'app' redis server: %s" % repr(e))
        raise e

    return client

# --- `redis_client` can be None.
# --- if `key_extra` is not a string, it will be run through
# `serialise_key_extra` before it's added on to the key.
# --- if `f` is not None, it will be run on the input in the event of a
# cache miss.
def redis_cache(redis_client, key,
                f, args, kw=None,
                key_extra=None,
                serialise_key_extra=json.dumps,
                serialise_result=ident,
                deserialise_hit=ident,
                expire=None, skip=False,
                extended_return=False):

    kw = kw or {}

    def run():
        return f(*args, **kw)

    def err(e, tag):
        the_type, value, traceback = exc_info()
        warn('Redis error on %s: %s' % (tag, repr(e)))
        print_tb(traceback)

    def go():
        if key_extra is not None:
            if not isinstance(key_extra, str):
                key_extra_s = serialise_key_extra(key_extra)
            else:
                key_extra_s = key_extra
            the_key = '%s:%s' % (key, key_extra_s)
        if redis_client is None:
            debug('redis_cache: UNAVAILABLE (key=%s)' % the_key)
            return False, run()
        if skip:
            debug('redis_cache: SKIP (key=%s)' % the_key)
            return False, run()
        try:
            cached = redis_client.get(the_key)
        except Exception as e:
            err(e, 'get')
            debug('redis_cache: error on get, running function')
            return False, run()
        if cached is not None:
            debug('redis_cache: HIT (key=%s)' % the_key)
            return True, deserialise_hit(cached)
        debug('redis_cache: MISS (key=%s)' % the_key)
        if f is None:
            return False, None
        res = run()
        try:
            redis_client.set(the_key, serialise_result(res))
        except Exception as e:
            err(e, 'set')
            debug('redis_cache: error on set, returning missed result')
            return False, res
        if expire is not None:
            redis_client.expire(the_key, expire)
        return False, res

    is_hit, result = go()
    if extended_return:
        return is_hit, result
    else:
        return result
