# -*- coding: utf-8 -*-

# ------ simple pretty printer with colors.

import re

from acatpy.io.speak import say
from acatpy.colors import yellow

def pretty_print(x, indent=4):
    say(_pprint(x, 0, indent))

def _pprint(x, cur_indent, indent_amount):
    qp = _qprint(x, cur_indent, indent_amount)
    return '\n'.join(qp) if iscoll(qp) else qp

def _qprint(x, cur_indent, indent_amount):
    if isinstance(x, str):
        return(_pyquote(x))

    if not iscoll(x):
        return(str(x))

    if hasitems(x):
        trailing_comma = ','
        abc = cur_indent + indent_amount
        defg = ' ' * cur_indent
        ind = ' ' * abc
        fmt = lambda k, v: (
            ind,
            yellow(_pprint(k, abc, indent_amount)),
            _pprint(v, abc, indent_amount),
            trailing_comma
        )
        comp = ('%s%s: %s%s' % fmt(k, v) for k, v in x.items())
        delim = ('{\n', '\n%s}' % defg)
        return _sprint(comp, delim, '\n', 0)

    if isinstance(x, tuple): delim = ('(', ')')
    else: delim = ('[', ']')
    comp = (_pprint(y, cur_indent, indent_amount) for y in x)
    return _sprint(comp, delim, ', ', 0)

def _sprint(comp, delim, joiner, indent_amount):
    ind = ' ' * indent_amount
    return [('%s%s%s%s' % (delim[0], joiner.join(comp), ind, delim[1]))]

def _qyquote(x):
    return re.sub("'", "\\'", x)

def _pyquote(x):
    if re.match("'", x) and re.match('"', x):
        return "'" + _qyquote(x) + "'"
    if re.match("'", x):
        return '"' + x + '"'
    return "'" + x + "'"

def test():
    pretty_print({
        'een': [3, 4, 5],
        'twee': 2,
        'drie': {
            'drie-een': [6, 7, 8],
            'drie-twee': (1, 2),
            'drie-drie': {
                'vier-een': 'vier-een',
                'vier-twee': [1, 2, (3, 4)],
            },
        },
    })

def iscoll(x):
    if isinstance(x, str): return False
    return bool(getattr(x, '__iter__', None))

def hasitems(x):
    return getattr(x, 'items', None)
