#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class Args(object):
    def __init__(self, args, kw_args=None):
        self.args = args
        self.kw_args = kw_args

    def apply(self, f):
        return f(*self.args, **self.kw_args)

def is_args(x):
    return isinstance(x, Args)

def infinite_range(start=0, step=1):
    i = start
    while True:
        yield i
        i = i + step

def infinite_zip_x(xs):
    return zip(xs, infinite_range(0))

def is_collection(x):
    return hasattr(x, '__iter__') and not isinstance(x, str) and not isinstance(x, dict)

def fmt_results(x):
    return list(x) if is_collection(x) else x

def toets(f, desc_group, args, expect, eq=lambda x, y: x == y, desc=None):
    result=None
    if is_args(args):
        result = args.apply(f)
    else:
        if not is_collection(args): args = [args]
        result = f(*args)
    desc_total = '%s -> %s' % (desc_group, desc) if desc else desc_group
    if not is_collection(expect):
        assert eq(result, expect), desc_total + " (\nexpected: %s\ngot: %s)" % (fmt_results(expect), fmt_results(result))
        return
    for resulted, expected in zip(result, expect):
        assert eq(resulted, expected), desc_total + " (\nexpected: %s\ngot: %s)" % (fmt_results(expected), fmt_results(resulted))
