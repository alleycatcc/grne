from config import get as config_get

conf_main = config_get().main

def format_xslt(xslt, xslt_format):
    py_ext_ns = conf_main['python_extension_ns']
    xslt_format = xslt_format or {}
    return xslt.format(ns=py_ext_ns, **xslt_format)

def make_xslt_extensions(xs):
    py_ext_ns = conf_main['python_extension_ns']
    return [(py_ext_ns, name, the_class) for name, the_class in xs]
