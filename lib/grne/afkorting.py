# -*- coding: utf-8 -*-

import re

import pydash

from acatpy.io.speak import debug

from grne.afkorting_cit_raw import _prepare_expansions_aut_werk, quirks as abbr_cit_quirks

regex = None

_expansions_normal = {
    'aanw.': 'aanwijzend, demonstratief',
    'abs.': 'absoluut (gebruikt), absolutus',
    'abstr.': 'abstract(um/a)',
    'acc.': 'accusativus, -i',
    'acc. resp.': 'accusativus respectus',
    'acc. v.h. inw. obj.': 'accusativus van het inwendig object',
    'AcI': 'accusativus cum infinitivo',
    'AcP': 'accustivus cum participio',
    'act.': 'activum',
    'ad loc.': 'ad locum, ter (aangegeven) plaatse',
    'adj.': 'adjectief (bijvoeglijk naamwoord)',
    'adj. verb.': 'adjectivum verbale',
    'adv.': 'adverbium (bijwoord), adverbia(a)l(e)',
    'advers.': 'adversatief',
    'Aeol.': 'Aeolisch',
    'afær.': 'afæresis (wegval van een klinker aan het begin van een woord)',
    'afh.': 'afhankelijk',
    'afk.': 'afkorting, afgekort',
    'alg.': 'algemeen (NB: niet te verwarren met ‘meestal’)',
    'anal.': '(naar) analogie, analogisch',
    'anastr.': 'anastrofe (terugtrekking van het accent)',
    'anat.': 'anatomie, anatomisch(e)',
    'anders': 'anders, volgens anderen',
    'antw.': 'antwoord(en)',
    'aor.': 'aoristus',
    'apoc.': 'apocope (wegval van een klinker aan het eind van een woord)',
    'app.': 'appendix',
    'Arcad.': 'Arcadisch',
    'arch.': 'archaïsme, archaïsch(e)',
    'Arg.': 'Argivisch',
    'asp.': 'aspiratie, geaspireerd(e)',
    'assim.': 'assimilatie',
    'astr.': 'astrologie, astrologisch(e); astronomie',
    'athem.': 'athematisch',
    'Att.': 'Attisch of atticistisch',
    'attr.': '(als) attribuut, bijvoeglijke bepaling',
    'augm.': 'augment',
    'barb.': 'barbarisme',
    'baryt.': 'barytonon, barytonese',
    'bep.': 'bepaling',
    'bet.': 'betekenis',
    'bevest.': 'bevestigend, bevestiging',
    'bijv.': 'bijvoorbeeld',
    'bisyll.': 'bisyllabisch (tweelettergrepig)',
    'Boeot.': 'Boeotisch',
    'bot.': 'botanisch, plantkundig',
    'bouwk.': 'bouwkundig',
    'caus.': 'causaal',
    'causat.': 'causatief',
    'christ.': 'christelijk',
    'collect.': 'collectivum',
    'comp.': 'comparativus (vergrotende trap); comparationis (van vergelijking); comparatief, -tieve',
    'compl.': 'complement, aanvulling',
    'compos.': 'compositum, -a, samengesteld, samenstelling(en)',
    'concess.': 'concessief',
    'concr.': 'concretum, concreet',
    'cond.': 'conditioneel',
    'conj.': 'conjunctivus; conjunctum',
    'conject.': 'conjectuur, geconjiceerd',
    'conjug.': 'conjugatie (vervoeging)',
    'cons.': 'consonant (medeklinker)',
    'consec.': 'consecutief',
    'constr.': 'constructie, geconstrueerd',
    'contr.': 'contractie, samengetrokken, samentrekking',
    'corr.': 'correctie, gecorrigeerd',
    'correl.': 'correlatie(f)',
    'dat.': 'dativus',
    'decl.': 'declinatie (verbuiging)',
    'demin.': 'deminutivum (verkleinwoord)',
    'dent.': 'dentaal',
    'desid.': 'desiderativum',
    'dial.': 'dialect(en)',
    'dift.': 'diftong, tweeklank',
    'dir.': 'direct(e)',
    'dissim.': 'dissimilatie',
    'd.m.v.': 'door middel van',
    'doc.': 'document',
    'doelbep.': 'doelbepaling',
    'Dor.': 'Dorisch',
    'dual.': 'dualis',
    'dub.': 'dubieus, twijfelachtig',
    'dubb.': 'dubbel(e)',
    'dubbelz.': 'dubbelzinnig(e)',
    'd.w.z.': 'dat wil zeggen',
    'ed(d).': 'editie(s), uitgave(s)',
    'eig.': 'eigenlijk, in eigenlijke zin',
    'elis.': 'elisie',
    'encl.': 'enclitisch, encliticum (klein woord zonder eigen accent dat met het voorafgaande woord één accenteenheid vormt)',
    'entom.': 'entomologie, insectenkunde',
    'enz.': 'enzovoort',
    'ep.': 'episch',
    'epexeg.': 'epexegetisch',
    'epith.': 'epitheton',
    'ethn.': 'ethnicon (volkerennaam)',
    'etym.': 'etymologie, etymologisch(e)',
    'eufem.': 'eufemisme, eufemistisch(e)',
    'f.': 'femininum (vrouwelijk)',
    'fam.': 'familiair, familie',
    'filos.': 'filosofie, filosofisch, filosoof',
    'fin.': 'finaal',
    'fin. inf.': 'finale infinitivus',
    'f.l.': 'falsa lectio (foutieve lezing van een handschrift)',
    'freq.': 'frequentatief, frequentativum',
    'fut.': 'futurum',
    'fut. perf.': 'futurum van de perfectumstam',
    'gebr.': 'gebruikt, gebruikelijk, gebruikswijze(n)',
    'gen.': 'genitivus',
    'geneesk.': 'geneeskunde, geneeskundig',
    'geogr.': 'geografie, geografisch',
    'geometr.': 'geometrisch',
    'getal': 'getal',
    'gew.': 'gewoonlijk, over het algemeen',
    'gramm.': 'grammaticaal, -ale',
    'heterocl.': 'heteroclitisch (van verschillende stammen)',
    'hfdtelw.': 'hoofdtelwoord',
    'hist.': 'historie, historisch',
    'Hom.': 'Homerisch, bij Homerus',
    'homon.': 'homoniem',
    'hs(s).': 'handschrift(en)',
    'hypoth.': 'hypothetisch',
    'ibid.': 'ibidem, op dezelfde plaats',
    'ichth.': 'ichtyologisch, de bestudering van vissen betreffend',
    'id.': 'idem, dezelfde (betekenis)',
    'idiom.': 'idiomatisch',
    'iem.': 'iemand',
    'iets': 'iets',
    'i.h.b.': 'in het bijzonder',
    'imperat.': 'imperativus',
    'imperf.': 'imperfectum',
    'indecl.': 'indeclinabel (onverbuigbaar)',
    'indef.': 'indefiniet (onbepaald)',
    'indic.': 'indicativus',
    'indir.': 'indirect',
    'inf.': 'infinitivus, infinitiefconstructie',
    'instr.': 'instrument, instrumentaal, instrumentalis',
    'intens.': 'intensief, intensivum',
    'interj.': 'interjectie (tussenwerpsel)',
    'interpr.': 'interpretatie',
    'interrog.': 'interrogatief, vragend, vraagwoord, -zin',
    'intrans.': 'intransitief (onovergankelijk)',
    'inw.': 'inwendig',
    'Ion.': 'Ionisch',
    'iron.': 'ironisch',
    'iter.': 'iterativus, iteratief',
    'jur.': 'juridisch, rechtskundig, wettig',
    'klass.': 'klassiek',
    'kom.': 'komisch(e), (in) komedie',
    'koppelwerkw.': 'koppelwerkwoord',
    'Kret.': 'Kretenzisch',
    'lab.': 'labiaal',
    'Lac.': 'Laconisch',
    'Lat.': 'Latijn',
    'later Gr.': 'later Grieks',
    'l.c., ll.cc.': 'loco citato, locis citatis, op de geciteerde plaats(en)',
    'lect.': 'lectio, lezing (van een handschrift)',
    'Lesb.': 'Lesbisch',
    'lett.': 'letterlijk',
    'lidw.': 'lidwoord',
    'loc.': 'locativus',
    'log.': 'logisch, logica',
    'lyr.': 'lyriek, lyrisch',
    'm.': 'masculinum (mannelijk)',
    'marit.': 'maritiem, zee-',
    'med.': 'medium, media(a)l(e)',
    'med.-pass.': 'medio-passief',
    'metath.': 'metathesis (omzetting van klanken)',
    'meton.': 'metonymie, metonymisch',
    'milit.': 'militair',
    'm.n.': 'met name, vooral',
    'msch.': 'misschien',
    'muz.': 'muziek, muzikaal',
    'myth.': 'mythisch, mythologie',
    'n.': 'neutrum (onzijdig)',
    'N.B.': 'nota bene',
    'neg.': 'in negatieve zin',
    'neol.': 'neologisme',
    'Ned.': 'Nederlands',
    'nom.': 'nominativus',
    'obj.': 'object, objectivus',
    'obl.': 'obliquus',
    'obsc.': 'obsceen, in obscene zin',
    'onafh.': 'onafhankelijk',
    'ondersch.': 'onderschikking, ondergeschikt',
    'onomat.': 'onomatopee, onomatopeïsch',
    'onpers.': 'onpersoonlijk(e)',
    'onreg.': 'onregelmatig(e)',
    'ontk.': 'ontkenning, ontkennend(e)',
    'oorspr.': 'oorsprong, oorspronkelijk(e)',
    'opm.': 'opmerking',
    'opp.': 'oppositie, tegenovergesteld',
    'opt.': 'optativus',
    'ornith.': 'ornithologisch, vogelkundig',
    'oud': 'oud',
    'Oudatt.': 'Oudattisch',
    'overdr.': 'overdrachtelijk',
    'oxyt.': 'oxytonon',
    'p(p).': 'pagina(’s)',
    'parod.': 'parodie, parodiërend',
    'paroxyt.': 'paroxytonon',
    'partik.': 'partikel(s)',
    'partit.': 'partitivus',
    'pass.': 'passivum',
    'patron.': 'patroniem (vadersnaam)',
    'pejor.': 'pejoratief (geringschattend)',
    'perf.': 'perfectum',
    'perifr.': 'perifrase, perifrastisch',
    'perisp.': 'perispomenon',
    'pers.': 'persoon, persoonlijk',
    'personif.': 'personificatie, gepersonifieerd',
    'pleon.': 'pleonasme, pleonastisch',
    'plqperf.': 'plusquamperfectum',
    'plur.': 'pluralis, meervoud',
    'poët.': 'poëtisch, dichterlijk',
    'pos.': 'in positieve zin',
    'posit.': 'positivus (stellende trap)',
    'poss.': 'possessief, bezittelijk',
    'post-hom.': 'post-homerisch',
    'post-klass.': 'post-klassiek',
    'postpos.': 'in postpositie, postpositief, achterop geplaatst',
    'pot.': 'potentialis',
    'praes.': 'praesens',
    'pred.': 'predicaat, predicatief, -ieve',
    'pref.': 'prefix (voorvoegsel)',
    'pregn.': 'pregnant',
    'prep.': 'prepositie (voorzetsel)',
    'prep.bep.': 'prepositiebepaling',
    'prev.': 'preverbium (voorvoegsel bij werkwoord)',
    'priv.': 'privatief',
    'procl.': 'proclitisch, procliticum (klein woord zonder eigen accent dat met het volgende woord één accenteenheid vormt)',
    'proëm.': 'proëmium (inleiding)',
    'prolept.': 'prolepsis, proleptisch',
    'pron.': 'pronomen, pronominaal (voornaamwoord(elijk))',
    'proparoxyt.': 'proparoxytonon',
    'properisp.': 'properispomenon',
    'propos.': 'propositie',
    'ptc.': 'participium (deelwoord)',
    'ptc. conj.': 'participium conjunctum',
    'ps.sigm.': 'pseudo-sigmatisch(e)',
    'rangtelw.': 'rangtelwoord',
    'recipr.': 'reciproque, wederkerig',
    'redupl.': 'reduplicatie, geredupliceerd',
    'refl.': 'reflexief, wederkerend',
    'regelm.': '(op) regelmatig(e wijze)',
    'rel.': 'relativum (betrekkelijk voornaamwoord), relatief, -ieve',
    'relig.': 'religie, religieus, -euze',
    'resp.': 'respectus',
    'ret.': 'retorica, retorisch',
    's.v.': 'sub voce (bij het woord)',
    'sc.': 'scilicet (dat wil zeggen, namelijk, vul aan)',
    'seks.': 'seksue(e)l(e), uit de seksuele sfeer',
    'sigm.': 'sigmatisch(e)',
    'sim.': 'vergelijkbaar, op vergelijkbare wijze',
    'sing.': 'singularis (enkelvoud)',
    'soms': 'soms',
    'spec.': 'speciaal',
    'spir.': 'spiritus (aspiratieteken)',
    'spreekw.': 'spreekwoord(elijk)',
    'stam': 'stam',
    'stamaor.': 'stamaoristus',
    'strat.': 'strategisch, krijgskunde, krijgskundig(e)',
    'subj.': 'subject, onderwerp, onderwerps-',
    'subst.': 'substantivum, gesubstantiveerd',
    'suffix': 'suffix',
    'superl.': 'superlativus (overtreffende trap)',
    'suppl.': 'suppletief (aangevulde vorm uit ander paradigma)',
    'syll.': 'syllabe (lettergreep), syllabisch',
    'sync.': 'syncope (uitstoting van een klank midden in een woord)',
    'synon.': 'synoniem',
    'Syr.': 'Syracusaans',
    'techn.': 'techniek, technisch(e)',
    'telw.': 'telwoord',
    'temp.': 'temporeel',
    'term': 'term',
    'them.': 'thematisch(e)',
    'Thess.': 'Thessalisch',
    'tmes.': 'tmesis (een samengesteld woord wordt gescheiden door een ertussen geplaatst woord)',
    'trag.': 'tragisch, in tragedie',
    'trans.': 'transitief (overgankelijk)',
    'uitbr.': '(bij) uitbreiding',
    'uitdr.': 'uitdrukking(swijze)',
    'uitr.': '(als) uitroep',
    'v.': 'van',
    'var.': 'variant(en)',
    'vel.': 'velaar, velair',
    'verb.': 'verbaal, -ale (werkwoordelijk)',
    'vergel.': 'vergelijking',
    'verl.': 'verlenging',
    'veronderst.': 'verondersteld(e)',
    'vert.': 'vertaling, vertaald',
    'vgl.': 'vergelijk',
    'v.l.': 'varia lectio (afwijkende lezing)',
    'vlgs.': 'volgens',
    'vocat.': 'vocativus',
    'voegw.': 'voegwoord, conjunctie',
    'vol(l).': 'volumen (deel), volumina (delen)',
    'voor': 'voor',
    'voorafg.': 'voorafgaand(e)',
    'vrl.': 'vrouwelijk(e)',
    'werkw.': 'werkwoord',
    'wetensch.': '(in) wetenschappelijk(e zin)',
    'wisk.': 'wiskundig',
    'wortel': 'wortel',
    'wsch.': 'waarschijnlijk(erwijze)',
    'zelden': 'zelden',
    'zelfst. werkw.': 'zelfstandig werkwoord',
    'zie': 'zie',
}

_expansions_aut, _expansions_cit = _prepare_expansions_aut_werk()

# --- note, use array form even for a single key, because of the '.' in
# the keys.
def _expand(expansions, path, default=None):
    return pydash.get(expansions, path, default=None)

def expand_abbrev_normal(abbr):
    return _expand(_expansions_normal, [abbr])

def expand_citation(aut, werk, plaats):
    def format_result(res):
        rc = (res.get(_, '') for _ in ('aut', 'werk', 'plaats'))
        return '___'.join(rc)

    debug('before quirks aut=%s werk=%s plaats=%s' % (aut, werk, plaats))
    aut, werk, plaats = abbr_cit_quirks(aut, werk, plaats)
    debug('after quirks aut=%s werk=%s plaats=%s' % (aut, werk, plaats))
    warnings = []
    result = None
    if not aut:
        warnings.append("aut is empty")
        return warnings, None
    if not werk:
        aut_only_x = _expand(_expansions_aut, [aut])
        aut_only_np_x = _expand(_expansions_aut, [remove_period(aut)])
        if not plaats:
            aut_only = aut_only_x or aut_only_np_x
            if not aut_only:
                warnings.append("Couldn't expand aut=%s" % aut)
                return warnings, None
            result = {'aut': aut_only}
        else:
            if not aut_only_x:
                warnings.append("Couldn't expand aut=%s" % aut)
                return warnings, None
            result = {'aut': aut_only_x, 'plaats': plaats}
        return warnings, format_result(result)
    aut_werk = _expand(_expansions_cit, [aut, werk])
    aut_werk_np = _expand(_expansions_cit, [aut, remove_period(werk)])
    if not plaats:
        the_aut_werk = aut_werk or aut_werk_np
        if the_aut_werk is None:
            warnings.append("Couldn't expand aut=%s, werk=%s" % (aut, werk))
            return warnings, None
        aut_x, werk_x = the_aut_werk
        result = {'aut': aut_x, 'werk': werk_x}
    else:
        if aut_werk is None:
            warnings.append("Couldn't expand aut=%s, werk=%s" % (aut, werk))
            return warnings, None
        aut_x, werk_x = aut_werk
        result = {'aut': aut_x, 'werk': werk_x, 'plaats': plaats}
    return warnings, format_result(result)

# --- be sure to use re.X
def regex_afkortingen():
    global regex
    if regex: return regex
    keys = sorted(_expansions_normal.keys(), key=len, reverse=True)

    # --- e.g. 'getal': 'getal'
    keys = pydash.reject(keys, lambda k: _expansions_normal[k] == k)

    def ensure_boundary(s):
        return r'\b %s' % s

    rs = (re.escape(_) for _ in keys)
    rss = (ensure_boundary(_) for _ in rs)
    regex = r'%s' % '|'.join(rss)
    return regex

def remove_period(x):
    return re.sub(r'\.$', '', x)
