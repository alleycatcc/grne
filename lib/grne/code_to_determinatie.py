# -*- coding: utf-8 -*-

CODE_TO_DETERMINATIE = (
    {
        # --- 1 and 2: part of speech
        'name': 'pos',
        # --- special case
        'idx': -1,

        'data': {
            'a-': 'adj.',
            'ae': 'adj.',
            'c-': 'voegw.',
            'd-': 'adv.',
            'dd': 'aanw. adv.',
            'de': 'adv.',
            'di': 'adv. interrog.',
            'dr': 'adv. rel.',
            'dx': 'adv. indef.',
            'g-': 'partik.',
            'gm': 'modaal partik.',
            'i-': 'interj.',
            'm-': 'telw.',
            'n-': 'subst.',
            'ne': 'eigennaam',
            'p-': 'pron.',
            'pa': 'lidw.',
            'pc': 'pron. recipr.',
            'pd': 'aanw. pron.',
            'pi': 'pron. interrog.',
            'pk': 'pron. refl.',
            'pp': 'pers. pron.',
            'pr': 'pron. rel.',
            'ps': 'pron. poss.',
            'px': 'pron. indef.',
            'r-': 'prep.',
            'v-': 'werkw.',
            'vc': 'koppelww.',
            'y-': 'geometr. figuur',
        }
    },

    {
        # --- 6: mood
        'name': 'mood',
        'idx': 5,
        'data': {
            'i': 'indic.',
            's': 'conj.',
            'o': 'opt.',
            'm': 'imperat.',
            'n': 'inf.',
            'p': 'ptc.',
        },
    },

    {
        # --- 5: tense
        'name': 'tense',
        'idx': 4,
        'data': {
            'p': 'praes.',
            'i': 'imperf.',
            'a': 'aor.',
            'r': 'perf.',
            'l': 'plqperf.',
            'f': 'fut.',
            't': 'fut. perf.',
        },
    },

    {
        # --- 7: voice
        'name': 'voice',
        'idx': 6,
        'data': {
            'a': 'act.',
            'm': 'med.',
            'p': 'pass.',
            'e': 'med.-pass.',
        },
    },

    {
        # --- 3: person
        'name': 'person',
        'idx': 2,
        'data': {
            '1': '1',
            '2': '2',
            '3': '3',
        },
    },

    {
        # --- 10: degree
        'name': 'degree',
        'idx': 9,
        'data': {
            'c': 'comp.',
            's': 'superl.',
        },
    },

    {
        # --- 9: case
        'name': 'case',
        'idx': 8,
        'data': {
            'n': 'nom.',
            'g': 'gen.',
            'd': 'dat.',
            'a': 'acc.',
            'v': 'vocat.',
        },
    },

    {
        # --- 8: gender
        'name': 'gender',
        'idx': 7,
        'data': {
            'm': 'm.',
            'f': 'f.',
            'n': 'n.',
            'c': 'm. en f.',
        },
    },

    {
        # --- 4: number
        'name': 'number',
        'idx': 3,
        'data': {
            's': 'sing.',
            'p': 'plur.',
            'd': 'dual.',
        },
    },
)

def code_to_determinatie(code):
    if len(code) != 10:
        return 'Code not 10 chars long (%s)' % code, None

    unknown = 'unknown'

    res = []
    for block in CODE_TO_DETERMINATIE:
        idx, data = block['idx'], block['data']
        if idx == -1:
            # --- special case
            p = code[0:2]
        else:
            p = code[idx]
            if not p:
                return 'Error with code %s in block %s' % (code, idx), None
        if p == '-':
            continue
        res.append(data[p] or unknown)
    return ' '.join(res)
