import lxml.html

from acatpy.io.speak import warn
from acatpy.xml import xslt_transform

'''
def get_xslt():
    config = config_get()
    xslt_file = config.main['stylesheet_to_html']
    py_ext_ns = config.main['python_extension_ns']
    xslt = slurp(xslt_file)
    return re.sub('__NS__', py_ext_ns, xslt)
'''

def lemma_to_html(xml, xslt=None, return_string=True, parse_xml=True):
    xslt = xslt or get_xslt()
    extensions = []
    err, transformed = xslt_transform(xml, xslt, parse_xml=parse_xml, extensions=extensions)
    if err:
        warn("Error with transform: %s" % err)
        return err, None
    if return_string:
        return None, lxml.html.tostring(transformed, encoding=str)
    return None, transformed

def get_xslt():
    # --- we don't need extensions or format substitutions; if we ever do,
    # we have to think of a way to escape or replace the {} used in the
    # stylesheet.

    return '''<?xml version="1.0"?>

    <xsl:stylesheet version="1.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

      <xsl:output method="xml" encoding="UTF-8" omit-xml-declaration="yes" indent="no"/>

      <xsl:template match="contLem">
        <xsl:apply-templates select="lem | escLem | verwLem | xlLem"/>
      </xsl:template>

      <xsl:template match="niv123|nivABC|xlNivABC|xlNiv123|xlNiv-3|xlNiv-4|xlNiv-5">
        <!--
           A complicated way to turn list-like nodes into <ol> with a class
           attribute.

           Decide numbering paradigm in CSS.

           position() isn't good enough, not only because of whitespace nodes,
           but also because of, for example, werkV at the top of the list. I
           needed something like <xsl:number> and the following worked.

           The little dance with value-of is necessary in this approach
           because XSLT complains if it can't already match opening & closing
           tags.
         -->

        <xsl:variable name="tagName" select="local-name()"/>

        <xsl:if test="1 = count(preceding-sibling::*[local-name()=$tagName]) + 1">
          <xsl:value-of disable-output-escaping="yes" select="concat(
            '&lt;',
              'ol class=',
              '&quot;',
                $tagName,
              '&quot;',
            '&gt;'
            )"/>
        </xsl:if>

        <li>
          <xsl:apply-templates/>
        </li>

        <!-- </ol> -->
        <xsl:if test="0 = count(following-sibling::*[local-name()=$tagName])">
          <xsl:value-of disable-output-escaping="yes" select="'&lt;/ol&gt;'"/>
        </xsl:if>

      </xsl:template>

      <xsl:template match="lem|verwLem|escLem|xlLem">
        <div>
          <xsl:attribute name="class">
            <xsl:value-of select="local-name()"/>
          </xsl:attribute>
          <xsl:apply-templates/>
        </div>
      </xsl:template>

      <xsl:template match="vorm|xlBet|bet|escBet|cit|citIsgel|isgel|isgelCit|citG|citNV|vertM|gebrW|morfI|etym|toel|hulpI|werkV|hoofdW|verw|aut|werk|plaats|kruisVerw|tmp|i|r|s|b|link">
        <div>
          <xsl:attribute name="class">
            <xsl:value-of select="local-name()"/>
          </xsl:attribute>
          <xsl:for-each select="@*">
            <!-- after @migrate: remove 'target' from <link/> -->
            <!--xsl:if test="name() != 'target'"-->
              <xsl:attribute name="{name()}">
                <xsl:value-of select="."/>
              </xsl:attribute>
            <!--/xsl:if-->
          </xsl:for-each>
          <xsl:apply-templates/>
        </div>
      </xsl:template>

      <xsl:template match="*">
        <xsl:element name="{local-name()}">
          <xsl:for-each select="@*">
            <xsl:attribute name="{name()}">
              <xsl:value-of select="."/>
            </xsl:attribute>
          </xsl:for-each>
          <xsl:apply-templates/>
        </xsl:element>
      </xsl:template>
    </xsl:stylesheet>
    '''
