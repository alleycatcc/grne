<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet 
    version="2.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:f="http://functions.grne.org" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema">

    <xsl:function name="f:dotornot" as="xs:string">
        <xsl:param name="strng"/>
        <xsl:variable name="s">
            <xsl:choose>
                <!-- when there is a dot already do not add an extra dot -->
                <xsl:when test="matches($strng, '\.\s*[,:;]?\s*$')">
                    <xsl:value-of select="replace($strng, '\.\s*[,:;]?\s*$', '.')"/>
                </xsl:when>
                <xsl:when test="matches($strng, '[,;:]\s*$')">
                    <xsl:value-of select="replace($strng, '.\s*$', '.')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="replace($strng, '\s+$', '.')"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:sequence select="$s"/>
    </xsl:function>

   <xsl:function name="f:siblings" as="xs:string">
        <xsl:param name="nodes"/>
        <xsl:variable name="s">
            <xsl:for-each select="$nodes">
                <xsl:choose>
                    <xsl:when test="((local-name() = 'gebrW') and @remove_this)">
                        <xsl:value-of select="'XgebrW'"/>
                    </xsl:when>
                    <xsl:when test="((local-name() = 'cit') and @remove_this)">
                        <xsl:value-of select="'Xcit'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="local-name()"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </xsl:variable>
        <xsl:sequence select="$s"/>
    </xsl:function>

</xsl:stylesheet>
