<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
  <xsl:output method="xml"  indent="no" omit-xml-declaration="yes" />
  <xsl:template match="/">
    <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE contLem SYSTEM "../../hulpdocumenten/dtd/lemma.dtd">
</xsl:text>
  <xsl:apply-templates select="node()"/>
  </xsl:template>

  <xsl:template match="*|text()|comment()|@*">
      <xsl:copy>
        <xsl:apply-templates select="@* | node()"/>
      </xsl:copy>
  </xsl:template>
  
  <xsl:template match="processing-instruction()">
    <xsl:copy-of select="."/>
    <xsl:text>&#x0A;</xsl:text>
  </xsl:template>
  
  <xsl:template match="//gebrW">
    <xsl:copy>
      <xsl:if test="(following-sibling::*[1][self::cit] and following-sibling::*[2][self::cit])">
        <xsl:attribute name="check_this_again">
          <xsl:value-of select="'5'"/>
        </xsl:attribute>
      </xsl:if>
       <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>
  
</xsl:stylesheet>
