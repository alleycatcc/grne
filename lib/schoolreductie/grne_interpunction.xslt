<xsl:stylesheet
    xmlns:f="http://functions.grne.org"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">


    <xsl:import href="grne.functions.xsl"/>
    <xsl:output method="xml"  indent="no" omit-xml-declaration="yes" />

    <xsl:template match="/">
        <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE contLem SYSTEM "../../hulpdocumenten/dtd/lemma.dtd">
</xsl:text>
        <xsl:apply-templates select="node()"/>
    </xsl:template>

    <xsl:template match="*[@remove_this]"/>

    <xsl:template match="* | text() | comment() | @*">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="* | text() | comment() | @*" mode="special">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()" mode="special"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="processing-instruction()">
        <xsl:copy-of select="."/>
        <xsl:text>&#x0A;</xsl:text>
    </xsl:template>

    <!-- vertM ends with :\s* following (not removed) is vertM, gebrW or nothing. Replace : with ; ; or dotornot. Case 1, 2, 3 -->
    <xsl:template match="text()[ancestor::vertM[not(@remove_this)][descendant::text()[normalize-space(.)!=''][last()]=current()]][matches(current(), ':\s*$')]">
        <xsl:variable name="prnt" select="ancestor::vertM"/>
        <xsl:variable name="sibs" select="f:siblings($prnt/following-sibling::*)"/>
        <xsl:variable name="found"><xsl:value-of select="concat('[', ., ']')"/></xsl:variable>
        <xsl:choose>
            <xsl:when test="matches($sibs, '^(Xcit|XgebrW)(Xcit|XgebrW)*vertM')">
                <xsl:value-of select="replace(., ':\s*$', '; ')"/>
            </xsl:when>
            <xsl:when test="matches($sibs, '^(Xcit|XgebrW)(Xcit|XgebrW)*gebrW')">
                <xsl:value-of select="replace(., ':\s*$', '; ')"/>
            </xsl:when>
            <xsl:when test="matches($sibs, '^(Xcit|XgebrW)(Xcit|XgebrW)*$')">
                <xsl:value-of select="f:dotornot(.)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- vertM ends with ,\s* following (not removed) is vertM, gebrW or nothing, , is replaced with ; ; or dotornot. Case 1b, 2b, 3b.  -->
    <xsl:template match="text()[ancestor::vertM[not(@remove_this)][descendant::text()[normalize-space(.)!=''][last()]=current()]][matches(current(), ',\s*$')]">
        <xsl:variable name="prnt" select="ancestor::vertM"/>
        <xsl:variable name="sibs" select="f:siblings($prnt/following-sibling::*)"/>
        <xsl:variable name="found"><xsl:value-of select="concat('[', ., ']')"/></xsl:variable>
        <xsl:choose>
            <xsl:when test="matches($sibs, '^(Xcit|XgebrW)(Xcit|XgebrW)*vertM')">
                <xsl:value-of select="replace(., ',\s*$', '; ')"/>
            </xsl:when>
            <xsl:when test="matches($sibs, '^(Xcit|XgebrW)(Xcit|XgebrW)*gebrW')">
                <xsl:value-of select="replace(., ',\s*$', '; ')"/>
            </xsl:when>
            <xsl:when test="matches($sibs, '^(Xcit|XgebrW)(Xcit|XgebrW)*$')">
                <xsl:value-of select="f:dotornot(.)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- vertM that ends in ; no sibling, replace ; with dotornot Case 3c  -->
    <xsl:template match="text()[ancestor::vertM[not(@remove_this)][descendant::text()[normalize-space(.)!=''][last()]=current()]][matches(current(), ';\s*$')]">
        <xsl:variable name="prnt" select="ancestor::vertM"/>
        <xsl:variable name="sibs" select="f:siblings($prnt/following-sibling::*)"/>
        <xsl:variable name="found"><xsl:value-of select="concat('[', ., ']')"/></xsl:variable>
        <xsl:choose>
            <xsl:when test="matches($sibs, '^(Xcit|XgebrW)(Xcit|XgebrW)*$')">
                <xsl:value-of select="f:dotornot(.)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!--
        @arie: the following template is changed a bit. The case </toel> </vertM> is now treated properly (that's why we need
        matches(current(), '^\s$')
        The newlines are there for readability. Matter of taste.
    -->

    <!-- vertM ends with NOT ,.:; and spaces, following sibling is vertM, gebrW or nothing. Case 1d, 2d, 3d -->
    <xsl:template match="text()
    [ancestor::vertM[not(@remove_this)]
        [descendant::text()
            [last()]=current()]]
    [matches(current(), '^\s$')
    or matches(normalize-space(current()), '[^,.:;]\s*$')]
    ">
        <xsl:variable name="prnt" select="ancestor::vertM"/>
        <xsl:variable name="sibs" select="f:siblings($prnt/following-sibling::*)"/>
        <xsl:variable name="found"><xsl:value-of select="concat('[', ., ']')"/></xsl:variable>
        <xsl:choose>
            <xsl:when test="matches($sibs, '^(Xcit|XgebrW)(Xcit|XgebrW)*vertM')">
                <xsl:value-of select="replace(., '(.)\s*$','$1; ')"/>
            </xsl:when>
            <xsl:when test="matches($sibs, '^(Xcit|XgebrW)(Xcit|XgebrW)*gebrW')">
                <xsl:value-of select="replace(., '(.)\s*$','$1; ')"/>
            </xsl:when>
            <xsl:when test="matches($sibs, '^(Xcit|XgebrW)(Xcit|XgebrW)*$')">
                <xsl:value-of select="f:dotornot(.)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- gebrW ends with :\s* Following siblings are gebrW vertM or nothing : is replaced with ; ; or dotornot Case 4 5 6  -->
    <xsl:template match="text()[ancestor::gebrW[not(@remove_this)][descendant::text()[normalize-space(.)!=''][last()]=current()]][matches(current(), ':\s*$')]">
        <xsl:variable name="prnt" select="ancestor::gebrW"/>
        <xsl:variable name="sibs" select="f:siblings($prnt/following-sibling::*)"/>
        <xsl:variable name="found"><xsl:value-of select="concat('[', ., ']')"/></xsl:variable>
        <xsl:choose>
            <xsl:when test="matches($sibs, '^(Xcit|XgebrW)(Xcit|XgebrW)*gebrW')">
                <xsl:value-of select="replace(., ':\s*$', '; ')"/>
            </xsl:when>
            <xsl:when test="matches($sibs, '^(Xcit|XgebrW)(Xcit|XgebrW)*vertM')">
                <xsl:value-of select="replace(., ':\s*$', '; ')"/>
            </xsl:when>
            <xsl:when test="matches($sibs, '^(Xcit|XgebrW)(Xcit|XgebrW)*$')">
                <xsl:value-of select="f:dotornot(.)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- gebrW ends with NOT ,:; and spaces following not removed is gebrW space is replaced with dotornot Case 6d -->
    <xsl:template match="text()[ancestor::gebrW[not(@remove_this)][descendant::text()[normalize-space(.)!=''][last()]=current()]][matches(current(), '[^,:;\s]\s*$')]">
        <xsl:variable name="prnt" select="ancestor::gebrW"/>
        <xsl:variable name="sibs" select="f:siblings($prnt/following-sibling::*)"/>
        <xsl:variable name="found"><xsl:value-of select="concat('[', ., ']')"/></xsl:variable>
        <xsl:choose>
            <xsl:when test="matches($sibs, '^(Xcit|XgebrW)(Xcit|XgebrW)*gebrW')">
                <xsl:value-of select="replace(., '(.)\s*$','$1; ')"/>
            </xsl:when>
            <xsl:when test="matches($sibs, '^(Xcit|XgebrW)(Xcit|XgebrW)*$')">
                <xsl:value-of select="f:dotornot(.)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

<!-- gebrW that ends in ; no sibling, replace ; with dotornot Case 6c -->
    <xsl:template match="text()[ancestor::gebrW[not(@remove_this)][descendant::text()[normalize-space(.)!=''][last()]=current()]][matches(current(), ';\s*$')]">
        <xsl:variable name="prnt" select="ancestor::gebrW"/>
        <xsl:variable name="sibs" select="f:siblings($prnt/following-sibling::*)"/>
        <xsl:variable name="found"><xsl:value-of select="concat('[', ., ']')"/></xsl:variable>
        <xsl:choose>
            <xsl:when test="matches($sibs, '^(Xcit|XgebrW)(Xcit|XgebrW)*$')">
                <xsl:value-of select="f:dotornot(.)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- cit ends with ; following not removed is nothing ; dotornot is applied Case 7 -->
    <xsl:template match="text()[ancestor::cit[not(@remove_this)][descendant::text()[normalize-space(.)!=''][last()]=current()]][matches(., ';\s*$')]">
        <xsl:variable name="prnt" select="ancestor::cit"/>
        <xsl:variable name="sibs" select="f:siblings($prnt/following-sibling::*)"/>
        <xsl:variable name="found"><xsl:value-of select="concat('[', ., ']')"/></xsl:variable>
        <xsl:choose>
            <xsl:when test="matches($sibs, '^(Xcit|XgebrW)(Xcit|XgebrW)*$')">
                <xsl:value-of select="f:dotornot(.)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
