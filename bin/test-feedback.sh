#!/usr/bin/env bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")

host='http://localhost:8080'

. "$bindir"/functions.bash

USAGE="Usage: $0"

get-data () {
    echo '{"message": "hello", "name": "someone", "email": "someone@somewhere.nl"}'
}

go () {
    mci
    mcb curl
    mcb   -X POST
    mcb   -H 'Content-Type: application/json'
    mcb   --data "$(printf '{"data": %s}' "$(get-data)")"
    mcb   "$host"/api/feedback/mail
    mcg
}

fun go
