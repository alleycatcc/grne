#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import proj_paths # noqa

import json
import os
import sys

from acatpy.io.io import cmd

# --- usage: with-env.py /path/to/env.json cmd [args ...]

with open(sys.argv[1], 'r') as fh:
    env = json.loads(fh.read())
    for k, v in env.items():
        os.environ[k] = v
    cmd(sys.argv[2:], die=True)
