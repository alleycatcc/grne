#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import proj_paths # noqa

from functools import partial
from hashlib import sha256
import json
from os.path import abspath
from sys import argv
import sys

from lxml import etree

from acatpy.io.speak import error, info, warn

from grne.util.util import (
    parse_xml,
    read_xml_from_file,
)
from grne.util.util_redis import redis_cache, redis_client_get_persist

to_string = partial(etree.tostring, encoding='unicode')

def usage():
    script_name = abspath(argv[0])
    return '''Usage:

%s { -h | --help }
%s filename''' % (script_name, script_name)

def process_args():
    if len(argv) != 2:
        error(usage())
        sys.exit(1)
    _, arg = argv
    if arg == '-h' or arg == '--help':
        info(usage())
        sys.exit(0)
    return arg

def go(filename):
    xml = read_xml_from_file(filename)
    parsed = parse_xml(xml, throw=True)
    t2_input = to_string(parsed)
    t2_input_bytes = t2_input.encode()
    hashed_input = sha256(t2_input_bytes).hexdigest()
    # --- @todo repeated from util_redis
    t2_key = 't2:t2_0:%s' % hashed_input
    info('t2 key: %s' % t2_key)

    err, redis_client = redis_client_get_persist()
    if redis_client is None:
        die("Couldn't get redis client, check connection params/try restarting redis. Error was: " + err)

    try:
        is_cache_hit, transformed = redis_cache(
            redis_client, 't2:t2_0', None, (),
            key_extra=hashed_input,
            serialise_result=to_string,
            deserialise_hit=etree.fromstring,
            extended_return=True,
        )
    except Exception as e:
        warn('Error accessing redis cache (filename=%s) (stacktrace follows)' % filename)
        print_exception()
        raise e
    if transformed is None:
        warn('T2 cache not found: (file=%s t2-cache-key=%s)' %
            (msg_filename, 't2:t2_0:' + hashed_input))
    print(to_string(transformed))

filename = process_args()
go(filename)
