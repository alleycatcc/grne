#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import time

import proj_paths # noqa

from lxml import etree
import pydash

from grne.build.make_index import (
    get_data_lemma,
    sort_lemmata,
)
from grne.util.util import get_xml_from_file
from acatpy.speak import green
from acatpy.io.speak import info, warn

letters = [
    'beta', 'gamma', 'delta', 'zeta',
    'theta', 'iota', 'kappa', 'labda',
    'mu', 'nu', 'xi', 'pi',
    'rho', 'sigma', 'tau', 'upsilon',
    'phi', 'chi', 'psi',
]

output_file_all = '/tmp/__DATE__-lemmalijst-all.txt'
output_file_zonder_verw = '/tmp/__DATE__-lemmalijst-zonder-verw.txt'

'''
(a) een alfabetische lijst leveren die van elk lemma (inclusief verwijslemmata) alleen
het eerste volledige hoofdwoord bevat? (Alle woorden van elkaar gescheiden door een Enter)
(b) een zelfde lijst maar dan zonder alle verwijslemmata?
'''

'''
(Je kunt dus denk ik telkens alle tekens van het element <hoofdW> geven van
het begin tot aan de eerst spatie of komma?)
'''

def first_part_of_hoofdw(hw):
    m = re.match(r'([^\s,]+)', hw)
    if m is None: return None
    return m.group(1)

def get_hoofdw_from_filename(xml):
    hw_e = pydash.get(xml.findall('//hoofdW'), 0)
    if hw_e is None:
        raise Exception('No hoofdW in filename %s' % fn)
    return ''.join((hw_e.text, *(_.tail for _ in hw_e)))

def is_verwijslemma(xml):
    return pydash.get(xml.findall('/verwLem'), 0) is not None

def get_filenames_for_letter(letter, ignores=None):
    if ignores is None: ignores = []
    lemmata, homonyms = get_data_lemma(letter=letter.capitalize(),
                                       verbose_cmds=False)
    if len(lemmata) == 0:
        raise Exception('no lemmata returned for letter %s' % letter)
    lemmata = sort_lemmata(lemmata)
    def ignore(fn):
        for ig in ignores:
            if re.search(ig, fn): return True
        return False
    # --- (lemma, naam, homonym_nr, filename, indexI)
    return [x[3] for x in lemmata if not ignore(x[3])]

def process_fns(fhs, fns):
    fh_all, fh_zonder_verw = fhs
    for fn in fns:
        xml = get_xml_from_file(fn)
        hw_full = get_hoofdw_from_filename(xml)
        hw = first_part_of_hoofdw(hw_full)
        if hw is None:
            warn("Can't get first part of hoofdwoord: filename=%s, hw_full=%s" % (fn, hw_full))
        is_verw = is_verwijslemma(xml)
        fh_all.write('%s\n' % hw)
        if not is_verw:
            fh_zonder_verw.write('%s\n' % hw)

def get_date():
    lt = time.localtime()
    return '%d-%02d-%02d' % (lt.tm_year, lt.tm_mon, lt.tm_mday)

def make_output_filenames(*fns):
    date = get_date()
    f = lambda x: re.sub(r'__DATE__', date, x)
    return (f(fn) for fn in fns)

def go():
    ofa, ofzw = make_output_filenames(output_file_all, output_file_zonder_verw)
    with open(ofa, 'w') as fh_all, open(ofzw, 'w') as fh_zonder_verw:
        info('Writing output in files: %s, %s' % (green (ofa), green (ofzw)))
        for letter in letters:
            fns = get_filenames_for_letter(letter)
            process_fns((fh_all, fh_zonder_verw), fns)
go()
