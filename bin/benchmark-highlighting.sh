#!/usr/bin/env bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")

. "$bindir"/functions.bash

USAGE="Usage: $0"

# without highlighting: 30 ms / request
# with highlighting
#   with abridge: 56.5 ms / request
#   without abridge: 52 ms / request
# using in-memory xslt: saves 3 ms


url=http://localhost:8080/api/lemma/by-id
num_requests=50
tmp=/tmp/post

declare -A data
data[1]='{"data":{"ids":[950],"highlighting":{"950":["biastikos","biazw","Plat"]},"do_school":false,"do_post_processing":false}}'

with-curl () {
    mci
    mcb curl "$url"
    mcb   -H 'Content-Type: application/json'
    mcb   --data-binary
    mcb   "${data[1]}"
    mcg
}

do-ab () {
    mci
    mcb ab
    mcb   -n "$num_requests"
    mcb   -T 'application/json'
    mcb   -p "$tmp"
    mcb   "$url"
    mcg
}

with-ab () {
    local dataidx=$1
    local the_data=${data["$dataidx"]}
    redirect-out "$tmp" echo "$the_data"
    fun do-ab
}

# fun with-curl
fun with-ab 1
