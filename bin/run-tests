#!/bin/bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")
rootdir="$bindir"/..
utildir="$rootdir"/lib/grne/util

. "$bindir"/functions.bash

USAGE="Usage: $0 [-v]"

opt_v=
while getopts hv-: arg; do
    case $arg in
        h) warn "$USAGE"; exit 0 ;;
        v) opt_v=yes ;;
        -) OPTARG_VALUE="${OPTARG#*=}"
            case $OPTARG in
                help)  warn "$USAGE"; exit 0 ;;
                '')    break ;;
                *)     error "Illegal option --$OPTARG" ;;
                esac ;;
        *) error "$USAGE" ;;
    esac
done
shift $((OPTIND-1))

go () {
    local opts=()
    if [ "$opt_v" = yes ]; then
        opts+=(-vv --capture=sys)
    else
        opts+=(-rxXs)
    fi
    local o=$(join-out ' ' opts)

    mci
    mcb pytest-3 $o
    # -- we include this so pytest-3 gives better messages when assert
    # fails.
    mcb   "$utildir"/util_tests.py
    mcb   "$bindir"/tests.py
    mcb   "$bindir"/test-autocomplete.py
    mcg

}; fun go
