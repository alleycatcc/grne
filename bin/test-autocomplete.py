#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import proj_paths # noqa

from functools import partial
from os import path
from sys import exit, stderr, argv

import pytest

from grne.util.util_tests import (
    toets,
)

from grne.chars import (
    Spiritus_info,
)

from grne.autocomplete import (
    unique,
    get_wildcard_sql,
    get_spiritus_clause,
    check_bounds,
    check_wildcards,
    analyse_length_and_wildcards,
    process_input,
    autocomplete_lemma,
    autocomplete_pars,
)

require_pytest3 = False

if require_pytest3 and path.basename(argv[0]) != 'pytest-3':
    stderr.write('Must be run using pytest-3.\n')
    exit(1)

def test_unique():
    group = partial(toets, unique)
    test = partial(group, 'list of lists')
    test(([[1, 2], [1, 3], [1, 3], [1, 4]], True), [[1, 2], [1, 3], [1, 4]])
    test = partial(group, 'mix of elements in list')
    test(([[1, 54], [3, 57], 2, ('a', 'b'), [3, 57]], True), [[1, 54], [3, 57], 2, ('a', 'b')])

def test_get_wildcard_sql():
    group = partial(toets, get_wildcard_sql)
    test = partial(group, 'main')
    test(('abcd', False), 'abcd%')
    test(('abcd ', True), 'abcd ')
    test(('ab*cd?ef', False), 'ab_%cd_ef%')
    test(('ab???cd*** ', True), 'ab___cd_%_%_% ')

def test_get_spiritus_clause():
    group = partial(toets, get_spiritus_clause)
    test = partial(group, 'main')
    test(('lemma', '001', '100'), [["fs.spiritus_left & ? == ?", "fs.spiritus_right & ? == ?"], [1, 1, 4, 4]])
    test(('pars', '11001', '10101'), [["fss.spiritus_left & ? == ?", "fss.spiritus_right & ? == ?"], [25, 25, 21, 21]])
    test(('pars', '', ''), [])
    test(('lemma', '', '1'), [["fs.spiritus_right & ? == ?"], [1, 1]])

def test_check_bounds():
    group = partial(toets, check_bounds)
    test = partial(group, 'main')
    test(("abcd", False), [False, None])
    test(("abcd ", False), [True, 4])
    test(("a b c d ", False), [True, 7])
    test(("a b c d            ", False), [True, 7])
    test(("a*b", True), [False, None])
    test(("a*b ", True), [True, None])
    test(('a b', False), [False, None])
    test(('quer*', True), [False, None])

def test_check_wildcards():
    group = partial(toets, check_wildcards)
    test = partial(group, 'main')
    test(("abc*"), [True, True, 3])
    test(("abcd"), [False, False, None])
    test(("abc?"), [True, False, 3])
    test(("abc?def*"), [True, True, 3])
    test(("abc*def?"), [True, True, 3])
    test(("abc*def*"), [True, True, 3])
    test(("abc*de>??????XX***f*"), [True, True, 3])


def test_analyse_length_and_wildcards():
    group = partial(toets, analyse_length_and_wildcards)
    test = partial(group, 'main')
    test(("query"), ['query', 5, False, False, None])
    test(("query "), ['query', 5, False, True, 5])
    test((" query"), ['query', 5, False, False, None])
    test(("quer*"), ['quer', 4, True, False, None])
    test((" qu88??  ery    "), ['qu88', 4, True, True, 11])
    test((" qu88??  ery"), ['qu88', 4, True, False, None])
    test((" qu88?? **  ery"), ['qu88', 4, True, False, None])

# the function process_input is basically the same as
# analyse_and_convert_query, hence it does not need extensive testing
def test_process_input():
    group = partial(toets, process_input)
    test = partial(group, 'main')
    test("query", ["query", "loose", Spiritus_info('00000', '00000', [0, 0, 0, 0, 0], False)])
    test("ἱὉabἐᾘkk", ["ioabehkk", "loose", Spiritus_info('00000011', '00110000', [0, 0, 7, 7, 0, 0, -7, -7], True)])
    test('ὰαάᾳᾶ', ('ὰαάᾳᾶ', 'strict', Spiritus_info('00000', '00000', [0, 0, 0, 0, 0], False)))

def test_autocomplete_lemma():
    tail = lambda xs: xs[1:]

    # --- throw away the id field
    def strip_id(o):
        o['results'] = [tail(xs) for xs in o['results']]

    def eq(a, b):
        strip_id(a)
        strip_id(b)
        return a == b

    group = partial(toets, autocomplete_lemma, eq=eq)
    test = partial(group, 'main')
    test(("πιμε", 10), {
        'error': (None, None),
        'results': [
            [None, 'πιμελή', 'πιμελή-1'],
            [None, 'πιμελής', 'πιμελής-1'],
            [None, 'πιμελώδης', 'πιμελώδης-1']
        ]})
    test(("aga ", 10), {
        'error': (None, None),
        'results': [
            [None, 'ἄγα (ἄγᾱ)', 'ἄγα-1'],
            [None, 'ἄγα (ἄγη)', 'ἄγη-1'],
        ]})
    test(("ἀ", 10), {
        'error': (None, None),
        'results': [
            [None, 'ἀ', 'ἀ-1'],
            [None, 'ἄ', 'ἄ-1'],
            [None, 'ἆ', 'ἆ-1'],
            [None, 'ἀ-', 'ἀ--1'],
            [None, 'ἀάατος', 'ἀάατος-1'],
            [None, 'ἀαγής', 'ἀαγής-1'],
            [None, 'ἄαπτος', 'ἄαπτος-1'],
            [None, 'ἀάσθην', 'ἀάσθην-1'],
            [None, 'ἀάσχετος', 'ἀάσχετος-1'],
            [None, 'ἄατος', 'ἄατος-1'],
        ]})

def test_autocomplete_pars():
    group = partial(toets, autocomplete_pars)
    test = partial(group, 'main')
    test(("πιμελή", 10), {
        'error': (None, None),
        'results': [
            'πιμελή',
            'πιμελήν',
            'πιμελής'
        ]})
    test(("πιμε", 10), {
        'error': (None, None),
        'results': [
            'ʼπιμεῖναι',
            'πιμελή',
            'πιμελὴ',
            'πιμελῆ',
            'πιμελῇ',
            'πιμελήν',
            'πιμελὴν',
            'πιμελής',
            'πιμελῆς',
            'πιμελῶδες'
        ]})
    test(("para/bo", 10), {
        'error': (None, None),
        'results': [
            'παράβολοι',
            'παράβολον',
            'παράβολος'
        ]})
    test(("parabolo", 10), {
        'error': (None, None),
        'results': [
            'παράβολοι',
            'παραβόλοις',
            'παράβολον',
            'παράβολος',
            'παραβόλου',
            'παραβόλους'
        ]})
    test(("para4", 10), {
        'error': ('Invalid input', None),
        'results': None,
    })
    test(('p?ara', 10), {
        'error': (None, None),
        'results': [],
    })
    test(('bu?e', 10), {
        'error': (None, None),
        'results': ['βύζεται', 'Βύζεω', 'Βύκελον', 'Βύκελος'],
    })
    test(('ba/?aq', 10), {
        'error': (None, None),
        'results': ['βάραθρα', 'βάραθρον', 'βάραθρόν'],
    })
    test(('ἁα', 10), {
        'error': (None, None),
        'results': [],
    })
    # The following query gave no results in the old site, this was a bug
    # that is fixed now.
    test(('a)ge/l?i', 10), {
        'error': (None, None),
        'results': ['ἀγέλαι', 'ἀγέλαις'],
    })
    test(('aga', 10), {
        'error': (None, None),
        'results': ['ἄγα', 'ἀγάασθαι', 'ἀγάασθε', 'ἄγαγʼ', 'ἄγαγε', 'ἄγαγέ',
                    'ἀγαγεῖν', 'ἀγαγὲν', 'ἄγαγεν', 'ἄγαγες'],
    })
    test(('aga ', 10), {
        'error': (None, None),
        'results': ['ἄγα'],
    })
    test(('ἀ', 10), {
        'error': (None, None),
        'results': ['ἆ', 'Ἆ', 'ἀᾶ', 'ἀάατον', 'ἀάατος', 'ἀαγεῖς', 'ἀαγές', 'ἀαγὲς', 'ἄαν', 'ἄαπτοι']
    })
    test(('Ἀαρ', 10), {
        'error': (None, None),
        'results': ['Ἀαρών', 'Ἀαρὼν', 'Ἀαρῶνα', 'Ἀαρῶνι', 'Ἀαρῶνος'],
    })
    test(('ἀαρώ', 10), {
        'error': (None, None),
        'results': ['Ἀαρών'],
    })
    test(('ἀαρὼ', 10), {
        'error': (None, None),
        'results': ['Ἀαρὼν'],
    })
    test(('ἀαρῶ', 10), {
        'error': (None, None),
        'results': ['Ἀαρῶνα', 'Ἀαρῶνι', 'Ἀαρῶνος'],
    })

# --- known to fail because of bug with question marks.
@pytest.mark.xfail(reason='not exactly clear, but might have something to do with the combination of question marks, capital letters and strict mode')
def test_autocomplete_pars_bug0():
    group = partial(toets, autocomplete_pars)
    test = partial(group, 'main')
    test(('A)a?w\n', 10), {
        'error': (None, None),
        'results': [],
    })

# --- known to fail because of position bug
@pytest.mark.xfail(reason='The logic with the σ at the end of a word seems to be incomplete yet. This request should give a result.')
def test_autocomplete_pars_bug1():
    group = partial(toets, autocomplete_pars)
    test = partial(group, 'main')
    test(('ἁβουλεύσ', 10), {
        'error': (None, None),
        'results': ['ἁβουλεύσαμεν'],
    })
    test(('ἁβουλεύσα', 10), {
        'error': (None, None),
        'results': ['ἁβουλεύσαμεν'],
    })
