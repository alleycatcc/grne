#!/usr/bin/env bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")

. "$bindir"/functions.bash

USAGE="Usage: $0"

go-block () {
    id=22800
    nb=2
    na=2
    do_school=false
    echo "$id / $nb / $na / $do_school"
    mci
    mcb curl 'http://localhost:3000/api/lemma/block-by-id'
    mcb   -H 'Content-Type: application/json'
    mcb   --data-binary "{\"data\":{\"id\":$id,\"num_before\":$nb,\"num_after\":$na,\"do_school\":$do_school,\"highlighting\":{\"22798\": [\"valse\"], \"22800\": [\"bestaande\"]}}}"
    mcg
}

go-by-id () {
    # ids=(1 4 5 6 9 $(range 11 100))
    ids=(1)
    do_school=true
    ids_list=$(join-out , ids)
    cmd curl 'http://localhost:3000/api/lemma/by-id' -H 'Origin: http://localhost:3000' -H 'Accept-Encoding: gzip, deflate, br' -H 'Accept-Language: en-US,en;q=0.9,nl;q=0.8' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36' -H 'Content-Type: application/json' -H 'Accept: */*' -H 'Referer: http://localhost:3000/' -H 'Connection: keep-alive' --data-binary "{\"data\":{\"ids\":[$ids_list],\"highlighting\":{\"1\":[\"leider\"]},\"do_school\":$do_school}}"
}

# fun go-block
fun go-by-id
