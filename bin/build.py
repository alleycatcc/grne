#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import proj_paths # noqa

import os.path as path

from acatpy.io.speak import not_ok as speak_not_ok, ok as speak_ok, infon, info
from acatpy.io.io import die, cmd, cpa, rmdir
from acatpy.colors import red, yellow, bright_red
from acatpy.util import pathjoin

from grne.build.make_site import go as make_site
from grne.build.make_index import go_lemma as make_index_lemma
from grne.build.make_index import go_pars as make_index_pars
from grne.build.make_index import go_cit as make_index_cit
from grne.util.util_redis import redis_client_get_persist

from config import get as config_get

def do_cmd(*args, **kw):
    kw['die'] = True
    cmd(*args, **kw)

def check_lemmatiser(lpath):
    infon('Looking for lemmatiser db ')
    if not path.exists(lpath):
        speak_not_ok()
        die('Lemmatiser db not found (%s)' % red(lpath))
    speak_ok()

def make_query_parser(antlr4_path, generate_dir, grammar_spec):
    do_cmd([antlr4_path, '-o', generate_dir, '-Dlanguage=Python3', grammar_spec])
    return True

def prepare_export_scripts(export_scripts_path):
    do_cmd(['yarn'], cwd=export_scripts_path)
    return True

def make_frontend_main(*args):
    return make_frontend_x(*args)

def make_frontend_x(env, srcpath, buildpath):
    do_cmd(['yarn'], cwd=srcpath)
    step = 'build-%s' % env
    do_cmd(['yarn run %s' % step], cwd=srcpath, shell=True)
    if path.exists(buildpath):
        rmdir(buildpath)
    cpa(pathjoin(srcpath, 'build'), buildpath)
    return True

def make_site_with_cache(*args, **kw):
    err, redis_client_persist = redis_client_get_persist(warn=False)
    if err:
        die("Unable to connect to 'persist' redis server.\n\nSometimes redis doesn't bind to the expected IP address and restarting it may help. Error was: " + str(err))
    return make_site(*args, **kw, redis_client_persist=redis_client_persist)

def do_step(do_it, desc, run, pre=None):
    if not do_it:
        return info('[skip] %s' % bright_red(desc))
    info('[run] %s' % yellow(desc))
    if pre: pre()
    ok = run()
    if not ok: die('Failed at %s' % red(desc))

def go():
    config = config_get()

    do_step(
        config.build['do_make_query_parser'],
        'generate-query-parser',
        lambda: make_query_parser(
            config.user['antlr4_path'],
            config.main['query_parser_generate_dir'],
            config.main['query_parser_grammer_spec'],
        ),
    )

    do_step(config.build['do_lemma'], 'make-index-lemma', make_index_lemma)
    do_step(config.build['do_site'], 'make-site', make_site_with_cache)

    do_step(
        config.build['do_prepare_export_scripts'],
        'prepare-export-scripts',
        lambda: prepare_export_scripts(
            config.main['html-export-scripts-path'],
        ),
    )
    do_step(
        config.build['do_frontend_main_tst'],
        'make-frontend-main-tst',
        lambda: make_frontend_main(
            'tst-optimised',
            config.main['web_main_frontend_src_path'],
            config.main['web_main_build_path_tst'],
        ),
    )
    do_step(
        config.build['do_frontend_main_acc'],
        'make-frontend-main-acc',
        lambda: make_frontend_main(
            'acc',
            config.main['web_main_frontend_src_path'],
            config.main['web_main_build_path_acc'],
        ),
    )
    do_step(
        config.build['do_frontend_main_prd'],
        'make-frontend-main-prd',
        lambda: make_frontend_main(
            'prd',
            config.main['web_main_frontend_src_path'],
            config.main['web_main_build_path_prd'],
        ),
    )
    do_step(
        config.build['do_pars'], 'make-index-pars', make_index_pars,
        pre=lambda: check_lemmatiser(config.local['lemmatiser_path']),
    )
    # do_step(config.build['do_lemma'], 'make-index-lemma', make_index_lemma)
    do_step(config.build['do_cit'], 'make-index-cit', make_index_cit)
    # do_step(config.build['do_site'], 'make-site', make_site_with_cache)

    info('All done.')


go()
