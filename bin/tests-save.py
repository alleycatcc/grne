@pytest.mark.skipif(True, reason='needs update: has_spiritus was removed')
def test_pytest_has_spiritus_s():
    group = partial(toets, has_spiritus)
    test = partial(group, 'pure dasia and pure psili')
    test(["ἱὉabἐᾘkk"], [-7, -7, 0, 0, 7, 7, 0, 0])
    test(["ἱὉhaἼὄha"], [-7, -7, 0, 0, 7, 7, 0, 0])

    test = partial(group, 'mixed dasia and mixed psili and some random words')
    test(["ἃἅHkkᾖ"], [-7, -7, 0, 0, 0, 7])
    test(["ἀκτῆ"], [7, 0, 0, 0])
    test(["ἀκύμων"], [7, 0, 0, 0, 0, 0])

    group('no dasia nor psili', ["βέῃ"], [])
    group('mixed dasia and mixed psili', ["ἶἃᾒὊ"], [7, -7, 7, 7])

@pytest.mark.skipif(True, reason='needs update: has_spiritus_bitvector was removed')
def test_has_spiritus_bitvector():
    group = partial(toets, has_spiritus_bitvector)
    group('basic', ["hallo"], [['0', '0', '0', '0', '0'], ['0', '0', '0', '0', '0'], None])
    group('basic', ["ἑβψ"], [['0', '0', '1'], ['0', '0', '0'], 1])

    test = partial(group, 'pure dasia and psili')
    test(["ἱὉabἐᾘkk"], [['0', '0', '0', '0', '0', '0', '1', '1'], ['0', '0', '1', '1', '0', '0', '0', '0'], 1])
    test(["ἱὉhaἼὄha"], [['0', '0', '0', '0', '0', '0', '1', '1'], ['0', '0', '1', '1', '0', '0', '0', '0'], 1])

    test = partial(group, 'mixed dasia and mixed psili')
    test(["ἃἅHkkᾖ"], [['0', '0', '0', '0', '1', '1'], ['1', '0', '0', '0', '0', '0'], 1])
    test(["ἶἃᾒὊ"], [['0', '0', '1', '0'], ['1', '1', '0', '1'], 1])

@pytest.mark.skipif(True, reason='needs update: function was removed')
def test_pytest_has_pure_spiritus():
    group = partial(toets, has_pure_spiritus)

    group('pure dasis and pure psili', ["ἱὉabἐᾘkk"], [-7, -7, 0, 0, 7, 7, 0, 0])
    test = partial(group, 'mixed dasia and mixed psili and some random words')
    test(["ἱὉhaἼὄha"], [-7, -7, 0, 0, 0, 0, 0, 0])
    test(["ἃἅHkkᾖ"], [])
    test(["ἀκτῆ"], [7, 0, 0, 0])
    test(["ἀκύμων"], [7, 0, 0, 0, 0, 0])

    test = partial(group, 'no dasia nor psili')
    test(["βέῃ"], [])

    test = partial(group, 'mixed dasia and mixed psili')
    test(["ἶἃᾒὊ"], [])

@pytest.mark.skipif(True, reason='needs update: function was removed')
def test_has_pure_spiritus_bitvector_s():
    group = partial(toets, has_pure_spiritus_bitvector)
    test = partial(group, 'no description')
    # test for pure dasia
    test(["ᾡὑὉ"], [['1', '1', '1'], ['0', '0', '0'], 1])
    # test for pure psili
    test(["ἐᾘὨἀ"], [['0', '0', '0', '0'], ['1', '1', '1', '1'], 1])
    # test for mixed dasia and mixed psili
    test(["Ἕἣἥἶὂὄ"], [['0', '0', '0', '0', '0', '0'], ['0', '0', '0', '0', '0', '0'], None])

@pytest.mark.xfail(reason='neglected combining iota subscriptum in old code')
def test_to_latin_iota_expand_combining():
    def test(args, expect):
        assert to_latin(*args, True) == tuple(expect)
    test(["α͔"], ["ai", 1, []])

# --- known to fail because of position bug
@pytest.mark.xfail(reason='old code contains a bug involving the first position')
def test_analyse_and_convert_query_position_bug0():
    # --- 03AC => 03AC
    # test(["ά", "0"], ["ά", "strict", None])
    pass

# --- known to fail because of position bug
@pytest.mark.xfail(reason='old code contains a bug involving the first position')
def test_analyse_and_convert_query_position_bug1():
    # --- 03AC => 1F71
    # test(["ά", "1"], ["ά", "strict", []])
    pass

# --- known to fail because of position bug
@pytest.mark.xfail(reason='old code contains a bug involving the first position')
def test_analyse_and_convert_query_position_bug2():
    # --- 03AC β => 1F71 β
    # test(["άβ", "1"], ["άβ", "strict", []])
    pass

# --- known to fail because of position bug
@pytest.mark.xfail(reason='old code contains a bug involving the first position')
def test_analyse_and_convert_query_position_bug3():
    # --- 1F71 β => 1F71 β
    # test(["άβ", "0"], ["άβ", "strict", []])
    pass

# --- known to fail because of position bug
@pytest.mark.xfail(reason='old code contains a bug involving the first position')
def test_analyse_and_convert_query_position_bug4():
    # --- 1F71 β => 1F71 β
    # test(["άβ", "1"], ["άβ", "strict", []])
    pass

@pytest.mark.xfail(True, reason='obscure beta-code combinations')
def test_to_greek_precise_obscure():
    group = partial(toets, to_greek_precise)
    test = partial(group, 'canonical = combining: alpha + c.macron + c.acute')
    test(r'a&/', ('ᾱ́', True, []))
