#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import proj_paths # noqa

import io

from lxml import etree

from grne.util.util import get_xml_from_file, read_xml_from_file
from acatpy.io.speak import warn, info

xsd='/home/fritz/de/src/alleycat/grne/grne-lemmata/hulpdocumenten/xsd/lemma.xsd'

xml_bad='/home/fritz/ac/grne/grne-lemmata/Alpha/Alpha.1.17/αἰγίς.xml'
xml_good='/home/fritz/ac/grne/grne-lemmata/Gamma/Gamma.1.16/γωνίδιον.xml'

xml_bad = [
    '/home/fritz/ac/grne/grne-lemmata/Beta/Beta.1.4/βακταρικροῦσα.xml',
    '/home/fritz/ac/grne/grne-lemmata/Beta/Beta.1.12/βιβάσθων.xml',
    '/home/fritz/ac/grne/grne-lemmata/Beta/Beta.1.1/βᾶ.xml',
    '/home/fritz/ac/grne/grne-lemmata/Epsilon/Epsilon.18.0/ἐκβιάζω.xml',
    '/home/fritz/ac/grne/grne-lemmata/Pi/Pi.11.3/πρόκειμαι.xml',
    '/home/fritz/ac/grne/grne-lemmata/Kappa/Kappa.2.7/κατασημαίνω.xml',
    '/home/fritz/ac/grne/grne-lemmata/Sigma/Sigma.1.12/σκύλαξ.xml',
    '/home/fritz/ac/grne/grne-lemmata/Sigma/Sigma.1.41/συνέχω.xml',
    '/home/fritz/ac/grne/grne-lemmata/Sigma/Sigma.1.29/συμβαίνω.xml',
    '/home/fritz/ac/grne/grne-lemmata/Sigma/Sigma.1.33/συμφορά.xml',
]

xsd_parsed = etree.parse(xsd)
schema = etree.XMLSchema(xsd_parsed)

for xml in xml_bad:
    info("Looking at: %s" % xml)
    xml_contents = get_xml_from_file(xml)
    if schema.validate(xml_contents):
        info('ok')
    else:
        log = schema.error_log
        last_error = log.last_error
        warn('error %s' % last_error)
        info('not ok')

