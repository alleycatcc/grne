#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re

import proj_paths # noqa

from lxml import etree as etree
from grne.util.xml.replace_text import replace_text_in_xml_tree

def replace_input_node_ends_in_punc(punc_stan, punc_bekn):
    def replace(m):
        middle = []
        if punc_stan: middle.append(
            '<span class="punc-stan">%s</span>' % punc_stan,
        )
        if punc_bekn: middle.append(
            '<span class="punc-bekn">%s</span>' % punc_bekn,
        )
        print('middle %s' % middle)
        print('m.groups() %s' % list(m.groups()))
        ret = ''.join([
            *middle,
            m.groups()[1],
        ])
        print('ret "%s"' % ret)
        return ret
    return replace

rx_punc_save_spaces_m2 = r'([,.:;]) (\s*) $'

# vurig, onstuimig: bug in orig (2 periods)

def go():
    test1 = (
        '<span class="vertM">en: </span>',
        rx_punc_save_spaces_m2,
        replace_input_node_ends_in_punc(':', ';')
    )
    test2 = (
        '<gebrW>met gen. <r>in:</r> </gebrW>',
        rx_punc_save_spaces_m2,
        # --- but cit doesn't end in anything ...
        replace_input_node_ends_in_punc(':', ''),
    )
    test3 = (
        '<span class="gebrW">met acc. resp.: </span>',
        rx_punc_save_spaces_m2,
        replace_input_node_ends_in_punc(':', '.'),
    )

    doit(test3)

def doit(spec):
    xml, rx, replace = spec
    xmltree = etree.fromstring(xml)
    errors, newtree = replace_text_in_xml_tree(
        xmltree, rx, replace,
        re_flags=re.X,
    )
    print('errors %s' % errors)
    print('newtree %s' % etree.tostring(newtree))

go()
