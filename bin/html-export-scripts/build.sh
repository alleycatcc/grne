#!/usr/bin/env bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")
babelbin="$bindir"/node_modules/.bin/babel

. "$bindir"/functions.bash

USAGE="Usage: $0"

cwd "$bindir" "$babelbin" -d "$bindir"/lib "$bindir"/src "$@"
