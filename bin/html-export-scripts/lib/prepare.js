#!/usr/bin/env node

/*
 * Expects strings on standard input, separated by the token '====='
 *
 * <scss>
 * =====
 * <html 1>
 * =====
 * <html 2>
 * ====
 * <html n ...>
 *
 * Reads incrementally and spits out transformed html each time an html input is read.
 *
 * The steps in the processing are:
 * - Surround the scss with `.root {}` and convert the scss to css
 * - Surround html inputs with `<div class='root'>`, and run them through
 *   the 'juice' module to get html with inlined attributes.
*/
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _fishLib = _interopRequireWildcard(require("fish-lib"));

var _es = require("stick-js/es");

var _readline = _interopRequireDefault(require("readline"));

var _juice = _interopRequireDefault(require("juice"));

var _nodeSass = _interopRequireDefault(require("node-sass"));

var _op = function _op() {
  return _es.pipe.apply(void 0, arguments);
};

var _op2 = function _op2() {
  return _es.compose.apply(void 0, arguments);
};

var _op3 = function _op3() {
  return _es.composeRight.apply(void 0, arguments);
};

(0, _fishLib.bulletSet)({
  type: 'star'
});
var _process = process,
    stdin = _process.stdin,
    stdout = _process.stdout;

var toString = _op('toString', _es.dot);

var on = _op('on', _es.side2);

var clear = (0, _es.dot1)('splice')(0);
var ifEquals = (0, _es.recurry)(3)(function (x) {
  return (0, _es.ifPredicate)(_op(x, _es.eq));
});
var prepareHtml = (0, _es.sprintf1)("<div class='root'>%s</div>");

var doHtml = function doHtml(css, html) {
  return doJuice(css, _op(html, prepareHtml));
};

var doSass = function doSass(scss) {
  return _nodeSass["default"].renderSync({
    data: scss
  });
}; // --- garbage-in garbage-out


var doJuice = function doJuice(css, html) {
  return (0, _juice["default"])(_op([css, html], (0, _es.sprintfN)("\n    <style>%s</style>\n    %s\n  ")));
};

var logWith = function logWith(header) {
  return function () {
    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _fishLib.log.apply(void 0, [header].concat(args));
  };
};

var _makeCss = function _makeCss(scss) {
  return _op(_op(scss, (0, _es.sprintf1)('.root { %s }')), doSass);
}; // --- @throws


var makeCss = function makeCss(scss) {
  return (0, _es.tryCatch)(function (css) {
    return _op(_op(css, (0, _es.prop)('css')), toString);
  }, function (e) {
    return (0, _es.die)('Error running sass: ' + e);
  }, function () {
    return _op(scss, _makeCss);
  });
};

var createReadline = function createReadline() {
  return _readline["default"].createInterface({
    input: stdin
  });
};

var go = function go() {
  var scss = [];
  var html = [];
  var css;
  var buf = scss;
  var scssDone = false;

  _op(createReadline(), on('line', function (line) {
    return _op(line, ifEquals('=====')(function () {
      return _op(scssDone, (0, _es.ifTrue)(function () {
        var transformed = doHtml(css, _op(html, (0, _es.join)('')));
        stdout.write(transformed + '\n');
        stdout.write('=====' + '\n');

        _op(html, clear);
      }, function () {
        css = _op(_op(scss, (0, _es.join)('')), makeCss);
        scssDone = true;
        buf = html;
      }));
    }, function () {
      return _op(buf, (0, _es.appendM)(line));
    }));
  }));
};

go();