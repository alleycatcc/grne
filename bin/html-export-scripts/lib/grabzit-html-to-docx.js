#!/usr/bin/env node
"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _fs = _interopRequireDefault(require("fs"));

var _fishLib = _interopRequireWildcard(require("fish-lib"));

var _es = require("stick-js/es");

var _readline = _interopRequireDefault(require("readline"));

var _grabzit = _interopRequireDefault(require("grabzit"));

var _op = function _op() {
  return _es.pipe.apply(void 0, arguments);
};

var _op2 = function _op2() {
  return _es.compose.apply(void 0, arguments);
};

var _op3 = function _op3() {
  return _es.composeRight.apply(void 0, arguments);
};

(0, _fishLib.bulletSet)({
  type: 'star'
});
var _process = process,
    stdin = _process.stdin;

var on = _op('on', _es.side2);

var createReadline = function createReadline() {
  return _readline["default"].createInterface({
    input: stdin
  });
};

var length = _op('length', _es.prop);

var slice = _op('slice', _es.dot2);

var truncate = (0, _es.recurry)(2)(function (n) {
  return function (str) {
    return (0, _es.lets)(function () {
      return _op(_op(str, length), (0, _es.condS)([_op(_op(n, _es.lte), (0, _es.guard)(function () {
        return _es.id;
      })), _op(_es.otherwise, (0, _es.guard)(function () {
        return _op3(slice(0, n), (0, _es.concat)('…'));
      }))]));
    }, function (f) {
      return _op(str, f);
    });
  };
});

var convert = function convert(key, secret, outputPath, html) {
  var client = new _grabzit["default"](key, secret);

  try {
    client.html_to_docx(html);
    client.save_to(outputPath, function (error, id) {
      _op(error, (0, _es.ifOk)(_es.raise, function () {
        return (0, _fishLib.info)('[grabzit] successfully converted');
      }));
    });
  } catch (err) {
    (0, _fishLib.warn)(_op([err, _op(html, truncate(100))], (0, _es.sprintfN)('Error with conversion: %s (html: %s)')));
  }
};

var go = function go() {
  var html = '';
  var x = 0;
  var key;
  var secret;
  var outputPath;

  _op(_op(createReadline(), on('line', function (line) {
    if (x === 0) key = line;else if (x === 1) secret = line;else if (x === 2) outputPath = line;else html += '\n' + line;
    ++x;
  })), on('close', function () {
    return convert(key, secret, outputPath, html);
  }));
};

go();