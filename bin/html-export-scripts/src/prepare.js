#!/usr/bin/env node

/*
 * Expects strings on standard input, separated by the token '====='
 *
 * <scss>
 * =====
 * <html 1>
 * =====
 * <html 2>
 * ====
 * <html n ...>
 *
 * Reads incrementally and spits out transformed html each time an html input is read.
 *
 * The steps in the processing are:
 * - Surround the scss with `.root {}` and convert the scss to css
 * - Surround html inputs with `<div class='root'>`, and run them through
 *   the 'juice' module to get html with inlined attributes.
*/

defineBinaryOperator ('|',  (...args) => pipe     (...args))
defineBinaryOperator ('<<', (...args) => compose    (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import fishLib, {
  log, info, warn, error, green, yellow, magenta, brightRed, cyan, brightBlue,
  sprintf, getopt, bulletSet,
} from 'fish-lib'

bulletSet ({ type: 'star', })

import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, recurry,
  ifTrue,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, blush, always, T, F,
  prop, path,
  lets, letS,
  die, eq, ne,
  factory, factoryProps,
  appendM,
  tryCatch,
} from 'stick-js/es'

import readline from 'readline'

import juice from 'juice'
import sass from 'node-sass'

const { stdin, stdout, } = process

const toString = 'toString' | dot
const on = 'on' | side2
const clear = dot1 ('splice') (0)

const ifEquals = recurry (3) (
  x => ifPredicate (x | eq),
)

const prepareHtml = sprintf1 (`<div class='root'>%s</div>`)
const doHtml = (css, html) => doJuice (css, html | prepareHtml)

const doSass = (scss) => sass.renderSync ({
  data: scss,
})

// --- garbage-in garbage-out
const doJuice = (css, html) => juice (
  [css, html] | sprintfN (`
    <style>%s</style>
    %s
  `)
)

const logWith = (header) => (...args) => log (... [header, ...args])

const _makeCss = (scss) => scss
  | sprintf1 ('.root { %s }')
  | doSass

// --- @throws
const makeCss = (scss) => tryCatch (
  (css) => css | prop ('css') | toString,
  (e) => die ('Error running sass: ' + e),
  () => scss | _makeCss,
)

const createReadline = () => readline.createInterface ({
  input: stdin,
})

const go = () => {
  const scss = []
  const html = []
  let css
  let buf = scss
  let scssDone = false

  createReadline ()
    | on ('line', (line) => line | ifEquals ('=====') (
      () => scssDone | ifTrue (
        () => {
          const transformed = doHtml (css, html | join (''))
          stdout.write (transformed + '\n')
          stdout.write ('=====' + '\n')
          html | clear
        },
        () => {
          css = scss | join ('') | makeCss
          scssDone = true
          buf = html
        },
      ),
      () => buf | appendM (line),
    ))
}

go ()
