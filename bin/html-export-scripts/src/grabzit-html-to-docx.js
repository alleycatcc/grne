#!/usr/bin/env node

defineBinaryOperator ('|',  (...args) => pipe     (...args))
defineBinaryOperator ('<<', (...args) => compose    (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import fs from 'fs'

import fishLib, {
  log, info, warn, error, green, yellow, magenta, brightRed, cyan, brightBlue,
  sprintf, getopt, bulletSet,
} from 'fish-lib'

bulletSet ({ type: 'star', })

import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, recurry,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, blush, always, T, F,
  prop, path,
  lets, letS,
  raise, die, eq, ne,
  factory, factoryProps,
  lte, concat,
} from 'stick-js/es'

import readline from 'readline'

import grabzit from 'grabzit'

const { stdin, } = process

const on = 'on' | side2
const createReadline = () => readline.createInterface ({
  input: stdin,
})

const length = 'length' | prop
const slice = 'slice' | dot2

const truncate = recurry (2) (
  n => str => lets (
    () => str | length | condS ([
      n | lte | guard (() => id),
      otherwise | guard (() => slice (0, n) >> concat ('…')),
    ]),
    (f) => str | f,
  ),
)

const convert = (key, secret, outputPath, html) => {
  const client = new grabzit (key, secret)
  try {
    client.html_to_docx (html)
    client.save_to (outputPath, (error, id) => {
      error | ifOk (raise, () => info ('[grabzit] successfully converted'))
    })
  } catch (err) {
    warn (
      [err, html | truncate (100)] | sprintfN (
        'Error with conversion: %s (html: %s)',
      ),
    )
  }
}

const go = () => {
  let html = ''
  let x = 0
  let key
  let secret
  let outputPath
  createReadline ()
    | on ('line', (line) => {
      if (x === 0) key = line
      else if (x === 1) secret = line
      else if (x === 2) outputPath = line
      else html += '\n' + line
      ++x
    })
    | on ('close', () => convert (key, secret, outputPath, html))
}

go ()
