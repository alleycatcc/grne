#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import proj_paths # noqa

from grne.query_parser.parse import get_parser

from acatpy.io.speak import info
from acatpy.colors import green, red, yellow

test_choose = lambda x: x

test_aexpr0 = [
    (
        'field simple',
        'field:simple',
    ),
    (
        'field "quoted"',
        'field:quoted',
    ),
    (
        r'field "\"inner quote -> complex phrase\""',
        '{!complexphrase inOrder=true}field:"inner quote -> complex phrase"',
    ),
    (
        # --- no space around quotes.
        r'field "\"abc\" "',
        None,
    ),
    (
        # --- no characters outside quotes.
        r'field "plato \"van vader\""',
        None,
    ),
    (
        '(_text_ "a)e")',
        '_text_:ἀε',
    ),
    (
        r'field "quo\"ted"',
        'field:quo"ted',
    ),
    (
        r'field "quo\"{ed"',
        'field:quo"{ed',
    ),
    (
        r'field "\"van vader\""',
        '{!complexphrase inOrder=true}field:"van vader"',
    ),
    # ------ valid aExpr but not valid solr, ok.
    (
        r'field "\""',
        'field:"',
    ),
    # ------ valid aExpr but not valid solr, not ok.
    (
        r'field "a b"',
        None,
    ),
    (
        r'field "pan που"',
        None,
    ),
    # field "$ \"a\" "
    # --- valid aExpr, invalid json.
    (
        r'field "\)"',
        None,
    ),
    (
        r'field "a( \""',
        None,
    ),
    (
        r'field "a("',
        'field:ἁ'
    ),
    (
        r'field "\" \""',
        '{!complexphrase inOrder=true}field:" "',
    ),
    (
        r'field "\"\""',
        '{!complexphrase inOrder=true}field:""',
    ),
    (
        r'field "\"van vader\""',
        '{!complexphrase inOrder=true}field:"van vader"',
    ),
    (
        'field van vader',
        '(+field:van +field:vader)',
    ),
    (
        'and (a b c) (c d e f)',
        '(+(+a:b +a:c) +(+c:d +c:e +c:f))',
    ),
    (
        '(field van vader)',
        '(+field:van +field:vader)',
    ),
    (
        '(field "pa)ncat")',
        'field:πἀνξατ',
    ),
    (
        '(field panπαν)',
        'field:panpan',
    ),
    (
        '(field pan)',
        'field:pan',
    ),
    (
        '(field παν)',
        'field:pan',
    ),
    (
        '(and (field παν) (aut plat.))',
        '(+field:pan +aut:plat.)',
    ),
    (
        '(field πον)',
        'field:pon',
    ),
    (
        '(or (field παν) (aut Plat.))',
        '(field:pan aut:plat.)',
    ),
    # ------ latin queries are lowercased, because of analyse_and_convert_query
    (
        '(or (field παν) (aut Plat))',
        '(field:pan aut:plat)',
    ),
    (
        '(and (field Pan) (field San))',
        '(+field:pan +field:san)',
    ),
    (
        '(and (or (field πυτ) (aut Sap)) (or (field παφ) (aut Plat)))',
        '(+(field:put aut:sap) +(field:paf aut:plat))',
    ),
    (
        '(not (field plat))',
        '-field:plat',
    ),
    (
        '(not (field plat) (aut plot) (werk il))',
        '(-field:plat -aut:plot -werk:il)',
    ),
    (
        '(not (field plat plit) (aut plot) (werk il))',
        '(-(+field:plat +field:plit) -aut:plot -werk:il)',
    ),
    (
        '(and (aut plat) (not (werk plot)))',
        '(+aut:plat -werk:plot)',
    ),
    (
        r'(field "\u03b1\u03b5\u03b9\u03b3\u03b5\u03bd\u03b7\u03c2")',
        'field:aeigenhs',
    ),
    (
        '(field "\u03b1\u03b5\u03b9\u03b3\u03b5\u03bd\u03b7\u03c2")',
        'field:aeigenhs',
    ),
    (
        r'(field "\"aesch*ag*186\"")',
        '{!complexphrase inOrder=true}field:"aesch*ag*186"',
    ),
    (
        '(field ἀειγενής)',
        'field:ἀειγενής',
    ),
    (
        '(field ἀειγενής)',
        'field:ἀειγενής',
    ),
    (
        '(and (werk stuff tuff) (not (werk stoff)))',
        '(+(+werk:stuff +werk:tuff) -werk:stoff)',
    ),
    (
        '(and (and (werk stuff tuff) (werk staff taff)) (not (werk stoff)))',
        '(+(+(+werk:stuff +werk:tuff) +(+werk:staff +werk:taff)) -werk:stoff)',
    ),
    (
        '(and (field pros) (not (and (field pros) (aut plat))))',
        '(+field:pros -(+field:pros +aut:plat))',
    ),
    (
        'aut πλατ soph',
        '(+aut:plat +aut:soph)',
    ),
]

test_aexpr_illegal = [
    (
        r'field "\\""',
        None,
    ),
    # ------ invalid beta code.
    (
        r'field "&"',
        None,
    ),
    (
        r'field "("',
        None,
    ),
    (
        'and a b c',
        None,
    ),
]

test_pexpr_with_fieldname = [
    (
        '$ "plat" % "hoofdW-combined"',
        r'(hoofdW-combined "plat")',
    ),
    (
        '$ "plat" ∀ "hoofdW-combined"',
        r'(hoofdW-combined "plat")',
    ),
    (
        '$ "water" and "plat" # "vertM-combined"',
        r'(and (and (_text_ "water") (_text_ "plat")) (or (vertM-combined "water") (vertM-combined "plat")))',
    ),
    (
        '$ "water" and "plat" ∃ "vertM-combined"',
        r'(and (and (_text_ "water") (_text_ "plat")) (or (vertM-combined "water") (vertM-combined "plat")))',
    ),
    (
        '$ "water" and "plat" or "hdt" # "vertM-combined"',
        r'(and (or (and (_text_ "water") (_text_ "plat")) (_text_ "hdt")) ' +
        r'(or (vertM-combined "water") (vertM-combined "plat") (vertM-combined "hdt")))',
    ),
    (
        '$ "water" # "vertM-combined"',
        r'(and (_text_ "water") (or (vertM-combined "water")))',
    ),
][2:]

# --- pexpr -> aexpr always sends JSON.
test_pexpr0 = [
    # ------ double quotes in pexpr means JSON.
    (
        '$ "plat"',
        r'(_text_ "plat")',
    ),
    (
        '$ "plat vader"',
        r'(_text_ "plat vader")',
    ),
    # ------ double quotes necessary
    (
        '$ "$"',
        r'(_text_ "$")',
    ),
    # --- it is not possible to search for the word 'and' without it 
    # being surrounded by double quotes in the aexpr and therefore becoming
    # a complex phrase.
    (
        '$ "and"',
        r'(_text_ "and")',
    ),
    (
        r'$ "\"van vader\"" and "plat" and "hdt"',
        r'(and (and (_text_ "\"van vader\"") (_text_ "plat")) (_text_ "hdt"))',
    ),
    (
        r'$ "\"van vader\""',
        r'(_text_ "\"van vader\"")',
    ),
    (
        r'$ "\"van vader\"" and "\"van moeder\""',
        r'(and (_text_ "\"van vader\"") (_text_ "\"van moeder\""))',
    ),
    (
        r'$ "\"van vader\"" and "plat" and "\"van moeder\""',
        r'(and (and (_text_ "\"van vader\"") (_text_ "plat")) (_text_ "\"van moeder\""))',
    ),
    (
        r'$ "plat" and "hdt"',
        r'(and (_text_ "plat") (_text_ "hdt"))',
    ),
    (
        r'$ "plat"',
        r'(_text_ "plat")',
    ),
    (
        '$ "pam"',
        '(_text_ "pam")',
    ),
    (
        '$ "$pam"',
        '(_text_ "$pam")',
    ),
    (
        '$ "and"',
        '(_text_ "and")',
    ),
    (
        '$ "van" "vader"',
        '(and (_text_ "van") (_text_ "vader"))',
    ),
    (
        r'$ "\"van vader\""',
        r'(_text_ "\"van vader\"")',
    ),
    (
        r'$ "\"van vader\"" "\"voet zetten\""',
        r'(and (_text_ "\"van vader\"") (_text_ "\"voet zetten\""))',
    ),
    (
        '$ "pam"',
        '(_text_ "pam")',
    ),
    (
        '$ "stuff" and "steff" or "stoff"',
        '(or (and (_text_ "stuff") (_text_ "steff")) (_text_ "stoff"))',
    ),
    (
        '$ "tuff" and "taff" not "stoff"',
        '(and (and (_text_ "tuff") (_text_ "taff")) (not (_text_ "stoff")))',
    ),
    (
        r'$ "\"aesch*ag*186\""',
        r'(_text_ "\"aesch*ag*186\"")',
    ),
    (
        '$ "a)geleia"',
        '(_text_ "a)geleia")',
    ),
    (
        '$ "αειγενης"',
        '(_text_ "αειγενης")',
    ),
    (
        '$ "ἀειγενής"',
        '(_text_ "ἀειγενής")',
    ),
]

# ------ series expressions are nice to have, but not useful from the point
# of view of the page; the page still needs to tokenise & escape.
# --- also, precedence in complex expressions becomes hard to reason about.

test_pexpr_series = [
    (
        '$ "plat" "vader"',
        r'(and (_text_ "plat") (_text_ "vader"))',
    ),
    (
        '$ "plat" "vader"',
        r'(and (_text_ "plat") (_text_ "vader"))',
    ),
    (
        '$ "plat" "van vader"',
        r'(and (_text_ "plat") (_text_ "van vader"))',
    ),
    (
        '$ "hdt" or "plat" "van vader"',
        r'(or (_text_ "hdt") (and (_text_ "plat") (_text_ "van vader")))',
    ),
    (
        '$ "stuff" "tuff" and "staff" "taff" not "stoff" and "steff"',
        '(and (and (and (_text_ "stuff") (_text_ "tuff")) (and (_text_ "staff") (_text_ "taff"))) (not (and (_text_ "stoff") (_text_ "steff"))))',
    ),

    (
        '$ "tuff" and "taff" not "stoff"',
        '(and (and (_text_ "tuff") (_text_ "taff")) (not (_text_ "stoff")))',
    ),
    (
        '$ "tuff" and "taff" not "stoff" and "steff"',
        '(and (and (_text_ "tuff") (_text_ "taff")) (not (and (_text_ "stoff") (_text_ "steff"))))',
    ),
    (
        '$ "tuff" not "stoff" and "steff"',
        '(and (_text_ "tuff") (not (and (_text_ "stoff") (_text_ "steff"))))',
    ),

    (
        '$ "x" and "y"',
        '(and (_text_ "x") (_text_ "y"))',
    ),
    (
        # (x & y) ! z
        '$ "x" and "y" not "z"',
        '(and (and (_text_ "x") (_text_ "y")) (not (_text_ "z")))',
    ),

    (
        # (x & y) ! (z | q)
        '$ "x" and "y" not "z" or "q"',
        '(and (and (_text_ "x") (_text_ "y")) (not (or (_text_ "z") (_text_ "q"))))',
    ),
]

test_all = [
    *test_pexpr_with_fieldname,
    *test_aexpr0,
    *test_aexpr_illegal,
    *test_pexpr0,
    *test_pexpr_series,
]

test = test_choose(test_all)

for query, expect in test:
    parse = get_parser(query, p_all_field='_text_')
    has_error, parsed, expr_type, full_context = parse()
    if expect is None:
        ok = has_error
        if ok:
            oks = green('✔')
            print('%s %s, expected error' % (oks, query))
        else:
            oks = red('✘')
            print('%s %s, expected error, got %s' % (
                oks, query, parsed))
    else:
        ok = not has_error and parsed == expect
        if ok:
            oks = green('✔')
            print('%s %s %s %s' % (oks, query, yellow('→'), parsed))
        else:
            oks = red('✘')
            print('%s %s has_error %s\n→ got      = %s\n→ expected = %s' % (
                oks, query, has_error, parsed, expect))

info('all done')
