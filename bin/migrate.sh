#!/usr/bin/env bash

set -e

bindir=$(realpath "$(dirname "$0")")

. "$bindir"/functions.bash

USAGE="""Usage: $0
 To use this script, first copy
 cp -a grne-lemmata/klaar voor migratie/maand grne/tmp
 then do migrate.sh"""

rootdir="$bindir"/..
tmpdir="$rootdir"/tmp

# we assume that grne and grne-dpc are in the same directory
migratordir="$rootdir"/../grne-dpc/grne/bin

while getopts h-: arg; do
    case $arg in
        h) warn "$USAGE"; exit 0 ;;
        -) OPTARG_VALUE="${OPTARG#*=}"
            case $OPTARG in
                help)  warn "$USAGE"; exit 0 ;;
                '')    break ;;
                *)     error "Illegal option --$OPTARG" ;;
                esac ;;
        *) error "$USAGE" ;;
    esac
done
shift $((OPTIND-1))

errors=()

num_errors=0

got-error () {
  errors+=("$@")
  let num_errors=num_errors+1
}

show-errors () {
  if [ "$num_errors" = 0 ]; then
    info 'no errors'
    return 0
  fi
  local e
  for e in "${errors[@]}"; do
    warn "$e"
  done
  warn "Number of directories with errors: $num_errors"
}

convert-to-xml () {
  local dirname=$1
  local j
  for j in "$dirname"/*.doc{,x}; do
    # if nothing is found, don't do the libreoffice command
    if [ "$j" = "$dirname"/'*.doc' -o "$j" = "$dirname"/'*.docx' ]; then
      continue
    fi
    quiet-cmd
    mci
    mcb libreoffice
    mcb --convert-to odt:"OpenDocument Text Flat XML"
    mcb --outdir "$dirname"/out
    mcb "$j"
    mcg
    noisy-cmd
  done
}

apply-migrator () {
  local dirname=$1
  local j
  local x
  local output=()
  for j in ./out/*.odt; do
    x="${j/.odt/""}"
    y=$(basename "$x")
    info "migrating $y"
    cmd "$migratordir"/migrator -o migrated/"$x" "$j" >& /tmp/migrator-out-this
    if grep -P 'total failures: [^0]' /tmp/migrator-out-this; then
      got-error "One or more lemmata failed in file $y"
    fi
    cat >>/tmp/migrator-out /tmp/migrator-out-this
  done
}

go () {
  local dirname
  info "tmpdir: $tmpdir"
  cmd rm -f /tmp/migrator-out
  # the output of 'find', which is run in another process, is directed into the while loop
  # the IFS is there to preserve whitespaces, the -r and -d to get correct line breaks
  while IFS= read -r -d '' dirname; do
	  echo "$dirname"
    convert-to-xml "$dirname"
    cwd "$dirname" apply-migrator "$dirname"
  done < <(find "$tmpdir" -mindepth 2 -type d -print0)
  info 'all done'
  fun show-errors
  echo ''
  info 'output saved to /tmp/migrator-out'
}; fun go
