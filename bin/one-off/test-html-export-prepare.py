#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
from subprocess import Popen, PIPE

import proj_paths # noqa

from acatpy.speak import green
from acatpy.io.speak import info, warn
from acatpy.io.io import die, cmd
from acatpy.util import pathjoin
from acatpy.xml import xslt_transform

html = ['<div><div>stiv</div></div>', 'def']

o = Popen(
    './html-export-prepare',
    stdin=PIPE,
    stdout=PIPE,
    cwd='..',
)

write = lambda x: o.stdin.write((x + '\n').encode())
flush = lambda: o.stdin.flush()
write_marker = lambda: write('=====')
read = lambda: o.stdout.readline().decode()
is_marker = lambda x: x == '=====\n'

write('div { color: blue; }')
write_marker()
flush()
for h in html:
    info('putting html: ' + h)
    write(h)
    write_marker()
    flush()
    res = []
    while True:
        x = read()
        if is_marker(x):
            info('response: %s' % res)
            break
        else:
            res.append(x)
