#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import proj_paths # noqa
import re

from acatpy.util import pathjoin

from grne.build.make_index import get_words_files

from config import get as config_get

config = config_get()
bin_path = config.main['bin_path']

out_path = pathjoin(bin_path, "../tmp/found.out")

def go ():

    (fns_bad_parse, fns_bad_status, fns_status_ok), errors, (num_ignored, num_files, num_unreadable) = get_words_files(warn_on_bad_status=False)
    if errors:
        die("Error(s) with get_words_files: %s" % errors)
    if fns_bad_parse:
        die("some bad parse")
    num_bad = num_unreadable - num_ignored
    if num_bad:
        die("some bad %s" % num_bad)
    all_fns = [*fns_bad_status, *fns_status_ok]

    # clear out_file
    with open (out_path, 'w') as fh_out:
        fh_out.write("")

    for fname in all_fns:
        if fname == '': continue
        with open(fname, 'r') as fh:
            with open (out_path, 'a') as fh_out:
                s = fh.read ()
                if re.search(r'</toel></vertM>\s*\n\s*</niv123>', s):
                    continue
                if re.search(r'</toel></vertM>\s*\n\s*</cit>', s):
                    continue
                if re.search(r'</toel></gebrW>\s*\n\s*</niv123>', s):
                    continue
                if re.search(r'</toel></gebrW>\s*\n\s*</cit>', s):
                    continue
                if re.search(r'</r></gebrW>\s*\n\s*</cit>', s):
                    continue
                if re.search(r'</r></gebrW>\s*\n\s*</cit>', s):
                    continue
                if re.search(r'</r></vertM>\s*\n\s*</cit>', s):
                    continue
                if re.search(r'</r></vertM>\s*\n\s*</cit>', s):
                    continue
                ret1 = re.search(r'</toel></vertM>\s*\n[^n]+remove_this', s)
                ret2 = re.search(r'</toel></gebrW>\s*\n[^n]+remove_this', s)
                ret3 = re.search(r'</r></vertM>\s*\n[^n]+remove_this', s)
                ret4 = re.search(r'</r></gebrW>\s*\n[^n]+remove_this', s)
                if ret1 or ret2 or ret3 or ret4:
                    fh_out.write("%s\n" % fname)

go ()

