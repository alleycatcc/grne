#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re

import proj_paths # noqa

from lxml import etree

from grne.build.make_index import get_words_files
from grne.util.util import get_xml_from_file
from acatpy.speak import green
from acatpy.io.speak import info, warn
from acatpy.io.io import die
from acatpy.xml import xslt_transform

def tostring(node):
    return etree.tostring(node, encoding='unicode')

def print_etym(xml):
    etyms = xml.findall('//etym')
    assert len(etyms) < 2
    if not etyms:
        return
    etym = etyms[0]
    print('etym %s' % tostring(etym))

def remove_etym_link(filename):
    xml = get_xml_from_file(filename)
    assert xml is not None
    # print_etym(xml)
    err, transformed = xslt_transform(xml, xslt())
    if err:
        die(err)
    # print('transformed %s' % tostring(transformed))
    return(transformed)

def post_process(xml):
    xml_str = tostring(xml)
    xml_str = re.sub(r'(<\?[^>]+>)', r'\1\n', xml_str)
    doctype = '<!DOCTYPE contLem SYSTEM "../../hulpdocumenten/dtd/lemma.dtd">'
    return '%s\n%s' % (doctype, xml_str)

def go():
    # --- ignored = hulpdocumenten etc.
    (fns_bad_parse, fns_bad_status, fns_status_ok), errors, (num_ignored, num_files, num_unreadable) = get_words_files(warn_on_bad_status=False)
    if errors:
        die("Error(s) with get_words_files: %s" % errors)
    if fns_bad_parse:
        die("some bad parse")
    num_bad = num_unreadable - num_ignored
    if num_bad:
        die("some bad %s" % num_bad)
    all_fns = [*fns_bad_status, *fns_status_ok]
    num = len(all_fns)
    assert num == num_files
    todo = {}
    for filename in all_fns:
        transformed = remove_etym_link(filename)
        todo[filename] = post_process(transformed)
    wrote = 0
    for filename, newstring in todo.items():
        with open(filename, 'w') as f:
            f.write(newstring)
            wrote += 1
    info("Wrote %s file(s)" % green(wrote))
    warn('This script might insert newlines after processing-instructions, i.e., Oxygen comments. Please check before committing.')

def xslt():
    return r'''
    <xsl:stylesheet
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      version="1.0"
    >
      <xsl:template match="//etym//link">
        <xsl:apply-templates select="node()"/>
      </xsl:template>
      <xsl:template match="node()|@*|processing-instruction()">
        <xsl:copy>
          <xsl:apply-templates select="node()|@*|processing-instruction()"/>
        </xsl:copy>
      </xsl:template>
    </xsl:stylesheet>
    '''

go()
