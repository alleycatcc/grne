#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import proj_paths # noqa

import sys
from functools import namedtuple

import pydash

from acatpy.colors import bright_red, green, underline, yellow
from acatpy.io.io import die
from acatpy.io.speak import say, sayn, warn, info
from acatpy.io.term import init as term_init
from acatpy.io.term import term_rawlike, term_clear, getch_combo, go_up
from acatpy.solr_search import init as solr_search_init

from grne.query_parser.parse import get_parser
from grne.util.pretty_print import pretty_print

from config import get as config_get

symbol_star = '\x2a\x11'
symbol_integral = '\u2233'

class ProgrammerException(Exception):
    pass

class Input(object):
    def __init__(self):
        self.input = []
        self.has_parse = False
        self._cursor_pos = 0

    def input_s(self):
        return ''.join(self.input)

    def input_s_display(self):
        inp = self.input + [' ']
        cp = self._cursor_pos
        x = lambda n: underline(inp[n]) if n == cp else inp[n]
        i = (x(n) for n in range(0, len(inp)))
        return ''.join(i)

    def delete(self):
        cp = self._cursor_pos
        il = len(self.input)
        if cp == il: return
        self.input.pop(cp)

    def backspace(self):
        cp = self._cursor_pos
        if cp == 0: return
        self.input.pop(cp - 1)
        self._cursor_move(-1)

    def key(self, ch):
        cp = self._cursor_pos
        self.input.insert(cp, ch)
        self._cursor_move(len(ch))

    def _cursor_move(self, rel=None, abso=None, named=None):
        cp = self._cursor_pos
        il = len(self.input)
        if rel is not None:
            cp += rel
        elif abso is not None:
            cp = abso
        elif named == 'start':
            cp = 0
        elif named == 'end':
            cp = il
        else:
            raise ProgrammerException('bad call to _cursor_move')
        self._cursor_pos = max(0, min(il, cp))

    def named_key(self, name):
        if name == 'arrow-left':
            self._cursor_move(-1)
            return False
        if name == 'arrow-right':
            self._cursor_move(1)
            return False
        if name == 'home':
            self._cursor_move(named='start')
            return False
        if name == 'end':
            self._cursor_move(named='end')
            return False

        if name == 'ctrl-u':
            self.input = []
            self._cursor_move(named='start')
        elif name == 'ctrl-a':
            self._cursor_move(named='start')
        return True

class UI(object):
    Parsed = namedtuple('Parsed', [
        'has_errors_general',
        # --- a-expr ('alleycat')
        'has_errors_a', 'text_a',
        # --- s-expr ('solr')
        'has_errors_s', 'text_s',
    ])

    def __init__(self, the_input, prompt='', update=True):
        self.prompt = prompt
        self.no_input_indicator = symbol_integral
        self._input = the_input
        self.parsed = None, None, None, None, None

        self._prev_a_line_len = None
        self._prev_s_line_len = None
        self._prev_input_len = None
        self._lines_printed = None

        self._init_output()
        if update: self.update()

    @property
    def parsed(self):
        return self._parsed

    @parsed.setter
    def parsed(self, v):
        self._parsed = UI.Parsed(*v)

    def _init_output(self):
        self._prev_a_line_len = 0
        self._prev_s_line_len = 0
        self._prev_input_len = 0
        self._lines_printed = 0

    def _get_a_line(self, has_errors_a, text_a):
        if has_errors_a:
            return bright_red('✘')
        if text_a:
            return '%s %s' % (green('✔'), text_a)
        return '%s' % yellow(self.no_input_indicator)

    def _get_s_line(self, has_errors_s, text_a, text_s):
        if has_errors_s:
            return bright_red('✘')
        if text_s:
            return '%s %s' % (green('✔'), text_s)
        return '%s' % yellow(self.no_input_indicator)

    def _get_lines(self):
        has_errors, has_errors_a, text_a, has_errors_s, text_s = self.parsed

        if has_errors:
            return bright_red('✘'), bright_red('✘')

        a_line = self._get_a_line(has_errors_a, text_a)
        s_line = self._get_s_line(has_errors_s, text_a, text_s)

        return a_line, s_line

    def _get_parse_ui(self):
        a_line, s_line = self._get_lines()

        if s_line: s_line = 's ' + s_line
        if a_line: a_line = 'a ' + a_line

        return s_line, a_line

    def reset_buffer(self, do_update=True):
        self._init_output()
        if do_update: self.update()

    def update(self):
        s_line, a_line = self._get_parse_ui()
        s = self._input.input_s_display()

        go_up(num_lines=self._lines_printed - 1)
        say('\r%s' % (' ' * self._prev_s_line_len))
        say('\r%s' % (' ' * self._prev_a_line_len))
        sayn('\r%s' % (' ' * self._prev_input_len))

        go_up(num_lines=self._lines_printed - 1)
        say('\r%s' % s_line)
        say('\r%s' % a_line)
        sayn('\r%s' % s)

        self._prev_s_line_len = len(s_line)
        self._prev_a_line_len = len(a_line)
        self._prev_input_len = len(s)

        self._lines_printed = 3

def input_loop(searcher=None, term_prepare=None, term_restore=None):
    the_input = Input()
    ui = UI(the_input, prompt='٭ ')

    # --- k_xxx functions return True to mean that the input loop should
    # immediately continue.

    def k_enter():
        # --- @todo not pretty to use ui.parsed
        parsed = ui.parsed
        if not parsed: return True
        err_general, _, _, err_ps, text_s = parsed
        if not text_s: return True
        solr_errors, rs = searcher.search(text_s)
        term_restore()
        sayn('\n\r')
        if solr_errors:
            for e in solr_errors:
                warn("Got solr error: %s" % e)
            ui.reset_buffer()
        elif not rs:
            say('No results')
            ui.reset_buffer()
        else:
            for r in rs:
                pretty_print(r)
            say('')
            num = rs.hits
            info('Found %s result%s' % (green(num), '' if num == 1 else 's'))
            ui.reset_buffer()
        term_prepare()

    def k_named(name):
        needs_reparse = the_input.named_key(name)
        if not needs_reparse:
            ui.update()
            return True

    def k_other(keys):
        ignored = len(keys) - 1
        if ignored: warn('ignoring %s keystrokes' % ignored)
        ch, och = keys[0]
        if och:
            if och < 32:
                return True
            the_input.key(ch)
        else:
            raise ProgrammerException('no key')

    def parse_and_update_ui():
        input_s = the_input.input_s()
        if not input_s:
            ui.parsed = None, None, None, None, None
        else:
            has_errors, parsed, expr_type, _ = get_parser(input_s, p_all_field='_text_')()
            if has_errors:
                ui.parsed = True, None, None, None, None
            elif expr_type == 'a-expr':
                ui.parsed = False, False, input_s, False, parsed
            elif expr_type == 'p-expr':
                text_a = parsed
                has_errors_s, text_s, _, _ = get_parser(text_a)()
                ui.parsed = False, False, text_a, has_errors_s, text_s
            else:
                raise ProgrammerException('unexpected')
        ui.update()

    while True:
        name, keys = getch_combo()
        if name:
            if name == 'enter' and k_enter():
                continue
            if name == 'backspace':
                the_input.backspace()
            elif name == 'delete':
                the_input.delete()
            elif k_named(name):
                continue
        elif k_other(keys):
            continue

        parse_and_update_ui()

def go(target=None):
    target = target or 'tst'
    config = config_get()
    conf_local = config.local
    solr_cli = conf_local.get('solr_cli')
    solr_url = solr_cli.get(target)
    if not solr_url:
        die('No solr_cli url found for target %s' % target)
    searcher = solr_search_init(solr_url)
    term_init()
    term_clear()
    term_restore = term_rawlike()
    try:
        input_loop(searcher=searcher, term_prepare=term_rawlike, term_restore=term_restore)
    except Exception as e:
        term_restore()
        raise e
    term_restore()


go(target=pydash.get(sys.argv, 1))
