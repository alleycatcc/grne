#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import proj_paths # noqa

import json
from random import randint
import re
from sys import argv

from lxml import etree

from grne.build.make_index import get_words_files
from grne.chars import regex_greek

from grne.util.util import (
    ProgrammerException,
    get_xml_from_file,
)

from common import get_first_complete_hoofdW

from acatpy.speak import green, yellow
from acatpy.io.io import cmd, die
from acatpy.io.speak import info, warn

def tostring(node):
    return etree.tostring(node, encoding='unicode', pretty_print=True)

def get_words(also_status_ok=None, letter=None):
    # --- ignored = hulpdocumenten etc.
    (fns_bad_parse, fns_bad_status, fns_status_ok), errors, (num_ignored, num_files, num_unreadable) = get_words_files(verbose_cmds=False, warn_on_bad_status=False, also_status_ok=also_status_ok, letter=letter)
    if errors:
        # --- these warnings have probably already been printed, but print
        # the filenames again anyway.
        warn("Error(s) with get_words_files:\n%s" % '\n'.join('  %s' % x for x in errors))
    if fns_bad_parse:
        warn("Unable to parse %d files" % len(fns_bad_parse))
    num_bad = num_unreadable - num_ignored
    if num_bad:
        die("Unable to process %d files (not unreadable and not ignored)" % num_bad)
    if fns_bad_status:
        warn("Not going to perform transformation on %d files which have bad status (e.g. empty, in bewerking, etc.)" % len(fns_bad_status))
    return fns_bad_status, fns_status_ok

def rand_pluck(xs):
    return xs[randint(0, len(xs)-1)]

def chop(x, *ns):
    ret = []
    # xs = re.findall(r'(\w|%s)+' % regex_greek, x)
    xs = re.findall(r'\w+', x)
    xsl = len(xs)
    for n in ns:
        if xsl < n: continue
        for l in range(0, xsl - n + 1):
            chunk = xs[l:l+n]
            ret.append(' '.join(chunk))
    return ret

def do_node(node, ns, transform=None):
    if node is None: return []
    text = node.text
    if text is None: return []
    if transform is not None:
        text = transform(text)
    return chop(text, *ns)

def get_which():
    m = randint(0, 2)
    if m == 0:
        which = 'citG'
        thing = '_text_'
    elif m == 1:
        which = 'hoofdW'
        thing = 'hoofdW-combined'
    elif m == 2:
        which = 'vertM'
        thing = 'vertM-combined'
    return which, thing

def quote(x):
    return json.dumps(x)

def mk_query(chunks):
    query_part = ' or '.join(quote(quote(y)) for y in chunks)
    query = ' '.join((
        query_part,
        '∃',
        quote('_text_'),
    ))
    return quote('$ ' + query)

def mk_post_data(query):
    return '{"data":{"query":%s,"num_rows":50,"start_row":0}}' % query

def do_curl(query):
    post_data = mk_post_data(query)
    the_cmd=[
        'curl', 'https://woordenboekgrieks.nl/api/advanced-search/search',
        '-H', 'content-type: application/json',
        '--data-raw',
        post_data(query),
    ]
    cmd(the_cmd)


def go():
    num_queries = process_args()
    # info('Going to generate %s queries' % yellow(str(num_queries)))

    ret = {
        'citG': [],
        'hoofdW': [],
        'vertM': [],
    }
    _, all_filenames = get_words(letter='Alpha')
    for filename in all_filenames:
        tree = get_xml_from_file(filename)
        ret['vertM'].extend(do_node(tree.find('//vertM'), [1, 2, 3]))
        ret['hoofdW'].extend(do_node(tree.find('//hoofdW'), [1],
                                     transform=get_first_complete_hoofdW))
        # --- just the first citG etc.
        ret['citG'].extend(do_node(tree.find('//citG'), [3, 4, 5]))
    # print(ret['citG'])
    # print(ret['hoofdW'])
    # print(ret['vertM'])
    # --- writes the last one to /tmp/post
    for n in range(0, num_queries):
        which, thing = get_which()
        data = ret[which]
        chunks = []
        for m in range(0, 3):
            chunks.append(rand_pluck(data))
        query = mk_query(chunks)
        # do_curl(query)
        with open('/tmp/post', 'w') as fh:
            fh.write(mk_post_data(query))

def usage():
    script_name = abspath(argv[0])
    return '''Usage: %s <args>
<args> = -h/--help | [num-queries=1000]
''' % script_name

def process_args():
    _, *args = argv
    if len(args) > 1:
        die(usage())
    if len(args) == 0:
        return 1000
    return int(args[0])

go()
