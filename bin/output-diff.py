#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
This script is deprecated -- no longer necessary since the migration.
But it can be a useful reference for diffing JSON + XML.
'''

import proj_paths # noqa

from glob import glob
import json
from json.decoder import JSONDecodeError
import os
from os import path
from pprint import pprint
import re
from sys import stdout, exit
import tempfile

from lxml import etree
from lxml.etree import XMLSyntaxError
import xmldiff.main
from jsondiff import update as jsondiff_update, diff

from acatpy.io.speak import info, say
from acatpy.io.io import die, cmd, rm
from acatpy.colors import yellow, bright_red, red
from acatpy.util import pathjoin, is_string

from config import get as config_get

SHOW_JSON_DIFF = True

def config_script():
    return {
        'version_test': 'generate',
        'version_control': 'generate-2013-r944-all-noclean,noafkaut,spanshift-v0.22',
    }

def check_exists(*paths):
    for the_path in paths:
        if path.exists(the_path): continue
        die("Missing path: %s" % red(the_path))

def load_json(filename):
    with open(filename) as fh:
        try:
            return json.load(fh)
        except JSONDecodeError:
            info('Bad JSON in file: %s' % bright_red(filename))
    return None

def any_none(*x):
    for y in x:
        if y is None:
            return True
    return False

def is_xml(s):
    return s.find('xml') != -1

def xml_from_string(s):
    try: return etree.fromstring(s)
    except XMLSyntaxError: info("Can't read xml")

def xml_diff_xmldiff(xml_c, xml_t):
    tree1 = xml_from_string(xml_c)
    tree2 = xml_from_string(xml_t)
    if (any_none(tree1, tree2)):
        return info('Aborting xml diff')
    diff = xmldiff.main.diff_trees(tree1, tree2)
    return diff

def xml_diff_linediff(xml_c, xml_t):
    fd_c, filename_c = get_tempfile(prefix='control-xml-')
    fd_t, filename_t = get_tempfile(prefix='test-xml-')
    os.write(fd_c, xml_c.encode())
    os.write(fd_t, xml_t.encode())
    os.close(fd_c)
    os.close(fd_t)
    ret = cmd(
        ['diff', '-u', filename_c, filename_t],
        capture_stdout=True,
        die=False,
        complain=False,
    )
    rm(filename_c, verbose=False)
    rm(filename_t, verbose=False)
    return ret.out

# --- e.g. os.write(fd, x.encode()); os.fsync(fd); os.close(fd); rm(filename)
def get_tempfile(prefix=None, suffix=None, **kw):
    fd, filename = tempfile.mkstemp(prefix=prefix, suffix=suffix, **kw)
    return fd, filename

def eval_iter(iterable):
    for _ in iterable: pass

def escape_string(x):
    if not is_string(x): return x
    return "'%s'" % re.sub("'", "\\'", x)

def display_diff(the_diff, obj_c, obj_t, filenames):
    item_updates = None
    if isinstance(the_diff, list):
        print('the_diff %s' % the_diff)
        raise Exception('the_diff is a list')
    for k1, diff_item in the_diff.items():
        filename_t, filename_c = filenames[k1]
        item_updates = []
        update = diff_item.get(jsondiff_update)
        if not update: continue
        for k2, v in update.items():
            if not is_xml(v): continue
            xml_t, xml_c = v, obj_c[k1][k2]
            item_updates.append('%s\n' % xml_diff_xmldiff(xml_c, xml_t))
            item_updates.append('xml control:\n^%s$' % xml_c)
            item_updates.append('xml test:\n^%s$' % xml_t)
            item_updates.append('line diff: filename-test=%s, filename-control=%s:\n%s\n' % (filename_t, filename_c, xml_diff_linediff(xml_c, xml_t)))
        if item_updates:
            key_string = '/'.join(str(escape_string(x)) for x in (k1, k2))
            say(' item key %s, xml_diffs:' % yellow(key_string))
            eval_iter(say('  ' + x) for x in item_updates)
    if SHOW_JSON_DIFF:
        info('json diff:')
        pprint(the_diff)
    stdout.flush()

def do_jsondiff(file_c, file_t):
    obj_c, obj_t = [load_json(x) for x in [file_c, file_t]]
    if (any_none(obj_c, obj_t)):
        return info('Aborting diff')
    if len(obj_c) != len(obj_t):
        raise Exception('obj_c and obj_t have different lengths')
    obj_t_new = []
    obj_c_new = []
    filenames = []
    for ot, oc in zip(obj_t, obj_c):
        # --- words
        if isinstance(ot, dict):
            # skip_diff = ot.pop('skip_diff', None)
            # filename = ot.pop('filename', 'error, no filename')
            filename_t = ot.pop('filename')
            filename_c = oc.pop('filename')
            filename = (filename_t, filename_c)
            """
            if skip_diff:
                # NB, msg out of order
                info('[ignore] skipping diff, reason="%s"' % skip_diff)
                continue
            """
        else:
            filename = ('n/a (only words)', 'n/a (only words)')
        filenames.append(filename)
        obj_c_new.append(oc)
        obj_t_new.append(ot)
    the_diff = diff(obj_c_new, obj_t_new, syntax='explicit')
    if the_diff:
        say('\n--- %s\n+++ %s' % (file_c, file_t))
        display_diff(the_diff, obj_c_new, obj_t_new, filenames)
        return True
    return False

def diff_words(pathc, patht):
    has_diff = None
    entries_path_c, entries_path_t = [pathjoin(x, 'entries') for x in [pathc, patht]]
    filesc = glob('%s/words*.js' % entries_path_c)
    for the_file_c in filesc:
        base = path.basename(the_file_c)
        the_file_t = pathjoin(entries_path_t, base)
        if not path.exists(the_file_t):
            die("Missing file: %s" % bright_red(the_file_t))
        if do_jsondiff(the_file_c, the_file_t):
            has_diff = True
    return bool(has_diff)

def go():
    config = config_get(config_script=config_script())
    conf_main = config.main
    conf_script = config.script
    js_path = conf_main['web_legacy_js_path']
    pathc = pathjoin(js_path, conf_script['version_control'])
    patht = pathjoin(js_path, conf_script['version_test'])
    check_exists(pathc, patht)
    has_diff = diff_words(pathc, patht)
    if has_diff:
        info('Diffs not clean, exit 1')
        exit(1)
    info('Diffs are clean!')
    exit(0)


go()
