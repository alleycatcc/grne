#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
This script can be run in two modes, 'identity' mode and 'transform-t1'
mode.

In identity mode it runs through all the lemmata in the repo which can be
parsed and runs the 'identity transform' on them: parse the XML and then
deparse it again into a string. Currently this still produces a diff, for
example, the newline at the end of the file, other whitespace changes, and
things like `<label></label>` getting turned into `</label>`.

In transform-t1 mode it runs the T1 transform on all lemmata with an ok
status (not 'in bewerking'), and which do not contain the `<status-t1>`
metadata field, which means it hasn't already been run on that lemma. After
a lemma is transformed it gets a `<status-t1>` field with the value
`uitgevoerd`.

'''

import proj_paths # noqa

from os.path import abspath
from sys import argv, exit

from lxml import etree

from grne.build.make_index import get_words_files
from grne.school_reductie_xslt import xslt_t1_0, xslt_t1_1, xslt_t1_2, xslt_t1_3, xslt_t1_update_status

from grne.util.util import (
    ProgrammerException,
    get_xml_from_file,
    get_xml_status_t1,
)

from acatpy.speak import green
from acatpy.io.io import die
from acatpy.io.speak import info, warn
from acatpy.xml import xslt_transform

# xslt_transform
# This happens in acatpy/xml.py
# arguments:
# -xml, which is either a tree or a string
# -xslt, which is a string
# -parse_xml (which depends on whether xml is a string
# -extensions
# -quiet (if True, this means that no warnings are given in case of errors)
# -throw (if True, this means that errors are 'raised'

# it transforms:
# xslt_root = XML(xslt)
# xml_tree  = etree.fromstring(xml.encode())  (if necessary)
#
# transform = XSLT(xslt_root, **xslt_kw)
# result    = transform(xml_tree)

# Commentaar Allen
# Je moet de t1 status checken van elk lemma, (veld 't1-status' oid),
# en kijken dat de t1 nog niet is gedraaid op dat lemma.
# Je zal een 5de xslt nodig hebben die de status na de transformatie op
# 't1-uitgevoerd' (oid) zet.
# En denk even na wat je wil dat er gebeurt bij error tijdens de xslt_transform stap.
# Exception gooien? Exception vangen? Doorgaan? Stoppen? Etc.

# --- The first line (DOCTYPE) gets erased by all transformations, so we
# manually replace it. Joris's stylesheets erase the next two lines as well.
# We could fix this, but that would require changing his stylesheets, which is
# risky now that we've used them for so long. So in that case we add a 'full'
# header.

def lemma_header(full=False):
    line1 = '<!DOCTYPE contLem SYSTEM "../../hulpdocumenten/dtd/lemma.dtd">'
    line2 = '<?xml-stylesheet type="text/xsl" href="../../hulpdocumenten/xslt/lemma-html.xslt"?>'
    line3 = '<?xml-stylesheet type="text/css" href="../../hulpdocumenten/css/lemma-blok.css"?>'
    if full:
        return '\n'.join((line1, line2, line3))
    return line1

def tostring(node):
    return etree.tostring(node, encoding='unicode', pretty_print=True)

def identity_with_filename(filename):
    return tostring(get_xml_from_file(filename))

def t1_transformation(filename):
    # filename is a string
    xml = get_xml_from_file(filename)
    # xml is an xml_tree (or node?)
    # --- status_t1: 'uitgevoerd', 'gecontroleerd', ''
    err, status_t1 = get_xml_status_t1(xml, default='', parse=False)
    if err:
        die(err)
    if status_t1 in ['uitgevoerd', 'gecontroleerd']:
        return None
    transformed = xml
    if not status_t1:
        try:
            transformed = xslt_transform(transformed, xslt_t1_0(), throw=True)
            transformed = xslt_transform(transformed, xslt_t1_1(), throw=True)
            transformed = xslt_transform(transformed, xslt_t1_2(), throw=True)
            transformed = xslt_transform(transformed, xslt_t1_3(), throw=True)
        except Exception as e:
            err = 'Error with T1 transform (%s): %s' % (filename, e)
            die(err)
        try:
            transformed = xslt_transform(transformed, xslt_t1_update_status(), throw=True)
        except Exception as e:
            err = 'Error with T1 status update (%s): %s' % (filename, e)
            die(err)
    ret = tostring(transformed)
    if ret is None:
        warn("Transformation resulted in empty node! (filename=%s)" % filename)
        return None
    return '\n'.join((lemma_header(full=True), ret))

def xslt_status_update(status_field, status_cur, status_new):
    status_attr = status_field
    return '''
<xsl:stylesheet
  version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
  <xsl:output method="xml" indent="no" omit-xml-declaration="yes" />
  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="{status_field}[@{status_attr}='{status_cur}']">
    <{status_field} {status_attr}="{status_new}"/>
  </xsl:template>
</xsl:stylesheet>
    '''.format(
        status_field=status_field, status_attr=status_attr,
        status_cur=status_cur, status_new=status_new,
    )

def do_xslt(filename, xslt):
    xml = get_xml_from_file(filename)
    try:
        transformed = xslt_transform(xml, xslt, throw=True)
    except Exception as e:
        err = 'Error with xslt transform (%s): %s' % (filename, e)
        die(err)
    ret = tostring(transformed)
    if ret is None:
        warn("Transformation resulted in empty node! (filename=%s)" % filename)
        return None
    return '\n'.join((lemma_header(), ret))

def update_status_empty_xml_ok(filename):
    return do_xslt(filename, xslt_status_update('status', '', 'xml ok'))

def update_status_t1_uitgevoerd_gecontroleerd(filename):
    return do_xslt(filename, xslt_status_update('status-t1', 'uitgevoerd', 'gecontroleerd'))

def get_words(also_status_ok=None):
    # --- ignored = hulpdocumenten etc.
    (fns_bad_parse, fns_bad_status, fns_status_ok), errors, (num_ignored, num_files, num_unreadable) = get_words_files(warn_on_bad_status=False, also_status_ok=also_status_ok)
    if errors:
        # --- these warnings have probably already been printed, but print
        # the filenames again anyway.
        warn("Error(s) with get_words_files:\n%s" % '\n'.join('  %s' % x for x in errors))
    if fns_bad_parse:
        warn("Unable to parse %d files" % len(fns_bad_parse))
    num_bad = num_unreadable - num_ignored
    if num_bad:
        die("Unable to process %d files (not unreadable and not ignored)" % num_bad)
    if fns_bad_status:
        warn("Not going to perform transformation on %d files which have bad status (e.g. empty, in bewerking, etc.)" % len(fns_bad_status))
    return fns_bad_status, fns_status_ok

def go():
    mode = process_args()
    info("Running script in %s mode" % green(mode))
    all_filenames = None
    transform_fn = None
    if mode == 'identity':
        _, fns_status_ok = get_words(also_status_ok=[''])
        all_filenames = fns_status_ok
        transform_fn = identity_with_filename
    elif mode == 'transform-t1':
        _, fns_status_ok = get_words()
        all_filenames = fns_status_ok
        transform_fn = t1_transformation
    elif mode == 'update-status-empty-xml-ok':
        _, fns_status_ok = get_words(also_status_ok=[''])
        all_filenames = fns_status_ok
        transform_fn = update_status_empty_xml_ok
    elif mode == 'update-status-t1-uitgevoerd-gecontroleerd':
        _, fns_status_ok = get_words()
        all_filenames = fns_status_ok
        transform_fn = update_status_t1_uitgevoerd_gecontroleerd
    else:
        raise ProgrammerException('bad mode %s' % mode)
    num_transformations = 0
    for filename in all_filenames:
        transformed_str = transform_fn(filename)
        if transformed_str is None:
            continue
        num_transformations += 1
        with open(filename, 'w') as f:
            f.write(transformed_str)
    info("Transformed %s file(s)" % green(num_transformations))

def usage():
    script_name = abspath(argv[0])
    return '''Usage: %s <args>
<args> = -h/--help | <mode>
<mode> = --identity | --transform-t1 | --update-status-empty-xml-ok --update-status-t1-uitgevoerd-gecontroleerd
''' % script_name

def process_args():
    _, *args = argv
    if len(args) != 1:
        die(usage())
    arg, = args
    if arg == '-h' or arg == '--help':
        info(usage())
        exit(1)
    if arg == '--identity':
        return 'identity'
    if arg == '--transform-t1':
        return 'transform-t1'
    if arg == '--update-status-empty-xml-ok':
        return 'update-status-empty-xml-ok'
    if arg == '--update-status-t1-uitgevoerd-gecontroleerd':
        return 'update-status-t1-uitgevoerd-gecontroleerd'
    die(usage())


go()
