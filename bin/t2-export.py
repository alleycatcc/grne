#!/usr/bin/env python3
# -*- coding: utf-8 -*-

''' This script is meant to export all the t2 transformed lemmata. The
export consists of one file for each letter, and the letters should be in
the right order. The files are taken from the redis-persist-db, which has
as keys the hashes of the input files for the t2 transformation, and as
values the t2-transformed files. There is some extra information in the
xml that should be removed, for this we use a simple xslt transformation.
'''

import proj_paths # noqa

from functools import partial
from hashlib import sha256
from os.path import abspath
import re
from sys import argv, exit
from tempfile import mkdtemp

from lxml import etree

from acatpy.io.io import die
from acatpy.io.speak import info, warn, bright_red
from acatpy.xml import xslt_transform

from grne.build.make_index import (
    get_data_lemma,
    get_words_filenames,
    sort_lemmata,
)
from grne.util.util import (
    get_xml_status_t1,
    read_xml_from_file,
    print_exception,
    get_filename_stub,
    parse_xml,
)
from grne.util.util_redis import redis_cache, redis_client_get_persist
from grne.xslt import format_xslt, make_xslt_extensions
from grne.xslt_extensions import Extension

from common import get_first_complete_hoofdW

warn_on_fail_get_first_complete_hoofdW = True

# --- this list must be in the correct order

alphabet = [
    'alpha', 'beta', 'gamma', 'delta', 'epsilon',
    'zeta', 'eta', 'theta', 'iota', 'kappa', 'labda',
    'mu', 'nu', 'xi', 'omikron', 'pi', 'rho', 'sigma',
    'tau', 'upsilon', 'phi', 'chi', 'psi', 'omega',
]

to_string = partial(etree.tostring, encoding='unicode')

lemmata_to_skip = []
debug_only_lemmata = [
    # '/home/alleycat/grne-lemmata/Alpha/Alpha.1.30/ἁλιο.xml',
    # '/home/alleycat/grne-lemmata/Alpha/Alpha.1.30/ἀλίθιος.xml',
    # '/home/alleycat/grne-lemmata/Sigma/Sigma.1.49/σχέθω.xml',
]

class ProgrammerException(Exception):
    pass

class ExtensionFillIndexIItem(Extension):
    def execute(self, context, self_node, input_node, output_parent):
        arguments = self.process_children(context)
        if len(arguments) != 1:
            raise Exception('Expected exactly 1 child(ren) for ExtensionFillIndexIItem')
        (hoofdW_node,) = arguments
        output_parent.text = get_first_complete_hoofdW(hoofdW_node.text, warn_on_fail=warn_on_fail_get_first_complete_hoofdW) or ''

def get_alphabet_with_idx():
    idx = 0
    def inner(letter):
        nonlocal idx
        idx += 1
        return idx, letter
    return { letter: inner(letter) for letter in alphabet }

def is_only_whitespace(s):
    return re.match(r'^\s*$', s)

def do_xslt_transform(xml, homonym_nr):
    xslt_tmpl, hoofdW = get_xslt(homonym_nr)
    xslt = format_xslt(xslt_tmpl, {'hoofdW': hoofdW})
    extensions = make_xslt_extensions([
        ('fill-indexI-item', ExtensionFillIndexIItem),
    ])
    return xslt_transform(xml, xslt, extensions=extensions, throw=True)

def do_letter(redis_client, tmpdir, running_headers, letter_info):
    idx, letter = letter_info
    output_filename = '%s/%02d-%s.xml' % (tmpdir, idx, letter)
    info('------ Processing letter: %s' % letter)
    with open(output_filename, 'w') as fh:
        lemmata, homonyms = get_data_lemma(letter=letter.capitalize(),
                                           verbose_cmds=False)
        if len(lemmata) == 0:
            raise Exception('no lemmata returned for pattern %s' % letter)
        lemmata = sort_lemmata(lemmata)
        if debug_only_lemmata:
            info('Only doing the following lemma(ta) (see debug_only_lemmata):\n' +
                '\n'.join(('    ' + x for x in debug_only_lemmata))
            )
        # The keys of the redis_persist_db are given by
        # 't2:t2_0HASH', where HASH is the hash of encoded t2_input.
        # This follows from /lib/make_site.
        for _lemma, naam, homonym_nr, filename, _indexI in lemmata:
            if filename in lemmata_to_skip:
                info('Skipping %s (see lemmata_to_skip)' % bright_red(filename))
                continue
            if debug_only_lemmata and filename not in debug_only_lemmata:
                continue
            real_homonym_nr = None
            if homonyms.has(naam):
                real_homonym_nr = homonym_nr
            msg_filename = get_filename_stub(filename)
            xml = read_xml_from_file(filename)
            # The composition of parse_xml and to_string should probably be the identity,
            # but that is not the case.
            parsed = parse_xml(xml, throw=True)
            err, status_t1 = get_xml_status_t1(parsed, default='', parse=False)
            if err: die(err)
            if status_t1 == '' or status_t1 not in ['uitgevoerd', 'gecontroleerd']:
                warn('Skipping file %s (status-t1 is not uitgevoerd or gecontroleerd)' % (filename))
                continue
            t2_input = to_string(parsed)
            t2_input_bytes = t2_input.encode()
            hashed_input = sha256(t2_input_bytes).hexdigest()
            try:
                is_cache_hit, transformed = redis_cache(
                    redis_client, 't2:t2_0', None, (),
                    key_extra=hashed_input,
                    serialise_result=to_string,
                    deserialise_hit=etree.fromstring,
                    extended_return=True,
                )
            except Exception as e:
                warn('Error accessing redis cache (filename=%s) (stacktrace follows)' % filename)
                print_exception()
                raise e
            if transformed is None:
                die('T2 cache not found: (file=%s t2-cache-key=%s): xml follows: %s' %
                    (msg_filename, 't2:t2_0:' + hashed_input, t2_input))
            try:
                xml_clean = do_xslt_transform(transformed, real_homonym_nr)
            except Exception as e:
                warn('Error with xslt transform (filename=%s) (stacktrace follows)' % filename)
                print_exception()
                raise e
            # --- keep track of indexI_item for the running headers. We could
            # have done this earlier, but we do it here in case the earlier
            # logic changes at some point.
            indexI_item = xml_clean.findall('//indexI/item')
            if len(indexI_item) != 1:
                raise ProgrammerException('indexI/item is empty or more than 1')
            running_headers.append(indexI_item[0].text)
            str_clean = to_string(xml_clean)
            # --- remove all empty lines or lines containing only whitespace
            # (t2 leaves blank lines everywhere)
            str_clean = '\n'.join((x for x in str_clean.split('\n') if not is_only_whitespace(x)))
            str_clean = re.sub(r'\s*$', '', str_clean) + '\n'
            fh.write(str_clean)

'''
`homonym_nr` is None or a number.
- no PIs or comments
- remove attributes on <contLem>
- remove <metadata/>
- <link target="">x</link> -> x
- <geen-afk>x</geen-afk> -> x
'''
def get_xslt(homonym_nr):
    ident = '<xsl:apply-templates select="node()|@*"/>'
    hoofdW = ident
    if homonym_nr is not None:
        hoofdW = '%d. %s' % (homonym_nr, ident)
    return '''
<xsl:stylesheet
  version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:my="{ns}"
  extension-element-prefixes="my"
>
  <xsl:output method="xml" indent="no" omit-xml-declaration="yes" />
  <xsl:template match="processing-instruction()|comment()"/>
  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>
  <!-- This was a request by UL to (re)generate the first indexI/item, using
  a set of rules to extract it from <hoofdW>, so that the publisher can
  generate the running headers. -->
  <xsl:template match="metadata">
    <metadata>
      <indexI>
        <item>
          <my:fill-indexI-item>
            <xsl:element name="hoofdW">
              <xsl:value-of select="string(//hoofdW)"/>
            </xsl:element>
          </my:fill-indexI-item>
        </item>
      </indexI>
    </metadata>
  </xsl:template>
  <xsl:template match="link">
    <r>
      <xsl:apply-templates select="node()"/>
    </r>
  </xsl:template>
  <!-- Add homonym numbers -->
  <xsl:template match="hoofdW">
    <xsl:copy>{hoofdW}</xsl:copy>
  </xsl:template>
  <xsl:template match="geen-afk">
    <xsl:apply-templates select="node()"/>
  </xsl:template>
  <xsl:template match="/contLem">
    <contLem>
      <xsl:apply-templates select="node()"/>
    </contLem>
  </xsl:template>
</xsl:stylesheet>
    ''', hoofdW

def usage():
    script_name = abspath(argv[0])
    return '''Usage: %s { -h | --help | <args> }
<args> = all | <letters>
<letters> = { <letter> [...<letter>] }
<letter> = %s
''' % (script_name, ' | '.join(alphabet))

def enhance_alphabet(letters):
    alphabet_with_idx = get_alphabet_with_idx()
    for letter in letters:
        info = alphabet_with_idx.get(letter)
        if info is None:
            raise ProgrammerException('no idx for letter')
        idx, letter = info
        yield idx, letter

def process_args():
    _, *args = argv
    if len(args) == 0:
        die(usage())
    if len(args) == 1:
        arg = args[0]
        if arg == '-h' or arg == '--help':
            info(usage())
            exit(1)
        if arg == 'all':
            return enhance_alphabet(alphabet)
    for arg in args:
        if arg not in alphabet:
            die(usage())
    letters = args
    # --- sorting tuples works natively in python: (0, x) is less than (1,
    # y) for any x and y because tuples are sorted positionally.
    return sorted(enhance_alphabet(letters))

def go():
    letter_infos = process_args()
    err, redis_client = redis_client_get_persist()
    if redis_client is None:
        die("Couldn't get redis client, check connection params/try restarting redis. Error was: " + err)
    # this is the directory where the files will be printed.
    tmpdir = mkdtemp(dir='/tmp', prefix='t2-export-')

    info('Output directory: %s' % tmpdir)
    running_headers = []
    for letter_info in letter_infos:
        do_letter(redis_client, tmpdir, running_headers, letter_info)
    with open(tmpdir + '/headers.txt', 'w') as fh:
        fh.write('\n'.join(running_headers))

go()
