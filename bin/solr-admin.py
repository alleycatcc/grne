#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import proj_paths # noqa

import time

from grne.solr.admin import with_config as solr_with_config
from acatpy.io.speak import info, say

def go():
    solr = solr_with_config()
    script = [
        solr.start,
        solr.delete,
        solr.create,
        solr.config,
        solr.add_schema,
        solr.add_data,
    ]

    # script[:] = script[5:]

    for x in script:
        x()

    # --- important in docker builds to give asynchronous calls a chance to
    # finish.
    info('sleep 20')
    time.sleep(20)
    info('done sleeping')

def say_join(x, ch='\n'):
    say(ch.join(x))


go()
