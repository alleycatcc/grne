#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import proj_paths # noqa

from functools import partial
from os import path
import re
from sys import exit, stderr, argv

import pytest

from grne.util.util_tests import (
    Args,
    infinite_range,
    infinite_zip_x,
    is_collection,
    toets,
)

from grne.chars import (
    Spiritus_info,
    canonical,
    sort_plaats,
    get_spiritus_info,
    get_pure_spiritus_info,
    remove_macron,
    remove_breve,
    strip_markings,
    to_latin,
    to_greek_precise,
    expand_iota_subscriptum,
    normalise_names,
    normalise_hyphens,
    analyse_and_convert_query,
    regex_greek,
)

from grne.char_add_marking import (
    chars,
    xml_entities,
    # canonical_generate_from_entities,
    # canonical_dict_recurse,
    # canonical_dict_final,
)

from config import get as config_get

require_pytest3 = False

if require_pytest3 and path.basename(argv[0]) != 'pytest-3':
    stderr.write('Must be run using pytest-3.\n')
    exit(1)

def spiritus_info_empty(length):
    return Spiritus_info(
        '0' * length,
        '0' * length,
        [0] * length,
        False,
    )

config = config_get()

def test_sort_plaats():
    group = partial(toets, sort_plaats)
    # test for the first pattern in the function
    group('one of the args is -1', ["-1", "fdjskl"], -1)

    # tests for the second pattern
    test = partial(group, 'one of the 2 args only consists of letters')
    test(["abdh", "abdH"], 1)
    test(["ab", "a5"], 1)
    test(["ab", "a.c"], 1)
    test(["HJhofland", "hjHofland"], -1)

    # tests for the third pattern; everything else. It splits the string on .
    # and /s and letters. It removes the . . Then it compares the list
    # element by element.
    test = partial(group, 'main')
    test(["28.6", "28.11"], -1)
    test(["28.8", "28.8"], 0)
    test(["a.15", "15"], 1)
    test(["a.10", "a.15"], -1)
    test(["a 10", "a.10"], -1)
    test(["a .10", "a.10"], -1)
    test(["a 10", "a9"], -1)
    test(["157 b", "157a"], -1)
    test(["157 b", "157b"], -1)
    test(["28.1", "28,1"], -1)
    test(["157b", "157-b"], -1)
    test(["a 13", "a 14"], -1)
    test(["a.14", "a.13"], 1)
    test(["12.13.17", "12.6.17"], 1)
    test(["12.a.17", "12.6.17"], 1)

    test = partial(group, 'when the strings are the same')
    test(["12.13.14", "12.13.14"], 0)
    test(["ab i.13i", "ab i.13i"], 0)

# The following tests are written for functions that are part of Chars.pl

def test_remove_macron_s():
    def test(desc, args, expect):
        assert remove_macron(*args) == tuple(expect), desc
    test('normal letters', ["abcd"], ["abcd", 0])
    test('letters with macrons (from macron_map)', ["ᾱᾹῑῙ"], ["αΑιΙ", 4])
    test('combining macrons', ["θαα͞γ"], ["θααγ", 1])
    test('combining breves', ["ᾰ"], ["ᾰ", 0])
    test('breves', ["ᾰᾸ"], ["ᾰᾸ", 0])
    # @todo all entities from dtd

def test_remove_breve_s():
    def test(args, expect):
        assert remove_breve(*args) == tuple(expect)
    # test for letters with breves
    test(["ᾰᾸ"], ["αΑ", 2])
    # test for combining breves
    test(["a͝β"], ["aβ", 1])
    # test for letters with (combining) macrons
    test(["ᾱᾹα͞γ"], ["ᾱᾹα͞γ", 0])

def test_strip_markings():
    group = partial(toets, strip_markings)
    test = partial(group, 'no description')
    # some random letters
    test(("abcd", False), ("abcd", False))
    # test for letters without iota_subscriptum
    test(("Όϓἇ", False), ("Οϒα", True))
    # test for letters without iota_subscriptum, with iota_subscriptum
    # preserved
    test(("Όϓἇ", True), ("Οϒα", True))
    # test for letters with iota_subscriptum
    test(("ᾎᾬᾯᾨί", False), ("ΑΩΩΩι", True))
    # test for letters with iota_subscriptum, with iota_subscriptum
    # preserved
    test(("ᾎᾬᾯᾨί", True), ("ᾎᾬᾯᾨι", True))
    # test for letters with iota_subscriptum
    test(("ᾚᾙᾘ", False), ("ΗΗΗ", True))
    # test for letters with iota_subscriptum, with iota_subscriptum
    # preserved
    test(("ᾚᾙᾘ", True), ("ᾚᾙᾘ", False))
    test(("βᾱ", False), ("βα", True))

    group('combining macron & acutus', ("ἄκων", False), ('ακων', True))

def test_to_latin():
    group = partial(toets, to_latin)

    group('some random letters', "ᾀαβ", ("aab", True, []))
    test = partial(group, 'greek with iota subscriptum')
    test("ᾘᾀ", ("ha", True, []))
    test("ᾎᾬᾯᾨί", ("awwwi", True, []))
    test = partial(group, 'no description')
    test("βᾱ", ("ba", True, []))

def test_to_latin_iota_expand():
    def test(args, expect):
        assert to_latin(*args, True) == tuple(expect)

    # normal greek
    test(["αβ"], ["ab", True, []])
    # greek with iota_subscriptum (monoliet)
    test(["ᾘᾀ"], ["hiai", True, []])
    # roman letters
    test(["ab"], ["ab", False, []])
    # greek with iota subscriptum
    test(["ᾎᾬᾯᾨί"], ["aiwiwiwii", True, []])

def test_normalise_hyphens():
    group = partial(toets, normalise_hyphens)

    test = partial(group, 'main')
    test(['abc-'], ('abc', 1))
    test(['a-bc-'], ('abc', 2))
    test(['-a-bc-'], ('abc', 3))
    test(['-a---bc-'], ('abc', 5))

    test = partial(group, 'keep at beginning')
    test(Args(('abc',), { 'keep_at_beginning': True, }), ('abc', 0))
    test(Args(('a-bc',), { 'keep_at_beginning': True, }), ('abc', 1))
    test(Args(('a-bc-',), { 'keep_at_beginning': True, }), ('abc', 2))
    test(Args(('a---bc-',), { 'keep_at_beginning': True, }), ('abc', 4))
    test(Args(('-abc',), { 'keep_at_beginning': True, }), ('-abc', 0))
    test(Args(('-a-bc',), { 'keep_at_beginning': True, }), ('-abc', 1))
    test(Args(('-a-bc-',), { 'keep_at_beginning': True, }), ('-abc', 2))
    test(Args(('-a---bc-',), { 'keep_at_beginning': True, }), ('-abc', 4))

    test = partial(group, 'keep at end')
    test(Args(('abc',), { 'keep_at_end': True, }), ('abc', 0))
    test(Args(('a-bc',), { 'keep_at_end': True, }), ('abc', 1))
    test(Args(('-a-bc',), { 'keep_at_end': True, }), ('abc', 2))
    test(Args(('-a---bc',), { 'keep_at_end': True, }), ('abc', 4))
    test(Args(('abc-',), { 'keep_at_end': True, }), ('abc-', 0))
    test(Args(('a-bc-',), { 'keep_at_end': True, }), ('abc-', 1))
    test(Args(('-a-bc-',), { 'keep_at_end': True, }), ('abc-', 2))
    test(Args(('-a---bc-',), { 'keep_at_end': True, }), ('abc-', 4))

@pytest.mark.skipif(True, reason='needs update')
def test_analyse_and_convert_query():
    group = partial(toets, analyse_and_convert_query)

    def spiritus_info_empty(length):
        return Spiritus_info(
            '0' * length,
            '0' * length,
            [0] * length,
            False,
        )

    test = partial(group, 'no description')
    test(["abcd", True], ["abcd", "loose", spiritus_info_empty(4), []])
    test(["abcd", False], ["abcd", "loose", spiritus_info_empty(4), []])
    # --- 03C0, 03AC => 03C0, 03AC
    test(["πά", False], ["πά", "strict", spiritus_info_empty(2), []])
    # --- 03C0, 03AC => 03C0, 1F71
    test(["πά", True], ["πά", "strict", spiritus_info_empty(2), []])

    # --- skips the block with the position bug
    # --- => 1F71
    test(["a/", False], ["ά", "strict", spiritus_info_empty(1), []])
    test(["a/", True], ["ά", "strict", spiritus_info_empty(1), []])
    test(["αβ", False], ["ab", "loose", spiritus_info_empty(2), []])
    test(["αβ", True], ["ab", "loose", spiritus_info_empty(2), []])
    test(["πισος", False], ["pisos", "loose", spiritus_info_empty(5), []])
    test(["πισος", True], ["pisos", "loose", spiritus_info_empty(5), []])
    test(["α/β/", False], ["άβ", "strict", spiritus_info_empty(2), []])
    test(["α/β/", True], ["άβ", "strict", spiritus_info_empty(2), []])

    test(["ἱὉabἐᾘkk", True, True], ["ἱὉαβἐᾘκκ", "strict", Spiritus_info(
        '00000011',
        '00110000',
        [0, 0, 7, 7, 0, 0, -7, -7],
        True,
    ), []])

    test(["pa/l", False], ["πάλ", "strict", spiritus_info_empty(3), []])
    test(["pa/l", True], ["πάλ", "strict", spiritus_info_empty(3), []])
    test(["πῖsos", False], ["πῖσος", "strict", spiritus_info_empty(5), []])
    test(["πῖsos", True], ["πῖσος", "strict", spiritus_info_empty(5), []])
    test(["πa/l", False], ["πάλ", "strict", spiritus_info_empty(3), []])
    test(["πa/l", True], ["πάλ", "strict", spiritus_info_empty(3), []])
    test(["ā", True], ["ā", "loose", spiritus_info_empty(1), []])
    # breve monolith
    test(["βᾰ", False], ["βα", "strict", spiritus_info_empty(2), []])
    test(["βᾰ", True], ["βα", "strict", spiritus_info_empty(2), []])
    # breve combined
    test(["βᾰ", False], ["ba", "loose", spiritus_info_empty(2), []])
    test(["βᾰ", True], ["ba", "loose", spiritus_info_empty(2), []])
    # the tests below show that the opt 0 or 1 decides whether the canonical
    # map is applied. The two alphas on the right side are different.
    test(["βά", False], ["βά", "strict", spiritus_info_empty(2), []])
    test(["βά", True], ["βά", "strict", spiritus_info_empty(2), []])

def test_analyse_and_convert_query2():
    group = partial(toets, analyse_and_convert_query)

    test = partial(group, 'general')
    test(
        'βᾹ', ('βᾹ', 'strict', spiritus_info_empty(2), []),
        desc='macron monolith upper'
    )
    test(
        'βᾱ', ('βᾱ', 'strict', spiritus_info_empty(2), []),
        desc='macron monolith lower'
    )
    test(
        'βᾱ', ('βᾱ', 'strict', spiritus_info_empty(2), []),
        desc='macron combining lower'
    )

    test = partial(group, '1-character combining, spiritus right')
    test(('ἀ', True), ('ἀ', 'strict', Spiritus_info('0', '0', [0], False), []))
    test(('ἀ', False), ('ἀ', 'strict', Spiritus_info('0', '0', [0], False), []))

    test = partial(group, '1-character combining, acutus')
    test('ά', ('ά', 'strict', spiritus_info_empty(1), []))

    err1 = 'string must not begin with beta char'
    test = partial(group, 'complain if begin with beta char')
    test(("/O", False), (None, None, None, [err1]))

@pytest.mark.skipif(True, reason='needs update')
def test_canonical():
    combine = lambda *x: ''.join(x)
    group = partial(toets, canonical)
    test = partial(group, 'combining macron 0x0304')
    test('ᾱ', ('ᾱ', 1))

    test = partial(group, 'entities-3, various orders of combining')
    chs = (
        combine('α', chars.ac, chars.slc, chars.mc),
        combine('α', chars.ac, chars.mc, chars.slc),
        combine('α', chars.slc, chars.mc, chars.ac),
        combine('α', chars.slc, chars.ac, chars.mc),
        combine('α', chars.mc, chars.ac, chars.slc),
        combine('α', chars.mc, chars.slc, chars.ac),
    )
    for char, idx in infinite_zip_x(chs):
        test(
            char,
            (xml_entities['&alpha-macron-acutus-asper;'], 1),
            desc='alpha-macron-acutus-asper, char=%s, idx=%s' % (char, idx)
        )

    test = partial(group, 'entities-2, various orders of combining')
    chs = (
        combine('α', chars.mc, chars.ac),
        combine('α', chars.ac, chars.mc),
    )
    for char, idx in infinite_zip_x(chs):
        test(
            char,
            (xml_entities['&alpha-macron-acutus;'], 1),
             desc='alpha-macron-acutus, char=%s, idx=%s' % (char, idx)
            )

def test_spiritus_info():
    def to_canonical_and_get_info(s):
        can, _ = canonical(s)
        return get_spiritus_info(can)
    group = partial(toets, to_canonical_and_get_info)
    test = partial(group, '1-character combining, spiritus right')
    test('ἀ', Spiritus_info('0', '1', [7], True))
    test('abἀ', Spiritus_info('000', '100', [7, 0, 0], True))
    test = partial(group, '3-character combining, includes spiritus right')
    test('efᾱ́̓bc', Spiritus_info('00000', '00100', [0, 0, 7, 0, 0], True))
    test('defᾱ́̓bc', Spiritus_info('000000', '001000', [0, 0, 7, 0, 0, 0], True))
    test('ᾱ́̓', Spiritus_info('0', '1', [7], True))
    test('abᾱ́̓', Spiritus_info('000', '100', [7, 0, 0], True))

    test = partial(group, 'spiritus in several places')
    test('ἀἀἀ', Spiritus_info('000', '111', [7, 7, 7], True))
    test('ἀἁἀ', Spiritus_info('010', '101', [7, -7, 7], True))

def test_pure_spiritus_info():
    def to_canonical_and_get_info(s):
        can, _ = canonical(s)
        return get_pure_spiritus_info(can)
    group = partial(toets, to_canonical_and_get_info)
    test = partial(group, '1-character combining, spiritus right')
    test('ἀ', Spiritus_info('0', '0', [0], False))
    test('abἀ', Spiritus_info('000', '000', [0, 0, 0], False))
    test = partial(group, '3-character combining')
    test('ᾱ́̓', spiritus_info_empty(1))
    test('abᾱ́̓', spiritus_info_empty(3))

def test_to_greek_precise():
    group = partial(toets, to_greek_precise)
    test = partial(group, 'main')

    # test for some letters, notably the j, which doesn't change
    test(["aj"], ["αj", True, []])
    # the alphabet
    test(["abcdefghijklmnopqrstuvwxyz"], ["αβξδεφγηιjκλμνοπθρστυvωχψζ", True, []])
    # test for greek letters
    test(["αβξδεφγηιjκλμνοπθρστυvωχψζ"], ["αβξδεφγηιjκλμνοπθρστυvωχψζ", False, []])
    # test for sigma at the end of a word
    test(["abcdefghijklmnopqrs"], ["αβξδεφγηιjκλμνοπθρς", True, []])
    # fun test to see what happens with a greek sigma at the end of a word
    test(["αβσ"], ["αβς", True, []])
    # tests with beta code
    test(["a/e/"], ["άέ", True, []])
    test(["α/ε/"], ["άέ", True, []])
    test(["Ο/"], ["Ό", True, []])
    test(['ἀ\\\\'], ["ἂ", True, []])
    test(["ε)"], ["ἐ", True, []])
    test(["η("], ["ἡ", True, []])
    test(["ᾩ="], ["ᾯ", True, []])
    # the next test is a bit weird, it doesn't change the symbol, but does
    # say it's altered.altered
    test(["΅+"], ["΅", True, []])
    test(["Ἄ|"], ["ᾌ", True, []])
    test(["Υ&"], ["Ῡ", True, []])
    # see next comment
    test(["b("], ["β", True, []])
    # test to show that all beta characters which don't have a function
    # (i.e. don't combine with the previous character) are removed.
    test(["[][''//)']"], ["[][]", True, []])

    test(["/O"], ["", False, ['string must not begin with beta char']])

def test_expand_iota_subscriptum():
    def test(args, expect):
        assert expand_iota_subscriptum(*args) == expect

    test(["α"], "α")
    test(["ᾎᾬᾯᾨί"], "ΑἶΩἴΩἷΩἰί")
    test(["abcd"], "abcd")
    test(["Ω="], "Ω=")
    test(["ᾚᾙᾘ"], "ΗἲΗἱΗἰ")

def test_normalise_names():
    group = partial(toets, normalise_names)

    test = partial(group, 'main')
    test(Args(("abc",), { 'keep_at_end': True, }), "abc")
    test(Args(("abc-",), { 'keep_at_end': False, }), "abc")
    test(Args(("abc-",), { 'keep_at_end': True, }), "abc-")
    test(Args(("-a-b-c-",), { 'keep_at_end': True, }), "abc-")

    # the next tests shows that the function also removes dashes between
    # letters and numbers. In a comment in Chars.pm it was suggested
    # otherwise.
    test = partial(group, 'remove dashes between letters & numbers')
    test(Args(("-a-5-c-",), { 'keep_at_end': True, }), "a5c-")
    test(Args(("xx-yy-zz-1",), { 'keep_at_end': True, }), "xxyyzz1")
    test(Args(("----",), { 'keep_at_end': True, }), "-")
    test(Args(("----",), { 'keep_at_end': False, }), "")
    test(Args(("abc—d",), { 'keep_at_end': False, }), "abcd")
    test(Args(("abc－d",), { 'keep_at_end': False, }), "abcd")
    test(Args(("αβψ—δ",), { 'keep_at_end': False, }), "αβψδ")

    # tests for removing macrons and breves
    test = partial(group, 'remove macron/breve')
    test(["ᾱᾹῑῙ"], "αΑιΙ")
    test(["θαα͞γ"], "θααγ")
    test(["ᾰᾸ"], "αΑ")

def test_regex_greek():
    def f(s):
        m = re.match(r'^ (%s+) $' % regex_greek(), s, re.X)
        if m is None:
            return 0
        (t,) = m.groups()
        return len(t)

    group = partial(toets, f)
    test = partial(group, 'regex greek')

    test(('α',), 1)
    test(('αάέάέάέ',), 7)
    test(('a',), 0)
    test(('ὗ',), 1)
