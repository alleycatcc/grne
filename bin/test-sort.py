#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import proj_paths # noqa

import re
from random import randint

from grne.build.make_index import (
    sort_lemmata,
)

throw_away_case = False

def randomize_list(xs_orig):
    xs = xs_orig
    ret = []
    for i in range(0, len(xs)):
        # --- randint is inclusive
        x = xs.pop(randint(0, len(xs) - 1))
        ret.append(x)
    return ret

unique = lambda xs: list(dict.fromkeys(xs))
qw = lambda x: re.split(r'\s+', x)
expand_case = lambda x: (x.capitalize(), x)
elisie_combos = lambda e, x: (x, e + x, x + e, e + x + e)
expand_words = lambda l, ending: (l + ending,)

endings = [
    '',
    'αν',
    'ᾳv',
    'ην',
    'ῃν',
]

letters = randomize_list(qw('α β γ δ ε ζ η θ ι κ λ μ ν ξ ο π ρ σ τ υ φ χ ψ ω'))

elisies = [
    '’',
    # "'",
]

namen = (
    ec
    for l in letters
    for e in endings
    for word in expand_words(l, e)
    for c in expand_case(word)
    for el in elisies
    for ec in elisie_combos(el, c)
)

namen = unique(namen)

# print(list(namen))

def mk_lemma(naam):
    return ['_', naam, '_', '_', '_']

lemmata = sort_lemmata(
  (mk_lemma(naam) for naam in namen),
  throw_away_case=throw_away_case,
)
for lemma in lemmata:
  _, naam, _, _, _ = lemma
  print(naam)
