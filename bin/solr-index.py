#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# --- @todo deprecate script?

import proj_paths # noqa

from collections import deque

from sys import argv
from grne.solr_pre_index import process as solr_pre_index_process

from acatpy.io.io import die

class Report(object):
    def __init__(self):
        self.types = {}

    def update(self, type):
        cur = self.types.get(type) or 0
        cur += 1
        self.types[type] = cur

    def report(self):
        print('self.types %s' % self.types)

def reporter():
    types = {}

    def update(type):
        cur = types.get(type) or 0
        cur += 1
        types[type] = cur

    def x(type=None):
        if type: update(type)
        return types
    return x

def print_join(x, ch='\n'):
    print(ch.join(x))

def do_file(filename, verbose=True):
    xml_in = filename.read()
    err, xmls, _ = solr_pre_index_process(xml_in)
    if err:
        die("Error with solr pre index process: %s" % err)
    rep = reporter()
    consume(rep(type=type) for _, type in xmls)
    print_join('num %s: %s' % (x, y) for x, y in rep().items())

def consume(x):
    return deque(x)

def go(filename, verbose=True):
    with open(filename, 'r') as f:
        return do_file(f, verbose=verbose)


go(argv[1])
