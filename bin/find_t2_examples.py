#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re

import proj_paths # noqa

from lxml import etree

from acatpy.speak import green
from acatpy.io.speak import info, warn
from acatpy.io.io import die, cmd
from acatpy.util import pathjoin
from acatpy.xml import xslt_transform, xslt_transform_saxon_java

from grne.build.make_index import get_words_files
from grne.util.util import get_xml_from_file

from config import get as config_get

config = config_get()


# --- config


action = "search"
# action = "transform"

# files = "t2-lemmata"
files = "all-lemmata"

if action == "transform":
    output_filename = "/tmp/out-t2.xml"
else:
    output_filename = "/tmp/out-found.txt"

'''
below we define a lot of search strings, used to find lemmata with certain
properties, corresponding to the scheme in document 'Interpunctie in gereduceerde
versie feb19'. The scheme used there is not consistent; we chose to change
that a little to make it so, because it is too confusing otherwise.

the cases are defined as follows. The numbers loop through
(vertM vertM), (vertM gebrW),  .....
the letters correspond as follows:
: <-> 1, 2, 3, 4, 5, ....
, <-> 1b, 2b. 3b, .....
; <-> 1c, 2c, ....
∅ <-> 1d, 2d, ...

e.g.
case 1: vertM : vertM
case 1b: vertM , vertM
case 6: gebrW : niv/bet
case 6d: gebrW ∅ niv/bet

some cases cannot exist, and are therefore omitted.

@arie is not sure whether the search strings are correct. They
might find some junk (and not find useful results), but it seems that the
results are pretty good. We use the strings to find relevant lemmata, so the
problems mentioned are not that relevant. Some explanations:

--- tests whether elt ends with :
[re:test(normalize-space(.), ':\s*$', '')]

--- test whether at least one of the following siblings has a remove_this
[following-sibling::*[@remove_this]]

--- tests whether there is not a following sibling cit which does not
have a remove_this prop
[not(following-sibling::cit[not(@remove_this)])]

--- tests whether there is a following sibling vertM which does not have a
remove_this prop
[following-sibling::vertM[not(@remove_this)]]
'''

# --- search string for case 1 (vertM : vertM)
# --- OK
string_1 = r'''
//vertM[not(@remove_this)]
[re:test(normalize-space(.), ':\s*$', '')]
[following-sibling::*[@remove_this]]
[not(following-sibling::cit[not(@remove_this)])]
[not(following-sibling::gebrW[not(@remove_this)])]
[following-sibling::vertM[not(@remove_this)]]
'''

# --- search string for case 1b (vertM , vertM)
# --- OK
string_1b = r'''
//vertM[not(@remove_this)]
[re:test(normalize-space(.), ',\s*$', '')]
[following-sibling::*[@remove_this]]
[not(following-sibling::cit[not(@remove_this)])]
[not(following-sibling::gebrW[not(@remove_this)])]
[following-sibling::vertM[not(@remove_this)]]
'''

# --- search string for case 1d (vertM ∅ vertM) (proposal)
# --- OK
string_1d = r'''
//vertM[not(@remove_this)]
[re:test(normalize-space(.), '[^;:,.]\s*$', '')]
[following-sibling::*[@remove_this]]
[not(following-sibling::cit[not(@remove_this)])]
[not(following-sibling::gebrW[not(@remove_this)])]
[following-sibling::vertM[not(@remove_this)]]
'''

# --- search string for case 2 (vertM : gebrW)
# if we include the following line, the lemmata from /2/
# are not found:
# [not(following-sibling::vertM[not(@remove_this)])]
# --- OK
string_2 = r'''
//vertM[not(@remove_this)]
[re:test(normalize-space(.), ':\s*$', '')]
[following-sibling::*[@remove_this]]
[not(following-sibling::cit[not(@remove_this)])]
[following-sibling::gebrW[not(@remove_this)]]
'''

# --- search string for case 2b (vertM, gebrW)
# --- OK
string_2b = r'''
//vertM[not(@remove_this)]
[re:test(normalize-space(.), ',\s*$', '')]
[following-sibling::*[@remove_this]]
[not(following-sibling::cit[not(@remove_this)])]
[not(following-sibling::vertM[not(@remove_this)])]
[following-sibling::gebrW[not(@remove_this)]]
'''

# --- search string for case 2d (vertM ∅ gebrW) (proposal)
# --- OK
string_2d = r'''
//vertM[not(@remove_this)]
[re:test(normalize-space(.), '[^;:,.]\s*$', '')]
[following-sibling::*[@remove_this]]
[not(following-sibling::cit[not(@remove_this)])]
[not(following-sibling::vertM[not(@remove_this)])]
[following-sibling::gebrW[not(@remove_this)]]
'''

# --- search string for case 3 (vertM : niv/bet)
# --- OK
string_3 = r'''
//vertM[not(@remove_this)]
[re:test(normalize-space(.), ':\s*$', '')]
[following-sibling::*[@remove_this]]
[not(following-sibling::*[not(@remove_this)])]
'''

# --- search string for case 3b (vertM , niv/bet)
# --- OK
string_3b = r'''
//vertM[not(@remove_this)]
[re:test(normalize-space(.), ',\s*$', '')]
[following-sibling::*[@remove_this]]
[not(following-sibling::*[not(@remove_this)])]
'''

# --- search string for case 3c (vertM ; niv/bet)
# --- OK
string_3c = r'''
//vertM[not(@remove_this)]
[re:test(normalize-space(.), ';\s*$', '')]
[following-sibling::*[@remove_this]]
[not(following-sibling::*[not(@remove_this)])]
'''

# --- search string for case 3d (vertM ∅ niv/bet)
# --- OK
string_3d = r'''
//vertM[not(@remove_this)]
[re:test(normalize-space(.), '[^;:,.]\s*$', '')]
[following-sibling::*[@remove_this]]
[not(following-sibling::*[not(@remove_this)])]
'''

# --- search string for case 4 (gebrW : gebrW)
# --- OK
string_4 = r'''
//gebrW[not(@remove_this)]
[re:test(normalize-space(.), ':\s*$', '')]
[following-sibling::*[@remove_this]]
[not(following-sibling::cit[not(@remove_this)])]
[not(following-sibling::vertM[not(@remove_this)])]
[following-sibling::gebrW[not(@remove_this)]]
'''

# --- search string for case 4b (gebrW , gebrW) (proposal)
# (probably not necessary: this should not happen, and is not found much)
# --- OK
string_4b = r'''
//gebrW[not(@remove_this)]
[re:test(normalize-space(.), ',\s*$', '')]
[following-sibling::*[1][@remove_this]]
[following-sibling::gebrW[not(@remove_this)]]
'''
# [not(following-sibling::cit[not(@remove_this)])]
# [not(following-sibling::vertM[not(@remove_this)])]

# --- search string for case 4c (gebrW ; gebrW)
# (probably not necessary: the ; should not change)
# --- OK
string_4c = r'''
//gebrW[not(@remove_this)]
[re:test(normalize-space(.), ';\s*$', '')]
[following-sibling::*[1][@remove_this]]
[not(following-sibling::cit[not(@remove_this)])]
[not(following-sibling::vertM[not(@remove_this)])]
[following-sibling::gebrW[not(@remove_this)]]
'''

# --- search string for case 4d (gebrW ∅ gebrW)
# (note: this is case 4b in the 'schoolreductie document')
# --- OK
string_4d = r'''
//gebrW[not(@remove_this)]
[re:test(normalize-space(.), '[^;:,.]\s*$', '')]
[following-sibling::*[@remove_this]]
[not(following-sibling::cit[not(@remove_this)])]
[not(following-sibling::vertM[not(@remove_this)])]
[following-sibling::gebrW[not(@remove_this)]]
'''

# --- search string for case 5 (gebrW : vertM)
# --- OK
string_5 = r'''
//gebrW[not(@remove_this)]
[re:test(normalize-space(.), ':\s*$', '')]
[following-sibling::*[@remove_this]]
[not(following-sibling::cit[not(@remove_this)])]
[not(following-sibling::gebrW[not(@remove_this)])]
[following-sibling::vertM[not(@remove_this)]]
'''

# --- search string for case 5b (gebrW , vertM) (case 5b is a proposal)
# --- LvB: this shouldn't happen, and he removed the hits, so this is not
# longer relevant.
# --- OK
string_5b = r'''
//gebrW[not(@remove_this)]
[re:test(normalize-space(.), ',\s*$', '')]
[following-sibling::*[1][@remove_this]]
[not(following-sibling::cit[not(@remove_this)])]
[not(following-sibling::gebrW[not(@remove_this)])]
[following-sibling::vertM[not(@remove_this)]]
'''

# --- search string for case 6 (gebrW : niv/bet)
# --- OK
string_6 = r'''
//gebrW[not(@remove_this)]
[re:test(normalize-space(.), ':\s*$', '')]
[following-sibling::*[@remove_this]]
[not(following-sibling::*[not(@remove_this)])]
'''

# --- search string for case 6b (gebrW , niv/bet)
# --- LvB: this shouldn't happen, and he removed the hits, so this is not
# longer relevant.
# --- OK
string_6b = r'''
//gebrW[not(@remove_this)]
[re:test(normalize-space(.), ',\s*$', '')]
[following-sibling::*[@remove_this]]
[not(following-sibling::*[not(@remove_this)])]
'''

# --- search string for case 6c (gebrW ; niv/bet) (proposal)
# --- OK
string_6c = r'''
//gebrW[not(@remove_this)]
[re:test(normalize-space(.), ';\s*$', '')]
[following-sibling::*[@remove_this]]
[not(following-sibling::*[not(@remove_this)])]
'''

# --- search string for case 6d (gebrW ∅ niv/bet) (proposal)
# --- OK
string_6d = r'''
//gebrW[not(@remove_this)]
[re:test(normalize-space(.), '[^;:,.]\s*$', '')]
[following-sibling::*[@remove_this]]
[not(following-sibling::*[not(@remove_this)])]
'''

# --- search string for case 7 (cit : niv/bet)
# (proposal, but this string doesn't find anything, so is probably not necessary)
# --- OK
string_7 = r'''
//cit[not(@remove_this)]
[re:test(normalize-space(.), ':\s*$', '')]
[following-sibling::*[@remove_this]]
[not(following-sibling::*[not(@remove_this)])]
'''

# --- search string for case 7 (cit , niv/bet)
# (proposal, but this string doesn't find anything, so is probably not necessary)
# --- OK
string_7b = r'''
//cit[not(@remove_this)]
[re:test(normalize-space(.), ',\s*$', '')]
[following-sibling::*[@remove_this]]
[not(following-sibling::*[not(@remove_this)])]
'''

# --- search string for case 7c (cit ; niv/bet)
# note, this is case 7 in the doc
# --- OK
string_7c = r'''
//cit[not(@remove_this)]
[re:test(normalize-space(.), ';\s*$', '')]
[following-sibling::*[@remove_this]]
[not(following-sibling::*[not(@remove_this)])]
'''

# --- search string for case 7d (cit ∅ niv/bet) (proposal)
# (probably not needed)
string_7d = r'''
//cit[not(@remove_this)]
[re:test(normalize-space(.), '[^;:,.]\s*$', '')]
'''
# [following-sibling::*[@remove_this]]
# [not(following-sibling::*[not(@remove_this)])]

# --- search string for case 8d (cit ∅ gebrW)
# (not found)
string_8d = r'''
//cit[not(@remove_this)]
[re:test(normalize-space(.), '[^;:,.]\s*$', '')]
[following-sibling::*[@remove_this]]
[not(following-sibling::cit[not(@remove_this)])]
[not(following-sibling::vertM[not(@remove_this)])]
[following-sibling::gebrW[not(@remove_this)]]
'''

# --- search string for case 9d (cit ∅ vertM)
# (not found)
string_9d = r'''
//cit[not(@remove_this)]
[re:test(normalize-space(.), '[^;:,.]\s*$', '')]
[following-sibling::*[@remove_this]]
[not(following-sibling::cit[not(@remove_this)])]
[not(following-sibling::gebrW[not(@remove_this)])]
[following-sibling::vertM[not(@remove_this)]]
'''

# --- search string for case 10d (cit ∅ cit)
# (not found)
string_10d = r'''
//cit[not(@remove_this)]
[re:test(normalize-space(.), '[^;:,.]\s*$', '')]
[following-sibling::*[@remove_this]]
[not(following-sibling::vertM[not(@remove_this)])]
[not(following-sibling::gebrW[not(@remove_this)])]
[following-sibling::cit[not(@remove_this)]]
'''

# xpath expression used for searching
if  action == "search":
    search_r_string = string_4b


# --- not used

# def tostring(node):
    # return etree.tostring(node, encoding='unicode')

# xml_str = tostring(xml)

# ---

# --- example search_r_strings

        # //vertM[re:test(., ', $', '')]/following-sibling::*[1][self::gebrW[@remove_this]]
        # //cit[re:test(., '[a-zA-Z]\s*$', '')]/following-sibling::*[1][self::cit[@remove_this]]
        # //vertM[re:test(., '[a-zA-Z]:\s*$', '')]/following-sibling::*[1][self::cit[@remove_this]][following-sibling::*[self::vertM]]
        # //cit[re:test(., '[a-zA-Z]\s*$', '')]/following-sibling::*[1][self::cit[@remove_this]][not(following-sibling::*[@remove_this])]
        # //gebrW[not(@remove_this)][re:test(., '[a-zA-Z]:\s*$', '')]/following-sibling::*[1][self::cit[@remove_this]][following-sibling::*[self::vertM]]
        # //cit[re:test(., '[a-zA-Z]\s*$', '')]
        # //cit[re:test(., '[a-zA-Z]\s+$', '')]
        # //cit[re:test(., '[a-zA-Z]\.\s*$', '')]
        # //vertM[re:test(., '[a-zA-Z]:\s*$', '')]/following-sibling::*[1][self::cit[@remove_this]][following-sibling::*[1][self::vertM]]
        # //vertM[re:test(., '[a-zA-Z],\s*$', '')]/following-sibling::*[1][self::cit[@remove_this]][following-sibling::*[1][self::vertM]]
        # //vertM[re:test(., '[a-zA-Z]:\s*$', '')]/following-sibling::*[1][self::cit[@remove_this]][following-sibling::*[1][self::gebrW]]
        # //vertM[re:test(., '[a-zA-Z],\s*$', '')]/following-sibling::*[1][self::cit[@remove_this]][following-sibling::*[1][self::gebrW[not(@remove_this)]]]
        # //vertM[re:test(., '[a-zA-Z]:\s*$', '')]/following-sibling::*[1][self::cit[@remove_this]][not(following-sibling::*[@remove_this])]
        # //vertM[re:test(., '[a-zA-Z]:\s*$', '')]/following-sibling::*[1][self::cit[@remove_this]][not(following-sibling::*[@remove_this])]
        # //vertM[re:test(., '[a-zA-Z],\s*$', '')]/following-sibling::*[@remove_this][following-sibling::*[1][self::vertM]]
        # //vertM[re:test(., '[a-zA-Z],\s*$', '')]/following-sibling::*[1][@remove_this]/following-sibling::*[1][@remove_this][following-sibling::*[1][self::vertM]]
        # //bet[count(niv123) > 1]
        # //niv123[re:test(., 'med.-pass.\s*$')]
        # //niv123[re:test(string(.), '[a-zA-Z]\.[ ]+\s*$', '')]
        # [/following-sibling::*[2]][@remove_this]

# ---


def get_filenames(files):
    if files == "all-lemmata":
        (fns_bad_parse, fns_bad_status, fns_status_ok), errors, (num_ignored, num_files, num_unreadable) = get_words_files(warn_on_bad_status=False)
        if errors:
            die("Error(s) with get_words_files: %s" % errors)
        if fns_bad_parse:
            die("some bad parse")
        num_bad = num_unreadable - num_ignored
        if num_bad:
            die("some bad %s" % num_bad)
        all_fns = [*fns_bad_status, *fns_status_ok]
        num = len(all_fns)
    elif files == "t2-lemmata":
        t2_test_path = config.local['t2_test_path']
        # --- ignored = hulpdocumenten etc.
        test_fns_ret = cmd(
            ['find', '-L', t2_test_path, '-name', '*.xml', '-print0'],
            capture_stdout=True,
        )
        test_fns = test_fns_ret.out.split('\0')[0:-1]
        # fns = all_fns
        # fns = ['/home/arie/grne/grne/test/t2-unit-tests/4-gewenst/προκαλύπτω.xml']
        all_fns = test_fns
        num = len(all_fns)
    else:
        die("config: files not properly set")
    return num, all_fns


def do(filename, output_filename, action):
    if action == "search":
        xml = get_xml_from_file(filename)
        regexpNS = "http://exslt.org/regular-expressions"
        find = etree.XPath(search_r_string,
            namespaces={'re': regexpNS})
        found = find(xml)
        if found:
            with open(output_filename, 'a') as fh:
                fh.write("%s\n" % filename)
    elif action == "transform":
        with open(filename, "r") as fh_i:
            xml_input_string = fh_i.read()
            xml_transformed_tree = xslt_transform_saxon_java(
                xml_input_string,
                config.main['schoolreductie_xslt_t2_path'],
                config.user['saxon_jar_path'],
            )
            xml_transformed_string = etree.tostring(xml_transformed_tree, encoding='unicode', pretty_print=True)
            with open(output_filename, 'a') as fh:
                fh.write(xml_transformed_string)
    else:
        die("config: action not properly set")


def go():
    config = config_get()

    # clear output file
    open(output_filename, 'w').close()

    num, fns = get_filenames(files)

    print('Action to perform: %s' % action)
    print('output_filename: %s' % output_filename)
    print('files to use: %s' % files)
    print('Num of files: %s' % num)

    for filename in fns:
        do(filename, output_filename,  action)

go()
