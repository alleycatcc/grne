#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import proj_paths # noqa

from functools import partial
import os
from sys import exit, stderr, argv
from os.path import dirname, basename

from lxml import etree
import pytest # noqa
import xmldiff.main

from acatpy.io.io import cmd, die
from acatpy.util import pathjoin
from acatpy.xml import xslt_transform_saxon_java

from grne.util.util_tests import toets
from config import get as config_get

require_pytest3 = False

config = config_get()

if require_pytest3 and basename(argv[0]) != 'pytest-3':
    stderr.write('Must be run using pytest-3.\n')
    exit(1)

# --- Note: failed tests show 'expected: []', which is not intuitive, but it
# is because it expects an empty diff, so expected is [].
# To see what it actually expected, and what the result of the transformation is,
# set this to `True` to print it.
print_xml_strings = True

def t2(input_path, expected_path):
    with open(input_path, "r") as fh_i, open(expected_path, "r") as fh_e:
        os.chdir(dirname(input_path))
        xml_input_string = fh_i.read()
        xml_transformed_tree = xslt_transform_saxon_java(
            xml_input_string,
            config.main['schoolreductie_xslt_t2_path'],
            config.user['saxon_jar_path'],
        )
        xml_transformed_string = etree.tostring(xml_transformed_tree, encoding='unicode', pretty_print=True)
        xml_expected_string = fh_e.read()

        diff = xmldiff.main.diff_texts(xml_expected_string, xml_transformed_string)

        # only print the expected and transformed strings if there is a diff
        if not diff == []:
            if print_xml_strings:
                print('xml_expected_string %s' % xml_expected_string)
                print('xml_transformed_string %s' % xml_transformed_string)
        for d in diff:
            print(d)
        return diff

def prnt(x):
    with open('/tmp/t2-debug', 'a') as fh:
        fh.write(x + '\n')

def run_tests(test, input_path_stub, expected_path_stub):
    path_prefix = config.local['t2_test_path']
    input_dir, expected_dir = [pathjoin(path_prefix, y) for y in (input_path_stub, expected_path_stub)]

    def crawl(dir):
        ret = cmd(['find', '-L', dir, '-name', '*.xml', '-print0'], capture_stdout=True, die=True)
        fns = ret.out.split('\0')[0:-1]
        num_fns = len(fns)
        fns_dict = {basename(x): x for x in fns}
        return num_fns, fns_dict
    num_input, input_dict = crawl(input_dir)
    num_expected, expected_dict = crawl(expected_dir)
    if num_input != num_expected:
        die('Mismatch: found\n %s input files (%s)\n %s expected files (%s)' % (num_input, input_dir, num_expected, expected_dir))
    for base, _ in sorted(input_dict.items()):
        input_path = input_dict[base]
        expected_path = expected_dict.get(base, None)
        if expected_path is None:
            die('Input file and expected file must have the same basename (giving up on %s)' % base)
        test([input_path, expected_path], [[]])

def test_1():
    group = partial(toets, t2)
    test = partial(group, "rule 1")
    run_tests(test, '1', '1-gewenst')

def test_1b():
    group = partial(toets, t2)
    test = partial(group, "rule 1b")
    run_tests(test, '1b', '1b-gewenst')

def test_1d():
    group = partial(toets, t2)
    test = partial(group, "rule 1d")
    run_tests(test, '1d', '1d-gewenst')

def test_2():
    group = partial(toets, t2)
    test = partial(group, "rule 2")
    run_tests(test, '2', '2-gewenst')

def test_2b():
    group = partial(toets, t2)
    test = partial(group, "rule 2b")
    run_tests(test, '2b', '2b-gewenst')

def test_2d():
    group = partial(toets, t2)
    test = partial(group, "rule 2d")
    run_tests(test, '2d', '2d-gewenst')

def test_3():
    group = partial(toets, t2)
    test = partial(group, "rule 3")
    run_tests(test, '3', '3-gewenst')

def test_3b():
    group = partial(toets, t2)
    test = partial(group, "rule 3b")
    run_tests(test, '3b', '3b-gewenst')

def test_3c():
    group = partial(toets, t2)
    test = partial(group, "rule 3c")
    run_tests(test, '3c', '3c-gewenst')

def test_3d():
    group = partial(toets, t2)
    test = partial(group, "rule 3d")
    run_tests(test, '3d', '3d-gewenst')

def test_4():
    group = partial(toets, t2)
    test = partial(group, "rule 4")
    run_tests(test, '4', '4-gewenst')

def test_4d():
    group = partial(toets, t2)
    test = partial(group, "rule 4d")
    run_tests(test, '4d', '4d-gewenst')

def test_5():
    group = partial(toets, t2)
    test = partial(group, "rule 5")
    run_tests(test, '5', '5-gewenst')

def test_6():
    group = partial(toets, t2)
    test = partial(group, "rule 6")
    run_tests(test, '6', '6-gewenst')

def test_6c():
    group = partial(toets, t2)
    test = partial(group, "rule 6c")
    run_tests(test, '6c', '6c-gewenst')

def test_6d():
    group = partial(toets, t2)
    test = partial(group, "rule 6d")
    run_tests(test, '6d', '6d-gewenst')


def test_7c():
    group = partial(toets, t2)
    test = partial(group, "rule 7c")
    run_tests(test, '7c', '7c-gewenst')

# --- add tests to run here
test_1()
test_1b()
test_1d()
test_2()
test_2b()
test_2d()
test_3()
test_3b()
test_3c()
test_3d()
test_4()
test_4d()
test_5()
test_6()
test_6c()
test_6d()
test_7c()

