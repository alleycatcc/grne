import re

from pydash import flat_map

from acatpy.io.io import die
from acatpy.io.speak import warn

from grne.chars import (
    regex_dash, regex_punc, regex_greek,
    normalise_hyphens, normalise_macron_breve
)

def is_odd(x):
    return x % 2 != 0

# --- expand e.g. (ἐ)πέλασ(σ)α to the list:
#   ἐπέλασσα
#   πέλασσα
#   ἐπέλασα
#   πέλασα

def expand_hoofdW_parens(hoofdW):
    tokens = re.split(r'( \(.\) )', hoofdW, flags=re.X)
    if len(tokens) <= 1:
        return [hoofdW]
    j = -1
    stems = ['']
    # --- e.g. hoofdW = (ἐ)πέλασ(σ)α
    for token in tokens:
        j += 1
        # --- token is parenthesized part, e.g. (ἐ) or (σ)
        if is_odd(j):
            char = re.sub(r'[()]', '', token)
            stems = flat_map(stems, lambda s: [s, s + char])
        # --- token is the part in between
        else:
            stems = [x + token for x in stems]
    return stems

def get_first_complete_hoofdW(hoofdW, warn_on_fail=True):
    def check(token, regex):
        return re.search(regex, token, flags=re.X)

    for token in re.split(r',? \s+', hoofdW, flags=re.X):
        # --- element begins with a space (shouldn't happen, but will cause
        # a crash below if it does)
        if token == '':
            continue
        token, _cnt = normalise_hyphens(token, keep_at_beginning=True,
                                        keep_at_end=True)
        token, _cnt_m, _cnt_b = normalise_macron_breve(token)
        if check(token, r'^ \s* $'):
            continue
        if not check(token, regex_greek()):
            continue
        token = re.sub(r'[()]', '', token)
        return token
    if warn_on_fail:
        warn("get_first_complete_hoofdW failed for input: " + hoofdW)
    return None

'''
This is a python version of an old DPC function (`getIndexI` in
Process::Meta.pm) which is (still) used by the migrator, but nowhere else.

It uses some heuristics to try and determine the first complete usable
token from <hoofdW/>.

It is no longer used but we'll keep it for reference.
'''

def get_first_complete_hoofdW_dpc(hoofdW, allow_dash_at_end=False, warn_on_fail=True):
    def check(token, regex):
        return re.match(regex, token, flags=re.X)

    for token in re.split(r'\s+', hoofdW):
        # --- disallow e.g. -το and (-ρᾰ-)
        if check(token, r'^ \(? %s' % regex_punc()):
            continue
        if not allow_dash_at_end and check(token, r'.+ %s $' % regex_dash()):
            continue
        token, _cnt = normalise_hyphens(token)
        token, _cnt_m, _cnt_b = normalise_macron_breve(token)
        if check(token, r'^ \s* $'):
            continue
        if not check(token, regex_greek()):
            continue
        token = re.sub(r'^ .* (%s | \( | \)) .*' % regex_greek(), r'\1', token)
        m = check(token, r'((%s | \( | \) )+)' % regex_greek())
        if m is None:
            continue
        token = m.groups()[0]
        expand_parens = expand_hoofdW_parens(token)
        if not expand_parens:
            die("Internal error (expand_parens), input was: " + token)
        return expand_parens[0]
    if warn_on_fail:
        warn("get_first_complete_hoofdW failed for input: " + hoofdW)
    return None
