#!/usr/bin/env bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")

. "$bindir"/functions.bash

rootdir="$bindir"/..
serverdir="$rootdir"/server

USAGE="Usage: $0 { --mode=dev/deploy } [--stats=stats-file]"

mode=
stats_file=
while getopts h-: arg; do
    case $arg in
        h) warn "$USAGE"; exit 0 ;;
        -) OPTARG_VALUE="${OPTARG#*=}"
            case $OPTARG in
                help)  warn "$USAGE"; exit 0 ;;
                mode=dev) mode=dev ;;
                mode=deploy) mode=deploy ;;
                stats=*) stats_file="$OPTARG_VALUE" ;;
                '')    break ;;
                *)     error "Illegal option --$OPTARG" ;;
                esac ;;
        *) error "$USAGE" ;;
    esac
done
shift $((OPTIND-1))

go () {
    local conf
    local args=("$@")
    if [ "$mode" = dev ]; then
        conf=wsgi.dev.ini
    elif [ "$mode" = deploy ]; then
        conf=wsgi.deploy.ini
    else
        error "$(printf "Missing mode.\n\n$USAGE")"
    fi

    # --- @todo consider using --thunder-lock

    # --- checking environment vars occurs inside the flask application.
    mci
    mcb cwd "$serverdir"
    mcb cmd uwsgi
    mcb   --ini "$conf"
    if [ -n "$stats_file" ]; then
        mcb --stats "$stats_file"
    fi
    mcb   "${args[@]}"
    mcg

}; fun go "$@"
