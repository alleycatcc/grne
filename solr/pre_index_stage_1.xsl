<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:my="{ns}"
  extension-element-prefixes="my"
  version="2.0"
>

  <xsl:template match="doc[@type='main']">
    <!-- remove @type -->
    <xsl:copy>
      <xsl:apply-templates select="node()"/>
      <xsl:call-template name="verw-string-main"/>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="doc[@type='cit']">
    <!-- remove @type -->
    <xsl:copy>
      <xsl:apply-templates select="node()"/>
      <xsl:call-template name="verw-string-cit"/>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="doc/field[@name='type']">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:value-of select="ancestor::doc/@type"/>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="doc[@type='cit']/field[@name='_root_']">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <my:get-cit-root>
        <xsl:value-of select="//doc[@type='main']/field[@name='id-naam']/text()"/>
      </my:get-cit-root>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="doc/field[@name='id']">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <my:make-uuid>
        <xsl:value-of select="ancestor::doc/@type"/>
        |
        <xsl:value-of select="//doc[@type='main']/field[@name='id-naam']/text()"/>
      </my:make-uuid>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="doc/field[@name='hoofdW-clean']">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <my:make-hoofdW-clean/>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="doc/field[@name='hoofdW-sort']">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <my:make-hoofdW-sort/>
    </xsl:copy>
  </xsl:template>
  <xsl:template name="verw-string-main">
    <xsl:apply-templates mode="verw-string-main-each" select="//doc[@type='cit']"/>
  </xsl:template>
  <xsl:template mode="verw-string-main-each" match="*">
    <xsl:call-template name="verw-string-common"/>
  </xsl:template>
  <xsl:template name="verw-string-cit">
    <xsl:call-template name="verw-string-common"/>
  </xsl:template>
  <xsl:template name="verw-string-common">
    <field name='verw-string'>
      <!--xsl:copy-of select="@*"/-->
      <xsl:variable name="verw-string-aut" select="./field[@name='aut']"/>
      <xsl:variable name="verw-string-werk" select="./field[@name='werk']"/>
      <xsl:variable name="verw-string-plaats" select="./field[@name='plaats']"/>
      <xsl:variable name="verw-string-aut-trim" select="normalize-space($verw-string-aut)"/>
      <xsl:variable name="verw-string-werk-trim" select="normalize-space($verw-string-werk)"/>
      <xsl:variable name="verw-string-plaats-trim" select="normalize-space($verw-string-plaats)"/>
      <xsl:variable name="verw-string-concat" select="concat(
             $verw-string-aut-trim,
        ' ', $verw-string-werk-trim,
        ' ', $verw-string-plaats-trim
      )"/>
      <!-- normalise again because some elements might have been empty -->
      <xsl:value-of select="normalize-space($verw-string-concat)"/>
    </field>
  </xsl:template>
  <xsl:template match="@*|node()">
    <xsl:choose>
      <xsl:when test="contains(@name, '-loose')">
        <xsl:copy>
          <xsl:copy-of select="@*"/>
          <my:make-loose/>
        </xsl:copy>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:apply-templates select="node() | @*"/>
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>

