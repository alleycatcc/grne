<!--
  • We generate a 'loose' version <x-loose/> for fields with Greek texts named <x>.

  • Decisions about how to tokenize, what to index and/or store, and how to
    make/populate the fulltext index ('copy fields') are not made here.

  • We can leave punctuation and whitespace messy here and let solr take
    care of it via field/tokeniser rules.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

  <!-- remove processing instructions -->
  <xsl:template match="processing-instruction('xml-stylesheet')" />

  <!-- there are no non-Greek fields of 'main' documents.
       the remaining candidates, vertM & toel, have also been removed:
       toel can contain link, and vertM can contain toel, so both can
       therefore contain Greek.

       same for 'cit': everything can contain toel.

  <xsl:template mode="disable" match="N/A">
    <xsl:call-template name="make-field">
      <xsl:with-param name="name" select="local-name()" />
      <xsl:with-param name="value" select="." />
    </xsl:call-template>
  </xsl:template>

  <xsl:template mode="disable-cit" match="N/A">
    <xsl:call-template name="make-field">
      <xsl:with-param name="name" select="local-name()" />
      <xsl:with-param name="value" select="." />
    </xsl:call-template>
  </xsl:template>

  -->

  <!-- Greek fields of 'main' documents
       x -> x
       x -> x-loose

       hoofdW is done separately below, in order to generate -clean and
       -sort in addition to -loose.
       -->
  <xsl:template match="
       //werkV
    |  //gebrW
    |  //morfI
    |  //vertM
    |  //toel
  ">
    <xsl:call-template name="make-field">
      <xsl:with-param name="name" select="local-name()" />
      <xsl:with-param name="value" select="." />
    </xsl:call-template>
    <xsl:call-template name="make-field-loose">
      <xsl:with-param name="name" select="local-name()" />
      <xsl:with-param name="value" select="." />
    </xsl:call-template>
  </xsl:template>

  <!-- Greek fields of 'cit' documents
       x -> x
       x -> x-loose -->
  <xsl:template match="
       //citG
    |  //citNV
    |  //plaats
    |  //aut
    |  //werk
  ">
    <xsl:call-template name="make-field">
      <xsl:with-param name="name" select="local-name()" />
      <xsl:with-param name="value" select="." />
    </xsl:call-template>
    <xsl:call-template name="make-field-loose">
      <xsl:with-param name="name" select="local-name()" />
      <xsl:with-param name="value" select="." />
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="//etym">
    <xsl:call-template name="make-field">
      <xsl:with-param name="name" select="'etym'" />
      <xsl:with-param name="value" select="." />
    </xsl:call-template>
    <xsl:call-template name="make-field-loose">
      <xsl:with-param name="name" select="'etym'" />
      <xsl:with-param name="value" select="." />
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="//hoofdW">
    <xsl:call-template name="make-field">
      <xsl:with-param name="name" select="'hoofdW-clean'" />
      <xsl:with-param name="value" select="." />
    </xsl:call-template>
    <xsl:call-template name="make-field">
      <xsl:with-param name="name" select="'hoofdW-sort'" />
      <xsl:with-param name="value" select="." />
    </xsl:call-template>
    <xsl:call-template name="make-field">
      <xsl:with-param name="name" select="'hoofdW'" />
      <xsl:with-param name="value" select="." />
    </xsl:call-template>
    <xsl:call-template name="make-field-loose">
      <xsl:with-param name="name" select="'hoofdW'" />
      <xsl:with-param name="value" select="." />
    </xsl:call-template>
  </xsl:template>

  <!-- indexI/item -> index-item
       indexI/item -> index-item-loose -->

  <xsl:template match="//metadata//indexI/item">
    <xsl:call-template name="make-field">
      <xsl:with-param name="name" select="'index-item'" />
      <xsl:with-param name="value" select="." />
    </xsl:call-template>
    <xsl:call-template name="make-field-loose">
      <xsl:with-param name="name" select="'index-item'" />
      <xsl:with-param name="value" select="." />
    </xsl:call-template>
  </xsl:template>

  <!-- naam -> id-naam -->
  <xsl:template match="//metadata/naam">
    <xsl:call-template name="make-field">
      <xsl:with-param name="name" select="'id-naam'" />
      <xsl:with-param name="value" select="." />
    </xsl:call-template>
  </xsl:template>

  <!-- [not currently used]: naam/@id -> id-naam-id -->
  <xsl:template mode="disable" match="//metadata/naam/@id">
    <xsl:call-template name="make-field">
      <xsl:with-param name="name" select="'id-naam-id'" />
      <xsl:with-param name="value" select="." />
    </xsl:call-template>
  </xsl:template>

  <!-- woordS/@ -> woordS -->
  <xsl:template match="//metadata/woordS/@*">
    <xsl:call-template name="make-field">
      <xsl:with-param name="name" select="'woordS'" />
      <xsl:with-param name="value" select="." />
    </xsl:call-template>
  </xsl:template>

  <!-- named templates -->
  <!-- note: <xsl:value-of select> converts the results to a string,
       equivalent to calling string(), so <r>, <link> etc. are pulled away,
       which is what we want.
  -->

  <xsl:template name="make-field">
    <xsl:param name="name" />
    <xsl:param name="value" />
    <field>
      <xsl:attribute name="name">
        <xsl:value-of select="$name" />
      </xsl:attribute>
      <xsl:value-of select="$value" />
    </field>
    <xsl:apply-templates select="@*"/>
  </xsl:template>

  <xsl:template name="make-field-loose">
    <xsl:param name="name" />
    <xsl:param name="value" />
    <field>
      <xsl:attribute name="name">
        <xsl:value-of select="concat($name,'-loose')" />
      </xsl:attribute>
      <xsl:value-of select="$value" />
    </field>
  </xsl:template>

  <!-- root -->
  <xsl:template match="/contLem">
    <root>
      <doc type="main">
        <field name="id"/>
        <field name="type"/>
        <xsl:apply-templates select="node() | @*"/>
      </doc>
    </root>
  </xsl:template>

  <!-- identity -->
  <xsl:template match="@*|node()">
    <xsl:apply-templates select="node() | @*"/>
  </xsl:template>
</xsl:stylesheet>
