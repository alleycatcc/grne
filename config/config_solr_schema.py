# -*- coding: utf-8 -*-

def get(**kw):
    '''
    For fields which should be copied to fulltext, but do not
    themselves need to be indexed, set everything but copy-to
    to False.

    In particular, the term vectors will still be stored.

    Use text_gen_sort instead of text_general to make a generic
    non-multivalued field sortable.

    @todo why are aut/werk/plaats multivalued?
    @todo do we still need termVectors etc.?
    '''

    return {
        'schema': [
            {
                'solr-conf': {
                    'name': '_root_',
                    'type': 'string',
                    'indexed': True,
                    'multiValued': False,
                    'stored': False,
                    'termVectors': False,
                    'termPositions': False,
                    'termOffsets': False,
                },
                'copy-to': set(),
            },
            {
                'solr-conf': {
                    'name': '_text_',
                    'type': 'text_general',
                    'indexed': True,
                    'multiValued': True,
                    'stored': True,
                    'termVectors': True,
                    'termPositions': True,
                    'termOffsets': True,
                },
                'copy-to': set(),
            },
            {
                'solr-conf': {
                    'name': 'hoofdW-combined',
                    'type': 'text_general',
                    'indexed': True,
                    'multiValued': True,
                    'stored': True,
                    'termVectors': False,
                    'termPositions': False,
                    'termOffsets': False,
                },
                'copy-to': set(),
            },
            {
                'solr-conf': {
                    'name': 'vertM-combined',
                    'type': 'text_general',
                    'indexed': True,
                    'multiValued': True,
                    'stored': True,
                    'termVectors': False,
                    'termPositions': False,
                    'termOffsets': False,
                },
                'copy-to': set(),
            },
            {
                'solr-conf': {
                    'name': 'id-naam',
                    'type': 'text_gr',
                    'indexed': True,
                    'multiValued': False,
                    'stored': True,
                    'termVectors': True,
                    'termPositions': True,
                    'termOffsets': True,
                },
                'copy-to': set(),
            },
            {
                'solr-conf': {
                    'name': 'type',
                    'type': 'text_general',
                    'indexed': True,
                    'multiValued': False,
                    'stored': False,
                    'termVectors': False,
                    'termPositions': False,
                    'termOffsets': False,
                },
                'copy-to': set(),
            },
            {
                'solr-conf': {
                    'name': 'vertM',
                    # --- text_nl doesn't allow wildcard queries.
                    'type': 'text_general',
                    'indexed': True,
                    'multiValued': True,
                    'stored': True,
                    'termVectors': True,
                    'termPositions': True,
                    'termOffsets': True,
                },
                'copy-to': {'_text_', 'vertM-combined'},
            },
            {
                'solr-conf': {
                    'name': 'vertM-loose',
                    # --- text_nl doesn't allow wildcard queries.
                    'type': 'text_general',
                    'indexed': True,
                    'multiValued': True,
                    'stored': True,
                    'termVectors': True,
                    'termPositions': True,
                    'termOffsets': True,
                },
                'copy-to': {'_text_', 'vertM-combined'},
            },
            {
                'solr-conf': {
                    'name': 'gebrW',
                    'type': 'text_general',
                    'indexed': False,
                    'multiValued': True,
                    'stored': False,
                    'termVectors': False,
                    'termPositions': False,
                    'termOffsets': False,
                },
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'gebrW-loose',
                    'type': 'text_general',
                    'indexed': False,
                    'multiValued': True,
                    'stored': False,
                    'termVectors': False,
                    'termPositions': False,
                    'termOffsets': False,
                },
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'aut',
                    'type': 'text_gen_sort',
                    'indexed': True,
                    'multiValued': True,
                    # @prod -> False
                    'stored': True,
                    'termVectors': True,
                    'termPositions': True,
                    'termOffsets': True,
                },
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'aut-loose',
                    'type': 'text_gen_sort',
                    'indexed': True,
                    'multiValued': True,
                    # @prod -> False
                    'stored': True,
                    'termVectors': True,
                    'termPositions': True,
                    'termOffsets': True,
                },
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'werk',
                    'type': 'text_gen_sort',
                    'indexed': True,
                    'multiValued': True,
                    # @prod -> False
                    'stored': True,
                    'termVectors': True,
                    'termPositions': True,
                    'termOffsets': True,
                },
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'werk-loose',
                    'type': 'text_gen_sort',
                    'indexed': True,
                    'multiValued': True,
                    # @prod -> False
                    'stored': True,
                    'termVectors': True,
                    'termPositions': True,
                    'termOffsets': True,
                },
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'plaats',
                    'type': 'text_general',
                    'indexed': True,
                    'multiValued': True,
                    # @prod -> False
                    'stored': True,
                    'termVectors': True,
                    'termPositions': True,
                    'termOffsets': True,
                },
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'plaats-loose',
                    'type': 'text_general',
                    'indexed': True,
                    'multiValued': True,
                    # @prod -> False
                    'stored': True,
                    'termVectors': True,
                    'termPositions': True,
                    'termOffsets': True,
                },
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'verw-string',
                    'type': 'text_general',
                    'indexed': False,
                    'multiValued': True,
                    'stored': False,
                    'termVectors': False,
                    'termPositions': False,
                    'termOffsets': False,
                },
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'citNV',
                    'type': 'text_general',
                    'indexed': False,
                    'multiValued': True,
                    'stored': False,
                    'termVectors': False,
                    'termPositions': False,
                    'termOffsets': False,
                },
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'citNV-loose',
                    'type': 'text_general',
                    'indexed': False,
                    'multiValued': True,
                    'stored': False,
                    'termVectors': False,
                    'termPositions': False,
                    'termOffsets': False,
                },
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'citG',
                    'type': 'text_general',
                    'indexed': False,
                    'multiValued': True,
                    'stored': False,
                    'termVectors': False,
                    'termPositions': False,
                    'termOffsets': False,
                },
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'citG-loose',
                    'type': 'text_general',
                    'indexed': False,
                    'multiValued': True,
                    'stored': False,
                    'termVectors': False,
                    'termPositions': False,
                    'termOffsets': False,
                },
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'toel',
                    'type': 'text_general',
                    'indexed': False,
                    'multiValued': True,
                    'stored': False,
                    'termVectors': False,
                    'termPositions': False,
                    'termOffsets': False,
                },
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'toel-loose',
                    'type': 'text_general',
                    'indexed': False,
                    'multiValued': True,
                    'stored': False,
                    'termVectors': False,
                    'termPositions': False,
                    'termOffsets': False,
                },
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'hoofdW',
                    'type': 'text_general',
                    # --- indexed must be true, because we want it stored.
                    'indexed': True,
                    'multiValued': False,
                    'stored': True,
                    'termVectors': True,
                    'termPositions': True,
                    'termOffsets': True,
                },
                'copy-to': {'_text_', 'hoofdW-combined'},
            },
            {
                'solr-conf': {
                    'name': 'hoofdW-loose',
                    'type': 'text_general',
                    'indexed': True,
                    'multiValued': False,
                    'stored': True,
                    'termVectors': True,
                    'termPositions': True,
                    'termOffsets': True,
                },
                'copy-to': {'_text_', 'hoofdW-combined'},
            },
            {
                'solr-conf': {
                    'name': 'hoofdW-clean',
                    'type': 'text_general',
                    # --- indexed must be true, because we want it stored.
                    'indexed': True,
                    'multiValued': False,
                    'stored': True,
                    'termVectors': True,
                    'termPositions': True,
                    'termOffsets': True,
                },
                # --- ok, not truly necessary though.
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'hoofdW-sort',
                    'type': 'text_gen_sort',
                    # --- indexed must be true, because we want it stored.
                    'indexed': True,
                    'multiValued': False,
                    'stored': True,
                    'termVectors': True,
                    'termPositions': True,
                    'termOffsets': True,
                },
                'copy-to': set(),
            },
            {
                'solr-conf': {
                    'name': 'werkV',
                    'type': 'text_general',
                    'indexed': False,
                    'multiValued': True,
                    'stored': False,
                    'termVectors': False,
                    'termPositions': False,
                    'termOffsets': False,
                },
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'werkV-loose',
                    'type': 'text_general',
                    'indexed': False,
                    'multiValued': True,
                    'stored': False,
                    'termVectors': False,
                    'termPositions': False,
                    'termOffsets': False,
                },
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'morfI',
                    'type': 'text_general',
                    'indexed': False,
                    'multiValued': False,
                    'stored': False,
                    'termVectors': False,
                    'termPositions': False,
                    'termOffsets': False,
                },
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'morfI-loose',
                    'type': 'text_general',
                    'indexed': False,
                    'multiValued': False,
                    'stored': False,
                    'termVectors': False,
                    'termPositions': False,
                    'termOffsets': False,
                },
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'index-item',
                    'type': 'text_general',
                    'indexed': True,
                    'multiValued': True,
                    'stored': False,
                    'termVectors': False,
                    'termPositions': False,
                    'termOffsets': False,
                },
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'index-item-loose',
                    'type': 'text_general',
                    'indexed': True,
                    'multiValued': True,
                    'stored': False,
                    'termVectors': False,
                    'termPositions': False,
                    'termOffsets': False,
                },
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'etym',
                    'type': 'text_general',
                    'indexed': False,
                    'multiValued': False,
                    'stored': False,
                    'termVectors': False,
                    'termPositions': False,
                    'termOffsets': False,
                },
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'etym-loose',
                    'type': 'text_general',
                    'indexed': False,
                    'multiValued': False,
                    'stored': False,
                    'termVectors': False,
                    'termPositions': False,
                    'termOffsets': False,
                },
                'copy-to': {'_text_'},
            },
            {
                'solr-conf': {
                    'name': 'woordS',
                    'type': 'text_general',
                    'indexed': False,
                    'multiValued': True,
                    'stored': False,
                    'termVectors': False,
                    'termPositions': False,
                    'termOffsets': False,
                },
                'copy-to': {'_text_'},
            },
        ],
    }
