# -*- coding: utf-8 -*-

def get(**kw):
    return {
        'do_make_query_parser': True,
        'do_prepare_export_scripts': True,
        'do_frontend_main_tst': True,
        'do_frontend_main_acc': True,
        'do_frontend_main_prd': True,
        'do_pars': True,
        'do_cit': True,
        'do_lemma': True,
        'do_site': True,
    }
