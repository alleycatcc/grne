# -*- coding: utf-8 -*-

import proj_paths # noqa

from enum import Enum
import re

from acatpy.util import pathjoin

Status = Enum('Status', [
    'xml_ok',
    'xml_ok_na_wijziging',
    'xml_in_bewerking',
    'eindredactie_ok',
    'eindredactie_in_bewerking',
])

StatusT1 = Enum('StatusT1', [
    'none',
    'uitgevoerd',
    'gecontroleerd',
])

# --- (string-value, ok)
status_codes_table = {
    Status.xml_ok: ('xml ok', True),
    Status.xml_ok_na_wijziging: ('xml ok na wijziging', True),
    Status.xml_in_bewerking: ('xml in bewerking', False),
    Status.eindredactie_ok: ('eindredactie ok', True),
    Status.eindredactie_in_bewerking: ('eindredactie in bewerking', False),
}

# --- (string-value, t1-has-been-run, t1-has-been-checked)
status_t1_codes_table = {
    StatusT1.none: ('', False, False),
    StatusT1.uitgevoerd: ('uitgevoerd', True, False),
    StatusT1.gecontroleerd: ('gecontroleerd', True, True),
}

status_strings_good = [s for _, (s, ok) in status_codes_table.items() if ok]

status_t1_strings_run = [s for _, (s, run, _) in status_t1_codes_table.items() if run]
status_t1_strings_not_run = [s for _, (s, run, _) in status_t1_codes_table.items() if not run]
status_t1_strings_checked = [s for _, (s, _, checked) in status_t1_codes_table.items() if checked]

'''
This is used (for now) in the broken link tool.
New letter directories added to the XML sources must match the keys of this
dictionary.
'''

letter_map = {
    'Alpha': 'α',
    'Beta': 'β',
    'Gamma': 'γ',
    'Delta': 'δ',
    'Epsilon': 'ε',
    'Zeta': 'ζ',
    'Eta': 'η',
    'Theta': 'θ',
    'Iota': 'ι',
    'Kappa': 'κ',
    'Labda': 'λ',
    'Mu': 'μ',
    'Nu': 'ν',
    'Xi': 'ξ',
    'Omicron': 'ο',
    'Pi': 'π',
    'Rho': 'ρ',
    'Sigma': 'σ',
    'Tau': 'τ',
    'Upsilon': 'υ',
    'Phi': 'φ',
    'Chi': 'χ',
    'Psi': 'ψ',
    'Omega': 'ω',
}

def get(rootpath):
    web_path = pathjoin(rootpath, 'web')
    web_db_path = pathjoin(web_path, 'db')
    web_htdocs_path = pathjoin(web_path, 'htdocs')
    web_main_htdocs_path = pathjoin(web_htdocs_path, 'main')
    solr_path = pathjoin(rootpath, 'solr')
    lib_path = pathjoin(rootpath, 'lib')
    lib_grne_path = pathjoin(lib_path, 'grne')
    query_parser_path = pathjoin(lib_grne_path, 'query_parser')
    schoolreductie_path = pathjoin(lib_path, 'schoolreductie')
    frontend_src_path = pathjoin(rootpath, 'frontend')
    bin_path = pathjoin(rootpath, 'bin')

    return {
        'bin_path': bin_path,
        'html-export-prepare-binpath': pathjoin(bin_path, 'html-export-prepare'),
        'html-export-grabzit-binpath': pathjoin(bin_path, 'html-export-grabzit'),
        'html-export-scripts-path': pathjoin(bin_path, 'html-export-scripts'),
        'web_pars_db_path': pathjoin(web_db_path, 'pars.db'),
        'web_cit_db_path': pathjoin(web_db_path, 'cit.db'),
        'web_lemma_db_path': pathjoin(web_db_path, 'lemma.db'),
        'web_main_build_path_tst': pathjoin(web_main_htdocs_path, 'tst', 'build'),
        'web_main_build_path_acc': pathjoin(web_main_htdocs_path, 'acc', 'build'),
        'web_main_build_path_prd': pathjoin(web_main_htdocs_path, 'prd', 'build'),
        'web_main_frontend_src_path': pathjoin(frontend_src_path, 'main'),
        'stylesheet_pre_index_stage_0': pathjoin(solr_path, 'pre_index_stage_0.xsl'),
        'stylesheet_pre_index_stage_1': pathjoin(solr_path, 'pre_index_stage_1.xsl'),
        'solr_conf_dir': pathjoin(solr_path, 'config', 'config-grne'),
        'solr_core_name': 'grne',
        'solr_fulltext_field': '_text_',
        'solr_highlight_markers': ('__START__', '__STOP__'),
        'solr_fields_to_return': ('hoofdW-clean',),
        'query_parser_generate_dir': pathjoin(query_parser_path, 'antlr4_parser'),
        'query_parser_grammer_spec': pathjoin(query_parser_path, 'Query.g4'),
        'status_strings_good': status_strings_good,
        'status_t1_strings_checked': status_t1_strings_checked,
        'status_t1_strings_run': status_t1_strings_run,
        'status_t1_strings_not_run': status_t1_strings_not_run,
        # --- for both advanced & main lemma index.
        'lemma_filename_filters': [
            lambda x: not re.search('^.*hulpdocumenten/', x),
            lambda x: not re.search('^.*t2-testlemmata/', x),
            lambda x: not re.search('^.*projectdocumenten/', x),
            lambda x: not re.search('^.*metadata/', x),
            lambda x: not re.search('^.*test/', x),

            # --- @debug
            # lambda x: re.search('ἐγώ.xml', x),
        ],
        'python_extension_ns': 'python-extension-ns',
        'schoolreductie_xslt_t2_path': pathjoin(schoolreductie_path, 'grne_interpunction.xslt'),
        'search_highlight_class': 'search-highlight',
        'citG_class': 'citg-word',
        'highlight_snippet_roots': ('lem', 'verwLem', 'xlLem', 'escLem'),
        # --- test case: remove werkV, search 'maken and creatieve'
        # --- 'vorm' is handled separately inside the stylesheet.
        'highlight_snippet_ancestors': (
            'hoofdW', 'etym', 'morfI', 'cit', 'gebrW', 'vertM', 'werkV',
        ),
        # --- don't abridge snippets which are shorter than this.
        'search_highlight_abridge_cutoff_length': 500,
        'letter_map': letter_map,
        'max_file_upload_mb': 20,
    }
