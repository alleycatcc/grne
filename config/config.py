# -*- coding: utf-8 -*-

from functools import namedtuple
import os.path as path

from acatpy.util import pathparent, pathjoin

import config_build
import config_local
import config_main
import config_solr_schema
import config_user

# --- use the location of this config file as the starting point for
# calculating everything else.
# --- note that __file__ can be relative, so we need to do this only once
# when the module is first interpreted.

this_file = path.abspath(__file__)
rootpath = pathparent(path.dirname(this_file))
sources_path = pathjoin(rootpath, 'sources')

def get(config_script=None):
    Conf = namedtuple('Conf', [
        'main', 'local', 'user', 'build', 'schema', 'script',
    ])

    return Conf(
        config_main.get(rootpath),
        config_local.get(sources_path),
        config_user.get(),
        config_build.get(),
        config_solr_schema.get(),
        config_script,
    )
