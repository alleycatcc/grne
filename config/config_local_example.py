# -*- coding: utf-8 -*-

import proj_paths # noqa

from acatpy.util import pathjoin

def get(sources_path):
    lemma_input_path = pathjoin(sources_path, 'lemmata')
    return {
        'lemma_input_path': lemma_input_path,
        't2_test_path': pathjoin(
            lemma_input_path, 't2-testlemmata'
        ),
        'lemmatiser_path': pathjoin(
            sources_path,
            'lemmatiser',
            'GreekLexiconLogeion-20131219.db',
        ),
        # --- for running the flask server locally; not necessary for normal
        # builds.
        'solr_url': None,
        # --- for using the cli analyse mode or bin/solr-search.py; not
        # necessary for normal builds.
        'solr_cli': {
            # 'tst': 'http://some-host:8983/solr/grne',
            # 'acc': 'http://some-host:8983/solr/grne',
        },
    }
