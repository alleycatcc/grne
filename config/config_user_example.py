# -*- coding: utf-8 -*-

import proj_paths # noqa

def get(**kw):
    return {
        'saxon_jar_path': '/usr/share/java/Saxon-HE-9.9.0.1.jar',
        # --- only for console analyse
        'browser_path': '/etc/alternatives/x-www-browser',
        # --- for administering a local server using the 'solr' binary; see
        # also 'env' in the local config.
        'solr_port': 8983,
        'solr_path': '/opt/solr/solr-7.5.0',
        # --- necessary?
        'solr_data_path': '/var/local/grne-solr',
        'antlr4_path': '/usr/bin/antlr4',

        # --- redis_client_app is a connection to a redis server which
        # belongs to this running instance of the app. It handles app-level
        # caching (optional -- caching by the proxy server is better), and
        # storing of job ids for analyse batch mode (mandatory).

        # In the current architecture, this means it runs in the docker
        # container in which the app runs.

        # It may also persist its storage using a mounted folder in order to
        # persist if the instance is restarted (not to be confused with
        # `redis_client_persist`, which persists *across builds*).

        # This server must be running at the time the app is started.

        # The reason we still split it up per environment is for local
        # development. When it's deployed each server is isolated in its own
        # environment anyway so this isn't actually necessary.

        'redis_app': {
            'tst': {
                'host': 'localhost',
                'port': 6379,
                'db': 3,
                'socket_connect_timeout': 10,
            },
            'acc': {
                'host': 'localhost',
                'port': 6379,
                'db': 2,
                'socket_connect_timeout': 10,
            },
            'prd': {
                'host': 'localhost',
                'port': 6379,
                'db': 1,
                'socket_connect_timeout': 10,
            },
        },

        # --- redis_client_persist is a connection to a redis server which
        # persists across builds.

        # It is available not only to the app while it's running but also
        # during the build. Both of them can use it for reading & writing.
        # To do this we run it on an openly reachable tcp port with a
        # password.

        # In the current architecture it runs on the same host that apache
        # runs on, and is mounted into the docker container using a shared
        # port.

        # So this is the place we cache the T2 transforms.

        # This server *may* be running at the time the app is started. If
        # not we will show a warning that the T2 cache is disabled but not
        # halt the app.

        # It is configured using environment variables.

        # Note that it has no notion of environments (it couldn't, since
        # it's available while building and our back-end build doesn't
        # distinguish between environments).
    }
